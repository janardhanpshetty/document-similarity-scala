### Topic: This article discusses what happens when you click the "View" button for the different file types on the Notes & Attachments related list.
You may notice that when clicking the "View" button on the Notes & Attachments related list the behavior of the link changes depending on the file type of the attachment.
 
Resolution
Common file extensions and the related browser behavior
 
.pdf/.png/.jpeg
 - The browser opens a new tab and displays the .PDF file.
 
For .PDF, .PNG and .JPG files, if you want to download these files instead of opening them in the browser you may hold the "ALT" button on the keyboard and left click on "View". These types of file can be opened using the browser because the browser natively supports the opening of these file types. Alternatively, you can also right click on the "View" link and choose "Save link as".
.
doc/docx, .xls/xlsx, .ppt/pptx
 - Depending on the browser and setting it will either open the browser's download window and the user can click on the file to open it, or immediately open the default application set to open the file.
 
For .DOC, XLS and PPT files, since the browser does not display these natively, it will be downloaded on the host computer and opened using the corresponding applications.
 
Good to Know: 
These are the common behaviors for these file types, however, there are browser specific settings and extensions that change how the browser handles the file. First, check that you are using a 
supported browser
 for Salesforce and then check your browser settings to make any modifications. 
