### Topic: This article talks about loading data to currency fields through the API.
To answer your question with integrating data through the API. I wanted to explain why this is occurring. The field type for a currency field is the Double type primitive. This has a slightly different behavior in the API versus through the UI when it comes to rounding numbers. 
 
Double values. Fields of this type can contain fractional portions (digits to the right of the decimal place), such as ConversionRate in CurrencyType. In the API, all non integer values (such as double Currency Field Type and Percent Field Type) contain values of type double. Some restrictions may be applied to double values: 
 
• scale: Maximum number of digits to the right of the decimal place. 
• precision: Total number of digits, including those to the left and the right of the decimal place 
 
The maximum number of digits to the left of the decimal place is equal to precision minus scale. In the online application, precision is defined differently—it is the maximum number of digits allowed to the left of the decimal place. 
 
In this case, the Scale of the field is 2 and the precision is 16 when defined through the UI. The API has the unique ability to store the value with a more precise value. When the value is saved through the UI it is rounded prior to storing the value. When the value is loaded through the API no rounding occurs. Unfortunately it is not possible to modify this functionality in the UI. When you are viewing the data in a record or detail record, it will show as it is rounded. From looking at the data for example when you export the data, you will see all character values for the currency fields that were loaded through your integration with the API.
Resolution
To answer your question with integrating data through the API. I wanted to explain why this is occurring. The field type for a currency field is the Double type primitive. This has a slightly different behavior in the API versus through the UI when it comes to rounding numbers. 
 
Double values. Fields of this type can contain fractional portions (digits to the right of the decimal place), such as ConversionRate in CurrencyType. In the API, all non integer values (such as double Currency Field Type and Percent Field Type) contain values of type double. Some restrictions may be applied to double values: 
 
• scale: Maximum number of digits to the right of the decimal place. 
• precision: Total number of digits, including those to the left and the right of the decimal place 
 
The maximum number of digits to the left of the decimal place is equal to precision minus scale. In the online application, precision is defined differently—it is the maximum number of digits allowed to the left of the decimal place. 
 
In this case, the Scale of the field is 2 and the precision is 16 when defined through the UI. The API has the unique ability to store the value with a more precise value. When the value is saved through the UI it is rounded prior to storing the value. When the value is loaded through the API no rounding occurs. Unfortunately it is not possible to modify this functionality in the UI. When you are viewing the data in a record or detail record, it will show as it is rounded. From looking at the data for example when you export the data, you will see all character values for the currency fields that were loaded through your integration with the API.
