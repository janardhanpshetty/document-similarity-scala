### Topic: Refreshing Report Chart Data | Salesforce
Refreshing Report Chart Data | Salesforce
Refreshing Report Chart Data
Normally, charts refresh data once every 24 hours. But you can change a chart to refresh each time someone opens the page it’s on. The refresh option is under the Chart Properties dialog box of the page layout editor. However, we recommend daily refresh over selecting the option, because users will soon reach the refresh limit or will wait for chart data to show until refresh is complete.
Daily Refresh
Charts refresh data once every 24 hours. If within that time users want the latest, they can click 
Refresh
 on the chart.
Refresh When User Opens the Page
To change a chart’s normal refresh, select 
Refresh each time a user opens the page
 in the Chart Properties dialog box of the page layout editor. This option triggers a chart refresh each time someone opens the page the chart is on. Selecting the option is not recommended for two reasons.
There’s a risk of reaching the chart refresh limit faster. Refreshes count towards the hourly limit for each user and organization.
For reports that take longer to run, selecting this option can make users wait to see chart data.
Parent topic:
 
Customizing a Report Chart in a Page Layout
Previous topic:
 
Filtering Report Charts to Show Data Relevant to the Page
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_embed_chartdialog_datarefresh.htm&language=en_US
Release
202.14
