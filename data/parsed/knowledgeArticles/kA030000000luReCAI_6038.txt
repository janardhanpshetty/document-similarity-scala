### Topic: The article explains how to fix Salesforce for outlook Sync error while converting single event to a recurring event
Salesforce for Outlook 2.8 
and later has improved the sync error messaging.
If you sync a single event from Outlook into Salesforce or bi
directionally 
and then you convert that single event to a recurring events in Outlook, Salesforce For Outlook (SFO) sync would fail and you'd see the SFO icon in the system tray changing to a red exclamation sign.
If you double click on this icon you would see the following error  
 
 
Also the Trace.log file would have a more meaningful error message logged
 
2015-05-12 19:09:10,930 [SyncThread] ERROR syncengine      
[Event] An error occurred in converting OL record Meeting SFM - weekly of type Event. 
Error: Sfdc.Synchronization.InvalidUpdateOperationException: Detected a previously-synced single event that was promoted to a series in outlook
See the 
Article Number  000214863  - Salesforce for Outlook - Location of the log files to locate
 and check the SFO log files
Resolution
1- You would need to delete that event in Outlook and create a new recurring event 
2- Also search for it in the Salesforce to make sure that it hasn't syncd partially or as a single event and delete it
3- Restart Salesforce for Outlook and sync again
