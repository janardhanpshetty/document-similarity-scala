### Topic: How to use Splunk to determine accurate pageview count, agent performing the pageview, and resources performing page views when a customer is reporting that the pageview is being calculated incorrectly.
Each Edition of Salesforce org comes with different maximum Page views limits. There can be times that customer is reporting they hit the pageview limit even though they do not have that much traffic. They need us to identify what caused the high Pageview limit.
 
Resolution
From our Help and Training menu (
https://help.salesforce.com/HTViewHelpDoc?id=sites_limits.htm&language=en_US#SitesPageViewsWhatCounts
), it shows what components are count for pageview. Currently, the only missing piece from the doc is Content. If a page from Force.com site is using Content, the content counts as  pageview.
In additional there is appExchange tool to show the pageview for each site in an org. Customer can get the package from 
https://appexchange.salesforce.com/listingDetail?listingId=a0N30000001SUEwEAO
. The package includes different reports to show the pageview for each site.
If customer believes the pageview count is calculated incorrectly, we can use splunk to determine the hit of each page, the agent who accesses the page, and what resources are used for the pageview.
To determine what are agents that that access the site.
index=na7 organizationId=00DA0000000Y7LJ logRecordType=J siteId=0DMA0000000TPpC earliest=01/01/14:00:00:00 latest=01/07/14:00:00:00 pageName="*"
|eval agentType=IF(like(User_Agent,"%MSOffice%"),"MsOffice",
IF(like(User_Agent,"%crawler%") OR like(User_Agent,"%bot%") OR isNull(User_Agent),"Crawler",
"Other")) |stats count by agentType
To determine the hit count for each page during a time span.
index=na7 organizationId=00DA0000000Y7LJ logRecordType=J siteId=0DMA0000000TPpC earliest=01/01/14:00:00:00 latest=01/07/14:00:00:00 pageName="*" | stats count by pageName
If the number does not add up and it does not match the report, we need to access the Site Proxy.
First, start to check what are the components that count as pageview. This result should be close to the result you got from the previous query.
index=sites sourcetype=sites_squid_log earliest=02/03/14:16:00:00 latest=02/04/14:15:59:59 uri_host="
www.vta.org
" cacheResult!="*HIT*" requestUrl!="*/resource/*" requestUrl!="*/servlet/*" (content_type="text/html" OR content_type="text/xml" OR content_type="text/plain" OR requestUrl="*.apexp" OR requestUrl="*.jsp")
Second, run the following splunk query to identify what other components are used for pageView count.By adding the events below and the previous one, it gives you the number that is closed to the report.
index=sites sourcetype=sites_squid_log earliest=02/03/14:16:00:00 latest=02/04/14:15:59:59 uri_host="
www.vta.org
" cacheResult!="*HIT*" requestUrl!="*/resource/*" requestUrl!="*/servlet/*" requestUrl="*/sfc/servlet.shepherd/*"
Note: The report is 8hrs ahead of GMT. We will need to subtract 8 hrs to obtain GMT to get the correct result.
Investigation from R&D- 
https://gus.my.salesforce.com/apex/adm_workdetail?id=a07B0000000cuAWIAY
 
