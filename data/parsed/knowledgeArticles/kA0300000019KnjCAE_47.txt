### Topic: Find out more about why MyDomain sign in isn't functioning as expected and how to fix it.
Customer was able to login to their organization via their MyDomain URL e.g. org62.my.salesforce.com but suddenly the login no longer brings the user into the org instead brings them to the login page instead of their IDP or login returns them to the login page under the MyDomain url again. 
This can be caused by duplicate usage of the same MyDomain URL. Typically this is impossible for a customer / partner / user to get into this situation as when signing up domains they have to check if they are used or not and aren't allowed to use them if already in use however some instances where a support agent or something with BT access may have
a) changed the MyDomain field in a production org to the same as another live organization thus bypassing the existing check
b) signed up a copy of an org into production thus copying the MyDomain of the org into another live organization and creating a duplicate or
c) In Sandbox we've seen this happen when the production org MyDomain is changed by support from one org to another. 
e.g.
- Org1 has MyDomain company1, Org2 has no MyDomain.
- Both orgs have 3 sandboxes called UAT, SIT and Dev.
- Support on customer request changes the Org1 MyDomain to blank and Org2 MyDomain to company1. 
- Customer refreshes the sandboxes on Org2 and they now have the MyDomain company1--UAT-cs*.my.salesforce.com thus duplicating the Sandboxes from Org1 which were not refreshed resulting in duplicated domains and failing logins.  
Resolution
If Production
, check for duplicate MyDomain
Use BT page /domains/servlet.OrgDomainMappings (add this to the end of any of the BT instance URLs) to get a list of orgs with same domain prefix. OrgDomainMappings does not display cloudforce.com domains and may be missing some orgs due to limitations to the page size.
Check (replacing <MyDomain> with the domain part e.g. org26) 
https://bt1.my.salesforce.com/services/mydomain/OrgDomainInstanceFinder?mydomain=<MyDomain>&response=xml
If this gacks then we know we have a duplicate domain.
Which org had the domain first? The other one needs to be changed. Investigating the duplicate orgs BT should reveal how the org got the same domain, usually this is either the org was signed up in production using a Template with the domain in question or a engineer manually changed the domain via BT (We don't perform ultraDNS checks when domains are changed via BT).
Solution: 
Change domain in duplicate org and if required change domain in first org DomainA -> DomainB  and back DomainB -> DomainA.
-------------------------------------------------------------------
If Sandbox
, check for duplicate MyDomain
Use BT page /domains/servlet.OrgDomainMappings get a list of orgs with same domain prefix.
Go through the list to check each org domain name to find out the other org id that has duplicate domain name(domain--sandbox name).
Solution: 
Change domain in duplicate org(s) and if required change domain in first org DomainA -> DomainB  and back DomainB -> DomainA to force the DNS registration and CNAME update.
Notes:
For more common issues with MyDomain check out the 
CCE Security teams Google Site
.
