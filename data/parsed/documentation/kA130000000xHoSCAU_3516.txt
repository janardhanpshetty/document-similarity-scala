### Topic: Lightning Experience Record Pages in Lightning App Builder (Pilot) | Salesforce
Lightning Experience Record Pages in Lightning App Builder (Pilot) | Salesforce
Lightning Experience Record Pages in Lightning App Builder (Pilot)
In Developer Edition orgs, you can use the 
Lightning App Builder
 to create and customize 
Lightning Experience
 record pages.
Available in: 
Lightning Experience
Available in: 
Developer
 Edition
Note
Creating and editing 
Lightning Experience
 record pages using the 
Lightning App Builder
 is currently available to Developer Edition organizations through a pilot program. Pilot programs are subject to change, and as such, we cannot guarantee a particular time frame in which this feature can be enabled. Any unreleased services or features referenced in this document, press releases, or public statements are not currently available and may not be delivered on time or at all. Customers who purchase our services should make their purchase decisions based on features that are currently available.
Before you can create or edit 
Lightning Experience
 pages with the 
Lightning App Builder
, enable the feature in your Developer Edition org. From Setup, enter 
App Builder
 in the 
Quick Find
 box, then select 
Lightning App Builder
. From the 
Lightning App Builder
 page, select 
Enable App Builder for 
Lightning Experience
.
Note
If you enable the pilot feature and customize pages, and then disable the feature, all your customizations are hidden. Your 
Lightning Experience
 record pages then revert to the standard 
Salesforce
 default page format.
You can also create custom 
Lightning
 components that work on record home pages in 
Lightning Experience
. To learn more about that, see “
Configure Components for 
Lightning Experience
 Record Home Pages (PILOT)
” in the 
Lightning
 Components Developer Guide
.
Configure Lightning Experience Record Pages Using the Lightning App Builder (Pilot)
You can use the 
Lightning App Builder
 to add, remove, or reorder components on a record page in 
Lightning Experience
 to give your users a customized view for each object’s records.
Customize Tabs on Lightning Experience Record Pages Using the Lightning App Builder (Pilot)
With the 
Lightning App Builder
, you can create, update, delete, and change the order of tabs and tab sets on record pages in 
Lightning Experience
. Configure the tabs that your users see, name them whatever you like, and add a custom set of components to each tab.
Considerations for Creating and Editing Lightning Experience Pages with the Lightning App Builder (Pilot)
Here are some things to keep in mind when working with 
Lightning Experience
 pages in the 
Lightning App Builder
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=lightning_app_builder_customize_record_home_pilot.htm&language=en_US
Release
200.20
