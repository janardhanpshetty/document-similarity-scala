### Topic: Setting Up the Salesforce Connector | Salesforce
Setting Up the Salesforce Connector | Salesforce
Setting Up the Salesforce Connector
Set up your Salesforce connector for either production or sandbox environments.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Choose the option that best describes your situation.
If you’re setting up the connector for
You’ll set up the connector using the instructions in
Either a production or a sandbox environment for the first time
Set Up the Connector to Sync with Either Production or Sandbox Environments
A sandbox environment in addition to an already existing connector you’ve already set up for your production environment
Set Up the Connector to Sync with Sandbox Environments
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=pardot_get_start_setting_up.htm&language=en_US
Release
202.14
