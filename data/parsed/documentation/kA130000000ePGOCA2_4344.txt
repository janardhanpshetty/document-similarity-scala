### Topic: Learn Wave Analytics with Trailhead | Salesforce
Learn Wave Analytics with Trailhead | Salesforce
Learn Wave Analytics with Trailhead
Trailhead is the fun and free way to learn Salesforce. Now you can get acquainted with Wave Analytics through Trailhead, using a special Wave-enabled Developer Edition org.
For the Wave Analytics trails, you can’t use an old Developer Edition org. You must sign up for the special Developer Edition that comes with a limited Analytics Cloud Wave Platform license and contains sample data required for the Wave trails.
To access all of the Wave Analytics trails, start here: 
https://developer.salesforce.com/trailhead/trail/wave_analytics_explorer
. You’ll find the sign up for the special Developer Edition org in the Wave Analytics Basics trail.
See Also:
Wave Analytics Limits
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_tutorials.htm&language=en_US
Release
202.14
