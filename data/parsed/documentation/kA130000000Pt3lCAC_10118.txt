### Topic: Limits for Visual Workflow | Salesforce
Limits for Visual Workflow | Salesforce
Limits for Visual Workflow
When using Visual Workflow, keep flow limits and Apex governor limits in mind.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Maximum number of versions per flow
50
Maximum number of executed elements at run time
2,000
Maximum number of active flows and processes per org
500
Maximum number of flows and processes per org
1,000
Maximum number of flow interviews or groups of scheduled actions (from processes) that are waiting at one time
30,000
Maximum number of flow interviews that are resumed or groups of scheduled actions that are executed per hour
1,000
Maximum number of relative time alarms defined in flow versions or schedules based on a field value in processes
20,000
Apex Governor Limits that Affect Flows
Salesforce strictly enforces limits to ensure that any runaway flows don’t monopolize shared resources in the multitenant environment. Per-transaction limits, which Apex enforces, govern flows. If an element causes the transaction to exceed governor limits, the system rolls back the entire transaction. The transaction rolls back even if the element has a defined fault connector path.
Flows in Transactions
Each flow interview runs in the context of a 
transaction
. A transaction represents a set of operations that are executed as a single unit. For example, a transaction can execute Apex triggers and escalation rules in addition to a flow interview. If one interview in a transaction fails, all the interviews in the transaction are rolled back, as well as anything else the transaction did. The transaction doesn’t retry any of the operations—including the flow interview.
Flow Bulkification in Transactions
Programmers can design their code so that similar actions are performed together in one batch. For example, one operation to create 50 records rather than 50 separate operations that each create one record. This process is called 
bulkification
, and it helps your transaction avoid governor limits. If you’re working with flows, you don’t even have to think about bulkification. Flow interviews bulkify actions for you automatically.
See Also:
Visual Workflow
Limits and Considerations for Visual Workflow
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_admin_flow_limits.htm&language=en_US
Release
202.14
