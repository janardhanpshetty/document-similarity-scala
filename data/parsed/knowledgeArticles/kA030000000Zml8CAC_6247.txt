### Topic: This article will go over the reason why this can not be removed.
We have a client that has Licenses to our Customer Community and has asked us if we can remove the specification of (Client / Customer) whenever they post on chatter.
Resolution
Unfortunately this is working by design.
The main reason for this specification next to a Customer's Name when they post is "Security".  This way your Internal Users are aware about customers and know to be extra cautious when posting on that group. As any post they make is visible to your customer, you need to be careful about posting any confidential information.
 
