### Topic: Solution to follow if you receive a "Exception: Failed to move template to results folder rename limit was exceeded (100)" error message, "Failed to download the template file https..." message or "Please ensure that Microsoft Word is installed on your machine.
What is the mail merge error "Failed to download..." or "Failed to move...?"
Resolution
If you receive a 
"Exception: Failed to move template to results folder rename limit was exceeded (100)"
 error message, 
"Failed to download the template file https..."
 message or 
"Please ensure that Microsoft Word is installed on your machine. Word could not fire the event"
 message, please attempt the following:
1. In Internet Explorer click the Tools menu, then select Internet Options.
2. On the General tab, locate the Temporary Internet Files section and click the Settings button. Select the "Every visit to the page" radio button and ensure there is enough disk space for the cache (the ideal cache size is between 20 and 50 megabytes (MB)).Make sure your computer has enough disk space on its hard drive. When you are finished, click the OK button.
3. In the Temporary Internet Files section in the General tab, click the "Delete Cookies" and "Delete Files" buttons.
4. Click the Privacy tab. In the Web Sites area or using the Sites button click the Edit button. In the Web Address text box, enter "https://*.salesforce.com" (without the quotation marks) and click the Allow button. When you are finished, click the OK button.
5. Click the Advanced tab. Scroll to the bottom of the Settings box. The "Do not save encrypted pages to disk" check box should NOT be selected.
6. Also on the advanced tab, the "Empty Temporary Internet Files Folder when browser is closed" check box should be selected, as well as TLS 1.0 must be used.
7. Click the Security tab. Click the "Internet" icon (represented by a globe graphic). Click the Custom Level button. Scroll almost to the bottom of the Settings box to "Active Scripting". Also ensure that the Enabled option is selected. Now scroll to the miscellaneous options, in the option that says “Display Mixed Content” and makes sure this is set to Enabled. When you are finished, click the OK button.
8. Also on the Security tab, click the "Trusted Sites" icon (represented by a check mark graphic). Click the Sites button. In the "Add this Web site to the zone" text box, enter "https://*.salesforce.com" (without the quotation marks) and click the Add button. When you are finished, click the OK button.
9. With the "Trusted Sites" icon still selected, click the Custom Level button. Scroll to the option that says “Display Mixed Content” and makes sure this is set to Enabled.
10. Click the "OK" button.
11. Close all open browser windows, re-launch Internet Explorer, and login to salesforce.com and try the mail merge.
12. If the above steps do not help please also try deleting the folder "Salesforce Merge Results" that can be found on your desktop and doing the mail merge again.
 
Note: If none of the steps above work to resolve the problem, please contact customer support to have a case opened for your issue.
Also refer to the following articles 
Mail Merge 'Trusted Add Ins' & 'Macro Security Settings'
  and 
Performing a Mail Merge using Office 2010 with Windows 7 results in an error
Note:
Before enabling the extended mail merge in the user interface please make sure for different browser like "Google chrome & Mozilla Firefox" you get the Bookmarks & Settings Imported from IE and then activate the Extended mail merge in the UI and it should resolve the error message.
