### Topic: Standard case auto-response stops working when trigger is active.
Standard auto-response rules stop working if a trigger on case updates the case in context in the AfterInsert phase.
 
Resolution
To solve this you should consider making the update in the BeforeInsert so that a new update is not initiated for the case. If absolutely necessary to update the case in the AfterInsert phase, then you should use DMLOptions and set EmailHeader.TriggerAutoResponseEmail to true.
