### Topic: This article explains what permissions are granted by the "Add Tags" Library permission and why users without are able to tag Content.
You might notice that your CRM Content users are able to add tags to content, even when they don't have the "Add Tags" library permission.
Resolution
A user can always add tags to content as long as they are a member of the Library in question and 
any 
of the following permissions is assigned to them:
Manage Library
Add Content
In other words, having 
either
 permission will automatically give you the ability to tag content. 
What does the "Add Tags" library permission do?
This permission allows you to tag content if for some reason, you don't have any of the permissions stated above, allowing you to tag content only. 
 
