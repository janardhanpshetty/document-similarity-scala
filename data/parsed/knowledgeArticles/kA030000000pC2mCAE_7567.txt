### Topic: Administrators and end user might get an error message when authenticating to Appexchange marketplace with their salesforce credentials "There was a problem with your authentication attempt. Please try again. If you continue to encounter problems, contact your administrator"
Administrators and end user might get an error message when authenticating to Appexchange marketplace with their salesforce credentials "There was a problem with your authentication attempt. Please try again. If you continue to encounter problems, contact your administrator"
 
Resolution
1) Go to Administration Setup ->Manage App -> Connected Apps OAuth Usage
2) Select "Appexchange"
3) Select Unblock
The above steps should resolve this issue.
