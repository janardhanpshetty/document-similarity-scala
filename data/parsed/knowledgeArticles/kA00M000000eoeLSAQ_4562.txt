### Topic: Case List lightning component in Communities can display an empty table when there is a mismatch between Filter Name and existing List Views.
The Community Builder can be used to add a "Case List" lightning component to display a list of Case records.  If this list shows up empty (does not display Case Details), it signifies a problem with the Case Filter Name or ID.
In order for a Case List component in Communities to display correctly, its "Case Filter Name or ID" must match an existing Case List View in the internal organization.
Resolution
To correct this issue:
1. In the internal organization, click the "
Edit
" link next to the Case List View you are trying to display in Communities.
2. Copy the value in the "
View Unique Name
" field.
3. Navigate back to Community Builder | 
Page Editor
.
4. Select the Page that contains the affected Case List.
5. Click on the Case List to display the 
Property Editor
 in the right-hand pane.
6. Update "Case Filter Name or ID" with the "Unique Name" copied in Step 2.
7. Click the 
Publish 
button in the upper right to save the change.
