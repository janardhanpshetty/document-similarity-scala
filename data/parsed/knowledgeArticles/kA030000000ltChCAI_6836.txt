### Topic: Access to the Private AppExchange App must be granted using the permission sets that are created when you install the app.
When I installed the 
Private AppExchange app
, two permission sets were created for granting access to the app: Store User and Store Admin. Is it possible to grant access to the Private AppExchange with profiles instead of these permission sets? 
Resolution
Access 
must be granted using the Store Admin or Store User permission set. The Private AppExchange app is designed using code that depends on one of the two permission sets being assigned.
If access is granted using profiles instead of permission sets, users will receive an error message:
"You do not have access to the store. Please contact your Salesforce system administrator for assistance." 
 
