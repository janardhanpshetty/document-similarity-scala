### Topic: Reports and Dashboards: What’s Not in Lightning Experience | Salesforce
Reports and Dashboards: What’s Not in Lightning Experience | Salesforce
Reports and Dashboards: What’s Not in Lightning Experience
Learn about the issues to expect when using reports and dashboards in Lightning Experience. Some Salesforce data and features aren’t available in the new user interface. And some features don’t have full parity with what’s available in Salesforce Classic.
Reports
Features Available with Limitations in Lightning Experience
Feature
Notes about Lightning Experience Availability
Object Support for Reporting
Even though Lightning Experience doesn’t support all the standard Salesforce objects yet, the Report Builder in Lightning Experience does. Users can create and view reports for all standard objects. Reports related to objects not yet supported in Lightning Experience open directly in the new interface and behave like any other report.
Historical Trend Reporting
Historical trend reporting with charts is supported in Lightning Experience, but tabular views of historical trend reports aren’t available.
Export Reports
Reports exported from Lightning Experience never include a disclaimer that reads “Confidential Information - Do not Distribute” in the report footer. The disclaimer is excluded even if you disable the 
Exclude Disclaimer from Exported Reports
 setting in setup.
Filters
These filters are available when building or editing a report, but are not shown in the filter panel when viewing a report. Even though the filters are not shown, they still filter the report.
Row limit filters
Historical field filters
Standard filters (except for date, picklist, and string filters)
Relative date filters, such as 
Created Date equals LAST 7 DAYS
, aren’t supported.
Features Not Available in Lightning Experience
Feature
Notes about Lightning Experience Availability
Joined Reports
Schedule Report Refreshes
Users can’t schedule report refreshes in Lightning Experience, but scheduled report refreshes from Salesforce Classic continue to work in Lightning Experience.
Print Reports
Lightning Experience doesn’t offer a print option, but users can still print reports using their browser’s 
print
 function.
Subscribe to Reports
Add to Campaign
Role Hierarchies
Accessibility Mode for the Report Builder
Users with Accessibility Mode turned on must switch to Salesforce Classic to create and edit reports.
Dashboards
Considerations When Using Dashboards in Lightning Experience
After saving a dashboard in Lightning Experience, you can’t edit it in Salesforce Classic. Instead of editing a Salesforce Classic dashboard in Lightning Experience, consider cloning the dashboard and editing the clone. That way, you can still edit the original dashboard in Salesforce Classic.
When you drill into a report from a filtered dashboard in Lightning Experience, the report isn’t filtered. When you drill into a report from a filtered dashboard in Salesforce Classic, the report is filtered in the same way as the dashboard.
Features Not Available in Lightning Experience
Feature
Notes about Lightning Experience Availability
Create, Edit, or Delete Filtered Dashboards
Lightning Experience users can view filtered dashboards. Filtered dashboards always open with no filters applied. It’s possible to apply or clear filters that were added in Salesforce Classic.
Switch to Salesforce Classic to create, edit, or delete filtered dashboards, or to add, change, or remove dashboard filters.
Create, Edit, or Delete Dynamic Dashboards
Users can view dynamic dashboards, but must switch to Salesforce Classic to create, edit, or delete them.
Schedule Dashboard Refreshes
Users can’t schedule dashboard refreshes in Lightning Experience, but scheduled dashboard refreshes from Salesforce Classic continue to work in Lightning Experience.
Post a Dashboard Component to Chatter
Visualforce Components on Dashboards
Link from a Dashboard Component to a Website or Email Address
Accessibility Mode for the Dashboard Editor
Users with accessibility mode turned on can still create and edit Lightning Experience dashboards, but for a fully accessible experience they must switch to Salesforce Classic.
Folders
Features Not Available in Lightning Experience
Feature
Notes about Lightning Experience Availability
Standard Folders, containing default reports and dashboards that come packaged with Salesforce
Move Reports and Dashboards between Folders
Create and Delete Report and Dashboard Folders
Folders created in Salesforce Classic and their contents are available in Lightning Experience. Users can save inside these folders when creating or cloning a report or dashboard.
Share Report and Dashboard Folders
Sharing permissions set on folders in Salesforce Classic are obeyed in Lightning Experience, but sharing permissions can’t be set nor changed.
Search for Report and Dashboard Folders
Users can’t search for report and dashboard folders in Lightning Experience. Global search results include reports and dashboards, but not report and dashboard folders.
Charts
Considerations When Using Charts in Lightning Experience
Feature
Notes about Lightning Experience Availability
Table Charts
To create table charts, or to edit them, switch to Salesforce Classic.
Combination Charts
Combination charts are only available on dashboards. On reports, combination charts display as bar or donut charts.
Combination charts which include both a stacked vertical bar chart and a line chart only show the stacked vertical bar chart. The line chart is not drawn.
Combination charts with two groupings and a line chart display differently in Lightning Experience than they do in Salesforce Classic. In Lightning Experience, the line chart depicts both groupings. In Salesforce Classic, the line chart only depicts 1 grouping (the first one).
To add a combination chart to a dashboard, or to edit one, switch to Salesforce Classic.
A vertical combination chart with two groupings, two measures, and a line chart displays as a bar chart in Lightning Experience. The bar chart displays two groupings, but the measure depicted by the line chart isn’t shown.
Funnel Charts
To add a funnel chart to a dashboard, or to edit one, switch to Salesforce Classic.
Gauge Charts
Breakpoints in gauge charts only accept whole numbers. For example, 
5
 is a valid breakpoint, but 
5.1
 isn’t.
On reports, gauge charts appear as a bar charts.
Scatter Charts
On reports, scatter charts display as bar charts.
To add a scatter chart to a dashboard, or to edit one, switch to Salesforce Classic.
Cumulative Line Chart
On reports, cumulative line charts appear as bar charts.
Combine Small Groups into “Others”
To combine small groups, switch to Salesforce Classic.
Chart Editor
In the chart editor, users can’t:
Limit the set of report data drawn on the chart
Change sorting direction
Change measurement units (for example, from meters to centimeters)
Add more than 2 aggregates
Use charts from source reports
Charts with More than 100 Dimensions
In Lightning Experience, if there are more than 100 dimensions, then only the first 100 dimensions are drawn on the chart. To avoid this limitation, set report filters that return 100 or fewer dimensions.
Chart Colors
The standard Lightning Experience color palette replaces custom colors that users set in Salesforce Classic.
Features Not Available in Lightning Experience
Feature
Notes about Lightning Experience Availability
Pie Charts
Pie charts from Salesforce Classic display as donut charts in Lightning Experience.
See Also:
Report Limits
Dashboard Limits
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=lex_gaps_limitations_analytics.htm&language=en_US
Release
202.14
