### Topic: Get more information about Salesforce1's support for notes, documents, files, and attachments, including limits.
Salesforce1 supports documents that are uploaded through the Files action OR through Chatter. Documents and any attachments uploaded through the Notes and Attachments section on a record list view aren't guaranteed to be accessible in Salesforce1.  The content can still appear under the section, but Salesforce can't guarantee they will be viewable/downloadable in Salesforce1. 
Resolution
Users will need to use the Files tab or action to upload documents into Salesforce in order for them to be available in Salesforce1. There are some basic requirements that you'll want to keep in mind while uploading content. 
Files formats are supported include 
 
.docx
, 
.pdf
, 
.ppt
, 
.pptx
, 
.xls
, and 
.xlsx
 
All image files are supported including 
.gif
, 
.jpg
, and 
.png
 
Android does NOT allows previews in .doc and .docx file types, iOS can allow previews in: .docx files.
100 MBs is the supported File upload size.  Please keep in mind network bandwidth plays a critical role in the size of uploads, as well as the speed with which they are upload.
Files larger than 5MB may not be able to be previewed inside the app and will need to be opened in another program.
For Android Users: When 
using the File action
 you're only able to attach existing Salesforce Files.  To add new files from your device, use the Post action and tap the paper clip icon.
For iOS Users: When 
using the File action
 you're only able to attach photos and videos.  To add new files from your device, use the "Copy to Salesforce1" sharing option, when holding down on a file / document.
