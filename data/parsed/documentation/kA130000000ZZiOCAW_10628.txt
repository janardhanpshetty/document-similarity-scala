### Topic: Enable Topics for Articles | Salesforce
Enable Topics for Articles | Salesforce
Enable Topics for Articles
With topics on articles, you can classify and search articles by assigning topics. Topics can be added from the article view and detail pages. Suggested topics, which are only supported in English, are terms extracted from the article, so that they are more concrete and precise than a data category assignment. When searching, topics can be used to index the article, so the matched articles are more relevant to keyword searches.
Available in: Salesforce Classic
Salesforce Knowledge is available in 
Performance
 and 
Developer
 Editions and in 
Unlimited
 Edition with the Service Cloud.
Salesforce Knowledge is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions.
User Permissions Needed
To enable topics:
“Customize Application”
Note
Contrary to data categories, topics added to an article are not transferred to the same article in another language.
Topics for articles are enabled for each article type.
From Setup, enter 
Topics for Objects
 in the 
Quick Find
 box, then select 
Topics for Objects
.
Click the article type name where you want to enable topics.
Check 
Enable topics
.
Select which fields you want to use for suggestions.
Click 
Save
.
Via profile or permission set, under System Permissions, define which agents can assign, create, delete, and edit topics.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=knowledge_topics.htm&language=en_US
Release
202.14
