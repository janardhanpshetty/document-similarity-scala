### Topic: Delete a Territory Model | Salesforce
Delete a Territory Model | Salesforce
Delete a Territory Model
Delete a territory model if your organization no longer actively uses the model for territory management or reference. You can’t delete an active territory: the model must be in 
Planning
 or 
Archived
 state.
Available in: Salesforce Classic
Available in: 
Developer
 and 
Performance
 Editions and in 
Enterprise
 and 
Unlimited
 Editions with the Sales Cloud
User Permissions Needed
To delete a territory model:
“Manage Territories”
Note
This information applies to Enterprise Territory Management only, not to previous versions of Territory Management.
Deleting a territory model also deletes all its associated territories and account assignments. You can’t cancel the process or restore a deleted model. The process may take several hours, so we’ll send you an email when it’s complete.
From Setup, enter 
Territory Models
 in the 
Quick Find
 box, then select 
Territory Models
.
Find the model in the list. Check the value in the 
State
 column to make sure the model is in the 
Planning
 or 
Archived
 state.
Click 
Del
.
Later, check your email for confirmation that the deletion process is complete.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tm2_delete_territory_model.htm&language=en_US
Release
202.14
