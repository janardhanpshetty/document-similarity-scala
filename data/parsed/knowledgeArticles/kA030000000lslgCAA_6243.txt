### Topic: A Browser Compatibility error is encountered when selecting a Topic or Notes within the Agent Console. The use of Topics and Notes is not supported in the Agent Console.
Users receive a "Browser Compatibility" error message when selecting Topics or Notes within the Agent Console. 
Resolution
The use of Topics and Notes is not supported within the Agent Console.
