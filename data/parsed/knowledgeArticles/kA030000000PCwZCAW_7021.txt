### Topic: How to customize the View All Campaign Members Link to use a Custom Report
How to customize the View All Campaign Members Link to use a Custom Report
Sometimes customers will want to change the Campaign Call Down report which is used by default in the View All Campaign Members custom link.
Resolution
You can achieve this by creating a custom link, follow these steps:
Build a Campaign with Campaign Members report.
Save the report
Copy the ID from the URL of the report
Example:
https://na15.salesforce.com/
00Oi000f00gEf5N 
<< Report ID
Create a new Button or Link and label it "View All Campaign Members".
(Non - Enhanced Setup) Your Name | Setup | App Setup | Customize | Campaigns | Buttons, Links and Actions
(Enhanced Setup) Setup | Customize | Campaigns | Buttons, Links and Actions
Code the Button or Link URL like this (where ReportID is the ID from Step 3):
/ReportId?scope=1&scopeid=Campaign.Name}
Click Save.
Edit the Campaigns page layout and add this link to it
