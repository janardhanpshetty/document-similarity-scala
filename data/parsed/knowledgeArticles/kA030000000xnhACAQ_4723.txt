### Topic: The option Folder Sharing is not available in the setup menu for a Developer Org
In a developer org, under:
Build - Customize - Reports & Dashboards
"Folder Sharing" is not shown.
Resolution
Please contact Salesforce support, and request for activation of "
Show Analytics Folder sharing"
