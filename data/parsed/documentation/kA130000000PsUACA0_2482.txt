### Topic: Examples of Workflow Rules | Salesforce
Examples of Workflow Rules | Salesforce
Examples of Workflow Rules
Looking for ideas on how workflow rules can help streamline your business? Check out these examples.
Available in: both Salesforce Classic and Lightning Experience
Flow triggers are not available in Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Business Processes
Follow Up Before Contract Expires
Follow Up when Platinum Contract Case Closes
Assign Credit Check for New Customer
Notify Account Owner About New, High-Priority Cases
Set a Default Entitlement for Each New Case
Update Shipment Status if Shipment is Delayed
Automatically Activate New Users
Cross-Object Processes
Notify Sales VP About Cases Filed for Top Accounts
Set Default Opportunity Name
Set Target Resolution Date for Cases
Update Application Record when Candidate Accepts Job
Deal Management
Track Closed Opportunities
Override Default Opportunity Close Date
Report Lost Opportunities
Report Unassigned Leads
Send Alert if Quote Line Item Discount Exceeds 40%
Notifications
Notify Key People About Account Owner Changes
Set Reminder for Contact Birthday
Set Reminder for High-Value Opportunity Close Date
Notify Account Owner of Updates by Others
Follow Up Before Contract Expires
Object
Contract
Description
Send an email reminder to the renewal manager 20 days before a contract’s end date.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Contract: Status equals Activated)
Immediate Actions
None
Time-Dependent Actions
20 Days Before Contract: End Date—
Email Alert:
 Send an email reminder to the renewal manager to confirm whether the client would like an extension.
Follow Up when Platinum Contract Case Closes
This example assumes there is a 
Contract Type
 custom picklist used to identify the contract level on cases, and that the picklist contains the value “Platinum.”
Object
Case
Description
Send a feedback request in email to the case contact seven days after a high-priority case is closed if the customer has a platinum contract agreement.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Case: Priority equals High) and 
(Case: Closed equals True) and 
(Case: Contract Type equals Platinum)
Immediate Actions
None
Time-Dependent Actions
7 Days After Case: Date/Time Closed—
Email Alert:
 Send a feedback request to the case contact.
Assign Credit Check for New Customer
This example assumes there is a 
New Customer
 custom field on opportunities.
Object
Opportunity
Description
Assign the Accounts Receivable (AR) department a task to check the credit of a potential customer 15 days before the opportunity close date if the amount is greater than $50,000.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Opportunity: Amount greater than 50000) and 
(Opportunity: Closed equals False) and 
(Opportunity: New Customer equals True)
Immediate Actions
None
Time-Dependent Actions
15 Days Before Opportunity: Close Date—
Task:
 Create a task for users in the Accounts Receivable role to run a credit check.
Notify Account Owner About New, High-Priority Cases
This example assumes there is a Service Level Agreement custom picklist called SLA that identifies the agreement level on accounts and contains the value “Platinum.”
Object
Case
Description
Notify the account owner when a high-priority case is created for accounts with a platinum service level agreement.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Case: Priority equals High) and 
(Account: SLA equals Platinum)
Immediate Actions
Email Alert:
 Email the details of the high-priority case to the account owner.
Time-Dependent Actions
None.
Set a Default Entitlement for Each New Case
This example assumes there is an active, 
autolaunched
 flow that looks up the relevant entitlement based on the account, asset, or contact associated with the new case and updates the case with the entitlement name.
The Process Builder has superseded flow trigger workflow actions, formerly available in a pilot program. Organizations that are using flow trigger workflow actions can continue to create and edit them, but flow trigger workflow actions aren’t available for new organizations.
Object
Case
Description
Set a default entitlement on each new case.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Case: Status not equal to Closed)
Immediate Actions
Flow Trigger:
 Look up and assign the relevant entitlement to the case. Pass the account, asset, or contact associated with the new case into the relevant flow variable to enable the entitlement lookup. Also pass the case ID into the relevant flow variable to enable the case update.
Time-Dependent Actions
None.
Update Shipment Status if Shipment is Delayed
Object
Shipment
Description
Update the Shipment Status field to “Delayed” if a shipment has exceeded the expected delivery date and not reached the customer.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Shipment: Status not equal to Delivered)
Immediate Actions
None.
Time-Dependent Actions
1 day after Shipment: Expected Delivery Date—
Field Update
: Change Shipment Status field to “Delayed” on Shipment record.
Automatically Activate New Users
Object
User
Description
Make sure that each new user is active, so that the user can log in to Salesforce.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(User: Active equals False)
Immediate Actions
Field Update
: Set 
Active
 to True.
Time-Dependent Actions
None.
Notify Sales VP About Cases Filed for Top Accounts
Use the following workflow rule if your sales VP wants to know about any cases filed for top accounts, where top accounts are determined by size and revenue.
Object
Case
Description
Notify Sales VP about any cases filed for top accounts.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
AND(Account.AnnualRevenue > 500000, Account.NumberOfEmployees > 5000)
Immediate Actions
Email Alert:
 Notify VP about cases for big accounts.
Time-Dependent Actions
None.
Set Default Opportunity Name
The opportunity naming convention for some companies is 
Account Name: Opportunity Name
. To automate the default name of each opportunity in your organization, create the following workflow rule.
Object
Opportunity
Description
Enforce opportunity naming convention.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and every time it’s edited
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
NOT(CONTAINS( Name, Account.Name ))
Immediate Actions
Field Update
: Set opportunity name to the following formula:
Account.Name & ": " & Name
Time-Dependent Actions
None.
Set Target Resolution Date for Cases
This example illustrates how to automatically set a case resolution date based on the value of a field on the associated account. It uses a custom picklist field on accounts called 
Support Level
, which has three values–Basic, Standard, and Premium–as well as a custom date field on cases called 
Target Resolution Date
.
Use the following three workflow rule examples to automatically set the target resolution date of a case based on the support level for the related account.
Set Resolution Date for Basic Support
Object
Case
Description
Sets the case target resolution date for accounts that have basic support level to 30 days from today.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following formula evaluates to true:
ISPICKVAL(Account.Support_Level__c , "Basic")
Immediate Actions
Field Update
: Set the 
Target Resolution Date
 to Today() + 30
Time-Dependent Actions
None.
Set Resolution Date for Standard Support
Object
Case
Description
Sets the case target resolution date for accounts that have standard support level to 14 days from today.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following formula evaluates to true:
ISPICKVAL(Account.Support_Level__c , "Standard")
Immediate Actions
Field Update
: Set the 
Target Resolution Date
 to Today() + 14
Time-Dependent Actions
None.
Set Resolution Date for Premium Support
Object
Case
Description
Sets the case target resolution date for accounts that have premium support level to 5 days from today.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following formula evaluates to true:
ISPICKVAL(Account.Support_Level__c , "Premium")
Immediate Actions
Field Update
: Set the 
Target Resolution Date
 to Today() + 5
Time-Dependent Actions
None.
Update Application Record when Candidate Accepts Job
Use the following workflow rule if you want to close the Application record when a Candidate accepts a job. Cross-object field updates to the master record are supported between custom objects in a master-detail relationship.
Object
Candidate
Description
Changes the Application Status to “Closed” for the custom Application object when the Candidate Status for the custom Candidate object changes to “Accepted.”
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Candidate: Status equals Accepted)
Immediate Actions
Field Update:
 Change Application Status field to “Closed” on parent Application record.
Time-Dependent Actions
None.
Track Closed Opportunities
This example assumes there is a “Closed Opportunities” record type that provides additional information to certain profiles.
Note
For information on record types, see 
Record Types
.
Object
Opportunity
Description
Change the record type of closed-won opportunities.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and every time it’s edited
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Opportunity: Closed equals True) and 
(Opportunity: Stage equals Closed Won)
Immediate Actions
Field Update:
 Set the record type to “Closed Opportunities.”
Time-Dependent Actions
None.
Override Default Opportunity Close Date
Object
Opportunity
Description
Override the default close date from the close of the quarter to six months after the opportunity is created.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Opportunity: Closed equals False)
Immediate Actions
Field Update:
 Use the following formula to set the opportunity close date to six months after the creation date:
DATE( YEAR(TODAY()) , (MONTH(TODAY()) + 6), DAY(TODAY()))
Time-Dependent Actions
None.
Report Lost Opportunities
Object
Opportunity
Description
Notify the VP of Sales when a deal is lost if the stage was “Proposal/Price Quote” and the amount was greater than $1,000,000.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and every time it’s edited
.
Rule Criteria (Filter)
Run this rule if the following formula evaluates to true:
AND( ISCHANGED(StageName), ISPICKVAL(PRIORVALUE(StageName) , "Proposal/Price Quote"), ISPICKVAL(StageName,"Closed Lost"), (Amount >1000000))
Immediate Actions
Email Alert:
 Notify the VP of Sales role that the deal was lost.
Time-Dependent Actions
None.
Report Unassigned Leads
This example assumes that all unassigned leads are placed in an unassigned leads queue by a leads assignment rule.
Object
Lead
Description
Ensure that unassigned leads are tracked in a timely manner by notifying the manager if a lead is not accepted in two days.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
Lead Owner equals Unassigned Lead Queue
Immediate Actions
None.
Time-Dependent Actions
2 Days After Lead: Last Modified Date—
Email Alert:
 Notify the manager role that there are unassigned leads in the queue that are older than two days.
Send Alert if Quote Line Item Discount Exceeds 40%
Object
Quote Line Item
Description
Ensure that an email alert is sent if a sales rep applies a quote line item discount that exceeds 40%.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
Quote Line Item: Discount is greater than 40
Immediate Actions
Email Alert:
 Notify the manager role that the quote line item discount exceeds 40%.
Time-Dependent Actions
None.
Notify Key People About Account Owner Changes
Object
Account
Description
Notify key people in the sales department when the owner of an account changes if the account's annual revenue is greater than $1,000,000.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and every time it’s edited
.
Rule Criteria (Filter)
Run this rule if the following formula evaluates to true:
AND( ISCHANGED(OwnerId), AnnualRevenue > 1000000 )
Immediate Actions
Email Alert:
 Notify the person in the sales operations role of the change in account ownership.
Time-Dependent Actions
None.
Set Reminder for Contact Birthday
This example assumes there is a 
Next Birthday
 custom formula field that uses the following formula to calculate the date of the contact's next birthday on contact records:
IF(MONTH(Birthdate) > MONTH(TODAY()),DATE(YEAR(TODAY()),MONTH(Birthdate),DAY(Birthdate)),
IF(MONTH(Birthdate) < MONTH(TODAY()),DATE(YEAR(TODAY())+1,MONTH(Birthdate),DAY(Birthdate)),
IF(DAY(Birthdate) >= (DAY(TODAY())),DATE(YEAR(TODAY()),MONTH(Birthdate),DAY(Birthdate)),
DATE(YEAR(TODAY())+1,MONTH(Birthdate),DAY(Birthdate)))))
Object
Contact
Description
Send an email to the contact two days before the contact's birthday.
Evaluation Criteria
Evaluate the rule when a record is: 
created
.
Rule Criteria (Filter)
Run this rule if the following formula evaluates to true:
(Contact: Birthdate not equal to null) and 
(Contact: Email not equal to null)
Immediate Actions
None.
Time-Dependent Actions
2 Days Before Contact: Next Birthday—
Email Alert:
 Send a birthday greeting to the contact's email address.
Set Reminder for High-Value Opportunity Close Date
Object
Opportunity
Description
Remind the opportunity owner and senior management when the close date is approaching for a large deal, an opportunity that has an amount greater than $100,000. Also, create a follow-up task for the opportunity owner if the deal is still open when the close date passes.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and any time it’s edited to subsequently meet criteria
.
Rule Criteria (Filter)
Run this rule if the following criteria are met:
(Opportunity: Amount greater than 100000) and 
(Opportunity: Closed equals False)
Immediate Actions
None.
Time-Dependent Actions
30 Days Before Opportunity: Close Date—
Email Alert:
 Notify the opportunity owner that 30 days remain.
15 Days Before Opportunity: Close Date—
Email Alert:
 Notify the opportunity owner that 15 days remain.
5 Days After Opportunity: Close Date—
Task:
 Create a follow-up task for the opportunity owner to update the deal. 
Email Alert:
 Notify senior management to involve executives.
Notify Account Owner of Updates by Others
Object
Account
Description
Notify the account owner when someone else updates the account if the account's annual revenue is greater than $1,000,000.
Evaluation Criteria
Evaluate the rule when a record is: 
created, and every time it’s edited
.
Rule Criteria (Filter)
Run this rule if the following formula evaluates to true:
AND( (LastModifiedById <> CreatedById), (AnnualRevenue > 1000000) )
Immediate Actions
Email Alert:
 Notify the account owner that someone else has updated the account.
Time-Dependent Actions
None.
See Also:
Workflow
Set the Criteria for Your Workflow Rule
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workflow_examples.htm&language=en_US
Release
202.14
