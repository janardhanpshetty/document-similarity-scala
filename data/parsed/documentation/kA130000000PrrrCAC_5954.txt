### Topic: Forecasts Quotas Overview | Salesforce
Forecasts Quotas Overview | Salesforce
Forecasts Quotas Overview
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Available in: 
Professional
 Edition with the “API Enabled” permission
Note
This information applies to Collaborative Forecasts and not to 
Customizable Forecasts
.
A forecast quota is the sales goal assigned to a user on a monthly or quarterly basis. A manager’s quota equals the amount the manager and team are expected to generate together. The quota rollup is done manually by users and managers, and either revenue or quantity data can be used. If your organization has more than one type of forecast enabled, each forecast type maintains its own separate quota information.
Note
In Product Family forecasts, sales managers can’t see their own quotas and quota attainment for individual product families. Only their total quota appears in the rollup table.
For example, three sales representatives might have combined quota amounts of $75,000 and their forecasts manager might have an individual quota of $30,000. The manager’s quota in this case is $105,000. You can only edit direct subordinates’ quotas, and not your own; additionally, you must have the “Manage Quotas” permission.
If forecast quotas are enabled for your organization, quota data displays in two locations on the Forecasts tab.
A column that contains quota amounts for a specific period. If your forecast includes product families, this column includes quotas for each product family.
A row that contains the percentage amounts attained for a specific period in a specific forecast rollup.
If you’re unsure about how to enter quota data or make updates to quotas, speak to your administrator.
Tip
Hover and click on a column boundary and adjust the width to view data more easily.
See Also:
Collaborative Forecasts
Forecasts Tab Overview
Showing or Hiding Quota Information
Enable Quotas in Forecasts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=forecasts3_quotas_overview.htm&language=en_US
Release
202.14
