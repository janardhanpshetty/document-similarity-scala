### Topic: This article informs on how to resolve the error message Remote Server returned '< #5.7.12 smtp;550 5.7.12 RESOLVER.RST.SenderNotAuthenticated; Delivery restriction check failed as sender not authenticated>'.
Sometimes when an email is sent from Salesforce to an Office 365 Distribution group, the user that sent or triggered the email gets a bounced email with the following error code:
Remote Server returned '< #5.7.12 smtp;550 5.7.12 RESOLVER.RST.SenderNotAuthenticated; Delivery restriction check failed as sender not authenticated>'
This happens when the 
Distribution Group is not allowed to receive emails from outside the organization. 
Resolution
This is not a Salesforce issue. Users should change the email server settings to allow the Distribution Group to receive emails from outside the organization. Contact email admin or internal IT for help in changing the settings.
Related article from Microsoft: 
Distribution groups
