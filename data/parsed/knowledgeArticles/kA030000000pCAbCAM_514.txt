### Topic: FAQ about responsive lists in Winter 16 for Salesforce Lightning Console.
In Winter 16 the console introduced responsive lists as part of the Lightning Console redesign. Please be aware of the following things:
Note: 
Pixel sizes given in this article are not definite and are subject to change. They should be seen as rough guides and not precise values to be shared.
Resolution
How to enable/disable responsive lists
In order to reach the setup page go to Setup->Create->Apps and edit the console you wish to change. This option is per-console. It looks like this: 
 
Supported Browsers
Responsive lists are supported in Chrome, Firefox, and Internet Explorer 11. Users with older versions of Internet Explorer will experience the non-responsive pinned list even if this option is enabled. 
Minimum Width and Automatically Hiding
The pinned list has a minimum width. If it is resized by a user to a width smaller than 250px (this value may change in future releases and is not necessarily a permanent value) it will collapse and be hidden. It can be expanded by the user in order to be shown again. 
Column Wrapping/Hiding and Horizontal Scrollbars
The pinned list is responsive, meaning it will show columns based on the width available. There are three general categories for the list:
Too narrow 
 - The list will be collapsed automatically and hidden.
Normal 
- The columns will wrap and will show as many as can fit in the available width. It's roughly 1 column per 100px of width. No horizontal scrollbar will show and any columns which are not able to be shown are not available. Customers should place the most important columns first in their list views in order to be sure they're shown when the list is shrunk. 
Wide 
- After about 800px wide a user will see the entire list with all columns. At this point the horizontal scrollbar will appear. 
Usability Questions and Keyboard Shortcuts
Because not every column is shown under normal circumstances, Salesforce has also released a pinned list zoom keyboard shortcut to help users. By pressing 'N' to focus on the pinned list and then 'Z' to zoom, they will be taken to a full screen view of their list. Pressing 'Z' again will go back to the previous unzoomed view. This helps agents get a quick look at the entire list if they require more information than is shown in the responsive list in a narrower state. 
Also see release notes: 
http://docs.releasenotes.salesforce.com/en-us/winter16/release-notes/rn_console_responsive_list.htm
