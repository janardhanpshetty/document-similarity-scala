### Topic: Merge Fields for Formulas | Salesforce
Merge Fields for Formulas | Salesforce
Merge Fields for Formulas
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
A merge field is a field you can put in an email template, mail merge template, custom link, or formula to incorporate values from a record.
Syntax and Formatting
Merge fields for formulas aren’t enclosed in curly braces or preceded by an exclamation point, nor are they preceded by the type of record. For example: 
AccountNumber
. To ensure you’re using the correct syntax, use the 
Insert Field
 button or the drop-down list in the formula editor.
See Also:
Tips for Using Merge Fields in Formulas
Build a Formula Field
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_merge_fields_formulas.htm&language=en_US
Release
202.14
