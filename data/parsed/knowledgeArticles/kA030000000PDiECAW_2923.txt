### Topic: This article will highlight why you must use .csv. and how to save an Excel file into .csv format. Make sure you follow these directions.
I am trying to upload an Excel file into Salesforce but I am receiving an error message.
 
Resolution
The reason for this is Salesforce only supports the CSV format.  Such as Dataloader, an Import Wizard or an Appexchange App use CSV formatted spreadsheets.
To save a file as a .csv make sure you follow these directions:
In Microsoft Excel:
1. Click on File and Choose “Save As”
2. Change the “Save as Type” to CSV (Comma delimited)
3. Click “Save”
4. Click “OK” when the message “The selected file type does not support workbooks that contain multiple sheets” is displayed.
5. Click “Yes” when the message “may contain features that are not compatible with CSV (Comma delimited). Do you want to keep the workbook in this format?” is displayed.
