### Topic: Automatically Get Geocodes for Addresses | Salesforce
Automatically Get Geocodes for Addresses | Salesforce
Automatically Get Geocodes for Addresses
Help your sales and marketing teams find nearby prospects, assign territories and campaigns, and more! All this is a cinch with geocodes. In just a few clicks, you can automatically get geocodes for addresses on accounts, contacts, and leads.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Important
Geocodes are added to records using Data.com technology. However, a Data.com license is 
not
 required to use this feature.
Considerations for Setting Up Geocode Clean Rules
Review these considerations before adding geocode information to Salesforce accounts, contacts, and leads using Data.com geocode clean rules.
Set Up Geocode Clean Rules
In a few steps, you can set up and activate your Data.com geocode clean rules. Then, geocodes are automatically added to existing accounts, contacts, and leads. New accounts, contacts, and leads get this information when they’re saved.
Guidelines for Setting Up Geocode Clean Rules
Follow some important guidelines when automatically adding geocode information to Salesforce records with Data.com.
Check the Clean Status of Geocode Clean Rules
The Data.com geocode clean rules automatically add geocodes for addresses on your accounts, contacts, and leads. You can check whether an individual record was processed with a geocode clean rule and the status of that processing. If you’d like to test the clean rule, you can manually clean the record using the clean rule. Remember: With clean rules, your records are automatically cleaned, so manually cleaning a record isn’t necessary.
Verify Geocodes Were Added to Records
Since geocode fields are not typically visible on a record, you need to complete a few steps to verify that geocodes were added to Salesforce records with a Data.com geocode clean rule.
Geocode Fields
Geocode information identifies a location using a latitude, a longitude, and an accuracy rating. Geocode fields are available for standard addresses on accounts, contacts, and leads in Salesforce. Geocode fields are not visible on records, but they can be viewed using the Salesforce API.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_admin_automatically_get_geocodes_for_addresses.htm&language=en_US
Release
202.14
