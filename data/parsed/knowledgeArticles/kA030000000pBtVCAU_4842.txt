### Topic: This document describes how to add a button to easily refresh all metrics for a goal.
Overview
 
With Work.com Goals, we’ve made it easier to keep a metric up to date by linking it to a Salesforce report. However, out of the box, the metric must be refreshed manually. 
 
It’s possible to customize and extend the metric refresh behavior. This document describes how to add a button to easily refresh all metrics for a goal. Additionally, this document covers how to refresh 
all 
metrics in the organization, which lays the framework for admins looking to extend the example so that metrics can be refreshed automatically on a scheduled basis.
 
Note: 
This document is provided as example code and requires development resources or Salesforce development expertise to implement
the API endpoints specified in this document have not been made publicly available and are subject to change.
there’s a limitation of a maximum of 50 callouts per apex invocation (and hence metric refreshes) for scheduled jobs. See 
here
 for more information.
Resolution
 
Setup Refresh All Metrics on a Goal
Step 1 - Set up the Refresh Metrics code
 
This class will update all metrics linked to Salesforce reports and update them.
 
Navigate to Setup->Develop->Apex Classes
Click New
Copy and paste the code below 
Replace the 
INSTANCE 
variable with your salesforce org. You can find your salesforce org in the browser address bar after logging into Salesforce. e.g. 
https://na15.salesforce.com
Click Save
 
public class RefreshMetrics implements Schedulable {
   private static final String INSTANCE = 'https://na15.salesforce.com'; // NOTE: replace with your salesforce instance
 
   public void execute(SchedulableContext ctx) {
       refreshMetrics(null);
   }
   
 
   // Refreshes metrics which aren't overdue for the specified goalId. If the goalId is null, all non-overdue metrics are refreshed
   public static void refreshMetrics(Id goalId) {    
       System.debug('GoalId: ' + goalId);
   
       // Date today = DateTime.now().dateGMT(); // optionally can use GMT date  
       Date today = Date.today();
       String sessionId = Userinfo.getSessionId();
       Id userId = Userinfo.getUserId();
       System.debug('Session id: ' + sessionId);
       System.debug('User id: ' + userId);       
       System.debug('today: ' + today);
       
       // Get metric data links
       Set<Id> keys = new Set<Id>();
       for (MetricDataLink link : [select targetId from MetricDataLink]) {
           keys.add(link.targetId);
       }
       System.debug('Keys:' + keys);
       
       // Get metrics to refresh
       List<Metric> metrics = null;
       if (goalId == null) {
           metrics = [select id from Metric where (dueDate >= :today or dueDate = null) and id in :keys];
       } else {
           metrics = [select id from Metric where goalId = :goalId and (dueDate >= :today or dueDate = null) and id in :keys];        
       }
       
       // refresh metrics
       for (Metric metric : metrics) {
           refreshMetric(metric.id, sessionId, userId);
       }
   }  
  
  // @future (callout=true) // NOTE asynchronous http callout required in scheduled jobs
   public static void refreshMetric(Id metricId, String sessionId, Id userId) {
       System.debug('Processing metric: ' + metricId);
       System.debug('Session id: ' + sessionId);
       System.debug('User id: ' + userId);       
        
       HttpRequest req = new HttpRequest();
       String url = INSTANCE + '/services/data/v33.0/actions/standard/metricRefresh';
       req.setEndpoint(url);
       req.setMethod('POST');
       req.setHeader('Authorization', 'OAuth ' + sessionId);
       req.setHeader('Force-Instance-Url', INSTANCE); 
       req.setHeader('Force-User-Id', userId);
       req.setHeader('Accept', 'application/json');
       req.setHeader('Content-Type', 'application/json');
       req.setBody('{"inputs":[{"metricId":"' + metricId + '"}]}'); // metric id
  
       Http http = new Http();
       HTTPResponse res = http.send(req);
       System.debug(res.getBody());
   }
}
 
Step 2 - Set up the Remote Site
 
The Refresh Metrics class above requires API access which requires a Remote Site entry.
 
Navigate to Setup->Security Controls->Remote Site Settings
Click New Remote Site
Specify the Remote Site Name, e.g. na15
Specify the Remote Site URL. You can find your salesforce org in the browser address bar after logging into Salesforce. e.g. https://na15.salesforce.com
 
Step 3 - Set up the Refresh Metrics controller
 
This class will call the code above which in turn refreshes the metrics.
 
Navigate to Setup->Develop->Apex Classes
Click New
Copy and paste the following and click Save:
 
public with sharing class RefreshMetricsOnGoalController {
   public RefreshMetricsOnGoalController(ApexPages.StandardController standardController) {
       RefreshMetrics.refreshMetrics(standardController.getId());
   }
}
 
Step 4 - Set up the Refresh Metrics page
 
This is the page which will call the code above which in turn refreshes the metrics.
 
Navigate to Setup->Develop Pages
Click New
Give a name for the Refresh Metrics page
Copy and paste the following and click save:
<apex:page standardController="Goal" extensions="RefreshMetricsOnGoalController">
<div style="text-align:center;padding-top:30%;font-size:200%">Metrics Refreshed!</div>
<div style="text-align:center;padding-top:20px"><a href="/{!Goal.Id}">Back to the Goal</a></div>
</apex:page>
 
Step 5 - Set up the Refresh Metrics button
 
This is the page which will call the code above which in turn refreshes the metrics.
 
Navigate to Setup->Customize->Work.com->Goals->Buttons, Links, and Actions
Click New Button or Link
Specify the following:
 
 
Step 6 - Add the Refresh Metrics button to the page layout
 
Navigate to Setup->Customize->Work.com->Goals->Page Layouts
Click Edit on the Layout
Click on Buttons and add the Refresh Metrics button under Custom Buttons and click save:
 
Congratulations! The Refresh Metrics button is now configured on the Goals Details Page. Clicking the button will refresh all related metrics for the Goal which are linked to reports and not yet overdue.
 
Setup Refresh All Metrics for the Organization
Step 1 - Set up the Refresh All Metrics code
 
See step 1 above
 
Step 2 - Set up the Remote Site
 
See step 2 above
 
Step 3 - Set up the Refresh All Metrics controller
 
This class will call the code above which in turn refreshes the metrics.
 
Navigate to Setup->Develop->Apex Classes
Click New
Copy and paste the following and click Save:
 
public class RefreshMetricsLaunchController {
 
   public void launchEndToEnd() {
       RefreshMetrics ne = new RefreshMetrics();
       ne.execute((SchedulableContext) null);
       Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'All set!'));               
   }
}
 
Step 4 - Set up the Refresh All Metrics page
 
This is the page which will call the code above which in turn refreshes the metrics.
 
Navigate to Setup->Develop Pages
Click New
Give a name for the Refresh All Metrics page
Copy and paste the following and click save:
<apex:page controller="RefreshMetricsLaunchController">
<apex:form >
   <apex:pageMessages id="pageMessages"></apex:pageMessages>
   <div style="margin: 20px">
   <apex:commandButton value="REFRESH METRICS" action="{!LaunchEndToEnd}"></apex:commandButton>
   </div>
</apex:form>
</apex:page>
 
Step 5 - Set up the Refresh All Metrics tab
 
Add a tab so that it’s easy to get to the Refresh All Metrics page
 
Navigate to Setup->Create->Tabs
Click New beside Visualforce Tabs
Select the Refresh All Metrics page above
Give a name for the Label
Choose a Tab Style
Click save
 
Congratulations! The Refresh All Metrics tab is now configured on the Goals Details Page. Clicking the button will refresh all metrics for the entire organization which are linked to reports and not yet overdue. Note that this sample code will only refresh up to 50 metrics.
 
Frequently Asked Questions
 
Q: Is there a maximum number of metrics I can refresh?
 
A: Yes there’s a limitation of a maximum of 50 callouts per apex invocation (and hence metric refreshes) for scheduled jobs. 
See 
here
 for more information. There’s also a limit to metric refreshes (
both manual and automated) and also to any calls you make to the Analytics API. See additional limits 
here
. 
 
Q: How do I set up the auto refresh code to refresh on a recurring basis?
 
A: The example code above provides a good starting baseline. Here’s a few things you need to do as well:
 
The refresh code in Step 1 already includes a scheduleable interface, so it’s possible to schedule the job by clicking on the “Schedule Apex” button (More information 
here
). 
You’ll also need to uncomment 
@future (callout=true) 
to make callouts from scheduled jobs. However, only a maximum of 50 future callouts can be made by default, so the query for metrics needs to be set to a maximum of 50 as well and the business logic adjusted accordingly so that the next time it runs, it’ll refresh the remaining metrics.
The refresh code in Step 1 also relies on Userinfo.getSessionId() for authentication which works when the code is invoked manually, but won’t work from a Scheduled context. This needs to be updated with a different authentication mechanism (more information 
here
).
 
Q: My metrics are linked to reports with dynamic filters like “My Opportunities” or “My Team’s Opportunities”. Can I still use the sample code in this document?
 
A:  Yes, the sample code runs in the context of the 
metric owner
. This means that the results of the report will be for the 
metric owner
 and not the current user, just like when you click on the refresh link in the metric.
