### Topic: Some considerations about the Easy Account Access for External Users functionality in Spring '16.
External users can now access their account right from the community. The My Account link in the User Profile Header menu takes the user to their account record, so they can view and update their information. 
You can read more about this feature in the Release Notes: 
Easy Account Access for External Users
.
Here are some other things to keep in mind to use this functionality:
Resolution
Q: 
Does My Account work for Internal Users, too?
A: 
This functionality works only for external users. If you want to test it out, make sure you're logged into the Community as an external user and not with your normal salesforce log in.
Q: 
Can we configure what it's displayed on that page?
A: 
The information displayed in My Account is driven by the account detail page so if you need to change the information displayed there you'd have to control it using different page layouts.
Safe Harbor
: In the future we will look into the possibility of having a specific view for My Account to allow having different configurations for account detail and My Account pages.
Q:
 Does My Account work for Person Account records?
A:
 Yes, it works. Read More about 
Person Account Behaviors
 and 
What is a Person Account
.
Q: 
Do Delegated Administrators have the ability to add new Community users on the My Account page?
A: 
Not at the moment but, 
Safe Harbor
, it's something we are also considering for future releases.
