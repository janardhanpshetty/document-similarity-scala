### Topic: In customizable forecasting there Opportunities with a forecast date matching the close date.
As per 
Standard Functionality
 the 
Forecast Date
 should match the Fiscal period that meets the 
Close Date
 range.
Resolution
In an instance that an 
Opportunity 
has the
 Forecast
 date same as the close date you need to do the following steps:
1. Go to 
Forecasting
 tab | Opportunities sub tab
2. Look for the Opportunity that has the same
 Forecast Date
 and 
Close Date
 and click on 
Edit
3. On the next page just click on save without changing anything
4. Go back to the 
Customizable Forecasting Reports
: Opportunity Forecasts report and run the report.
You will now see that the 
Forecast Date 
now matches the fiscal period that meets the 
Close Date
 range.
See Also:
Fiscal Years
Set the Fiscal Year
