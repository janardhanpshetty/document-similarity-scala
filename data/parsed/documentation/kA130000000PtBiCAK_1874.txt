### Topic: Lead Reports | Salesforce
Lead Reports | Salesforce
Lead Reports
Use lead reports to show information about the source and status of leads, how long it takes to respond to leads, neglected leads, and the history of lead fields.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Special Features of Lead Reports
Consider the following when running lead reports:
Standard Reports
Choose the Lead History report type to track the history of standard and custom fields on leads where field histories are set to tracked. Use this report to see tracked fields' old and new values.
Tip
If you have the “Create and Customize Reports” permission, you can use the 
View
 drop-down on a Lead History Report to view lead history data by My Leads, My Team's Leads, User Owned Leads, Queue Owned Leads, and All Leads.
Tips for Lead Reports
Limit your report view to “My team’s leads” to see leads owned by users who report to you in the role hierarchy.
Lead reports can show all leads, both converted and unconverted. To limit your report to just unconverted leads, enter filter criteria of “Converted equals 0.”
The 
Last Activity
 of a lead is the most recent due date of an activity on the record. The following past or future activities set this date:
Any event
Closed tasks
You can create a report of your lead information, export that data to Excel, and then do a mass mail merge using Microsoft® Word.
Lead Report Limitations
You can't use filter conditions to search the results of the 
Old Value
 and 
New Value
 fields.
You can’t use filter logic if you are filtering by 
Field/Event
.
Parent topic:
 
Standard Report Types
Previous topic:
 
Forecast Reports
Next topic:
 
Opportunity Reports
See Also:
The Report Run Page
Limit Report Results
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_lead.htm&language=en_US
Release
202.14
