### Topic: You can enable users to register for or log into an associated portal seamlessly from your site. See the below steps on how this can be accomplished.
How we can implement Single Sign-On for Customer Portal Users from one org to another org.
Resolution
You can enable users to register for or log into an associated portal seamlessly from your site. 
Enable the portal for login using the following steps: 
Click Your Name | Setup | Customize | Customer Portal | Settings, or click Your Name | Setup | Customize | Partners | Settings. 
If you have not enabled your portal, select Enable Customer Portal or Enable Partner Relationship Management and click Save. 
Click Edit for the portal you want to modify. 
Configure the portal as follows: 
Select the Login Enabled checkbox. 
Select a user for the Administrator field. 
Optionally, set the Logout URL. If this is not set, users are taken to the site home page on logout. 
Click Save. 
If you are using a Customer Portal and want to allow self-registration, follow these steps: 
Click Your Name | Setup | Customize | Customer Portal | Settings. 
Click Edit for the portal you want to associate with your Force.com site. 
Configure the Customer Portal as follows: 
Select Self-Registration Enabled. 
Select Customer Portal User for both the Default New User License and Default New User Profile fields. 
Depending on your portal license, you may want to select a different profile for the Default New User Profile field. 
Select User for the Default New User Role field. 
Click Save. 
Associate the site pages with the default portal users: 
Click Your Name | Setup | Customize | Customer Portal | Settings, or click Your Name | Setup | Customize | Partners | Settings. 
Click the name of the portal that you want to associate with your site. 
Click the name of each profile associated with your portal users and do the following: 
Scroll down to the Enabled Visualforce Page Access section and click Edit. 
Add the appropriate public site pages to the Enabled Visualforce Pages list. This allows portal users with that profile to view these pages. 
Click Save.
 
Also, For setting up SSO for Customer Portal User you can refer the following document: 
Enabling Single Sign-On for Portals
