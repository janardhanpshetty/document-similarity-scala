### Topic: Create a Custom Picklist Field | Salesforce
Create a Custom Picklist Field | Salesforce
Create a Custom Picklist Field
Create custom picklist fields to enable your users to select values from lists that you define.
Available in: Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To create or change custom fields:
“Customize Application”
Watch a Demo: 
Custom Fields: Picklists
You can create two types of picklist fields:
Picklist—Allows users to select a single value from a list that you define.
Multi-select picklist—Allows users to select more than one picklist value from a list that you define. These fields display each value separated by a semicolon.
Note
Picklist fields aren’t available for external objects. You can’t add a multi-select picklist, rich text area, or dependent picklist custom field to opportunity splits.
From the object management settings for your object, go to the fields section.
For Knowledge validation status picklists, from Setup, enter 
Validation Statuses
 in the 
Quick Find
 box, then select 
Validation Statuses
.
In the custom fields related list, click 
New
.
Select 
Picklist
 or 
Picklist (Multi-Select)
, and then click 
Next
.
Enter a label for the picklist field.
To use the values from an existing global picklist, select 
Use global picklist definition
. To use values that you create specifically for this picklist, select 
Enter values for the picklist, with each value separated by a new line.
Important
This release contains a beta version of global picklists that’s production-quality but has known limitations. You can provide feedback and suggestions for global picklists in the 
Global, Restricted Custom Picklists group in the Salesforce Success Community
.
If you didn’t use a global picklist, enter picklist values.
Put each value on a separate line. Values can be up to 255 characters long.
Optionally choose to sort the values alphabetically or to use the first value in the list as the default value, or both.
If you select both checkboxes, Salesforce alphabetizes the entries and then sets the first alphabetized value as the default.
Note
Don’t assign default values to fields that are both required and unique, because uniqueness errors can result. See 
About Default Field Values
.
Choose whether to restrict this picklist’s values to an admin-approved list. Selecting 
Strictly enforce picklist values
 prevents users from loading unapproved values through the API.
If you’re creating a multi-select picklist, enter how many values you want displayed at a time on edit pages. The number of values determines the box height.
Enter description or help text if desired, and then click 
Next
.
Set field-level security for the picklist field, and then click 
Next
.
Choose the page layouts on which to include the picklist field.
Click 
Save
.
See Also:
Create Custom Fields
Add or Edit Picklist Values
Create a Global Picklist (Beta)
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_creating_picklists.htm&language=en_US
Release
202.14
