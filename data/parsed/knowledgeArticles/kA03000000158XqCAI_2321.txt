### Topic: This article lists most common scenarios where associating an email to a Salesforce record may fail with or without an error message
List of all the knowledge articles mentioned below
Article Number 000199147
Article Subject:  Unable to Add Email using Salesforce for Outlook Side Panel - Error in getting contacts
Article Number 000212913 
Article Subject:  Side Panel error when trying to add an email - We couldn't add this item. Please try adding this item
Article Number 000186553
Article Subject:  
Add Email envelope icon is missing from the Salesforce for Outlook in the Side Panel
Article Number 000198753
Article Subject:  Side Panel displays error "email wasn't added because... DataTruncated" when adding emails
Article Number
 
000229910
Article Subject:  Salesforce for Outlook: Emails are added but adding attachment fails - “We couldn't add this item. Please try adding this item again"
Article Number  000212913
Article Subject: Salesforce for Outlook - Side Panel error when trying to add an email - We couldn't add this item. Please try adding this item again
 
 
Before you start, we recommend reviewing the following articles for more information on how email association works
 
Article Number     000211889 - Add Email using the Salesforce for Outlook Side Panel
 
Article Number     000211788 - Add Email to Multiple Salesforce record using Salesforce For Outlook Side Panel
Common Troubleshooting techniques when Email association fails using the 
Add Email button
 (not the Side Panel)
 
1- Take Outlook offline
2- Select email and click Add Email button. Check the Outbox.
3- You must see one email in Outbox.  The original selected email is now added as attachment to this new email with Email To Salesforce address in To field.
If this information is correct, make Outlook online again and see if you have connectivity to internet and you can send email.
If the email can be sent successfully then the plugin is working properly. Please contact Salesforce support for further investigation. Please also contact your I.T department as the issue may be related to your company firewall. (When contacting support please provide the time and date when you attempted to add the email, also please attach the log files for Salesforce for Outlook.
 
Resolution
Scenario #1
Users are unable to add an email from the Side Panel by clicking on the add email envelope icon. Side Panel displays an error message in red and would try to add the email but then it stops and the envelope never turns green. 
 
Root Cause
The 
Name
, 
Comment 
and
 Due Date
 fields of the 
Task 
Object are not visible for the affected user's profile and the FLS (Field Level Security) is set to Hidden
 
Resolution
To address this issue refer to 
Article Number 000199147
Article Subject:  Unable to Add Email using Salesforce for Outlook Side Panel - Error in getting contacts
 
*******************************
 
Scenario #2
When you select an email in Outlook, in Inbox or any other folder as well as composing a new email, or even replying to an email and trying to associate that email to a record in Salesforce using the Side Panel, the Side Panel throws an error message on top after you click on the Envelope icon next to the record name
 
 
Root Cause
It is not easy to say but possible causes could be any one or more of the following:
 
- A validation Rule on the Task object
- A workflow Rule
- An Apex trigger
- Email body/content is too large (Description field
) 
Resolution
To address this issue refer to 
Article Number 000212913 
Article Subject:  Side Panel error when trying to add an email - We couldn't add this item. Please try adding this item
 
 
*******************************
Scenario #3
The small Add email envelope icon 
is not visible in the Side Panel
Root Cause
The Add Email option is not enabled for your organization
 
Resolution
To address this issue refer to
 
Article Number 000186553
Article Subject
:  
Add Email envelope icon is missing from the Salesforce for Outlook in the Side Panel
 
*******************************
Scenario #4
When trying to add emails to records in Salesforce, the Side Panel sometimes displays a red error message with a wording like "email wasn't added because... DataTruncated" with some additional text from the email.
 
Root Cause
Possible Workflow rules that would update Task Fields
 
Resolution
To address this issue refer to 
Article Number 00
0198753
Article Subject:  Side Panel displays error "email wasn't added because... DataTruncated" when adding emails
 
 
*******************************
Scenario #5
Clicking on the envelope icon 
in the side panel adds the email to a Salesforce record but when you select an attachment, it fails with the following error 
SFO log file would contain
2015-08-07 13:21:18,675 [VSTA_Main ] WARN adxsidepanel ::JAVASCRIPT::ERROR::AssociationError::xhr=[object Object]:status=error:error=Unauthorized:xhr.message=
Session expired or invalid:xhr.code=INVALID_SESSION_ID:xhr.fields=undefined
2015-08-07 13:21:18,679 [VSTA_Main ] WARN adxsidepanel ::JAVASCRIPT::We couldn't add this item. Please try adding this item again.
2015-08-07 13:21:18,805 [VSTA_Main ] WARN adxsidepanel ::JAVASCRIPT::ERROR::We couldn't add this item. Please try adding this item again.
Root Cause
In reality this issue is not related to SFO or its settings. You would need to disable the "
Lock sessions to the IP address from which they originated
" in the impacted orgs
.
 
 
Resolution
To address this issue refer to 
Article Number
 
000229910
Article Subject:  Salesforce for Outlook: Emails are added but adding attachment fails - “We couldn't add this item. Please try adding this item again"
 
*******************************
Scenario #6
Salesforce for Outlook - Side Panel error when trying to add an email - We couldn't add this item. Please try adding this item again.
 
Root Cause
The error message could be due to a number of reasons, but a potential cause could be:
 
- A validation Rule on the Task object
- A workflow Rule
- An Apex trigger
- Email body/content is too large (Description field)
 
Resolution
To 
address this issue refer to 
Article Number  
000212913
Article Subject: 
 S
alesforce for Outlook - Side Panel error when trying to add an email - We couldn't add this item. Please try adding this item again
 
 
