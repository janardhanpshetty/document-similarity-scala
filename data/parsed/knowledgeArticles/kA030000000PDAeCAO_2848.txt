### Topic: This article discusses how, when and why Chatter Desktop updates occur.
Occurrence of Chatter Desktop
Resolution
Chatter Desktop updates occur roughly on a monthly basis and occur automatically.  The installed client will periodically check Salesforce for updates.  Users are then advised via a pop-up within the Desktop app that an update is available. 
The only exception to this is for customers who have deployed the Managed Version of Chatter Desktop.  The Managed Version does not regularly check Salesforce for updates.
For more information about Chatter Desktop, see "Chatter Desktop Overview" in our Help portal.
Note that our safe harbor statement applies for any forward-looking statements and any dates provided are subject to change (http://www.salesforce.com/company/investor/safe_harbor.jsp). 
