### Topic: Salesforce1 will now show up to 200 list Views on an object.
How do I control list views within Salesforce1?
Resolution
In Salesforce1 you can display up to 200 list views that are available for the object. (Previously it was only the 4 most recently accessed online by the user)  
With the downloadable iOS and Android applications, the user must access to list view within the full site desktop interface at least one time before it will show up.
You can also Sort and Filter these Views using the Sort or Filter Icons. 
Note: You will only see the first 5 fields of any particular list view when viewing in the app.
If a List View filters on a Owner field and they try to filter by a Queue, no records will be returned. The workaround is to create a formula field to return the text value and filter on the custom formula field. As an example, a User using Case Owner Alias to filter by Case Queues will yield results in the full desktop site; However, no results in Salesforce1. Creating the following formula field below and replace Case Owner Alias with the custom formula field with a custom field  titled Case Owner.
Example formula:
IF(Owner:Queue.QueueName != "", Owner:Queue.QueueName, 
IF(Owner:Queue.QueueName == "", Owner:User.Alias, ""))
***A
 User must click on the list view that they just selected, then the # More... option, below the four initial list views, to be able to access the remaining list views, up to 200. Initially, they will only see the first four list views prior to having performed this step.
 
