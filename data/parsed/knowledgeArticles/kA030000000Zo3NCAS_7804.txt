### Topic: This article helps you with the steps which will help you in creating list view on cases to see cases from specific contact/s
We want to create a case list view on cases where instead of filtering by only the owner of the case, we can filter by the contact name.
Resolution
To accomplish this type of list view on cases complete the following steps:
Create a new list view on Cases.
For step 2, 
Filter By Owner:
 select 
All Cases
For
 Filter By Additional Fields (Optional)
: choose  
Contact Name equals <put the contacts name here in the format quotationMark LastName comma space FirstName quotationMark>  
[Example: 
Contact Name equals "LastName, FirstName"
]
Choosing this will allow you to filter a list view for cases from a specific Contact
