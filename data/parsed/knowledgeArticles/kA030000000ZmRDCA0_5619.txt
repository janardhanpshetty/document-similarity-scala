### Topic: Missing Master records in sandbox copy may result in error -554779788 or -2087116159 when attempting delete or edit the Detail record in the relationship.
If the Master in a Master-Detail Relationship is missing in your sandbox environment, you may receive an Internal Server Error that looks something like: XXXXXXXXX-XXXXX (-554779788) or XXXXXXXXX-XXXXX (-2087116159).
When working with a partial sandbox copy, this error may occur if the Master record in the Master-Detail relationship was not included in the sample Object data copied to your partial sandbox. 
When working with a full sandbox copy, this error may occur if changes are made to the records in production while the sandbox copy is processing. 
Resolution
If this issue interferes with testing in your sandbox, please contact Salesforce support for assistance. A script will be run on the back end that removes orphaned Detail records.
