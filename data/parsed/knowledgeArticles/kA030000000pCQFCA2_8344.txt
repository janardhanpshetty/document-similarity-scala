### Topic: Case report to show only the latest comment in the report and not all the comments.
How to include only latest Comment on Reports?
 
Resolution
Use case: If we create a report on cases and add the case comments field to the report. The report pulls all the public comments. There might be a business scenario where the customer needs to show only the latest comment in the report.
Below mentioned is a workaround which shows how to include only latest Comment in the Reports:
Step 1:
 
Create a new " Long Text Area" case field called Last Public Comment 2.
1. Click on your Name | Setup | App Setup | Customize | Cases | Fields
Click on new and create a new “Long Text Area" case field called Last Public Comment 2.
Step 2
: 
Create a case comment workflow.
1. Click on your Name | Setup | App Setup | Create |
 
Workflow & Approvals| Workflow rules
Click on new rule and select the object as case comment. Give a name to the rule (Latest case comment workflow)
2. Select the evaluation criteria as "created, and every time it’s edited"
3.In the rule criteria select "Criteria are met" and the rule as “Case Comment: Published EQUALS True"
4.Click on save and next. Now click on "Add workflow Action" and select "New field update".
5. Enter name for the field update as "Update last comment". In field to update select the object as "Case" and select the field as Last Public comment 2.
6. In "Specify New Field Value" select  "Use a formula to set the new value" and enter " CommentBody " in the formula editor and click save.
7. Now click "Done" and "Activate" the workflow.
Step 3
: 
Create a report
Create a report on cases and add the "Last Public Comment 2" field to the report instead of case comments.
The "Last Public Comment 2" will sow only the latest public comment.
