### Topic: Set Up Debug Logging | Salesforce
Set Up Debug Logging | Salesforce
Set Up Debug Logging
To activate debug logs for users, Apex classes, and Apex triggers, configure trace flags and debug levels in the Developer Console or in Setup. Each trace flag includes a debug level, start time, end time, and log type.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view, retain, and delete debug logs:
“Manage Users”
You can retain and manage the debug logs for specific users, including yourself, and for classes and triggers.
The following are the limits for debug logs.
Each debug log must be 
2 MB
 or smaller. Debug logs that are larger than 2 MB are reduced in size by removing older log lines, such as log lines for earlier 
System.debug
 statements. The log lines can be removed from any location, not just the start of the debug log.
Each org can retain up to 
50 MB
 of debug logs. Once your org has reached 50 MB of debug logs, the oldest debug logs start being overwritten.
Configure Trace Flags in the Developer Console
To configure trace flags and debug levels from the Developer Console, click 
Debug
 | 
Change Log Levels
. Then complete these actions.
To create a trace flag, click 
Add
.
To edit an existing trace flag’s duration, double-click its start or end time.
To change a trace flag’s debug level, click 
Add/Change
 in the Debug Level Action column. You can then edit your existing debug levels, create or delete a debug level, and assign a debug level to your trace flag. Deleting a debug level deletes all trace flags that use it.
Create Trace Flags in Setup
From Setup, enter 
Debug Logs
 in the 
Quick Find
 box, then click 
Debug Logs
.
Click 
New
.
Select the entity to trace, the time period during which you want to collect logs, and a debug level.
View, Edit, or Delete Trace Flags in Setup
To manage trace flags from Setup, complete these actions.
Navigate to the appropriate Setup page.
For user-based trace flags, enter 
Debug Logs
 in the 
Quick Find
 box, then click 
Debug Logs
.
For class-based trace flags, enter 
Apex Classes
 in the 
Quick Find
 box, click 
Apex Classes
, click the name of a class, then click 
Trace Flags
.
For trigger-based trace flags, enter 
Apex Triggers
 in the 
Quick Find
 box, click 
Apex Triggers
, click the name of a trigger, then click 
Trace Flags
.
From the Setup page, click an option in the Action column.
To delete a trace flag, click 
Delete
.
To modify a trace flag, click 
Edit
.
To modify a trace flag’s debug level, click 
Filters
.
To create a debug level, click 
Edit
, and then click 
New Debug Level
.
Configure Debug Levels in Setup
To manage your debug levels from Setup, enter 
Debug Levels
 in the 
Quick Find
 box, then click 
Debug Levels
. To edit or delete a debug level, click an option in the Action column. To create a debug level, click 
New
.
See Also:
Monitor Debug Logs
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=code_add_users_debug_log.htm&language=en_US
Release
202.14
