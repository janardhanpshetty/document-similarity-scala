### Topic: Assign a Permission Set to Multiple Users | Salesforce
Assign a Permission Set to Multiple Users | Salesforce
Assign a Permission Set to Multiple Users
From any permission set page, you can assign the permission set to one or more users.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Professional
, 
Group
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To assign a permission set to users:
“Assign Permission Sets”
Walk Through It: assign a permission set
See Also:
Remove User Assignments from a Permission Set
Assign Permission Sets to a Single User
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=perm_sets_mass_assign.htm&language=en_US
Release
202.14
