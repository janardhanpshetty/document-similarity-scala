### Topic: The update changes how and when session IDs are generated and managed. Salesforce servers handle the change automatically, but if you have a custom integration that relies on session ID values, we recommend that you verify this update for compatibility in a sandbox or Developer Edition organization before enabling it permanently in your production organization.
Visualforce session management has been improved to more correctly handle transitions between different salesforce servers. The critical update is transparent in normal use but fixes problems with certain edge cases.
 
The update changes how and when session IDs are generated and managed. Salesforce servers handle the change automatically, but if you have a custom integration that relies on session ID values, we recommend that you verify this update for compatibility in a sandbox or Developer Edition organization before enabling it permanently in your production organization.
Resolution
Activate the feature in a sandbox of your organization. This feature will be enabled for all organizations in the near future.
