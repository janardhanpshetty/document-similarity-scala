### Topic: There is no way to change the type of the metric from the UI once it is saved. However, as the metric does not have a start date, you can delete and recreate the metric with the correct metric type.
Is there a way to change the metric type once it is saved?
Resolution
Currently, there is no way to change the type of the metric from the UI once it is saved. However, as the metric does not have a start date, you can delete and recreate the metric with the correct metric type.
