### Topic: When running an opportunities report, you have several choices in the standard filter area that allow you to choose what types of records should be searched. If your company is using the Opportunity Team Selling feature of Salesforce, your options increase.
Opportunity Reports: Opportunity Team Selling
Resolution
When running an opportunities report, you have several choices in the standard filter area that allow you to choose what types of records should be searched. If your company is using the Opportunity Team Selling feature of salesforce.com, your options increase. The list below identifies what each choice means and how it affects the scope of your search.
My Opportunities:
Searches ONLY the opportunities you OWN.
My Team-selling opportunities:
Searches ONLY the opportunities where you are on the SALES TEAM.
My Team-selling and my own Opportunities:
Searches BOTH the opportunities you OWN and the opportunities where you are on the SALES TEAM.
My Team's Opportunities:
Searches ONLY the opportunities OWNED by you and the users who report to you in the role hierarchy.
My Team's Team-selling and their Opportunities:
Searches the opportunities OWNED by you and the users that report to you in the role hierarchy, as well as opportunities where you or the users who report to you in the role hierarchy are on the SALES TEAM.
All Opportunities:
Searches ALL visible opportunities.
