### Topic: Solution Fields | Salesforce
Solution Fields | Salesforce
Solution Fields
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
A solution has the following fields, listed in alphabetical order.
Field
Description
Created By
User who created the solution including creation date and time. (Read only)
Language
The language in which a solution is written.
Available for organizations with 
multilingual solutions
 enabled.
Master Solution
The solution with which a translated solution is associated and from which its title and details are derived.
Available for organizations with 
multilingual solutions
 enabled.
Master Solution Details
Detailed description of the master solution from which a translated solution's details are derived. (Read only)
Available for organizations with 
multilingual solutions
 enabled.
Master Solution Title
Title of the master solution from which a translated solution's title is derived. (Read only)
Available for organizations with 
multilingual solutions
 enabled.
Modified By
User who last changed the solution fields, including modification date and time. This does not track changes made to any of the related list items on the solution. (Read only)
Open Case Detail
Link to printable view of the case that was used to create the solution. Displays only if the solution was created when the case was closed. Displays only when editing a solution. (Read only)
Out of Date
Checkbox that indicates that a translated solution's title and details may need translating to match the title and details of the master solution with which it is associated.
Available for organizations with 
multilingual solutions
 enabled.
Public
Note
Before Spring ‘14, this field was called 
Visible in Self-Service Portal
.
Indicates the solution is available in the Self-Service Portal and Customer Portal.
If Communities is enabled in your organization, this field specifies whether a solution is visible to external users in communities.
Solution Details
Detailed description of the solution. Up to 32 KB of data are allowed.
Solution details are either displayed in text or HTML, depending on how your organization is set up.
When the HTML solution detail is displayed in list views and search results, only the first 255 characters are displayed. This number includes HTML tags and images that are removed.
Solution Number
Automatically generated identifying number. (Read only) Administrators can modify the format and numbering for this field.
Solution Title
Title of the solution describing the customer’s problem or question.
Status
Status of the solution, for example, Draft, Reviewed. Entry is selected from a picklist of available values, which are set by an administrator. Each picklist value can have up to 40 characters.
Visible in Public Knowledge Base
Indicates the solution is a public solution.
This field only applies to solutions, not articles in the 
public knowledge base
.
Custom Links
Listing of custom links for solutions set up by your administrator.
See Also:
Creating Solutions
Displaying and Selecting Solutions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sol_fields.htm&language=en_US
Release
202.14
