### Topic: Unsupported Salesforce Objects and Fields in Wave | Salesforce
Unsupported Salesforce Objects and Fields in Wave | Salesforce
Unsupported Salesforce Objects and Fields in Wave
The sfdcDigest transformation can’t extract data from all Salesforce objects and fields. Consider these limitations before configuring the extraction of Salesforce objects.
For information about all Salesforce objects and fields, see the 
Object Reference for Salesforce and Force.com
.
Unsupported Objects
The sfdcDigest transformation can’t extract data from these Salesforce objects.
AuthProvider
BrandTemplate
ChatterConversation
ChatterConversationMember
ChatterMessage
ConnectedApplication
ContentFolderLink
ContentWorkspace
ContentWorkspaceDoc
CorsWhitelistEntry
CustomNotDeployed__OwnerSharingRule
EmailDomainKey
EmailServicesAddress
EmailServicesFunction
EmailStreamMessage
EmailStreamShareOption
EmailStreamShareOptionHistory
EmailStreamShareOptionOwnerSharingRule
EmailStreamShareOptionShare
EmailTemplate
EventType
EventTypeParameter
EnvironmentHub
EnvironmentHubInvitation
EnvironmentHubMemberRel
FeedPollChoice
FeedPollVote
KnowledgeArticleVersion
LoginGeo
LoginHistory
NetworkActivityAudit
NewsArticleFeed
NetworkModeration
OrganizationProperty
OrgWideEmailAddress
OutboundField
PackageLicense
PackageMemberOwnerSharingRule
PartnerNetworkSyncLog
PermissionSet
PermissionSetLicense
Profile
ReputationLevel
ReputationLevelLocalization
ReputationPointsRule
SearchPromotionRule
SelfServiceUser
SessionPermSetAssignment
SetupAssistantAnswer
SetupAssistantProgress
SsoUserMapping
TenantSecret
Territory2ModelHistory
TwoFactorInfo
UserLogin
UserPackageLicense
UserProvAccount
UserProvAccountStaging
UserProvisioningConfig
UserProvisioningLog
UserProvisioningRequest
UserProvisioningRequestOwnerSharingRule
UserProvisioningRequestShare
UserProvMockTarget
UserRecordAccess
VerificationHistory
WebLink
WebLinkLocalization
The sfdcDigest transformation cannot extract data from external objects created in Salesforce. External objects are similar to custom objects, except that they map to data located outside Salesforce.
If you include an unsupported or inaccessible object in the sfdcDigest transformation, the dataflow fails at run time with an error message.
Unsupported Fields
The sfdcDigest transformation cannot extract data from these fields:
Object
Unsupported Fields
Account
CleanStatus
ActionPlanItem
ItemId
AuthSession
LoginGeoId
LoginHistoryId
CaseArticle
KnowledgeArticleId
Contact
CanAllowPortalSelfReg
CleanStatus
ContentDocument
ParentId
ContentFolderLink
ParentEntityId
CustomPerson__p
Title
DocumentAttachmentMap
ParentId
EmailMessage
ActivityId
EmailRoutingAddress
EmailServicesAddressId
EnvironmentHubMember
EnvironmentHubId
ExternalEventMapping
EventId
InstalledMobileApp
ConnectedApplicationId
Lead
CleanStatus
KnowledgeArticle
MasterLanguage
KnowledgeArticleVersion
IsOutOfDate
TranslationCompletedDate
TranslationExportedDate
TranslationImportedDate
Network
CaseCommentEmailTemplateId
ChangePasswordEmailTemplateId
ForgotPasswordEmailTemplateId
WelcomeEmailTemplateId
Organization
SelfServiceEmailUserOnCaseCreationTemplateId
SelfServiceNewCommentTemplateId
SelfServiceNewPassTemplateId
SelfServiceNewUserTemplateId
WebToCaseAssignedEmailTemplateId
WebToCaseCreatedEmailTemplateId
WebToCaseEmailTemplateId
WebToLeadEmailTemplateId
PermissionSet
PermissionsEditEvent
PermissionsEditTask
PermissionSetLicense
MaximumPermissionsEditEvent
MaximumPermissionsEditTask
Profile
PermissionsEditEvent
PermissionsEditTask
ThirdPartyAccountLink
SsoProviderId
User
LastPasswordChangeDate
UserPreferencesEnableVoicePilot
WorkBadge
RewardId
WorkBadgeDefinition
RewardFundId
If you include a field with an unsupported field in the sfdcDigest transformation, the dataflow ignores the field.
In addition, Salesforce recommends that you do not extract data from the MayEdit field of the Account object. Extracting data from this field significantly decreases performance and can cause the dataflow to fail.
Unsupported Field Types
The sfdcDigest transformation can’t extract data from fields with these field types.
base64
composite (like address and location)
data category group reference
encrypted string
If you include a field with an unsupported field type in the sfdcDigest transformation, the dataflow ignores the field.
See Also:
sfdcDigest Transformation
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_integrate_salesforce_extract_transformation_unsupported_objects_fields.htm&language=en_US
Release
202.14
