### Topic: Article describing case routing for Live Agent cases for Salesforce Technical Support
Case Creation & Routing
 - Cases for Live Agent are created as normal, set the 
Functional Area to "Live Agent"
 
and assign using 
Active Assignment Rules
. These cases will route to the CRM Configuration Skill Group and will be assigned to the Core Team supporting the product.
Case Handling
 - The Live Agent team is staffed to support customers with live deployments. Cases should be created and routed per the notes above, process questions can be addressed via chatter 
Support - 'CRM Config' Skill Group- Chatter Group
.
Social Media
 - If we receive any issues related to Live Agent for Technical Support from Social Media, please create a case per details above. Please at mention 
@
Support - 'CRM Config' Skill Group
 on any case logged. Please monitor keywords "
Live Agent
".
Live Agent Overview
Click for the
 
Live Agent Overview
Resolution
