### Topic: Create, Preview, and Activate Quote Templates | Salesforce
Create, Preview, and Activate Quote Templates | Salesforce
Create, Preview, and Activate Quote Templates
Define the look of your company’s quote PDFs by creating templates that your sales reps can choose when they create quote PDFs.
Available in: Salesforce Classic
Available in: 
Performance
 and 
Developer
 Editions
Available in: 
Professional
, 
Enterprise
, and 
Unlimited
 Editions with the Sales Cloud
User Permissions Needed
To create quote templates:
"Customize Application"
To view quote templates:
"View Setup and Configuration"
Watch a Demo: 
Creating Quote Templates
From Setup, enter 
Templates
 in the 
Quick Find
 box, then select 
Templates
 under Quotes.
Click 
New
, and then select a template, such as 
Standard Template
, on which to base your new template.
Give your new template a name, and then click 
Save
.
In the template editor, drag the elements that you want to the template, and then complete the details. To add:
One or more Quote fields or fields from related objects, use a section and add fields to it.
Text that you can edit and format, such as terms and conditions, use 
Text/Image Field
.
An image, such as your company logo, use 
Text/Image Field
.
A table of Quote fields or fields from a different object, such as Quote Line Item, use a list.
Click 
Quick Save
 to save your changes and continue working on the template.
Click 
Save and Preview
 to make sure that the quote PDFs that users create will look the way you want them to.
Preview shows templates in system administrator profile view. The preview and the template show the rich text and images that you’ve added. Other data is simulated.
Important
Save and Preview
 saves changes to your template, so after you preview, you can’t undo them.
Click 
Save
 when you’re finished.
Return to the Quote Templates page, and then click 
Activate
.
See Also:
Enhance Quote Templates
Quote Template Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=quotes_template_create.htm&language=en_US
Release
202.14
