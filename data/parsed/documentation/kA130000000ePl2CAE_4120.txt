### Topic: Contact Roles Limitations | Salesforce
Contact Roles Limitations | Salesforce
Contact Roles Limitations
Contact roles have some limitations.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
When you create an opportunity from a contact detail page, that contact becomes the primary contact on the opportunity. However, a contact role isn’t automatically assigned.
On case contact roles, the 
Primary
 option isn’t available. The primary contact on a case is always the contact listed under 
Contact Name
 in the Case Detail section.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_contact_roles.htm&language=en_US
Release
202.14
