### Topic: Set Up Salesforce to Communicate with Microsoft® Exchange | Salesforce
Set Up Salesforce to Communicate with Microsoft® Exchange | Salesforce
Set Up Salesforce to Communicate with Microsoft® Exchange
After running the Remote Connectivity Analyzer, log in to Salesforce to enable Lightning Sync and provide your service account credentials.
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To access Lightning Sync setup in Salesforce:
“View Setup and Configuration”
From Salesforce Setup, enter 
Lightning Sync Setup
 in the 
Quick Find
 box, then select 
Lightning Sync Setup
.
Under Enable Lightning Sync, click 
Edit
.
Check 
Enable Lightning Sync
 and click 
Save
.
Enter the service account user name you created in Exchange, including your company’s domain and top-level domain. For example, if you work for AW Computing, and the service account mailbox you’ve created is 
svcacct@awcomputing.com
, enter your user name in either of these formats:
svcacct@awcomputing.com
awcomputing.com\svcacct
Some network configurations require one format rather than the other. If your first attempt to get Salesforce and Exchange to communicate is unsuccessful, try the other format.
Enter your service account password.
If your sales reps’ email domains are different than your service account domain, enter the additional domains in a comma-separated list. For example, if your service account is 
svcacct@awcomputing.com
, but your reps’ email addresses are in the domain 
awcomputing.net
, enter 
awcomputing.net
 as an additional domain so Lightning Sync can recognize it.
Click 
Save
.
Parent topic:
 
See the Big Picture for Setting Up Lightning Sync
Previous topic:
 
Run the Microsoft® Remote Connectivity Analyzer with Lightning Sync Parameters
Next topic:
 
Run the Lightning Sync Connection Test
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=productivity_sync_exchange_admin_access_service_account.htm&language=en_US
Release
202.14
