### Topic: in summer 15 release, uninstalling some package may fail due to error: cApp Name could not be removed during uninstall. Cannot complete this operation. This connected app is referenced elsewhere in salesforce.com. Remove the usage and try again. : User Provisioning Config
In summer 15 release, uninstalling some package which contains connected app may fail due to error: 
TestcApp could not be removed during uninstall. 
Cannot complete this operation. 
This connected app is referenced elsewhere in salesforce.com. Remove the usage and try again. : User Provisioning Config
(TestcApp is the assumed connected app name)
Resolution
R&D logged a bug "
W-2609782
" to resolve the root cause.
Before the fix is released, the workaround is to follow the steps below:
1. log in to workbench using api version 34+(this object is only available in version 34+), and run the follow queries:
SELECT ConnectedAppId,Id FROM UserProvisioningConfig 
ConnectedAppId Id 
0H4B00000004IxqKAE 
0JeB00000004C9hKAE
 
0H4B0000000.....    0JeB0000......
0H4B0000000.....    0JeB0000......
SELECT Id,Name FROM ConnectedApplication WHERE Name LIKE '%TestcApp%' 
Id Name 
0H4B00000004IxqKAE
 TestcApp for Salesforce
 
0H4B00000004IxrKAE TestcApp for Salesforce (QA) 
2. As we can see from above, the record with id 
0JeB00000004C9hKAE
  is the one which is causing the issue. We cannot delete the record via api in workbench because UserProvisioningConfig object is readonly. (refer to doc: 
Link
.)
3. This record can be accessed via UI and then can be deleted, eg, access it using URL: https://nax.salesforce.com/
0JeB00000004C9hKAE
 and you will see Delete button.
4. Delete the record and then you should be able to uninstall the package.
 
