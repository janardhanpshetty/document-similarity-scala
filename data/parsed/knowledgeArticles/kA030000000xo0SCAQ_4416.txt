### Topic: This needs a support case to be raised to salesforce. Raise a case and Salesforce support should be able to assist appropriately
Report Charts on page layout is not available
Click path
In Standard Objects, simply go to setup | Customize | Object | Page Layout
For Custom Objects, simply go to setup | Create | Objects | Page Layout
Try to check the list of options for the page layout
Option to add "Report Chart" is not showing on the list (related list, fields, etc)
Resolution
This is a rare occurrence and normally happens for organizations that haven't opted in for the auto enabling of a new feature. Open a case with Salesforce support to get this resolved appropriately.
