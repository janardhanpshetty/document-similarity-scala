### Topic: Detailed steps on how to delete a thanks that has been awarded from one's profile.
Is it possible to delete a badge that has been awarded from one's profile?
Resolution
1. Open 
Dataloader
 and click on 
Export
2. Select Data Object 
Badge Received( WorkBadge)
3. Click on 
Select all fields
  button and then 
Finish
4. Open the exported file in Excel and identify the records you would like to delete
5. Copy the 
ID
 field you would like to delete
6. Paste the copied 
ID
 at the end of the URL in Salesforce (
e.g. https://na15.salesforce.com/
0W1i0000000L0Uo
) and confirm that this is the record you would like to delete
7.
 Copy and paste the ID into a new csv file and save the file
8. Open Dataloader and select the 
Delete
 function
9. Select object as 
Badge Received
.
10. Choose this newly created file and map the ID field
11. Proceed to delete the record. 
