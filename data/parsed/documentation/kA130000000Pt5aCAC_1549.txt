### Topic: Assigning Cases | Salesforce
Assigning Cases | Salesforce
Assigning Cases
Available in: Salesforce Classic
The case assignment options vary according to which Salesforce Edition you have.
User Permissions Needed
To view cases:
“Read” on cases
To take ownership of cases from queues:
“Edit” on cases
You can assign cases to users or queues in a variety of ways.
Using an Assignment Rule for Web-to-Case, Email-to-Case, or On-Demand Email-to-Case
In Professional, Enterprise, Unlimited, Performance, and Developer Edition organizations, web- and email-generated cases are automatically assigned to users or queues based on criteria in your active case assignment rule.
Cases that do not match the assignment rule criteria are automatically assigned to the 
Default Case Owner
 specified in the Support Settings.
Using an Assignment Rule when Creating or Editing a Case
In Professional, Enterprise, Unlimited, Performance, and Developer Edition organizations, when creating or editing a case, you can check a box to assign the case automatically using your active case assignment rule. An email is automatically sent to the new owner if your administrator specified an email template in the matching rule entry. If you want this checkbox to be selected by default, your administrator can modify the appropriate page layout. If required, your administrator can edit the page layout to hide the assignment checkbox but still force case assignment rules.
Changing Ownership of Multiple Cases (administrators only)
From any case list page, an administrator, or a user with the “Manage Cases” permission, can manually assign one or more cases to a single user or queue.
Taking Cases from a Queue
To take ownership of cases in a queue, go to the queue list view, check the box next to one or more cases, and click 
Accept
.
Note
The organization-wide sharing model for an object determines the access users have to that object's records in queues:
Public Read/Write/Transfer
Users can view and take ownership of records from any queue.
Public Read/Write or Public Read Only
Users can view any queue but only take ownership of records from queues of which they are a member or, depending on sharing settings, if they are higher in the role or territory hierarchy than a queue member.
Private
Users can only view and accept records from queues of which they are a member or, depending on sharing settings, if they are higher in the role or territory hierarchy than a queue member.
Regardless of the sharing model, users must have the “Edit” permission to take ownership of records in queues of which they are a member. Administrators, users with the “Modify All” object-level permission for Cases, and users with the “Modify All Data” permission, can view and take records from any queue regardless of the sharing model or their membership in the queue.
Changing Ownership of One Case
To transfer a single case you own or have read/write privileges to, click 
Change
 next to the 
Case Owner
 field on the case detail page, and specify a user, partner user, or queue. Make sure that the new owner has the “Read” permission on cases. The 
Change
 link displays only on the detail page, not the edit page.
In Group, Professional, Enterprise, Unlimited, Performance, and Developer Edition organizations, check the 
Send Notification Email
 box to automatically send an email to the new case owner.
Creating a Case Manually (default assignment)
When you create a case from the Cases tab, you are automatically listed as the case owner, unless the assignment rule checkbox is displayed and you select it to enable the assignment rule. If it’s selected by default, you can override the assignment rule and assign yourself as the owner by deselecting the checkbox.
See Also:
Guidelines for Working with Cases
Changing Multiple Cases
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cases_assign.htm&language=en_US
Release
202.14
