### Topic: Guidelines for Making Search Faster | Salesforce
Guidelines for Making Search Faster | Salesforce
Guidelines for Making Search Faster
Follow these guidelines to help your users find information faster.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions 
except Database.com
Records are included in search results only if the object’s field that contains the information matching the search term is searchable.
After a searchable object’s record is created or updated, it could take about 15 minutes or more for the updated text to become searchable.
To make searches faster across your org:
Disable search for custom objects that your users aren’t actively searching
Choose which custom objects your users can search by enabling the 
Allow Search
 setting on the custom object setup page. If you don’t need a custom object’s records to be searchable, disable search for that custom object. Making a custom object searchable when you don’t need your users to find its records slows down searches across your org.
By default, search is disabled for new custom objects. Disabling search doesn’t affect reports and list views.
Note
Custom object records are searchable in the Salesforce user interface only if the custom object is associated with a custom tab. Users aren't required to add the tab for display.
Disable search for external objects that your users aren’t actively searching
To disable search for an external object, deselect 
Allow Search
 on its setup page. To include an external object in SOSL and Salesforce searches, enable search on both the external object and the external data source.
By default, search is disabled for new external objects. However, you can validate and sync an external data source to automatically create external objects. 
Syncing always enables search on the external object when search is enabled on the external data source, and vice versa.
As with custom objects, unnecessarily making an external object searchable can slow down searches across your org.
Avoid making significant changes to your org at once
Creating or updating many records at the same time, such as via data imports, increases the time it takes for each record to become searchable. If you have a large org with many users who frequently make simultaneous updates, schedule bulk uploads and background processes to run during non-peak hours.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_index_considerations.htm&language=en_US
Release
202.14
