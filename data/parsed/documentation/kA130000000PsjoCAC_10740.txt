### Topic: Monitoring Scheduled Jobs | Salesforce
Monitoring Scheduled Jobs | Salesforce
Monitoring Scheduled Jobs
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Reporting Snapshots and Dashboards are not available in 
Database.com
User Permissions Needed
To monitor scheduled jobs:
“View Setup and Configuration”
The All Scheduled Jobs page lists all reporting snapshots, scheduled Apex jobs, and dashboards scheduled to refresh.
To view this page, from Setup, enter 
Scheduled Jobs
 in the 
Quick Find
 box, then select 
Scheduled Jobs
. Depending on your permissions, you can perform some or all of the following actions.
Click 
Del
 to permanently delete all instances of a scheduled job.
View the details of a scheduled job, such as the:
Name of the scheduled job
Name of the user who submitted the scheduled job
Date and time at which the scheduled job was originally submitted
Date and time at which the scheduled job started
Next date and time at which the scheduled job will run
Type of scheduled job
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_monitoring_jobs.htm&language=en_US
Release
202.14
