### Topic: Please see Resolution section
We're seeing SAML login failures from SP-initiated login with an error message of Invalid InResponseTo. We are sending and receiving the InResponseTo as part of our SP-initiated SSO login. Can you please provide more details on how the SP-initiated login and InResponseTo works with the SFDC implementation?
Resolution
When SAML Assertions fail with "InResponseTo: Invalid", here are some scenarios on why it happens. 
InResponseTo is one method we(Salesforce) use to decrease the likelihood of forged response attacks. This forces the attacker to intercept the request before sending a forged response call. Our time-limit increases this security further. As our app waits for a response then the attributed time is also available for an interception and forged response to occur. 
A common use of message insertion would be a denial of service attack. 
InResponseTo can be an issue in three situations: 
1. The InResponseTo data does not match our SAML request ID 
2. The Response breaches our 8 minute time limit 
3. No InResponseTo was expected 
1 & 2 above are almost similar. If the user is logging in from login.salesforce.com, Salesforce sends a SAML request Id. If the SAML response comes back after the 8 min window, then this request ID will be different and SAML assertion will fail. 
