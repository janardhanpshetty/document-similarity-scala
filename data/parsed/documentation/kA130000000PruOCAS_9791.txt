### Topic: How can I improve my Data.com search results? | Salesforce
How can I improve my Data.com search results? | Salesforce
How can I improve my Data.com search results?
It’s easy. Just use 
search modifiers
. To see examples, just click the info icon (
) above each Data.com search field. It’s also helpful to save searches you use frequently.
See Also:
Understand Data.com Searches
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_datadotcom_how_do_i_improve_search_results.htm&language=en_US
Release
202.14
