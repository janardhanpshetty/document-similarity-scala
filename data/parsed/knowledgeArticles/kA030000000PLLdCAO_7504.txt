### Topic: This article explains the effect of Multi-Currency on decimal places in Currency Fields.
After setting the number of decimals on a currency field, these settings will not take on the page, if you set the number of decimals to zero, you might still see decimals. Or you might set the number of decimals to two, but you will see none.
You can also see a different behaviour that the number of decimals will differ depending of the Currency that the record is in. Decimals for USD, but not for YEN.
Resolution
This is an expected behavior when Multi-Currency is enabled in your Org. 
Settings to a currency field for an object will be superseded by the setting in the Currency setup
 
For example
: (Consider the Decimal Place setting on the currency field is set to 2) 
If you type the opportunity amount as “123.89” on the record and its Currency field Decimal Place setting is set to “0”, the amount will display as “124”. 
Similarly, if you type the opportunity amount as “124” on the record and its Currency field Decimal Place setting is set to “2”, the amount will display as “124.00”, irrespective of what the Decimal Place setting is defined at the field level.
Locating or Editing 'Decimal Place' 
Settings 
at the Currency Level
Click on your Name||Setup|| Administration Setup|| Company Profile|| Manage Currencies (If you have “Enable Improved Setup User Interface” disabled) 
​
OR
Click on your Name||Setup|| Administer|| Company Profile|| Manage Currencies (If you have “Enable Improved Setup User Interface” enabled)
Click on the button ‘Manage Currencies’ which will take you to the Currency Setup Page.
Under ‘Active Currencies’ section, you can review the ‘Decimal Places’ defined for individual Currency.
To make changes to the same, click on ‘Edit’ next to the Currency you want to make changes in. 
