### Topic: Wave Labs direct support
Wave Labs is available here: 
jumpstartwave.com
 
Contents
There are some great tools in this package - not the least of which is a visual dataflow editor that allows you to open an existing dataflow, make changes using a visual editor and republish it without having to download, edit with a text editor and re-upload.
 
 
Current tools:
Dataflow Editor
XMD Editor
Wave Workbench
SAQL Editor
Create Snapshots
Dashboard Templates
 
Future Tools:
Trending Dashboard Wizard
Migration Tool
Scheduled Notifications
Support
Wave Labs is described as a community-developed unmanaged package and is 
not supported by Salesforce Support
.
Most inquiries can be addressed through the Wave Analytics Success Community: 
https://success.salesforce.com/featuredGroupDetail?id=a1z30000006IDYqAAO
 
The Labs team does provide an email address for help.
Resolution
For more detailed support inquiries, we can refer customers to the direct channel with the following wording:
"Wave Labs is not directly supported by Salesforce Support. In order to best assist you, the Labs team has set up a channel for Wave Labs support requests. Please email 
WaveLabs@salesforce.com
 for assistance."
