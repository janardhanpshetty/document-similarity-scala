### Topic: Deploy Using Change Sets | Salesforce
Deploy Using Change Sets | Salesforce
Deploy Using Change Sets
Available in: both Salesforce Classic and Lightning Experience
Available in 
Enterprise
, 
Performance
, 
Unlimited
, and 
Database.com
 Editions
You can deploy workflows, rules, Apex classes and triggers, and other customization from a sandbox organization to your production organization. You can create an outbound change set in the Salesforce user interface and add the components that you would like to upload and deploy to the target organization. To access change sets, from Setup, enter 
Outbound Change Sets
 in the 
Quick Find
 box, then select 
Outbound Change Sets
.
See Also:
Change Sets
Choose Your Tools for Developing and Deploying Changes
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=code_tools_changesets.htm&language=en_US
Release
202.14
