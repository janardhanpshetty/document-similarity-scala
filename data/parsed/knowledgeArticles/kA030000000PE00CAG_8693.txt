### Topic: Learn what information our Admin team needs when you request a mass transfer of Accounts from one owner to another in your org.
If you need to make a mass transfer of accounts request, this is the minimum information our Admin team will need to process your request.
Resolution
Admin Request: Mass Account Record Transfer
Note: Is this in Sandbox or Production? If Sandbox, which sandbox? Would you like us to contact you before proceeding with work, even if all the requirements are clear? We will always contact you if something needs further explanation.
1. Who are we transferring records FROM?
2. Who are we transferring the records TO?
3. Mark a “Y” next to the options below that apply to the Account record transfer:  
Transfer open opportunities not owned by the existing account owner
Transfer closed opportunities  Transfer open cases owned by the existing account owner
Transfer closed cases
Keep Account Team
Keep Sales Team on opportunities transferred to new owner
NOTE: All of our Configuration Case Templates can be accessed on our 
Premier Success Plans Tool kit
.
Scroll down to the section “Customize Salesforce for your Business” | Find the tile “Configuration Case Templates” | Click "Download"
 
Or, click directly to the 
Configuration Case Templates
.​
