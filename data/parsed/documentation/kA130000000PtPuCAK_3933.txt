### Topic: Sample Approval Process: Job Candidates | Salesforce
Sample Approval Process: Job Candidates | Salesforce
Sample Approval Process: Job Candidates
When your company interviews candidates for a position, you may have several levels of approval before you can send an offer letter. Use this example to create a three-step approval process that requires approval from multiple management levels.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Prep Your Organization
Before creating the approval process:
If you don't yet have a custom object to track candidates, create a custom object and tab called Candidates. Add the appropriate fields such as 
Salary
, 
Offer Extended
 (checkbox), and 
Date of Hire
.
Create an email template to notify approvers that an approval request needs to be reviewed. Direct users to the approval page in Salesforce by including approval process merge fields.
Create the Approval Process
Create an approval process on the Candidate custom object using the following specifications:
Don't enter filter criteria because you want all submitted offers to be approved.
Choose the 
Manager
 field as the next automated approver.
Select the email template you created for this approval process.
Choose the record owner or any other user that you want to be able to submit offer letters.
Create three approval steps:
Create a step named 
Step 1: Manager Approval
:
No filter is necessary as you want all records to advance to this step.
In the 
Automatically assign to approver(s)
 option, select the manager of the user submitting the request.
If appropriate, choose 
The approver's delegate may also approve this request
 if you want to allow the user in the 
Delegated Approver
 field to approve requests.
Create a second step named 
Step 2: VP Approval
:
No filter is necessary as you want all records to advance to this step.
Choose 
Let the user choose the approver
 to allow the manager to select the appropriate VP to approve the request.
If appropriate, choose 
The approver's delegate may also approve this request
 if you want to allow the user in the 
Delegated Approver
 field to approve requests.
Choose 
Perform ONLY the rejection actions for this step...
 so that the request returns to the manager for changes if the VP rejects the request.
Create a third step named 
Step 3: CFO Approval
:
No filter is necessary as you want all records to advance to this step.
Choose 
Automatically assign to approver(s)
 and select the name of your CFO.
If appropriate, choose 
The approver's delegate may also approve this request
 if you want to allow the user in the 
Delegated Approver
 field to approve requests.
Choose 
Perform all rejection actions for this step AND all final rejection actions. (Final Rejection)
 so that offer letters rejected by your CFO are completely rejected.
Tip
Consider creating the following final approval actions:
Email alert to notify the user who submitted the offer letter request.
Field update to select the 
Offer Extended
 checkbox.
Consider creating the following final rejection actions:
Email alert to notify the manager that the offer won't be extended.
Wrap Things Up
After creating the approval process, add the Approval History related list to the Candidates object page layout.
Tip
Consider adding the Items To Approve related list to your custom home page layouts. It gives users an instant view of the approval requests that are waiting for their response.
If available, use your sandbox to test the approval process, then activate it.
See Also:
Define a Custom Object
Manage Email Templates
Set Up an Approval Process
Prepare Your Org for Approvals
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_useful_approval_processes_jobs.htm&language=en_US
Release
202.14
