### Topic: The automated program responsible for delivery of tracking data to Coremetrics was interrupted or failed to complete.
If the automated program responsible for delivery of tracking data to Coremetrics was interrupted or failed to complete, follow the steps below to pull back tracking data outside of the previous 24 hour window.
Resolution
The Coremetrics import process expects data to be delivered in 1-day increments. Therefore, it'll be necessary to generate a separate file for each day where historic tracking data is absent. For each additional day, update the date value in step #5 and the file naming pattern in steps #6 and #12 with the new day’s date, and then re-run the program as described in step #21.
 
Generate 1-day files
 
1. Click 
Interactions 
| 
Activities 
| 
Data Extract
.
2. Create a new Data Extract Activity with a name of “Manual Coremetrics Tracking Data Extract” or something similar.
3. Use an identical File Naming Pattern as is found in the existing Coremetrics Data Extract Activity.
4. Select “Core Metrics LiveMail 2” as the Extract Type.
5. Select a “Specific Range” of one day, e.g. 8/15/2011 to 8/16/2011 (12AM to 12AM).
6. Replace the Year, Month, and Day wildcards in the File Naming Pattern with the date of the end date from step #5.
7. 
Save
 the Data Extract Activity.
8. Click 
File Transfer
.
9. Create a new File Transfer Activity with the name "Manual Coremetrics Tracking File Transfer” or something similar.
10. Click 
Upload.
11. Select the 
Coremetrics FTP File Location.
12. Use the same File Naming Pattern as specified in step #6.
13. 
Save
 the File Transfer Activity.
14. Click 
My Programs
.
15. Create a new Program with the name “Manual Coremetrics Tracking Program” or something similar.
16. Edit the Program’s Process.
17. Add the Data Extract Activity created in step #2 as the only Activity in the Program’s first Step.
18. Add a second Step with the File Transfer Activity from step #9 as the only Activity.
19. 
Save
 the Process.
20. 
Save
 the Program.
21. Start the Program with the “Start immediately, run once” option.
