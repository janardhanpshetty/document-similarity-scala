### Topic: Creating and Editing Case Comments on Case Detail Pages | Salesforce
Creating and Editing Case Comments on Case Detail Pages | Salesforce
Creating and Editing Case Comments on Case Detail Pages
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view case comments:
“Read” on cases
To add case comments and make case comments public:
“Edit” or “Create” on cases
To edit or delete case comments added by other users:
“Modify All” on cases
To edit, delete, or make public your existing case comments:
“Edit Case Comments”
Click 
New
 or 
Edit
 on the Case Comments related list.
Optionally, select 
Public
 to enable comment notifications to the contact on the case, and to let the contact view the comment on the Customer Portal or Self-Service.
Type comments in 
Comment
.
Click 
Save
.
Note
Starting with Spring ’12, the Self-Service portal isn’t available for new orgs. Existing orgs continue to have access to the Self-Service portal.
Note
If you publish cases and case comments to external contacts via Salesforce to Salesforce, all public case comments are automatically shared with a connection when you share a case. To stop sharing a comment, select 
Make Private
.
Tip
On the Case Comments related list:
Click 
Del
 to delete an existing comment.
Click 
Make Public
 or 
Make Private
 to change the public status of a comment on the Customer Portal or Self-Service portal. Case comments marked 
Public
 display as private messages from customer support in Chatter Answers. They don’t display to the entire community. For example, if a support agent adds a public case comment, it displays only to the case’s contact private messages in Chatter Answers. Support agents can read all private and public case comments.
See Also:
Case Comments
Creating and Editing Case Comments
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=case_comments_create_detail_page.htm&language=en_US
Release
202.14
