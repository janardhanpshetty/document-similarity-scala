### Topic: Delete Components from Managed Packages | Salesforce
Delete Components from Managed Packages | Salesforce
Delete Components from Managed Packages
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To delete components from a package:
“Create AppExchange Packages”
After you've uploaded a 
Managed - Released
 package, you may find that a component needs to be deleted from your organization. One of the following situations may occur:
The component, once added to a package, can't be deleted.
The component can be deleted, but can only be undeleted from the Deleted Package Components page.
The component can be deleted, but can be undeleted from either the Deleted Package Components page or through the Recycle Bin
To access the Deleted Package Components page, from Setup, enter 
Packages
 in the 
Quick Find
 box, then select 
Packages
. Select the package that the component was uploaded to, and then click 
View Deleted Components
. You can retrieve components from the Recycle Bin and Deleted Package Components page any time 
before
 uploading a new version of the package. To do this, click 
Undelete
 next to the component.
After a package is uploaded with a component marked for deletion, it is deleted forever.
Warning
Although a component is deleted, its 
Name
 remains within Salesforce. You can never create another component with the same name. The Deleted Package Components page lists which names can no longer be used.
To access the Deleted Package Components page, from Setup, enter 
Packages
 in the 
Quick Find
 box, then select 
Packages
. Select the package that the component was uploaded to, and then click 
View Deleted Components
. If a component can be retrieved through the Recycle Bin, it can also be retrieved through this page. You can retrieve the following types of components from here.
Apex classes and triggers that don't have 
global
 access.
Custom tabs.
Visualforce components with 
public
 access.
Protected components, including:
Custom labels
Custom links (for Home page only)
Workflow alerts
Workflow field updates
Workflow outbound messages
Workflow tasks
Workflow flow triggers
The Process Builder has superseded flow trigger workflow actions, formerly available in a pilot program. Organizations that are using flow trigger workflow actions can continue to create and edit them, but flow trigger workflow actions aren’t available for new organizations.
Data components, such as Documents, Dashboards, and Reports. These are the only types of components that can also be undeleted from the Recycle Bin.
You can retrieve components from the Recycle Bin and Deleted Package Components page any time 
before
 uploading a new version of the package. To do this, click 
Undelete
 next to the component.
The Deleted Components displays the following information (in alphabetical order):
Attribute
Description
Action
If the 
Managed - Released
 package hasn't been uploaded with the component deleted, this contains an 
Undelete
 link that allows you to retrieve the component.
Available in Versions
Displays the version number of the package in which a component exists.
Name
Displays the name of the component.
Parent Object
Displays the name of the parent object a component is associated with. For example, a custom object is the parent of a custom field.
Type
Displays the type of the component.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=viewing_deleted_components.htm&language=en_US
Release
202.14
