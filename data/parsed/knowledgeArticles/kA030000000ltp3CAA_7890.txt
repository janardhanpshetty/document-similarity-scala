### Topic: This article would be helpful when there is a need to filer a report based on Data Categories.
Customer can have a requirement to create a report where they would like to filter on Data Categories. Example filtering on FAQs alone to check the articles in that data category
Resolution
Data Categories are used in Knowledge Articles and Chatter Answers that classify the articles based on the category which helps the users to browse articles easily.
Few commonly used Data Category is FAQ, Sales Related, Billing Related etc., and these Data Category varies from business to business.
There are currently no field named 'Data Category' available with the standard/custom report types, however the "Data Category" information is available as a Cross Filter in 'Knowledge Articles' standard report type.
To achieve this, please follow the below steps :
Click on the Reports Tab and select a report on Knowledge. Example '
Knowledge Articles
'.
Click on the 'Add' drop down button next to the "filter" option and you can find the 'Data Category' cross Filter (easily identifiable by an intersecting Circle)
This way you can filter the knowledge articles present in your Org based on the Data Category.
Note: Data Category is a Hierarchy like structure and hence the operator that you would find are Data Category "At", "Above" etc., Here "At" can be equated to the regular "Equals" operation found commonly in filter
 
