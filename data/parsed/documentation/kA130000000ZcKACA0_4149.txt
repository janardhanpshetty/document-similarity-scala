### Topic: Selecting a Best Answer for a Question in Chatter | Salesforce
Selecting a Best Answer for a Question in Chatter | Salesforce
Selecting a Best Answer for a Question in Chatter
One of the advantages of asking a question in Chatter is that users can select the best answer for a question. When a question has a best answer, your users can quickly resolve their issue by going directly to the best answer.
Available in: Salesforce Classic
Chatter Questions is available in: 
Group
, 
Professional
, 
Developer
, 
Performance
, 
Enterprise
, and 
Unlimited
 Editions
Moderators and the person who asked the question can:
Select the best answer for a question
Remove the best answer status from an answer
Only one answer can be selected as the best answer. Moderators can be the Chatter moderator, the community moderator, or the Salesforce administrator. If a user doesn’t have permission to select the best answer for a question, they don’t see the 
Select as Best
 option.
When an answer is selected as a best answer, a check mark (
) appears next to it. A copy of the best answer also appears at the top of the list of answers so other users can quickly spot it.
See Also:
Display Similar Questions and Articles in Chatter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_chatter_questions_best_answer.htm&language=en_US
Release
202.14
