### Topic: Modifying Field Access Settings | Salesforce
Modifying Field Access Settings | Salesforce
Modifying Field Access Settings
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view field accessibility:
“View Setup and Configuration”
To change field accessibility:
“Customize Application”
AND
“Manage Profiles and Permission Sets”
From the field accessibility grid, you can click any field access setting to change the field’s accessibility in the page layout or in field-level security. The Access Settings page then lets you modify the field access settings.
In the Field-Level Security section of the page, specify the field's access level for the profile.
Access Level
Enabled Settings
Users can read and edit the field.
Visible
Users can read but not edit the field.
Visible
 and 
Read-Only
Users can’t read or edit the field.
None
We recommend that you use field-level security to control users’ access to fields rather than creating multiple page layouts to control field access.
In the Page Layout section of the page, you can:
Select the 
Remove or change editability
 radio button and then change the field access properties for the page layout. These changes will affect all profile and record type combinations that currently use this page layout.
Alternatively, you can select the 
Choose a different page layout
 radio button to assign a different page layout to the profile and record type combination.
See Also:
What Determines Field Access?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=modifying_field_access_settings.htm&language=en_US
Release
202.14
