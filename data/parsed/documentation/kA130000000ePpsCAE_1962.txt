### Topic: OData 2.0 Type Mapping | Salesforce
OData 2.0 Type Mapping | Salesforce
OData 2.0 Type Mapping
Understand how the OData 2.0 adapter for Salesforce Connect maps OData types to Salesforce metadata field types.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Developer
 Edition
Available for an extra cost in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Salesforce Connect supports only the following types when syncing metadata and converting values between Salesforce and an external system.
OData 2.0 Primitive Types
OData 2.0 Type
Salesforce Metadata Field Type
Binary
TextArea
Boolean
Checkbox
Byte
Number
DateTime
DateTime
DateTimeOffset
DateTime
Decimal
Number
Double
Number
Guid
Text
Int16
Number
Int32
Number
Int64
Number
SByte
Number
Single
Number
String
Text when the declared length of the OData string column is 255 or fewer characters
LongTextArea when the declared length of the OData string column is greater than 255 characters
Time
Text
Tip
A binary value from an external system is represented in Salesforce as a base64-encoded string. You can convert it to a value of type 
Blob
 by using the 
EncodingUtil.base64Decode(inputString)
 Apex method.
OData 2.0 Complex Types
Salesforce Connect supports OData complex types as follows.
External data of complex type is flattened into a string that contains field names and values. For example, an address is flattened into the following string.
Street: 55 East 5th Street, City: New York, State: NY, Zip: 10003
An external object custom field associated with an OData complex type on the external system is always read only, even if the external object is writable.
See Also:
Sync Considerations for Salesforce Connect—All Adapters
OData Reference for Salesforce Connect—OData 2.0 and 4.0 Adapters
Apex Developer Guide
: EncodingUtil Class: 
base64Decode(inputString)
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=odata_type_mapping_v2.htm&language=en_US
Release
202.14
