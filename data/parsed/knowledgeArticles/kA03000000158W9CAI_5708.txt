### Topic: SFO installs completes but the application does not load and the system tray icon never shows up
ISSUE
Salesforce for Outlook (SFO) installation fails with the following error
 
    
or sometimes there are no error messages during the install but after you install SFO  there is no System Tray icon. If you try to manually run the application by double clicking on the Salesforce nothing happens. This mostly occurs if you have Office 365 or 2013 Click to run version
If you check the Windows Vista, 7, 8 or 8.1 
Control Panel\All Control Panel Items\Notification Area Icons,
 there are no icons installed there either
 
CAUSE
Most probably the 
.
NET Programmability Support
 
for Office is missing, not installed or if it is, it has been corrupted.
If you have 
Office 365
 or 
Click to Run 
version, most probably it was not installed. This type of installation usually downloads and installs the minimum resources needed for the user to run Office applications and the rest of the features are downloaded as needed up on first use.
 
Resolution
 
1- First uninstall SFO if you have already installed it and also make sure you have local administrative rights on your computer
2- Press and hold the Windows key on your keyboard and press the R key to open up the Run command box
3- Type in  
appwiz.cpl
 and click on OK
4- This is going to open the 
Control Panel\Programs\Programs and Features 
5- Locate your version of Microsoft Office 2013 and click on 
Change
.
Now if you have full versions of Microsoft Office such as 2010 or 2013 pro (any other versions but Office 365 or click to run version) you will see the 
Add or Remove Features 
option
 
If you have Office 365 or click to run version you won't have this option and instead you will have the repair option only. Either a quick repair or an Online Repair. Always choose the Online Repair
6- After step 5 above is completed, restart your machine and re-install SFO.
you can also use the 
Office Configuration Analyzer ool (OffCAT)
 to verify your Office installation
 
