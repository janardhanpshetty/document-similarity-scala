### Topic: Burst Sending allows for a more streamlined deployment of complex Email Sends to large quantities of subscribers.
Learn how Burst Sending allows for a more streamlined deployment of complex Email Sends to large quantities of subscribers. 
Resolution
Who uses Burst Sending?
Burst Sending is typically used by enterprise clients whose Email Sends target millions of subscribers at a time and are built with complex code for enhanced personalization. 
 
How does it work?
When an email contains personalized AMPScript code it must be evaluated on a per subscriber bases.  Burst Sending allows for a Send to be scheduled for the future, and while it's waiting on the send time to deploy, it builds all of the emails personalized for the subscribers and holds them on the machine.  At the time of send, all of the prebuilt emails are able to deploy at a faster rate than a normal Send because it bypasses the need to build the email at send time. 
For more information about Burst Sending or to request it be enabled in your account, please reach out to your Account Manager.
