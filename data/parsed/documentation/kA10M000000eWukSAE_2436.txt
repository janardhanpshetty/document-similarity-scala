### Topic: Collaborate Within Your Company | Salesforce
Collaborate Within Your Company | Salesforce
Collaborate Within Your Company
Stay current with what’s happening in your company. Share your knowledge in Chatter posts and comments, collaborate in groups, and access files and data across your organization.
Share Updates with People (Chatter)
Share updates with coworkers, collaborate in Chatter groups, and view record updates.
Create, Share, and Organize Files
Post files to feeds and records, share files with customers in the Salesforce cloud, and sync files between your local drive and Salesforce.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_within_company.htm&language=en_US
Release
202.14
