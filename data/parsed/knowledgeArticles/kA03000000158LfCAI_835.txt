### Topic: This contains the SQL process for determining whether or not a particular number texted STOP to a SharedShortCode.
If a client is on a shared short code they have their own stop message and when subscribers text this in it is written to ClientDB on SMSMOMessageResponse table. 
When they simply text STOP in though it is actually written to the "master account" that holds the STOP keyword which doesn't necessarily mean it is on the Client's DB. To find this you must query the short code across SystemDB in order to find what account this is.
Resolution
1. Login to Centro
2. Open SQL Server Management Studio
3. Connect to the ClientDB
4. Run the below SQL Statement updating the ShortCode:
SELECT sc.ShortCode, sc.SharedKeyword, sc.ClientId, m.DBID as [DB where the Master "ShortCode or LongCode" resides] 
FROM systemdbserver.systemdb.dbo.SmsMemberSharedShortCode sc with (nolock) 
JOIN members m with (nolock) on m.memberid= sc.clientid 
WHERE sc.SharedKeyword = 'STOP' ----Enter keyword 
AND sc.ShortCode = '80565' ----Enter ShortCode or LongCode
5. Find what DB the "master account" is on within the results.
6. Connect to the DB from step 5
7. Execute the following SQL:
SELECT * FROM SMSMOMessageResponse with (nolock) 
WHERE ClientID = 54708 --Replace with ClientID you found in last query 
AND MobileNumber = 17657207316 --Replace Mobile Number 
AND MessageRespondingTo like '%STOP%'
 
