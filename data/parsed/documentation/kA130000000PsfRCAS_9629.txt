### Topic: “View All” and “Modify All” Permissions Overview | Salesforce
“View All” and “Modify All” Permissions Overview | Salesforce
“View All” and “Modify All” Permissions Overview
The “View All” and “Modify All” permissions ignore sharing rules and settings, allowing administrators to grant access to records associated with a given object across the organization. “View All” and “Modify All” can be better alternatives to the “View All Data” and “Modify All Data” permissions.
Available in: Salesforce Classic
Available in: 
All
 Editions
Be aware of the following distinctions between the permission types.
Permissions
Used for
Users who Need them
View All
Modify All
Delegation of object permissions
Delegated administrators who manage records for specific objects
View All Data
Modify All Data
Managing all data in an organization; for example, data cleansing, deduplication, mass deletion, mass transferring, and managing record approvals
Administrators of an entire organization
View All Users
Viewing all users in the organization. Grants Read access to all users, so that you can see their user record details, see them in searches, list views, and so on.
Users who view all users in the organization, especially if the organization-wide default for the user object is Private. Administrators with the “Manage Users” permission are automatically granted the “View All Users” permission.
“View All” and “Modify All” are not available for ideas, price books, article types, and products.
“View All” and “Modify All” allow for delegation of object permissions only. To delegate user administration and custom object administration duties, 
define delegated administrators
.
“View All Users” is available if your organization has User Sharing, which controls user visibility in the organization. To learn about User Sharing, see 
User Sharing
.
See Also:
Object Permissions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=users_profiles_view_all_mod_all.htm&language=en_US
Release
202.14
