### Topic: This article should be used members of our internal Tier 2 Feature Activation team when there is a need to "Authenticated Website Profile" access issue that prevents Person Account Activation
This can be run by our internal Tier 2 Feature Activation team only.
These are the steps to resolve the issue outlined in the following KB:
INTERNAL: Person Accounts cannot be enabled because of "Authenticated Website" Profile
Background: This behavior occurs when the "Authenticated Website Profile" or "Overage Authenticated Website Profile" has Read access to the Accounts object. The behavior only affects organizations with no Profile Access (Professional Edition, Group Edition) as they can't remove the access. This access prevents us from enabling Person Accounts in BT and presents an error. 
We may see the same error in relation to the 
Overage Authenticated Website profile. 
 
Resolution
The following steps can be taken to remove Account Read Access for the Authenticated Website Profile:
Detach the file '
Authenticated_Website_Profile_Fix.zip
'** to a local folder. 
Get login access granted from a system administrator user in the target org.
Obtain a session id for the system administor. You get get this from the 'Session' link beside the user in the User list view via Blacktab.
Login to workbench (
https://workbench.internal.salesforce.com/login.php
) using the session id from #2. Double check that you are now logged into workbench the correct org and user from #3 (hover your mouse over the User/Org Name in the upper right corner).
​Log in via the Advanced section.
 While the login process does try to pull the appropriate login server based on specifics within the session ID, it is not always accurate. You may need to update the login URL to redirect it to the customer's server manually. To do so, simply remove  "login" from the URl and replace it with the server identifier (examples: na14, eu3, ap0, etc.) from which the customer's organization is located.
From options at the top of the page click on 'Migration | Deploy'.
Select the file from #1 (
Authenticated_Website_Profile_Fix.zip
).
Check the checkbox for 'Single Package' i.e. enable.
Click on 'Next', double check the file and settings, and then click on 'Deploy'.
After a short time, usually <1 minute, the deployment should complete. If successful, the results will show that 'profiles/Authenticated Website.profile' was changed (changed:true) under componentSuccesses. 
To verify, go back to the org and open the 'Authenticated Website' profile. Under 'Standard Object Permissions', all CRUD access to 'Account' should now be removed.
The deployment simply removes 'readAccess' from Account in the object permissions of the affected profile. With regards to granting CRUD access to Account for these profiles, since the license shouldn't have access to Accounts anyway, the customer should be advised that this is not possible (WAD). Person Accounts requires access to accounts and contacts, which is not permitted for profiles using that license, as documented here: 
https://help.salesforce.com/HTViewHelpDoc?id=users_license_types_platformportal.htm&language=en_US
**if the issue is with the 
Overage Authenticated Website profile, then use the Overage_Authenticated_Website.zip file. 
