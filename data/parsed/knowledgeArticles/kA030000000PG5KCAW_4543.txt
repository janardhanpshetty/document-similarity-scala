### Topic: Explanation of alignment issues between the "API Requests, Last 24 Hours" field and the "API Usage Last 7 Days" report.
Some of our customers have found that the "API Requests, Last 24 Hours" value displayed on the Company Profile does not align with what is displayed on the "API Usage Last 7 Days" report.
Resolution
There are two locations within the application in which you can view the calls consumed.
 
The first is located within the Company Information section within Setup. The figures are stored within the "API Requests, Last 24 Hours" field.
The second is through a standard report available in the standard Administrative Reports folder.
 
The value being tracked within the "API Requests, Last 24 Hours" field shows what has been consumed within the last 24 hours. This means that the calls consumed at 12:36pm will not be made available for use again until the following day. Any calls consumed will continue to be counted towards this count until that time. 
 
The "API Usage Last 7 Days" report displays based on the day the individual call is consumed.
 
While relaying similar information, these two sources should not be used in direct comparison of each other. For example. an API call consumed on Tuesday evening will still be contributing to the quantity of the "API Requests, Last 24 Hours" field until they are returned for use on Wednesday evening. That same call will be displayed as used on Tuesday with no impact on the volume consumed on Wednesday.
