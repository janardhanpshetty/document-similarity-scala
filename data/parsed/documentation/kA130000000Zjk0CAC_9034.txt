### Topic: Query Results Grid | Salesforce
Query Results Grid | Salesforce
Query Results Grid
The Query Results grid displays each record as a row. You can create, update, and delete records without leaving the Developer Console. For SOSL search results with multiple objects, each object is displayed on a separate tab.
To open a record in the results, click the row and click 
Open Detail Page
. To edit the record, click 
Edit Page
 to jump to the record in Salesforce.
To create a record, click 
Insert Row
. Enter the information and click 
Save Rows
.
Note
To insert a row, the query results must contain all the required fields for the object. The required fields must be simple text or number fields. If these conditions aren’t met, a blank row is created but you can’t save it. In this case, click 
Create New
 to create a record in Salesforce.
To edit a record within the Query Results grid, double-click the row. Make your changes and click 
Save Rows
.
To delete a record, select the related row and click 
Delete Row
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=code_dev_console_tab_query_editor_query_results_grid.htm&language=en_US
Release
202.14
