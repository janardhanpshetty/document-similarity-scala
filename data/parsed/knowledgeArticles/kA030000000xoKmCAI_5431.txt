### Topic: Workaround to have badge images display correctly.
You may notice that some badge images appear blurred when viewed through Badge Info popup.
Resolution
Badge images are stored as documents and resized to 128x128 pixels.  If you upload badge images of a different size they may appear blurry or pixelated.  For best results upload badge images that are at least 128x128.
