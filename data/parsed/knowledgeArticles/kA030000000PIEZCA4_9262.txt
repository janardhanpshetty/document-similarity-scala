### Topic: FeedItem.isAccessible() returns true even when Chatter is disabled
If the getGlobalDescribe() method is called from an installed managed package, it returns sObject names and Chatter specific entities such as NewsFeed and UserProfileFeed, even if Chatter is not enabled in the installing organization.   
We may see an error such as: 
 
"System.RequiredFeatureMissingException: Field 'AboutMe' on object 'User' is not supported"
Resolution
This is how the feature was originally spec'ed. This feature is owned by the packaging team, and the spec was written by Andrew Smith. 
This is done to allow partners to write code that references chatter entities and allows it to compile. That makes it easier to develop a single package that can run in orgs that both have and don't have chatter enabled.
The hack is to do a try-catch with a query to see if we get the following error: System.RequiredFeatureMissingException: Field 'FullPhotoUrl' on object 'User' is not supported.
Refer to the following GUS investigation  : https://gus.salesforce.com/a07B0000000LGeBIAW
 
