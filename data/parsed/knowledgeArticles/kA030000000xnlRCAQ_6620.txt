### Topic: This article shows how to set Chatter email notifications for multiple community users using Data Loader.
We would like to mass add users to a communities chatter group. We do not want emails going out to customers when mass adding them to the groups.
 
Resolution
 
Chatter email notifications can be toggled for community users by performing an update operation to the NetworkMember object using Data Loader.
 
1. Export the CSV file via Data Loader. Make sure that you include and select the following fields so they're included in your export file as columns on the spreadsheet:
 
- NETWORKMEMBERID
- NETWORKID (Community ID)
- MEMBERID (User ID)
- PREFERENCESDISABLEALLFEEDSEMAIL - Can only have the value of TRUE or FALSE. By setting the field's value to TRUE you are effectively performing the same action as deselecting the "Receive emails" preference via the community member's Chatter Email Settings.
2. Modify the file according to your preference. Some permissions can also be toggled by looking at the 
NetworkMember
 table.
3. Mass Update the NetworkMember table using Data Loader.
See Also:
Export Data
Insert, Update, or Delete Data Using Data Loader
Turning off all Chatter email notifications for users via the Data Loader
 
