### Topic: Formula Operators and Functions A–H | Salesforce
Formula Operators and Functions A–H | Salesforce
Formula Operators and Functions A–H
Use the following operators and functions when building formulas. Click on the name of the operator or function below to view more details. All functions are available everywhere that you can include a formula such as formula fields, validation rules, approval processes, and workflow rules, unless otherwise specified.
Note
Extraneous spaces in the samples below are ignored.
+ (Add)
Description:
Calculates the sum of two values.
Use
:
value1 + value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Formula Field Example
:
Amount + Maint_Amount__c + Services_Amount__c
This formula calculates the sum of the product 
Amount
, maintenance amount, and services fees. Note that 
Maint amount
 and 
Service Fees
 are custom currency fields.
Report Example
:
EMAIL_OPT_OUT:SUM + DO_NOT_CALL:SUM
 calculates all 
Email Opt Out
 fields plus all 
Do Not Call
 fields on the leads in your report. This formula is a number data type that returns a positive integer.
Validation Rule Example
:
You may have a custom object that allows users to track the total number of hours worked in a week. Use the following example to ensure that users cannot save a time card record with more than 40 hours in a work week.
Monday_Hours__c + 
Tuesday_Hours__c + 
Wednesday_Hours__c + 
Thursday_Hours__c + 
Friday_Hours__c > 40
Use a formula like this one in a validation rule to display the following error message when the total number of hours entered for each work day is greater than 40: “Your total hours cannot exceed 40.” This example requires five custom fields on your custom object, one for each day of work.
- (Subtract)
Description:
Calculates the difference of two values.
Use:
value1 - value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Example:
Amount - Discount_Amount__c
This formula calculates the difference of the product 
Amount
 less the 
Discount Amount
. Note that 
Discount Amount
 is a custom currency field.
Report Example:
AMOUNT:SUM - Product.Discount_Amount__c:SUM
 calculates the difference of all 
Amount
 fields and all 
Discounted Amount
 custom fields on the products in your report. This formula is a currency data type that returns a currency sign and decimal places.
* (Multiply)
Description:
Multiplies its values.
Use:
value1 * value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Example:
Consulting_Days__c * 1200
This formula calculates the number of consulting days times 1200 given that this formula field is a currency data type and consulting charges a rate of $1200 per day. Note that 
Consulting Days
 is a custom field.
Report Example:
RowCount * AGE:AVG
 calculates the record count times the average age value of your report. This formula is a number data type that returns a positive or negative integer or decimal. 
/ (Divide)
Description:
Divides its values.
Use:
value1 / value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Example:
AnnualRevenue/ NumberOfEmployees
This formula calculates the revenue amount per employee using a currency field.
IF(NumberOfOpportunities > 0, 
  NumberOfWonOpportunities / NumberOfOpportunities, null)
This formula calculates the win rate of opportunities on a campaign.
Report Example:
% Won Opportunities
WON:SUM / RowCount
 calculates the percent of 
Won
 opportunities using a record count representing the number of all opportunities in your report. This formula is a number data type that returns a positive or negative integer.
% Difference between Cost and Sales Price
(TOTAL_PRICE:SUM - QUANTITY:SUM * Product2.Cost__c:SUM) / (QUANTITY:SUM * Product2.Cost__c:SUM)
 calculates the average percent difference between what a product costs and its selling price on a product-by-product level. Note that 
Product2.Cost__c:SUM
 is a custom currency field named 
Cost
 on products, which includes the cost of each product. This formula is a percent data type that returns a positive or negative integer. For best results, use this on a summary Opportunities with Products report that is summarized by 
Product Name
 and includes summary totals for 
Quantity
, 
Total Price
, and 
Cost
.
^ (Exponentiation)
Description:
Raises a number to a power of a specified number.
Use:
number^integer
 and replace 
number
 with a merge field, expression, or another numeric value; replace 
integer
 with a merge field that contains an integer, expression, or any integer.
Example:
NumberOfEmployees^4
 calculates the number of employees to the 4th power.
Report Example:
ACTIVE:SUM ^ 2
 calculates the number of active Salesforce users to the 2nd power for administration. This formula is a number data type that returns a positive integer. 
Tips:
Avoid replacing 
integer
 with a negative number.
() (Open Parenthesis and Close Parenthesis)
Description:
Specifies that the expressions within the open parenthesis and close parenthesis are evaluated first. All other expressions are evaluated using standard operator precedence.
Use:
(expression1) expression2...
 and replace each 
expression
 with merge fields, expressions, or other numeric values.
Example:
(Unit_Value__c - Old_Value__c) / New_Value__c 
calculates the difference between the old value and new valuedivided by the new value.
Report Example:
(DURATIONHOURS:SUM * RowCount) / 24
 calculates the duration of all event times the record count per 24 hours. This formula is a percent data type that returns a positive or negative integer or decimal, representing what percent of a day is spent on events.
= and == (Equal)
Important
Don’t use this function for a null comparison, such as 
MyDateTime__c == null
. Use 
ISBLANK
 instead.
Description:
Evaluates if two values are equivalent. The = and == operator are interchangeable.
Use:
expression1=expression2
 or 
expression1 == expression2
, and replace each 
expression
 with merge fields, expressions, or other numeric values.
Example:
Due Date
Due Date = CreatedDate + 5
 returns true if the due date is equal to five days following a record’s created date.
Commission Amount
IF(Probability =1, ROUND(Amount*0.02, 2), 0)
This formula calculates the 2% commission amount of an opportunity that has a probability of 100%. All other opportunities will have a commission value of 0.
Possible results:
An opportunity with a 
Probability
 of 90% will have a commission of 0.
An opportunity with a 
Probability
 of 100% and an 
Amount
 of $100,000 will have a commission of $2,000.
<> and != (Not Equal)
Important
Don’t use this function for a null comparison, such as 
MyDateTime__c != null
. Use 
ISBLANK
 instead.
Description:
Evaluates if two values are not equivalent.
Use:
expression1 <> expression2
 or 
expression1 != expression2
, and replace each 
expression
 with merge fields, expressions, or other numeric values.
Example:
IF(Maint_Amount__c + Services_Amount__c<> Amount, 
 "DISCOUNTED", "FULL PRICE")
This formula displays “DISCOUNTED” on product if its maintenance amount and services amount do not equal the product amount. Otherwise, displays “FULL PRICE.” Note that this example uses two custom currency fields for 
Maint Amount
 and 
Services Amount
.
< (Less Than)
Description:
Evaluates if a value is less than the value that follows this symbol.
Use:
value1 < value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Example:
IF(AnnualRevenue < 1000000, 1, 2)
 assigns the value “1” with revenues less than one million and the value “2” to revenues greater than one million.
> (Greater Than)
Description:
Evaluates if a value is greater than the value that follows this symbol.
Use:
value1 > value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Example:
IF(commission__c > 1000000, "High Net Worth", "General")
 assigns the “High Net Worth” value to a commission greater than one million. Note, this is a text formula field that uses a commission custom field.
<= (Less Than or Equal)
Description:
Evaluates if a value is less than or equal to the value that follows this symbol.
Use:
value1 <= value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Example:
IF(AnnualRevenue <= 1000000, 1, 2)
 assigns the value “1” with revenues less than or equal to one million and the value “2” with revenues greater than one million.
>= (Greater Than or Equal)
Description:
Evaluates if a value is greater than or equal to the value that follows this symbol.
Use:
value1 >= value2
 and replace each 
value
 with merge fields, expressions, or other numeric values.
Example:
IF(Commission__c >= 1000000, "YES", "NO")
 assigns the “YES” value with a commission greater than or equal to one million. Note, this is a text formula field that uses a custom currency field called 
Commission
.
&& (AND)
Description:
Evaluates if two values or expressions are both true. Use this operator as an alternative to the logical function AND.
Use:
(
logical1
) && (
logical2
)
 and replace 
logical1
 and 
logical2
 with the values or expressions that you want evaluated.
Example:
IF((Price<100 && Quantity<5),"Small", null)
This formula displays “Small” if the price is less than 100 and quantity is less than five. Otherwise, this field is blank.
|| (OR)
Description:
Evaluates if at least one of multiple values or expressions is true. Use this operator as an alternative to the logical function OR.
Use:
(
logical1
) || (
logical2
)
 and replace any number of logical references with the values or expressions you want evaluated.
Example:
IF((ISPICKVAL(Priority, "High")) || (ISPICKVAL(Status , "New")), ROUND(NOW()-CreatedDate, 0), null)
This formula returns the number of days a case has been open if the 
Status
 is new or the 
Priority
 is high. If the case was opened today, this field displays a zero.
Validation Rule Example:
(Discount_Rate__c < 0) || (Discount_Rate__c > 0.40)
This validation rule formula displays the following error message when the 
Discount Rate
 custom field is not between 0 and 40%: "Discount Rate cannot exceed 40%."
& (Concatenate)
Description:
Connects two or more strings.
Use:
string1&string2
 and replace each 
string
 with merge fields, expressions, or other values.
Example:
"Expense-" & Trip_Name__c & "-" & ExpenseNum__c
This formula displays the text “Expense-” followed by trip name and the expense number. This is a text formula field that uses an expense number custom field.
ABS
Description:
Calculates the absolute value of a number. The absolute value of a number is the number without its positive or negative sign.
Use:
ABS(number)
 and replace 
number
 with a merge field, expression, or other numeric value that has the sign you want removed.
Example:
ABS(ExpectedRevenue)
 calculates the positive value of the 
Expected Revenue
 amount regardless of whether it is positive or negative.
AND
Description:
Returns a TRUE response if all values are true; returns a FALSE response if one or more values are false.
 Use this function as an alternative to the operator 
&& (AND)
.
Use:
AND(logical1,logical2,...)
 and replace 
logical1,logical2,...
 with the values that you want evaluated.
Formula Field Example:
IF(AND(Price<1,Quantity<1),"Small", null)
This formula displays “Small” if the price and quantity are less than one. This field is blank if the asset has a price or quantity greater than one.
BEGINS
Description:
Determines if text begins with specific characters and returns TRUE if it does. Returns FALSE if it does not.
Use:
BEGINS(
text
, compare_text)
 and replace 
text, compare_text
 with the characters or fields you want to compare.
Example:
IF(BEGINS (Product_type__c, "ICU"), "Medical", "Technical")
This example returns the text “Medical” if the text in any 
Product Type
 custom text field begins with “ICU.” For all other products, it displays “Technical.”
Tips:
This function is case sensitive so be sure your 
compare_text
 value has the correct capitalization.
When using this function in a validation rule or workflow rule, fields that are blank are considered valid. For example, if you have a validation rule that tests to see if the serial number of an asset begins with “3,” all assets that have a blank serial number are considered valid.
BLANKVALUE
Description:
Determines if an expression has a value and returns a substitute expression if it does not. If the expression has a value, returns the value of the expression.
Use:
BLANKVALUE(
expression
, 
substitute_expression
)
 and replace 
expression
 with the expression you want evaluated; replace 
substitute_expression
 with the value you want to replace any blank values.
Example:
Example 1
BLANKVALUE(Department, “Undesignated”)
This formula returns the value of the 
Department
 field if the 
Department
 field contains a value. If the 
Department
 field is empty, this formula returns the word 
Undesignated
.
Example 2
(BLANKVALUE(Payment_Due_Date__c, StartDate +5)
This formula returns the date five days after the contract start date whenever 
Payment Due Date
 is blank. 
Payment Due Date
 is a custom date field.
Tips:
Use BLANKVALUE instead of NULLVALUE in new formulas. BLANKVALUE has the same functionality as NULLVALUE, but also supports text fields. Salesforce will continue to support NULLVALUE, so you do not need to change existing formulas.
A field is not empty if it contains a character, blank space, or zero. For example, a field that contains a space inserted with the spacebar is not empty.
Use the 
BLANKVALUE
 function to return a specified string if the field does not have a value; use the 
ISBLANK
 function if you only want to check if the field has a value.
If you use this function with a numeric field, the function only returns the specified string if the field does not have a value and is not configured to treat blank fields as zeroes.
BR
Description:
Inserts a line break in a string of text.
Use:
BR()
Example:
CASE(ShippingCountry, 
"USA", 
  ShippingStreet & BR() &
  ShippingCity & ", 
  " & ShippingState & " " & 
  ShippingPostalCode & BR() 
  & ShippingCountry, 
"France", 
  ShippingStreet & BR() & 
  ShippingPostalCode & " " & 
  ShippingCity & BR() & 
  ShippingCountry, "etc")
This formula field displays a formatted mailing address for a contact in standard format, including spaces and line breaks where appropriate depending on the country.
Tips
:
Do not remove the parentheses after the function name.
Keep the parentheses empty. They do not need to contain a value.
Remember to surround the BR() with concatenation operators: &.
Avoid using this function in mail merge templates.
This function is not available in custom buttons and links, s-controls, or reports.
CASE
Description:
Checks a given expression against a series of values. If the expression is equal to a value, returns the corresponding result. If it is not equal to any values, it returns the 
else_result
.
Use:
CASE(
expression
,​
value1
, 
result1
, 
value2
,​ 
result2
,...,​ 
else_result
)
 and replace 
expression
 with the field or value you want compared to each specified value. Replace each value and result with the value that must be equivalent to return the result entry. Replace 
else_result
 with the value you want returned when the expression does not equal any values.
Formula Field Example:
Days Open for Cases
Use this example of a custom formula field called 
Days Open
 to display different text depending on the number of days a case has been open:
CASE(Days_Open__c, 3, 
  "Reassign", 2, "Assign Task", "Maintain")
The following text is displayed:
“Reassign” for any case open three days.
“Assign Task” for any case open two days.
“Maintain” for all other cases.
Last Activity Month
This formula field displays the month of the last activity or “None” if there are no activities.
CASE(MONTH(LastActivityDate),
1, "January",
2, "February",
3, "March", 
4, "April", 
5, "May", 
6, "June",
7, "July",
8, "August",
9, "September",
10, "October",
11, "November",
12, "December",
"None")
Default Value Example
:
Discount Rate
Use the following default value formula to insert a different discount rate on an opportunity based on the department of the person creating the opportunity:
CASE(User.Department,​​ "IT", 0.25, "Field", 0.15, 0)
In this example, the formula inserts a discount rate of 25% on any opportunity created by a user in the “IT” department or 15% on any opportunity created by someone in the “Field” department. A zero is applied if the creator does not belong to either of these departments. This is a custom percent field on opportunities that uses the standard user field 
Department
.
Product Language
You may want to associate a product with its language so that your users know the type of documentation or adapter to include. Use the following default value formula to automatically set the language of a product based on the country of the user creating the product. In this example, the default value is “Japanese” if the user's country is “Japan” and “English” if the user's country is “US.” If neither is true, the default value “unknown” is inserted into the 
Product Language
 field.
CASE($User.Country ,​​ "Japan", "Japanese", "US", "English","unknown")
Tips:
Be sure your 
value1
, 
value2
... expressions are the same data type.
Be sure your 
result1
, 
result2
... expressions are the same data type.
CASE functions cannot contain functions that return true or false. Instead, make true or false expressions return numbers such as:
CASE(1, IF(ISPICKVAL​ (Term__c, "12"),​ 1, 0),
 12 * Monthly_Commit__c,​​
 IF(ISPICKVAL(Term__c, "24"), 1, 0),​​
 24 * Monthly_Commit__c, 0)
In this formula, 
Term
 is a picklist field that is multiplied by the Monthly Commit whenever it contains the value 1 for true.
The 
else_result
 value is required.
CASE functions return an error whenever any of the expressions return an error, regardless of which one should be returned. For example, 
CASE(Field__c,"Partner", "P",​ "Customer", "C", LEFT(Field__c, -5))
 returns an error even if the value of the field is “Partner” or “Customer” because the last statement is illogical.
If the field in your CASE function is blank, it returns your 
else_result
 value. For example, this formula: 
CASE(Days_Open__c, 3, "Reassign", 2,​ "Assign Task", "Maintain")
 displays “Maintain” if the 
Days Open
 field is blank, 0, or any value other than 2 or 3.
Use CASE functions to determine if a picklist value is equal to a particular value. For example the formula 
CASE(Term__c, "12", 12 * Monthly_Commit__c, "24",​ 24 * Monthly_Commit__c, 0)
 multiplies the 
Monthly Commit
 amount by 12 whenever the 
Term
 is 12 or multiplies the 
Monthly Commit
 amount by 24 whenever the 
Term
 is 24. Otherwise, the result is zero.
CASESAFEID
Description:
Converts a 15-character ID to a case-insensitive 18-character ID.
Use:
CASESAFEID(
id
)
 and replace 
id
 with the object’s ID.
Example:
CASESAFEID (Id)
This formula replaces the 15-character ID with the 18-character, case-insensitive ID.
Tips:
Convert to 18-character IDs for better compatibility with Excel.
The CASESAFEID function is available everywhere that you can define a formula except reports and s-controls.
CEILING
Description:
Rounds a number up to the nearest integer.
Use:
CEILING(
number
)
 and replace 
number
 with the field or expression you want rounded.
Example:
Rounding Up (literal value)
CEILING(2.5)
This formula returns 3, which is 2.5 rounded up to the nearest number.
Earthquake Magnitude
CEILING(Magnitude__c)
 returns the value of a formula number field that calculates the magnitude of an earthquake up to the nearest integer.
CONTAINS
Description:
Compares two arguments of text and returns TRUE if the first argument contains the second argument. If not, returns FALSE.
Use:
CONTAINS(
text
, 
compare_text
)
 and replace 
text
 with the text that contains the value of 
compare_text
.
Example:
IF(CONTAINS(Product_Type__c, "part"), "Parts", "Service")
This formula checks the content of a custom text field named 
Product_Type
 and returns “Parts” for any product with the word “part” in it. Otherwise, it returns “Service.” Note that the values are case sensitive, so if a
 Product_Type
 field contains the text “Part” or “PART,” this formula returns “Services.”
Tips:
This function is case sensitive so be sure your 
compare_text
 value has the correct capitalization.
When using this function in a validation rule or workflow rule, fields that are blank are considered valid. For example, if you have a validation rule that tests to see if the serial number of an asset contains “A,” all assets that have a blank serial number are considered valid.
The CONTAINS function does not support multi-select picklists. Use 
INCLUDES
 to see if a multi-select picklist has a specific value.
DATE
Description:
Returns a date value from year, month, and day values you enter. Salesforce displays an error on the detail page if the value of the DATE function in a formula field is an invalid date, such as February 29 in a non-leap year.
Use:
DATE
(year,month,day)
 and replace 
year
 with a four-digit year, 
month
 with a two-digit month, and 
day
 with a two-digit day.
Example:
DATE(2005, 01, 02)
 creates a date field of January 2, 2005.
DATEVALUE
Description:
Returns a date value for a date/time or text expression.
Use:
DATEVALUE(
expression
)
 and replace 
expression
 with a date/time or text value, merge field, or expression.
Example:
Closed Date
DATEVALUE(ClosedDate)
 displays a date field based on the value of the 
Date/Time Closed
 field.
Date Value
DATEVALUE("2005-11-15")
 returns November 15, 2005 as a date value.
Tips:
If the field referenced in the function is not a valid text or date/time field, the formula field displays #ERROR!
When entering a date, surround the date with quotes and use the following format: YYYY-MM-DD, that is, a four-digit year, two-digit month, and two-digit day.
If the 
expression
 does not match valid date ranges, such as the MM is not between 01 and 12, the formula field displays #ERROR!
Dates and times are always calculated using the user’s time zone.
DATETIMEVALUE
Description:
Returns a year, month, day and GMT time value.
Use:
DATETIMEVALUE(
expression
)
 and replace 
expression
 with a date/time or text value, merge field, or expression.
Example:
Closed Date
DATETIMEVALUE(ClosedDate)
 displays a date field based on the value of the 
Date/Time Closed
 field.
Date Value
DATETIMEVALUE("2005-11-15 17:00:00")
 returns November 15, 2005 5:00 PM GMT as a date and time value.
Tips:
DATETIMEVALUE is always calculated using GMT time zone and can’t be changed.
When entering a specific date, surround the date with quotes and use the following format: YYYY-MM-DD, that is, a four-digit year, two-digit month, and two-digit day.
If the 
expression
 does not match valid date ranges, such as the MM is not between 01 and 12, the formula field displays #ERROR!
DAY
Description:
Returns a day of the month in the form of a number between 1 and 31.
Use:
DAY(
date
)
 and replace 
date
 with a date field or value such as 
TODAY()
.
Example:
DAY(Code_Freeze__c)
 returns the day in your custom code freeze date. Note this does not work on date/time fields.
DISTANCE
Description:
Calculates the distance between two locations in miles or kilometers.
Use:
DISTANCE(
mylocation1
, 
mylocation2
, 'unit')
 and replace 
mylocation1
 and 
mylocation2
 with two location fields, or a location field and a value returned by the 
GEOLOCATION
 function. Replace 
unit
 with mi (miles) or km (kilometers).
Examples:
Distance between two geolocation fields
DISTANCE(warehouse_location__c, store_location__c, 'mi')
This formula returns the distance, in miles, between the warehouse and the store. In this example, 
warehouse_location__c
 and 
store_location__c
 are the names of two custom geolocation fields.
Distance between an address field and a geolocation field
DISTANCE(BillingAddress, store_location__c, 'mi')
This formula returns the distance, in miles, between an account’s billing address and a store. In this example, 
BillingAddress
 is the standard billing address field on an Account object, and 
store_location__c
 is the name of a custom geolocation field.
Distance between a custom geolocation field and fixed coordinates
DISTANCE(warehouse_location__c, GEOLOCATION(37.775,-122.418), 'km')
This formula returns the distance, in kilometers, between the warehouse and the known latitude and longitude 37.775°, -122.418° (San Francisco).
Distances with conditions
IF(DISTANCE(warehouse_location__c, ShippingAddress, 'mi')<10, "Near", "Far")
This formula updates a text formula field to 
Near
 if the distance between the warehouse and the account shipping address compound field is less than 10 miles. Otherwise, it updates the text field to 
Far
.
Tip
Although DISTANCE can be calculated in miles or kilometers, the unit is not returned in the calculation. If possible, include the unit of measure in the name of your distance formula field, so users know whether the distance is in miles or kilometers.
Tips:
The DISTANCE function returns a number data type. Distance is always calculated in decimals, even if you’re displaying the geolocation notation in degrees, minutes, and seconds in the user interface. Specify the number of decimal places to show when you create a custom field.
The DISTANCE function isn’t available in reports, but it can be used in list views. To use DISTANCE in your reports, set up a formula field, and then reference the field in your reports.
DISTANCE is the only formula function that can use 
GEOLOCATION
 parameters.
There are limitations on DISTANCE accuracy and equality calculations.
DISTANCE supports only the logical operators > and <, returning values within (<) or beyond (>) a specified radius.
Distance is calculated as a straight line—as the crow flies—regardless of geography and topography between the two points.
For additional details, see “
How SOQL Calculates and Compares Distances
” in the 
Force.com SOQL and SOSL Reference
.
EXP
Description:
Returns a value for e raised to the power of a number you specify.
Use:
EXP(
number
)
 and replace 
number
 with a number field or value such as 5.
Example:
Exponent of a Literal Value
EXP(3)
This formula returns the value of e to the third power.
Compound Interest
Principal__c * EXP(Rate__c * Years__c)
This formula calculates the compound interest based on a custom currency field for principal, custom percent field for rate, and custom number field for years.
FIND
Description:
Returns the position of a string within a string of text represented as a number.
Use:
FIND(
search_text
, 
text
[, 
start_num
])
 and replace 
search_text
 with the string you want to find, replace 
text
 with the field or expression you want to search, and replace 
start_num
 with the number of the character from which to start searching from left to right.
Example:
Street Address
FIND(" ", Street)
 returns the character position of the first space in the 
Street
 field. You can use this number to find out the length of the street address as a means of separating a street address from street name in an address field.
Deriving Website Addresses
SUBSTITUTE(Email, LEFT(Email, FIND("@", Email)), "www.")
 finds the location of the @ sign in a person's email address to determine the length of text to replace with a “www.” as a means of deriving their website address.
Tips:
Be sure to remove the brackets, [ and ], from your formula before validating it.
If the field referenced in your 
text
 parameter is blank, the formula field displays 0.
Your 
search_text
 parameter is case sensitive and cannot contain any wildcard characters.
If your search does not return any results, a 0 displays in the field.
The 
start_num
 parameter is optional. If you do not enter a 
start_num
 value, the formula uses the value one, or the first character in the string.
If your 
start_num
 is not greater than zero, a 0 displays in the field.
If your 
start_num
 is greater than the length of the text, a 0 displays in the field.
When entering your 
start_num
 parameter, remember that some fields like the 
Website
 field are unique because a “http://” is automatically appended to the beginning of the text you enter.
Note that the first character in a string is designated as one rather than zero.
FLOOR
Description:
Returns a number rounded down to the nearest integer.
Use:
FLOOR(
number
)
 and replace 
number
 with a number field or value such as 5.245.
Example:
Commission Amounts
FLOOR(commission__c)
 rounds commission down to the nearest integer.
GEOLOCATION
Description:
Returns a geolocation based on the provided latitude and longitude. Must be used with the DISTANCE function.
Use:
GEOLOCATION(
latitude
, 
longitude
)
 and replace 
latitude
 and 
longitude
 with the corresponding geolocation, numerical code values.
Examples:
Distance between a custom geolocation field and fixed coordinates
DISTANCE(warehouse_location__c, GEOLOCATION(37.775,-122.418), 'km')
This formula returns the distance, in kilometers, between the warehouse and the known latitude and longitude 37.775°, -122.418° (San Francisco).
Tips:
The GEOLOCATION function returns a location data type that can be used only by, and must be used with, the 
DISTANCE
 function. The GEOLOCATION function doesn’t work on its own.
GETRECORDIDS
Description:
Returns an array of strings in the form of record IDs for the selected records in a list, such as a list view or related list.
Use:
{!GETRECORDIDS(
object_type
)}
 and replace 
object_type
 with a reference to the custom or standard object for the records you want to retrieve.
Custom Button Example:
{!REQUIRESCRIPT ("/soap/ajax/13.0/connection.js")} var records =
                  {!GETRECORDIDS($ObjectType.Sample)}; var newRecords = []; if (records[0] == null) { alert("Please select at least one row") } else {
                  for (var n=0; n<records.length; n++) { var c = new sforce.SObject("Case"); c.id = records[n]; c.Status = "New";
                  newRecords.push(c); } result = sforce.connection.update(newRecords); window.location.reload(); }
In this example, all selected case records are updated with a 
Status
 of “New.” To set this up in your organization, create a custom list button for cases with the following attributes:
Display Type
 is “List Button”
Behavior
 is “Execute JavaScript”
Content Source
 is “OnClick JavaScript”
Paste the sample code above into the content of your custom button. Finally, add the list button to the a page layout that contains the Cases related list, such as accounts or opportunities. Users can select any number of cases in the related list and click the list button to change the status of those cases at once. Notice the check for 
records[0] == null
, which displays a message to users when they do not select at least one record in the list.
Tips:
Use global variables to access special merge fields for s-controls, custom buttons, and links.
Activities are special types of objects. Use {!GETRECORDIDS($ObjectType.Task)} when creating a task list button. Use {!GETRECORDIDS($ObjectType.Event)} when creating an event list button.
This function is only available in custom buttons, links, and s-controls.
GETSESSIONID
Description:
Returns the user’s session ID.
Use:
GETSESSIONID()
 
Example:
HYPERLINK
("https://www.myintegration.com?sId="&
GETSESSIONID() & "?&rowID="&Name & "action=CreateTask","Create
a Meeting Request")
creates a link to an application outside of Salesforce, passing the parameters so that it can connect to Salesforce via the API and create the necessary event.
Tips:
Important
$Api.Session_ID
 and 
GETSESSIONID()
 return the same value, an identifier for the current session in the current context. This context varies depending on where the global variable or function is evaluated. For example, if you use either in a custom formula field, and that field is displayed on a standard page layout in Salesforce Classic, the referenced session will be a basic Salesforce session. That same field (or the underlying variable or formula result), when used in a Visualforce page, references a Visualforce session instead.
Session contexts are based on the domain of the request. That is, the session context changes whenever you cross a hostname boundary, such as from 
.salesforce.com
 to 
.visual.force.com
 or 
.lightning.force.com
.
Session identifiers from different contexts, and the sessions themselves, are different. When you transition between contexts, the old session is replaced by the new one, and the old session is no longer valid. The session ID also changes at this time.
Normally Salesforce transparently handles session hand-off between contexts, but if you’re passing the session ID around yourself, be aware that you might need to re-access 
$Api.Session_ID
 or 
GETSESSIONID()
 from the new context to ensure a valid session ID.
Note also that not all sessions are created equal. In particular, sessions obtained in a Lightning Experience context have reduced privileges, and don't have API access. You can't use these session IDs to make API calls.
HTMLENCODE
Description:
Encodes text and merge field values for use in HTML by replacing characters that are reserved in HTML, such as the greater-than sign (>), with HTML entity equivalents, such as 
&gt;
.
Use:
{!HTMLENCODE(
text
)}
 and replace 
text
 with the merge field or text string that contains the reserved characters.
Example:
If the merge field 
foo__c
 contains 
<B>Enter the user's name<b>
, 
{!HTMLENCODE(foo__c)}
 results in: 
&lt;B&gt;Enter the user&#39;s name&lt;/b&gt;
Tips:
This function is only available in custom buttons and links, and in Visualforce.
HYPERLINK
Description:
Creates a link to a URL specified that is linkable from the text specified.
Use:
HYPERLINK(
url
, 
friendly_name
 [,
target
])
 and replace 
url
 with the Web address, replace 
friendly_name
 with the link text, and, optionally, replace 
target
 with the window or frame in which to display the content.
Example:
Creating Events
HYPERLINK("00U/e?
retURL=%2F006x0000001T8Om&what_id="
& Id, 
"Create Event")
adds a link called “Create Event” that, when clicked, creates a new event that is associated with the current object.
Phone Dialer
HYPERLINK("http://servername/call?id=" & Id & "&phone=" & Phone, Phone)
creates a linkable phone number field that automatically dials the phone number when clicked. In this example, replace 
"servername"
 and 
"call"
 with the name of your dialing tool and the command it uses to dial. The merge field, 
Id
, inserts the identifier for the contact, lead, or account record. The first 
Phone
 merge field tells the dialing tool what number to call and the last 
Phone
 merge field uses the value of the 
Phone
 field as the linkable text the user clicks to dial.
Tips:
Hyperlink formula fields are of type text.
Include the protocol and URL in quotes as in 
HYPERLINK("http://www.cnet.com", "cnet")
.
Avoid using text functions such as LEN, LEFT, or RIGHT on HYPERLINK function results.
When linking to Salesforce pages, use a relative link, such as “00U/e?retURL=%...”, for hyperlink formulas unless you want to add the formula field to a search layout. Use the complete URL, including the server name and https://, in a hyperlink formula to add it to a search layout. Note that formula fields are not available in search result layouts.
Use the 
$Api
 variable to reference API URLs.
Be sure to remove the brackets, [ and ], from your formula before validating it.
The 
target
 parameter is optional. If you do not specify a 
target
, the link opens in a new browser window. Some common 
target
 parameters are:
_blank
Displays link in a new unnamed window.
_self
Displays link in the same frame or window as the element that refers to it.
_parent
Displays link in the immediate frameset parent of the current frame. This value is the same as _self if the current frame has no parent.
_top
Displays link in the full original window, canceling any other frames. This value is the same as _self if the current frame has no parent.
For more information on basic HTML tags, consult an HTML reference on the Internet.
The HYPERLINK function is available everywhere that you can define a formula except default values, field updates, s-controls, validation rules, approval processes, custom buttons and links, and workflow rules.
See Also:
Formula Operators and Functions I–Z
Formula Operators and Functions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_functions_a_h.htm&language=en_US
Release
202.14
