### Topic: Calculate the difference between 2 Date/Time data type fields, where the output would be Days/Hours/Mins."
To calculate the difference between 2 date/time data type fields, where the output would be xxDays/xxHours/xxmins
Example: Considering 2 date/time data type fields such as created date and Due Date, we would want to know the duration as to when a record would be due in terms of days/hours/minutes such as 
02days 03hours 24 min
Resolution
We would first create a custom field called “Due Date” whose data type would be Date/time,
Setup>customize>accounts>fields>custom fields>new>date/time
We would then create a custom formula field with data type “number” and name “d2subd1” that would have the following syntax
Setup>customize>accounts>fields>custom fields>new>formula>number
Created.date – due_date__c
Note: 
when using the data type of date/time in custom formula fields, we would only have the option to choose the data type as number for the output; else we would encounter an error while checking syntax
Now we create another Custom formula field but this time we choose the data type as text for the output.
We would create the following syntax
Setup>customize>accounts>fields>custom fields>new>formula>text
IF (d2subd1__c > 0,
 
TEXT(FLOOR(d2subd1__c)) & " days " & 
TEXT( FLOOR( 24 * (d2subd1__c - FLOOR(d2subd1__c) ))) & " hours" & 
TEXT( ROUND(60 * (ROUND( 24 * (d2subd1__c - FLOOR(d2subd1__c) ),8) - FLOOR( 
ROUND( 24 * (d2subd1__c - FLOOR(d2subd1__c) ),8)) ),0)) & " min " 
, "")
Where with the help of the advanced formula editor we insert the merge field for the formula we created in the previous step namely“d2subd1__c”
The output for the above formula on the detail page would look something like this,
02days 03hours 24 min
Now we would not want the custom formula field “d2subd1__c” to be visible on the detail page of the page layout, so we can opt to hide it via Field level security, in this way we would have just one custom formula field that would derive the output.
Setup>Security controls>Field accessibility>account>view by fields> 
