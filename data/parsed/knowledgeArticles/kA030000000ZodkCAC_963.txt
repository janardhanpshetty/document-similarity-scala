### Topic: Can we can add hyperlinks and also have some text in red / bold or italics in live agent auto greeting? Answer is No, it is not currently supported. You may only put plain text in it, no HTML so HTML tags like <b> <font> <i> <href> etc. won’t work. Full URL e.g. http://www.salesforce.com will automatically recognize as hyperlink in chat greeting.
Can we can add hyperlinks and also have some text in red / bold or italics in live agent auto greeting?
Resolution
No, it is not currently supported. You may only put plain text in it, no HTML so HTML tags like <b> <font> <i> <href> etc. won’t work. Full URL e.g. http://www.salesforce.com will automatically recognize as hyperlink in chat greeting.
