### Topic: Join us in a Circles of Success best practices discussion along with a number of other Salesforce customers in a moderated interactive discussion on strategies to own and manage your company’s data. Ask questions, share ideas, discuss, and engage – that’s the blueprint for a successful best practice discussion!
Become a Data Management Rockstar
 
 
Are you struggling to maintain the cleanliness and completeness of your data in Salesforce? Are you looking for best practices for using Salesforce functionality to manage your data effectively? 
 
Join us in a Circles of Success best practices discussion along witha number of otherSalesforce customers in a moderated interactive discussion on strategies to own and manage your company’s data. Ask questions, share ideas, discuss, and engage – that’s the blueprint for a successful best practice discussion! 
 
Some of the key topics we will cover include:
Importance of naming standards
Top data validation rules for keeping data clean
Sharing model implications on data management
Employing the right data management tools
Using built-in functionality to automatically de-dupe and keep your data clean
Effectively implementing data governance
Target Audience
Business leaders with intermediate level knowledge of Salesforce administration/configuration.
Learning Objectives
You will walk out of this Circles of Success best practice discussion with the ability to make decisions on:
Strategies for ensuring that data is entered properly into Salesforce
Best Practices around how to clean and manage your data in Salesforce
Resources you can leverage to take advantage of Salesforce built in data management capabilities
Recommended apps from the Salesforce AppExchange
Pre-Requisites
Access to a PC or Mac with ability to install 
GoToWebinar
Success Community Topics:
Any discussions/collaboration on the Salesforce Success Community relevant to this Roundtable will be tagged with the following Topics.
#CoS
#CirclesOfSuccess
#DataManagement
#SMB
 
For more resources check out the 
Achieve More Hub: Improve Data Quality
 
 
Resolution
