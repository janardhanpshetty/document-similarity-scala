### Topic: This article references questions about how Chatter desktop authenticates.
What are the common chatter desktop installation questions?
Resolution
How does Chatter Desktop authenticate?
 
Chatter Desktop authenticates with Salesforce using Oauth2.  This allows a persistent login to Salesforce removing the need for the application to reestablish its session at timed intervals. Chatter Desktop also works natively with delegated authentication in the same way the Salesforce application does.
 
Where do I navigate to install Chatter Desktop (Install Badge Location)?
 
You can find 2 install links (Install Badge locations):
1. “Get Chatter Desktop” in the “What to do next” box in the Chatter tab
2. Navigate to
 Setup | Personal Setup | Desktop Integration | Chatter Desktop
*If you are using the Improved Setup User Interface, please click on
 
Name | My Settings | Desktop Add Ons | Chatter Desktop
If I need to find Chatter Desktop logs files where are they located?
Mac:
    /[USER]/Library/Preferences/sfdc-desktop*/Local Store/application.log
Windows Vista and Windows 7:
    C:\Users\<USER>\AppData\Roaming\sfdc-desktop*\Local Store\application.log
Windows XP:
    C:\Documents and Settings\<USER>\Application Data\sfdc-desktop*\Local Store\application.log
 
What is the 
Shared Local Object (SLO)
 and where is it located?
The shared local object stores persistent information for a Chatter Desktop installation, e.g. last screen position, size, etc.
 
Mac
    /[USER]/Library/Preferences/sfdc-desktop*
 
Windows Vista, Windows 7, Windows 8.x:
    C:\Users\<USER>\AppData\Roaming\sfdc-desktop*
Windows XP:
    C:\Documents and Settings\<USER>\Application Data\sfdc-desktop*
 
What is the Encrypted Local Store (ELS) and where is it located?
 
The encrypted local store holds the Oauth2.0 identities for a user
Mac:
    /[USER]/Library/Application Support/Adobe/AIR/ELS/sfdc-desktop*
Windows Vista, Windows 7, Windows 8.x:
    C:\Users\<USER>\AppData\Roaming\Adobe\AIR\ELS\sfdc-desktop*
Windows XP:
    C:\Documents and Settings\<USER>\Application Data\Adobe\AIR\ELS\sfdc-desktop*
 
 
Install Errors
?
What do I do if I get the install error below?
"This application cannot be installed because this installer has been misconfigured. Please contact the application author for assistance."
Resolution 1:
 Try to install from one of the 
Install locations previously mentioned
Resolution 2
: Delete the 
Shared Local Object (SLO)
 (location mentioned above) and  the 
Encrypted Local Store (ELS)
 
directories and try to install from Install Badge again
Resolution 3:
 Uninstall Adobe AIR Runtime (Use “Add/Remove Programs” or "Uninstall programs" in Windows, or Run “Adobe AIR Uninstaller” app on Mac) and install from Install Badge again
 
 
Connection or Authentication errors?
 
If you have installed Chatter Desktop and are getting the error: "
To access your Chatter data, authorize this application in Salesforce.com. Cannot access Salesforce.com Verify your computer is online
" when clicking the authenticate button please ensure the application has access to the internet and Salesforce.com. Also check to ensure the machine the application is installed on has access to *.salesforce.com on port 443 and web access to Verisign.com for regular Certificate authentication. 
Double check any local proxy or firewall settings on the LAN.
 
