### Topic: This article explains how to create a formula field to display the Account Address field on the Contact Record
Making information easy to access can streamline processes and help increase productivity. We'll show you how System Admins can make Account information available on the Contact object. 
 
Resolution
Create a formula field
 
1. Click 
Setup
.
2. Click 
App Setup
  | then click 
Customize
 | click 
Contacts
 | click 
Fields
.
3. Click 
New
.
4. Select
 Formula
.
5. Enter your formula syntax.
For the Account Billing Address use this formula syntax:
  
Account.BillingStreet & BR() &
Account.BillingCity & ", " &
Account.BillingState & " " &
Account.BillingPostalCode & BR() &
Account.BillingCountry
 
For the Account Shipping Address use this formula syntax:
Account.ShippingStreet & BR() &
Account.ShippingCity & ", " &
Account.ShippingState & " " &
Account.ShippingPostalCode & BR() &
Account.ShippingCountry
 
6. Click Next.
7. Add the desired field level security and page layouts in steps 4 and 5 respectively.
Heads up
 - The fields Account.BillingAddress and Account.ShippingAddress can't be referenced in formulas as those fields are Location Type fields.
