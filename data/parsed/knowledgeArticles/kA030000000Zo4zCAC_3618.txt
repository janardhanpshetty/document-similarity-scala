### Topic: With this new feature in Summer'15, you can view query plans for your reports and list views in the Developer Console’s Query Editor. If you want to discover why your slow-to-run reports and list views are taking long time to load, then this feature will help you.
Resolution
1. Find the ID of your report or list view in its URL.
2. To enable the Query Plan button in the Query Editor, click Help > Preferences, set       Enable Query Plan to true, and then click Save. 
3. Enter the report or list view ID in the Query Editor, and then click Query Plan.
4. Inspect the query plan for your report or list view. If custom indexes are available for your organization, use query plans to help you decide when to request a custom index from Salesforce.
