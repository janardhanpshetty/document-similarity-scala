### Topic: Get sample code that shows how to implement Visualforce cloning.
Resolution
To 
implement Visualforce cloning
 on a custom Visualforce page, an Apex class is required to implement the logic.
URLFOR can only be used for calling the existing Clone function without a custom Visualforce page.
 
Example Visualforce cloning code
 
<apex:commandButton value="Clone" action="{!URLFOR($Action.ObjectName.Clone,<ObectName>.id)}"/>
