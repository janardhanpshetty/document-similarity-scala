### Topic: Summary of changes to the feedback and performance feature in Winter '16.
What's changed with enhanced feedback?
Resolution
The following is a summary of the changes made with enhanced feedback and performance summaries:
Users will now access their performance summaries from the Feedback tab and Admins will manage the performance cycle from the Performance Cycles tab.
Cloning a cycle will no longer clone the questions.  See 
here
 for more details.
#first_name can no longer be used as a placeholder for the subject's name in summaries.  See 
here
 for more details.
You can no longer preview a question set.
You can create custom 
list views
 for Feedback and Performance Cycles (e.g. list to view active performance cycles).
You can change the 
page layout
 for 
Feedback and Performance Cycles
 and expose 
custom fields
.
