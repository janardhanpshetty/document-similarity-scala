### Topic: Using the Chatter Answers Q&A Tab | Salesforce
Using the Chatter Answers Q&A Tab | Salesforce
Using the Chatter Answers Q&A Tab 
Available in: Salesforce Classic
Chatter Answers is available in: 
Enterprise
, 
Developer
, 
Performance
, and 
Unlimited
 Editions.
User Permissions Needed
To view questions:
“Read” on questions
To ask and reply to questions:
“Create” on questions
To view cases:
“Read” on case
To change cases:
“Edit” on case
Note
Starting in Summer ’16, Chatter Answers isn’t available in new orgs. Instead, you can use Chatter Questions, a Q&A feature that’s seamlessly integrated into Chatter. With Chatter Questions, users can ask questions and find answers without ever needing to leave Chatter.
Chatter Answers is a self-service and support community where users can post questions and receive answers and comments from other users or your support agents. Chatter Answers brings together Case, Questions and Answers, and Salesforce Knowledge articles in a unified experience.
Chatter Answers lets your customers:
Post, browse, and reply to questions using the Q&A tab.
Delete their own questions and replies.
Flag questions and replies as spam, hateful, or inappropriate.
Receive emails when their questions are answered or when best answers are chosen for questions they’re following.
Collaborate publicly or privately with support agents to resolve issues related to open cases.
Search and review articles from Salesforce Knowledge.
Like a post or Salesforce Knowledge article to help determine its popularity.
Upload photos of themselves to their user profiles.
View other users’ total number of posts and number of replies marked as best answers by others.
Search
: Customers can search for existing questions before they post their own.
Filter
 and 
Sort
: Community members and support agents can select different viewing options for questions in the feed.
Searches in the Chatter Answers Q&A tab can be filtered to show questions based on:
All Questions
 shows all questions in the zone, as well as Salesforce Knowledge articles, when enabled.
Unanswered Questions
 shows all questions that don’t have replies.
Unsolved Questions
 shows all questions that don’t have a best answer.
Solved Questions
 shows all questions that have a best answer, as well as Salesforce Knowledge articles, when enabled.
My Questions
 shows all questions you’ve asked and are following.
You can then sort the results based on the following options:
Date Posted
 sorts questions with the most recently asked questions appearing first.
Recent Activity
 sorts questions with the most recent replies and comments appearing first.
Most Popular
 sorts questions that have received the most likes, upvotes, and followers appearing first.
Question
: Customers can post a question to the community for help. Other members of the community can post answers or follow the question to receive email notifications on subsequent posts.
Browse by Category: If categories are enabled in the community, members can click the category name to show questions related to that category.
Reputation
: Community members can earn points and ratings that display on hover over their photos in the feed
Comment
: Community members and support agents can comment on the question, and the customer or agent can select a comment as the best answer.
Chatter Answers lets service organizations:
Create multiple communities and organize them into different zones, with each zone having its own focus and questions.
Brand and customize communities.
Give agents the opportunity to respond to customers publicly or privately.
Automate the creation of cases from questions using an Apex trigger and workflow rules.
Deflect customer inquiries through participation.
Encourage participation by publicly displaying user statistics.
Moderate questions and answers from the Q&A tab in the internal Salesforce application or from the community.
A customer’s question is typically answered on the Q&A tab using one of these processes:
Question Answered by a Similar Question with a Best Answer
Question Answered by the Members within the Community
Question Answered by a Support Agent
Question Answered by a Salesforce Knowledge Article
A customer types a question or keyword into the Chatter Answers Q&A tab and clicks 
Post Your Question
.
A similar question with a best answer appears in search results.
The customer selects that question, and views the answer.
A customer types a question or keyword into the Chatter Answers Q&A tab and clicks 
Post Your Question
.
No similar questions display in search results.
The customer continues to enter a description of the question and clicks 
Post to Community
to post a public question.
A community member or support agent reads the question and adds a comment, which answers the customer’s question.
A customer types a question or keyword into the Chatter Answers Q&A tab and clicks 
Post Your Question
.
No similar questions display in search results.
The customer continues to enter a description of the question and selects 
Post Privately to Representative
 to post a private question.
The private question is converted to a case.
A support agent reads the case and adds a private comment, which answers the customer’s question.
A customer types a question or keyword into the Chatter Answers Q&A tab and clicks 
Post Your Question
.
A similar Salesforce Knowledge article with an answer displays in search results.
The customer selects the article, and views the answer.
See Also:
Chatter Answers Terminology
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=questions_portal_overview.htm&language=en_US
Release
202.14
