### Topic: This article would help you locate the standard ‘Phone’ field so that it can be used as a filter within Process using Process Builder.
I am trying to create a process which should update the standard “Phone” field on contact record. However, I am unable to locate this field.
 
Resolution
The standard phone field does not appear in the field’s list by its name “Phone”. However, it is visible in the list as ‘Business phone’. Once you select this field, you will notice that the API name results as,
 
[Contact].Phone
 
