### Topic: Explanation of why On Demand Email to Case Cases, are creating without the default Routing Address field values.
BEHAVIOR:
When sending an inbound message to a On Demand Email to Case Routing Address, the default value for the fields:
Case Priority
Case Origin
Case Record Type
are not saving as designated in the Routing Address settings (Setup | Cases | Email-to-Case | (Routing Address)).  Rather, they appear to be defaulting to the default values configured on the field level.
 
Resolution
CAUSE:
This behavior is typically caused by permission level access.  The Automated Case User (Setup | Cases | Support Settings), is the running user that the Organization utilizes to create the Case record.  In most Cases, the above behavior occurs after the Routing Address is created.  If a new Record Type is created/used with the Routing Address, the Automated Case User's Profile will also need to have access to the Record Type.  If access is not granted, the aforementioned behavior will occur. 
