### Topic: Define the Path That a Flow Takes | Salesforce
Define the Path That a Flow Takes | Salesforce
Define the Path That a Flow Takes
Identify which elements the flow executes and in what order by connecting the elements on your canvas together.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To open, edit, or create a flow in the Cloud Flow Designer:
“Manage Force.com Flow”
On the canvas, find the node at the bottom of the source element.
Drag the node onto the target element.
If prompted, select which outcome to assign to the path.
See Also:
Remove Connectors from a Flow
Flow Connectors
Customize What Happens When a Flow Fails
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_connector_add.htm&language=en_US
Release
202.14
