### Topic: Test Code Coverage: Class with no functions will always show 0% coverage
A class has no functions; only variables. A test class was written for this class but it 
din't
 show any coverage.
 
#KBFeedback
 
 
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
April 13, 2016 at 9:00 AM
  
Shane Porter
Done.
Show More
Like
Unlike
 
  ·  
 
April 15, 2016 at 5:41 AM 
Attach File
 
Click to comment
 
 
Sumit Dey
 
published a new version of this Knowledge Base.
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Bookmark
Add Topics
April 13, 2016 at 6:21 AM
  
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
Developer
A class has no functions; only variables. A test class was written for this class but it didn't show any coverage.
Resolution
Code coverage looks at usage of a class based on functions called from test class. So, the main class should at least have one function and that function must be used in test class.
Main Class
public class ClassToCheckCodeCoverage {
  public static final String stringA = 'String A';
    
    public void someFunction() {
        //some processing
    }
}
Test Class
@IsTest
public class ClassToCheckCodeCoverageTest{
    @istest
    static void testsomeFunction(){
            ClassToCheckCodeCoverage  obj= New ClassToCheckCodeCoverage ();   

            //Following call is what will show code coverage
           //Ofcourse, there will be some logic to assert an output from class
            obj.someFunction();
    }
}
 
