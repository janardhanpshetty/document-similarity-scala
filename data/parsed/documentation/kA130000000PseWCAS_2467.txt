### Topic: Customizing Ideas Settings | Salesforce
Customizing Ideas Settings | Salesforce
Customizing Ideas Settings
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To customize Ideas settings:
“Customize Application”
To manage organization-wide settings for Ideas:
From Setup, enter 
Ideas Settings
 in the 
Quick Find
 box, then select 
Ideas Settings
.
Click 
Edit
.
Use the 
Enable Ideas
 checkbox to enable or disable Ideas for your organization.
Disabling Ideas removes the Ideas tab and users will no longer be able to access active zones, but these zones will reappear on the Ideas tab the next time you enable Ideas.
Optionally, select 
Enable Text-Formatting, Images and Links
 to enable the Ideas HTML editor, which gives users WYSIWYG HTML editing and image referencing capabilities when they post or comment on ideas. 
Warning
Once you enable the Ideas HTML editor, you cannot disable it. If you do not see the 
Enable Text-Formatting, Images and Links
 checkbox, the Ideas HTML editor is enabled for your organization by default.
If your organization does not already have the multi-select 
Categories
 field enabled, click the 
Enable
 button located below the Categories message at the top of the page. This button is not displayed if your organization already has the 
Categories
 field enabled.
If the 
Categories
 field is already enabled, the 
Enable Categories
 checkbox is selected. Once the field is enabled, you cannot disable it.
Select 
Enable Reputation
 to let users earn points and ratings based on their activity in each zone.
Select an 
Ideas User Profile
 type for all user profiles in the zone.
User Profile Type
Description
Chatter profile
The user’s Chatter profile is the default user profile type. If you select this option and a user doesn’t have a Chatter profile, then the Ideas zone profile is used.
Ideas zone profile
The profile that the user sets up for the Ideas zone. This profile type is used for Ideas zones in portals.
Custom profile with a Visualforce page
You can specify a Visualforce page for a custom profile for all Ideas users in the zone. If you select this profile type, you must specify a Visualforce page in 
Custom Profile Page
.
In the 
Half-Life (in Days)
 field, enter a number of days.
The half-life setting determines how quickly old ideas drop in ranking on the Popular Ideas subtab, to make room for ideas with more recent votes. A shorter half-life moves older ideas down the page faster than a longer half-life.
Note
This field does not appear if Ideas is disabled. To modify the 
Half-Life (in Days)
 field, save your changes after enabling ideas, and then click 
Edit
 on the Ideas Settings page.
Click 
Save
.
See Also:
Enable Idea Themes
Managing Ideas
Administrator setup guide: Salesforce Ideas Implementation Guide
Encouraging Innovation with Idea Reputation
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=ideas_settings.htm&language=en_US
Release
202.14
