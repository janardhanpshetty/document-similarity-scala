### Topic: Document Library Overview | Salesforce
Document Library Overview | Salesforce
Document Library Overview
Each document that is stored in the document library resides in a folder. The folder’s attributes determine the accessibility of the folder and the documents within it.
Available in: Salesforce Classic
Available in: 
All
 Editions except 
Database.com
Document libraries store documents that aren’t attached to records. Access your library documents via the Documents tab. If your Documents tab is not visible, customize your display to show it.
Note
The Documents tab is not part of Salesforce CRM Content.
See Also:
Differences Between Files, Salesforce CRM Content, Salesforce Knowledge, Documents, and Attachments
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=docs_def.htm&language=en_US
Release
202.14
