### Topic: I created an Event(create record) "Global Action" and added to the layout, and now some users are reporting Related List loading and Collapsible Sidebar functionality are not working as expected.
I created an Event(create record) "Global Action" and added to the layout, and now some users are reporting Related List loading and Collapsible Sidebar functionality are not working as expected.
This will occurs if your orgs has "Shared Activities" feature enabled and the user's profile does not have READ CRUD for LEAD.
A create Event record "Global Action" will cause Related List loading and Collapsible Sidebar functionality to not work as expected.
 
The impact is Sidebar cannot be collapsed or restore, Related lists on objects (standard/custom) will not load and keeps on spinning.
 
Resolution
This is working as design as "Shared Activities" requires READ on LEAD for certain global action such as Event creation, hence if removed READ access to LEAD this will caused the unsuspected behavior.
If you do not want your users to have visibility to Lead but need this function to work customer can use the following workaround:
1) re-enable read on lead on the affected profile
2) now go on section "Field-Level Security" and click on "Lead"
3) Here selectively work on "Visible" and "Read Only" check boxes to show or hide fields to obtain your level of security for this profile, this way you will be able to give the global action read access to Lead Object but not show the fields to the user.
** R&D has indicated they may re-open and change this designed behavior, waiting for more updates so feel free to monitor W-1759184 https://gus.my.salesforce.com/a07B0000000Vimn. this article will be updated when/if anything changes
