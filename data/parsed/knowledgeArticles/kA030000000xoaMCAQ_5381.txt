### Topic: Details on how to create a report to show metrics without the related parent goals data.
How do I create a report on metrics?
Resolution
Before you begin make sure you have installed the Work.com Reports package.  You can find the download links in the 
Work.com Report and Dashboard Overview
.
1. Go to the 
Reports
 tab and create a new report
2. Search and select the 
Goals & Metrics - Summer '15
 report type
3. Customize the report and add any field not already included by default
