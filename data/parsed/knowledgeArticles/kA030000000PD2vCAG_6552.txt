### Topic: Within a Contact & Accounts Report Type, the Mailing Address field will populate itself from the Account's Billing Address upon the event that the Mailing Address is blank.
While trying to build a report on the standard report type "Contacts & Accounts" empty fields in the contact are also getting populated. Why is this happening?
Resolution
Within a Contact & Accounts Report Type the following field shows as containing value from account record if those fields are not populated in the contact
1) Mailing Address field will populate itself from the Account's Billing Address
2) Contact Phone will show value from Account Phone field
But if you open the contact record the fields will be blank.
This is very specific to the standard report type "Contacts and account"
If a new custom report type is created on the Accounts and Contacts objects this will not happen
