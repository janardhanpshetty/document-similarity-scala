### Topic: Enable Visualforce Pages for the Salesforce1 Mobile App | Salesforce
Enable Visualforce Pages for the Salesforce1 Mobile App | Salesforce
Enable Visualforce Pages for the Salesforce1 Mobile App
You can use Visualforce to extend the Salesforce1 app and give your mobile users the functionality that they need while on the go.
 Before adding a Visualforce page to Salesforce1, make sure the page is enabled for mobile use or it won’t be available in the mobile apps.
Available in Lightning Experience in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Available in Salesforce Classic in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To enable the display of Visualforce in Salesforce1:
“Customize Application”
“Author Apex”
Tip
Before exposing existing Visualforce pages in Salesforce1, consider how they’ll look and function on mobile phones and tablets. Most likely, you’ll want to create a new page specifically for mobile form factors.
Visualforce pages must be enabled for mobile use before they can display in these areas of the Salesforce1 user interface:
The navigation menu, via a Visualforce tab
The action bar, via a custom action
Mobile cards on a record’s related information page
Overridden standard buttons, or custom buttons and links
Embedded in record detail page layouts
Lightning pages
To enable a Visualforce page for Salesforce1:
From Setup, enter 
Visualforce Pages
 in the 
Quick Find
 box, then select 
Visualforce Pages
.
Click 
Edit
 for the desired Visualforce page.
Select 
Available for Salesforce mobile apps and Lightning Pages
 then click 
Save
.
Consider these notes about Visualforce support in Salesforce1.
Standard tabs, custom object tabs, and list views that are overridden with a Visualforce page aren’t supported in Salesforce1. The Visualforce page is shown for full site users, but Salesforce1 users will see the default Salesforce1 page for the object. This restriction exists to maintain the Salesforce1 experience for objects.
You can also enable Visualforce pages for Salesforce1 through the metadata API by editing the 
isAvailableInTouch
 field on the ApexPage object.
The 
Salesforce Classic Mobile Ready
 checkbox on Visualforce Tab setup pages is for Salesforce Classic Mobile only and has no effect on Visualforce pages in the Salesforce1 apps.
See Also:
Customize the Salesforce1 Navigation Menu
Manage Mobile Cards in the Enhanced Page Layout Editor
Viewing and Editing Visualforce Pages
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_sf1_visualforce.htm&language=en_US
Release
202.14
