### Topic: Enabling Development Mode | Salesforce
Enabling Development Mode | Salesforce
Enabling Development Mode
Available in: Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To enable development mode:
“Customize Application”
Although you can view and edit Visualforce page definitions on the Visualforce Pages page in Setup, enabling Visualforce development mode is the best way to build Visualforce pages. Development mode provides you with:
A special development footer on every Visualforce page that includes the page’s view state, any associated controller, a link to the component reference documentation, and a page markup editor that offers highlighting, find-replace functionality, and auto-suggest for component tag and attribute names.
The ability to define new Visualforce pages just by entering a unique URL.
Error messages that include more detailed stack traces than what standard users receive.
To enable Visualforce development mode:
From your personal settings, enter 
Advanced User Details
 in the 
Quick Find
 box, then select 
Advanced User Details
. 
No results? Enter 
Personal Information
 in the 
Quick Find
 box, then select 
Personal Information
.
Click 
Edit
.
Select the 
Development Mode
 checkbox.
Optionally, select the 
Show View State in Development Mode
 checkbox to enable the View State tab on the development footer. This tab is useful for monitoring the performance of your Visualforce pages.
Click 
Save
.
See Also:
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=pages_dev_mode.htm&language=en_US
Release
202.14
