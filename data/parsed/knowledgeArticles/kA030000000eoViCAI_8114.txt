### Topic: Validation Status
When attempting to Add Members or Update Members on a Campaign from an import file, the user gets the following message:  "URL No Longer Exists".
The link points to:
https://instance.lightning.force.com/dataImporter/dataimporter.app?objectSelection=Lead,Account,PersonAccount&campaignId=CamapaignID
or
https://instance.lightning.force.com/dataImporter/dataimporter.app?objectSelection=CampaignMember
The issue occurs, when the org has the Black Tab permission 
"Disable Data Importer"
 enabled.
 
Resolution
The permission has to be disabled by the Program Managers in the PINT Team. Please escalate the case to R&D and mention the investigation: 
W-2941277
.
