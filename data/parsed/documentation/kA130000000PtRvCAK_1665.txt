### Topic: Can I automatically email leads that come from my website? | Salesforce
Can I automatically email leads that come from my website? | Salesforce
Can I automatically email leads that come from my website?
Yes. You can create auto-response rules that will email these prospects using templates that vary based on criteria you set up.
See Also:
Leads FAQ
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_leads_can_i_automatically.htm&language=en_US
Release
202.14
