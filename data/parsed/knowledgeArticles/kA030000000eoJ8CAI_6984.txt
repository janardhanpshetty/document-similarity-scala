### Topic: When navigating to the Knowledge Tab, you may encounter the error like Severe Error trying to refresh article listing which includes an error Id ending in (2134695572). This is typically the result of a point in time sandbox copy issue caused by modifying Knowledge Article data during a refresh. To resolve the issue, customer needs to log a case to Salesforce.
ISSUE: 
When navigating to the Knowledge Tab, you may encounter the error:
Severe Error trying to refresh article listing
 which includes an error Id ending in (2134695572).
[Please note that error was initially reported with error ID: 2134695572. However, most customer will report error with "Severe Error trying to refresh article listing" , but different error ID]
Resolution
CAUSE:
This is typically the result of a point in time sandbox copy issue caused by modifying Knowledge Article data during a refresh.  
Resolution:
Please 
log 
support case to Salesforce for resolution.
