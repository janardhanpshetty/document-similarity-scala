### Topic: Chatter Favorites Overview | Salesforce
Chatter Favorites Overview | Salesforce
Chatter Favorites
 Overview
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Chatter favorites on the Chatter tab give you easy access to list views, Chatter feed searches, and topics that you want to stay on top of. For example, if you and your coworkers use the hashtag topic #acme to track information about your customer Acme, you might want to add #acme as a favorite so you can easily access these updates without leaving the Chatter tab.
You can have up to 50 favorites. If you haven't added any favorites, the Favorites section doesn't appear
 on the Chatter tab
.
Chatter displays the four favorites most recently added or viewed.
Click a favorite to see the updates.
Click 
X
 more
 to see all your favorites.
Click 
, which appears on hover, to remove a favorite.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_favorites_overview.htm&language=en_US
Release
202.14
