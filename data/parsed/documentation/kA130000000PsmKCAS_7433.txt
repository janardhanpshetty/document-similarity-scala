### Topic: Defining Syndication Feeds | Salesforce
Defining Syndication Feeds | Salesforce
Defining Syndication Feeds
Available in: Salesforce Classic
Available in: 
Developer
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
Create, edit, or delete a feed definition:
“Modify All Data”
Subscribe to a feed
No special user permission required
Syndication feeds
 give users the ability to subscribe to changes within Force.com sites and receive updates in external news readers. Simply by defining a SOQL query and mapping, you can syndicate changes to public data to your end users. You can create one or more syndication feeds for your organization's public sites or any Visualforce page. 
The syndication feed displays the records specified by a SOQL query. Users can subscribe to the feed and view the feed when they visit the site or page.
Define a syndication feed, including what records are returned, and which data from the records is displayed:
Name
A descriptive name for this feed, which distinguishes it from other feeds you may create. Use only letters, numbers, or the underscore character “_”. Do not use more than one underscore character in a row.
Description
Describe the feed. For example, “Account first name, last name, and region for the last ten accounts created or edited.”
Query
The SOQL query that defines which records are returned to the syndication feed. To ensure fast performance, some limitations on the SOQL query are imposed:
If the SOQL query does not specify a limit, then no more than 20 records are returned.
Query limits can't exceed 200 results. If you make a query with a limit beyond this number, only the first 200 records are returned.
If the SOQL query does not have an ORDER BY value specified, records are ordered by the 
LastModifiedDate
 value if there is one, or by 
SystemModstamp
 value if not
COUNT
 is not supported.
Aggregate
 queries are not supported. For example, this query cannot be used in a syndication feeds SOQL query:
SELECT Name, (SELECT CreatedBy.Name FROM Notes) FROM Account
You can use 
bind variables
, a variable whose value is supplied in the URL. For more information, see 
Using Bind Variables for Queries and Mapping
.
Note
The guest user must have appropriate sharing and field-level security access or you cannot save your query, because the Force.com platform verifies access and sharing before saving.
Mapping
Because syndication feeds use the ATOM web publishing protocol, you must provide a mapping value that matches objects and fields on the returned records with ATOM constructs. Note that all values must be string literals. For more information about mapping elements, see 
ATOM-Object Mapping
.
You can use 
bind variables
, a variable whose value is supplied in the URL. For more information, see 
Using Bind Variables for Queries and Mapping
.
Max Cache Age Seconds
Because many users may access a feed at the same time, Salesforce caches the feed data, for 3600 seconds by default. You can modify this to a minimum of 300 seconds, or for as long as you wish. Query results that are older than the time specified here are dropped, and a new query is run on the next request for that information, that is, the next time a user opens a page containing a feed that they have subscribed to.
Active
Select this checkbox to make the feed available for subscription. Once a feed is active, users have the option of subscribing to it.
ATOM-Object Mapping
You must specify a mapping in the syndication feed definition. The mapping relates ATOM constructs such as entry title to the corresponding value in a record, for example, “Account Name.” A full set of mappings represents a news feed, and the query represents the content of each news item in a feed. For example, Lead records created today or Contacts with updated Account information.
A 
feed element
 is the envelope for each part of a news item, and an 
entry element
 is the contents of the envelope.
Mapping also allows you to apply short labels to various fields for display purposes.
The following table lists each ATOM and object element and explains what values should be provided:
Feed Element
Entry Element
Description
fa
Required only if 
ea
 (entry author) is not specified. Feed author. For example, 
fa:"Acme Feed Author Admin Mary"
 shows the feed author as Admin Mary.
fid
Optional (because default value is supplied). Id of the feed. By default, this value is the public site URL. If you specify a value, it must be a valid internationalized resource identifier (
IRI
). An IRI is a URL generalized to allow the use of Unicode.
fl
Optional (because default value is supplied). Feed link. For example, 
fl:"http://www.salesforce.com"
. News readers usually interpret this element by linking the feed title to this URL.
fst
Optional. Feed subtitle. For example, 
&map=ft:"Newest Opportunities",fst:"Western Division"
 shows the feed title Newest Opportunities and subtitle Western Division.
ft
Required. Feed title. For example, 
ft:"Newest Opportunities"
.
ea
Required only if 
fa
 (feed author) is not specified. Entry author. For example, 
ea:"Account created by: " + Account.CreatedBy 
.
ec
Required. Entry content. For example,
ec:"description for " Name "<br>" Description
 shows the value of the 
Name
 field with additional text. The output of a feed for this example resembles the following:
description for Ajax Industries Description
ect
Optional. Entry content of type 
text
, 
html
, or 
xhtml
. For example, 
ect: html
 for HTML content. Default is 
text
.
el
Optional. Entry link. Must be a valid URI. This value is usually a link to another representation of the content for the entry. For example, the link could be to a record in the Salesforce user interface. News readers usually interpret this element by linking the entry title to this URL For example, 
el:"Account.URl"
.
es
Optional. Entry summary. An optional summary of the entry content. For example, 
et: Account.Name, es: Account.Name + "’s account number, website, and description", ec: Account.AccountNumber + " " + Account.Website + “ “ + Account.Description 
If not specified, news readers display the content defined using the 
ec
 element.
est
Optional. Entry summary of type 
text
, 
html
, or 
xhtml
. For example, 
est: html
 for HTML content. Default is 
text
. Do not specify a value unless 
es
 has been specified.
et
Required. Entry title, a field name. For example, 
et:Name
.
eu
Optional. By default, the required ATOM element 
<updated>
 value is automatically provided by the 
LastModifedDate
 of the main entity being queried; usually the object in the main 
FROM
 clause of the SOQL query. This value indicates the last time an entry or feed was modified. If you wish to change this default behavior, you can specify a different object or field's 
LastModifedDate
 be used. For example:
Query: 
SELECT Id, Name, MyDate__c FROM Account
Mapping Parameter: 
eu: MyDate__c
Query: 
SELECT Id, Lastname, Account.LastMOdifiedDate FROM Contact
Mapping Parameter: 
eu: Account.LastModifiedDate
The following example shows a valid mapping values for a syndication feed:
ft: "Harry Potter", et: Name, ec: "description for " Name "<br>" Description, el: "/" Id, ect: html
Feeds are displayed in the guest user context of the public site where they are displayed. Therefore, if you have used custom labels to manage internationalization, and specified labels in your mapping, users see those labels displayed in the language of the guest user. For more information, see 
Custom Labels and Feeds
.
You can only use string literals in feed mapping. You cannot use, for example, date literals such as 
TODAY
 or 
LAST_WEEK
.
After you have defined a feed, you should test it, and then make the feed active by selecting the 
Active
 checkbox as described above. For more information about testing, see 
Testing Syndication Feeds
.
Using Bind Variables for Queries and Mapping
You can use 
bind variables
 in the 
WHERE
 clause of the SOQL query. Bind variables must be in the following form:
{!
var_name
}
The following query uses a bind variable named 
accountID
.
SELECT Name, Description
FROM Account
WHERE Id = {!accountID}
Note that this is not the literal name of the field, but an arbitrary name. At run time, the value for 
accountID
 is passed to the query from the URL. This feed is accessed from the following URL and passes in the account ID as part of the query string parameter:
site_URL
/services/xml/My'Account'Feed?accountId=0013000000BmP4x
You can also use bind variables for mapping values.
The following implementation details apply to the use of bind variables for queries:
You cannot use more than 20 bind variables in a feed definition, queries and mapping combined.
The bind variable name cannot be more than 100 characters.
You can use a bind variable only on the right side of a filter operation to represent part of a string. Because it represents part of a string, it must be in quotes. For example, the following is a valid query:
SELECT Id, Name FROM Account WHERE Name = '
{!myBindVariable}
'
The following queries are not valid, because the bind variable is not in a valid position, and is not functioning as the variable for a literal string:
SELECT Id, Name FROM Account WHERE 
{!myFieldName}
 = 'Joe'
SELECT Id, 
{!myFieldName}
 FROM Account WHERE IsDeleted = false
You cannot use a bind variable to represent a field name. This means a bind variable cannot be use on the left side of a filter operation.
You cannot use a bind variable to change the meaning or structure of a query for security reasons. Any special characters you specify in the bind replacement value are interpreted as literal characters when the query is evaluated.
Custom Labels and Feeds
For feeds that need to be localized into different languages, you can use custom labels to define the string in multiple languages. Then in the mapping definition, you simply refer to the custom label. When a request comes in, the custom label inspects the guest user language and returns the translated text, which is used in the mapping.
Custom labels can be specified in a field with the following syntax:
map_element_name
: "{!$LABEL.
custom_label_name
}"
Use the following procedure to specify a custom label in a feed:
From Setup, enter 
Custom Labels
 in the 
Quick Find
 box, then select 
Custom Labels
. You may wish to name the custom label after the mapping element that takes its value, for example 
feedTitle
 for the 
ft
 element.
Enter the values for all supported languages.
Specify the custom label in the feed mapping.
For example, assume that you create a feed containing information on all the houses your company is trying to sell. For English users, the title of the feed should be “The Houses,” but for Spanish users, the title of the feed should be “Las Casas.” You would create a custom label, for example, 
feedTitle
. In English, its value is “The Houses,” and the Spanish value is “Las Casas.” Then, in the feed mapping definition, specify the following for the feed title 
fc:
:
ft: "{!$LABEL.feedTitle}"
Visualforce and Feeds
To add a feed to a Visualforce page, use the Visualforce standard HTML features. For example, assuming the Visualforce page is located in the base directory of the site, it can contain a tag like the following:
<A HREF=""/services/xml/theFeedName">My feed</A>
The text 
My feed
 links to the feed.
If you want to link the feed from an image, include an inline image tag similar to the following:
<A HREF="/services/xml/theFeedName"><img src="feed.gif"></A>
You must upload your own image.
To add the icon to the address bar, add the link tag to the 
<head>
 tag of the Visualforce page:
<link href='https://org62.my.salesforce.com/help/doc/en_US/URI of feed'
   type='application/x.atom+xml'
   rel='feed'
   title='A nice descriptive title'/>
See Also:
About Syndication Feeds
Testing Syndication Feeds
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=feeds_creating.htm&language=en_US
Release
202.14
