### Topic: Health Cloud Resources | Salesforce
Health Cloud Resources | Salesforce
Health Cloud Resources
In addition to online help, Salesforce creates video demos, guides, and tip sheets to help you learn about our features and successfully administer Salesforce.
Health Cloud
For End Users
For Admins
Guides and Tip Sheets
Getting Started with Salesforce Health Cloud 
Health Cloud Implementation Guide
Health Cloud Object Reference Guide
Industries REST API Developer Guide
Videos
Collaborate with Patients and Colleagues Using Chatter
Manage Your Day with the Today Page
Get a Comprehensive View of Your Patient’s Medical Records
Use the Timeline to Monitor Patient Health History
Create Patients in the Health Cloud Console
Create Customized Lists of Patients
Manage the Patient Care Team
Use Care Plans to Help Patients Reach Their Goals
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=healthcare_resources.htm&language=en_US
Release
202.14
