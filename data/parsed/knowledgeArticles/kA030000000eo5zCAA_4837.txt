### Topic: Winter ‘17 release*, we will end support of Salesforce1 access via BlackBerry 10
 
What is the change and when is it happening?
With the Winter ‘17 release*, we will end support of Salesforce1 access via BlackBerry 10. While you will still be able to use Salesforce1, support for technical or product issues will no longer be provided by Salesforce and no further development will take place. 
 
*
Currently targeted for October 2016; date subject to change
 
What exactly will happen after the EOL/EOS date passes?
Users will be able to continue to use Salesforce1 from the BlackBerry 10 browser, however, support for Salesforce1 access from the BlackBerry 10 browser will end by Winter ’17. 
Will I (customer) be able to access my data after the retirement date?
Yes.  Customer data will not be affected in any way.
 
What will happen with any old data left in the retired product?
No customer data is stored in Salesforce1 accessed via the BlackBerry 10 browser.
 
What action must I (customer) take?
Customers leveraging Salesforce1 in the BlackBerry 10 browser will need to transition their users to BlackBerry's native Connect to Salesforce app by the Winter ‘17 release, at which time official support for Salesforce1 in the BlackBerry 10 browser will end.
What are the migration options?
Because all data accessed in Salesforce1 via the BlackBerry 10 browser remains stored in the customer’s Salesforce organization, there is no work required to migrate data.  Rather customers will simply need to transition their mobile use cases to BlackBerry’s Connect to Salesforce app.
 
What resources are there for me (customer) to use?
For more information, please refer to 
BlackBerry’s Connect to Salesforce User Guide
.
 
What is the recommended replacement or workaround for customer?
We encourage our customers to use the native Connect to Salesforce app. It provides offline capabilities at no additional cost.
Resolution
