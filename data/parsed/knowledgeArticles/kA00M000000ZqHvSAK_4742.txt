### Topic: This article contains all of the information and resources available about the new Lightning editions.
This article contains all of the information and resources available about the new Lightning editions.
The last update to this article occurred on April 29, 2016.
What is the change?
On February 2, 2016, Salesforce introduced new core Salesforce editions for Sales Cloud and Service Cloud to give customers more customization and configuration capabilities. Salesforce’s Professional Edition, Enterprise Edition and Unlimited Edition for Sales Cloud and Service Cloud will be replaced by Lightning Professional Edition, Lightning Enterprise Edition and Lightning Unlimited Edition for Sales Cloud and Service Cloud.
You and your end users will not experience any service disruptions while the change is made. For full details on all of the new features and capabilities coming to your org please read the Summer ‘16 
release notes
.
 
When will the new Lightning edition features be available?
The new Lightning edition features will be available for your instance according to the following schedule:
Instances
Lightning Editions Availability Date
NA45
May 3, 2016, 09:00 AM PDT (May 3, 2016, 04:00 PM UTC)
EU0, EU1, EU2, EU3, EU4, EU5, EU6
May 8, 2016, 09:00 AM PDT
(May 8, 2016, 04:00 PM UTC)
NA0, NA2, NA3, NA4, NA5, NA6, NA7, NA8, NA9, NA10, NA11, NA12, NA13, NA14, NA15, NA16, NA17, NA18, NA19, NA20, NA21, NA22, NA23, NA24, NA25, NA26, NA27, NA28, NA29, NA30, NA31, NA32, NA33, NA34, NA41, NA44
May 17, 2016, 09:00 AM PDT  (May 17, 2016, 04:00 PM UTC)
AP0, AP1, AP2, AP3, AP4
May 19, 2016, 09:00 AM PDT
(May 19, 2016, 04:00 PM UTC)
Sandboxes
May 8, 2016, 09:00 AM PDT (May 8, 2016, 04:00 PM UTC)
What action do I need to take?
No action is required to prepare for this change. Admins will have full control over the new features and capabilities, meaning no end users will have access to any of them until an admin configures them.
 
To ensure that you are fully prepared for the Lightning edition upgrades, we will be hosting webinars with our Success Services team. The webinars are full but we will make a recording
 available 
here
 on May 4, 2016. Access the recording on May 4 and you'll learn all about the new capabilities of each Lightning edition, the impact to your users, and where to go for more information.
Resources
For more information on all of the changes coming to your org review the 
release notes
. Common questions are answered on this 
FAQ
.
Resolution
