### Topic: Browsing Questions within a Zone or Category | Salesforce
Browsing Questions within a Zone or Category | Salesforce
Browsing Questions within a Zone or Category
Available in: Salesforce Classic
Data categories and answers are available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions.
User Permissions Needed
To view the Answers tab:
“Read” on questions
To ask and reply to questions:
“Create” on questions
To vote for replies:
“Read” on questions
Note
Starting with Summer ’13, Answers isn’t available in new orgs. Instead, you can use Chatter Questions, a Q&A feature that’s seamlessly integrated into Chatter. With Chatter Questions, users can ask questions and find answers without ever needing to leave Chatter. Existing orgs will continue to have access to Answers if it was enabled before the Summer ’13 release.
To view all questions within a zone or category, go to the Answers tab and click the zone name or category that appears below the 
Get an Answer
 heading.
Browsing Questions within a Zone
After clicking the zone name to view all questions within that zone, you can:
Ask a question
.
Filter questions so you only see the open or resolved questions. 
A question is considered resolved when the person who asked the question selects one of the replies as the best answer. Community members can continue to post replies and vote for replies even when the question has been resolved.
Sort questions by:
Recent Activity—Shows the questions that have the most recent replies at the top of the list
Newest—Shows the questions that have been most recently asked at the top of the list
Oldest—Shows the oldest questions at the top of the list
Click 
Reply
 below a question to 
post a reply
.
Click a category to view all the questions associated with that category.
All the categories within your zone appear under the zone name at the top of the page.
The 
question icons
 identify whether the question has been resolved or is still open. 
Browsing Questions within a Category
When community members ask a question, they associate a single category with their question to make it easier to find within the community. To browse all questions associated with a category, click the category name to display the category detail page.
You can click on a category from the following locations:
When 
viewing a question
, the categories associated with that question appear below the question description.
When browsing questions within a zone, all the categories in the zone appear below the zone name.
On the 
answers home page
, all the categories in the zone appear below the zone name.
From the category detail page, you can:
Ask a question
 that is automatically associated with the category you are viewing.
View all questions associated with that category, or filter the questions so you only see the open or resolved questions. A question is considered resolved when the person who asked the question selects one of the replies as the best answer. Community members can continue to post replies and vote for replies even when the question has been resolved.
Sort questions by:
Recent Activity—Shows the questions that have the most recent replies at the top of the list
Newest—Shows the questions that have been most recently asked at the top of the list
Oldest—Shows the oldest questions at the top of the list
Click 
Reply
 below a question to 
post a reply
.
The 
question icons
 identify whether the question has been resolved or is still open.
See Also:
Answers Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=answers_questions_by_community.htm&language=en_US
Release
202.14
