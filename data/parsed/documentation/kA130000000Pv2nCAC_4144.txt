### Topic: Searching in a Specific Feed | Salesforce
Searching in a Specific Feed | Salesforce
Searching in a Specific Feed
Use feed search to find information in a specific context.
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
You can search for information in feeds on records, groups, topic pages, user profiles, and on the Chatter and Home tabs. A contextual feed search is helpful when you want to confirm if something was once discussed in that specific feed.
Click 
 above the feed to look for information in that feed. For example, use the feed search on a group’s page to find information in that group.
Type your search terms and press ENTER or click 
.
You can search for hashtag topics, mentions, and files posted in the feed, or refine your search using wildcards, operators, and quotation marks to match on exact phrases.
Tip
To search for hashtag topics with multiple words, use brackets after the hashtag and around the words. For example, to find all instances of #Universal Paper, type 
#[universal paper]
 in the search box.
Search results display with matching terms highlighted. Filters or sorting criteria used in the feed apply to feed search results as well.
Click 
 to clear your search results and return to the feed.
Feed search behavior may vary slightly depending on where you perform the search.
Feed search is not supported for feeds on list views.
You can only search feeds you have access to via sharing rules.
On the Chatter tab, feed search results are also limited by your 
feed type selections
. For example, if you select the To Me feed type, and select additional 
filters
 and 
sorting criteria
, all of those criteria apply to the feed search results.
Feed search results are limited to the 
posts that are accessible
 from the context you search within. For example, if you search the feed on a user’s profile, the results include the posts and comments accessible from the user’s profile. This includes both the posts and comments shared by or with the user.
Feed search returns matches for file or link names shared in posts, but not in comments.
Record field changes aren’t included in record feed search results.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_feed_search_in_groups.htm&language=en_US
Release
202.14
