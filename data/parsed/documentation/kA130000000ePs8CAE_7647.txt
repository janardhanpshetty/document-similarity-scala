### Topic: Set Up Salesforce Permissions for the Event Monitoring Wave App | Salesforce
Set Up Salesforce Permissions for the Event Monitoring Wave App | Salesforce
Set Up Salesforce Permissions for the Event Monitoring Wave App
Select licenses and create and assign permission sets to enable admins and users to access the Event Monitoring Wave App.
User Permissions Needed
To create and edit permission sets:
“Manage Profiles and Permission Sets”
Note
As part of Event Monitoring, you also get the Event Monitoring Wave App. Use this app to upload and access only the data provided to you as part of your subscription. Please prevent your users from using the app to upload or access any other data. Salesforce sometimes monitors such usage. The Event Monitoring Wave App is available in English only.
Permission
Function
“Use Wave Analytics Templated Apps”
Enables access to all Wave templated apps.
“Access Event Monitoring Wave Templates and Apps”
Provides specific access to the Event Monitoring Wave App as well as the data and prebuilt dashboards included with it.
“Manage Wave Analytics Templated Apps”
Gives admins the ability to create Wave apps for users in their orgs, in this case the Event Monitoring Wave App.
“Edit Wave Analytics Dataflows”
Enables admins to upload and download JSON for existing data flows.
Create permission sets using these permissions the same way that you do for any Salesforce permission set. Select a permission set license, create permission sets based on the permissions enabled by that license, and then assign these permission sets to users or prifles. Typically you’ll create two types of permission sets, one for users and one for admins.
Select the Event Monitoring Wave App permission set license.
 All Event Monitoring Wave App users in your org need the Event Monitoring Wave App permission set license. Select and assign this license to users just as you would other Salesforce permission set licenses.
Create and assign a permission set that enables users to access the Event Monitoring Wave App.
 Most users in your org only need to view the Event Monitoring Wave App. For those users, create a permission set that includes “Use Wave Analytics Templated Apps” and “Access Event Monitoring Wave Templates and Apps”. These permissions let users view anything in Event Monitoring Wave and create and edit dashboards. Assign this permission set to the appropriate users or profiles.
Create and assign a permission set that enables admins and some users to access and manage the Event Monitoring Wave App.
 Admins and some users need to create the Event Monitoring Wave App and share it with others. Create a permission set with the Event Monitoring Wave license and assign it to these admins and users. With these permissions, users can view everything in Event Monitoring Wave and create and edit dashboards. But they can also create, share, and delete the app, schedule and work with data flows, upload external data, and more.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_help_setup_admin_wave_permissions.htm&language=en_US
Release
202.14
