### Topic: Prevent duplicate Leads when adding lead as Campaign Member using the Import tool.
If you want to prevent duplicate Leads during import -- especially when directly adding a Lead as a Campaign Member, you can use Duplicate Management. Below are the steps on how to prevent duplicates when importing a lead in a Campaign record:
Resolution
NOTE: You must prepare an excel file that covers the data needed on Lead (e.g. Owner, Last Name and Lead Status are required)
1. Create a Matching Rule.
   a. Go to Setup | Data.com Administration | Duplicate Management | Matching Rules.
   b. Click New Rule button.
   c. Select Lead as object then click Next.
   d. Enter a Rule Name and Unique Name.
   e. For Matching Criteria, select Last Name on Field box and Exact for Matching Method. (since Last Name is required)
   f. Save.
   g. Make sure to activate the Matching Rule.
NOTE: You can add more fields depending on your requirement. On this part, we only want to prevent Leads that has the same Last Name.
2. Create a Duplicate Rule.
   a. Go to Setup | Data.com Administration | Duplicate Management | Duplicate Rules.
   b. Click New Rule dropdown and select Lead.
   c. Enter a Rule Name.
   d. Select a Record-Level Security.
   e. Specify an action. (For this one I have set Action on Create | Allow | Alert and Report)
   f. Under Matching Rules, select Leads for "Compare Leads With".
   g. Matching Rule = (Matching Rule for Lead you have just created)
   h. Make sure fields mapping is correct. (It will display yellow exclamation when field mapping is incorrect and green if it is correct)
   i. Set Conditions and make criteria: 
Campaign: Campaign Name
NOT EQUAL TO
null (Since you want to prevent Leads associated to Campaign)
    j. Save and make sure to activate the duplicate rule.
Now go to an existing Campaign and follow the steps below:
1. Click Manage Members dropdown button and select Add Members - Import File.
2. Click Import Leads button. (This will open up the Lead Import Wizard)
3. On Step 6, click Choose File button and select the excel file you created/to be imported.
4. Fill in the rest of the Steps if needed then click Next.
5. Check if mapping is correct then click Next.
6. Click Import Now.
You will get a message that the import is in progress and that you check it under Imports queue in Salesforce. (Setup | Monitor | Imports)
Since we are using Duplicate Management, the import will finish, however, you will receive an email saying that the import was not successful because of a validation error if the system recognizes there is a duplicate Last Name for Lead.
The email message will be something like this:
===============================================
Salesforce.com import of file Lead Import.csv has finished, processed 3 lines
Salesforce.com has just completed your import process!
 
Result:
Total records processed: 2
Leads created: 0
Campaign Members created: 0
 
Lead Validation Errors: 2
Line 2: You're creating a duplicate record. We recommend you use an existing record instead.
Line 3: You're creating a duplicate record. We recommend you use an existing record instead.
 
 
We strongly recommend that you check a few of your imported records to verify that your information was imported as expected.
 
If you encounter any problems or have any questions, please contact us by clicking Help & Training at the top right of any salesforce.com page and choosing the My Cases tab.
 
Thank you!
 
Customer Support
salesforce.com
=================================================
