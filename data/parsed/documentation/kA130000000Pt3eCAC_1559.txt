### Topic: Chatter Feed Search Results | Salesforce
Chatter Feed Search Results | Salesforce
Chatter Feed Search Results
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Watch a Demo: 
Search in Salesforce Classic
Search results for Chatter feeds display posts and comments that include your search terms.
Chatter feed searches aren’t affected by your search scope; Chatter feed search results include matches across all objects.
Note
Changes to record fields aren't included in search results. For example, if you've enabled feed tracking on the 
Billing Address
 field for the Acme account and modified the Acme billing address, search results for 
Acme
 include 
Acme — Suzanne Powell
 Looking for volunteers to help with the Acme account
, but don't include 
Acme — Suzanne Powell
 changed Billing Street to 10 Main Road.
Sort your search results by posts only or by posts and comments, just as in feeds.
Comment on, like, share, bookmark, and delete posts in the search results, just as in feeds.
Add, edit, or delete topics on posts in the search results, just as in feeds.
Follow, share, download, preview, and upload new versions of files in the search results, just as in feeds.
View the detail pages for Chatter files, groups, topics, and people by clicking the respective name in the update.
View a single feed update
 by clicking the timestamp below the update, for example, 
Yesterday at 12:57 AM
.
Click 
Add to Favorites
 to save a Chatter feed search to your 
favorites
 on the Chatter tab
.
To view search results for records, such as accounts, contacts as well as Chatter people, groups, topics, and files, click 
Records
 at the top left of the page.
Tip
For a more focused search of the feeds in a specific group, profile, record, or other Chatter feed, use 
feed search
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_feed_search_results.htm&language=en_US
Release
202.14
