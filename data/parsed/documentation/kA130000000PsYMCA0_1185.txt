### Topic: Article and Translation Import and Export Status | Salesforce
Article and Translation Import and Export Status | Salesforce
Article and Translation Import and Export Status
Find the status of your article imports and exports on the Article Imports page in Setup.
Available in: Salesforce Classic
Salesforce Knowledge is available in 
Performance
 and 
Developer
 Editions and in 
Unlimited
 Edition with the Service Cloud.
Salesforce Knowledge is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions.
To check the status of your imports and exports, from Setup, enter 
Article Imports
 in the 
Quick Find
 box, then select 
Article Imports
. If you've enabled multiple languages for Salesforce Knowledge, you see two tables: one for article and translation imports and another for exports for translation.
Import information includes:
Possible actions
.Zip file names
Who submitted it and when
Status
Started and completed dates
Article types
Export information includes:
Possible actions
Zip file names
Who submitted it and when
Status
Started and completed dates
Status descriptions are as follows:
Status
Description
Possible Action
Pending
The import or export will start as soon as the previous pending import or export completes.
You can click 
Cancel
 to cancel the import or export.
Processing
The import or export is processing.
If you want to stop the process, or if the process has been stopped, call Salesforce Support. Salesforce may stop an import or export if a maintenance task has to be performed or the import or export exceeds one hour.
Stopping/Stopped
Salesforce Support is stopping or has already stopped the import or export.
Contact Salesforce Support to restart the import or export, or click 
Cancel
 to cancel an entry.
Aborted
The import or export has been canceled. The articles that already imported or exported successfully are available in Salesforce.
You can restart an import or export, click 
Del
 to delete an entry, or click 
Email Log
 to receive the completion email and check the details of your import or export.
Completed
The import or export is complete.
Successfully imported articles are visible on the Article Management tab on the 
Articles
 subtab. Successfully imported translations are visible on the Article Management tab on the 
Translations
 subtab.
This status doesn't mean the import or export is successful. Click 
Email Log
 to see the log file attached to the completion email and check the details of your import or export.
Click the exported .zip file name to save or open the file on your system.
Parent topic:
 
Import Existing Information into Salesforce Knowledge
Previous topic:
 
Create an Article .zip File for Import
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=knowledge_article_importer_05status.htm&language=en_US
Release
202.14
