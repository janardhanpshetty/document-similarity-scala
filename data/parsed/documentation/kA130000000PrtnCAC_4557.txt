### Topic: Can I import into custom fields? | Salesforce
Can I import into custom fields? | Salesforce
Can I import into custom fields?
Yes. Your administrator must create the custom fields prior to import.
For checkbox fields, records with a value of 
1
 in the field are imported as checked while a value of 
0
 is not checked.
See Also:
Importing Records
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_import_general_can_i_import.htm&language=en_US
Release
202.14
