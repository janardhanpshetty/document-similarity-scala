### Topic: How to make the field required to default to the only value.
I have a picklist field that only has one value assigned to it (through field dependency).  How do I auto-fill it?  It currently defaults to "--None--" and we have to select the value manually.
Resolution
There is an idea for the topic in the IdeaExchange website 
here
.
As a workaround to default a picklist field to a specific value, when only one value exists or only one value is available through a field dependency, we have to make the field required on the page layout.
To make a field required on the page layout, please follow the steps below:
Navigate to the page layout of your object
Edit the desired page layout - More on 
Editing Page Layouts for Standard Objects
 or for 
Custom Objects
Click on the wrench icon related to the picklist field
Set as Required
Save it
