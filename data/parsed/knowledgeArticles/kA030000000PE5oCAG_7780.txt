### Topic: This article provides the development guidelines for APEX Code in 62 Org.
INTERNAL - APEX code development guidelines for 62 org
Resolution
      Here is the Apex Coding guidelines for us to share with internal and external developers, please follow these guidelines when working on projects for 62 org:
 
·
         
The Apex code should follow best practice guidelines as documented in API reference guide (
http://www.salesforce.com/us/developer/docs/apexcode/salesforce_apex_language_reference.pdf
) 
·
         
Always try to use the latest version of the API available GA. This ensures optimum performance and supports latest methods. 
·
         
Keep in mind the order of execution when coding trigger. This is important as this may impact some other behavior already at the object level against which you are coding. Here is order of execution:
o
        
Triggers and Order of Execution
When a record is saved with an insert, update, or upsert statement, the following events occur in order:
1. The original record is loaded from the database (or initialized for an insert statement)
2. The new record field values are loaded from the request and overwrite the old values
3. All before triggers execute
4. System validation occurs, such as verifying that all required fields have a non-null value, and running any user-defined
validation rules
5. The record is saved to the database, but not yet committed
6. The record is reloaded as required for Apex and/or workflow rules
7. All after triggers execute
8. Assignment rules execute
9. Auto-response rules execute
10. Workflow rules execute
11. Escalation rules execute
12. All DML operations are committed to the database
13. Post-commit logic executes, such as sending email
·
         
The Apex code must have 92% or above code coverage using test methods. 
·
         
All the execution logic should be in Apex classes and the trigger should invoke the classes as required. 
·
         
The triggers must support bulk inserts and updates on the object the trigger is coded.
·
         
Proper commenting should be used across all the sub-sections of the code to clearly document the behavior of that sub-section. 
·
         
Any calls/reference made to external web services should be clearly documented in the header of the class. 
·
         
Apex Code must provide proper exception handling
o
        
The apex app does what the business wants when things go wrong. e.g. if the update limit is hit what should the program do. This has to be excepted by the business. 
·
         
If the apex code has explicit dependencies on other objects, fields like formula fields, then it should be clearly stated in the code as comments. 
·
         
Proper care and testing must be taken to ensure that any updates made by Trigger have expected behavior on the triggered workflow rules, validation Rules, assignment rules etc.
 
In case of any apex limits questions, developers are advised to discuss with Mike Fullmore (Bus Ops) in detail.
