### Topic: This articles describes the steps to be done to help resolve the issue of when the Nonprofit Starter Pack 3.0 is already installed within the instance but is not automatically upgrading to the latest version (as designed for NPSP 3.0)
Organization is currently using the Nonprofit Starter Pack 3.0 and yet it is not upgrading automatically to the latest version. For example customer has NPSP 3.51 installed but it has not upgraded to version 3.74. If you are not certain which version of the Nonprofit Starter Pack (NPSP) you have installed please click 
here
. If you are are using NPSP 1.0 or 2.0 click 
here
.
 
Resolution
This issue sometimes occurs when the organization upgraded from the past from an older version of NPSP (1.0 or 2.0). When this happens you will need to run the NPSP 3.0 Installer again. You will need to do the following:
Log into your instance of Salesforce where the Nonprofit Starter Pack 
Go to the 
NPSP Installer page
 by clicking on the following link: 
http://mrbelvedere.salesforcefoundation.org/mpinstaller/npsp
 
Click on the button "Log In" and choose the instance  you want to upgrade and then click on your username
Wait for the install to collect information. Scroll down the page and click on the button "Install".
A pop up will appear "Confirm Installation", in the box you will need to type out your username. Then click on "install"
A bar will appear above called "Pending". You will need to wait (it can take some time, but usually not long because you already have NPSP 3.0 installed). If any errors appear follow the messages to correct the issues.
When done it will say Succeeded, and you're done.
 
