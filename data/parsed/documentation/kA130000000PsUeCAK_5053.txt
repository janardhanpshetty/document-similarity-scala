### Topic: Create a Salesforce Classic Dashboard | Salesforce
Create a Salesforce Classic Dashboard | Salesforce
Create a Salesforce Classic Dashboard
Create a dashboard to provide a graphical view of the data in your reports.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create dashboards:
“Run Reports” AND “Manage Dashboards”
To edit and delete dashboards you created:
“Run Reports” AND “Manage Dashboards”
To edit and delete dashboards you didn’t create:
“Run Reports,” “Manage Dashboards,” AND “View All Data”
To create, edit, and delete dynamic dashboards:
“Run Reports” AND “Manage Dynamic Dashboards”
This topic is about creating dashboards for reports in Salesforce Classic. For information on creating dashboards in Wave Analytics, review the article 
Create a Dashboard in Wave Analytics
.
Tip
You can clone a dashboard to quickly create a new dashboard with the same properties and components as the one you're viewing. Click 
Clone
, modify the dashboard settings, and save.
Create the custom reports containing the data you want to display. 
Important
Be sure to store these reports in folders that your intended dashboard viewers can access.
Click the Dashboards tab.
Click 
Go To Dashboard List
.
Click 
New Dashboard
 to create a new dashboard.
To modify an existing dashboard, click its name from the list.
Customize your dashboard and click 
Save
.
Parent topic:
 
Build a Dashboard
Next topic:
 
Working with Dashboards
See Also:
Delete a Dashboard
Install the CRM Sample Dashboards from AppExchange
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dashboards_create.htm&language=en_US
Release
202.14
