### Topic: Here's how you would delete or inactivate coaching relationships
How can I delete a coaching relationship?
Resolution
Since coaching relationships are often part of the employee record, most users won’t be able to delete a coaching relationship unless they have the Delete permission on the Coaching object. However, they can mark a coaching relationship as inactive so that they can keep their coaching lists focused:
 
 
Admins or other users with the Delete permission on Coaching can delete coaching relationships by navigating to the specific coaching relationship and hitting the 
Delete
 button.
