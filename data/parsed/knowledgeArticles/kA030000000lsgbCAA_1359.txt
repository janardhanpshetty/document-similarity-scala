### Topic: If you add a social account and then changes their avatar afterwards (days, months, or years later), Social Studio does not get notified of that changed and as such the avatar does not update. If the avatar was changed long enough ago that the urls that we reference for the avatars is no longer displaying the old avatar, it returns nothing and the image appears broken. If you revoke access from twitter and facebook and re-authorize the social accounts in social studio, we should request the new avatars and update them (though it won't be instant due to caching )
Why does the avatar for a user or social account sometimes just display as a broken image in Social Studio?
Resolution
If you add a managed account and then changes your avatar afterwards (days, months, or years later) Social Studio does not get notified of that changed and as such that avatar does not update. If the avatar was changed long enough ago that the URLs that referenced the original avatar is no longer displaying the old avatar, it returns nothing and the image appears broken.
If you revoke Salesforce Marketing Cloud access from Twitter and re-authorize the social account in Social Studio, Social Studio will request the new avatars and update them. Please note that it can take some time for the avatars to update after reconnecting the accounts. If you disconnect and reconnect your social accounts in Social Studio, you may lose some or all of your historical content for the social account. For best results, reconnect your social account in Social Studio immediately after you revoke access.
 
Related Topics:
Twitter Help Center: How to revoke an application 
Social Studio: Adding a Twitter Account
