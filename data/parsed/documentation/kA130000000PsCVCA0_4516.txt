### Topic: Opportunity Fields | Salesforce
Opportunity Fields | Salesforce
Opportunity Fields
The fields for opportunities contain a wide variety of information to help you track your pending and potential sales. Depending on your page layout and field-level security settings, some fields may not be visible or editable.
Available in: Salesforce Classic and Lightning Experience
The available fields vary according to which Salesforce Edition you have.
Field
Description
Account Name
Name of account that opportunity is linked to. You can enter the account name, or select the account using the lookup icon.
If you change the account for an opportunity that has partners, all partners are deleted from the Partners related list.
Amount
Estimated total sale amount. For organizations using multiple currencies, the amount is shown in your personal currency by default. Change the 
Opportunity Currency
 picklist to track the amount in another currency.
For opportunities with products, the amount is the sum of the related products. You cannot directly edit the amount unless the opportunity has no products. To change the amount for an opportunity that contains products, edit the sales price or quantity of the related products.
If you change the price book for an opportunity that has products, all products are deleted from the Products related list, but the value in the opportunity’s Amount field remains.
Close Date
Date when you plan to close the opportunity. You can enter a date, or choose a date from the calendar that displays when you put your cursor in the field.
Opportunities with a 
Close Date
 in a given month will tally in the forecast for that month, unless you assign them to the Omitted category while editing the forecast.
When you set an open opportunity's 
Stage
 to a type of “Closed/Won,” the 
Close Date
 is set to the current date in Coordinated Universal Time (UTC). At certain times of day, UTC will differ by one day from your time zone.
Contract
Contract that the opportunity is linked to.
Created By
User who created the opportunity including creation date and time. (Read only)
Custom Links
Listing of custom links for opportunities as set up by your administrator. Available only in Salesforce Classic.
Description
Description of the opportunity. Up to 32KB of data are allowed. Only the first 255 characters display in reports.
Expected Revenue
Calculated revenue based on the 
Amount
 and 
Probability
 fields.
Forecast Category
Forecast category name that is displayed in reports, opportunity detail and edit pages, opportunity searches, and opportunity list views. The setting for an opportunity is tied to its 
Stage
. For more information on forecast categories, see 
Working with Forecast Categories 
.
Last Modified By
User who last changed the opportunity fields, including modification date and time. This does not track changes that are made to any of the related list items on the opportunity. (Read only)
Lead Source
Source of the opportunity, for example, Advertisement, Partner, or Web. Entry is selected from a picklist of available values, which are set by an administrator. Each picklist value can have up to 40 characters.
Next Step
Description of next task in closing opportunity. Up to 255 characters are allowed in this field.
Opportunity Currency
The default currency for all currency amount fields in the opportunity. Amounts are displayed in the opportunity currency, and are also converted to the user’s personal currency. Available only for organizations that use multiple currencies.
For opportunities with products, the currency is tied to the currency of the associated price book. You cannot directly edit this field unless you first delete the products.
Opportunity Division
Division to which the opportunity belongs. This value is automatically inherited from the related account.
Available only in organizations that use divisions to segment their data.
Opportunity Name
Name of the opportunity, for example, Acme.com - Office Equipment Order. Up to 120 characters are allowed in this field.
Opportunity Owner
Assigned owner of opportunity. Not available in Personal Edition.
Opportunity Record Type
Name of the field that determines what picklist values are available for the record. The record type may be associated with a sales process. Available in Professional, Enterprise, Unlimited, Performance, and Developer Editions.
Partner Account
Read-only field that indicates the opportunity is owned by a partner user.
Primary Campaign Source
Name of the campaign responsible for generating the opportunity.
For opportunities created during lead conversion, this field is automatically filled in with the campaign name from the lead. If the lead has multiple associated campaigns, the campaign with the most recently updated member status is inserted into the opportunity.
For opportunities with multiple influential campaigns, click 
Edit
 next the primary campaign in the Campaign Influence related list on the opportunity detail page and select the 
Primary Campaign Source
 checkbox. The campaign will display in the 
Primary Campaign Source
 field on the opportunity.
Private
Indicates that the opportunity is private. Only the record owner, users above that role in the hierarchy, and administrators can view, edit, and report on private opportunities. Not available in Group or Personal Editions. Private opportunities do not trigger 
Big Deal Alerts
 or workflow rules. Private opportunities count towards your forecasts only if you have Customizable Forecasting or Collaborative Forecasts enabled. Users with the “View All Forecasts” permission can see private opportunities within the Forecasts tab.
Note
When you mark opportunities 
Private
, any opportunity teams, opportunity splits, and sharing are removed. (The owner of a private opportunity receives 100% of split types that total that amount.)
Probability
Likelihood that opportunity will close, stated as a percentage.
The 
Probability
 value is always updated by a change in the 
Stage
 value, even if 
Probability
 is marked as read only on your page layout. Users with access to edit this field can override the value.
Quantity
Total of all 
Quantity
 field values for all products in the Products related list if the opportunity has products. General use field if the opportunity does not have products.
Stage
Current stage of opportunity based on selections you make from a predefined list, for example, Prospect or Proposal. Entry is selected from a picklist of up to 100 available values, which are set by an administrator. Each picklist value can have up to 40 characters.
Your administrator correlates the values in this picklist with Forecast Category values that determine how the opportunity contributes to your forecast.
When you set an open opportunity's 
Stage
 to a type of “Closed/Won,” the 
Close Date
 is set to the current date in Coordinated Universal Time (UTC). At certain times of day, UTC will differ by one day from your time zone.
Synced Quote
Quote that’s synced to the opportunity. Only one quote can sync to the opportunity at a time.
Territory
Territory with which the opportunity is associated. Only available if your organization has territory management.
Type
Type of opportunity, for example, Existing Business or New Business. Entry is selected from a picklist of available values, which are set by an administrator. Each picklist value can have up to 40 characters.
See Also:
Opportunity Product Fields
Opportunities
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=opp_fields.htm&language=en_US
Release
202.14
