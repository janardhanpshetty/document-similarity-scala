### Topic: Currently, it is not within standard Salesforce functionality to save reports to a standard report folder.
Why am I unable to save a custom report to a standard report folder?
I have all the necessary permissions to save and edit reports as well as the permissions to create and edit report folders.
Resolution
Currently, it is not within standard Salesforce functionality to save reports to a standard report folder. These folders are available as read-only folder which allow users to create custom reports based of these examples. To save the reports you will want to save them to a custom report folder. For information on how to create custom report folders follow the link provided below.
See Also:
Standard Report 
Types
 
Creating and Editing Folders
 
Managing Folders
 
 
