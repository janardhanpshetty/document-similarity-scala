### Topic: Error - Batch failed: FeatureNotEnabled: Binary field not supported
While exporting content related object 
(ContentVersion)
 and attachments object 
(Attachment)
 or Feed Item 
(Content Data field)
 we receive an error:
Error: 
Batch failed: FeatureNotEnabled: Binary field not supported
Resolution
What is Base64 data type?
Base 64-encoded binary data. Fields of this type are used for storing binary files in Attachment records, Document records, and Scontrol records. In these objects, the Body or Binary field contains the (base64 encoded) data, while the BodyLength field defines the length of the data in the Body or Binary field. In the Document object, you can specify a URL to the document instead of storing the document directly in the record.
Objects such as attachments object 
(Attachment)
 or Feed Item 
(Content Data field)
 contains binary data encoded in Base64.
While exporting such data, if 
"Bulk API
" is enabled, we receive the error "
Batch failed: FeatureNotEnabled: Binary field not supported
". This is because extraction of items of  type Base64 fields is not supported using bulk API.
Base64 type of fields can be extracted using SOAP API.
While exporting data using API such as using Data Loader, Bulk API option must be disabled to avoid the error.
 
