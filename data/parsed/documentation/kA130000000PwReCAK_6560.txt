### Topic: Searchable Fields: Document | Salesforce
Searchable Fields: Document | Salesforce
Searchable Fields: Document
Available in: Salesforce Classic
Available in: 
All
 editions except 
Database.com
To find a document, use global search or the 
Find Document
 button on the Documents tab. Neither sidebar search nor advanced search are designed to find documents.
Searchable Fields
Sidebar Search
Advanced Search
Standard Lookup Search
Global Search
Documents Tab
Name
Body
Keywords
All standard text fields
All custom auto-number fields and custom fields that are set as an external ID
(You don't need to enter leading zeros.)
All custom fields of type text, text area, long text area, rich text area, email, and phone
See Also:
Searchable Fields by Object in Salesforce Classic
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_fields_document.htm&language=en_US
Release
202.14
