### Topic: Learn how we catch exceptions and increase timeout while making callout to a third party API
While making a callout to third party web service using HTTPS request, we can implement exception handling to catch exceptions and action accordingly. If the error is a timeout error there are ways to increase the timeout as well.
Resolution
Catch the CalloutException
 
try { 
//Execute web service call here 
HTTPResponse res = http.send(req); 

//Helpful debug messages 
System.debug(res.toString()); 
System.debug('STATUS:'+res.getStatus()); System.debug('STATUS_CODE:'+res.getStatusCode()); 
} catch(System.CalloutException e) { 
//Exception handling goes here.... retry the call, whatever 
}
Callout timeout is configurable in Apex Code (up to a maximum setting for a single callout).
Example of setting a custom timeout for HTTP callouts using the Apex HTTPRequest object
 
1	HttpRequest req = new HttpRequest();
2	req.setTimeout(2000); // timeout in milliseconds
The default timeout is 10 seconds. The minimum is 1 millisecond and the maximum is 120 seconds. If the callout is timing out, please try and increase the timeout on the HTTP request to avoid that.
