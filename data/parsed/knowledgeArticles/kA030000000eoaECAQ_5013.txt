### Topic: Describes the process to create and assign Permission Sets for Sales Wave.
Wave products all follow a similar structure of "Permission Set License" plus "Permission Set" to manage access to the Wave environment. Once Permission Set Licenses for Sales Wave users have been assigned, the next step is to create and assign Permission Sets to target users.
Before Creating Permission Sets
Review your users' needs to determine the structure of Permission Sets you'll be creating.
Permissions
Generally, Sales Wave users will fall into one of two roles: Manager and User.
A Manager Permission Set will need the following Permissions:
"Access Sales Cloud Analytics Templates and Apps" - Can create and use apps from Sales Cloud Analytics template.
"Edit Wave Analytics Dataflows" - Edit the Wave Analytics dataflow and view errors related to it.
"Manage Wave Analytics Templated Apps" - Can create and manage apps based on available app templates.
"Use Wave Analytics Templated Apps" - Can view apps based on available app templates.
A User Permission Set will need the following Permissions:
"Access Sales Cloud Analytics Templates and Apps" - Can create and use apps from Sales Cloud Analytics template.
"Use Wave Analytics Templated Apps" - Can view apps based on available app templates.
If Managers and Users need to download data from lenses and dashboards, they will need the following Permission:
"Download Wave Analytics Data" - Download screenshots and data in tabular format through the Wave Analytics user interface.
Resolution
Creating and assigning a Sales Wave Permission Set
Follow these steps to assign the needed Permission Set to target users:
1. In Setup under the Administer section, go to Manage Users, then the sub-section "Permission Sets". 
2. Click "New". 
3. Enter a Label. Something descriptive like "Sales Wave Manager" or "Sales Wave User" is recommended. 
4. The API Name should populate after you click out of the Label field. Enter a Description if you wish. 
5. Leave the User License field set to "--None--".
6. Click "Save". 
7. On the Permission Set screen, in the System section, click "System Permissions". 
8. Click "Edit". 
9. Select the needed permissions.
10. Click "Save". 
11. Click "Manage Assignments". 
12. Click "Add Assignments". 
13. Locate the target user(s) in the list and check the box for the user(s). 
14. Click "Assign".
Repeat these steps for each Permission Set.
Related Wave Setup Content:
Enable Wave Analytics
Assigning Permission Set Licenses for Wave Analytics
Create and Assign Wave Analytics Platform Permission Sets
Create and Assign Sales Wave Permission Sets
Create and Assign Service Wave Permission Sets
