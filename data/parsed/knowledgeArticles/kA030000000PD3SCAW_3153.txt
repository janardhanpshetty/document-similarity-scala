### Topic: You might receive this error while sending mail through a trigger in a recently refreshed sandbox. Reasons could be: Trial accounts do not allow the use of mass email; You are missing a permission in the backend; The access level might have been set to "system e-mails".
Why we get the following error when sending mail through a trigger "System.EmailException: SendEmail failed. First exception on row 0; first error: NO_MASS_MAIL_PERMISSION, Single email is not enabled for your organization or profile. Single email must be enabled for you to use this feature.: []" 
The trigger is executed by a user with System Administrator profile, both Mass mail and Single Mail are enabled for the profile.
 
Resolution
The reasons are :
 
>>Trial accounts do not allow the use of mass email. You will have to purchase a license to go live with your org.
 
>>You are missing a permission in the backend and must log a case with Salesforce support to enable "Respect SendEmail API" and "SendEmail API".
 
>>The access level might have been set to "system e-mails".
 In setup | Administration setup | Email Administration | Deliverability, verify if the access level is set to "All e-mails".
 
The e-mail deliverability is a new feature released in spring 13 and as per the release notes, all newly refreshed sandboxes default to "system email only"
 
Page number 46 in the link below :
https://help.salesforce.com/HTViewHelpDoc?id=data_sandbox_implementation_tips.htm&language=en_US
