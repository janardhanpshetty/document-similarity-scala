### Topic: This article says how to use the Percentage fields in a Process Builder Field updates. The Percentage fields when used directly in Process Builder will not fetch the correct information.
This article says how to use the Percentage fields in a Process Builder Field updates. The Percentage fields when used directly in Process Builder will not fetch the correct information.
Hence, for this requirement to be accomplished, we need to first create a Custom Formula field with Number as the Return Type. Then, use the Percentage field in the Formula Merge field. And this formula field can then be used as the criteria in Process Builder setup.
 
Resolution
Below are the steps to create a 
Custom Formula field
:
For 
Standard Objects
, Go to 
Setup | Customize | (Object Name) | Fields
. 
For 
Custom objects
, please Go To 
Setup | Create| Objects | (Object Name)
.
Click on 
New 
in 
Custom Fields & Relationships
.
Select the 
Data Type
 as 
Formula
 and click on 
Next
.
In the New window, provide the 
Field Label
.
Select the 
Formula Return type
 as 
Number
.
Set 
the 
Decimal Places
 if required.
Click on 
Next
.
In the Formula edit Layout, click on 
Insert Field button
.
The 
select 
the 
Percentage field 
to be used in Process Builder.
Click on 
Next
.
Assign 
the field as 
visible for 
the 
required 
set of 
Profiles 
and click on 
Next
.
Add 
the field to the
 Page Layouts
, but this can be hidden in the Layout as we will use this only for the Process Builder requirements.
Click on Save.
The New Formula field is created and available for use.
Below are the steps, to use the Formula field in Process Builder Criteria:
Go to 
Setup | Create | Workflows & Approvals | Process Builder
.
Click on 
New 
Button on the top right corner of the page.
Enter 
the 
Process Name and Description 
(if required).
Click on 
Save
.
Set
 the 
Evaluation criteria and Field Criteria
 for the Process Builder.
Instead of
 referring the 
Actual percentage field
 in the Formula for field updates, 
refer
 the 
New Formula field
 that is been created.
For Example:
If a Probability Amount of Opportunity needs to be calculated, then in Process Builder 
instead of
 using the Formula as 
[Opportunity].Probability * [Opportunity].Amount, 
use 
the formula as 
[Opportunity].NewField * [Opportunity].Amount.
Activate the Process Builder by clicking on the Activate Button on the Right side top corner of the page.
This Process Builder now can update the fields with the correct value.
