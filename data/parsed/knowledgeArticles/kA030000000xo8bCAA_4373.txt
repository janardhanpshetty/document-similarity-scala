### Topic: The Process Builder allows you to update child records whenever the parent record is modified.
How do I update child records from its parent using Process Builder?
Resolution
The 
Process Builder
 allows you to update child records whenever the parent record is modified. Here's how: 
Create a 
new Process
. 
Select the Record you would like to update. Example record in account Object. 
Define the 
Criteria. 
In "Immediate Action," select 
Update Records. 
Click on the "Object" drop down: 
Select the Account record that started your process a
nd 
Select a record related to the Account:
 
These are radio buttons and only one can be selected and to update child records you need to select send option “Select a record related to the Account”
In "Find a field," look for the child object name you want to update.
If child object is a Custom Object the name would contain "__r" at the end.
Select the Child Object name, and then click 
Save. 
In "Set new value
"
 section, define the field that you want to update in child records and what value it should contain
Click 
Save.
Similar steps can be used to update lookup fields using process builder too.
