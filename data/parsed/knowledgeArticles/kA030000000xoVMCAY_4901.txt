### Topic: Send Spreading is an automatic feature for Email Sends that are sending to greater than 25,000 Subscribers when Send Throttling is not being used.
Send Spreading is an automatic feature for Email Sends that are sending to greater than 25,000 Subscribers when Send Throttling is not being used. It is not user-initiated.
Marketing Cloud processes 500 Subscribers per batch, which means it sends the email through the Mail Transfer Agent(s) to 500 Subscribers at a time. Send Throttling and Send Spreading ensure the email Sends stay within these limits.
Resolution
 
Send Throttling
Send throttling allows you to select the range of hours during which these batches are sent and a volume per particular time period. If a volume limit is not specified, the throttle ensures the system does not send a greater number than the throttle limit during the specified time period. Using Send Throttling will only use one slot to do the Subscriber batch sending.
 
Send Spreading
When Send Throttling is not used, the volume limits still need to be managed. An Email Send to more than 25,000 Subscribers will automatically use Send Spreading to ensure the volume of Sends is under the limit by sending the email batches in multiple slots. If the Email Send is to less than 25,000 Subscribers, it will use only one slot to do the Subscriber batch sending.
 
