### Topic: Steps to delete a self, peer or skip level summary.
Although it's possible to delete these summaries once a self summary has been deleted for a cycle it cannot be re-added.
Resolution
1. Log in as a Salesforce Admin in Workbench (
https://workbench.developerforce.com/login.php
) 
2. From the top menu choose 
queries | SOQL Query
 and paste the following in the 
Enter or modify a SOQL query below
 text-area: 
 
SELECT Id, Name from WorkPerformanceCycle WHERE Name = 'Peer Test'
Be sure to replace the 'Peer Test' text with the name of your performance cycle 
3. Hit the Query button and copy the value in the Id column 
4. Paste the following query in the 
Enter or modify a SOQL query below
 text-area: 
 
SELECT FeedbackRequestState,FeedbackType,Id,PerformanceCycleId,QuestionSetId,RecipientId FROM WorkFeedbackRequest WHERE FeedbackRequestState = 'Draft' AND FeedbackType = 'CycleSelf' AND PerformanceCycleId = 'ID COPIED IN STEP 3' AND RecipientId = 'ID OF USER WHO NEEDS TO BE ASSIGNED A PERFORMANCE REVIEW' AND SubjectId = 'ID OF THE USER THE PERFORMANCE REVIEW IS ABOUT'
Be sure to replace CycleSelf with CyclePeer if you want to delete peer summaries.  If you want to delete skip level summaries replace CycleSelf with CycleSkip.
To get Id's of users you can visit their detail page and copy it from the web browser's URL:
5. Hit the Query button 
Only one record should be returned here. 
6. Click on the value in the Id column to be redirected to the request then hit the Delete button. After doing that the self (or peer or skip-level) summary should be deleted for the user. 
