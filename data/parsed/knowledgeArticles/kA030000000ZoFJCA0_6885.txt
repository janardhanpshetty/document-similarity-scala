### Topic: A summary of how to have Checkout Order Center Access enabled for your organization.
Available in:  
Contact Manager
. 
Group
. 
Enterprise, Performance, Unlimited 
and 
Developer 
Editions
Under certain scenarios the 
Check-Out Order Access 
feature may not be available for your Organization. Salesforce Support can help activate this feature. Prior to making this request it's best to review the following documentation:
What is Checkout & how can I access it?
Why am I unable to activate Checkout Enabled?
Note:
 This feature is not available for Foundation Organizations. 
 
Resolution
If you've reviewed all relevant documentation and would like to move forward with requesting this feature be enabled, please take the following steps: 
 
1. Have a System Administrator log a Case with Salesforce Support. 
2, Please include all important details, including the Organization ID where this needs to be enabled.
3. Support will review the Case and action it as needed. 
