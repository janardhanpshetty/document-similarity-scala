### Topic: Delete a List View in Salesforce Classic | Salesforce
Delete a List View in Salesforce Classic | Salesforce
Delete a List View in Salesforce Classic
You can delete a custom view when you no longer need it.
Available in: Salesforce Classic
Available in: 
All 
Editions
User Permissions Needed
To delete public list views
“Manage Public List Views”
Note
These steps work in Salesforce Classic. 
If you see a row of tabs across the top of your screen, you're in Salesforce Classic. If you see a navigation bar on the left, you’re in Lightning Experience
.
Select a custom list view.
Click 
Edit
.
Click 
Delete
.
See Also:
Create a List View in Salesforce Classic
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=custom_del.htm&language=en_US
Release
202.14
