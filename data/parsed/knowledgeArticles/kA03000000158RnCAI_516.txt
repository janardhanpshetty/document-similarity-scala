### Topic: Gus number for lightning
For Tier 3 and other reps who can access our 
GUS Org
, please refer to the following extensive known issue list before logging an investigation in GUS. This list will be up to date:
https://gus.lightning.force.com/one/one.app#/sObject/00OB0000000rsx0/view?t=1440203576525
 
Resolution
