### Topic: Process Formula Limitations | Salesforce
Process Formula Limitations | Salesforce
Process Formula Limitations
Formulas that are used as conditions in a criteria node have some limitations. If a process contains an invalid formula, you can’t save or activate the process.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
All formulas that are used in a criteria node must:
Return 
true
 or 
false
. If the formula returns 
true
, the associated actions are executed.
Not contain more than 3,000 characters.
Not contain an unsupported function.
Tip
Parentheses aren't included automatically when you insert a function. Be sure to add parentheses, for example, 
TODAY()
, when building a formula.
Unsupported Functions
If a formula in a process uses any of the following functions, the formula will return 
null
.
GETRECORDIDS
IMAGE
INCLUDE
PARENTGROUPVAL
PREVGROUPVAL
REQUIRE SCRIPT
VLOOKUP
For a complete list of operators and functions for building formulas in Salesforce, see 
Formula Operators and Functions
.
Note
If your process criteria uses a formula, don’t create a formula that always evaluates to true, such as 
2 < 5
.
ISCHANGED is available as both a formula function and as an operator. When it’s used as a formula function in process criteria, you can’t reference a child record’s related fields. For example, ISCHANGED isn’t supported when referencing a 
[Case].Contact.AccountId
 field, but it can be used when referencing 
[Case].ContactId
.
See Also:
Tips for Working with Picklist and Multi-Select Picklist Formula Fields
Process Builder Advanced Option Considerations
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=process_criteria_formula.htm&language=en_US
Release
202.14
