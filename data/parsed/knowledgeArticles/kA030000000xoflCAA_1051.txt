### Topic: This article would help you understand the Summer 15 functionality of Chatter files including @mentions when sharing files with groups.
Include @Mentions When Sharing Files with Groups
When you share a file with groups in 
Chatter
, now you can @mention people and groups in your message.
When you use 
Share with Groups
 to share a file from anywhere in 
Chatter
, you can now include @mentions in your message. This notifies the @mentioned people and groups that you have shared the file.
Resolution
