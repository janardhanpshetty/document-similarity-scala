### Topic: Salesforce A ----Callout----> Salesforce B ----Callout----> Webservice If an external system initiates a Callout from Org A and that Callout invokes another callout from Org B we see the 'Callout Loop Not Allowed' error. We can make a Callout to a salesforce Org from another salesforce Org, but you cannot do it from within another Callout.
"System.CalloutException: Callout loop not allowed"  is usually seen when a callout originates from another callout. 
Salesforce A  ----Callout----> Salesforce B  ----Callout----> Webservice
If an external system initiates a Callout from Org A and that Callout invokes another callout from Org B we see the 'Callout Loop Not Allowed' error. We can make a Callout to a salesforce Org from another salesforce Org, but you cannot do it from within another Callout.
Resolution
1. In Org B create a Webservice class AccountPlan  making a callout to another Webservice 
For example Org B is making a Callout to the calculator webservice to add two integers calculator.wsdl attached to the article. 
2. Generate WSDL for class AccountPlan . Call it OrgB.wsdl
3. In Org A, use OrgB.wsdl to create WSDL2Apex Class soapSforceComSchemasClassAccplan
4. Add Org A add https://
OrgBInstance
.salesforce.com/services/Soap/class/AccountPlan to remote site settings 
5. In Org A create the Rest class CallAccountPlan to initiate the Rest call
@RestResource(urlMapping='/createAccountPlan/*')
global with sharing class CallAccountPlan{
    
    @HttpGet
    global static String doGet(){
        soapSforceComSchemasClassAccplan.AccountPlan stub = new soapSforceComSchemasClassAccplan.AccountPlan();
        stub.SessionHeader = new soapSforceComSchemasClassAccplan.SessionHeader_element();
        stub.SessionHeader.sessionId = 
Session Id
;
        return stub.createAccountPlan();
        }
}
6. Make a GET call /services/apexrest/createAccountPlan/ from REST explorer in Workbench
7. We see the error : message: System.CalloutException: Web service callout failed: WebService returned a SOAP Fault: System.CalloutException: Callout loop not allowed
This is because Org B is making a Callout upon initiation of Callout from Org A . 
For workarounds refer 
Article
 
