### Topic: NTERNAL ONLY knowledge article that summarizes the technology involved in the Informatica Cloud MDM application.
AppExchange Listing Information:
Partner:
  
Informatica
Application Name: 
Informatica Cloud MDM
AppExchange Listing Name / URL:
Informatica Cloud MDM:
 https://appexchange.salesforce.com/listingDetail?listingId=a0N30000003JRU8EAO
Partner Success Team: 
Joseph Ulep (SAM) / William Yeh (TE) / Nakul Mistry (AE)
PPS Entitlement:  
https://org62.my.salesforce.com/a0a30000000vF15
 
Application Overview:
Combined Product Description:
 Informatica Cloud uniquely provides true SaaS Data Integration and Data Quality Applications to address the data management needs of enterprises of all sizes.
Cleanse, Standardize, De-duplicate and Consolidate, Hierarchy manage your data in Salesforce.
Data Cleansing: Informatica Cloud MDM cleans data immediately whenever Accounts, Leads and Contacts are created or updated based on your corporate standards for customer data such as country names, ISO codes, street suffixes, and legal forms.
Deduplication: Informatica Cloud MDM automatically de-dupes duplicate Accounts, Leads or Contacts in Salesforce and other data sources using its configurable, fuzzy Duplicate Detection process. Cloud MDM also prevent users from creating duplicate records.
Consolidation: Cloud MDM extends your Salesforce with enterprise-grade Master Data Management. You can also consolidate / enrich your customer data across your other corporate databases, such as SAP, Oracle, Microsoft, or others. Also build Hierarchies.
Informatica Corporation provides data integration software and services that empower organizations to access, integrate, and trust all its information assets, giving organizations a competitive advantage in today’s global information economy. As the independent data integration leader, Informatica has a proven track record of success helping the world’s leading companies leverage all their information assets to grow revenues, improve profitability, and increase customer loyalty. That is why Informatica is known as the #1 independent Data Integration company.
Informatica Cloud uniquely provides true SaaS Data Integration and Data Quality Applications to address the integration needs of enterprise of all sizes. Our Cloud Applications are tightly integrated with the leading SaaS applications, enabling our services to operate quickly and seamlessly through this tightly integrated architecture.
Hierarchies , Hierarchy Management , Multi-Dimensional, Multi-Source, Multi-Type. Franchise management. Consolidation, Enrichment, Data Cleansing, De-dupe, Multi-Org Consolidation, Multiple Salesforce Environments, Multiple Orgs, Contact Validation.
 
Distribution Details:
 
Target Market Segment: 
MM (Mid-market); GB (Large business); ENTR (Enterprise -- 3,500+ employees)
Sold to: Existing customers
Distribution Mechanism: AppExchange and website
Target User Demographic:
 
Admin User - User will manage and configure the Master Data mappings, batch jobs and mass data loads.
Sales and Service CRM User - User receives the benefit of the clean, master data within their SFDC CRM app
 
Application Technology Overview:
Marketecture Diagram:
 
Solution Components:
Standard Objects Referenced: Contact, Account, Lead, Campaign
Number of Custom Objects: 1-10
Number of Reports: 21-30
Number of Classes: 21-30
Number of Triggers: 1-10
Number of Visualforce Pages: 1-10
ISVforce technology leveraged: Managed Package; Aloha; Push Upgrades; LMA; Trialforce
Is Force.com primary system of record: No
Other platforms used: N/A
Has managed package: Yes
Has composite Web App: No
Has native mobile app?: No 
100% native on Force.com Platform: No
Type of Application: Native with optional External webservices
Supported Editions: Enterprise, Unlimited / PXE
Approved for install into existing orgs? Yes 
Has Extension Packages? No 
Has Composite Web Services?: Yes
Has Desktop App: No
 
UI Architecture:
Supported UI Types: Desktop Browser-Based
Mobile Devices Supported - N/A (text field)
Developer tools used for browser UI: Visualforce/Apex; JS frameworks (JQuery, ExtJS, etc.)
% of app not using SFDC standard look / feel: 50%
 
Integration Methods:
Integration at UI layer: No
UI Integration Methods: N/A
Realtime integration required: Yes 
Realtime integration methods: Inbound; Outbound; 
Avg number of API calls in 24 hour period: Thousands
Batch Integration Required: Yes
Batch Integration Frequency: Several Times a day
Avg number of records per batch: Thousands
Batch ETL Methods: N/A
 
Large Data Concerns:
More than 20MB of data per user: No (checkbox)
Largest data set stored on single object: Millions
Describe queries run on large data sets: Large result sets required; Heavy inspection of result set required
Reporting on large data sets with SFDC tools? No 
Data Volume Concerns? This app has been in the market for many years now. They've been conducting continuous LDV testing. They are aware of large volume limitations.
 
Sample UI Screenshots:
 
 
Resolution
