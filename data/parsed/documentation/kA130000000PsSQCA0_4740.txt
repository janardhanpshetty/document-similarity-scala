### Topic: Viewing Your Salesforce for Outlook Matching Criteria | Salesforce
Viewing Your Salesforce for Outlook Matching Criteria | Salesforce
Viewing Your Salesforce for Outlook Matching Criteria
See the matching criteria your administrator assigned to you.
This feature available to manage from: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To access your Salesforce for Outlook configuration
Assigned to an active configuration
To view your configuration
Assigned to a configuration
If you have multiple Salesforce contacts that match a contact in Outlook, Salesforce for Outlook needs a way to determine which contact to sync. Your administrator sets criteria for the way Salesforce for Outlook chooses the correct contact.
Review the matching preference assigned to you. Available options include:
Most recent activity
—Choose the Salesforce contact that shows the most recent activity (such as a phone call or email), as shown in the contact’s Activity History related list. This option is the default matching criteria.
Last updated
—Choose the Salesforce contact that was most recently modified.
Oldest
—Choose the Salesforce contact that has the earliest creation date.
See Also:
Manage Your Salesforce for Outlook Configuration
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=outlookcrm_personal_config_matching.htm&language=en_US
Release
202.14
