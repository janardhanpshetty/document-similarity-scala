### Topic: Post to a Record’s Chatter Feed from a Process | Salesforce
Post to a Record’s Chatter Feed from a Process | Salesforce
Post to a Record’s Chatter Feed from a Process
Post to the feed of the record that started the process.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, or view processes:
“Manage Force.com Flow”
AND
“View All Data”
You can post to the record’s Chatter feed only if feed tracking is enabled for the object that the process is associated with. The feed item will appear as if the user who started the process—by creating or editing a record—created the post.
After you’ve created an action and selected “Post to Chatter” for the action type, fill in the relevant fields to add the action to your process.
Warning
If the feed that the process tries to post to isn't available when the process is triggered (for example, because the user is now inactive), the user sees an error and the process fails.
Enter a name for this action. 
This text appears on the canvas and helps you differentiate this action from others in your process. The name is truncated to fit on the canvas.
In the 
Post to
 field, select This Record.
Fill out the message that you want to post. You can insert merge fields, add a topic, and mention users or groups. 
The message can contain up to 10,000 characters.
You can only reference topics that already exist. If you reference a merge field and that field doesn’t have a value, it appears as a blank value.
Click 
Save
. 
See Also:
Chatter Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=process_action_post_record.htm&language=en_US
Release
202.14
