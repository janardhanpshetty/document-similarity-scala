### Topic: Contact Fields | Salesforce
Contact Fields | Salesforce
Contact Fields
Contacts have the following fields, listed in alphabetical order. Availability of fields depends on how your admin set up Salesforce.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
Field
Description
Account Name
The account that the contact is linked to. Enter the account name, select the account from a list, or create an account. Private contacts don’t have an account.
Allow Customer Portal Self-Registration
If you allow access to a customer portal, when selected, allows contacts to self-register for it.
Assistant
The contact’s assistant. Maximum 40 characters.
Asst. Phone
The assistant’s phone number. Maximum 40 characters.
Birthdate
The contact’s birthday. Click the field, and then choose a date from the calendar.
Contact Currency
The default currency for all currency amount fields in the contact. Amounts are displayed in the contact currency and converted to the user’s personal currency. Available when multiple currencies are used.
Contact Division
The division that the contact belongs to. This value is inherited from the related account.
Available when divisions are used to segment data.
Contact Owner
The contact’s assigned owner. Not available in Personal Edition.
Contact Record Type
The name of the field that determines what picklist values are available for the record. Available in Professional, Enterprise, Unlimited, Performance, and Developer Editions.
Created By
The user who created the contact. Includes creation date and time. Read only.
Custom Links
A list of custom links for contacts, as set up by your administrator.
Department
The associated business or organizational unit. Maximum 80 characters.
Description
The contact’s description. Maximum 32 KB of data. The first 255 characters appear in reports.
Email
The contact’s email address. A valid email address is required. Maximum 80 characters.
Click the email address in this field to send an email using your personal email application. This action isn’t logged as an activity on the contact record.
If the 
Gmail Buttons and Links
 feature is enabled, click the Gmail link next to the field to send an email from your Gmail account.
Fax
The contact’s fax number. Maximum 40 characters.
First Name
The contact’s first name, as displayed on the contact edit page. Maximum 40 characters.
First Name (Local)
The contact’s first name, translated into the local language.
Home Phone
The contact’s home phone number. Maximum 40 characters.
Last Modified By
The user who last changed the contact fields, including modification date and time. This field doesn’t track changes that were made to any of the related list items on the contact. Read only.
Last Name
The contact’s last name as displayed on the contact edit page. Maximum 80 characters.
Last Name (Local)
The contact’s last name, translated into the local language.
Last Stay-in-Touch Request Date
The date when the most recent Stay-in-Touch request was sent.
Last Stay-in-Touch Save Date
The date when the most recent Stay-in-Touch request was returned and merged.
Lead Source
The record source: for example, Advertisement, Partner, or Web. The entry is selected from a picklist of available values, which the administrator sets. Maximum 40 characters for each picklist value.
Mailing City
The city in the mailing address. Maximum 40 characters.
Mailing Country
The country in the mailing address. Maximum 80 characters.
Mailing State/Province
The state or province in the mailing address. Maximum 80 characters.
Mailing Street
The street in the mailing address. Maximum 255 characters.
Mailing Zip/Postal Code
The zip or postal code in the mailing address. Maximum 20 characters.
Middle Name
The contact’s middle name, as displayed on the contact edit page. Maximum 40 characters.
To enable this field, contact Salesforce Customer Support. Next, from Setup, enter 
User Interface
 in the 
Quick Find
 box, then select 
User Interface
. Then select 
Enable Middle Names for Person Names
.
Middle Name (Local)
The contact’s middle name, translated into the local language.
To enable this field, contact Salesforce Customer Support. Next, from Setup, enter 
User Interface
 in the 
Quick Find
 box, then select 
User Interface
. Then select 
Enable Middle Names for Person Names
.
Mobile
The contact’s mobile phone number. Maximum 40 characters.
Name
The contact’s combined first name, middle name, last name, and suffix, as displayed on the contact detail page.
Other City
The city in another address for the contact. Maximum 40 characters.
Other Country
The country in another address for the contact. The entry is selected from a picklist of standard values or entered as text. If the field is a text field, maximum 80 characters.
Other State/Province
The state or province in another address for the contact. The entry is selected from a picklist of standard values or entered as text. If the field is a text field, maximum 80 characters.
Other Street
The street address in another address for the contact. Maximum 255 characters.
Other Zip/Postal Code
The zip or postal code in another address for the contact. Maximum 20 characters.
Other Phone
Another phone number for the contact. Maximum 40 characters.
Phone
The contact’s primary phone number. Maximum 40 characters.
Reports To
The name of the contact’s manager. Enter a contact name, or select a contact from the list.
Salutation
The title for addressing the contact, for example, Mr., Ms., or Dr. The entry is selected from a picklist of available values, which the administrator sets. Maximum 40 characters.
Suffix
The suffix in the contact’s name, as displayed on the contact edit page. Maximum 40 characters.
To enable this field, contact Salesforce Customer Support. Next, from Setup, enter 
User Interface
 in the 
Quick Find
 box, then select 
User Interface
. Then select 
Enable Middle Names for Person Names
.
Title
The contact’s position within the organization. Maximum 80 characters.
Username
For Self-Service contacts only. The 
Username
 defaults to the 
Email
. Contacts enter their usernames when logging in to the Self-Service portal.
Note
Starting with Spring ’12, the Self-Service portal isn’t available for new orgs. Existing orgs continue to have access to the Self-Service portal.
See Also:
Considerations for Creating Contacts
Considerations for Changing the Account for Contacts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=contacts_fields.htm&language=en_US
Release
202.14
