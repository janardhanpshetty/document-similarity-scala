### Topic: Examples of Macros | Salesforce
Examples of Macros | Salesforce
Examples of Macros
These examples show how you can create different types of macros based on your business needs.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Add and Replace Field Values in a Case Using Macros
Suppose that your support agents often add the same field values to a record, or that they often replace a field value. You can create a macro that automatically adds content to a field or that replaces the values in a field. Using a macro saves agents time because it automates repetitive and routine actions, freeing them to focus on helping customers.
Insert Quick Text in a Social Post
Suppose that support agents often respond to customer questions on social networks, such as Twitter or Facebook. You can use a macro to automatically insert a reply into the post using Quick Text or text. This type of macro enables agents to quickly respond to customers without interrupting their workflow.
Automatically Attach a Salesforce Knowledge Article to an Email in Case Feed Using Macros
Perhaps your support agents often send customers the same article in Salesforce Knowledge. This example explains how to create a macro that automatically selects a specific article and inserts it into an email in Case Feed. This macro lets agents answer a common customer question by clicking one button, instead of spending time searching for the article and copying it into the email.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=macros_examples.htm&language=en_US
Release
202.14
