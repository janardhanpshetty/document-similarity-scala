### Topic: Adding Code Samples to Posts | Salesforce
Adding Code Samples to Posts | Salesforce
Adding Code Samples to Posts
You can paste code samples into your posts in Chatter Answers and Ideas.
When you enable Chatter Answers and Ideas, users can add code samples to their posts. Code can be copied from any text editor and pasted into the body of a post with the formatting preserved.
Copy the code sample from a text editor to your clipboard.
In the editor, enter the text for your posts and click the 
 button to add the code sample.
Paste the code sample into the 
Add a code sample
 text box and click 
OK
.
The code sample appears in the body of the post with its formatting intact.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_using_html_editor_code_snippets.htm&language=en_US
Release
202.14
