### Topic: Field-Level Security on Articles | Salesforce
Field-Level Security on Articles | Salesforce
Field-Level Security on Articles
Field-level security lets administrators restrict readers’ access to specific fields on detail and edit pages. For example, you can make a “Comment” field in an article visible for Internal App profiles but not for public Community profiles.
Available in: Salesforce Classic
Salesforce Knowledge is available in 
Performance
 and 
Developer
 Editions and in 
Unlimited
 Edition with the Service Cloud.
Salesforce Knowledge is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions.
If using both 
article-type layout
 and field-level security to define field visibility, the most restrictive field access setting always applies. For example, if a field is hidden in the article-type layout, but visible in the field-level security settings, the layout overrides security settings and the field aren’t visible. Some user permissions override both page layouts and field-level security settings. For example, users with the “Edit Read Only Fields” permission can always edit read-only fields regardless of any other settings.
Important
Field-level security doesn’t prevent searching on the values in a field. When search terms match on field values protected by field-level security, the associated records are returned in the search results without the protected fields and their values.
You can define security via a permission set, profile, or field.
Define field-level security via permission sets or profiles
For permission sets or profiles, from Setup, either:
Enter 
Permission Sets
 in the 
Quick Find
 box, then select 
Permission Sets
, or
Enter 
Profiles
 in the 
Quick Find
 box, then select 
Profiles
Select a permission set or profile.
Depending on which interface you're using, do one of the following:
Permission sets or enhanced profile user interface—In the 
Find Settings...
 box, enter the name of the object you want and select it from the list. Click 
Edit
, then scroll to the Field Permissions section.
Original profile user interface—In the Field-Level Security section, click 
View
 next to the object you want to modify, and then click 
Edit
.
Specify the field's access level.
Note
These field access settings override any less-restrictive field access settings on the article-type layouts.
Click 
Save
.
Define field-level security via fields
For fields, from Setup, enter 
Knowledge Article Types
 in the 
Quick Find
 box, then select 
Knowledge Article Types
.
Select the article type that contains the field to modify.
Select the field and click 
Set Field-Level Security
.
Specify the field's access level.
Note
These field access settings override any less-restrictive field access settings on the article-type layouts.
Click 
Save
.
After setting field-level security, you can 
modify the article-type layouts
 to organize the fields on detail and edit pages.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=knowledge_custom_field_fls.htm&language=en_US
Release
202.14
