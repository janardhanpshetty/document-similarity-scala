### Topic: Sample Apex code to give a Thanks
How to Give Thanks via Apex?
Resolution
*For use with non-Reward badges*
1. Create a Thanks record (
http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_workthanks.htm
)
 
WorkThanks thanks = new WorkThanks(GiverId = '005i0000000TwE7', Message = 'Thanks for delivering on your Q4 Sales!');
insert thanks;
2. Create a Badge record that links to the related Thanks record (
http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_workbadge.htm
)
 
WorkBadge badge = new WorkBadge(DefinitionId = '0W1i0000000LDmqCAG', RecipientId = '005i0000001CHco', SourceId = thanks.Id);
insert badge;
3. Create a FeedItem record that links to the related Thanks record with Type set to RypplePost (
http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_feeditem.htm
)
 
FeedItem post = new FeedItem(
	ParentId = '005i0000001CHco',
	CreatedById = '005i0000000TwE7',
	Body = 'Thanks for delivering on your Q4 Sales!',
	RelatedRecordId = thanks.Id,
	Type = 'RypplePost'
);
insert post;
