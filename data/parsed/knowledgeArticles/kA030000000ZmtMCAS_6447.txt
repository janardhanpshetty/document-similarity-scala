### Topic: If you need assistance in editing licenses in LMA (License management App), review the information given in this article
Editing Licenses
User Permissions To edit licenses and packages:  “Read” AND “Edit”
 
Resolution
You can modify licenses for each installer. To modify a license:
Click the Licenses tab.
Select the appropriate license.
Click Modify License.
Note
: If Modify License is not visible, check your page layout or contact your administrator.
Make the necessary changes to editable fields.
Click Save.
Editable License Fields
You can modify the following License fields
  
Field
Expiration
Enter the expiration date to identify the last day the installer can access the package under the license agreement, or check 
Does not expire
 if the license does not expire.
Seats
Enter a positive number to set the number of licenses or check 
Site License
 to make the package available to all users in the installer's organization. The value defaults to Site License.
Status
Use the drop-down list to set the license status. Available values are:
• 
Trial
: Set to Trial to allow the installer to try out the package for up to 90 days. Once a trial license is converted to an active license, it cannot return to a trial license.
• 
Active
: Set to Active to allow the installer to use your package according to your license agreement.
• 
Suspended
: Set to Suspended to prohibit the installer from accessing the package. You may want to set a package to Suspended if a user fails to pay for the license.
Note: When the installer uninstalls the package, the status is set to Uninstalled. The license manager cannot set or change this status. Once the package is uninstalled the license becomes read only and is no longer editable. The allowed status changes are:
• Trial to Active
• Active to Suspended
• Suspended to Active
 
