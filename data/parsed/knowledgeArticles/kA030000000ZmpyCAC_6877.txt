### Topic: This presents a way to export SOQL query results from Workbench
Workbench is a powerful, web-based suite of tools designed for administrators and developers to interact with Salesforce organizations via the Force.com APIs.
A need might arise to export the SOQL query results of workbench to an Excel sheet so that you can analyze the data. There is no standard way or a button which is available for users to export the query result. 
Resolution
Once the SOQL query is run, the results are displayed in a table on the same page. 
A 
table html tag will be created with the 
output. 
Here is a way to export the data.
Note: This works perfectly only when all the results are displayed in one page. 
1. Open your Browser Console/Inspect Element on the browser. 
Once you are in the inspect element, the screen should look similar to this: 
 
2. After this, using the inspect element (magnifying glass in this case) click on any cell of the table and that should take you to the HTML table tag code. 
It will look similar to this: 
3. Now scroll up to the beginning of the table tag and collapse the table tag to get the table tag code in one line. Copy the line. 
It should look similar to this: 
4. Now paste the copied table tag line and paste it in excel sheet. The data will be displayed properly in columns and rows of excel with appropriate heading row. 
