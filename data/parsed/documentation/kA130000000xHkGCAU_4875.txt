### Topic: Configure Lookup Dialog Search in Lightning Experience | Salesforce
Configure Lookup Dialog Search in Lightning Experience | Salesforce
Configure Lookup Dialog Search in Lightning Experience
Customize which columns appear to users in the lookup dialog search results using the Search Results search layout customization setting. Users aren’t able to filter using these columns. They are intended to provide contextual help for determining which record to associate.
Available in: Lightning Experience
Available in: 
All
 Editions 
except Database.com
User Permissions Needed
To specify lookup filter fields:
“Customize Application”
Use 
Search Results
 under the 
Search Layouts
 customization setting to change which fields appear in the search results for both global search and lookup dialog search. You don’t need to separately update 
Lookup Dialogs
.
The order of fields in the search layout also affects the secondary field displayed in instant results. The second usable field as chosen in this step appears as the secondary field in instant results. Examples of unusable fields are HTML-formatted fields, inline image fields, or long-text fields.
See Also:
Find Object Management Settings in Lightning Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_lookup_config_lex.htm&language=en_US
Release
202.14
