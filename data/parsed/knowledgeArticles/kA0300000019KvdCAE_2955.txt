### Topic: Enable Session Termination for Security Central showing in the audit trail
Enable Session Termination for Security Central perm is showing in the audit trail as changed form ON to Off.
Resolution
You may see this setting turn from on to off in the setup audit trail if you make one of the following changes.
 
Security Level for Two Factor Authentication was set to High Assurance Session Settings    
Security Level for My SAML IDP changed from Standard to High Assurance 
This is not exclusive to these settings.
The feature "Enable Session Termination for Security Central" is redundant and has no impact on the org security.
As the perm "Enable Session Termination for Security Central" is now redundant we are looking at removing this in
future release. 
