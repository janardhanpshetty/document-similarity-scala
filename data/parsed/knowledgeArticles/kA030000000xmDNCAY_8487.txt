### Topic: When the System Administrator is logged in as a user that has granted login access, they cannot see the org's open and closed cases on H&T.
In Enterprise, Unlimited and Developer Editions, 
salesforce.com
 users can grant login access to their System Administrators to assist with resolving issues.
If a System Administrator is properly setup as a Help and Training Admin to see 
My organization's open and closed cases
, it would be expected that they would have visibility to all their org's cases via the Help and Training portal.
However, all the org's open cases may not be visible if the Admin is currently using granted access and 
logging in as another user
 who is not setup as a Help and Training Admin.  This is expected because case visibility in the Help and Training portal is restricted and based on the user who the Admin is currently logged in as.
 
Resolution
The administrator will need to log out as the user they're currently logged in as and then refresh or re-navigate to Help & Training.
