### Topic: Removing Licenses for Installed Packages | Salesforce
Removing Licenses for Installed Packages | Salesforce
Removing Licenses for Installed Packages
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To manage licenses for an AppExchange package:
“Manage Package Licenses”
To remove licenses for an AppExchange package from multiple users:
From Setup, enter 
Installed Packages
 in the 
Quick Find
 box, then select 
Installed Packages
.
Click 
Manage Licenses
 next to the package name.
Click 
Remove Multiple Users
.
To show a filtered list of items, select a predefined list from the 
View
 drop-down list, or click 
Create New View
 to define your own custom views.
Click a letter to filter the users with a last name that corresponds with that letter or click 
All
 to display all users who match the criteria of the current view.
Select users.
To select individual users, use the checkboxes. Selected users appear in the Selected for Removal list. When the list includes all users for which you want to remove licenses, click 
Remove
.
To select all users in the current view, click 
Remove All Users
, then click 
OK
.
You can also remove licenses for an AppExchange package from a single user using the following options:
From Setup, enter 
Users
 in the 
Quick Find
 box, then select 
Users
 and click 
Remove
 next to the package in the managed packages list.
From Setup, enter 
Installed Packages
 in the 
Quick Find
 box, then select 
Installed Packages
. Then, click 
Manage Licenses
 next to the package name, and click 
Remove
 next to the user.
See Also:
Managing Licenses for Installed Packages
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=distribution_removing_user_licenses.htm&language=en_US
Release
202.14
