### Topic: Skills Limitations | Salesforce
Skills Limitations | Salesforce
Skills Limitations
Skills and endorsements have special behaviors and limitations.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Note the following restrictions:
The Skills feature is not supported in Communities.
All skill records are visible to users with the “Read” permission on Skills.
Skill User and Endorsement records can only be deleted by the owner and users with the “Modify All Data” permission.
To restore the users and endorsements associated with a deleted skill, retrieve the skill from the recycle bin. Creating a skill with the same name will not restore the prior associations.
Users can only endorse others, not themselves. Users can’t be endorsed more than once by the same person on the same skill.
Administrators can endorse users on behalf of other users through the API.
Users can’t see the skills widget on a Chatter profile page unless Chatter is enabled and users have the “Read” permission on Skills and Skill Users.
See Also:
Skills Overview
Skills Customization
Enable or Disable Work.com Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workcom_skills_limitations.htm&language=en_US
Release
202.14
