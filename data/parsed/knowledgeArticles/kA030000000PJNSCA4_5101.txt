### Topic: This article will help to retrieve the user information like access level etc from "UserRecordAccess" table provided by Salesforce.
Customer was trying to get 
SOQL query to fetch the details of how much access a user and a public group would have on Account.
Resolution
UserRecordAccess is related to Object Records and Users. You can Use that table as a wrapper of Records-Sharing-Groups. That is the only way to filter data for an specific user.
 
The code will  be :
 
List <Account> a = new List <Account>(); 
List <User> u = new List <User>(); 
List <ID> sRecordIDs = new List <ID>(); 
a = [select id from Account]; system.debug('>>>'+a.size()); 
u = [select id from User]; 
system.debug('>>>'+u.size()); 
for(Account c : a) 
{ UserRecordAccess i =null; i = [SELECT RecordID FROM UserRecordAccess WHERE UserId = '00590000001OZ5bAAG' AND RecordID=: c.id AND HasReadAccess = True]; 
sRecordIDs.add(i.id); 
} 
system.debug('>>>'+ sRecordIDs.size()); 
system.debug('>>>'+ sRecordIDs); 
 
 
Below are the few links to get more data from the "UserRecordAccess" table :
 
http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_userrecordaccess.htm 
 
http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_concepts_security.htm 
 
http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_methods_system_userinfo.htm 
