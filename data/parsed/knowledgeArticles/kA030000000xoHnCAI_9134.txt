### Topic: Are you looking to roll out a new Community? Read on to determine whether a template or a custom build is right for your organization.
Is your organization in the process of planning a Community implementation? Community Templates provide a fast and easy way to stand up a Community declaratively. Custom Communities are ideal for organizations who are interested in deploying Objects that aren't included in the templates, and can incorporate Visualforce elements as well.  
As of the Spring '15 release, t
he available Objects are dependent on the Template you’ve selected:
Kokua
 and 
Koa
: Knowledge, and Cases & Web2Case functionality
Napili
: Q&A, Knowledge, & Cases & Web2Case functionality
If additional Objects are required, you will need to use the tab-container (traditional Community) or create your own Community in 
site.com
 studio. 
There are two ways to create a custom Community outside of the Community Templates:
1. Salesforce Tabs + Visualforce
2. Full Custom using the Force.com/Site.com Advanced Configuration Options. 
If your organization's use cases and required Objects extend beyond what's offered out of the box through the Community Templates, we encourage you to explore one of the custom Community routes. The Salesforce Tabs + Visualforce Community allows you to build a bulk of the Community declaratively whilst providing the flexibility of extending the Community through Visualforce as well. 
The following infographic highlights the key decision points for building a Custom or Templated Community:
For more resources check out the 
Getting Started with Communities Hub
Resolution
