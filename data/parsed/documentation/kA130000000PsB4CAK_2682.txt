### Topic: Sandbox Setup Considerations | Salesforce
Sandbox Setup Considerations | Salesforce
Sandbox Setup Considerations
Sandboxes behave almost the same as your Salesforce production org does, but important differences affect how you configure and test a sandbox org.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Database.com
 Editions
User Permissions Needed
To view a sandbox:
“View Setup and Configuration”
To create, refresh, activate, and delete a sandbox:
“Modify All Data”
Consider the following before you create a sandbox.
Servers and IDs
Sandbox and production orgs always have unique org IDs. The sandbox copy engine creates an org as part of each creation and refresh request. So, the org ID of your sandbox changes each time your sandbox is refreshed. Salesforce inserts the new value in any place the org ID is used, such as text values and metadata.
To find the org ID of the org that you’re currently logged in to, from Setup, enter 
Company Information
 in the 
Quick Find
 box, then select 
Company Information
. Any script or process, such as test scripts or Web-to-Lead, that depends on a “hard coded” org ID must use the current ID for the sandbox. When you deploy your changes to a production org, update those scripts or processes with the production org ID.
Salesforce creates sandbox orgs on several instances. When a sandbox is created or refreshed, Salesforce selects an instance for your sandbox, so your sandboxes sometimes appear on different instances and have different URLs.
When data is copied to a sandbox, object IDs (unique identifiers for all objects—the same as the 
ID Field Type
 in the developer API) for records are copied. After being copied, however, object IDs don’t synchronize between your production org and your sandbox. The sandbox and its corresponding production org act as independent orgs. Object data (and corresponding object IDs) that are created in the production org after the creation or refresh of a sandbox aren’t synchronized into the sandbox. The sandbox has the same behavior—new objects that are created in the sandbox aren’t synchronized back to the production org.
Users and Contacts
User information is included in a sandbox copy or refresh for all sandbox types. Because all Salesforce usernames must be unique and reference a single org, all copied usernames are modified to ensure uniqueness during the copy process.
For each username, the copy process applies modifications as necessary to generate a unique, new username.
First, the sandbox name is appended to the username. For example, the username 
user@acme.com
 for a sandbox named 
test
 becomes 
user@acme.com.test
.
If the resulting username is not unique, a second modification is performed in which some characters and digits are prepended to the modified username. This second modification results in a username such as 
00x7Vquser@acme.com.test
.
When you log in with the modified username, you log in to the corresponding sandbox.
The copy process doesn’t copy Contact data to Developer or Developer Pro sandboxes. Therefore, Customer Portal users aren’t copied. However, the copy process does copy the Customer Portal licenses, so you can create Customer Portal users in these sandboxes as needed.
When you create or refresh a sandbox, user email addresses are modified in your sandbox so that production users don’t receive automatically generated email messages from the sandbox. User email addresses are appended with an example domain (
@example.com
), and the original 
@
 sign is replaced with 
=
. This modification ensures that the system ignores these email addresses. For example, a user email of 
awheeler@universalcontainers.com
 in production becomes 
awheeler=universalcontainers.com@example.com
 when migrated to sandbox. If you want sandbox users to receive automatically generated emails as part of testing, you can correct the email addresses while logged in to the sandbox.
Warning
Sandboxes change Salesforce user email addresses, but don’t change other email addresses in Salesforce, such as email addresses in contact records. To avoid sending unsolicited email from your sandboxes, manually invalidate or delete all email addresses in your sandboxes that don’t belong to users of the sandbox. When testing outbound email, change contact email addresses to the addresses of testers or an automated test script.
Email Deliverability
New and refreshed sandboxes have the default email deliverability setting 
System email only
. To configure email deliverability settings, in the sandbox org, from Setup, enter 
Deliverability
 in the 
Quick Find
 box, then select 
Deliverability
. If editable, set the 
Access level
 in the Access to Send Email section. You may not be able to edit the Access level if Salesforce has restricted your organization’s ability to change this setting.
No access
: Prevents all outbound email to and from users.
System email only
: 
Allows only automatically generated emails, such as new user and password reset emails.
All email
: Allows all types of outbound email. Default for new, non-sandbox organizations.
Tip
The 
System email only
 setting is especially useful for controlling email sent from sandboxes so that testing and development work doesn’t send test emails to your users.
Newly created sandboxes default to 
System email only
.
Sandboxes created before Spring ’13 default to 
All email
.
Creating, Refreshing, and Deleting Sandboxes
You can specify a post-copy script to run on a sandbox every time it is refreshed (and the first time it is created). Specify the script when you create the sandbox.
You can copy Site.com and Site.com community sites to sandboxes.
Sandbox copy is a long-running operation that occurs in the background. You are notified of the completion of a sandbox copy via email. Sandbox refreshes can complete in hours, days, or even more than a week.
Several conditions affect the duration of a sandbox copy or refresh, including the number of customizations, data size, numbers of objects and configuration choices, and server load. Also, sandbox refreshes are queued, so your copy might not start immediately after your request.
A sandbox is not a point-in-time snapshot of the exact state of your data. Furthermore, we recommend that you limit changes to your production org while a sandbox is being created or refreshed. Setup and data changes to your production org during the sandbox creation and refresh operations can result in inconsistencies in your sandbox. You might detect and correct some inconsistencies in your sandbox after it is created or refreshed.
Creating or refreshing a sandbox occurs over time. Running a large process or updating orgs of 30 GB or more during creation or refresh can cause inconsistencies in your sandbox.
Some types of sandboxes are not available if you’ve reached your org's limit. For example, if your org is limited to one Full sandbox, and you already have a Full sandbox, you can’t create another Full sandbox. However, you can refresh your existing Full sandbox.
When you’re finished with a sandbox, you can refresh it. This process replaces the current sandbox with a new copy of your production org.
If you have reduced your org’s number of sandbox licenses, a 
Delete
 link displays next to existing sandboxes. Delete a sandbox before creating or refreshing any more sandboxes.
If you have active Salesforce-to-Salesforce connections in your sandbox, deactivate the connections and then reactivate them after the sandbox is refreshed. The connections and mappings aren’t copied to the refreshed sandbox.
Matching Production Licenses
You can match provisioned licenses in production to your sandbox org without having to refresh your sandbox. Matching updates sandbox license counts to match the counts in production, adds licenses that are in production but not in sandbox, and deletes licenses that aren’t in production.
To match licenses, log in to your sandbox. From Setup, enter 
Company Information
 in the 
Quick Find
 box, select 
Company Information
, then click 
Match Production Licenses
. You get an alert when the matching process starts. After all licenses have been matched in sandbox, you get a confirmation email. The confirmation email is sent to the user who initiates the license copy.
Matching production licenses requires that your sandbox and production are on the same Salesforce release. If your sandbox has been upgraded to the next release—for example, during sandbox preview—but production hasn't been upgraded, you can't match production licenses.
The Match Production Licenses tool isn’t available in trial orgs. To use the tool, you must have the “Modify All Data” permission.
Configuring Full Sandboxes
When you create or refresh a Full sandbox, you can configure it to determine what data is copied. Minimizing the amount of data you include speeds up your sandbox copy.
The Object History, Case History, and Opportunity History options allow you to select the number of days of history from your production org to copy to your sandbox. 
You can copy from 0 to 180 days of history, in 30-day increments. The default value is 0 days.
By default, Chatter data isn’t copied to your sandbox. Chatter data includes feeds and messages. Select 
Copy Chatter Data
 if you want to include it.
The setup audit trail history of your production org isn’t copied to your sandbox. The audit trail for your sandbox org starts when you begin to use it.
Archived activities (tasks and events that aren’t available in the production org because they’re over a year old) and password history (users’ previous passwords) aren’t copied.
Note
Don’t increase the default selections unless special circumstances require it. Large amounts of data can significantly lengthen the time it takes to copy your sandbox.
Accessing Sandboxes
Access changes for sandbox users:
A sandbox refresh deletes and recreates the sandbox as a new copy of the production org. In effect, this process reverses any manual access changes you’ve performed. If you created sandbox-only users, they no longer exist, and a user’s profile and permissions revert to their values in the production org. After a refresh, make any access changes in the new copy.
You can create users in your production org that are inactive, and then activate them in a sandbox. This method is a good way to create a user that has the appropriate permissions to develop in a sandbox.
Many development and testing tasks require the “Modify All Data” permission. If your developers don’t have that permission in the production org, increase their permissions in the sandbox. Exercise caution when granting this permission in sandbox orgs that contain sensitive information copied from production (for example, social security numbers).
Users added in a production org after creating or refreshing a sandbox don’t have access to the production org instance’s related sandboxes. To create users in a sandbox, log in as the administrator on the sandbox org and create them in the sandbox instance.
You can create users for sandbox development, but these new users count against the number of licensed users in your org. To reduce your license count, you can disable production users who don’t need access to the sandbox before you create or refresh a sandbox.
Always log in to your sandbox org using the 
https://test.salesforce.com
 login URL.
Remember to log in using the modified username as described in 
Users and Contacts
.
If using the API, after you log in, use the redirect URL that is returned in the 
loginResult
 object for subsequent access. This URL reflects the instance on which the sandbox is located and the appropriate server pool for API access.
Sandbox copies are made with federated authentication with SAML disabled. Any configuration information is preserved, except the value for 
Salesforce Login URL
. The 
Salesforce Login URL
 is updated to match your sandbox URL, for example 
https://
yourInstance
.salesforce.com/
, after you re-enable SAML. To enable SAML in the sandbox, from Setup, enter 
Single Sign-On Settings
 in the 
Quick Find
 box, then select 
Single Sign-On Settings
; then click 
Edit
, and select 
SAML Enabled
.
 Change the value of the 
Salesforce Login URL
 in the certificate for your client application as well.
Sandbox Limits Notes
Sandboxes don’t send email notifications when storage limits are reached. However, if you reach the storage limit of your 
sandbox
, you can’t save new data in your sandbox. To check your storage limits, from Setup, enter 
Storage Usage
 in the 
Quick Find
 box, then select 
Storage Usage
 in your sandbox.
Customization and Data Changes
Customizations and data changes in your production org aren’t reflected in your existing sandboxes. Create or refresh a sandbox to incorporate the most recent customizations made to your org.
You can only add, edit, or delete Apex using the Salesforce user interface in a Developer Edition or sandbox org. In a Salesforce production org, you can only make changes to Apex by using the 
compileAndTestAPI()
 call.
If your sandbox is the same version as Force.com AppExchange, you can:
Install and deploy apps from Force.com AppExchange in your sandbox.
Publish apps from your sandbox to Force.com AppExchange.
Publishing managed packages from a Force.com Sandbox is not advised, as refreshing or deleting the sandbox prevents any revisions to that managed package.
The version of your sandboxes can differ from Force.com AppExchange around the time of a Salesforce release. Check the logo in the upper left corner of your sandbox home page for version information.
If your org uses quote templates, and you create a Developer Pro sandbox, templates that contain 
Text/Image
 fields can’t be opened for editing within the sandbox.
If your production org uses an image in quote templates and you copy the org to your sandbox, the image path is not correct and the image appears as a broken link. To display the image, reinsert it from the correct location on your sandbox.
Service Exclusions
The following features are disabled and can’t be enabled in sandboxes.
Contract expiration warnings
Case escalation
Contract expiration warnings and case escalation are disabled because they automatically send email to contacts, customers, and production org users.
Subscription summary
Data exports (by clicking 
Export Now
 or 
Schedule Export
 on the Weekly Export Service page in Setup)
The ability to create Salesforce sandboxes
The ability to copy email service addresses that you create in your sandbox to your production org
The ability to publish Site.com sites
Other Service Differences
Only custom links created as relative URLs, such as 
/00Oz0000000EVpU&pv0={!Account_ID}
, work when copied to your sandboxes. Custom links created as absolute URLs, such as 
https://yourInstance.salesforce.com/00Oz0000000EVpU&pv0={!Account_ID}
, don’t work in your org's sandboxes. We recommend that you use only relative URLs in your production org. Otherwise, correct the URLs in each sandbox.
Salesforce has a background process that permanently deletes records in the Recycle Bin that are older than 15 days. This process runs at different times on different servers, so its timestamp in your sandbox differs from its timestamp in your production org. Applications and integrations that depend on this timestamp can fail if they are first connected to one environment, such as your production org, and then connected to another environment, such as your sandbox. Keep this behavior in mind when developing applications and integrations that depend on this timestamp.
The time of the latest execution of the background delete process is available through the 
getDeleted()
 API call.
For Salesforce authentication providers set up in the Summer ’14 release and earlier, the user identity provided by a sandbox does not include the org ID. The destination org can’t differentiate between users with the same user ID from two sources (such as two sandboxes). To differentiate users, edit the existing Salesforce Auth. Provider settings in the destination org, and select the checkbox to include the org ID for third-party account links. After you enable this feature, your users must reapprove the linkage to their third-party links. Salesforce authentication providers created in the Winter ’15 release and later have this setting enabled by default.
After an org’s sandbox refresh is completed, a user has login access to a sandbox for 10 years after the sandbox refresh date if the user:
Is a system administrator.
Is copied into the sandbox from the production org, not created directly in the sandbox.
To log in as any user, access your sandbox via 
test.salesforce.com
. The option to log in as any user isn’t available when users access a sandbox from production by using the Login link.
See Also:
Create, Clone, or Refresh a Sandbox
Sandboxes
Sandbox Licenses and Storage by Type
Sandbox License Expiration
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_sandbox_implementation_tips.htm&language=en_US
Release
202.14
