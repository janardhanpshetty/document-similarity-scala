### Topic: Variable Declarations are no longer counted for code coverage, some customers may see a decrease in code coverage percentage (%) as a result
Customers may notice an increase or decrease in their overall code coverage percentage in the Spring '15 release onwards due to a change in lines counted against code coverage. Declaration of variables is no longer counted as a line to be covered in Apex Tests. 
In the example below you will see the previous behavior Winter '15 and earlier compared to the current behavior. 
Winter '15 Code Coverage would have indicated the below example as 3/3 lines covered when apex tests were run with the corresponding test class covering the method. 
In Spring '15 release, included in a patch release, the same code will show as 2/2 lines being covered when the same apex test classes are run. Note the variable doc of type String is no longer highlighted. In this example code coverage % would not have changed for the class however some customers may see changes if their declarations account for a code coverage increase and some other methods/classes are not covered or partially covered. 
NOTE:
As mentioned above get and set accessor code blocks still require code coverage as expected.
Declaration of a variable that also assigns an initial value to itself are counted and covered in your apex classes.
Variable declarations inside of methods are still counted.
Resolution
If you notice a drop in your code coverage we recommend reviewing currently uncovered or partially covered apex code and increasing the coverage through the creation of additional unit tests and as always as a best practice we recommend customer to strive for 100% code coverage rather than focusing on the 75% requirement.
For additional assistance on code coverage please see our 
Introduction to Apex Code Test Methods.
