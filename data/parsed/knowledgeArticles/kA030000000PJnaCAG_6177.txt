### Topic: This article explains what will happen to existing opportunities when the probability value is changed under Setup.
It is possible to change the probability value assigned to a stage value for opportunities. For example, if by default Closed/Won has a probability of 100%, you can change that to 60%, so that opportunities in Closed/Won stage, will have a probability of 60%.
When you make this type of change, existing opportunities are not updated.
Resolution
The probability field is directly linked to the stage value selected, however, the probability can be manually edited by users who have edit access to this field. Since Salesforce allows this, it means that the probability value doesn't act as a fixed value, instead, it works as a default value.
For example, if you choose Closed/Won on the stage, Salesforce will give you a 
default
 value of 100%, but you can manually edit this value to 57% for example.
Because of this, when you change the probability linked to a stage value under Setup, existing opportunities 
will not
 show the new probability value, since the probability was already saved on the record before the change. If you then go to one of these opportunities and manually change the stage, you will now see its new 
default
 value now you can select it too.
