### Topic: Set Up Data.com Clean Rules | Salesforce
Set Up Data.com Clean Rules | Salesforce
Set Up Data.com Clean Rules
Help your sales and marketing teams maintain great relationships with customers and pursue the most promising leads and opportunities by automatically enriching your Salesforce accounts, contacts, and leads with the latest information. It’s all done using Data.com clean rules.
What Are Clean Rules?
Clean rules keep your data squeaky clean, complete, and up-to-date. Just set up and activate your organization’s Data.com clean rules. Then, your records are automatically checked against a data service to see if new data is available. Based on the clean rule settings, your records are updated with the new data. With clean rules, maintaining high-quality data is a piece of cake!
Automatically Get Geocodes for Addresses
Help your sales and marketing teams find nearby prospects, assign territories and campaigns, and more! All this is a cinch with geocodes. In just a few clicks, you can automatically get geocodes for addresses on accounts, contacts, and leads.
Automatically Enrich Leads with Critical Company Information
Make it easier for your sales and marketing reps to pursue the most promising leads and immediately assign them to the right territories and campaigns. If your organization has a Data.com Premium Clean license, then it’s easy to enrich your leads with valuable company information like annual revenue, industry, D-U-N-S number, and number of employees.
Guidelines for Determining How Well Clean Rules Are Working
Do you want to know how well Data.com Clean is working for your organization? Check out the Clean Vitals page, which shows the match rate for your clean rules. You’ll see the percentage of processed records that Data.com matched to a record in the data service. When a record’s matched, it can be updated with the latest data.
Standard Data.com Clean Rules
Learn key details about the standard clean rules, which help you automatically add or assess information on your Salesforce records. Find a description of how each clean rule works and the fields it affects.
Statuses for Data.com Clean Rules
If you’ve activated any Data.com clean rules, you’ll see a status associated with the rule on records. This status indicates how the record’s data compares with the data service.
Parent topic:
 
Set Up Data.com Clean
Previous topic:
 
Configure List Views for Data.com Clean
Next topic:
 
Report on Salesforce Records with a Specific Data.com Clean Status
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_admin_set_up_clean_rules.htm&language=en_US
Release
202.14
