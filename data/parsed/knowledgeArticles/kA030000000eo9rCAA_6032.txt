### Topic: Explanation of Email to Case best practices around email retention and Automated Case User settings.
In some cases, an inbound email message redirected to the Email to Case - Email Services Address may not process as expected due to a number or reasons including but not limited to:
 
Custom Apex Triggers
Row Locks
Non-RFC Compliant emails
Permissions
Case object settings and configurations
In these situations, the logic of our application server is to send an email error notification to the configured Automated Case User.
Setup | Customize | Cases | Support Settings | (Automated Case User)
The error notification will generally indicate the reason for the failure, and depending on the type of issue and where along the processing path it fails, in some scenarios the original message may be included in text format along with original internet header information.
 
Resolution
If an error occurs during processing, the Case or Inbound Email Message record will not be created.  The Case will need to be either created manually, or resubmitted through the saved email in the internal Routing Address inbox.
As a recommended best practice, the following should be implemented:
 
When redirecting emails from the Routing Address internal inbox to the Email Services Address, the original received email should be retained in the inbox, or saved in another folder or location.  This provides a backup of all emails received and redirected to Salesforce for compliance purposes. 
For the Automated Case User email address, do not utilize an Email to Case Routing Address.  This could potentially cause an email loop situation.  Rather, ensure that the email address is of a user that can monitor for errors.  Another option is to assign an email address that is an internal alias that sends to a group of internal users.
 
