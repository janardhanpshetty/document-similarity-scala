### Topic: Show More
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
April 21, 2015 at 3:32 PM
  
Attach File
 
Click to comment
 
 
Verlin Kerns
 to salesforce.com Only
@Brandon Barbarito
 
@Bernard Spencer
I have located another article that is exactly the same as this one:
Article # 174058
Show More
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Add Topics
March 5, 2013 at 7:04 AM
  
Attach File
 
Click to comment
 
 
Show All (1)
Followers
« 
Go Back
Information
 
Description
Filter Report Data | Salesforce
Filter Report Data
What if your report gives you more data than you need? Use filters to pare down your report until it only shows the data that you want.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
User Permissions Needed
To add or edit a filter
“Create and Customize Reports”
AND
“Report Builder”
To lock or unlock filters so that users can’t edit them while viewing a report in Lightning Experience
“Create and Customize Reports”
AND
“Report Builder”
To edit a filter while viewing a report in Lightning Experience
“Run Reports”
In Salesforce Classic, filter your report from the report builder.
In Lightning Experience, there are two ways to filter reports: either from the Report Builder or while viewing a report. To add or edit report filters, use the Report Builder. To edit existing, unlocked report filters while you’re reading a report, run the report and then edit filters directly from the filters pane (
). You can edit existing filters from the filters pane, but you can’t add new ones.
Note
In Lightning Experience, these filters are available in the Report Builder, but are not shown in the filter panel when viewing a report. Even though the filters are not shown, they still filter the report.
Row limit filters
Cross filters
Historical field filters
Standard filters (except for scope and date filters)
From the Report builder, open the 
Add
 dropdown menu and select a filter type:
Field Filter
 to filter on fields. For example, use a field filter to filter by 
Account Name equals Acme
.
Filter Logic
 to customize how existing filters apply to your report. Each filter is assigned a number. If you’d like your report to return records that meet the criteria of Filter 1 and either Filter 2 or Filter 3, use this filter logic: 
Filter 1 AND (Filter 2 OR Filter 3)
. Filter logic requires at least one field filter.
Cross Filter
 to filter on one object’s relationship to another object. Cross filter on 
Accounts with Opportunities
 so that your report only returns Accounts that have Opportunities. Add a subfilter to a cross filter to further filter by the second object. For example, the Opportunity subfilter 
Amount greater than 50000
 causes your report to return Accounts that have Opportunities worth more than $50,000.00.
Row Limit
 to limit the number of report results in tabular reports. To see which five Accounts have the largest annual revenue, set a row limit of 
Top 5 Accounts by Annual Revenue
.
Standard filters, such as date filters, are applied by default to most objects. Look for them underneath the Add dropdown menu and customize them as necessary. Different objects have different standard filters.
Enter filter criteria.
For help entering filter criteria, see Filter Operators Reference and Add Filter Logic.
Optionally, to prevent people from editing a field filter while reading your report in Lightning Experience, check 
locked
.
Click 
Save
.
To read your filtered report, click 
Run Report
. In new reports, the Run Report button doesn’t appear until you save your report.
Note
In the Lightning Experience Report Builder, save your report before running it or risk losing some changes. Specifically, be sure to save after adding:
Row limit filters
Cross filters
Formula fields
Bucket fields
Example
Say that you want your team to call new leads at companies with more than 100 employees located in California, Arizona, or Nevada. You have a leads report with fields like 
Lead Status
, 
Number of Employees
, and 
State
. Your report gives a complete overview of your entire company’s leads. But you only want to see 
new
 leads that 
have more than 100 employees
 and are 
located in California
. Apply these filters to your report:
Lead Status equals New
Number of Employees greater than 100
State includes California, Arizona, Nevada
Now your leads report returns only the leads you need.
Filters Type Reference
Several different types of filters help you scope your report data: standard filters, field filters, cross filters, and row limit filters. Each filter type filters your report in different ways. This list of filter types helps you choose the right filter types for your report.
Filter Operators Reference
The operator in a filter is like the verb in a sentence. Operators specify how filter criteria relate to one another. Refer to this list of filter operators while setting filters on list views, reports, dashboards, and some custom fields.
Add Filter Logic
Filter logic governs how and when filters apply to your report.
Notes about Filtering on Types of Fields and Values
Keep these tips in mind when filtering on text fields, date fields, numeric values, picklist values, and blank or null values.
Tips for Filtering on Multiple Currencies
Tips for filtering on currency fields when your organization uses multiple currencies.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_builder_filtering.htm&language=en_US
Release
202.14
