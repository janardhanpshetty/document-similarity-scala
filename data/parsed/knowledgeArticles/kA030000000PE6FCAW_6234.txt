### Topic: A recently created or updated Case or Lead Assignment Rule isn't setting the Owner on new/edited Case or Lead records that appear to perfectly match the rule's criteria or formula conditions.
A recently created or updated Case or Lead Assignment Rule isn't setting the Owner on new/edited Case or Lead records that appear to perfectly match the rule's criteria or formula conditions. Why isn't ownership being assigned?
Resolution
1) Be sure your Case or Lead Assignment Rule is 
active
:
Setup | Customize | Case (or Lead) | Assignment Rules
See the "Active" checkbox and edit the Assignment to make it active (just one can be active at a time)
2) Check the 
criteria 
or 
formula 
you have for each rule component:
Setup | Customize | Case (or Lead) | Assignment Rules
[Edit] the different lines as needed, check logic, fields, values, etc.
3) In addition to having correct criteria or formula in an active Case or Lead Assignment Rule, you also need to apply those rules for actual use. This is done in your Case and Lead 
page layouts
.
Setup | Customize | Case (or Lead) | Page Layouts | Edit the layout
Click on the Layout Properties button and select the checkboxes for the Case Assignment:
NOTE: 
To force case assignment rules, select Default under Case Assignment Checkbox. This means end users won’t see any option to turn off case assignment when editing a case. You can also decide to include the Assign using active assignment rulescheckbox by selecting Show on edit page. If you select both options, the assignment checkbox is displayed and is checked by default. The default setting will force the assignment rule to run unless you also display the checkbox and a user manually deselects the checkbox.
Click OK and the click Save
Note
: Cases can also be assigned to the Default Case Owner if the assignment rules fail to locate an owner (the case doesn't meet the criteria of any active rule), this might give the appearance that the case is being assigned to the incorrect owner. You can change the Default Case Owner by customizing the 
Support Settings
. 
