### Topic: Manage Profile Lists | Salesforce
Manage Profile Lists | Salesforce
Manage Profile Lists
Profiles define how users access objects and data, and what they can do within the application. When you create users, you assign a profile to each one. To view the profiles in your organization, from Setup, enter 
Profiles
 in the 
Quick Find
 box, then select 
Profiles
.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Custom Profiles available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view profiles, and print profile lists:
“View Setup and Configuration”
To delete profile list views:
“Manage Profiles and Permission Sets”
To delete custom profiles:
“Manage Profiles and Permission Sets”
Viewing Enhanced Profile Lists
If enhanced profile list views are enabled for your organization, you can use additional tools to customize, navigate, manage, and print profile lists.
Show a filtered list of profiles by selecting a view from the drop-down list.
Delete a view by selecting it from the drop-down list and clicking 
Delete
.
Create a list view or edit an existing view
.
Create a profile
.
Print the list view by clicking 
.
Refresh the list view after creating or editing a view by clicking 
.
Edit permissions directly in the list view
.
View or edit a profile by clicking its name.
Delete a custom profile by clicking 
Del
 next to its name.
Note
You can’t delete a profile that’s assigned to a user, even if the user is inactive.
Viewing the Basic Profile List
Create a profile
.
View or edit a profile by clicking its name.
Delete a custom profile by clicking 
Del
 next to its name.
Creating and Editing Profile List Views
Edit Multiple Profiles with Profile List Views
If enhanced profile list views are enabled for your organization, you can change permissions in up to 200 profiles directly from the list view, without accessing individual profile pages.
See Also:
Edit Multiple Profiles with Profile List Views
Profiles
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=users_profiles_view.htm&language=en_US
Release
202.14
