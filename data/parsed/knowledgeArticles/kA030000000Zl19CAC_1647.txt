### Topic: Is it possible to increase the text size in Salesforce1 app? No
Is it possible to increase the text size in Salesforce1 app?
Resolution
No, it is not possible to increase the text size in the Salesforce1 app. Also, it is independent from the Text Size value defined in the Settings on iOS platform.
