### Topic: When does Discover data refresh?
Documentation advises clients who want to know when their Discover data is refreshed to contact support.
Resolution
Discover, like Audience Builder, refreshes data "overnight" via a program. 
To find when Discover refreshes, go to the client's programs, find the one using the ETL activities, and view the schedule.
