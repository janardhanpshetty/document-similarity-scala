### Topic: This issue applies only to processes that: - Contain scheduled actions AND - Were created or updated between in Spring '16 before February 3, 2016
This issue applies only to processes that:
- Contain scheduled actions AND
- Were created or updated between in Spring '16 before February 3, 2016
What is the issue?
Salesforce automatically deletes workflow scheduled actions if a record changes and no longer meets the associated criteria. Due to an issue discovered during the Spring ‘16 release, scheduled actions may remain in the queue and execute even if a record no longer meets the associated criteria.
The issue was fixed on February 3, 2016 but action may still be required to ensure that your processes created or updated prior to February 3, function as intended.
For more insight into the issue, access our Known Issue site 
here
 
Resolution
What action must I take?
If you created or updated a process with a scheduled action in Spring ‘16 before February 3, 2016, those scheduled actions may remain in your workflow queue and execute even if a record no longer meets the associated criteria.
For Active or Inactive processes, create a new version of your process using the following steps:
1. Access the Process Builder via Setup
2. Select your process
3. Click “Clone”
4. Name the new version of your process and click “Save”
5. Activate your process if the process should remain active
NOTE: If your process was deployed in a package, complete the above steps, then re-package and re-deploy the process.
If you suspect that your process may have created scheduled actions that are not relevant, delete those actions from your Paused and Waiting Interviews queue using the following instructions:
1. Access Flows via Setup (enter “Flows” in the Quick Find box for access). The Waiting Interviews related list displays any unexecuted scheduled actions in your organization, as well as any waiting flow interviews.
2. For each unexecuted group of scheduled actions that you want to delete, click “Del.” You can identify the correct row by the Flow Name field, which corresponds to the process’s Process Name.
