### Topic: Information on the process for changing the subscriber key on existing records as well as enabling Subscriber Key
Migrating Subscriber Key Info:
When an account has Subscriber Key enabled, Subscriber Key replaces Email Address as the unique identifier for a subscriber.
Clients may contact support inquiring on how to change the subscriber key value for an existing record. For example, they would like to start using a specific customer number as Subscriber Key instead of email address.
Should I enable Subscriber Key information. 
 
Resolution
All Subscriber Key migrations, whether they be enabling Subscriber Key or disabling Subscriber Key are handled by the client reaching out to their Account Executive.
When an account needs Subscriber Key enabled, the AE will create a request for the Provisioning Team to enable the business rule. 
When an account already has Subscriber Key specified on subscriber records and the client wants this value changed to some other value, this type of request now requires an SOW. The client should be directed to contact their Account Executive for a Service Engagement.
When a client needs to disable Subscriber Key, they will need to reach out to their AE in order to have a Service Engagement to delete duplicate records prior to Services or Provisioning disabling the business rule.
The attached internal document will help explain the complexities that this scenario introduces and the process that will be followed once work begins on the SOW. Please feel free to share with the AE when letting them know the client is interested in this type of migration. DO NOT share with the client.
------------------------------------------------------------------------------------------------------ 
As of 01/27/2015 - Open through Aloha/ > Google Docs > 
Click HERE to Open Google Doc
------------------------------------------------------------------------------------------------------ 
If subscriber data already exists in the account and the customer wants the Subscriber Key enabled, a service engagement is required. 
See the business rule page in base for Subscriber Key 
HERE
 for the most up-to-date information. 
This feature is free but only turned on at the client's request, and this is done by the Provisioning Team through their AE.
 
