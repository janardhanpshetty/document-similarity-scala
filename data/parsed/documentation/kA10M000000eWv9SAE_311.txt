### Topic: Encrypt Fields and Files | Salesforce
Encrypt Fields and Files | Salesforce
Encrypt Fields and Files
Specify the fields and files you want to encrypt. Remember that encryption is not the same thing as field-level security or object-level security. Those should already be in place before you implement your encryption strategy.
Available as add-on subscription in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions. Requires purchasing Salesforce Shield. Available in 
Developer
 Edition at no charge for organizations created in Summer ’15 and later.
Available in both Salesforce Classic and Lightning Experience.
Encrypt Fields
Select the fields you want to encrypt. When a field is encrypted, its value is masked for users who don’t have permission to view encrypted data.
Encrypt Files and Attachments
For another layer of data protection, encrypt files and attachments. If Shield Platform Encryption is on, the body of each file or attachment is encrypted when it’s uploaded.
Fix Compatibility Problems
When you select fields or files to encrypt, Salesforce automatically checks for potential side effects and warns you if any existing settings may pose a risk to data access or your normal use of Salesforce. You have some options for how to clear these problems up.
See Also:
Set Up Shield Platform Encryption
How Shield Platform Encryption Works
Platform Encryption Best Practices
Tradeoffs and Limitations of Shield Platform Encryption
Platform Encryption Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_pe_encrypt.htm&language=en_US
Release
202.14
