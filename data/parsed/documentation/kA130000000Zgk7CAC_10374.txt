### Topic: Options for Customizing the Connector | Salesforce
Options for Customizing the Connector | Salesforce
Options for Customizing the Connector
Customize the connector based on your preferences.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Select any of these options to customize your connector.
The option
Which
Automatically create prospects in Pardot if they’re created in Salesforce
Enables a trigger to set any newly created leads or contacts in Salesforce to sync to Pardot. This won’t happen for historical leads or contacts—only for those created after installing the Pardot AppExchange package in Salesforce, and will work only for leads and contacts created with an email address.
If this feature is enabled later on, any leads and contacts with email addresses that were created after the Pardot package was installed in Salesforce will be pushed down to Pardot. Select a default campaign from the dropdown for any prospect created with this option.
Automatically change emails in Pardot to reflect changes in Salesforce
Automatically updates email addresses for Pardot prospects if an email address of a lead or contact is updated in Salesforce.
Learn more about keeping email address updates in sync in 
About Email Address Updates Between Salesforce and Pardot
.
Automatically match Salesforce users to Pardot users
Lets you send emails from any user fields, including, for example, support contacts and account managers.
Learn how to set up this functionality is 
Send Emails from Salesforce
.
Exclude Salesforce Partner and Customer Portal users from Prospect assignment
Prevents Salesforce Partner and Customer Portal users from displaying in the “CRM Username” dropdown when you edit a user in Pardot.
Allow editing of Prospect lists within the CRM
Lets users add and remove Pardot prospects from within Salesforce. If you check this box, you must add the Visualforce elements to your Leads and Contacts page layouts.
Connect to a Salesforce sandbox account
Let’s you connect to a sandbox environment.
Sync emails with the CRM
Automatically logs an activity for every email sent to a prospect (list, drip, autoresponder and 1-to-1 prospect emails).
Note
This can quickly fill up your activity views, especially on an account object. Disabling 
Email Logging in Pardot
 is not retroactive. If the email logging may interfere with your workflow in Salesforce, we recommended that you do not enable this option.
Sync plugin emails with the CRM
Logs an activity for emails you send through Pardot for Microsoft® Outlook®, our Thunderbird extension, our Apple Mail plug-in, or our add-in for Gmail. 
Enable Salesforce emails for task creation
Lets you send a notification email to users when a task is created.
Enable Salesforce emails for assignment rules
Lets Salesforce send a notification email to users when assignment rules run.
Enable Salesforce emails for user and queue assignments
Lets Salesforce send a notification email to users when an assignment takes place for a user or queue.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=pardot_get_start_production_add_connector_optional_settings.htm&language=en_US
Release
202.14
