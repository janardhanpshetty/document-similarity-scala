### Topic: Support: Handling Increasing the Maximum total response size for Visualforce Remoting Request
When customer exceeds the Visualforce RemoteAction response size limit they will get the following error message: 
"Remoting response size exceeded maximum of 15 MB." 
Customer would like to increase the limit for RemoteAction from 15MB to 20MB.
Resolution
When a customer requests to increase Visualforce or Javascript remoting response size please let them know that:
The limits of 15 MB are in place for very good reasons: 
1) Anything above 15 MB for a Web page (the primary purpose of VF) is unusable anyway, and
2) Shared resource limits ensure that other customers don't suffer slow response times because of one org.
 
The maximum default limit for remote response size is 15 MB for an org.
This limit is not increased for any reason as
 it might cause performance degradation in the service. A temporary increase requires valid business reason and requires 
Product Management approval.
To get the approval Tier-3 needs to log an investigation with the R&D to proceed; as they need to know why the customer thinks 15 MB is not enough.
Again this does not 
guarantee that the R&D would approve the limit increase.
Here are best practices that would help the customer to stay with in the 15 MB limit 
http://www.salesforce.com/us/developer/docs/pages/Content/pages_js_remoting.htm#handling_remote_response
