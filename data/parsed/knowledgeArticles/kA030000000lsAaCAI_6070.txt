### Topic: This article will go over a work around for the Clone Button being missing from the Event in your Calendar.
How come the Clone Button has been added to my page layout for Events, but I don't observe it when I am actually in the Event?
 
Resolution
The Save& New Event serves as the same behavior as the Clone button.
If you Edit an existing or past event, the option to Save & New Event will be available. When you click this button, it takes you to a new event,  capturing all details from the "cloned" event.
There are ideas in the IdeaExchanges suggesting improvements to the Calendar features.
1.) Calendar Improvements Desperately Needed:
https://success.salesforce.com/ideaView?id=08730000000Kpd7AAC
2.) Improve Calendar UI - More Google Like:
https://success.salesforce.com/ideaView?id=08730000000Br2FAAS
