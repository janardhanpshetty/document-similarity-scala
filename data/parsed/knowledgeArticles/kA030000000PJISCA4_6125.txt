### Topic: After creating an Email Alert or Outbound message for a Workflow that fires upon publishing an Article, it is not being evaluated.
After creating an Email Alert or Outbound message for a Workflow that fires upon publishing an Article, it is not being evaluated.
Resolution
This is expected behavior.
Please reference the following article:
Salesforce Knowledge Workflow Rules and Approval Processes
 
Note at the bottom of the page:
Workflow rules and approval processes apply to the “Draft to Publication” portion of the article publishing cycle. Approval processes aren't available for translation or archiving.
When an article is published from the edit page, the article is first saved and then published. Workflow rules apply to the saved draft article but not the published article.
 
As triggers cannot be used with Knowledge Objects, please promote the following idea (which is currently under consideration) on the IdeaExchange:
Workflow Rules and Email Alerts for Knowledge
https://success.salesforce.com/ideaView?id=08730000000I9m7AAC
