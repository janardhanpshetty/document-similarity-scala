### Topic: Customers may get an error "Your import failed: API Name Lead is not supported" when inserting records using Data Import Wizard, the legacy importer might also fail with the Internal Sever Error Message ending in -115424815
When Data Import Wizard is used to perform Insert / Update or Upsert operation on the lead object it might fail with an error  "Your import failed: API Name Lead is not supported" when inserting records using Data Import Wizard, the legacy importer might also fail with the Internal Sever Error Message ending in -115424815. Please note that the issue is specific to importing Lead records only and when the legacy importers ends with the Internal Server Error Message, if you get the same UI message but legacy importer is working fine the issue may not be related to this article. Contact salesforce support so that the issue can be investigated accordingly.
Resolution
When there are too many users with CRUD rights on a combination of Account, Contact, Solution, Lead, and Person Accounts then it's possible for Data Import Wizard to fail when mapping owners on an import. This also appears to cause the legacy importer to fail. At this time the exact upper limit and combination of CRUD that causes this error is being investigated to better handle this unintended behavior.
The customers can use the below workarounds
1) Please do not map the Owner ID field during the initial import, once the import completes you can update the owners of the specific leads in a separate update call.
2) Please use 
Apex Data Loader
 to perform insert operation
3) You can also use 
Mass Transfer Records Wizard
 to transfer the ownership of the newly created records provided they have to be assigned to the same owner.
