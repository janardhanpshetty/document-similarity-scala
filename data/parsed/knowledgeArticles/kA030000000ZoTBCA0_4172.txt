### Topic: Updating the amount field in the same minute is not tracking all changes in the stage history but do in the field history.
Going into an opportunity and adding a product and then removing a product within the same minute will display all changes in the field history but not in the stage history:
Please see stage history where we have several changes to the history
However when you look at the stage history you only see one line:
Resolution
This is being done by design. There is a setting in the stage history making it, so when changes are made within a 5 min window on opportunity line items, we only update the previous stage history record instead of creating a new history record. ​
