### Topic: apex:actionFunction names can collide with other javascript functions causing unexpected problems.
If Users poorly choose the name for an actionFunction, it will collide with (and replace) already defined functions with the same name. For example a customer creates actionFunctions named addEvent, removeEvent, and updateEvent. This will cause a page to go haywire when rendered/loaded because addEvent and removeEvent are global functions defined in main.js. Interestingly this behavior only happens with showHeader=true.
Resolution
Repro
Create this VF page: 
<apex:page > 
<apex:form > 
<apex:actionFunction name="addEvent"/> 
</apex:form> 
</apex:page> 
When you load the page, it will continually reload. If you add showHeader="false" it does not happen.
Workaround
The workaround is to namespace your actionFunction. If I change the name to myOrg.addEvent, then all is well. 
A good practice would be to always define a namespace for User's actionFunctions, or even namespace our addEvent and removeEvent functions. 
In the absence of a code change, we should document that customer's should be careful when they name their actionFunctions, and ask them to manually namespace them.
GUS Investigation: 
W-1755965
