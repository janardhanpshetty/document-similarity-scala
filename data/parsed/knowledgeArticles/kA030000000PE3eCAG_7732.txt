### Topic: If you wish to review your Account's Usage Report, please follow this article.
We tend to only review this usage report during an Account's Health Check, but sometimes it is very useful to discuss the trends in TLP or lack of usage during the year. To do this go to the Usage History Related List on the Account.
Resolution
Feel free to save as the below report to display the stats on your account:
Premier Support: Account Usage History
https://na1.salesforce.com/00O30000003J6Au
