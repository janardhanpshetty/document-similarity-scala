### Topic: Understanding the Contributor’s Page Editing View | Salesforce
Understanding the Contributor’s Page Editing View | Salesforce
Understanding the Contributor’s Page Editing View
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To edit only content in Site.com sites:
Site.com Contributor User
 field enabled on the user detail page
AND
Contributor role assigned at the site level
Open a site page on the Overview tab by double-clicking the page or hovering over it and clicking 
| 
Edit
. The page opens as a new tab.
Using the toolbar (1), you can:
Undo and redo your actions.
Import assets
, such as images and files.
Preview the page
 in a browser window.
Update the appearance of the page
 using the Branding Editor.
Using the Page Elements pane (2), you can 
drag content blocks and widgets
 (if available) to editable areas of the page.
On the page canvas (3), you can 
edit page text
 and 
add images
. If editable areas are available, you can drag page elements to the page.
Using the live mode options (4), you can 
see how the page appears on various devices
 when the page is live.
See Also:
Creating Site Pages as a Site.com Contributor
Using Site.com Studio as a Contributor
Understanding the Inline Editing Toolbar
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_editor_page_edit.htm&language=en_US
Release
202.14
