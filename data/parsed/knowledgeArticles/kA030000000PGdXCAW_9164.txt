### Topic: Lists of URL Formats that can be seen in Salesforce
What are the different URL formats that you may see for Salesforce Pages / Sites?
Resolution
MyDomain: 
The protocol: https://
The subdomain prefix: your brand or term
The domain: my.salesforce.com (For developer editions: developer-edition.my.salesforce.com)
Examples:
https://universalcontainers.my.salesforce.com
https://MyCompany-developer-edition.my.salesforce.com
 
Force.com UnSecure Sites:
The protocol: http://
The subdomain: Your Site name
The domain: .force.com
Examples: 
http://www.mycompany.force.com
http://mycompany.force.com/developers
 
 
Force.com Secure Sites:
Protocol: https://
The subdomain : Your Site Name
The Domain: DIFFERS BY EDITION. See Examples below:
Developer Edition -  https://mycompany-developer-edition.na1.force.com 
Sandbox - https://mycompany.mysandbox.cs1.force.com 
Production - https://mycompany.secure.force.com 
 
 
