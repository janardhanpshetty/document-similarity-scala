### Topic: Create HTML Email Templates | Salesforce
Create HTML Email Templates | Salesforce
Create HTML Email Templates
You can create HTML email templates using letterhead. You can’t change the letterhead or the layout in an existing template. To create an HTML email template, you must have at least one active letterhead.
Available in: Salesforce Classic
Available in: 
All
 Editions
Mass email not available in: 
Personal
, 
Contact Manager
, and 
Group
 Editions
HTML and Visualforce email templates not available in: 
Personal
 Edition
User Permissions Needed
To create or change HTML email templates:
“Edit HTML Templates”
To create or change public email template folders:
“Manage Public Templates”
To create an HTML email template:
Do one of the following:
If you have permission to edit public templates, from Setup, enter 
Email Templates
 in the 
Quick Find
 box, then select 
Email Templates
.
If you don’t have permission to edit public templates, go to your personal settings. Enter 
Templates
 in the 
Quick Find
 box, then select 
Email Templates
 or 
My Templates
—whichever one appears.
Click 
New Template
.
Choose 
HTML (using Letterhead)
 and click 
Next
.
Choose a folder in which to store the template.
To make the template available for use, select the 
Available For Use
 checkbox.
Enter an 
Email Template Name
.
If necessary, change the 
Template Unique Name
. This unique name refers to the component when you use the Force.com API. In managed packages, this unique name prevents naming conflicts in package installations. This name can contain only underscores and alphanumeric characters, and must be unique in your org. It must begin with a letter, not include spaces, not end with an underscore, and not contain two consecutive underscores. With the 
Template Unique Name
 field, you can change certain components’ names in a managed package and the changes are reflected in a subscriber’s organization.
Select a 
Letterhead
. The letterhead determines the logo, page color, and text settings of your email. In an existing template, you can’t change this field. To use a different letterhead after you’ve created a template, create another template.
Select the 
Email Layout
. The email layout determines the columns and page layout of the message text. To see samples, click 
View Email Layout Options
. In an existing template, you can’t change this field. To use a different email layout after you’ve created a template, create another template.
If desired, choose a different character set from the 
Encoding
 drop-down list.
Enter a 
Description
 of the template. Both template name and description are for your internal use only. The description is used as the title of any email activities you log when sending mass email.
Click 
Next
.
Enter a subject for the email you send.
Enter the text of the message by clicking any section and entering text. To prevent anyone using the template from editing a section, click the padlock icon.
Change the style of your text by selecting the text and using the format toolbar.
If desired, enter merge fields in the template subject and body. When you send an email, these fields are replaced with information from your lead, contact, account, opportunity, case, or solution.
Click 
Next
.
If desired, enter the text-only version of your email or click 
Copy text from HTML version
 to paste the text from your HTML version without the HTML tags automatically. The text-only version is available to recipients who can’t view HTML emails.
Warning
We recommend that you leave the text-only version blank. If you leave it blank, Salesforce automatically creates the text-only content based on the current HTML version. If you enter content manually, subsequent edits to the HTML version aren't reflected in the text-only version.
Click 
Save
.
Tip
View a sample of the template populated with data from records you choose and send a test email by clicking 
Send Test and Verify Merge Fields
.
See Also:
Create Custom HTML Email Templates
Create Visualforce Email Templates
Adding Images to Email Templates
Create a Letterhead
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=creating_html_email_templates.htm&language=en_US
Release
202.14
