### Topic: Available Events and Actions | Salesforce
Available Events and Actions | Salesforce
Available Events and Actions
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
Choose from several event triggers and actions when you 
create an event
.
When This Event Occurs...
Event
Occurs When...
Double click
The user double-clicks the page element.
Click
The user clicks the page element.
Focus
The focus moves to the page element.
Load
The page or page element is loaded in a browser window.
Blur
The focus moves from the page element.
Mouse in
The user moves the mouse pointer over the page element.
Mouse out
The user moves the mouse pointer out of the page element.
Trigger This Action...
Action
Description
Add CSS Class
Dynamically adds a CSS class to style the targeted item. For example, to alter the appearance of a page element, you could add a new CSS class to it.
Alert
Displays a popup browser alert message.
Animate
Animates CSS properties, such as Top, Left, Width, and Height, which you specify by entering appropriate values in the 
CSS
 field.
For example, if targeting an image, you can enter values such as 
opacity: "0.4", width: "70%"
, which changes the image's appearance according to the speed and effect you set.
Delay
Adds a delay (measured in milliseconds) before the action that follows. (Ensure you select the 
Chained
 checkbox to tie it to the subsequent action.)
Execute JavaScript
Runs custom JavaScript code, which you enter by clicking 
Edit Script
 to open the Custom Code Editor.
Go To Page
Goes to the designated page number in data repeaters and data tables. See 
Adding Pagination to Data Repeaters and Data Tables
.
Hide Element
Hides the targeted item according to the speed and effect you set.
Next Page
Goes to the next page in data repeaters and data tables. See 
Adding Pagination to Data Repeaters and Data Tables
.
Previous Page
Goes to the previous page in data repeaters and data tables. See 
Adding Pagination to Data Repeaters and Data Tables
.
Remove CSS Class
Removes a CSS class from the targeted item to dynamically remove its style. For example, to alter the appearance of a page element, you could remove the CSS class associated with it and replace it with another.
Repeat
Repeats the action that follows by the specified number of times, with the specified delay between each occurrence. (Ensure you select the 
Chained
 checkbox in the Properties pane to tie it to the following action.)
Set Element Attribute
Dynamically sets the specified attribute value of the targeted item. For example, if targeting an image, you could change the image source by entering 
src
 in the 
Attribute Name
 field and entering the image URL in the 
Attribute Value
 field. You can also add custom name/value pairs for advanced coding purposes.
Show Element
Reveals the targeted item according to the speed and effect you set.
Submit
Submits the selected form's data.
Toggle Element
Switches the visibility of the targeted element according to the speed and effect you set.
See Also:
Adding Pagination to Data Repeaters and Data Tables
Events Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_events_available.htm&language=en_US
Release
202.14
