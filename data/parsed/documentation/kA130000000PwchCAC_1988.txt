### Topic: Searchable Fields: Note | Salesforce
Searchable Fields: Note | Salesforce
Searchable Fields: Note
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 editions
Searchable Fields
Sidebar Search
Advanced Search
Global Search
Body
Title
See Also:
Searchable Fields by Object in Salesforce Classic
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_fields_note.htm&language=en_US
Release
202.14
