### Topic: Define Custom Object Fields | Salesforce
Define Custom Object Fields | Salesforce
Define Custom Object Fields
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Divisions are not available in 
Database.com
.
User Permissions Needed
To view and edit standard fields:
“Customize Application”
To create custom fields:
“Customize Application”
Custom object fields store the data for your custom object records.
Custom Fields for Custom Objects
You can create custom fields to store information unique to your organization. You can also create custom relationship fields to associate your custom object with another object in Salesforce.
Standard Fields for Custom Objects
Custom objects automatically include the following standard fields. Click 
Edit
 to modify any of the editable fields.
Field
Description
Created By
User who created the record.
Currency
Currency of the record if multicurrency is enabled.
Division
Division to which the custom object record belongs. Custom objects that are “detail” objects in a master-detail relationship inherit their division from the master object. Custom objects that are not related to other records are automatically in the global division. Available only in organizations that use divisions to segment their data.
Last Modified By
User who most recently changed the record.
Name
Identifier for the custom object record. This name appears in page layouts, related lists, lookup dialogs, search results, and key lists on tab home pages. By default, this field is added to the custom object page layout as a required field. 
Owner
Assigned owner of the custom object record. If the custom object becomes the detail side of a master-detail relationship, this field is removed, as ownership of the data is controlled by the master object, or by the primary master object for a custom object with two master-detail relationships.
Note
Custom objects on the “detail” side of a master-detail relationship can't have sharing rules, manual sharing, or queues, as these require the 
Owner
 field.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dev_objectfields.htm&language=en_US
Release
202.14
