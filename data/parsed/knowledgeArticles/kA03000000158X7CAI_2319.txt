### Topic: This KB tells about the process how we can enable BT perm which would allow customer to deploy Flow in active state.
Customer is asking that they want to deploy Flow in active State.
In Summer 15' we built the ability to over ride the deployment of Flows.  Currently when customer deploy the Flow,it deploys as inactive.
Resolution
We will likely enable customers to do this via an org perm in a future release.  However,if the customer is asking to get it done here is what could be done:-
Flow PM can enable in their org which would allow the customer to deploy Flow in active state.
That stated if the override is on, its a a major change to behavior and we want careful turn it on.In other words we want to control its use at the PM level.We also want to monitor how's working for customers. 
This perm was added to support continuous deployment, this means it will not only deploy a flow version as active but it will also overwrite any version even if it is active. Customers should be very careful using this feature and it should not be turned on without complete understanding of the implications.
 Below are the 
prerequisites
 
to get it activated :-
1.  Customers who wants this should test in sandbox.   If the provide a sandbox ID we'll enable the perm. 
 
2.  Once they've confirmed they've tested and understand the behavior we can enable prod orgs.
Tier  2 - Please escalate it to Dev T3.
Tier 3 - Plese contact PM by @mentioning him/her or emailing him/her to get it enabled in the org.
Note : This feature needs PM approval to be enabled.
