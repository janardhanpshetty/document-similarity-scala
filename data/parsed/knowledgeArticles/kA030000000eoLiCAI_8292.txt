### Topic: Considerations and steps for disabling Wave Analytics.
Considerations before disabling Wave Analytics:
1. The Default Wave Dataflow will be lost. Back up the Dataflow Definition through the Data Monitor before proceeding with disablement.
2. The Integration and Security Users created by Wave will be set as inactive. The Profiles associated with these users will remain intact.
3. Datasets, Dashboards, and Lenses will remain intact, but will not be accessible.
4. Wave must be disabled prior to the related licenses being deprovisioned or access to the disable functionality will be removed. If licenses have been deprovisioned prior to disablement, please contact Salesforce Support for assistance.
Resolution
Steps to disable Wave Analytics:
1. Setup | Wave Analytics | Getting Started
2. Click "Disable Analytics"
