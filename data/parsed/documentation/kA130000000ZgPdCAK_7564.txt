### Topic: Launch a Flow from a Process | Salesforce
Launch a Flow from a Process | Salesforce
Launch a Flow from a Process
Start an autolaunched flow from your process to automate complex business processes—create flows to perform logic and have events trigger the flows via processes—without writing code.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, or view processes:
“Manage Force.com Flow”
AND
“View All Data”
In order to launch a flow from a process, you must create and activate the flow. The flow must be autolaunched. For details, see 
Create a Flow
.
Enter a name for this action. 
This text appears on the canvas and helps you differentiate this action from others in your process. The name is truncated to fit on the canvas.
For 
Flow
, search for and select the flow that you want to launch from this process.
Only active, autolaunched flows are available.
Optionally, click 
Add Row
 to set values for the flow’s variables.
Flow Variable
Start typing the name of the flow variable whose value you want to set, or click 
 to select a flow variable from the drop-down list.
You can set values for any variables in the flow that have 
Input/Output Type
 set to Input Only or Input and Output.
Type
Select the type of value that you want to set. For example, select 
String
 to manually enter the values for a Text collection variable, or select 
Reference
 to use the value of a record for an sObject variable.
Value
Set a value for the flow variable.
For collection variables, use the text entry field to specify a value. The value must match the collection variable's data type.
For sObject variables, use the field picker to select an ID field. The ID must correspond to a record whose object type matches the sObject variable’s object type.
For sObject collection variables, use the field picker to select a related list. The selected records’ object type must match the sObject collection variable’s object type. For example, populate an sObject collection variable with all child contact records associated with the account that started the process.
Note
If the related list is empty when the flow tries to populate the sObject collection variable with field values from those records, the process fails.
Click 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=process_action_flow.htm&language=en_US
Release
202.14
