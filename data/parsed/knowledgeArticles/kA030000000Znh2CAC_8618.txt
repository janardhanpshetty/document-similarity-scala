### Topic: Individual Email Result Sent Events are not posting are not posting in the Salesforce Marketing Cloud Connector v3.
In version 1.29 and higher of  the Salesforce Marketing Cloud Connector version 3, a new configurable option was added to the Admin Console, which allows direct control for whether Sent Events are returned with Individual Email Results. This option is unselected by default.
If an account was previously returning Individual Level Tracking, there is a behavior change after installing the upgrade to version 1.29 or higher. Sent Events are no longer returned by default.
Resolution
In order to return Sent Events,  follow the steps below:
1. With a System Administrator user, access the ExactTarget tab.
2. Select the sub-tab 
Admin Console
.
3. Under the 
Tracking
 section, select the 
Sent Events Tracking 
checkbox.
4. Click 
Submit.
Sent Events will be returned with Individual Email Results for all sends moving forward.
