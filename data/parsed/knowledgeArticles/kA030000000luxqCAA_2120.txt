### Topic: If you are interested in some sample Workflow & Validation Rules as well as some sample Approval Processes, make sure to check out these workbooks.
Automation & Validation Samples for your Salesforce Instance
To help you get some ideas for useful Workflow Rules, Validation Rules or Approval Processes, make sure to check out the cookbooks for each of the features below with some sample processes described:
Workflow Rules:
https://login.salesforce.com/help/pdfs/en/salesforce_useful_workflow_rules.pdf
Validation Rules:
https://login.salesforce.com/help/pdfs/en/salesforce_useful_validation_formulas.pdf
https://help.salesforce.com/HTViewHelpDoc?id=fields_useful_validation_formulas_account_address.htm&language=en_US
Approval Processes:
https://login.salesforce.com/help/pdfs/en/salesforce_useful_approval_processes.pdf
 
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
