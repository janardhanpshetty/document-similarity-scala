### Topic: Exclude the Confidential Information Disclaimer from Reports | Salesforce
Exclude the Confidential Information Disclaimer from Reports | Salesforce
Exclude the Confidential Information Disclaimer from Reports
By default, report footers include a disclaimer that reads “Confidential Information - Do Not Distribute”. The disclaimer reminds users to be mindful of who they share reports with, helping to ensure that third parties don’t view your reports. At your discretion, exclude the disclaimer from your reports.
Available in: Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
User Permissions Needed
To modify report and dashboard settings:
“Customize Application”
From Setup, enter 
Reports and Dashboards Settings
 in the 
Quick Find
 box, then select 
Reports and Dashboards Settings
.
Select 
Exclude Disclaimer from Exported Reports
 and 
Exclude Disclaimer from Report Run Pages and from Printable View Pages
.
Click 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=exclude_report_disclaimer.htm&language=en_US
Release
202.14
