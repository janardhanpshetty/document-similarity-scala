### Topic: create a report on contacts and accounts and then add a custom filter
Resolution
Please create a new report using the standard report type "
Contacts & Accounts
"
use the: contact created date range all time
and then add a custom filter:
birthday equals 
this month
, the report will appear as the following example:
Please note: for further examples of Relative dates that can be used on reports, please r
eview the article 
Relative Date Values for Filter Criteria
2) If you need to create a list of contacts and order it by month when they celebrate the birthday, please create new report using the standard report type "
Contacts & Accounts
"
use the: contact created date range all time
Change the format of the report into "summary" clicking on the arrow next to "tabular format" and then selecting summary:
Drag and drop the birthdate field into the "drop a field here to create a grouping" section.
Click on the white arrow next to the grouping you just created and select:
group dates by> Calendar month in Year.
You will then see your contacts ordered by month when they celebrate the birthday.
