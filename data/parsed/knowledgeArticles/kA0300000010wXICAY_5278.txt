### Topic: A canvas app embedded into an object's page layout cannot be displayed by an <apex:detail> component contained in a Visualforce page that is part of a package, unless the canvas app is part of the same package.
A canvas app embedded into an object's page layout cannot be displayed by an <apex:detail> component contained in a Visualforce page that is part of a package, unless the canvas app is part of the same package. If the canvas app is not part of the same package as the Visualforce page, the following error is displayed:
"Oops, there was an error rendering Force.com Canvas application [Canvas_App_Name]. Your browsing session has ended or is invalid. Please re-login to Salesforce.com again."
Resolution
For security reasons, packaged (third party) Visualforce pages are prevented from loading canvas apps unless they are part of the same package as the Visualforce pages.
