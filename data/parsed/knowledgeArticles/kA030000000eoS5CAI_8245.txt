### Topic: Information regarding "Insufficient access rights on cross-reference id" error in Wave Analytics Dataset Creator
When creating a dataset with the Dataset Creator, you can choose which app to create the dataset within. If you choose an app created with a Sales Wave template, you will receive the following error:
Insufficient access rights on cross-reference id
This is expected behaviour, as the Dataset Creator does not have access to Sales Wave apps.
Resolution
To correct this, choose an app that is not Sales Wave templated.
If you wish to create a dataset within the Sales Wave app, you will need to manually modify the Sales Wave dataflow definition.
