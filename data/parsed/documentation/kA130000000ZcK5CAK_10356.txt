### Topic: Configure Social Accounts, Contacts, and Leads | Salesforce
Configure Social Accounts, Contacts, and Leads | Salesforce
Configure Social Accounts, Contacts, and Leads
You can disable the Social Accounts, Contacts, and Leads feature or turn some social networks on or off.
Available in: Salesforce Classic and Lightning Experience
Business accounts available in: 
All
 Editions except 
Database.com
Person accounts available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Contacts available in 
All
 Editions except 
Database.com
Leads available in: 
Groups
, 
Professional
, 
Enterprise
, 
Unlimited
, and 
Developer
 Editions
If you disable the Social Accounts, Contacts, and Leads feature for your login, you won’t see any social networks for your accounts, contacts, or leads. Or, if you turn off access to an individual social network, you won’t be able to view or access data for that social network.
Note
In Salesforce Lightning Experience and the Salesforce1 mobile app, users can access Twitter only, even if other networks are enabled.
From your personal settings, enter 
Social
 in the 
Quick Find
 box, then select 
My Social Accounts and Contacts Settings
, 
My Social Accounts and Contacts
, or 
Settings
—whichever one appears.
Set up Social Accounts and Contacts so that it works the way you want it to.
Click 
Save
.
See Also:
Troubleshoot Social Accounts, Contacts, and Leads
Social Accounts, Contacts, and Leads
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=social_networks_configue.htm&language=en_US
Release
202.14
