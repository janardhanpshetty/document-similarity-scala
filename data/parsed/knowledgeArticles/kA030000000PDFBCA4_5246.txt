### Topic: My data has been lost or deleted and I don't have a backup, can Salesforce help us recover it? Learn about the Data Recovery Service.
In the event that your data has been lost or deleted with no backup, Salesforce may be able to assist you with recovering the data. Learn more below. 
Resolution
 
General Information
What is Data Recovery?
Data Recovery is a last resort process where Salesforce Support can recover your data at a specific point in time in case it has been permanently deleted or mangled during a data import. Data Recovery was formerly known as "Data Restoration."
 
What does this process cost?
 
Because of the manual intervention, there is a cost. The cost is relative to the amount of manual work and time needed to perform the recovery. 
The price for this service is a flat rate of $US 10,000 
(Ten Thousand US Dollars) for the one Organization that's being recovered. The work involved actually costs us much more than that, but we pay for a portion of the service.
A Data Recovery is only an option after you have exhausted all other reasonable efforts to recover the data, such as restoring from the recycle bin, reinserting the data from a .CSV backup or querying the API for 
IsDeleted
 records. If you're interested in the Data Recovery process please log a Support case stating that you would like to learn more about the data recovery process.
 
Is it preventable?
 
Yes, we recommend that you use a partner backup solution that can be found on the 
Appexchange
, run full reports and export them to your desktop or use the 
Data Export
 feature that is included in all editions of Salesforce. Anytime you'll be making changes within your Organization (Example: Changing field data type or updating data with the Data Loader/Import Wizard) we strongly recommend that you use one of the above methods to back up any data that could be affected by the change. It's better to be safe than sorry.
 
What do I get from the Data Recovery Service?
 
You'll get a complete backup of all your data in .CSV files on the date/time you specify. Each object in your Salesforce Organization will be exported into the .CSV file and provided to you via secured channels. You can then use our importing tools to insert the data back into your Organization.
 
Important: 
We cannot insert the data into your Organization for you, but if you contact your Account Executive they can give you information about hiring professional services to do the work for you.
 
What is a .CSV file?
 
A .CSV (comma separated value) file is a format that is widely used in the industry and can be opened in Microsoft Excel or Open Office. There'll be a CSV file for each of the database tables (Objects) in Salesforce.
 
Does it include metadata?
 
No, it doesn't include metadata. It holds the customer data only. Metadata is a structured data that tells how Reports, List Views, and Page Layouts are displayed.
 
How far back can the data be recovered?
 
Data can be recovered up to 3 months back from the current date. There are a few stages:
 
Trial Organizations are kept for as long as the trial states (7 days, 14 days or 30 days). If it’s not converted or a contract is signed against the Organization before the end of the trial, it's put on hold and kept for 30 more days. At that point it’s deleted. We can go back 90 days to recover from today’s date.
Production Organizations are put on hold when the contract expires and kept for 180 more days. At that point it’s deleted. We can go back 90 days from today’s date.
 
Note:
 At any time when it’s on hold (which is 30 days for trial or 180 days for Org that once had a contract), we still have the data and no data recovery needs to be performed. We can do a data export or if they are to sign a contract, the Organization will be flipped back on.
 
How long does it take?
 
The process must be completed manually and usually takes a minimum of 20 business days (4 calendar weeks).
 
How do I get the process started?
First, if your Org is API enabled (Enterprise Edition and above), make sure an export of all operations with the Data Loader tool has been completed first using the condition where Isdeleted="True." Review our Help documentation on 
How to determine which Salesforce user deleted a record?
 for more details. If you don't know who deleted the records, please have a System Administrator log a case with Salesforce Support. 
Once you're ready for the Data Recovery, please log another case with Support. In the subject type, "Can you please perform a Data Recovery?" In the description provide the date/time you would like the data recovered from. An agent will contact you to confirm that there's no other way to recover the data and confirm pricing details. You'll be able to pay via purchase order number or credit card.
 
FAQ
 
Q: We converted/deleted a custom field and lost its data, can we get the data back?
A
: Yes. The custom field wizard notifies you that changes to field type may permanently delete data. We do our best to warn you of changes that can delete/mangle data, but in the unfortunate event that this occurs, the data can be recovered through this process. As previously stated we still strongly recommend that you back up data before making any kind of changes in your Organization.
 
Q: If my trial Organization is on hold and who can do the data export?
A
: The system admin can still login and the only thing they can do is a Data Export.
 
Q: Can the data recovery services restore List Views, Reports, or Workflows?
A
: We can only recover structured data, like records from the Account Object. List views, Reports, and Workflows are considered metadata, which tells how to display the data.
 
Q: 20 business days is a lot of time, can the process be expedited?
A
: Unfortunately no. It involves multiple Applied Engineering resources to get the backups and recover the database. It can take up to 10 days end-to-end to "rewind" a server to your specified point in time and then additional time is needed to export the data.
 
Q: Is the Data recovery process a rollback feature that restores the data to a specific date?
A
: The Data Recovery service has the same contents of a 
Salesforce Data Export Service
. Once we perform the data recovery, the .CSV files given to the requestor would have to be imported back into Salesforce using an external API tool such as Data Loader/Workbench or by seeking professional service specialized in importing deleted data.
