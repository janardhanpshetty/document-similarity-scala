### Topic: A customer has refreshed their sandbox and reconfigured their Branded Login page to redirect or display options to offer SAML login via their IdP or other authentication method however even with these options added and navigating to the My Domain URL (company.cs*.my.salesforce.com) they are instead presented with the standard login page under their My Domain URL.
A customer has refreshed their sandbox and reconfigured their Branded Login page to redirect or display options to offer SAML login via their IdP or other authentication method however even with these options added and navigating to the My Domain URL (company.cs*.my.salesforce.com) they are instead presented with the standard login page under their My Domain URL.
 
Resolution
The issue is caused by the previous sandbox that the refresh replaced it may be "stuck" in another status other than "Pending Delete" which schedules a sandbox for disconnect and deletion. 
- R&D / CCE (Some Tier 3 depending on Permissions) will be required to set the previous Sandbox as "Pending Delete", to check if the previous Sandbox is still active go to the Production org attached to the Sandbox with the issue noting the current Sandbox name. 
- Under 
Setup (BT) > Data Management > Sandboxes
 click on the corresponding Sandbox name you are concerned with. On this page you will see a number of sections however the one we need is "Previous Sandbox Organization" and the field "Previous Sandbox Org ID"
- The field will contain the ID of the last Sandbox prior to the current Active one, copy this ID and search the Sandbox Blacktab for it, if you can't find it the org has most likely been deleted, if you can find it check the status of the org and if it is not "Pending Delete" please log an investigation with the Sandbox R&D team to set it to "Pending Delete" status.
- Once the previous Sandbox has been deleted you will need the customer to go to 
Setup > Domain Management > My Domain
 and under "
Login Page Settings" section edit and remove the IdP provider option by unchecking the box and saving the page. Once this is done, repeat and recheck the box for the IdP login again saving the page. This should now resolve the issue.
