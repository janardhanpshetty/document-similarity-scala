### Topic: Enable Lightning Experience on developer org
Customers using 
Developer Org 
may not have the 
Lightning tab
 visible to 
Enable
 the lightning feature.Customer's may raise a Case with support to 
Enable
 or to 
Activate the Lightning Experience.
 
Resolution
Escalate the case to 
Tier 3 
with the 
Developer Org
 
Id
 along with the required Information.
Once Tier 3 enables the 
"Allow SFX / Lightning Desktop  from off to on"
 permission on BT ,Lightning tab will be visible in the developer edition Org and customer will find this above Salesforce1 Quick Start on left side panel on his org.
Customer will need to manually click on the tab and then 
Enable
 the 
Lightning feature
 for users.
