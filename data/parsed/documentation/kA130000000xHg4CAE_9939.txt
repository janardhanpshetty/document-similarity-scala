### Topic: Reorder the App Launcher Apps in Salesforce Lightning Experience | Salesforce
Reorder the App Launcher Apps in Salesforce Lightning Experience | Salesforce
Reorder the App Launcher Apps in Salesforce Lightning Experience
You can change the organization’s default visibility and order in which apps appear in the App Launcher.
Available in: Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view apps:
“View Setup and Configuration”
To manage apps:
“Customize Application”
The App Launcher displays all a user’s available Salesforce apps, and any connected apps an administrator installs for the organization. 
As an administrator, you can use the App Launcher to set the default sort order and visibility for the apps in your organization.
 Then, users can reorder their own view within the App Launcher.
From Setup, do one of the following:
Enter 
Apps
 in the 
Quick Find
 box, then select 
Apps
, and then click 
Reorder
.
Enter 
App Menu
 in the 
Quick Find
 box, then select 
App Menu
.
Drag the apps in the list, as desired, to change the app order. The changes take effect immediately.
Click 
Visible
 or 
Hidden
 to show or hide individual apps from the App Launcher for all users of the organization.
All the apps installed in the organization are shown for sorting. However, the apps that a user sees in the App Launcher varies depending on each app’s visibility settings and the user’s permissions. For example, an administrator can typically see more apps than a standard user. You can see all the apps you have permission for, and that are visible to your profile. Connected apps and service providers must have a Start URL to be listed.
See Also:
What Is an App?
Open a Different App
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reordering_applauncher_lex.htm&language=en_US
Release
202.14
