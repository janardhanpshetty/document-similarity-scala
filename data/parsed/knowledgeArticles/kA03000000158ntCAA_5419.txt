### Topic: This article explains why we cannot postpone or advance the release on an instance for a customer
Customers may encounter issues due to which they would like to either postpone or advance the release. An example would be customer's current code might not work well with Spring 16 release, so they would want to postpone the release by a week so that they can fix the code before the instance gets upgraded.
Unfortunately we cannot postpone or advance the release for a particular org in an instance.
Resolution
Salesforce does not advance or postpone release on an instance for any customer. 
We are a multi-tenant organization, which means thousands of customers share the same infrastructure (instance). Releases are to infrastructure, not orgs, so all customers on an instance receive the release at the same time.
The benefit of keeping all customers on the same codeline is that we are able to invest more resources in building new features than maintaining variations of old code. We deliver more than 700 features a year in three automatic upgrades.
 The release timings are also planned well in advance and so changes to that schedule cannot be done.
