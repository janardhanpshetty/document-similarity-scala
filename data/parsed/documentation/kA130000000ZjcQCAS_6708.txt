### Topic: What’s the Difference Between Notes and the Old Note-Taking Tool? | Salesforce
What’s the Difference Between Notes and the Old Note-Taking Tool? | Salesforce
What’s the Difference Between Notes and the Old Note-Taking Tool?
Notes is an enhanced version of the original Salesforce note-taking tool. With Notes, you can get a number of features that help you take better notes faster and increase productivity.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Notes vs. the Old Note-Taking Tool
Feature
Notes
Old Note-Taking Tool
Add notes to records
Use the Notes related list to manage notes
Use the Notes & Attachments related list to manage notes
Notes save automatically
(Lightning Experience only)
Check spelling
(Lightning Experience only)
Add private notes to records
Create private, standalone notes (not related to records)
Relate notes to multiple records
(Lightning Experience and Salesforce1 only)
Use rich text formatting, including bulleted and numbered lists
Search Salesforce for just notes
Create tasks from notes
(Salesforce1 only)
Create a report on your notes
Add images to a note
(Lightning Experience only)
View or revert to an earlier version of a note
(Lightning Experience only)
Notes are automatically related to the parent account
See Also:
Guidelines for Using Notes in the Salesforce1 Mobile App
Create Notes and Add Them to Records
Notes
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=notes_differences.htm&language=en_US
Release
202.14
