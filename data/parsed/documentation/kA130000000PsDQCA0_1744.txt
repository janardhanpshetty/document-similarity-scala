### Topic: Get High Quality Data When You Need It Right In Salesforce | Salesforce
Get High Quality Data When You Need It Right In Salesforce | Salesforce
Get High Quality Data When You Need It Right In Salesforce
High quality data is key to business success. When data quality is high, it means your records are complete and up-to-date; it means you have the right connections at the companies that interest you; and it means you have the information you need to close deals and expand your business. In short, high quality data lets you understand, adapt, focus, and execute with surgical precision. Data.com offers a suite of products to give your business the high quality data it needs when it needs it. That way, you spend less time entering and updating data and more time growing your business.
Available in: Salesforce Classic and Lightning Experience
Data.com Prospector license available in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Data.com Clean license available in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Data.com Social Key available with a Data.com Clean license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
The free version of 
Data.com
 is available to all Salesforce organizations and includes the following functionality.
For organizations with Professional Edition and above, administrators can use Duplicate Management tools to control whether and when users can create duplicate records inside Salesforce. Administrators can customize the logic that’s used to identify duplicates and can create reports on the duplicates that they allow users to save.
All users can search across the complete Data.com database of accounts and contacts. However, free version users can’t see phone and email information for contacts, see account cards or D&B data on account records, add records to Salesforce, or clean records. To take full advantage of Data.com’s data, purchase Data.com licenses and set up the feature.
The Data.com product suite includes Data.com Prospector, Data.com Clean.
Note
For organizations with Performance Edition, users automatically get Data.com Corporate Prospector and Data.com Corporate Clean. For an additional cost, you can upgrade to Data.com Premium Prospector and Data.com Premium Clean.
Data.com Prospector
Use Data.com Prospector to search valuable company information from Dun & Bradstreet and millions of crowd-sourced contacts to find the records your business needs. Add these records to Salesforce as new accounts, contacts, and leads, or export them to use in other apps. It’s a great way to get the data you need to plan your sales territories, segment campaigns, find new accounts to engage, and expand your sales network.
Data.com Clean
Data.com Clean provides a number of ways to keep your Salesforce CRM records up to date by leveraging company information from D&B and millions of crowd-sourced contacts. There are two versions of the Data.com Clean product: Corporate Clean and Premium Clean. They differ based on the D&B fields that are available when you clean account or lead records.
Data.com Social Key
Data.com Social Key works with Social Accounts and Contacts and Data.com Clean to make it easier to learn more about your contacts and leads in Salesforce or on social network sites. Your organization must have Data.com Clean enabled to use Social Key.
See Also:
Data.com Clean
Prospect for Companies, Contacts, and Leads Right in Salesforce
How do I know which Data.com product my organization has?
What product should my organization use to clean our account, contact, and lead records?
Data.com Editions and Pricing
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=jigsaw_int_overview.htm&language=en_US
Release
202.14
