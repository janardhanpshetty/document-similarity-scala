### Topic: The Utilization Dashboard comprises most of the first tab of Cloud Pulse. This FAQ describes how the various elements of the Utilization Dashboard are computed.
 
How were the Utilization Segments determined?
We used statistical analysis, knowledge of usage patterns, and best judgement to determine the broad usage categories that customers can be grouped into. Segments vary according to the peer group of a customer.
How are Clouds defined?
WebPAR differentiates between the cloud types shown in the table below. There are two types of cloud definitions: one is based on the product(s) you purchased, the other is defined by your actual usage.
  
Cloud
Definition
Sales     
Customers who currently have provisioned Sales Cloud licenses.
Service
Customers who currently have provisioned Service Cloud licenses.
Data     
Customers with provisioned Jigsaw licenses (exclusively).
Collaboration   
Customers with provisioned Chatter licenses (exclusively).
Custom
Customers with provisioned Platform licenses.
Sales, Service & Custom  
Customers with provisioned Sales, Service and Platform licenses.
Sales, Service (Usage) & Custom
Customers with provisioned Sales Cloud and Platform Licenses and Service Cloud usage.
Sales & Custom
Customers with provisioned Sales cloud and Platform licenses.
Sales, Service & Custom (Usage)
Customers with provisioned Sales and Service licenses and Platform usage.
Sales & Service
Customers with provisioned Sales and Service licenses.
Sales (Usage), Service & Custom
Customers with provisioned Service and Platform licenses, and Sales usage.
Service & Custom
Customers with provisioned Service and Platform licenses.
Sales, Service (Usage) & Custom (Usage)
Customers with provisioned Sales cloud license, and Service and Platform usage.
Sales & Service (Usage)
Customers with provisioned Sales cloud license, and Service usage.
Sales & Custom (Usage)
Customers with provisioned Sales cloud license, and Platform usage.
Service (Usage) & Custom (Usage)
Customers with provisioned Sales cloud license, but only Service and Platform usage.
Service (Usage)
Customers with provisioned Sales cloud license, but only Service usage.
Custom (Usage)
Customers with provisioned Sales cloud license, but only Platform usage.
Sales (Usage), Service & Custom (Usage)
Customers with provisioned Service cloud license and Sales and Platform usage.
Sales (Usage) & Service
Customers with provisioned Service cloud license and Sales usage.
Service & Custom (Usage)
Customers with provisioned Service cloud license and Platform usage.
Sales (Usage), Service (Usage) & Custom
Customers with provisioned Platform license and Sales and Service usage.
Sales (Usage) & Custom
Customers with provisioned Platform license and Sales usage.
Service (Usage) & Custom
Customers with provisioned Platform license and Service usage.
Other   
Everything else.
How are Usage Clouds determined?
Sales Usage is determined based on the number of sales-centric objects in the account divided by the total number of users, e.g. Sales Usage = (Accounts + Leads + Oppties + Activities + Contacts)/non-portal-users. If the Sales Usage > 2000, we assign the account into Sales (Usage). Service usage is based on the number of cases created, e.g. Service Usage = number of cases/non-portal-users. If Service Usage > 50, then we assign the account into Service (Usage). Platform usage is a function of the number of Apex lines of code written. If Apex lines of code written > 25K, then the account has Custom (Usage).
What are the metrics used in the computation of Utilization Segments? How are the metrics defined?
  
Metric Name
Definition
TLP (Portal)
True Login Percentage (
Portal Licenses only
): Percentage of activated users who have logged in during the past 14 days. This metric tells us what percent of users use our application on a regular basis.
TLP (Non-Portal)
True Login Percentage (
Non-Portal Licenses only
): Percentage of activated users who have logged in during the past 14 days. This metric tells us what percent of users use our application on a regular basis.
Records/User (Custom objects)
Number of all custom record objects per user. Higher record volume indicates higher business value.
Records/User (Standard objects)
Number of standard record objects per user. Higher record volume indicates higher business value. Records of the following standard object types are counted:
Leads, Oppties, Contacts, Accounts, Forecasts, Events, Tasks, Cases, Notes, Solutions, Attachments and Documents.
CRUDs/User (Custom objects)
Number of custom objects created and edited per non-portal user in the last 14 days. These are browser-based edits only. This number excludes edits via API calls.
CRUDs/User (Standard objects)
Number of custom objects created and edited per non-portal user in the last 14 days. The following standard object types are counted:
Leads, Oppties, Contacts, Accounts, Forecasts, Events, Tasks, Cases, Notes, Solutions, Attachments and Documents. These are browser-based edits only. This number excludes edits via API calls.
Reports/User
Number of reports created per user. Presence of reports usually means that the customer’s leadership relies on SalesForce to gain visibility into the business processes.
Report Consumption/User
Number of distinct reports consumed per user in the last 14 days.
Chatter Adoption
Weighted average of 5 chatter usage metrics most indicative of adoption: # uploaded files per chatter user, percentage of chatter contributors logged in past 14 days, # active groups per chatter user in past 14 days, # comments/posts per chatter user in past 14 days, # guest groups per chatter user.
Success Plan
The type of support the customer has. The values are: standard, premier, and premier+. These values are translated into numeric (2,3, and 4, respectively) in the system for a more efficient segmentation.
Workflow Rules
Number of active Workflow Rules (not workflows). Presence of workflows usually means that the customer has automated some of its business processes.This metric does not include approvals.
Custom App / Salesforce to Salesforce (S2S)
Flag To indicate that Custom Apps have been installed 
or
 S2S is enabled
Platform, Partner or CPL
Flag to indicate that the customer has Platform 
or
 Partner Portal 
or
 Customer Portal Licenses
Custom Profiles
Number of custom profiles. A higher number of custom profiles is indicative of higher adoption.
License Utilization (Portal)
Number of licenses assigned to users divided by the number of licenses owned - For 
Portal Licenses
 only. This metric is useful for gauging how complete the rollout is.
License Utilization (Non-Portal)
Number of licenses assigned to users divided by the number of licenses owned - For 
Non-Portal Licenses
 only. This metric is useful for gauging how complete the rollout is.
Not all of these metrics apply to all peer groups (e.g. Portal related metrics only apply for customers utilizing portals).
How are the Utilization Segments for each metric computed?
For each metric listed above, we compare the value for the company to the Utilization Segment boundaries for the respective peer group and assign the Utilization Segment for the metric. 
 
Which licenses are included in "Portal" and "Non Portal" TLP and License Utilization?
All licenses that leverage a customer facing portal or website  are included in the “portal”category. Licenses that are accessed via the Salesforce.com website are considered “non-portal”. Chatter free licenses are excluded.
 
Why do the number of licenses we purchased and the number shown here differ?
The quantity of product purchased does not translate exactly to the number of licenses. Every product has a definition which specifies an “equivalent value”. The number of licenses is the quantity purchased multiplied by the equivalent value, specifying the number of users they can have. Each license has a different number of users associated with it.
Why does the Utilization Segment of my instance change during certain months? How is this handled?
There could be changes in the usage patterns of your instance due to seasonal fluctuations. The most common of these fluctuations stem from vacations and holidays. Unfortunately, there is not a trivial solution to this issue since different regions and especially countries have varying vacation and holiday times; e.g. Australia vs. Israel. These changes in utilization are reflected in the Utilization Segments, hence a customer’s segment may shift temporarily, but it generally recovers after these periods. 
The Utilization Segment is wrong and does not make sense.  What can I do?
It is unlikely but not impossible that we made a mistake when defining the Segment boundaries for the particular peer group of your instance, or while computing the overall Utilization Segment.  However, based on extensive internal experience with these metrics, the following are more likely causes for any apparent discrepancy:
Please check how your instance compares to the median (the grey bar) for each metric.  Is it below or above the median?  Keep in mind, there are situations where the median of a group is simply too low and all companies in that peer group need improvement.
Please note that the “Custom Apps/S2S” and “Platform, Partner or CPL” flags 
also 
contribute to the overall Utilization Segment.
Please consider the big picture for your instance: How many metrics are in each Utilization Segment?  And which of these are the more significant metrics (like 
TLP,
 
Records/User, or Non-Portal License Utilization
)?
How did you determine the boundaries for the Utilization Segments for each metric?
For each peer group, we analyzed the usage patterns of companies over the last year. Based on that, we determined values that are typical for companies and segmented them using statistical analysis. We also looked across peer groups (e.g. companies using the same edition,  companies using the same cloud) to make sure the boundaries are coherent. 
What about Jigsaw?
 Jigsaw is covered as the “Data Cloud”. See the cloud definitions above.
Are CRUDs/User being created from API calls which may skew the results?
No, we filter the data set to exclude API calls for this usage variable
Is the records/user field calculated based on CRM users, all users (partner, ideas, force.com, customer portal) or some other method?
It is computed based on the non-portal users for the instance.
I see that the peer group median and the value for my instance is higher than the maximum of the bar. How can this happen?
The peer group median has been computed using the values for the last 6 months. We will adjust the maximum for the metric as well as the boundaries on an as-needed basis. 
Resolution
