### Topic: Considerations for Using Duplicate Management | Salesforce
Considerations for Using Duplicate Management | Salesforce
Considerations for Using Duplicate Management
Data.com Duplicate Management includes limits for duplicate rules, matching rules, and duplicate record sets.
Considerations for Duplicate Rules
Duplicate rules are available for accounts, contacts, leads, and custom objects. All other objects, including Opportunities and Person Accounts, are not currently supported.
Duplicate rules don’t run when:
Records are created using Quick Create.
Leads are converted to accounts or contacts 
and 
your organization doesn’t have the “Use Apex Lead Convert” permission.
Records are restored with the 
Undelete
 button.
Records are added using Lightning Sync.
Records are manually merged.
A Self-Service user creates records and the rules include conditions based on the User object.
Duplicate rule conditions are set for lookup relationship fields and records with no value for these fields are saved. For example, you have a condition that specifies a duplicate rule only runs when 
Campaign
 DOES NOT CONTAIN ‘Salesforce’
. Then, if you add a record with 
no value
 for the 
Campaign
 field, the duplicate rule doesn’t run.
Sometimes, if duplicate rules are set for an alert to show when duplicates are found, users are 
always
 blocked from saving records and 
do not 
see a list of duplicates. This situation happens when:
Records are added using the data import tools.
A person account is converted to a business account (and the newly created business account matches existing business accounts).
Records are added or edited using Salesforce APIs.
If you’re saving multiple records at the same time and your duplicate rules are set to 
Block
 or 
Alert
, records within the same save aren’t compared to each other; they are only compared with records already in Salesforce. This behavior doesn't affect the 
Report
 action, and duplicate record sets include records that match other records in the same save.
Custom picklists are not supported when they’re included in a matching rule that’s used in a cross-object duplicate rule.
The customizable alert text in duplicate rules isn’t supported by the Translation Workbench.
Up to 5 active duplicate rules are allowed per object.
Up to three matching rules are allowed per duplicate rule, and each matching rule must be of a different object.
Starting in Spring ‘15, all new Salesforce orgs come with Duplicate Management features turned on for accounts, contacts, and leads. New orgs come with standard account, contact, and lead duplicate rules. Each duplicate rule is associated with a matching rule. You can deactivate these rules or create custom rules.
Considerations for Matching Rules
Matching rules are available for accounts, contacts, leads, and custom objects. All other objects, including Opportunities and Person Accounts, are not currently supported.
Standard and custom matching rules that use fuzzy matching methods only support Latin characters, and, if you’re using international data, we recommend using the Exact matching method with your matching rules.
If a field on an object is no longer available to your organization, it could cause matching rules with mappings to this field to be ignored and duplicate detection to be affected. Check all duplicate rule field mappings for an object if there is a change to the fields available to your organization. For example, the 
Clean Status
 field is only available to customers with a Data.com license. If your organization no longer has a Data.com license, this field is no longer available and matching rules with mappings to this field are ignored.
Only 1 lookup relationship field is allowed per matching rule.
Up to 5 active matching rules are allowed per object.
Up to 25 total active matching rules are allowed.
Up to 100 total matching rules are allowed (both active and inactive).
Up to 5 matching rules can be activated or deactivated at a time.
Matching rules that include fields with Platform Encryption do not detect duplicates. If your organization has Platform Encryption enabled, make sure that your matching rules do not include encrypted fields.
Considerations for Duplicate Record Sets
By default, duplicate record sets are visible to only administrators, but the administrator can grant visibility to other users.
If a lead is identified as a duplicate but converted before the duplicate record set is created, the converted lead isn’t included in a duplicate set.
See Also:
Duplicate Rules
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=duplicate_management_considerations.htm&language=en_US
Release
202.14
