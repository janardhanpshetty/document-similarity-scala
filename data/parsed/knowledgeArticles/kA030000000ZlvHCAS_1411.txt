### Topic: Salesforce.com training materials, study guides, and exercise guides are the property of Salesforce.com and are not available for soft copy distribution
All the materials produced by Salesforce University, including but not limited to PowerPoint decks, exercise guides, and videos, are the sole property of 
salesforce.com
. The materials cannot be used, reproduced, or distributed without the express permission of 
salesforce.com
. 
 
When attending an instructor-led course, students will receive a paper copy of the materials, Electronic copies of materials are rarely, if ever, distributed.
Resolution
