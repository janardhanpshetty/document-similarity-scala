### Topic: This article explains how to calculate duration a picklist value stays in a record.
To calculate the duration of how long a field stays in a particular Picklist Value, please follow the details mentioned below: 
Example 
Picklist Value - Opportunity Stage, Case Status, Lead Source values.
Resolution
To achieve this requirement we need 2 fields and 2 workflows for 1 picklist value.
1. Create 2 field : New_TimeStamp(Date/Time Field) and New_Total Days(Number Field)
2. Create the 1st Workflow to update the New_TimeStamp field. 
 Set the Evaluation Criteria as "Evaluate the rule when a record is created, and any time it’s edited  to subsequently meet criteria".
 Rule Criteria - Opportunity : Status equals Negotiation.
 In Workflow Action Choose Field Update (Field to update : New_TimeStamp)
 Update the field using the formula: 
NOW()
 Activate the Workflow
Through the above workflow, everytime the Opportunity Status changes to Negotiation, it would update with the current Date/Time value.
3. Create the 2nd Workflow to update the New_Total Days field. 
 Set the Evaluation Criteria as "Evaluate the rule when a record is created, and every time it’s edited".
 Rule Criteria : 
AND ( ISCHANGED (Status), ISPICKVAL( PRIORVALUE (Status), 'Negotiation'))
 In Workflow Action Choose Field Update (Field to update : New_Total Days)
 Update the field using the formula: 
IF( ISBLANK ( PRIORVALUE( New_Total Days )),0,PRIORVALUE(New_Total  Days))+  (NOW() - New_TimeStamp )
 Activate the Workflow.
Through the above workflow, it calculates the overall duration of how long Opportunity Stage was under Negotiation.
Say, the Opportunity Stage was under 'Negotiation' for 1 day and then moved onto 'Need Analysis' Stage and was again changed back to 'Negotiation' and stayed for 1 more Day; so our Workflow adds the previous 1 day duration and the current 1 Day giving an Overall Duration of 2 Days.
NOTE 
: 
The above Workflows can be used for any picklist values. T
his field can also be used for reporting purpose in Chart and Dashboards.
Example use cases : A support organization can be interested in finding out how long a Case Status is in Working or Escalated Status at record level.
