### Topic: How to export your owned or purchased contacts from Data.com Connect
How do I export my owned contacts from my account in Data.com Connect? 
Resolution
When you purchase, add, or update a contact it’s added to your "
My Contacts
" list. Connect provides two ways to see your contacts:
All Contacts
 gives you quick access to all your contacts in one list
Royalty Contacts
 shows only your current royalty contacts.
You can export your contacts from All Contacts and Royalty Contacts directly to a file, or add them to your shopping cart, to capture their latest business information.
You might want to use your shopping cart instead of exporting directly to a file if, for example, you’re creating a list of contacts you searched for and plan to buy, and want to add contacts you own to that list. We don’t charge you when you export contacts from your contact lists — you already own them.
Exporting Contacts Directly to a File
Go to 
My Account
 | 
My Contacts
 | select 
All Contacts
 or 
Royalty Contacts
.
Select one or more contacts from the list and click 
Export Contacts
.
Click 
Export Selected
 or 
Export All
. Name your export file in the dialog and export the contacts.
You see the file in 
My Account
 | 
Exports
 when the export completes.
Exporting Contacts from Your Shopping Cart
Go to 
My Account
 | 
My Contacts
 | select 
All Contacts
 or 
Royalty Contacts
.
Select one or more contacts from the list and click 
Export Contacts
.
Click 
Add Selected to Cart
 or 
Add All to Cart
.
When you’re ready to complete the export, open your cart. You can export some or all the contacts from the list of contacts in your cart, or quickly export all the contacts in your cart from the Cart Summary.
To export from the cart list, select one or more contacts and click 
Check Out
. Choose 
Check Out Selected
 or 
Check Out All
. Name your export file and click 
Get Contacts
.
To export from the Cart Summary, review the summary information then click 
Check Out Now
.
Tips
If you are a Connect Plus premium plan member you can upload a suppression list to automatically remove suppressed contacts from your shopping cart. Once your suppression file has been uploaded, navigate to your shopping cart and select the 
Remove Suppressed Contacts
 button, which filters them out of your cart, giving you room to add more contacts to export.
If you receive an error saying "
You cannot export more than 50,000 contacts at one time, Please change your selection
." Consider breaking your export into smaller chunks and try again.
For more information please check out our 
Data.com Connect Help
 center 
 
