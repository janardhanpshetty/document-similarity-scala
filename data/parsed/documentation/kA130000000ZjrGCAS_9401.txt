### Topic: Which Microsoft® Email Integration Product is Right for My Company? | Salesforce
Which Microsoft® Email Integration Product is Right for My Company? | Salesforce
Which Microsoft® Email Integration Product is Right for My Company?
Compare Lightning for Outlook with our legacy products, Salesforce for Outlook and Email to Salesforce. Reviewing each product’s system requirements can help you determine which product—or combination of products—is the best fit for your company.
This feature available to manage from: both Salesforce Classic and Lightning Experience
Lightning for Outlook is available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Salesforce for Outlook is available in: 
All
 Editions
Email to Salesforce is available in: 
All
 Editions
Whether you’re preparing to migrate from a retired product like Connect for Outlook, or considering Salesforce email integration for the first time, you’ll help your sales professionals streamline the work they do between their Microsoft-based email and Salesforce when you set up Lightning for Outlook, Salesforce for Outlook, or Email to Salesforce.
Each email integration feature is unique, and compatible with different Microsoft products. You can introduce just one of these features to your users—or a combination—depending on which best fits your company’s needs.
Sync Between Email Applications and Salesforce
Give your users the confidence that their records are up-to-date—whether users are working in their email application or in Salesforce. Depending on the combination of email integration products you choose, you can help users keep contacts, events, and tasks in sync between their Microsoft email and Salesforce. To set up your users to sync items, consider either Lightning Sync—the sync component for Lightning for Outlook—or 
Salesforce for Outlook
 sync.
Experience Salesforce Directly from Email Applications
Set up your users to view and create Salesforce records, or add emails to Salesforce, without ever leaving their email applications. To set up your users to work with Salesforce records directly from their email, consider the Lightning for Outlook, the Salesforce for Outlook side panel, or Email to Salesforce.
Compare Microsoft Email Integration Products
Determine which features are best for you and your teams. First, compare the system requirements to learn which features are compatible with your company’s computing environment. Then, take a look at our feature overviews to learn what each feature does.
If you’re working with
This product lets you experience Salesforce directly from your email application
This product lets you add emails to Salesforce records
This product lets you sync contacts and events
This product lets you sync tasks
Exchange Online (Office 365)
and
Outlook 2016
Outlook 2013
Lightning for Outlook
or
Salesforce for Outlook Side Panel
Lightning for Outlook
or
Salesforce for Outlook Side Panel
Lightning Sync
or
Salesforce for Outlook sync
Salesforce for Outlook sync
Exchange 2013 (on-premises)
and
Outlook 2016
Outlook 2013
Lightning for Outlook
or
Salesforce for Outlook Side Panel
Salesforce for Outlook Side Panel
Lightning Sync
or
Salesforce for Outlook sync
Salesforce for Outlook sync
Exchange 2010
and
Outlook 2016
Outlook 2013
Outlook 2010
Salesforce for Outlook Side Panel
Salesforce for Outlook Side Panel
Lightning Sync
or
Salesforce for Outlook sync
Salesforce for Outlook sync
Exchange 2007
and
Outlook 2013
Outlook 2010
Outlook 2007
Salesforce for Outlook Side Panel
Salesforce for Outlook Side Panel
Salesforce for Outlook sync
Salesforce for Outlook sync
Mac OS X
and
Exchange Online (Office 365)
and
Outlook Web App (OWA)
Lightning for Outlook
Email to Salesforce
Lightning Sync
Mac OS X
and
Exchange 2013
Exchange 2010
Email to Salesforce
Lightning Sync
Mobile
Email to Salesforce
Lightning Sync
See Also:
Considerations for Running Lightning for Outlook and Salesforce for Outlook Simultaneously
Lightning Sync
Lightning for Outlook
View Salesforce Records in Microsoft® Outlook®
How Does Email to Salesforce Work?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sfo_vs_email_connect.htm&language=en_US
Release
202.14
