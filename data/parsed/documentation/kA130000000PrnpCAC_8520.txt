### Topic: Uninstalling a Package | Salesforce
Uninstalling a Package | Salesforce
Uninstalling a Package
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To uninstall packages:
“Download AppExchange Packages”
You can remove any installed package, including all of its components and all data in the package. Additionally, any custom fields, links, or anything else you added to the custom app after installation are also removed.
To remove a package:
From Setup, enter 
Installed
 in the 
Quick Find
 box, then select 
Installed Packages
.
Click 
Uninstall
 next to the package that you want to remove.
Select 
Yes, I want to uninstall...
 and click 
Uninstall
.
After an uninstall, Salesforce automatically creates an export file containing the package data, as well as any associated notes and attachments. When the uninstall is complete, Salesforce sends an email containing a link to the export file to the user performing the uninstall. The export file and related notes and attachments are listed below the list of installed packages. We recommend storing the file elsewhere because it’s only available for a limited period of time after the uninstall completes. 
Tip
If you reinstall the package later and want to reimport the package data, see 
Importing Package Data
.
Notes on Uninstalling Packages
If you’re uninstalling a package that includes a custom object, all components on that custom object are also deleted. This includes custom fields, validation rules, s-controls, custom buttons and links, as well as workflow rules and approval processes.
You can’t uninstall a package whenever any component in the package is referenced by a component that will not get included in the uninstall. For example:
When an installed package includes any component on a standard object that another component references, Salesforce prevents you from uninstalling the package. This means that you can install a package that includes a custom user field and build a workflow rule that gets triggered when the value of that field is a specific value. Uninstalling the package would prevent your workflow from working.
When you have installed two unrelated packages that each include a custom object and one custom object component references a component in the other, Salesforce prevents you from uninstalling the package. This means that you can install an expense report app that includes a custom user field and create a validation rule on another installed custom object that references that custom user field. However, uninstalling the expense report app prevents the validation rule from working.
When an installed folder contains components you added after installation, Salesforce prevents you from uninstalling the package.
When an installed letterhead is used for an email template you added after installation, Salesforce prevents you from uninstalling the package.
You can’t uninstall a package if a field added by the package is being updated by a background job, such as an update to a roll-up summary field. Wait until the background job finishes, and try again.
Uninstall export files contain custom app data for your package, excluding some components, such as documents and formula field values.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=distribution_uninstalling_packages.htm&language=en_US
Release
202.14
