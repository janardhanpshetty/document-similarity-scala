### Topic: Why can't my Community User manage external users after being set up as a Delegated External User Administrator?
Community User Profiles can be set up as Delegated External User Administrators, and they can manage other Community members whose profiles are selected under the Delegated External User Administration Related List on their Profile.
However, these users will only have the option to manage users in these profiles if they are under the same Account as the delegated admin.
Resolution
The Delegated External User Administrator must be under the same Account as the users they would like to manage.
As noted above, the permission "Delegated External User Administrators" needs to be given to a profile.
Having set this permission, you need to set which profiles can be given by Delegated admins to their users, this is also done on the Profile. But this is not possible with the "Enable Enhanced Profile User Interface" enabled. You first need to disable this, then you will be able to see the Related List, and add the profiles to be managed.
