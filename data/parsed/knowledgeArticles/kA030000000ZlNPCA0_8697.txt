### Topic: This article helps us to use frontdoor.jsp to login to Salesforce and Communities
Learn how to use frontdoor.jsp to give users access to Salesforce from a custom Web interface, such as remote access Force.com site or other API integration, using their existing session ID and the server URL.
Resolution
Use frontdoor.jsp to give users access to Salesforce
 from a custom web interface, such as a remote access Force.com site or other API integration, using their existing session ID and the server URL. For communities, use a web-scoped access token. Here's an example of what the URL would look like. 
https://instance.salesforce.com/secur/frontdoor.jsp?sid=session_ID&retURL=optional_relative_url_to_open
 
Retrieve the session ID
 
 
1. The access_token from an OAuth authentication. Note that one of the scopes specified when you create a connected app must be web or full.
2. The LoginResult returned from a SOAP API login() call.
3. The Apex UserInfo.getSessionId().
The session ID returned using the Visualforce {!GETSESSIONID()} can’t be used on frontdoor.jsp.
Not all session types are supported with frontdoor.jsp, such as community API sessions (security risk). For these sessions, consider using SAML for single sign-on instead.
 
