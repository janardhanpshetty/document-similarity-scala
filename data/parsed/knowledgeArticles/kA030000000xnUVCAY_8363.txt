### Topic: For installation on Govt Cloud (NA21 and CS32), packages from the Appexchange must have the Is Apex Certified box checked on and the development (DE) org for that package must not be inactive or deleted.
In addition to the packaging considerations and issues that may come up when installing managed and unmanaged packages from the Salesforce Appexchange on any Salesforce org, there are additional considerations when installing packages on Govt Cloud orgs (those hosted on NA21 and CS32).
NOTE: Any managed or unmanaged package developed on an organization inside Gov Cloud may be installed on other organizations that are also inside Gov Cloud
Resolution
Requirements & Limitations
These package installation requirements apply only to Govt Cloud orgs:
1. If the package was developed oustide of Govt Cloud, the development organization for the package must be active.
If the development org is deleted or locked, package installation will fail in the packaging UI with stacktrace error id (30313541).
Please see: 
W-2350648
Workaround
: None at this time.
2. If the package was developed outside of Govt Cloud, the package must also have a True value for the Is Apex Certified checkbox field on its packaging metadata record (04t record).
If the package is not marked as Apex Certified, then the package installation will fail with this error using the original installer:
Invalid package. The package cannot be installed in your organization.
With the new Aura installer, it will likely fail with 
stacktrace error id (-1457151713)
.
Please also see: 
W-2431668
Workaround
: If the package is an internal Salesforce app or a Salesforce Labs app, you can reach out to Partner Ops for assistance with getting the Apex Certified box checked (See 
W-2453273
).
For third-party packages, the developer needs to submit the app for our Appexchange security review and upon passage, to have the Apex Certified box checked by Partner Ops.
Known Issues
1. The new Aura package installation URL is not currently Govt Cloud aware.
Packages using the new Aura installer, that are also not affected by #2 above, will fail with stactrace error id (-761958515).
Please see: 
W-2436054
 and W-2451316
Workaround:
 Construct an installation URL using the original packaging functionality as described in 
Article 000199219
.
