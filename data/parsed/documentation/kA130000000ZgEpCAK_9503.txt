### Topic: What Salesforce Fields Are Affected by Data.com Clean? | Salesforce
What Salesforce Fields Are Affected by Data.com Clean? | Salesforce
What 
Salesforce Fields Are Affected by Data.com Clean?
Available in: Salesforce Classic
Available with a Data.com Prospector license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available with a Data.com Clean license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Note
These fields may be different if your organization uses customized Data.com-to-Salesforce field mappings.
If you can’t edit one or more fields on an account, contact, or lead record, you may receive a message that says, 
You have limited access to data, so you can not completely clean this record
 or 
You have limited access to account fields, so you can not update all the fields shown here
. If this happens, ask your administrator for help.
When you use Data.com Clean to clean a Salesforce record, either manually or with automated jobs, the record’s fields are updated with Data.com data. Here’s a list of the fields that are updated.
Account Fields
Account Name
Industry
Account Site
NAICS Code
Annual Revenue
NAICS Description
Billing Address
Ownership
Clean Status
Phone
D&B Company
 (Premium Prospector and Clean only)
SIC Code
Data.com Key
SIC Description
D-U-N-S Number
Ticker Symbol
Description
Tradestyle
Employees
Website
Fax
Year Started
Contact Fields
Clean Status
Mailing Address
Data.com Key
Phone
Email
Title
Name
Lead Fields
Address
Email
Annual Revenue
Industry
Clean Status
Name
Company
No. of Employees
Company D-U-N-S Number
Phone
D&B Company
 (Premium Prospector and Clean only)
Title
Data.com Key
See Also:
Why did I receive a message that says, You have limited access to data, so you can’t completely clean this record?
Clean Individual Salesforce Records
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_default_fields_affected_by_clean.htm&language=en_US
Release
202.14
