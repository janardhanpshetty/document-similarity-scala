### Topic: This article is the fifth of five major steps required to get Government Splunk access.
Step 6 of 6
This article is the fifth of five major steps required for Government Splunk access.
See Also: 
Overview of Splunk Access in the Government Cloud
 
Resolution
Once all of the above is done, you are ready to access Splunk using one of these URLs in the browser of your choice (Opera or Firefox):
https://shared-logsearch1-2-wax.ops.sfdc.net
https://shared-logsearch2-1-wax.ops.sfdc.net
https://shared-logsearch1-1-chx.ops.sfdc.net
https://shared-logsearch1-2-chx.ops.sfdc.net
https://shared-logsearch2-1-chx.ops.sfdc.net
Your Splunk login is the same as your 
kerberos 
login and password.
