### Topic: Guidelines with templates to deal with Support follow ups when Site Reliability blocks / throttles a customer.
Audience:
 Tier 2
Scope:
 Contacting customer on Org Block/Throttle
 
When 
Tier 3 creates an Outbound case
 for an org block or throttle, 
Tier 2 contacts the customer
.  The process usually is straightforward but if the customer contact is unresponsive, it can mean Tier 2 needs to reach out to include further resources.
To assist with Tier 2 reaching out to the customer, below are sample communication templates to drive efficient communication that includes the appropriate information.
These templates are to be guides and do need to be modified for the situation as needed.
 
Resolution
 
 
Email Template Samples
Please make sure you replace all the information included between brackets [ ] with the relevant information from the Case / Contact / Account as necessary.
  
Initial Contact:
 
"Dear {!Contact.FirstName},
 
My name is {!User.Name} and I'm a {!User.Title}, which is part of the Technical Support Team at Salesforce.
I'm reaching out to you as you're listed as [PLEASE CHANGE THIS AS REQUIRED: Designated Contact / Primary Contact / Manager...] for {!Account.Name}.
 
Our Site Reliability Team has observed unusual activity coming from your Organization and a [THROTTLE / BLOCK] rule has been put in place to prevent additional issues.
 
You may have observed some issues relating to specific functionalities or activities as a result of us placing this rule on your Org, therefore we will need your help to identify it and correct the issue.
 
The details we have at the moment are:
Organization ID:
Instance:
Sandbox Name: [IF IT'S A SANDBOX. IF NOT, REMOVE THE LINE]
Action: [THROTTLE / BLOCK]
URI:
Symptoms:
 
If you are the correct person to discuss this with then I'd appreciate if you can provide me with a phone number and the best time to call you and I'll reach out to you directly. If you are not the correct person then I'd be grateful if you can point me to the right person who can work with us on this.
 
Thank you in advance,"
 
Follow up for different GEOs:
“Dear {!Contact.FirstName},
 
My name is {!User.Name} and I’ve taken ownership of the Outbound Case #{!Case.CaseNumber} during [GEO] hours.
Please note that the [THROTTLE / BLOCK] mentioned on the previous email is still in place and we would like to talk to you about it if you’re available.
 
Just as a reminder, here are the current details:
Organization ID:
Instance:
Sandbox Name: [IF IT'S A SANDBOX. IF NOT, REMOVE THE LINE]
Action: [THROTTLE / BLOCK]
URI:
Symptoms:
 
Let me know if you’re available for a call or if you have any information that can be related to this incident. If we should be contacting someone else within your Organization in relation to this issue then it would be great if you can point me to the correct resource.
 
Thank you in advance,”
 
@ mention to Account Teams:
Hello [@Account-Team-mention-here], we’re having difficulties finding the right Contact for this Account and we need their help to resolve a [THROTTLE / BLOCK] that has been in place since [START-DATE]. We reached out to [LIST-THE-CONTACTS] via [EMAIL/PHONE] but we didn’t manage to get an answer from [HIM/HER/THEM]. Do you have any suggestion as to who should be contacted for this Account that could help us with this issue?
Just to give you some context: Site Reliability had to put a [THROTTLE / BLOCK] on this customer’s [PRODUCTION / SANDBOX] due to their [SUMMARY-OF-THE-CUSTOMER’S-ACTIVITY]. Thank you in advance!
