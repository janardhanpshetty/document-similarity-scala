### Topic: There are several considerations and behaviors to bear in mind when enabling Notes and using the Files functionality with both Lightning Experience and Salesforce Classic users. This article provides additional details concerning the Notes and Files functionality.
With Winter '16 the 
enhanced note-taking tool
 (Notes) was made generally available.
The new Notes tool is primarily designed as a productivity feature to help users do more within Lightning Experience. In order to accommodate this, the legacy Notes & Attachments feature was split into two distinct Notes and Files features.
Before enabling Notes review 
What’s the Difference Between Notes and the Old Note-Taking Tool?
Resolution
It is recommended to plan user training and education with the following caveats in mind before rolling out:
 
1. Administrators need to 
Configure Page Layouts for Notes
 while continuing to maintain the Notes & Attachments related list on page layouts.
 
This will allow users to leverage the Notes & Attachments related list to both view historical notes created prior to enabling the enhanced note-taking tool and continue adding attachments to records. Enabling Notes removes the ability to create a note from the Notes & Attachments related list and users will need to leverage the new Notes related list.
 
2. Prior to Spring '16, if both Salesforce Classic and Lightning Experience users exist in your org, it was not recommended to 
Add the Files related list to page layouts
. However, please review the note below regarding enhancements introduced with the Spring '16 release.
Note:
 With Spring '16, the Files related list now has feature parity between Salesforce Classic and Lightning Experience. The "Upload Files" button which was previously only available on the Files related list in Lightning Experience is now also available for Salesforce Classic users as well. So with Spring '16, instead of instructing users to leverage the Notes & Attachments related list to upload Attachments, it is now recommended that administrators add the Files related list to page layouts and instruct both Lighting Experience and Salesforce Classic users alike to exclusively use the Files related list to add new attachments to records moving forward.
To help facilitate the change over to Files from Notes & Attachments a new preference, "Files uploaded to the Attachments related list on records are uploaded as Salesforce Files, not as attachments" has been introduced to ensure that Attachments added via the legacy Notes & Attachments related list are now uploaded as Files moving forward. It is recommended that administrators for orgs created prior Spring '16 enable this feature and for more details see the 
Upload Files to Notes & Attachments List in Salesforce Classic
 section of the Spring '16 release notes.
 
Prior to Spring '16,  attachments and files added to records in Lightning Experience via "Upload a File" button in the Notes & Attachments related list were visible in both the Files and Notes & Attachments related lists. However, if the "Upload a File" button is used from the Files related list in Lightning it will not show up in the Notes & Attachments related list and therefore be unavailable for Salesforce Classic users (unless the Files related list has also been added to their page layout).
Salesforce Classic Limitations with Notes and Files:
- Prior to Spring '16 the "Upload Files" button was not available on the Files related list and Salesforce Classic users still needed to attach files via the Notes & Attachments related list. However, as of Spring '16 the "Upload Files" button is now available on the Files related list in both Lightning Experience and Salesforce Classic.
- HTML editor or formatting tools are not available when using the New Note quick action from a record's Chatter feed in Salesforce Classic.
- It is only possible to relate a note to one record in Salesforce Classic and to relate a Note to a record the user must:
A) Initiate the note creation from within the record's Chatter feed via the "New Note" chatter quick action in the chatter feed publisher. See 
Setup Notes
 for more details.
Note:
 Formatting tools are not available via the "New Note" quick action and users will need to access the note record via the related list or Files tab after it's created to enter desired formatting.
B) Click "New Note" from the desired record's Notes related list to associate it to that record.
Note:
 Clicking New Note from the related list will always create a new note record.  See 
Notes record gets created after clicking on Cancel button
 for more information.
See Also:
Winter '16: New Note button is missing on the Notes & Attachments related list
Create Notes and Add Them to Records in Salesforce Classic
Create Notes and Add Them to Records in Salesforce Lightning Experience​
Importing Notes to the ContentNote object using the Apex Data Loader
Lightning Experience: Attachments are being replaced by Files and not returned in search
