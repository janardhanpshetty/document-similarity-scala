### Topic: Following this article you can get rid of the error while executing fillContactInfo trigger on the cases
Getting error while executing fillContactInfo trigger on Cases
Resolution
Because of the SOQL query which is non-selective.
 
Generally speaking, you should try the criteria used in your SOQL query is selective. Selective criteria:
o Are filtered on an indexed field (typically a field with Unique or External ID permission, o fields that contain foreign keys like lookup or master-detail fields).
o Are not used in conjunction with OR filters (unless both sides of the OR are indexed and the total records returned is below the threshold)
o When considered individually return less than 10% of the records for the first million records, and 5% for records over a million with a cap at 333K records as the max threshold. For instance, given the SOQL query
SELECT id FROM account WHERE field1__c = 'abc' and field2__c = '123'
for the criteria to be selective
SELECT count() FROM account WHERE field1__c = 'abc'
and
SELECT count() FROM account WHERE field2__c = '123'
should return less records than the aforementioned threshold.
 
It may be necessary to custom index certain fields to make the query selective if the criteria cannot be modified to narrow down the number of records returned. For information on custom indexing, please check the following:
- 
Article #6007
- R&D site on Custom Indexing (mainly for Tier 3): 
https://sites.google.com/a/salesforce.com/performance/Home/
custom
-indexing-in-sfdc?AuthEventSource=SSO
