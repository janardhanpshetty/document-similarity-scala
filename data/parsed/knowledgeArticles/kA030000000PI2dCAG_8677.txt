### Topic: Steps to locate the Radian6 Engagement Console Debug Log files within both the Engagement Console and "My Computer".
The debug logs record the API calls that are made for the actions that you take while you work in the Engagement Console. These logs are helpful to provide to Marketing Cloud Support for troubleshooting if you encounter any issues while using the Engagement Console.
Resolution
Locate the debug logs in the Engagement Console
1. Log in to the Engagement Console
2. Go to 
File | Preferences.
3. Click 
Engagement Console.
4. Locate the debug logs listed under "Debug Logs".
There is a list of Debug Logs available that are time stamped. The most recent debug log is listed first. 
5. Choose the log for the time period that may have captured the error.
Please also note the approximate time the issue occurred and provide that information when you contact the Marketing Cloud Support team. 
6. Open the correct time stamped link. 
7. Right click on the page.
8. Click 
Save As 
or 
Save Page As
 depending on your browser.
9. Select where you would like to save the file on your computer.
 
Locate the Engagement Console debug logs from the "My Computer" File
 
Windows XP
Windows 7
Mac OSX 10.6 & Earlier (Leopard, Snow Leopard)
Mac OSX 10.7 & Later (Lion, Mountain Lion)
Windows XP
 
1. Click 
Start.
 
2. Enter the following in the search field:
"C:\Documents and Settings\<username>**\Applicationdata\"
3. In the "Roaming" folder that opens, open the folder called "com.radian6.<
string of numbers
>"
4. Open the "Logs" folder to locate the Engagement Console debug logs.
Note:
The entire com.radian6 folder is safe to delete as a troubleshooting step as it will be recreated the next time the Engagement Console is restarted. Please note: This clears all of your local data including your preferences, stack configuration, personal accounts, and private (non-shared) macros.  Your local data will be reset the next time you log in.
 
Windows 7
1. Click 
Start
.
2. Enter the following in the search field: 
 \Users\<
username
>\appdata\roaming\
Note: This username refers to your personal username that you use to log in to your computer. If you are unsure of your username, please contact your IT Administrator to confirm your username.
2. Press 
Enter
 to open the "Roaming" folder.
3. Inside the "Roaming" folder, open the folder 
com.radian6.<
string of numbers
>
.
4. Open the 
com.radian6
 folder.
5. Open the 
Local Store
 folder.
6. Open the 
Logs 
folder to retrieve the Engagement Console debug logs.
 
Note: 
The entire com.radian6 folder is safe to delete as a troubleshooting step as it will be recreated the next time the Engagement Console is restarted. Please note: This clears all of your local data including your preferences, stack configuration, personal accounts, and private (non-shared) macros. Your local data will be reset the next time you log in. 
If this folder is not visible to you, it may be hidden. Contact your IT Administrator to assist in showing your hidden folders.
 
MAC OSX 10.6 and earlier
 
1. From the desktop, either press
 Command+Shift+G 
all at once  or go to 
Finder | Go | Go to Folder.
 
2. Paste 
"Users/<
username>
/Library/Preferences/
" in the "Go to Folder" search window. 
Replace <username> with the username you use to login to your computer.
3. Press 
Enter
.
This opens the "Preferences" folder.
3. Open the 
com.radian6 
folder.
4. Within this folder, open the folder called 
Logs
 to retrieve the Engagement Console logs.
 
Note:
The entire com.radian6 folder is safe to delete as a troubleshooting step as it will be recreated the next time the Engagement Console is restarted. Please note: This clears all of your local data including your preferences, stack configuration, personal accounts, and private (non-shared) macros.  Your local data will be reset the next time you log in.
 
MAC OSX 10.7 and later
OSX 10.7 (Lion) defaults to hiding the users' Library director. To temporarily display the folder to locate your debug logs, follow these steps:
1. From the desktop, either press 
Command+Shift+G
 all at once or go to  
Finder | Go | Go to Folder
.
The “go to folder” search window will pop up. 
2. In the "Go to Folder" search window, enter  
~/Library/Preferences/.
3. Press 
Enter
 to temporarily access the Library.
This temporarily opens the "Preferences" folder to locate the "com.radian6" folder.
4. Within the com.radian6 folder, open the folder called 
Logs
 to retrieve the Engagement Console debug logs.
When you are done, close this window. The library folder is no longer visible again.
 
