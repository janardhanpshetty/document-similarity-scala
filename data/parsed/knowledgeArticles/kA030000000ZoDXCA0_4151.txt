### Topic: Change Sharing Settings with a huge amount of Data in production or sandbox org. How long will this take?
Changed the Organization-Wide Defaults Sharing Settings from Public to Private in Production and Sandbox Org. Does Salesforce have an estimate or a job tracker about the time it could take for making this change? 
Resolution
There is no "estimated time" for changes to occur in your org with OWD.
If you are currently in Administrative lock (meaning your administrator is locked out the org) you will receive an email upon completion of the change. 
