### Topic: This article describes how Data.com APIs provide access to the most up-to-date Data.com records
The 
Data.com
 APIs provide access to the most up-to-date 
Data.com
 records. You can search, match, and purchase company and contact records.
Available in: 
Developer
 Edition, 
Professional
 Edition (add-on), 
Enterprise
 Edition, and 
Unlimited
 Edition.
 
queryMore()
 is Supported by All Datacloud Objects
When no 
LIMIT
 is specified, queryMore() returns the entire response in 25-record chunks. You can scroll through the full set of results 25 records at a time.
A 
LIMIT
 that’s set from 1 and 100 returns the actual number of records or the number of records equal to the 
LIMIT
 value, whichever is fewer. This query returns only the first 75 records from a response that contains more than 1,000 records. A next page is not returned.
SELECT City,State,Street,CompanyId
FROM 
DatacloudCompany

WHERE Name like 'Salesforce'
LIMIT 75
Specify a 
LIMIT
 greater than or equal to the number of records in the response to scroll through large responses in 100-record chunks. For example, in a response with 1,900 records and a 
LIMIT
 that’s set to 2,000, you can scroll through the complete response in chunks of 100 records. You can only scroll through as many records as are specified in the 
LIMIT
. If your 
LIMIT
 is less than the number of records in the response, 
queryMore()
 processes only the number of records that’s specified in the 
LIMIT
 value.
Filter on Name and LocationStatus in the DatacloudDandBCompany Object
You can now filter on 
LocationStatus
 and 
Name
 in the DatacloudDandBCompany object.
Field
=
!=
<
<=
>
>=
LIKE
IN
NOT IN
ORDER BY
LocationStatus
√
 
 
 
 
 
 
√
 
 
Name
√
 
 
 
 
 
√
√
 
 
Resolution
