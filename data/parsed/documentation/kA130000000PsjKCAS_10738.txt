### Topic: Relationship Group Considerations | Salesforce
Relationship Group Considerations | Salesforce
Relationship Group Considerations
Available in: Salesforce Classic
Available in: Salesforce for Wealth Management
To make the most of relationship groups and relationship group members, review the notable behaviors and limitations below.
Relationship Group Considerations
Relationship groups:
Support roll-up related lists for activities and custom objects only.
Are completely separate from the Partners related list on accounts.
Do not have directly associated activities, notes, or attachments.
Only support required custom fields that are configured with a default value. Such custom fields will display on relationship group detail and edit pages, but not in the relationship groups wizard.
Only support validation rules on custom fields that were added after the Relationship Group managed package was installed.
Cannot be renamed to a different name than “relationship group,” because they are a custom object in a managed package.
Can be created and edited through the relationship group wizard regardless of field-level security settings on the 
Description
 field.
Relationship Group Member Considerations
Relationship group members:
Are limited to a maximum of 20 per relationship group.
Cannot be imported via the import wizard for custom objects.
Do not support tagging.
Have the division of the parent relationship group.
Are owned by both the parent relationship group and the parent account.
Do not support validation rules.
Do not support universally required fields.
Cannot be renamed to a different name than “relationship group member,” because they are a custom object in a managed package.
Can have the fields 
Primary
, 
Secondary
, and 
Include in Roll-Up
 edited through the relationship group wizard even when the field-level security on those fields is “read-only.”
Cannot be included in workflow rules or approval processes.
Cannot be used in Salesforce to Salesforce.
Are not supported in Connect Offline.
Are only supported in API version 11.0 and later. In the API, using the 
upsert
 call on relationship group members is not supported.
See Also:
Relationship Groups Overview
Relationship Group Members
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=relgroups_consider.htm&language=en_US
Release
202.14
