### Topic: Using CSS Reset | Salesforce
Using CSS Reset | Salesforce
Using CSS Reset
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To build, edit, and manage Site.com sites:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
Every browser has set presentation defaults, but unfortunately they aren't standardized across all browser types. This means that when you use CSS to style your site, it may not render as expected when you view it in different browsers. For example, browsers differ in how they display:
Unordered and ordered lists
Top and bottom margins for headings
Indentation distances
Default line-heights
A CSS reset cancels the differences between browsers to control how browser elements are presented to the end user. You can either use Site.com's CSS reset, or you can add your own CSS reset code.
To use Site.com's CSS reset:
In the Style Sheets view on the Overview tab, open the style sheet by double-clicking it, or hovering over it and clicking 
| 
Edit
.
Click 
| 
Insert CSS Reset
.
Ensure the CSS reset is positioned at the top of the style sheet. To move it, drag it to the correct location in the pane on the left.
To add your own CSS reset code:
In the Style Sheets view on the Overview tab, open the style sheet by double-clicking it, or hovering over it and clicking 
| 
Edit
.
Click 
Edit Style Sheet Code
 to open the CSS editor.
Paste the code at the top of the style sheet code.
Click 
Save and Close
.
See Also:
Creating and Using CSS Style Sheets
Cascading Style Sheets Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_style_css_reset.htm&language=en_US
Release
202.14
