### Topic: Resolving an error presented when attempting to remove the ExactTarget solution in Microsoft Dynamics CRM 2011.
I am attempting to remove ExactTarget in Microsoft Dynamics CRM 2011 by:
1. Going to 
Settings | Solutions.
2. Selecting 
ExactTarget
.
3. Clicking 
Delete
.
However, I am getting the message "An unexpected error has occurred." After downloading the details, the full error is displayed similar to the following:
 
Unhandled Exception: System.ServiceModel.FaultException`1[[Microsoft.Xrm.Sdk.OrganizationServiceFault, Microsoft.Xrm.Sdk, Version=5.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35]]: An unexpected error occurred.Detail: 
<OrganizationServiceFault xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.microsoft.com/xrm/2011/Contracts"> 
<ErrorCode>-2147220970</ErrorCode> 
<ErrorDetails xmlns:d2p1="http://schemas.datacontract.org/2004/07/System.Collections.Generic" /> 
<Message>An unexpected error occurred.</Message> 
<Timestamp>2012-06-20T08:34:19.9138191Z</Timestamp> 
<InnerFault i:nil="true" /> 
<TraceText i:nil="true" /> 
</OrganizationServiceFault>
 
Resolution
You're receiving this message because the operations required to uninstall are taking longer than the default timeout settings will allow.
To successfully uninstall the ExactTarget solution, change the registry timeout values, so they're double the default settings. 
After those changes are made, re-attempt deleting the solution. Once the solution is successfully removed, revert the settings in the registry back to their default values.
 
Default timeouts
  
Registry
Default timeout value
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\MSCRM\OLEDBTimeout 
30 (seconds) 
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\MSCRM\ExtendedTimeout 
1000000 (milliseconds) 
 
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\MSCRM\NormalTimeout 
300000 (milliseconds) 
