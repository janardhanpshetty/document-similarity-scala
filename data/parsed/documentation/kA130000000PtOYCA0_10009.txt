### Topic: Set Up Lead Management | Salesforce
Set Up Lead Management | Salesforce
Set Up Lead Management
Help your sales teams track prospects and build a strong pipeline of opportunities. Your sales teams use Salesforce leads to work and qualify their prospects with the goal of creating opportunities.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Using leads can give your sales staff instant access to the latest prospects and ensures that no leads are ever dropped. Successful lead management helps sales and marketing manage the inbound lead process, track lead sources, and analyze return on their marketing investment.
From Setup, get started using lead management.
Go to the object management settings for leads. From the fields section, create custom lead fields that track information specific to your company. Also, map your custom lead fields to account, contact, and opportunity fields so that the data gets converted when users convert leads. Edit the 
Lead Status
 picklist to choose the default status for new and converted leads.
Enter 
Lead Settings
 in the 
Quick Find
 box, then select 
Lead Settings
 to specify your default lead settings.
Enter 
Assignment Rules
 in the 
Quick Find
 box, then select 
Lead Assignment Rules
 to set up lead assignment rules that automatically assign leads.
Enter 
Web-to-Lead
 in the 
Quick Find
 box, then select 
Web-to-Lead
 to automatically capture leads from your website.
To let your sales teams edit converted leads, enter 
User Interface
 in the 
Quick Find
 box, then select 
User Interface
. Then, select 
Enable "Set Audit Fields upon Record Creation" and "Update Records with Inactive Owners" User Permissions
.
To create sales queues for leads or custom objects, from Setup, enter 
Queues
 in the 
Quick Find
 box, then select 
Queues
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_leadmgmt.htm&language=en_US
Release
202.14
