### Topic: Create Content Rules to Moderate Your Community | Salesforce
Create Content Rules to Moderate Your Community | Salesforce
Create Content Rules to Moderate Your Community
Create and modify rules for your community to moderate member-generated content. Content rules protect your community from offensive language and inappropriate content created by spammers or malicious members. You can create content rules that block, replace, flag, or allow you to review and approve member-generated content.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view, create, edit, and delete rules:
“Manage Communities” OR “Create and Set Up Communities”
AND
Is a member of the community whose Community Management page they’re trying to access.
If your community is using the Napili template, moderation rules apply to questions and group posts created by your community members.
You can also use the Metadata API or Tooling API to set up content rules.
Some things to keep in mind:
Your org can have up to 30 rules. This limit is per org, not per community. This limit includes both content rules and rate rules.
Each content rule can have up to three keyword criteria and 10 member criteria.
Rules that block content run first, followed by rules to review and approve content, then rules that replace content, and last by rules that flag content.
 
If two or more rules perform the same action, the oldest rule runs first, based on the date the rule was created. Rules to replace content don’t run when the content also applies to a review rule—we want community managers to review the original content.
Tip
Before creating a rule, we recommend you create criteria to use in the rule.
Open Community Management
.
Click 
Moderation
 | 
Rules
, then click 
New
 and select 
Content Rule
.
Complete the following fields:
Name
—Enter a name for your rule.
Unique Name
—Enter a unique name for your rule. The unique name used by the API.
Description
—Optionally, enter a description.
Activate Rule
—If selected, the rule is activated.
Applies To
—Specify which types of member-generated content this rule applies to. Posts and comments only apply to content created in groups and user profiles. All feed types, such as polls and links, are supported.
Moderation Action
—Specify what you want to happen when the criteria is matched.
Block
 prevents the content from being published.
Review
 allows users with the “Can Approve Feed Post” permission to approve the content before it’s published.
Note
The Summer ’16 release contains a beta version of the pre-moderation feature, which means it’s a high-quality feature with 
known limitations
. This feature isn’t generally available unless or until Salesforce announces its general availability in documentation or in press releases or public statements. We can’t guarantee general availability within any particular time frame or at all. Make your purchase decisions only on the basis of generally available products and features. You can provide feedback and suggestions for this feature in the 
Community Implementation
 group in the Success Community.
Replace
 publishes the content with the keywords replaced as asterisks. For example, 
BadWord
 becomes 
*******
.
Flag
 publishes the content and then automatically flags the content as inappropriate.
Message for Member
—Specify the message that your member sees when their content is blocked. 
If you don’t specify a message, the member sees the standard message: “You can’t use 
%BLOCKED_KEYWORD%
 or other inappropriate words in this community. Review your content and try again.”
 The 
%BLOCKED_KEYWORD%
 variable displays up to five blocked words. You can also use this variable in your own custom message.
Tip
For international communities, you can translate this message. From Setup, enter 
Translate
 in the Quick Find box, then select 
Translate
. To provide a translation for the message, select the Moderation Rule setup component and expand the community the rule belongs to.
Member Criteria
—Specify member criteria to enforce this rule. Ask yourself, who should this rule apply to?
Content Criteria
—Specify the content criteria to enforce this rule. Ask yourself, what specific keywords should this rule apply to?
Important
Keep the following things in mind:
If you activate a content rule without specifying member criteria, the rule applies to all members.
If you activate a content rule without specifying content criteria, the rule either:
Prevents members from creating posts and comments entirely
Sends all posts for review
Flags all posts and comments
Yikes! Be careful.
If you select member criteria and content criteria, the rule applies only when both criteria are met.
Specifying criteria is optional. However, we recommend that you include criteria in your rules to target specific members and content.
Click 
Save
 to apply your changes.
See Also:
Translate Terms
Create Content Criteria to Moderate Your Community
Create Member Criteria to Moderate Your Community
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_moderator_manage_rules_content.htm&language=en_US
Release
202.14
