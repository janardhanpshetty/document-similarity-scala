### Topic: Custom Link Example: Link to Reports | Salesforce
Custom Link Example: Link to Reports | Salesforce
Custom Link Example: Link to Reports
Use custom links to run reports with filtered results from a Salesforce record detail page. For example, let’s say you frequently run a mailing list report for the contacts related to an account. You can create a custom link for accounts that links directly to a report that is automatically filtered to the account you are viewing. In this case, your custom link must pass the account’s unique record ID to the report.
Available in: Salesforce Classic and Lightning Experience
Custom buttons and links are available in: 
All
 Editions
Visualforce pages and s-controls are available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create or change custom buttons or links:
“Customize Application”
Copy the ID for the type of record by which you want to filter your report (in this example, an account). To do so, view the record and copy the 15-character ID from the last part of the URL. For example, 
https://na1.salesforce.com/
001200030012j3J
.
From the Reports tab, create the report you want by either customizing a standard report or creating a custom report.
Filter the report by the record ID you copied. For example, “Account ID equals 001200030012j3J”.
Run the report to verify that it contains the data you expect.
Click 
Customize
.
Click 
Save
 or 
Save As
 to save the report to a public folder accessible by the appropriate users. 
Save
 doesn’t create a custom report, whereas 
Save As
 does.
Run the report and copy the report’s URL from the browser.
Begin creating your custom link. Set the 
Content Source
 field to URL. In the large formula text area, paste the report URL you copied. Remember to omit the domain portion 
https://na1.salesforce.com
.
Add the custom link to the appropriate page layouts.
Verify that the new custom link works correctly.
Tip
When creating a report for use in a custom link, set date ranges and report options generically so that report results include data that can be useful for multiple users. For example, if you set a date range using the “Created Date” of a record, set the Start Date far enough in the past to not exclude any relevant records and leave the End Date blank. If you scope the report to just “My” records, the report might not include all records that a user can see. Try setting the report options to “All visible” records.
See Also:
Constructing Effective Custom Links
Custom Link Example: Link to Documents
Custom Link Example: Link to Files in Chatter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=custom_links_example_linking_to_reports.htm&language=en_US
Release
202.14
