### Topic: Guidelines for Selecting Data.com Clean Job Preferences | Salesforce
Guidelines for Selecting Data.com Clean Job Preferences | Salesforce
Guidelines for Selecting Data.com Clean Job Preferences
Before you define your Data.com Clean jobs, it’s important to understand the preferences you can set.
Available in: Salesforce Classic
Available with a Data.com Clean license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Data.com Clean offers three options for cleaning your data with jobs. Jobs can:
Flag all differences per record
 to 
only identify
 fields whose values are different from Data.com values
Flag differences and auto-fill blank fields
 to identify fields whose values are different 
and fill blank fields
 with values from Data.com.
Customize settings field by field
.
If you select the 
Customize
 option for any object, you can:
Flag fields on your Salesforce records that have different values from matched Data.com records.
Flag different field values on your Salesforce records and automatically fill blank fields with Data.com values.
Overwrite different field values on your Salesforce records with Data.com values. If you overwrite Salesforce record values, you should set history tracking for those fields.
You can set flag-only and flag and auto-fill preferences for entire records or by field. You set overwrite preferences by field.
If you use Data.com Premium Clean, you can also clean D&B Company records that are linked to accounts and leads. D&B Company clean jobs automatically fill or overwrite field values on D&B company records, but do not change data on linked account or lead records.
Your preferences take effect when the next scheduled clean job runs, and users will see the flags, as appropriate, when they clean records manually.
If, after changing matching services, you want to do a one-time rematch using the new matching service, you can manually remove the 
D-U-N-S Number
 on account records to force the matching service to rematch the records. Be aware that this might affect your match rates.
We don’t recommend mapping a custom field or skipping mapping for either matching service.
Converted leads are excluded from Lead clean jobs.
If you select a field to be overwritten by Clean jobs, and a user marks that field as 
wrong
 on a record, jobs will not overwrite that field on that record.
Person accounts are excluded from Account and Contact clean jobs.
See Also:
Set Up Data.com Clean
Set Up Data.com Clean Jobs
Define Your Preferences and Select an Account Matching Service for Data.com Clean
Guidelines for Selecting an Account Matching Service
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_understanding_clean_job_prefs.htm&language=en_US
Release
202.14
