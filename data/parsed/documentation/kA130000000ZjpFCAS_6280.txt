### Topic: Guidelines for Working with Large Flows | Salesforce
Guidelines for Working with Large Flows | Salesforce
Guidelines for Working with Large Flows
Business processes can be complex. When your flow is too large for the canvas, control the zoom, search in the Explorer tab, or collapse the left side panel.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Zoom
To zoom in and out of your flow, use the + and - buttons on the right side of the canvas.
Search in the Explorer tab
Looking for a specific element or resource? Search for it in the Explorer tab.
To find an element with a specific name, type in the search box.
To find all instances of a certain element or resource, click the magnifying glass and select the type.
Once you find the right resource in the Explorer tab, see which elements are using the resource. In the Description pane, click the Usage tab.
Once you find the right element in the Explorer, find that element in your canvas. Hover over the element, and click the magnifying glass. 
The element is highlighted in green in your canvas. 
If the element wasn’t in view, the Cloud Flow Designer automatically scrolls to show the element.
Collapse the left side panel
To hide the Palette, Resources, and Explorer tabs from your view, click the left arrow next to the side panel. That way, you get even more space in the canvas. 
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_considerations_largeflows.htm&language=en_US
Release
202.14
