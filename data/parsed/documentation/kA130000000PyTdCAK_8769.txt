### Topic: Give Thanks to Your Coworkers | Salesforce
Give Thanks to Your Coworkers | Salesforce
Give Thanks to Your Coworkers
Recognize your coworkers with badges and post your thanks directly to their Chatter feed.
Available in: Salesforce Classic
Available in: 
All Editions
 that have Chatter enabled.
Note
The Chatter Free license has limited functionality with Thanks features.
Rewards features require a Work.com license. This includes giving, creating, or receiving badges tied to rewards.
Thank someone if they’ve done a great job or to recognize an achievement.
Walk Through It: Give Thanks in Chatter
From the Chatter publisher, click 
Thanks
.
If 
Thanks
 isn’t displayed, click 
More
 and select 
Thanks
 from the publisher’s drop-down menu.
Type the name of the person you want to thank.
Currently, you can only thank one person at a time, but you can mention other people in your post’s message.
Select 
Change Badge
 to select a different badge for your post or keep the default badge.
You can choose from twelve pre-defined badges. Select the badge image to see more information about the badge.
Type a message for the person you’re thanking.
Select your audience.
My Followers
 to post to all your followers
A Group
 to post to a specific group. Search for the group and select the group from the list. The person you’re thanking is @mentioned at the beginning of the post’s message. When you post to a private group, only the group’s members can see your post to the group. However, the badge will also appear publicly on the Recognition tab of the recipient’s profile.
Click 
Share
.
Your Thanks post appears in your feed and the feed of the person you’re thanking. If you’re posting to a public group or a record page, the post appears on the group or record feed and their profile. If you’re posting to a private group, only members of the private group can see your post to the group. However, the badge will also appear publicly on the Recognition tab of the recipient’s profile.
See Also:
Allow Users to Thank Their Coworkers
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=chatter_thanks.htm&language=en_US
Release
202.14
