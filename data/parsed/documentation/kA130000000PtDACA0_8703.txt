### Topic: Quotes | Salesforce
Quotes | Salesforce
Quotes
Quotes in Salesforce represent the proposed prices of your company’s products and services. You create a quote from an opportunity and its products. Each opportunity can have multiple associated quotes, and any one of them can be synced with the opportunity. When a quote and an opportunity are synced, any change to line items in the quote syncs with products on the opportunity, and vice versa.
Available in: Salesforce Classic
Available in: 
Performance
 and 
Developer
 Editions
Available in: 
Professional
, 
Enterprise
, and 
Unlimited
 Editions with the Sales Cloud
Create and Manage Quotes
Create quotes showing your customers the prices of the products and services that you offer. You can create a set of quotes to show different combinations of products, discounts, and quantities so customers can compare prices. Then manage your quotes to keep them up-to-date.
Sync Quotes and Opportunities
Link a quote to the opportunity that it was created from. That way, updates to one record are always reflected in the other.
Create and Email Quote PDFs
Provide quotes to your customers the easy way: via PDF. Create quote PDFs from a standard template or from your company’s design. Then email your quote directly from the preview page for customer approval.
Considerations for Deleting Quotes
Before you delete a quote, it’s a good idea to understand how deleting affects quote-related PDFs and syncing with opportunities.
See Also:
Opportunities
Quotes Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=quotes_overview.htm&language=en_US
Release
202.14
