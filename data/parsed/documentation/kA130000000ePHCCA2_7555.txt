### Topic: Create Wave Analytics Permission Sets | Salesforce
Create Wave Analytics Permission Sets | Salesforce
Create Wave Analytics Permission Sets
To give users in your organization access to Wave features, create and assign one or more permission sets based on the level of analytics capabilities they need.
User Permissions Needed
To create permission sets:
“Manage Profiles and Permission Sets”
After assigning permission set licenses to users, the next step in the setup process is to create permission sets made up of Wave user permissions and assign them to users. You can create any permission sets you need to meet the needs of your users. We show you how to create two permission sets:
Manage Wave Analytics
 permission set, for users who administer and manage Wave Analytics and need access to all Wave Analytics features.
View Wave Analytics
 permission set, for users who explore data and occasionally upload new data to Wave Analytics and need access to a limited set of Wave Analytics features.
Note
Customers who purchased Wave before October 20, 2015: See 
Set up the Wave AnalyticsPlatform With Licenses Purchased Before October 20, 2015
 before following the instructions here.
Walk Through It: create, edit, and assign a permission set
Note
The Manage Wave Analytics permission set enables the equivalent functionality provided by the previous Wave Analytics Builder license (purchased before October 20, 2015). If you have a Builder license or you’re migrating to the new Wave Analytics platform license, follow these steps to create a permission set with the same functionality.
Create Manage Wave Analytics permission set, which gives users access to all Wave features.
In the Setup menu, under administer, click 
Manage Users | Permission Sets
 and then click 
New
.
Enter 
Manage Wave Analytics
 in the Label field. This automatically creates the API name, as well, which you can change if you’d like. Note that the API name can’t include spaces, end with an underscore, or have two consecutive underscores.
Warning
Follow this step precisely or some users in your organization may not be able to access Wave.
In the User License field, keep the default value 
--None--
. Do 
not
 select a user license.
Click 
Save
. The permission set details page shows the new permission set. Now add user permissions to the set.
Scroll to the bottom of the permission set details page, and click 
System Permissions
. Then click 
Edit
.
Select “Manage Wave Analytics” user permissions and click 
Save
. You’ve successfully created a permission set that enables a user to access all Wave features. You can now assign it to users, which is covered in Assign Permission Sets to Users. You should assign this permission set sparingly because it provides access to all Wave features, many of which are inappropriate for most analytics users.
Note
The View Wave Analytics permission set enables the equivalent functionality provided by the previous Wave Analytics Explorer license (purchased before October 20, 2015). If you have an Explorer license or you’re migrating to the new Wave Analytics platform license, follow these steps to create a permission set with the same functionality.
Create View Wave Analytics permission set, which enables users to view Wave lenses, dashboards, and datasets and import data to Wave.
In the Setup menu, under administer, click 
Manage Users | Permission Sets
 and then click 
New
.
Enter 
View Wave Analytics
 in the Label field. This automatically creates the API name, as well, which you can change if you’d like. Note that the API name can’t include spaces, end with an underscore, or have two consecutive underscores.
Warning
Follow this step precisely or some users in your organization may not be able to access Wave.
In the User License field, keep the default value 
--None--
. Do 
not
 select a user license.
Click 
Save
. The permission set details page shows the new permission set. Now add user permissions to the set.
Scroll to the bottom of the permission set details page, and click 
System Permissions
. Then click 
Edit
.
Select “Use Wave Analytics Cloud” and “Upload External Data to Wave Analytics” user permissions and click 
Save
. You’ve successfully created a permission set that enables a user to view the Wave datasets, lenses, and dashboards that they have permission to view upload data files from outside Salesforce. You can now assign it to users, which is covered in Assign Permission Sets to Users. 
Parent topic:
 
Wave Platform Setup
Previous topic:
 
Enable Wave Analytics and Assign Analytics Cloud - Wave Analytics Platform Permission Set Licenses
Next topic:
 
Assign Wave Analytics Permission Sets to Users
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_setup_create_permsets.htm&language=en_US
Release
202.14
