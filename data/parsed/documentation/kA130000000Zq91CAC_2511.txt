### Topic: Sales Wave Prebuilt Dashboards and Datasets | Salesforce
Sales Wave Prebuilt Dashboards and Datasets | Salesforce
Sales Wave Prebuilt Dashboards and Datasets
The Sales Wave app includes prebuilt dashboards and datasets to accelerate your Sales Wave data exploration.
User Permissions Needed
To use Wave apps:
“Use Wave Analytics Templated Apps”
To use Sales Wave:
“Access Sales Cloud Analytics Templates and Apps”
To create and manage Wave apps:
“Manage Wave Analytics Templated Apps”
“Edit Wave Analytics Dataflows”
Note
The dashboards and datasets included in your instance of Sales Wave may differ, depending on how you answer configuration wizard questions when you create the app.
Sales Wave Dashboards
The prebuilt dashboards in the Sales Wave app contain best practices that help you get value from your Salesforce data—fast. The dashboards let you manage pipeline and forecast and understand key business performance drivers, visualize trends, assign actions. They also help you get fast answers to questions you have about business results.
The following table guides you through Sales Wave dashboards, which give you an immediate picture of how your business is doing. You can also explore further on your own at any point. To learn more about exploring data in Wave, see 
Explore and Visualize Your Data
.
Note
Sales Wave uses the Salesforce role hierarchy to determine the security level of the data users can access. Users can see only data at their role level and all the roles below that level. The role hierarchy is also used to determine the manager roll-up structure on some Sales Wave dashboards. For example, if you want to drill down on your hierarchy to see how other teams and individuals are performing, use the “Owner Role View as” picklist.
Table 1. Sales Wave Dashboards
Dashboard Name
Contents
Effort Needed to Customize*
Has Manager Rollup?**
Sales Wave–Overview
Start here. Gives you a quick pulse of your business across forecast and pipe management, team performance, historical closed won business, and activity management.
High
No
Sales Wave–Pipeline Trending
Tracks opportunity changes in your pipeline such as Closed Won/Lost, Moved In/Out, Amount Reduced/Expanded.
High
No
Sales Wave–Forecast Review
Displays Closed Won Business and Expected to Close to let you know if you’re going to hit your quota.
High
Yes
Sales Wave–Pipe Review
Shows all open deals in the pipeline and lets you organize by multiple key dimensions so you can quickly identify the top open deals. You can also perform actions such as Create Task or Event in Salesforce.
Low
No
Sales Wave–Productivity
Monitors tasks and events completed by each person on the sales team.
High
Yes
Sales Wave–Business Review
Shows historical Closed Won business across key dimensions, such as by owner, customer, geography, product, and source. Shown in above image.
Low
No
Sales Wave–By Customer
Shows visualizations of trends within your customer base.
Low
No
Sales Wave–By Geo
Shows visualizations of trends by geographical area.
Low
No
Sales Wave–By Source
Shows visualizations of trends by source.
Low
No
Sales Wave–By Product
Shows visualizations of trends by product.
Low
No
Sales Wave–Leaderboard
Shows top- and bottom-ranked players by time periods and across key performance categories.
High
Yes
Sales Wave–Player Stats
Provides details for every person on your team.
High
Yes
Sales Wave - Pipe Activity
Benchmarks open vs. won deals to help you determine best practices to accelerate deals. Also helps you stay on top of top open deals by monitoring activity.
High
No
Sales Wave - Account Summary
Summarizes a selected account with average win rate, average sale cycle, open and won totals by geography, source, and/or product. Can be embedded in the Salesforce Account page layout.
Medium
No
Sales Wave - Personalized App Setup
Shows the answers you selected to the configuration wizard questions when you created the app.
Customization not recommended.
Not applicable
* Indicates the level of difficulty to edit the dashboard. If it’s marked High, we recommend you work with a partner or Salesforce service representative. The complexity of the queries used to create the dashboard requires expertise in the Wave platform..
** Indicates if the dashboard lets you drill down by team structure. Currently, Sales Wave supports only role hierarchy.
Sales Wave Datasets
The table lists all the standard datasets that can become part of Sales Wave when you create the app.
Table 2. Sales Wave Datasets
Dataset Name
Contents
Opportunities
Information about Opportunity, Accounts, and Users.
Opportunity Products
Product info for Opportunity, Accounts, and Users.
Tasks
Task information such as calls, emails, or tasks.
Events
Event information such as meetings.
Quota
Quota information; you need to 
upload CSV file
 with Quota data to update this dataset before using Sales Wave.
Opportunity History
Tracks changes on the Close Date, Stage, and Amount for an Opportunity.
Pipeline Trending
Creates the change type (Closed Won / Lost, Moved In / Out, Reduced / Expand) for the pipeline waterfall chart.
User Role
Basic information about Users.
User Manager
Role hierarchy information for Users used during manager roll-up.
Quota User Manager
Applies role hierarchy information to the Quota data used during manager roll-up.
Parent topic:
 
The Sales Wave Analytics App
Previous topic:
 
About the Sales Wave App
Next topic:
 
Set Up Salesforce Permissions for the Sales Wave Analytics App
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_app_sales_wave_dashboards.htm&language=en_US
Release
202.14
