### Topic: Salesforce transmits packets of various sizes when communicating with your machine. If you have done successful basic traceroutes and pings to Salesforce as highlighted in other solutions please follow these instructions to run some more extensive packet transmission tests. After typing each command let the process run to return you to the command prompt before proceeding with the next.
How do I run an extensive ping test with Salesforce's servers?
Resolution
Salesforce transmits packets of various sizes when communicating with your machine. If you have done successful basic traceroutes and pings to salesforce.com as highlighted in other solutions please follow these instructions to run some more extensive packet transmission tests. After typing each command let the process run to return you to the command prompt before proceeding with the next.
1. Click Start | Run and type cmd.
2. Type "ping -f -n 25 -l 1200 na1.salesforce.com >>C:\sfdcping.txt"
3. Type "ping -f -n 25 -l 1300 na1.salesforce.com >>C:\sfdcping.txt"
4. Type "ping -f -n 25 -l 1400 na1.salesforce.com >>C:\sfdcping.txt"
5. Type "ping -f -n 25 -l 1200 ssl.salesforce.com >> C:\sfdcping.txt"
6. Type "ping -f -n 25 -l 1300 ssl.salesforce.com >>C:\sfdcping.txt"
7. Type "ping -f -n 25 -l 1400 ssl.salesforce.com >>C:\sfdcping.txt"
In all steps, that is a "dash L" before the 1200, 1300, and 1400.
Navigate to your C:\ drive and look for the sfdcping.txt file that is
created during this process. For further analysis, submit a case with Salesforce Support and attach the file.
