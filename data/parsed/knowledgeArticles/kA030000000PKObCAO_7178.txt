### Topic: The approval process links are not taking the Custom domain. Workaround : Allow users to access their Org without requiring My Domain. Setup | Company Profile | My Domain | Login Policy
Customers with My Domain enabled may see some system emails generating links that do not contain the unique My Domain URL specific to the Org. Rather, the user may see the instance such as NA9 or CS7..etc, within the URL.
Resolution
To Reproduce:
 
In an Org with My Domain enabled: 
 
1) Run through steps to generate an approval process email 
 
RESULT: 
 
User will receive an email containing a link similar to the below: 
 
https://cs7.salesforce.com/p/process/ProcessInstanceWorkitemWizardStageManager?id=04iM0000000MC9E 
 
EXPECTED: 
 
https://MyDomainName.cs7.my.salesforce.com/p/process/ProcessInstanceWorkitemWizardStageManager?id=04iM0000000MC9E
 
Workaround: 
 
Allow users to access their Org without requiring My Domain. 
 
Setup | Company Profile | My Domain | Login Policy
