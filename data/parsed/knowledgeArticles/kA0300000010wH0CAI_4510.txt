Printable View
 | 
Help for this Page
Show Feed
 
Hide Feed
 
Follow
Following
Rate This Article
|vote=None|
Processing...
(Average Rating: No Rating)
Version 6
Language
English
Show Properties
 
Hide Properties
 
Show Feed
 
Hide Feed
 
Follow
Following
Rate This Article
|vote=None|
Processing...
(Average Rating: No Rating)
Version 6
Version
Published on
Language
English
Show Properties
 
Hide Properties
 
First Published
10/13/2015 3:06 PM
Last Modified
6/23/2016 5:38 PM
Last Published
6/23/2016 5:38 PM
Article Audience
Release
All
Product Features
Notes
Channels
Internal App
, 
Customer
, 
Public Knowledge Base
Article Number
000230535
### Topic: This article gives the information how to show the details for description field on notes when you click on printable view option.
Summary
This article gives the information how to show the details for description field on notes when you click on printable view option.
Validation Status
Validated External
Post
 
Question
 
New Note
 
More
 
File
Thanks
Video
Link
Poll
New Group
New Travel Approval
S1 Feedback
Coaching
Write something...
 
salesforce.com Only 
salesforce.com Only
All with access
Looking for answers...
No similar questions found.
Change Badge
Badge Title
What This Badge Means
Badges Left To Give
Go Back 
 
 Use This Badge 
Sharing with customers
Search this feed
 
 
|
Show
All Updates
 
Show
All Updates
Internal Only
Fewer Updates
Sort By
Latest Posts
Most Recent Activity
Success!
Sumit Dey
 
published a new version of this Knowledge Base.
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Bookmark
Add Topics
June 23, 2016 at 5:38 PM
  
Attach File
 
Click to comment
 
 
Tulsi Raju
 
published a new version of this Knowledge Base.
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Bookmark
Add Topics
June 1, 2016 at 8:32 AM
  
Attach File
 
Click to comment
 
 
Bernard Spencer
 
published a new version of this Knowledge Base.
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Bookmark
Add Topics
May 4, 2016 at 10:21 AM
  
Attach File
 
Click to comment
 
 
Sherry Lee Gondra
 to salesforce.com Only
#KBFeedback
 
Typographical error: “save”
3. Give the details on Title and Body field >
s ave
 it.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
October 28, 2015 at 2:39 PM
  
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Configuration
(Notes & Attachments)
 not showing on 
"Printable View
".
Resolution
Please find the steps to make description field visible on printable view:
Click on object tab > open any record.
On Notes and Attachment related list section > Click on new note button.
Give the details on Title and Body field >save it.
Click on printable view and check on related list > you will be able to view the information given within the description field.
Note:
If you have
 Enabled the Notes settings
, then you will be able to view the description field on printable view however you will not see any content within the field. If you wish to view the information mentioned within the description field, you may disable the notes settings. Below are the steps to disable the 
Notes Settings
:
Click on Setup
Click on Customize > Notes > Settings > uncheck the enabled notes box > save it.
