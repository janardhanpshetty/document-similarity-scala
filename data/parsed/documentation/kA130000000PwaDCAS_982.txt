### Topic: Use a New Version of an Entitlement Process | Salesforce
Use a New Version of an Entitlement Process | Salesforce
Use a New Version of an Entitlement Process
After you create a new version of an entitlement process, you can choose to apply it to all entitlements assigned to the previous version, or only to new entitlements. When you apply an entitlement process to an entitlement, it also applies the process to that entitlement’s active support records.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions with the Service Cloud
User Permissions Needed
To create and update entitlement processes:
“Manage Entitlements”
Watch a Demo
 
(2:20 minutes)
Note
To create multiple versions of entitlement processes, entitlement versioning must be enabled in your org. Select 
Enable Entitlement Versioning
 on the Entitlement Settings page in Setup.
When you create versions of entitlement processes with the same name, the version number and notes help you differentiate between versions. Salesforce prevents you from disabling entitlement versioning so you always know which version you’re working with.
Applying an Entitlement Process to a New Entitlement
Scenario: 
You’re creating a new entitlement and want to apply a particular version of an entitlement process to it.
Choose the entitlement process you want in the Entitlement Process lookup field on the entitlement.
Tip
After you click the lookup icon on the Entitlement Process field, select “All Versions” in the lookup dialog box. Otherwise, you can only choose from the default versions of existing entitlement processes.
Applying an Entitlement Process to an Existing Entitlement
Scenario: 
You made a new version of an entitlement process, and you want to switch all the entitlements that were using the previous version over to your new version.
From Setup, enter 
Entitlement Processes
 in the 
Quick Find
 box, then select 
Entitlement Processes
.
Click the name of the entitlement process you want to work with.
The list on the main Entitlement Processes page shows the default version of each process. Click the name of a process to see a list of all available versions of it.
On the detail page for the entitlement process, click the name of the new version that you want to apply to existing entitlements (and by default, to cases or work orders linked to those entitlements).
Click 
New Update Rule
.
Choose the version of the entitlement process you want to update from.
You can update from any other version of the process, whether or not it’s active.
Depending on the differences between the old and new versions of the entitlement process, updating an entitlement to the new version can trigger milestone warning and violation actions on that entitlement’s support records (such as cases or work orders). To avoid such warnings and violation actions, select 
Don’t Trigger New Milestone Warnings and Violations
. We recommend selecting this so you don’t trigger violation warnings on old entitlements and support records.
Click 
Save
.
The update rule detail page shows the estimated number of entitlements and support records that will be updated to use the new process.
Click 
Start
 to begin the update process.
Usually the update process completes within an hour, but it depends on the number of entitlements and records being updated. Throughout the update process, the update rule detail page refreshes periodically to show the number of entitlements and records processed. To stop the update at any time, click 
Stop
.
See Also:
Set Up an Entitlement Process
Updating an Entitlement Process
Create a New Version of an Entitlement Process
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=entitlements_process_using_new_versions.htm&language=en_US
Release
202.14
