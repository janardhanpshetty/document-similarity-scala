### Topic: Submit a Record for Approval | Salesforce
Submit a Record for Approval | Salesforce
Submit a Record for Approval
Depending on your org’s customizations, you can submit a record for approval directly from that record.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To submit a record for approval:
“Read” on the record
Go to the record that you want to submit for approval. 
Make sure it’s ready to be submitted. 
Before you can submit a record for approval, it must meet the criteria for an active approval process. If you’re not sure what the requirements are, ask your admin.
Click 
Submit for Approval
.
If an approval process applies to the record, Salesforce begins the approval process. This button isn’t available after the record has been submitted.
To keep tabs on the progress of your submitted approval, we recommend following the approval record in Chatter.
See Also:
Withdraw an Approval Request
Approval User Preferences
Approval Requests for Users
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_users_submit.htm&language=en_US
Release
202.14
