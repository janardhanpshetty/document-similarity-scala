### Topic: Web Request Limits | Salesforce
Web Request Limits | Salesforce
Web Request Limits
Limits for concurrent usage on web requests.
To ensure that resources are available for all Salesforce users, limits are placed on the number of long-running Web requests that one organization can send at the same time. Salesforce monitors the number of concurrent requests issued by all users logged in to your org and compares that number against the maximum limit. In this way, the number of concurrent requests is kept below the maximum limit. The limit ensures that resources are available uniformly to all orgs and prevents deliberate or accidental over-consumption by any one org.
If too many requests are issued by users in your org, you might have to wait until one of them has finished before you can perform your task. For example, assume that MyCorporation has 100,000 users. At 9:00 AM, each user requests a report that contains 200,000 records. Salesforce starts to run the report for all users until the maximum number of concurrent requests has been met. At that point, Salesforce refuses to take any additional requests until some of the reports have completed.
Similar limits are placed on requests issued from the API.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_administration_usage.htm&language=en_US
Release
202.14
