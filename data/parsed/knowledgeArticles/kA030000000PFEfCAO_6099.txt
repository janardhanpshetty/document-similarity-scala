### Topic: Accessing a converted Lead record normally displays Account and Contact links in addition to the converted date. However, in some instances these links are not displayed. This article provides the reason for this behavior.
Accessing a converted Lead record normally displays Account and Contact links in addition to the converted date.  However, in some instances these links are not displayed.
Resolution
If you delete the Account and Contact created/related during Lead conversion, the links to these items will be removed from the converted Lead record.  If the records are not in the recycle bin, you can run a report on your converted lead data and export this for import back into Leads/Accounts/Contacts.
For information on viewing the converted leads in your Org, click the link mentioned below:
How to view Converted Leads in your Organization
 
