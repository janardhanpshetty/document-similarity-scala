### Topic: This article talks about the new features, new resources and changed resources in REST API version 33.0
Please find below the new features, new resources and changed resources in REST API version 33.0
Resolution
Insert and Update Blob Data  is  now Generally Available
Previously we piloted a REST feature that enabled you to use the SObject Basic Information and SObject Rows resources to upload files with a size of up to 500 MB to Salesforce standard objects. The feature is now generally available, and the maximum file size has been increased to 2 GB for files that are uploaded to Salesforce CRM Content. Now you can manage large file uploads by using REST API.
Use CORS to Access REST API
CORS (Cross-Origin Resource Sharing) is a W3C recommendation that enables Web browsers to request resources from origins other than their own (cross-origin requests). For example, using CORS, a JavaScript script at https://www.example.com can request a resource from https://www.salesforce.com.
REST API now supports CORS. To access this API from JavaScript in a Web browser, add the origin that’s serving the script to the CORS whitelist. To add an origin to the CORS whitelist, from Setup, choose Security Controls > CORS. Click New, and then enter an origin URL pattern. The origin URL pattern must include HTTPS and a domain name and may include a port. The wildcard character (*) is supported and must precede a second-level domain name. For example, https://*.example.com adds all the subdomains of example.com to the whitelist. If a browser that supports CORS makes a request from an origin in the Salesforce CORS whitelist, Salesforce returns the origin in the Access-Control-Allow-Origin HTTP header, along with any additional CORS HTTP headers. If the origin isn’t in the whitelist, Salesforce returns HTTP status code 403.
You must still pass an OAuth token with requests that require it.
New Resources
Limits Resource
/vXX.X/limits
Previously we piloted the Limits resource for retrieving limits information for your organization, enabling you to conveniently track your remaining number of API calls, number of events, and storage amounts. The Limits resource is now generally available in REST API version 29.0 and later for API users with the “View Setup and Configuration” permission. Additionally, the resource can now be used to retrieve the following limits information.
• Daily generic streaming events (if generic streaming is enabled for your organization)
• Concurrent REST API requests for results of asynchronous report runs
• Concurrent synchronous report runs via REST API
• Hourly asynchronous report runs via REST API
• Hourly synchronous report runs via REST API
• Hourly dashboard refreshes via REST API
• Hourly REST API requests for dashboard results
• Hourly dashboard status requests via REST API
• Daily workflow emails
• Hourly workflow time triggers
This resource will be available within 24 hours after the Spring ’15 release.
SObject PlatformAction Resource
/vXX.X/sobjects/PlatformAction
PlatformAction is a virtual read-only object that enables you to query for actions—such as standard and custom buttons, quick actions, and productivity actions—that should be displayed in a UI, given a user, a context, device format, and a record ID.
List Invocable Apex Actions Resource
GET /vXX.X/actions/custom/apex
Returns a list of all available invocable Apex actions. Apex invocable actions provide a way to encapsulate pieces of logic in an Apex
method that can be used anywhere you can use Apex.
Response
{
"actions" : [ {
"label" : class_label,
"name" : [namespace__]class_name,
"type" : "APEX"
} ]
}
List Invocable Apex Actions Describe Resource
GET /vXX.X/actions/custom/apex/[namespace__]class_name
Returns the metadata for the specified invocable Apex action.
Response
{
"description" : null,
"inputs" : [ {
"byteLength" : 0,
"description" : null,
"label" : variable_label,
"maxOccurs" : (1 | 2000),
"name" : variable_name,
"picklistValues" : null,
"required" : false,
"sobjectType" : (null | sObject_type),
"type" : data_type
},
... ],
"label" : class_label,
"name" : [namespace__]class_name,
"output" : [ {
"description" : null,
"label" : (variable_label | "outputs"),
"maxOccurs" : (1 | 2000),
"name" : (variable_name | "outputs"),
"picklistValues" : null,
"sobjectType" : (null | sObject_type),
"type" : data_type
},
... ],
"standard" : false,
"targetEntityName" : null,
"type" : "APEX"
}
Invoke an Invocable Apex Actions Resource
POST /v33.0/actions/custom/apex/[namespace__]class_name
Invokes the specified invocable Apex action.
Request
{
"inputs" : [ {
"variable_name" : variable_value,
...
}, {
"variable_name" : variable_value,
...
},
... ]
}
Response
{
"outputs" : [ {
"variable_name" : variable_value,
...
}, {
"variable_name" : variable_value,
...
},
... ]
}
Changed Resources
Suggestions Resource
In Spring ’15, only one character is required in the q parameter for Chinese, Japanese, and Korean queries. We’ve also added a limit to the query string of 200 consecutive characters without a space break.
Additionally, these objects are now supported.
• ActionEmail
• ActionTask
• ContentVersion
• Document
• Product2
• Solution
• WorkflowRule
Query Resource Explain Parameter
You can now use the query resource to get feedback on how Salesforce optimizes the query for a report or list view. You do this by using a report or list view ID instead of a query string when using the query resource with the explain parameter. Here’s an example using a report ID:
/services/data/v33.0/query/?explain=00OD0000001hCzMMAU
The response from the query resource has been improved to include additional details on Salesforce optimizations. These details are provided in the notes field of the response JSON data. Here’s an example excerpt showing that the IsDeleted field couldn’t be used for an optimization because that field in the Merchandise custom object isn’t indexed in Salesforce:
{
...
"notes" : [ {
"description" : "Not considering filter for optimization because unindexed",
"fields" : [ "IsDeleted" ],
"tableEnumOrId" : "Merchandise__c"
} ],
...
}
Process Rules Resource
The Process Rules resource returns all active workflow rules, not all workflow rules as was previously indicated
