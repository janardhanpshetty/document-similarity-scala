### Topic: Apex callouts, Workflow outbound messaging, Delegated Authentication, and other HTTPS callouts will support TLS (Transport Layer Security) 1.1, TLS 1.2, and Server Name Indication (SNI).
Apex callouts, Workflow outbound messaging, Delegated Authentication, and other HTTPS callouts now support TLS (Transport Layer Security) 1.1, TLS 1.2, and Server Name Indication (SNI). Remote endpoints will have to be configured to support this update. Learn the answers to some common questions about this feature, below. 
 
 
Resolution
How were the customers informed about this update?
Technology Communications has sent emails to Organization Administrators whose Orgs would be impacted by this change.
 
Sample email:
"As an administrator of a Salesforce org that uses external callouts to pull critical information into Salesforce, we want to notify you of an upcoming change that will impact your org. In the Summer ‘15 release*, we plan to enable Transport Layer Security (TLS) version 1.1, TLS 1.2, and Server Name Indication (SNI) for Apex callouts, workflow outbound messaging, delegated authentication, and other HTTPS callouts. The impacted endpoint(s) we have identified for your org is ....."
 
How can I prepare my Organization for this change?
There are two methods to configure the remote endpoints:
 
1. Configure TLS settings to support TLS 1.1, TLS 1.2 and SNI. This would be the ideal case and prevent any handshake failures.
2. If customers can only enable TLS 1.0 and not the higher versions, then the remote endpoints should be configured to respond with a TLS 1.0 ServerHello instead of throwing an error, when it receives a ClientHello from us with one of the higher protocols. This method of re-negotiation is the only option as we do not have any rollback options on the upcoming change.
 
Salesforce will continue support TLS 1.0, but for TLS 1.0 to be negotiated, the remote server needs to respond as per the second method mentioned above. If the endpoint does not follow TLS standards in this regard and, instead, is configured to respond with an error when it receives a "TLS 1.1/1.2 ClientHello," they will start seeing handshake failures.
 
Do we have a retry mechanism for failed handshakes?
There is no retry mechanism when the handshake fails. Most web browsers retry with weaker protocols, but from a security perspective, retrying with weaker protocols is itself a security issue and that is not supported in Salesforce.
 
How can I test my endpoints before this release?
We advise just about all customers that make HTTPS callouts to create or refresh their sandbox before the Summer '15 sandbox preview window ends.
The general advice for TLS support is for customers to upgrade their systems to support TLS 1.2. That generally takes care of compatibility with the upcoming change, and it is equally important to test their endpoints in a Summer '15 preview sandbox.
Customers that cannot achieve TLS 1.2 compatibility should mostly be okay as we continue support TLS 1.0. However, the endpoints that fail upon seeing a TLS 1.2 ClientHello will need to be fixed to remain compatible with the Summer '15 release.
 
