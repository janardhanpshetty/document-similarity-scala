### Topic: In a post-install script, an sObject can be inserted although the isCreateable() call to a sObject Describe Result returns FALSE.
In a post-install script, an sObject can be inserted although the isCreateable() call to a sObject Describe Result returns FALSE.
This has been identified as a bug: 
W-1985329
The bug hasn't been put on Known Issues, as the correct behaviour is undetermined. (Should the LMA user have access to all objects?  If so, isCreateable() should return TRUE.  If not, it shouldn't be allowed to perform DML operations).
Resolution
If a customer reports this issue, inform them that it is working as designed, but is being considered by the R&D team.
Consult with a Backline team member that the issue is the same as bug W-1985329, and attach the bug number to the case.
