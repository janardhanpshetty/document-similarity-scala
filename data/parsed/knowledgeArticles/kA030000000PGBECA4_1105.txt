### Topic: Refer to this article if you observe that a Formula Field displays a hyperlink (either fully or partially).
You may notice that if you reference multiple formula fields of different Data Types in one formula field-it may display as a hyperlink-either fully or partially.  For example, a formula field containing 2 name fields, an address field, and an email field may be a TEXT Formula-but the Salesforce UI will return this as a hyperlink because of the ".com" naming convention in the email address-similarly if a company name has a .com, such as salesforce.com
 
Resolution
R&D has said this is working as designed, because of the intention of making email address hyperlinked.
