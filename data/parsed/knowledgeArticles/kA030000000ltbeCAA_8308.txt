### Topic: This article shares knowledge about Veeva CRM and Veeva's partnership with Salesforce
Veeva is a customized solution built on the 
Force.com
 platform, specifically to provide sales automation for Drug Manufacturers and Marketers in Pharma and BioTech companies. 
A preferred agreement was signed in 2010 that ALL Sales Cloud opportunities go to Veeva before Salesforce. Veeva will lead the sales cycle for any company that fits the criteria - unless the customer/prospect directly says they do NOT want to use Veeva.
Salesforce and Veeva Rules of Engagement Deck:
https://org62.my.salesforce.com/06930000004MNhv
For that reason AE’s at Salesforce don’t have any customer success stories for Veeva CRM. 
Veeva Customer Stories from their website: 
http://www.veeva.com/resource-type/success-story/
However, Salesforce does have plenty of resources for selling Service Cloud to Veeva customers, with integration to their Veeva Sales CRM.
This might be helpful: Service Cloud for Pharma Show Case:
https://showcase.secure.force.com/SSESiteMicrositeRecord?id=a003000000KkqvEAAR&tags=&rId=a013000000aheUoAAI
 
Customer stories in Service Cloud and Platform include Sunovion, Lilly, and Allergan – all available in the customer stories tool.
Veeva support will go through Veeva, not Salesforce. I would recommend communicating that to the customer to set up a support case on the Veeva side. 
Another helpful resource is the Chatter Group: Veeva Systems, ISV Partner:
https://org62.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F9300000008Xu5&s=veeva&r=1
Veeva on AppExchange: 
https://appexchange.salesforce.com/listingDetail?listingId=a0N300000016ZRTEA2
Resolution
