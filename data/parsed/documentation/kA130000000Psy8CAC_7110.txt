### Topic: Custom Tabs | Salesforce
Custom Tabs | Salesforce
Custom Tabs
Display custom object data or other web content using custom tabs in Salesforce.
Available in: both Salesforce Classic and Lightning Experience
Custom Object Tabs and Web Tabs available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Visualforce Tabs available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Lightning Page Tabs available in: 
All
 Editions except 
Database.com
User Permissions Needed
To create and edit custom tabs:
“Customize Application”
Custom tabs display custom object data or other web content embedded in the application. You can create any of these types of custom tabs.
Custom Object Tabs
Custom object tabs (available only at an app level and not on subtab apps) display the data of your custom object in a user interface tab. Custom object tabs look and function just like standard tabs.
Web Tabs
Custom web tabs display any external web-based application or web page in a Salesforce tab.
 You can design web tabs to include the sidebar or span across the entire page without the sidebar.
Visualforce Tabs
Visualforce tabs display data from a Visualforce page. Visualforce tabs look and function just like standard tabs.
Lightning Component Tabs
Lightning component tabs make Lightning components available in the Salesforce1 mobile app (via the navigation menu) or in Lightning Experience (via the App Launcher). Lightning components aren’t supported in Salesforce Classic.
Note
To expose Lightning components via Lightning component tabs, you must enable and deploy My Domain.
Lightning Page Tabs
Lightning Page tabs let you add Lightning Pages to the Salesforce1 and Lightning Experience navigation menus.
In Salesforce Classic, Lightning Page tabs don’t show up on the All Tabs page when you click the plus icon (
) that appears to the right of your current tabs. Nor do Lightning Page tabs show up in the Available Tabs list when you customize the tabs for your apps.
In Lightning Experience, if a Lightning Page tab is set to Default On, it appears in the Lightning Experience custom navigation menu assigned to that profile. If a Lightning Page tab is set to Default Off, it appears in the App Launcher under Other Items.
Subtab apps support only web tabs and Visualforce tabs.
Delegated administrators who can manage specified custom objects can also create and customize tabs for those custom objects.
See Also:
Creating Custom Apps
What is a Subtab App?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dev_tabdef.htm&language=en_US
Release
202.14
