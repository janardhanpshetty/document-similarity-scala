### Topic: High-level summary explaining splits, and linking to other useful documents to help you prepare for an upcoming split. Includes FAQs, pre- & post-split checklists, and a hard-coded references guide. If you are looking for information on preparing for a migration - where we move all customers on an instance from one data center to another data center - please see the How to prepare for a Migration Knowledge Article #000176190.
THIS KNOWLEDGE ARTICLE HAS BEEN ARCHIVED. 
Starting in 2016, we will no longer be performing instance split or migration maintenances. Instead, we will be conducting "instance refresh" maintenances. Instance refreshes will streamline the customer experience as well as the execution process for our tech teams. More information on instance refreshes can be found in the 
Instance Refresh Maintenance FAQ
 article.
 
 
Resolution
 
At Salesforce, customer success is our top priority. Due to the extraordinary growth and success of our customers, we occasionally need to move a subset of our customers to a new Salesforce instance.  This proactive maintenance is called a split. 
The goal of this maintenance is to deliver exceptional availability and reliability of our service.  We carefully review and monitor our capacity levels across all our instances to ensure we plan well ahead of our customers’ growth. When an instance approaches our threshold for capacity, we make the decision to perform a split.  Prior to performing a split, we make every effort to ensure customers are aware and prepared. Below are some common questions we receive from customers that you may find valuable
Note
: This document is for informational purposes only, and is not part of any legal or otherwise binding agreement. The policies and practices described in this document are subject to change at Salesforce's sole discretion.
 
Materials
Pre-Split Checklist (see attachment below)
This spreadsheet explains the steps you should take before the split occurs.
Post-Split Checklist (see attachment below)
This spreadsheet explains the steps to take once the split has been completed.
 
Table of Contents
General FAQs
Technical FAQs
Hard-Coded References
My Domain 
Network Settings
B
atch Jobs / Scheduled Jobs / Bulk API Jobs
Integrations & Certificates
Email
WSDLs
OAuth & Salesforce for Outlook
AppExchange
S
earch
Sandbox
Physical Delete
Read-Only Environments
Partner Portals
 
General FAQs
​
1.  Why do you perform a split?
The goal of these maintenances is to continue to deliver exceptional availability and reliability of our service.  We carefully review and monitor our capacity levels across all of our instances to ensure we plan well ahead of our customers’ growth. When an instance approaches our threshold for capacity, we make the decision to perform a split.  Prior to performing a split, we make every effort to ensure customers are aware and prepared.  
 
2. How will this split benefit me?
By performing split maintenance, we are planning for your growth and success. Split maintenance activities include proactively managing capacity on the Salesforce infrastructure. With these improvements, we can continue to provide exceptional performance, availability, and reliability of our service.
 
3.  When are splits scheduled?
Whenever possible, we schedule a split during the instance’s standard maintenance windows, which usually take place during off-peak business hours.  However, due to the service maintenance activities required for a split, the maintenance window may extend beyond the standard maintenance window.  
During 
this time, customers’ current and new instances will be unavailable unless Read-Only mode is in effect. You can read more about Read-Only mode here: 
https://help.salesforce.com/apex/HTViewSolution?urlname=Read-Only-Mode-Overview&language=en_US
.  If Read-Only mode is unavailable during the maintenance, users attempting to access their current or new instances during this window will be presented with a scheduled maintenance notification page.  For more information on our maintenance windows, please review this Knowledge Article: 
https://help.salesforce.com/HTViewSolution?id=000176208&language=en_US
 
4.  What does this maintenance page look like when an instance is unavailable (e.g. during maintenance)?  
See the image below.  However, if a customer has an SSO with a customer login page (which they control), they will get a 404 (Page Not Found) error. 
  
5.  How is Salesforce communicating the change to customers?
Salesforce communicates with all the customers impacted by the split through multiple avenues to ensure they are aware and prepared for this maintenance: ​
a.  Salesforce Product and Service Notification emails (sent to System Administrators)
b.  Outreach (phone or email) from your Account/Support team
For those customers remaining on their current instance, we will notify them of the extended maintenance window via our Salesforce Product and Service Notification emails.
 
6.  What action do I need to take to prepare for the split?  
a.  Review all custom links to confirm you use relative URL references, i.e. use login.salesforce.com instead of na4.salesforce.com.  Update all hardcoded references in Knowledge articles, integration and email templates
 to ensure they continue to work after the split.  
b.  Review any integration that uses the Force.com web services API to verify there are no hard-coded references to [instance].salesforce.com
c. Review any customer and/or partner portal setup for hard-coded references to [instance].salesforce.com
d. Continue to follow our best practices and whitelist IP address ranges from all of our data centers. (For more information on Whitelisting network IP addresses, check out this 
Knowledge Article
)
e.  
If you control your DNS timeout values set, then you will need to refresh your DNS cache and restart any integration following the maintenance.
 
7.  How will I know the maintenance windows for the new instance?
In most cases, the maintenance windows for the new instance will remain the same as your current instance.  For more information on our maintenance windows, please review this Help & Training Article: 
https://help.salesforce.com/HTViewSolution?id=000176208&language=en_US
 
8. Can I choose to stay on my instance following the split?
To ensure customer success, our Capacity Planning team takes into account multiple metrics to ensure a balance of the customers on the instances. We recommend that you follow all of our best practices which will minimize any disruptions from this or other future infrastructure improvements.  Unfortunately, you cannot choose whether you stay on the instance you are currently on, or move to the new instance during a split.
 
9. Why do I need to split to a new instance, when other customers get to stay on their current instance?
In order to make sure the split is balanced, the Capacity Planning team cannot take customer preference into consideration.  Once an instance is determined that it needs to split, analysis of the best way to split up the instance is done, with considerations around an efficient split model so we don’t have to split the instance again for another 2-3 years. That analysis includes a complex data model that takes into account over 25 business, usage, and technical metrics to determine the best balance of customers, both short and long-term, for each instance.  Due to this model driven approach, customer preference cannot be taken into account.   
10. How does Salesforce decide who stays on an instance and who moves to a new instance?
The Capacity Planning Team looks at a variety of metrics to determine whether a customer org should stay on the source instance or split to a new instance, to ensure balance of capacity consumption over time.  The split analysis covers detailed data analysis to ensure that we effectively distribute customer usage rates and growth projections across the instances for the best long term customer experience for all customers affected.
11. What are the factors that the Capacity Planning team leverages to make this decision?
At the instance level, Capacity Planning looks at a combination of factors to determine the best balance of customers. This includes: number of orgs, average daily transactions, database CPU consumption, app tier consumption, daily user count, current and forecasted growth rate of the above, potential for added growth, upcoming contract renewals, etc.  The usage and growth are assessed by taking into account database storage, file-force storage, database and app server cpu consumption, monthly transaction rates and database gets, consumption rate in our services and other company characteristics to balance post-split instances as best as possible across all of these fields and select the best long term destination for each org.
 
12. Following a split maintenance, will new customer signups take place on the original instance that was split and/or the new instance?
In order to ensure that there is room for current customers to grow on the instances, we do not add new customers to the original instance that was split or to the new instance. There are separate instances dedicated to new customer signups.
 
13.  Where can I go if I have further questions?
For further questions, please contact Customer Support by logging a case via the 
Help & Training portal
.  
You can also watch the Salesforce Instance Splits and Instance Migrations 
video here
.
 
Technical FAQs
The following questions are technical in nature, and were written for the Administrator at your organization, or your IT department. We recommend that you start with our 
Customer Instructions
, which include best practices for leveraging relative URLs in your integration and whitelisting IP addresses for all 
Salesforce
 data centers.
 
Hard-Coded References
1.  What is the difference between a hard-coded reference and a relative URL?
A hard-coded reference is a URL that contains the instance name in the URL (e.g. na1.salesforce.com). A relative URL is a way to identify a resource (e.g. any Salesforce object) relative to their context (in our example, relative to nax.salesforce.com without stating that beginning). A relative URL could look something like this: /servlet/servlet.FileDownload?file=01560000000BRIM
Instead of the equivalent, full path URL: https://naX.salesforce.com/servlet/servlet.FileDownload?file=01560000000BRIM. 
For more information on relative URLs, please check out our Knowledge Article: 
Best Practices When Referencing Server Endpoints
2.  How can I find hard-coded references?
The following Customer Instructions provide in-depth details of different options of how to find hard-coded references: 
https://help.salesforce.com/apex/HTViewSolution?urlname=Updating-Hard-Coded-References-FAQ&language=en_US
Salesforce’s recommended best best practice is to use relative URLs (e.g. login.salesforce.com) with any integration or Salesforce code, instead of hard-coding instances (na6.salesforce.com). By following this best practice, it will make it significantly easier for you to prepare for future instance migrations, one-off org migrations, or to implement 
My Domain
 functionality.  Make sure to update all hard-coded references in Knowledge articles and integration
. 
 
3.  What if I am using the self-service portal and it is pointing to an instance URL for login?
The self service login URL: sserv/login.jsp?orgid will redirect to the correct instance based on the OrgId.   
4.  How can I make references to Visualforce pages server agnostic? For example, https://c.na1.visual.force.com/apex/MyContactPage?id={!Contact.Id} is tied to a custom button.
To make references to Visualforce pages server agnostic, run a query within your custom button to pull the URL for the result you’re looking for. 
For the above example, the following will work with a custom button: {!URLFOR("../apex/MyContactPage", Contact.Id, [id=Contact.Id])}
5.  How can I search and update Knowledge Articles that reference other articles and sometimes hosted images which reference “[instance].salesforce.com” in the article content?
You will need to search and manually update the Knowledge Articles as it is not possible to find and update them with the Force.com IDE.  You can try to search through the regular API. Using workbench, perform the following SQL query:  
SELECT Id, MyField__c
FROM MyKnowledge__kav
WHERE PublishStatus = 'online' AND Language = 'en_US' AND MyText__c
LIKE '%salesforce.com%';
Note that the WHERE clause may not work for all types of fields. For instance, if you use RTA, you cannot filter on it. In this case, you need to get all the records and run a local search, or use a different filter. If you have further questions, log a case with Support.
Note that 
all links created through "Smart Link" functionality in the RTA editor will not be affected by a split. While the host name displays on the page for these specific links, the host name is not actually referenced in the HREF of the underlying code. Therefore, these links will not be affected and continue to function as expected post-split without requiring any changes.
6.  Do I need to update Email Templates that contain hard-coded references?
Yes, you will need to update Email Templates that contain hard-coded references as the split process does not update any customer code.
7.  Do I need to update any Chatter links, browser bookmarked URLs, etc?
No, you will not need to update any Chatter links, browser bookmarked URLs, etc. Any links accessed by browsers will automatically be redirected to the new instance via our servers.
8. Will Content URL links be redirected?
Yes, Content URL links will be redirected.
9. Is there a way to automatically change hard-coded links in Chatter posts to reflect the new instance?
No, there is no way to automatically change hard-coded links in Chatter posts to reflect the new instance. Please remember though that all browser links will be redirected to the new instance.
10. Will Org IDs change as a result of the split?
No, there will not be a change to Org IDs.
11. Will the Salesforce IDs and root-relative URLs of records be preserved?
Yes, Salesforce IDs and root-relative URLs for records will remain the same.
12. 
Do record IDs change at all as a result of a split? 
No. Existing record IDs will not change after a split. 
13. Do I need to change Live Agent Server endpoints (e.g. the URLs within Live Agent Chat buttons)?
Yes. Once the maintenance completes, we recommend that you replace the Live Agent Deployment Code and Button with the new deployment code. You can replace both through Live Agent Setup. As a best practice, we recommend replacing all deployment and button code used on other sites with the new snippet of code that you can access via Setup after the split maintenance has completed.
My Domain
14. If I have My Domain setup, do I need to do anything in preparation of this maintenance?
 
This split will be seamless to our My Domain customers, as long as you follow our best practices of whitelisting IP addresses, and you don’t have hard-coded references in your Visualforce pages.
If you have a Partner Portal, please note that instance-specific URLs won’t redirect to MyDomain URLs in Partner Portal.  In order to fix this, you will need to remove hard-coded references from your Visualforce pages and Widgets in Partner Portal before switching to My Domain.  Please see more information about this in our KB article here:
https://help.salesforce.com/apex/HTViewSolution?urlname=Updating-Hard-Coded-References-FAQ&language=en_US
 
15. Should I hold off on implementing My Domain until after the maintenance is complete?
No, there is no technical reason to hold off the implementation of My Domain because of this maintenance. The only timing to watch out for is when you ‘register’ the My Domain name. You should allow 24 hours for the request to complete, so we recommend avoiding making this change right before the maintenance to avoid the very remote chance of getting the DNS paired with an incorrect IP address (and missing the correction during maintenance). This registration step can be done well in advance if you have a tight schedule. 
Network Settings
16. Will I need to update my network settings prior to the maintenance window? If so, why?
If you are moving to a new data center due to a split, you will need to ensure your network settings include the IP ranges of both your current data center and your new data center.  If you do not add the IP ranges of the data center you are moving to, you will block your end-users from the new instance, causing them to receive an error message after the maintenance is complete.
Additionally, if you or your team have set up your corporate network settings to allow only certain IP ranges, including for Email Security Filters or Email Relaying, or due to Firewalls, please ensure that you are including our newest ranges added in April 2015.  More information on which IP ranges to include can be found in the 
What are the Salesforce IP Addresses to whitelist?
 Knowledge article.
 
17.  Can I remove my old IP addresses from my set of whitelisted IP ranges after the split is complete?
 
The best practice is to whitelist all of our data centers with the IP address ranges. There is no risk in whitelisting the specified range of IP addresses as Salesforce owns the range; it is not leased or shared in any way with any other organization. Salesforce has an IP address block allocated directly to Salesforce by the American Registry for Internet Numbers (ARIN).
 
If you still want to whitelist IP addresses for specific data centers, please ensure you include IP ranges for the data center that you are moving to, and the data center that will be used for your new instances’ disaster recovery.
 
Please note that if you remove the previous instance’s data center IP ranges, or put access restrictions to [previous instance name].salesforce.com, browser requests from static links (such as bookmarked URLs) pointing to that previous instance will not be redirected to the new instance. This is because they will not be able to reach the previous instances’ servers to be processed. Check out this 
Knowledge Article on Whitelisting 
for more information.
 
18. Is there a way for me to test my network settings to ensure they are up to date on my side?
Yes. You can test that your network settings are up-to-date by trying to reach an instance that is already live in the data center you are moving to. For example, if your instance is on AP1 and you are moving to an EMEA-based data center, point your browser to eu5.salesforce.com and you should be directed to a login page to enter your Salesforce credentials. If you have your network settings set up correctly to allow access to the EMEA data center, the login pool will log you into your Salesforce application and will automatically redirect you to your instance (i.e. ap1.salesforce.com). If you are unable to login, then, most likely, your network settings are restricting access to the EMEA data center. 
NOTE: The test may also fail if a maintenance is currently being performed on the particular instance in the EMEA data center (in this example, EU5). Check 
trust.salesforce.com/trust/instances
 to verify lack of maintenance before assuming your network settings are restricting access.
 
19. 
Do I need to update or make changes to my DNS after the split?
 
Yes, if you control your DNS timeout values set, then you will need to refresh your DNS cache and restart any integration following the maintenance.
Batch Jobs / Scheduled Jobs / Bulk API Jobs
 
20. Do I need to take any action on our batch, scheduled, long-running transactions, or bulk API jobs prior to the maintenance?
No, you do not need to take any action on batch, scheduled, long-running transactions, or bulk API jobs prior to the maintenance. Salesforce will queue all jobs and process them once the maintenance is complete.
 
21.  Will there be any impact to Web-to-Leads/Cases (W2X)?
No, there will not be any impact to W2X. W2X will be queued and processed once the maintenance is complete
23. Will there be a delay in Email-to-X (e.g. Email-to-Case) or Web-to-X (e.g. Web-to-Case) after the split?
Yes, Email-to-X and Web-to-X will be queued and then processed once the maintenance is complete.
 
24. Does the Split impact my Weekly Exports?
Yes, Weekly Exports will be temporarily suspended during the split maintenance window. Once the split maintenance is complete, Weekly Exports in 'Pending' status, as well as those that were scheduled to run during the split maintenance window will be automatically re-enqueued. Weekly Exports in the midst of processing at the time of the split maintenance will be resumed for orgs staying on their instance, and restarted for those splitting to a new instance.
 
Integrations & Certificates
25. Do I need to restart integration after the maintenance?
 
Yes, you may need to restart some integration after the maintenance. By restarting your integration following the maintenance window, you would be clearing the DNS lookup cache which would cause your integration to refresh and detect the IP address of the new data center.
 
26.  Is there a way to test our integration prior to the split in order to ensure they are set up correctly?
 
It is not possible to test integration before the split without MyDomain. If you implement MyDomain, you do not need to test your integration before the split. MyDomain requires you to fix any hard-coded references before going live.
 
27. Will this impact my CTI integration?
If you are using Open CTI and the CTI Adapter URL in your Call Center definition is hard-coded with your Salesforce instance (e.g. 
https://c.na6-visual.force.com/apex/Softphone
), this will impact your CTI integration. Please change this to a relative URL (e.g. /apex/Softphone) to ensure your CTI Integration will continue to work after the split.
If you are using the Salesforce Desktop CTI Integration Toolkit, this maintenance will not impact your CTI integration. URLs that the CTI integration directs the browser to will correctly redirect to the new instance.
To update this, you can navigate to the specific click path of the call center object: Setup > Customer > Call Centers.
For relative CTI Adapter URLs, or URLs pointing to non-Salesforce domains, this maintenance will not impact a CTI integration.
 28.
 
Will certificates created under Setup->Security Controls->Certificate and Key Management still be there after the move?
Certificates created under Certificate and Key Management will still be there after the move. However, if you stored the [instance].
salesforce.com
 certificate locally, then you will need to contact Support for the zip file of certificates.
If you are manipulating the hostnames that the API connects to (and not using DNS hostnames), your Java implementation might cache the certificates, which would require a restart of your Java API after the move.
29.  Will the certificates used with the current instance at [INSTANCE].salesforce.com and [INSTANCE]-api.salesforce.com be used with the new instance?  Is any action required-post maintenance?  
No customer action is required with these certs post-maintenance.
30. Will the same intermediate, root, and outbound customer-to-Salesforce certificates be used with the new instance?
 
Yes, the intermediate and root certificates will remain the same. Additionally, outbound certificates that are used for SAML, SSO, and APEX will not change.
 
31. Will this maintenance impact SSO?
 
No, this maintenance should not impact SSO. If your Org is not moving to a new data center, then nothing will change.  If your Org is moving to a new data center, then the originating IP address will change to the new data center’s' outbound proxy. The outbound SSO cert that customers use for SSO or APEX SSL authentication will not change.  ​ 
 
32. If I have certificates, do I need to re-download them post-split? 
a. Yes, if you are both moving to a new instance as part of the split and have stored the salesforce.com, my.salesforce.com, force.com, database.com, or cloudforce.com certificates locally previous to the split.
​
b. No, if you have NOT stored your certificates locally.
i. 
Certificates and keys that are set up in the Certificates and Keys Setup within your Salesforce org will move to the new instance. No action is needed on your part to keep these certificates; they will all move to the new instance.
c. 
If you are manipulating the hostnames that the API connects to (and not using DNS hostnames), your Java implementation might cache the certificates, which would require a restart of your Java API after the move.
Email 
33. Do I need to make any changes to my Email Settings?
Please review your email server IP ranges and mail relay IP addresses to ensure they are updated per the 
What Salesforce network IP ranges do I need to whitelist?
 article.
 
 34. Do I need to make any changes to my Sender Policy Framework (SPF) records?
It depends on how you have your SPF setup. We recommend enabling the “Enable compliance with standard email security mechanisms” option, but in the unlikely case where you have specific Salesforce IP addresses listed in your SPF, you will need to change them, or include our SPF records that cover all IP ranges.
 
35. Will the Apex Email Services (e.g. InboundEmailHandler) address continue to work after the split?
The address will continue to work as Salesforce will redirect the mail internally to the org's new instance. You may regenerate the email address to avoid unnecessary redirection if you’d like, but it’s not necessary. 
 
36. Will the EmailService Address change post-split?
No, the EmailService Address will not change post-split. 
37. Will Email Thread IDs change after the maintenance?
Existing Email Thread IDs will not change following the split maintenance. However, new Thread IDs may have a new format to reflect the new instance ID. Please do not create your own custom Thread ID formats, as that is not a Salesforce supported functionality and formats may change over time. This could negatively impact your Email-To-Case (E2C) functionality following the split maintenance window.
After a customer’s org moves to a new instance with a split maintenance, some customers may experience an issue with on-demand Email-To-Case (E2C) where replies with thread IDs are creating new cases and not attaching to the original case. This issue only occurs with customers who generate their own Custom Email Thread ID. Please note that creating custom Thread IDs is not supported.
In order to fix issues with Thread IDs, you may choose one of the following options:
(a) stop generating your own Email Thread ID and use out-of-box Thread ID that salesforce generates, or
 
  (b) update their custom formula to following the format ref:_00D[XX][yyyyy]._500[AA][bbbbb]:ref, where yyyyy and bbbbb are the 10-char ID with the leading zeros stripped. Email Thread ID formats that are parseable by our system code are as follows, (yyyyy and bbbbb are the record ID without the leading zeros) ref:_00DXXyyyyy._500AAbbbbb:ref (this is the current format) ref:00DXyyyyy.500Abbbbb:ref (this is the old format that is no longer used).
Please note that we do not officially support custom formulas for case Thread IDs and recommendation is for the customer to use out-of-box Thread ID that Salesforce generates, or, if needed, they could update their custom formula to follow the format above.
 
38. Do I need to request my Email Logs before the split?
If you are moving to a new data center during the split maintenance, then you need to request your Email Logs prior to the maintenance window. Email Logs from your former data center cannot be extracted after the maintenance. Once the Email Logs are requested, they will be stored in the database and migrated along with your other data during the split maintenance.
WSDLs
39. What actions do I need to take for any Apex Web Services Generated WSDLs?
​For any Apex Web Services Generated WSDLs, you need to modify the WSDL to point to the new instance, or you will need to regenerate the WSDL after the move to the new instance.  You can regenerate the WSDL before the split if you’d prefer, but you cannot implement it until the split has taken place.  
40. Is there a way to rewrite these WSDLs now so they are unbreakable and do not have to change before/after the split?
Yes. If the org has MyDomain enabled, the endpoint specified in the Apex WSDL points to the MyDomain host name (
xxx.my.salesforce.com
), which will not change after the migration/split. 
41. Does a partner WSDL need to be regenerated after the move?
The Partner WSDL should have the endpoint defined as a preferred generic URL (e.g. '
login.salesforce.com
'), and thus would not need to be regenerate after the move. To verify this, you can download the Partner WSDL from the org SETUP > DEVELOP > API menu, open up the XML, and search through to see if there are any instance specific URLs in it.
 
OAuth & Salesforce for Outlook
42. Will OAuth refresh tokens be affected by this maintenance?
If you have OAuth refresh tokens, they will not be impacted after the maintenance - with the exception of Salesforce for Outlook (SFO).  All versions of SFO will be required to login again. Follow the steps in the "
Salesforce for Outlook OAuth re authentication
" to ensure post-split users will be prompted to login again to the SFO plugin and any connectivity issues post-split are resolved.
AppExchange
43. Will apps from the AppExchange be impacted by this change?
No, we work with our ISV partners to ensure they don’t have any hard-coded URLs in their AppExchange apps. Additionally, many ISV partners follow our best practices to not hard-code specific instances in their integrations or code.  
 
44. What happens if you maintain AppExchange managed packages?
If you have hard-coded references in the managed packages, you will need to modify them.  We recommend that you re-test all managed packages when the maintenance is complete. 
 
Search 
45. Will there be any impact to Search after the maintenance is complete? 
No, Search will not be impacted after the maintenance is complete. 
 
Sandbox
 
46. Do I need to make any changes to my Sandbox before or after the maintenance?
No, as long as you don’t have any hard-coded references to your current production instance in your Sandbox, no changes are required to your Sandbox environment before or after the maintenance. If you have hard-coded references to your production instance in your Sandbox, please check out our Hard-Coded References section for details on how to update them. 
 
47. Should I refresh my Sandbox once the maintenance is complete?
If the split causes you to move to a new data center, we recommend that you refresh your Sandbox after the maintenance is complete to ensure optimal access to your file storage data.  If you are moving instances that are both located within the same data center, then a Sandbox refresh is not needed.
 
48. Can I get an up-to-date Sandbox refresh before the maintenance begins?
No, we cannot guarantee that you can get an up-to-date Sandbox refresh before the maintenance begins. We cannot predict how long a Sandbox refresh will take, so if you request a refresh of your Sandbox prior to the maintenance, and it does not finish in time, it will either resume once the maintenance is complete, or if you’re moving instances, restart the refresh request once the maintenance is complete. Please evaluate how long your previous Sandbox refreshes have taken, and plan accordingly.  Additionally, please be aware that we will suspend the Sandbox queues 4 hours before split and will keep them suspended 24 hours post split.
 
49. What happens to my Sandbox if it is in the middle of a refresh when the split occurs?
If your org was moved to a new instance while you were in the midst of a Sandbox refresh, the Sandbox copy will be automatically restarted after the split. If your org did not move during the split, your Sandbox refresh will automatically be resumed.
Physical Delete 
50.  How is physical delete impacted by a split?
We suspend physical delete the Monday before the split and unsuspend it 48 hours following the split maintenance window for both customers and Customer Support.
 
Read-Only Environments
51. Does Salesforce offer read-only environments for 24/7 customers who need access to the Salesforce application at all times?
Yes, Read-Only mode may be available for a portion of your maintenance. You can read more about Read-Only mode here: 
https://help.salesforce.com/apex/HTViewSolution?urlname=Read-Only-Mode-Overview&language=en_US
.  
 
Partner Portals
52.  Can I control what my end users will see when trying to access my partner portal or community during the split maintenance window?  
a.  It depends on several factors. The matrix below describes what to expect for each site type based on whether the domain supports https and whether a custom service-not-available (SNA) page has been set up on the site.
Site Type
HTTP-Only Domain, No SNA
HTTP-Only Domain, SNA Set
HTTPS Domain, No SNA
HTTPS Domain, SNA Set
Salesforce Site.com Communities
Generic service is unavailable page 
Generic service is unavailable page 
Generic service is unavailable page 
Generic service is unavailable page 
Salesforce Force.com Communities
Generic service is unavailable page
N/A
Generic service is unavailable page 
N/A
Force.com Sites (non-Community)
Generic service is unavailable page 
Custom SNA page
Blank page or connection error message
Blank page or connection error message
Site.com (non-Community)
Site is rendered while showing the error view for repeaters and forms
Site is always available
Site is rendered while showing the error view for repeaters and forms
Site is always available
b. Yes. If you are using a Force.com site on a http-only domain or a Salesforce Community on a custom https domain, assign a maintenance or error page for anyone trying to access your partner portal or community during the maintenance via the following link: 
https://help.salesforce.com/HTViewHelpDoc?id=sites_error_pages.htm&language=en_US
 . Please note that in a Force.com site that is not a Salesforce Community, this page is presently shown only on http-only domains and not on https domains. In a Salesforce Community, this page is shown only on custom https domains and not on the force.com subdomain.
c. If you are using Site.com, it will remain online during the maintenance window, though repeaters and forms, which are unique to Site.com, will show errors instead of presenting the repeater data and form data.
d. Any site or domain that can't have a service-not-available page configured from the criteria above will display a browser-specific error message after a timeout period elapses.
e. Any Force.com site that is not a Salesforce Community and does not have a service-not-available page configured will display a page that looks like the following on its http-only domains:
f. If you are accessing your 
secure.force.com
 subdomain via https, you will get a browser-specific error message after a timeout.
g. If you attempt to access Salesforce Communities, you will receive a browser-specific error message after timeout.
h. In order to set up a Login Message for their Customer Portal, Customers must have My Domain enabled.  Otherwise, their message may not display properly for all users.
