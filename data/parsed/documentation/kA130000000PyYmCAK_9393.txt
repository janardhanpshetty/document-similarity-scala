### Topic: Set the My Domain Login Policy | Salesforce
Set the My Domain Login Policy | Salesforce
Set the My Domain Login Policy
Secure your login by customizing the login policy for your domain.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Performance
, 
Unlimited
, 
Enterprise
, 
Developer
, 
Professional
, and 
Group
 Editions.
User Permissions Needed
To set login policy for a domain:
“Customize Application”
Customize your login policy to add a layer of security for your org. By default, users log in from a generic Salesforce login page, bypassing the login page specific to your domain. Users are also allowed to make page requests without your domain name, such as when using old bookmarks.
From Setup, enter 
My Domain
 in the 
Quick Find
 box, then select 
My Domain
.
Under My Domain Settings, click 
Edit
.
To turn off authentication for users who don't use your domain-specific login page, select the login policy. For example, selecting the policy prevents users from logging in at the generic 
https://<instance>.salesforce.com/
 login page, and then being redirected to your pages after login. This option enhances security by preventing login attempts by anyone who doesn't know your domain name.
Choose a redirect policy.
Choose 
Redirect to the same page within the domain
 to allow users to continue using URLs that do not include your domain name. Choosing this option does not enhance security for your org.
Note
Bookmarks don’t work when the 
Redirect to the same page within the domain
 option is selected for partner portals. Manually change the existing bookmarks to point to the new domain URL by replacing the Salesforce instance name with your custom domain name. For example, replace 
https://
yourInstance
.salesforce.com/
 with 
https://
yourDomain
.my.salesforce.com/
 in the bookmark’s URL.
Choose 
Redirected with a warning to the same page within the domain
 to remind users to use your domain name. After reading the warning, users are allowed to view the page. Selecting this option for a few days or weeks can help users transition to a new domain name, but does not enhance security for your org.
Choose 
Not redirected
 to require users to use your domain name when viewing your pages. This option provides the greatest level of security.
Click 
Save
.
See Also:
Set Up a Domain Name
Guidelines and Best Practices for Implementing My Domain
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=domain_name_setting_login_policy.htm&language=en_US
Release
202.14
