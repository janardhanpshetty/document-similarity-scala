### Topic: Control Individual API Client Access to Your Salesforce Organization | Salesforce
Control Individual API Client Access to Your Salesforce Organization | Salesforce
Control Individual API Client Access to Your Salesforce Organization
With API Client Whitelisting, restrict all API client applications, such as the Data Loader, to require administrator approval, unless the user’s profile or permission set has the “Use Any API Client” permission.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view the settings:
“View Setup and Configuration”
To edit the settings:
“Customize Application”
AND
“Modify All Data”
Administrators may grant some users API access through the “API Enabled” permission. After it’s given, this permission allows the user API access through any client (such as the Data Loader, Salesforce1, Salesforce for Outlook, or the Force.com Migration Tool). For finer control over which applications the user can use for API access, you can implement API Client Whitelisting. This feature leverages the existing authorization capabilities of connected apps. With API Client Whitelisting, an administrator can approve or block individual client application access for each associated connected app. All client applications that are not configured as connected apps are denied access. If you are not using connected apps, you can relax this restriction for individual users by assigning them a profile or permission set with “Use Any API Client” enabled.
Note
Contact Salesforce to enable API Client Whitelisting. After it’s enabled, all client access is restricted until explicitly allowed by the administrator. This restriction might block access to applications that your users are already using. Before you enable this feature, you should configure and approve connected apps for any client applications you want users to continue using, or give the users a profile or permission set with “Use Any API Client” enabled.
To configure API Client Whitelisting, do the following.
Contact Salesforce to get the feature enabled for your organization.
From Setup, enter 
Connected Apps
 in the 
Quick Find
 box, then select the option for managing connected apps.
In the App Access Settings, click 
Edit
.
Select 
Limit API access to installed connected apps with the "Admin approved users are pre-authorized" policy
.
Optionally, select 
Allow Visualforce pages to bypass this restriction
 so that any Visualforce pages that use the API continue to be authorized to access objects in the organization. If you enable API Client Whitelisting without selecting this option, only approved connected apps are authorized, and Visualforce pages might not behave as expected. Also, if unchecked, client applications that call 
getSessionId()
 are denied access. Apps that make API calls to Salesforce using a session obtained in a Visualforce context are denied access unless you select this checkbox.
Click 
Save
.
After you select this feature, all client applications need explicit approval by an administrator to be authorized for the organization, unless the user has a profile or permission set with “Use Any API Client” enabled.
Some components for commonly used apps are automatically installed as connected apps in organizations. These components support apps such as the Data Loader, Salesforce1, Workbench and more. After you select this feature, these components will also require approval, unless the user has a profile or permission set with “Use Any API Client” enabled. See 
Managing a Connected App
 for more information about these components.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_control_client_access.htm&language=en_US
Release
202.14
