### Topic: Divisions Overview | Salesforce
Divisions Overview | Salesforce
Divisions Overview
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Divisions let you segment your organization's data into logical sections, making searches, reports, and list views more meaningful to users. For example, you can create a report to show the opportunities for just the North American division, allowing you to get accurate sales numbers for the North American sales team. Divisions are useful for organizations with extremely large amounts of data.
Note
Divisions do not restrict users’ access to data and are not meant for security purposes.
Divisions can be assigned to users and to other records.
Record-level division
—Division is a field on individual records that marks the record as belonging to a particular division. A record can belong to a division created by the administrator, or it can belong to the standard “global” division, which is created automatically when your organization enables divisions. A record can belong to only one division at a time.
Default division
—Users are assigned a default division that applies to their newly created accounts, leads, and custom objects that are enabled for divisions.
Working division
—If you have the “Affected by Divisions” permission, you can set the division using a drop-down list in the sidebar. Then, searches will show only the data for the current working division. You can change your working division at any time. If you don’t have the “Affected by Divisions” permission, you’ll always see records in all divisions.
The following table shows how using divisions affects different areas.
Area
Description
Search
If you have the “Affected by Divisions” permission:
In sidebar search, you can select a single division, or all divisions.
In advanced search, you can select a single division or all divisions.
In global search, you can search a single division or all divisions.
For searches in lookup dialogs, the results include records in the division you select from the drop-down list in the lookup dialog window.
Note
All searches within a specific division also include the global division. For example, if you search within a division called Western Division, your results will include records found in both the Western Division and the global division.
If you do not have the “Affected by Divisions” permission, your search results always include records in all divisions.
List views
If you have the “Affected by Divisions” permission, list views include only the records in the division you specify when creating or editing the list view. List views that don’t include all records (such as My Open Cases) include records in all divisions.
If you do not have the “Affected by Divisions” permission, your list views always include records in all divisions.
Chatter
Chatter doesn’t support divisions. For example, you can’t use separate Chatter feeds for different divisions.
Reports
If you have the “Affected by Divisions” permission, you can set your report options to include records in just one division or all divisions. Reports that use standard filters (such as My Cases or My team’s accounts) show records in all divisions, and can’t further limited to a specific division.
If you do not have the “Affected by Divisions” permission, your reports always include records in all divisions.
Viewing records and related lists
When viewing the detail page of a record, the related lists show all associated records that you have access to, regardless of division.
Creating new records
When you create new accounts, leads, or custom objects that are enabled for divisions, the division is automatically set to your default division, unless you override this setting.
When you create new records related to an account or other record that already has a division, the new record is assigned to the existing record’s division. For example, if you create a custom object record that is on the detail side of a master-detail relationship with a custom object that has divisions enabled, it is assigned the master record’s division.
When you create records that are not related to other records, such as private opportunities or contacts not related to an account, the division is automatically set to the global division.
Editing records
When editing accounts, leads, or custom objects that are enabled for divisions, you can change the division. All records that are associated through a master-detail relationship are automatically transferred to the new division as well. For example, contacts and opportunities are transferred to the new division of their associated account, and detail custom objects are transferred to their master record’s new division.
When editing other types of records, you can’t change the division setting.
Custom objects
When you enable divisions for a custom object, Salesforce initially assigns each record for that custom object to the global division.
When you create a custom object record:
If the custom object is enabled for divisions, the record adopts your default division.
If the custom object is on the detail side of a master-detail relationship with a divisions-enabled custom object, the record adopts the division of the master record.
Relationships
If you convert a lookup relationship to a master-detail relationship, detail records lose their current division and inherit the division of their master record.
If you convert a master-detail relationship to a lookup relationship, the division for any detail records is determined by the previous master record.
If you delete a master-detail relationship, the division for any detail records is determined by the previous master record.
See Also:
Setting Up Divisions
Creating and Editing Divisions
Change the Default Division for Users
Changing Your Working Division
Transferring Multiple Records Between Divisions
Reporting With Divisions
Administrator tip sheet: Getting Started with Divisions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_division.htm&language=en_US
Release
202.14
