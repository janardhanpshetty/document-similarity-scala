### Topic: Learn how to enable / disable Report Subscriptions for your Organization.
A new feature has been released in 
Spring '15
 to allow for scheduled report notifications to be received without consuming the Organization-wide quantity. Once enabled, these Personal Analytics Notifications can be set up using the "Subscribe" button on the report itself. If this button isn't displayed for your reports, there may be additional settings required to make them available.
Resolution
In order to ensure that the Personal Analytics Notification feature can be used, we need to make sure that all of the relevant settings are enabled. There are two key features to be enabled. Without both being enabled, the feature will be inaccessible.
 
1. Enable report notification subscriptions for all users.
Clickpath: 
Setup
 | 
Customize
 | 
Reports & Dashboards
 | 
Report Notifications.
2. In-App Notifications
Click path: 
Setup
 | 
Mobile Administration
 | 
Notifications
 | 
Settings.
Note
: Report Subscription Notifications are 
limited to 5 subscribed reports per User
. This limit is hard coded and cannot be increased.
