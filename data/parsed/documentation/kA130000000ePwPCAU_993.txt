### Topic: Wave Analytics, Reports, and Dashboards Resources | Salesforce
Wave Analytics, Reports, and Dashboards Resources | Salesforce
Wave Analytics, Reports, and Dashboards Resources
In addition to online help, Salesforce creates video demos, guides, and tip sheets to help you learn about our features and successfully administer Salesforce.
Wave Analytics
For End Users
For Admins
Guides and Tip Sheets
Wave Analytics Setup Guide
Wave Analytics Security Implementation Guide
Wave Analytics Data Integration
Getting Started with Wave Replication Tipsheet
Videos
Create the Sales Wave App, Part 1: Using the Configuration Wizard
Create the Sales Wave App, Part 2: Upload the Quota CSV File and Schedule a Dataflow
Reports and Dashboards
For End Users
For Admins
Guides and Tip Sheets
Getting Started with Salesforce Reports and Dashboards
Analytics Workbook
Reports Tab
Using the Drag-and-Drop Report Builder
Do your big reports take a long time to return data?
Report Formula Summary Functions
FAQ: Scheduling Reports
Tips for Creating Dashboards
One Dashboard For Many Viewers
Taking Advantage of Dynamic Dashboards
Sample CRM Dashboards
What Are Joined Reports?
Using Cross Filters in Reports
Using Bucket Fields
Videos
Tips for Scheduling Reports
Make Data-Driven Decisions with Reports and Dashboards
An Overview of Dashboards
Getting Dashboards to Display the Right Data
Analytics Folder Sharing
Building Matrix Reports
Using Cross Filters in Reports
Getting Started with Buckets
Getting Started with Report Builder
Introducing Joined Reports in Salesforce
Making Your Reports Run Faster
Report Notifications
 
Embedding Charts Anywhere
 
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=analytics_resources.htm&language=en_US
Release
202.14
