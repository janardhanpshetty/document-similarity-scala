### Topic: If a Contact is related to at least one Campaign, this Campaign will show up on the Primary Campaign Source field when creating an Opportunity from that Contact's detail page.
Currently, if a Contact is related to at least one Campaign, this Campaign will show up on the Primary Campaign Source field when creating an Opportunity from that Contact's detail page.
When the Opportunity record is saved this Campaign will show up under the Campaign Influence related list. This will happen regardless of whether or not Campaign Influence is active or not and will only happen when the Opportunity is created from the Contact's page.
Resolution
The only way to prevent this from happening is to make sure that the Primary Campaign Source field is blank when an Opportunity is created, this has to be done manually.
How is this different from activating Campaign Influence?
When Campaign Influence is enabled, Campaigns will be automatically associated to a Opportunity if one of the members of the Campaign is also a Contact Role on the Opportunity and this will continue to happen as long as you keep adding Contact Roles to the Opportunity.
If Campaign Influence is disabled, Campaigns will not be added to the Campaign Influence related list when you add Contact Roles to the opportunity but they will be added when creating a new Opportunity.
