### Topic: sfdcDigest Transformation | Salesforce
sfdcDigest Transformation | Salesforce
sfdcDigest Transformation
The sfdcDigest transformation generates a dataset based on data that it extracts from a Salesforce object. You specify the Salesforce object and fields from which to extract data. You might choose to exclude particular fields that contain sensitive information or that aren’t relevant for analysis.
When you upload the dataflow definition file, Wave Analytics validates access to Salesforce objects and fields based on the user profile of the user who uploads the file. If the user profile does not have read access to a field or object, the upload fails.
At run time, Wave Analytics runs the dataflow as the Integration User. Wave Analytics validates access to Salesforce objects and fields based on the user profile of the Integration User. For example, if the dataflow tries to extract data from a custom field on which the Integration User does not have read access, the dataflow job fails.
Note
The Integration User is a preconfigured user that is created when Wave Analytics is enabled in your organization. If you or the Integration User need permission on a Salesforce object or field, ask the administrator to grant access.
For more information about preconfigured users in Wave Analytics, see the 
Wave Analytics Security Implementation Guide
.
Example
Let’s look at an example. You would like to create a dataset that contains all opportunities from the Opportunity object.
You create the following dataflow definition.
{    
   "Extract_Opportunities": {        
      "action": "sfdcDigest",        
      "parameters": {            
         "object": "Opportunity",
         "fields": [                
            { "name": "Id" },                
            { "name": "Name" },
            { "name": "Amount" },
            { "name": "StageName" },                
            { "name": "CloseDate" },
            { "name": "AccountId" },
            { "name": "OwnerId" },
            { "name": "OpportunitySupportTeamMembers__c" }
         ]  
      }    
   },    
   "Register_Opportunities_Dataset": {        
      "action": "sfdcRegister",        
      "parameters": {            
         "alias": "Opportunities",            
         "name": "Opportunities",            
         "source": "Extract_Opportunities"        
      }    
   }
}
Considerations When Using the sfdcDigest Transformation
Consider dataset storage limits when extracting data. For example, a dataset can contain a maximum of 5,000 fields, so be selective when choosing fields. See 
Wave Analytics Limits
.
The sfdcDigest transformation runs a SOQL query to extract data from a Salesforce object, and so is subject to SOQL limits. If the query exceeds any of these limits, it may return no results or cause the dataflow job to fail. For example, The length of the SOQL query cannot exceed 20,000 characters. To reduce the SOQL query length, consider breaking up the extract into two or more sfdcDigest transformations and then use the augment transformation to combine the results. For example, you might create one sfdcDigest transformation to extract half of the fields and create another sfdcDigest transformation to extract the remaining fields. See 
SOQL and SOSL Limits
.
Extracting Incremental Changes to Data (Pilot)
You can configure the sfdcDigest transformation to extract only records that changed in a Salesforce object since the last dataflow run. Use incremental extraction to decrease the time required to extract the data.
Filtering Records Extracted from a Salesforce Object
Add a filter to the sfdcDigest transformation to extract a subset of all records from a Salesforce object. You can filter records to reduce the number of extracted and processed records, exclude records that contain irrelevant or sensitive data, and increase dataflow performance.
Overriding Salesforce Field Metadata
You can override the field metadata that the sfdcDigest transformation extracts from a Salesforce object to make the data appear differently in a dataset. For example, Wave Analytics can add a default value to records that have missing values for a field.
Unsupported Salesforce Objects and Fields in Wave
The sfdcDigest transformation can’t extract data from all Salesforce objects and fields. Consider these limitations before configuring the extraction of Salesforce objects.
sfdcDigest Parameters
When you define an sfdcDigest transformation, you set the action attribute to 
sfdcDigest
 and specify the parameters for the object and fields that you want to extract. Optionally, you can also specify parameters to filter the records extracted from the Salesforce object.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_integrate_salesforce_extract_transformation.htm&language=en_US
Release
202.14
