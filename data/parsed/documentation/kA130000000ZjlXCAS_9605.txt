### Topic: Considerations for Deploying Flows with Packages | Salesforce
Considerations for Deploying Flows with Packages | Salesforce
Considerations for Deploying Flows with Packages
Flows can be included in both managed and unmanaged packages. Before you deploy one, understand the limitations and behaviors of packages that contain flows.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Component Dependencies
If you plan to deploy a flow with packages, consider limitations in migration support. Make sure your flows reference only packageable components and fields.
Referential integrity works the same for flows as it does for other packaged elements.
If any of the following elements are used in a flow, packageable components that they reference aren’t included in the package automatically. To deploy the package successfully, manually add those referenced components to the package.
Apex
Email Alerts
Post to Chatter
Quick Actions
Send Email
Submit for Approval
For example, if you use an email alert, manually add the email template that is used by that email alert. 
Flow Status
You can package only active flows. The active version of the flow is determined when you upload a package version. If none of the flow’s versions are active, the upload fails.
Updating Packages
To update a managed package with a different flow version, activate that version and upload the package again. You don’t need to add the newly activated version to the package. However, if you activate a flow version by mistake and upload the package, you’ll distribute that flow version to everyone. Be sure to verify which version you really want to upload.
You can’t include flows in package patches.
Other Limitations
If you register your namespace after you referenced a flow in a Visualforce page or Apex code, don’t forget to add the namespace to the flow name. Otherwise, the package will fail to install.
If someone installs a flow from a managed package, error emails for that flow’s interviews don’t include any details about the individual flow elements. The email is sent to the user who installed the flow.
Flow triggers aren’t packageable.
The Process Builder has superseded flow trigger workflow actions, formerly available in a pilot program. Organizations that are using flow trigger workflow actions can continue to create and edit them, but flow trigger workflow actions aren’t available for new organizations.
In a development organization, you can’t delete a flow or flow version after you upload it to a released or beta managed package.
See Also:
Considerations for Installed Flows
Create a Package
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_deploy_package.htm&language=en_US
Release
202.14
