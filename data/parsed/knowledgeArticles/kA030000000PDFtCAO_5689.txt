### Topic: The feature will permit your environment to use multiple currencies for currency fields. This feature is not enabled by default and is activated by Salesforce Support upon request.
Below are some aspects of the 
Multi-Currency
 feature that need review before this feature can be activated for your Organization.  If you would like to proceed with the activation of this feature, please open a Support case, including answers from items 1 - 7 below. 
Resolution
To our customers,
Thank you for your interest in the Multi-Currency feature for your environment. Before proceeding, there are some 
key implications
 in doing so that we want to make sure you are aware of. 
 
Important: 
Please note that the overall completion time for the activation of this feature could take up to 
3 business days
.
Please reply
 to the Support rep handling your case OR submit a case comment with the following information:
 
1. What is the Organization ID of the production or sandbox instance (please specify if sandbox or production) where you need the feature to be activated?  (Navigate to 
Setup
 | 
Company Profile
 | 
Company Information
):  
2. You must be an active System Administrator authorized on behalf of this Organization to request this feature. Please specify the username you're using for this Organization. If you're a Partner, logging this case on behalf of a customer, please also grant login access for 3 days as a proof of authorization (please note that support is not going to use the login access for this activation but is just to validate the request).
3. Please confirm that you understand that once Multi-Currency is activated, it CAN'T be deactivated? (we encourage testing this first in a trial/DEV organization or a sandbox):   
4. Please confirm the date and time (including timezone) you would prefer this activation to take place.
5. Please confirm that you consent to the lockout of this organization for a certain period of time as described below (please note that estimations are only indicative)?
6. Please note that activating the Multi-Currency feature will stamp all existing records with one currency code that will be the default currency stamp you have selected. This Organization default can be altered after the process is completed, but will only impact existing records if done so through record updates.
Note:
 Have you set the default currency to the desired value? If not, pleas navigate to 
Setup
 | 
Company Profile
 | 
Company Information
 | 
Edit
 | 
Currency Locale
.
Please review the supported currencies and ISO codes in the article 
"
Supported currencies
,"  and confirm the ISO code you select.
7. Have you followed the click path below and allowed support to activate Multi-Currency checking the box? 
Go to
 Setup
 | 
Administration Setup
 | 
Company Profile
 | 
Company Information
 | Populate “Allow Support to Activate Multiple Currencies,”  and then click 
Save. 
By enabling this check box, you are acknowledging that you are ready for multi-currency to be activated, but does not override the designated date/time for activation.
CAUTION: 
THIS PROCESS LOCKS THE ORG when we activate Multi-Currency, preventing users from logging in or any integrations from processing.
**For small orgs, we can activate anytime as the org lock time will be typically less than 20 minutes.
**For medium sized orgs, we can activate after 5pm your time zone and the org lock time can be typically up to an hour. We will coordinate with you before proceeding.
**For large orgs, we can activate after Friday 5pm your time zone and the org lock time can be up to several hours.  We must schedule these activations before end of day Wednesday before the weekend of activation. We will coordinate with you before proceeding.
 
We cannot guarantee that any particular activation time frame will be available, especially on short notice. We will do our best to accommodate the time requested, but be aware that the activation process may be initiated up to 90 minutes after the scheduled time. To ensure a smooth transition to multiple currencies, some activations for larger organizations may need to be scheduled a week (or more) ahead of time to avoid release windows, maintenances, etc. The case owner will work with you on scheduling the activation for a time that works for your business needs while also avoiding taking any risks by activating during a patch, maintenance window, etc. Since we are a global organization, we can accommodate activations during off-business hours as well. Please communicate all scheduling inquiries to the case owner.
 
Please read below as some aspects of the Multi-Currency feature need to be reviewed before this feature is activated in your Organization.
 
1. When Salesforce converts your Organization to Multi-Currency (MC), we must stamp all existing records with one currency code. Therefore, if you've entered Opportunities in MC (for example, MC is not activated and all reps in the US have entered USD and all reps in Europe have entered amounts in EUR) you will need to switch all of these to one currency first. This type of conversion can be supported via a SFDC paid implementation service.
2. Be aware that when you add a currency to your Organization's list of supported currencies, you cannot then delete that currency from the Administrator's screen. This is a purely cosmetic issue. You may deactivate currencies so end users will not see them when creating new Opportunities, etc. However, the Administrator will always see inactivated currencies in the manage currencies list. SFDC recommends keeping this in mind when doing initial configuration. Test with currencies that you will eventually use in the business.
3. All currency fields in the Organization will now display with the ISO code of the currency preceding the number. For example, $100 will change to USD 100. 
4. Currencies are automatically updated when conversion rates change.  The rates are not automatically maintained.  They must be updated manually.
5. Primary currency will show up as before but now an additional value in parentheses will show the converted amount in the secondary currency. The primary currency will be the corporate default currency unless overridden per record using fields such as Account Currency, Opportunity Currency, etc. The (converted) amount shown is always your personal default currency. Please note, the fields "AccountCurrency" and "OpportunityCurrency" are only available in "New: and "Edit" mode.
6. Objects with converted currency fields include Opportunities, Opportunity Products, Opportunity Product Schedules, Campaign Opportunity fields, and reports related to these objects and fields.  MC fields are on these Page Layouts by default.
7. By default only your current exchange rate settings in Salesforce are used for converting ALL amounts; changing an exchange rate, and all records, even closed Opportunities will show updated converted amount for secondary currency. 
Note:
 You can track historical exchange rates using Advanced Currency Management. When enabled by you, it allows the maintaining of a list of exchange rates and which date ranges they apply to. Converted "Amount" field on Opportunity, regardless of stage, will display based on exchange rate for the given Close Date. Changing the "Close Date" will impact (converted) amounts if it changes to a different exchange rate period.  Also, 
dated exchange rates
 are not used in forecasting, currency fields in other objects, or currency fields in other types of reports.
8. Opportunity records will have new standard required picklist field called "Opportunity Currency" visible only in the "New" and "Edit" modes. The selected currency for each Opportunity is used for the primary amount field.
9. End users will see a "Currency" option in 
Setup
 | 
Personal Information,
 which they may 
Edit
 and set to determine the currency for converted amounts. Switching that default will change the currency for converted amounts real time as they view or report on records.
10. Reports will show primary amounts per the Opportunity currency for each included record. Reports will show converted amounts in the currency of the user running the report OR the currency set using the picklist in the bottom of the report criteria page.
