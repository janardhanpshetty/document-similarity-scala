### Topic: We have lot of customer who are setting up https custom domain feature. When they upload the certificate on the Domain page with a CA-signed certificate that already supports that domain what will happen in the backend ?
We have lot of customer who are setting up https custom domain feature.
When they upload the certificate on the Domain page with a CA-signed certificate that already supports that domain what will happen in the backend ?
 
Resolution
When the customer uploads the certificate, it gets uploaded into the database of their instance using multiple levels of encryption (well, the private key has multiple levels of encryption, that is).
After the certificate is linked to a domain and the domain to one or more custom URLs, it gets published onto the custom https domains load balancers that are in front of our SR1 and SR2 instances in Chicago and Washington. Eventually, it will get published to the Tokyo and London datacenters, too (safe harbour).
The private key is encrypted at rest and in motion
