### Topic: Account Teams | Salesforce
Account Teams | Salesforce
Account Teams
An account team is a team of users who work together on an account. For example, an account team can include an executive sponsor, dedicated support representative, and project manager. Using account teams makes it easy to track collaboration on accounts.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Account teams aren’t the same as opportunity teams, although they have the same team member roles.
You can build an account team for each account that you own. When selecting an account team member, specify the role that the person plays on the account. Depending on your sharing model, you can specify each account team member’s level of access to the account and any contacts, opportunities, or cases associated with that account. That way, you can give some team members read-only access and others read/write access.
You can set up a default account team that includes the people who you normally work with on your accounts. You can choose to automatically add this default account team to all your accounts.
In a custom list view, you can filter account lists by the account teams in which you are a member. When creating or editing a custom list view for accounts, select the 
My Account Teams
 filter.
 
In account reports, you can filter accounts by the account teams in which you are a member.
Add Account Team Members
An account record owner or users above the owner in the role hierarchy who have read access on the account can add team members but not edit them. If they have edit access on the account, they can also edit or delete team members.
Considerations for Removing Account Team Members
Before you delete a member from an account team, make sure you’re aware of these considerations.
Guidelines for Setting Up Default Account Teams
Save time by setting up a default account team--a group of coworkers you typically work with on account. Then add the team to accounts that you own.
Account Team Fields
An account team member has the following fields, listed in alphabetical order. Your page layout and field-level security settings determine which fields are visible and editable.
See Also:
Guidelines for Setting Up Default Account Teams
Accounts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=accountteam_def.htm&language=en_US
Release
202.14
