### Topic: Describes the process to create and assign Permission Sets for Wave Analytics Platform.
Wave products all follow a similar structure of "Permission Set License" plus "Permission Set" to manage access to the Wave environment. Once Permission Set Licenses for Wave Analytics Platform users have been assigned, the next step is to create and assign Permission Sets to target users.
Before Creating Permission Sets
Review your users' needs to determine the structure of Permission Sets you'll be creating. Information regarding Wave User types is 
here
.
Permissions
Generally, Wave Analytics Platform users will fall into one of two roles: Manager and User.
A Manager Permission Set will need the "Manage Wave Analytics" Permission.
"Manage Wave Analytics" - Access all Wave Analytics features.
A User Permission Set will need the "Use Wave Analytics" Permission.
"Use Wave Analytics" - Use Wave Analytics and view the datasets, lenses, and dashboards that you have permission to view.
 
Other Permissions are available to fine-tune user capabilities:
"Create and Edit Wave Analytics Dashboards" - Create and edit Wave Analytics dashboards.
"Create Wave Analytics Apps" - Create Wave Analytics apps.
"Edit Wave Analytics Dataflows" - Edit the Wave Analytics dataflow and view errors related to it.
"Upload External Data to Wave Analytics" - Upload an external data file through the user interface to create a dataset.
"Download Wave Analytics Data" - Download screenshots and data in tabular format through the Wave Analytics user interface.
Resolution
Creating and assigning a Wave Platform Permission Set
Follow these steps to assign the needed Permission Set to target users:
1. In Setup under the Administer section, go to Manage Users, then the sub-section "Permission Sets". 
2. Click "New". 
3. Enter a Label. Something descriptive like "Wave Manager" or "Wave User" is recommended. 
4. The API Name should populate after you click out of the Label field. Enter a Description if you wish. 
5. Leave the User License field set to "--None--".
6. Click "Save". 
7. On the Permission Set screen, in the System section, click "System Permissions". 
8. Click "Edit". 
9. Select the needed permissions.
10. Click "Save". 
11. Click "Manage Assignments". 
12. Click "Add Assignments". 
13. Locate the target user(s) in the list and check the box for the user(s). 
14. Click "Assign".
Repeat these steps for each Permission Set.
Related Wave Setup Content:
Enable Wave Analytics
Assigning Permission Set Licenses for Wave Analytics
Create and Assign Wave Analytics Platform Permission Sets
Create and Assign Sales Wave Permission Sets
