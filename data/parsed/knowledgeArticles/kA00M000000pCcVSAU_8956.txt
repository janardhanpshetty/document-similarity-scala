### Topic: This article is an FAQ about the new Salesforce Lightning navigation model coming with the Winter '17 release.
What’s changing about navigation in Lightning Experience?
In the Winter ‘17 release, we’re delivering an improved navigation model in Lightning Experience. One of the primary drivers of this update is to bring the feature of app switching into Lightning. Your users will now be able to navigate more efficiently and also easily switch between apps. If you and your users know Salesforce Classic, the updated navigation model will feel very familiar.
In the improved navigation model, the existing vertical navigation menu on the left side of Lightning Experience will become a horizontal navigation bar at the top of the page (see screenshot above). Your users will be able to navigate to any items in the navigation bar just like before, but now they'll also be able to access other apps, including Lightning apps that you can brand and customize to help your users work more efficiently. For example, create a Lightning app for your finance department that includes all of the important items (including tabs) they’ll need to complete common tasks. You can customize the navigation bar color, brand it with a logo, and make the app available in the App Launcher for the user profiles associated with the finance department.
If you’re already using Lightning Experience in your org and have created navigation menus for one or more user profiles, these menus will be automatically converted to Lightning apps when your org is upgraded to Winter ‘17. This means that your menu names will be visible to end users after the Winter ‘17 release.
With Winter ‘17 also comes a redesigned App Launcher. After the Winter ‘17 release, the App Launcher will present the app’s name, logo, and description on an app tile. The description you use when you create an app (custom or connected) is what appears within the app tile. If your org has Salesforce Classic custom apps, the apps automatically work in Lightning Experience without any modifications.
 
Why is Salesforce making these changes?
You've spoken, and we've been listening. Based on feedback from customers, partners, and our ongoing research, we've made these changes to help your users be more productive. We believe the improved navigation model will save your users time and help them easily get to the items they use the most.
For example, in the improved navigation model your users can:
Navigate using item names instead of icons for easy recognition 
Complete actions and find recently accessed items and lists with a single click
Use a consistent and familiar navigation experience
 
As a Salesforce admin, can I opt out of these changes?
These changes are permanent and will be applied to all Lighting Experience orgs, including production and sandbox. Orgs can’t opt out.
 
Do these changes apply to both Lightning Experience and Salesforce1?
For Winter ‘17, the new navigation user interface applies only to the Lightning Experience.  
 
Which types of users does it apply to?
All users who have access to Lightning Experience will see the new navigation user interface when Winter ‘17 is released.
 
What kind of training will be available to users and admins about these changes?
We’ve created in-product training to help all users understand the changes. Visual indicators will point out the key navigation features and assist users as they experience it for the first time.  
Likewise, for admins, we’re providing in-product training for key tasks within the Setup screens for configuring and managing Lightning apps.
 
As a Salesforce admin, what do I need to do to get ready for these changes?
 
Your existing custom apps from Salesforce Classic will automatically work in Lightning Experience. You’ll want to edit your Classic apps once you’re using Winter ‘17 so you can take advantage of the new Lightning Experience navigation features, like logos, custom navigation bar colors, and custom Lightning pages. But you aren’t required to make changes and there will be no disruption to anyone using your Classic apps as a result of the new navigation model.
  
If you’re already using Lightning Experience in your org take the following steps:
 
1
 Review your current apps to verify that each description field contains information that is meaningful to your users. The description fields will be visible to your users in the new App Launcher following the Winter ‘17 release. For example, here's what our Sales app tile looks like:
2
 Approximately two weeks before your org is upgraded to Winter ‘17, we’ll automatically change all navigation menu names to “Lightning.” After this change, we recommend that you modify these temporary names and navigation menu descriptions to the actual names and descriptions you want associated with the Lightning apps that replace the navigation menus. The names you assign are how users will identify the Lightning apps in the user interface.
3
 After the Winter ‘17 release, you should edit your apps, to ensure they have the proper name, description, logo, navigation bar color, and other key settings.
NOTE: 
You can edit your Classic apps once you’re using Winter ‘17 to take advantage of the new Lightning Experience navigation features, but it’s not a requirement.
 
As a Salesforce admin, what changes can I expect when the Lightning Experience navigation menus that I created are converted to Lightning apps?
You can expect the following changes:
 
1 Name
Users see the name of the current Lightning app on the left side of the navigation bar. When we convert your navigation menus to Lightning apps, your original menu names change to "Lightning". You can easily change the “Lightning” name to something else in Setup.
2 Branding Image
After your navigation menu is converted to a Lightning app, you can brand it with a custom logo. This custom logo will appear in two places; (1) Lightning header and (2) App Launcher. 
If you don't supply a custom logo, by default, the 
Lightning header logo displays the Salesforce cloud icon (as seen in the above screenshot)
App Launcher logo displays a blue square with the letters “Li”
3 Branding Color
You have the option to specify the color of an app’s navigation bar. By default, the navigation bar is the friendly blue color you're used to seeing in Salesforce.
 
If I have more than one navigation menu, will all of my navigation menus be renamed to “Lightning”?
Yes. The name of all navigation menus will be “Lightning”.
 
When will my custom navigation menus be renamed to “Lightning”?
 
The process to convert the navigation menus to Lightning apps will begin according to the following schedule: 
  
Instance
Name Change Date
NA44, NA45
September 5, 2016
All other instances
September 19, 2016
 
Between the name change date above and the Winter ‘17 release for your instance, you’ll be able to edit your custom navigation menu names in the Lightning Experience Setup UI before they become visible to your end users (Instructions to update your navigation menu names follow below.).
 
This new edited name is what will display in the navigation bar when Winter ‘17 is released to your org. The specific time and date for your org’s Winter ‘17 upgrade window will be listed on 
trust.salesforce.com
.
 
How do I update my custom navigation menu names to something more meaningful after they’ve been renamed to “Lightning?”
 
To change your custom navigation menu names take the following steps:
 
1.
 From Salesforce Setup, enter “Navigation Menus” in the Quick find box, then select Navigation Menus. Notice the name of the navigation menu is renamed “Lightning”. The developer name of the navigation menu is unchanged and allows you to identify the navigation menu you’re renaming.
2.
 Click the dropdown icon of the navigation menu you’d like to rename.
3. 
Click 
Edit. 
4. 
Enter the new name. You can also update the developer name if desired. We recommend also updating the description as the App Launcher will use the name and description to create an app tile after the Winter ‘17 release.
5. 
Click 
Next.
 
6. 
Click 
Next. 
7. 
Click 
Save & Finish.
The updated navigation menu name now appears in Setup.
 
Repeat these instructions for all your custom navigation menus.
 
NOTE:
Consider the length of the new name when changing it. Remember that your app's  name is visible to your users. It appears on the App Launcher and in the navigation bar when the app is active.
If you create any new navigation menus within this two-week window, we won’t automatically rename them to “Lightning.”  For these navigation menus, we recommend that you name them as if you were naming a Lightning app.
 
Where did the tabs I added go from Customize my Tabs?
 
In Salesforce Classic, end-users can customize the tabs that are displayed in each app they are authorized to  access. Customize My Tabs functionality is not be supported in Lightning Experience. So, any customize tab settings that the end-user has made to the Salesforce Classic app won't be reflected in Lightning Experience.
Resolution
