### Topic: This article explains all the supported MIME type for E2S (Email to Salesforce) E2C (Email to Case)
What MIME types does E2X support?
E2X means any type of inbound email services such as Email to
Salesforce, Email to Case and such
MIME
 stands for 
Multi-purpose Internet Mail Extensions
.
MIME types form a standard way of classifying file types on the Internet. Internet programs such as Web servers and browsers all have a list of MIME types, so that they can transfer files of the same type in the same way, no matter what operating system they are working in.
A MIME type has two parts: a type and a subtype. They are separated by a slash (/).
For example:
The MIME type for Microsoft Word files is 
application 
and the subtype is 
msword
.
Together, the complete MIME type is 
application/msword.
Resolution
Currently, following MIME types are supported by inbound E2X services:
 
text/plain
text/html
multipart/*
application/octet-stream
application/*
text/*
image/*
audio/*
video/*
