### Topic: Outbound Change Set Validation Errors | Salesforce
Outbound Change Set Validation Errors | Salesforce
Outbound Change Set Validation Errors
If you receive an error about cross-version validation, the org used to create the outbound change set is running on a different version than the destination org. This error typically occurs during upgrades, because orgs may be upgraded at different times due to Salesforce staggered releases. If you receive this error, you can only deploy those components that are compatible between versions.
See Also:
Create an Outbound Change Set
Upload an Outbound Change Set
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=changesets_outbound_version_errors.htm&language=en_US
Release
202.14
