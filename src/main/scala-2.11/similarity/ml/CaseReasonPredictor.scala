package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.NaiveBayes
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
 * Created by jshetty on 9/15/16.
 * spark-submit --class similarity.ml.CaseReasonPredictor --master local[12] /Users/jshetty/spark-applications/documentsimilarity/target/scala-2.11/documentsimilarity_2.11-1.0.jar
 */
object CaseReasonPredictor {

  // Udf
  val getConcatenated = udf((first: String, second: String) => { first + " " + second })

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder.master("local[12]").appName("Multi-Class Classification Pipeline").getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext
    sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val inputPath = "/Users/jshetty/casedata/data/reason_subject_description/reason_subject_description_1year.csv"
    val input = spark.read.option("header","true").option("delimiter", "`").csv(inputPath).cache()

    val stopWordsPath = "/Users/jshetty/spark-applications/documentsimilarity/input/stopwords/english_custom_stopwords.txt"
    val stopWords = sc.textFile(stopWordsPath).collect()

    //val numFeatures = 270    // Word vector dimensionality
    val numFeatures = 1000
    val topK = 270
    val nGrams = 3

    // Pre-processing subject + description
    val textInput = input.withColumn("text", getConcatenated(col("SUBJECT"), col("DESCRIPTION")))

    // Indexer to convert String labels to Double
    val labelIndexer = new StringIndexer().setInputCol("REASON").setOutputCol("label").fit(textInput)
    val tokenizer = new RegexTokenizer().setInputCol("text").setOutputCol("words").setPattern("\\W")
    val textFiltered = new StopWordsRemover().setStopWords(stopWords).setInputCol("words").setOutputCol("wordsFiltered")
    val grams = new NGram().setInputCol("wordsFiltered").setOutputCol("wordsGrams").setN(nGrams)
    val hashingTF = new HashingTF().setInputCol("wordsGrams").setOutputCol("features").setNumFeatures(numFeatures)
    val selector = new ChiSqSelector().setNumTopFeatures(topK).setFeaturesCol("features").setLabelCol("label").setOutputCol("selectedFeatures")
    // Apply Naive Bayes
    val nb = new NaiveBayes().setLabelCol("label").setFeaturesCol("selectedFeatures")
    val labelConverter = new IndexToString().setInputCol("prediction").setOutputCol("predictedLabel").setLabels(labelIndexer.labels)

    val pipeline = new Pipeline().setStages(Array(labelIndexer, tokenizer, textFiltered,grams, hashingTF, selector, nb, labelConverter))

    // Split
    val Array(trainingData, validationData, testData) = textInput.randomSplit(Array(0.5, 0.2, 0.3))

    // Train model. This also runs the indexers.
    val model = pipeline.fit(trainingData)

    // Make predictions
    val trainPredictions = model.transform(trainingData)
    val validationPredictions = model.transform(validationData)
    val testPredictions = model.transform(testData)

    // Evaluation metric : Accuracy
    val evaluator = new MulticlassClassificationEvaluator().setLabelCol("label").setPredictionCol("prediction").setMetricName("accuracy")

    val accuracyTrain = evaluator.evaluate(trainPredictions)
    println("Accuracy on train data = " + accuracyTrain)
    val accuracyValidation = evaluator.evaluate(validationPredictions)
    println("Accuracy on validation data = " + accuracyValidation)
    val accuracyTest = evaluator.evaluate(testPredictions)
    println("Accuracy on test data = " + accuracyTest)

    // Evaluation metric: weighted f1 measure
    val evaluatorF1 = new MulticlassClassificationEvaluator().setLabelCol("label").setPredictionCol("prediction").setMetricName("f1")

    val accuracyTrainF1 = evaluatorF1.evaluate(trainPredictions)
    println("F1 weighted on train data = " + accuracyTrainF1)
    val accuracyValidationF1 = evaluatorF1.evaluate(validationPredictions)
    println("F1 weighted on validation data = " + accuracyValidationF1)
    val accuracyTestF1 = evaluatorF1.evaluate(testPredictions)
    println("F1 weighted on test data = " + accuracyTestF1)

   // Select test example rows to display.
    testPredictions.select("predictedLabel", "label", "features").show(5)

   spark.stop()
  }
}
