### Topic: The article provides information on the change for default Saleforce 1 navigation menu
With 194 Release, there is a slight change in the arrangements of the navigation menu for any new organization to Salesforce when using Salesforce1.
For New Orgs, these items are automatically included in 
all of the 
Salesforce1
 apps. 
Feed
Today
Dashboards 
Tasks 
Notes
Smart Search Items 
People 
Groups 
Reports 
Events 
Approvals.
Note: the order may slightly differ bases on different Mobile OS or SF1 version. 
Organizations that existed prior to Spring ’15 are not affected by this change.
To modify the navigation menu, see “
Customize the 
Salesforce1
 Navigation Menu
” in the 
Salesforce
 Help
Resolution
