### Topic: This article provides detailed steps to mass disable portal users via Data Loader including exporting portal user records, set their IsPortalEnabled value to false, and performing an update via Data Loader using the newly saved file.
How can mass disabling of portal users be done?
Resolution
Outlined here are the steps to disable portal users using Data Loader.
Export the User details using Data Loader
Start Data Loader
Click Export
Login, click Next
Choose the User Object
Click Browse to set the name of the file and where its is going to be saved.
From Choose the query fields, select the following fields: 
Id
 ,
 
IsActive 
,
 IsPortalEnabled
Set conditions find User Records where the IsPortalEnabled field is set to TRUE.
Click Finish to perform the operation, and then click Yes to confirm.
Click Ok to close.
Open the exported file and change the value for IsActive and IsPortalEnabled columns from TRUE to FALSE and save the file. Make sure it is saved using the CSV format. 
 
After the file has been updated, you can now go back to the Data Loader to update the user records. 
Start Data Loader
Click Update
Login, click Next
Choose the User object again
Click Browse to select your CSV file.
Click Next.
Click OK.
Define how the columns in your CSV file map to Salesforce fields. Click Create or Edit a Map then click on Auto-Match Fields to Columns, click ok
Click Next
For every operation, the Data Loader generates two unique CSV log files; one file name starts with “success,” while the other starts with “error.” Click Browse... to specify a directory for these files.
Click Finish to perform the operation, and then click Yes to confirm.
Click Ok to close.
Note: Once a portal user has been disabled, it
 
cannot be re-activated
. If the user would need to be re-activated then a new portal user record would need to be created.
