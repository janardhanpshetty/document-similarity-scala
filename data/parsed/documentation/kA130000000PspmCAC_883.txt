### Topic: Automatically Accepting Related Records From a Connection | Salesforce
Automatically Accepting Related Records From a Connection | Salesforce
Automatically Accepting Related Records From a Connection
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To accept or reject records:
“Read” and “Edit” on the record
Rules for Automatically Accepting Related Records From a Connection
When you share records using Salesforce to Salesforce, you can also share child records. When you accept a record from a connection, or when your connection accepts a record from you, the child records can be automatically accepted provided that certain criteria are met. Similarly, when a child record is shared at a later time using the 
Manage Connections
 link in the related list, and the parent record has already been accepted, the child record can be automatically accepted.
In order for a child record to be accepted automatically, the following criteria must be met:
The parent record must already be accepted in the organization.
The record must be related as a child to the parent record.
The child record cannot have two master records.
The child record cannot have multiple relationships to the same parent object.
The connection owner must be an active user.
The owner of the parent record must be an active user.
Important
When child records are accepted automatically, there may be a slight delay before they are visible in the related list of the parent record. Additionally, when a child record is shared using the 
Manage Connections
 link in the related list of a parent record, there may be a slight delay before the Sent Connection Name displays.
If a child record does not meet the criteria for automatic acceptance, it will need to be manually accepted from its object tab. For example, an opportunity record will need to be accepted from the Opportunities tab. A child record that needs to be manually accepted will display with the parent record name so that it can be properly assigned. For more information, see 
Accepting Records Shared from a Connection
.
See Also:
Subscribe to Fields in Salesforce to Salesforce
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=business_network_auto_accept.htm&language=en_US
Release
202.14
