### Topic: It is expected and by design that performing an update to record's OwnerId field via the API, using Data Loader for example, would not send out the standard notification email to the new record owner. It is recommended to manually notify new owners of record assignment via the API but as an alternative you may use a workflow rule and email alert to automate sending a notification.
It is not possible to trigger the standard "Send Notification Email" alert that's available when changing record ownership via the user interface when updating record ownership via the API.
This is expected behavior and by design to prevent potentially sending users an unmanageable number of email notifications as the result of performing large data operations that include updating record owners. It's not advisable or scalable to send an excessive number of emails to users when mass updating record owners.
Instead, consider sending a single email manually to each user to let them know that record ownership has been transferred to them as the result of an update operation. Users may then 
Create a List View
 or 
Create a Report
 to view and work through the newly transferred records from within Salesforce.
If you're updating a manageable number of records an administrator may choose to create a workflow rule and email alert action to automatically send notification emails upon updating record owners via the API.
 
Resolution
As an example, the following workaround uses a custom checkbox and/or restricts the workflow to only be triggered when the update is done by a specific user or users assigned a specific profile.
Note:
 It is recommended to use these instructions as a general guide for your own implementation and remember to always test new functionality in your sandbox before implementing it in production!
1. 
Create a custom field
 on your desired object using the following details:
- Data Type: Checkbox
- Field Label: Notify New Owner
- Default Value: Unchecked
- Set visibility to only the profiles that will be performing the update operations with the Data Loader
- Optionally, you can add the field to page layouts but since this will only be used to trigger workflows it's recommended to not add it to page layouts
2. 
Create a Workflow Rule
 on the object using the following details:
 
- Rule Name: Trigger Email Notification to new owners
- Description: Used to trigger email notifications to new record owners when a user with the System Administrator profile or specific users update the OwnerId field and Notify_new_owner__c is checked via the API
- Evaluate the rule when a record is: created, and every time it's edited
- Run this rule if the following: formula evaluates to true
AND(Notify_New_Owner__c=TRUE,
ISCHANGED(OwnerId))
Note:
 If the checkbox is checked and users change a record owner and select to "Send Notification Email" upon doing so, duplicate assignment notifications may be sent. To prevent this you can add additional criteria such as the following to only trigger the workflow when the update is performed by user assigned a profile or by specific users:
AND(Notify_New_Owner__c=TRUE,
ISCHANGED(OwnerId),
OR($User.ProfileId='
00e30000001HH9X
',
$User.Id='
00530000003xqAa
'))
Where 
00e30000001HH9X
 is the Id for a profile and 
00530000003xqAa
 is the Id for a specific user in your org. For more details on locating your org's own unique Ids for users and profiles see the article 
How to find a User or Profile Id
.
 
3. Add a Workflow Action: 
New Email Alert
 using the following details:
- Description: New record owner email alert for changing OwnerId
- Email Template: Create a new or select an existing email template to notify the new record owner on assignment. See 
Create Text Email Templates
 for more details. For reference, the standard record assignment email notification can be re-created as a template using the following details:
 
- Choose the type of email template you would like to create: Text
- Choose an appropriate folder to save the template in
- Check Available for Use
- Email Template Name: Record assignment notification
- Description: Used in the "Trigger Email Notification to new owners" Workflow rule's email alert
 
Note:
 For the purpose of demonstrating merge fields we're assuming that the workflow's email alert message template is for the Lead object:
 
Subject: 
<Lead>
: 
{!Lead.Name}
 has been assigned to you.
Email Body: 
<Lead>
: 
{!Lead.Name}
, 
{!Lead.Company}
 has been assigned to you.
To view the details of this 
<lead>
 in salesforce.com click on the following link:
{!Lead.Link}
Substitute out occurrences of 
<Lead>
 with your appropriate object's name and swap out your desired object's merge fields for those in 
bold
 above. When you're satisfied be sure to Send Test and Verify Merge Fields to confirm your template is correct.
 
- For Recipient Type select Search: Owner and move the Object Owner recipient over from the Available Recipients section to Selected Recipients
- Optionally, add additional email addresses to be notified
- From Email Address: Current User's email address and Save
Click Done for Step 3 of 3: Specify Workflow Actions and finally click to Activate your Workflow Rule.
Alternatively, consider promoting the Idea: 
Data loader and notification e-mails
 on the IdeaExchange so that we may see the ability to set this preference on update added to Salesforce functionality with a future release.
