### Topic: Responding to License Manager Requests | Salesforce
Responding to License Manager Requests | Salesforce
Responding to License Manager Requests
Available in: Salesforce Classic
Available in: 
Developer
 Edition
Package uploads and installs are available in 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To respond to registration requests:
“Customize Application”
A license manager is a Salesforce organization that tracks all Salesforce subscribers installing a particular AppExchange package. Salesforce administrators can choose to designate another organization as the license manager for one of their packages. The license manager does not need to be the same organization as the one from which the package is managed. To choose another organization as the license manager, all you need is an email address (not a Salesforce username). If a Salesforce administrator selects to have a third-party license manager and enters your email address, you will receive a license management request in email.
To respond to a registration request:
Click the link in the license management request email. This displays the registration request in the requestor's Developer Edition organization.
Click 
Accept
 to complete the registration process. Alternatively, click 
Reject
 to decline the request and close the browser; this prevents you from using the link again.
Note
If you accept this request, you authorize Salesforce to automatically create records in your Salesforce organization to track information about this package. Choosing a license manager organization is permanent and cannot be changed.
Enter the username and password for the Salesforce organization you want to use to manage licenses for this package. A license manager can be any Salesforce organization that has installed the free License Management Application (LMA) from Force.com AppExchange.
Click 
Confirm
.
See Also:
Managing Licenses for Installed Packages
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=distribution_license_manager_requests.htm&language=en_US
Release
202.14
