### Topic: Searchable Setup Objects in Lightning Experience | Salesforce
Searchable Setup Objects in Lightning Experience | Salesforce
Searchable Setup Objects in Lightning Experience
Use global search while in Setup to find specific setup records, such as the Lead Source picklist or the Sales Rep profile. Global search differs from Quick Find, which finds pages within the Setup menu, such as Account Settings or Profiles.
Available in: Lightning Experience
The types of records you can search vary according to the edition you have.
Search in Setup is only an option for global search while you’re in Setup. While within Setup, enter a record name, and select the 
in Setup
 option in instant results or press Enter.
On the search results page, use the search scope bar beneath global search to see results only for a specific Setup object. Top Results includes results from the Setup object pages you use most frequently.
The following Setup objects are always shown in the search scope bar. You can’t customize the order.
Users
Profiles
Permission Sets
Objects
Fields
Groups and Queues
If you want to see results for a Setup object not shown, use the 
More
 drop-down to the right of the list. Here’s a list of all the searchable Setup objects.
Approval Post Templates
Approval Processes
Assignment Rules
Compact Layouts
Custom Buttons or Links
Custom Home Pages
Duplicate Rules
Email Alerts
Email Templates
Field Updates
Fields
Groups and Queues
Home Page Components
Permission Sets
Profiles
Objects
Roles
Static Resources
Users
Workflow Outbound Messages
Workflow Rules
Workflow Tasks
Here are the columns shown in search results. You can’t customize the columns. The Type column lists the type of setup record, such as Field. The Object field shows the Salesforce object, such as Contact.
Name
Type
Object
Last Modified Date
Last Modified By
Setup search results have certain restrictions.
You can’t sort or filter results.
You can only search by the API name of the setup record.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_fields_setup.htm&language=en_US
Release
202.14
