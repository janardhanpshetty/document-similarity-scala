### Topic: Despite having appropriate security controls in place to allow access to records in a sandbox environment, users may encounter an "Insufficient Privileges" message upon attempting to navigate to the record via the user interface, or an "insufficient access rights on object id" message upon attempting to modify or delete affected records via the API.
If users are unable to access a particular record or subset of records in a sandbox environment,  first ensure that they have appropriate access granted via security controls.
If the affected user(s) clearly have appropriate access, via the View/Modify all data permissions for example, and are encountering an "Insufficient Privileges" message upon attempting to access the data through the user interface, or are receiving an error message stating, "insufficient access rights on object id" upon attempting to modify or delete records via the API:
Check the system audit fields on the affected records by exporting the data from the sandbox environment or check them in production if you cannot view/export the data from sandbox.
Compare the created or last modified system audit field's date stamps to see if they correspond to the affected sandbox's refresh date.  If they correlate to one another the unexpected behavior may be the result of the affected records being created or modified during the sandbox refresh process.
Resolution
A sandbox is not a point-in-time snapshot of the exact state of your data. Furthermore, we recommend that you limit changes to your production organization while a sandbox is being created or refreshed. Setup and data changes to your production organization during the sandbox creation and refresh operations may result in inconsistencies in your sandbox. You might detect and correct some inconsistencies in your sandbox after it is copied or refreshed.
If this appears to be the scenario and the behavior is disrupting your business process and or testing log a case with Support.  For efficient case handling includes the following information for our team's understanding of the impact and further investigation:
1)        What % of your users is affected?
2)        Does this affect data integrity?
3)        Do you have a viable workaround?
4)        Does this affect critical application functionality?
5)        What business functionality are you unable to perform?
6)        Is this related to a new integration/roll out/ramp/etc.?
7)        Is there an estimated revenue cost to this issue?
8)        Are there impacts beyond these that are affecting your business
Be sure to include the Organization ID for the affected sandbox which can be found by logging into the environment and navigating to Setup | Administration Setup | Company Profile | Company Information.  In the Organization Detail section there is a Salesforce.com Organization ID field: which is your sandbox Salesforce Org ID.
Provide the affected user's name(s) and ensure that login access is established for Support.  This can be set by navigating to Your Name | Setup | My Personal Information | Grant Login Access and set the expiration in the Support field to an appropriate time frame.
Lastly, include links or Ids for the affected records and detailed steps users are taking up to the point where the messages are encountered.
