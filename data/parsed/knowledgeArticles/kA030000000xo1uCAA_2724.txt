### Topic: When an Automation is paused, instances that are currently running will continue to process. The "Paused" status only pauses future Automations.
When an Automation is paused, instances that are currently running will continue to process in Marketing Cloud Automation Studio. 
Resolution
Instances of Automation that have already begun will continue when the Automation is paused. The 
Paused Status
 only stops future Automations from running. For example, if you click 
Paused
 on an Automation that's on a "Waiting Step," it will not stop this instance from continuing to process.
If you wish to stop all Automations, click
 Stop
. This will prevent any Automation from processing by deleting current progress and removing the Automation from the system.
