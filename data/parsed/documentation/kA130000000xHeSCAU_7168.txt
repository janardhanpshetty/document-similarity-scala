### Topic: Run and Then Read a Dashboard | Salesforce
Run and Then Read a Dashboard | Salesforce
Run and Then Read a Dashboard
Click on a dashboard’s name to run it. Dashboard charts are interactive, so be sure to hover and click on them to get more info!
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
Refresh Dashboard Data
Click 
Refresh
 to load the latest data into the dashboard. The data is as current as the date and time displayed after 
As of...
 at the top right corner of the dashboard.
Schedule a Dashboard Refresh
In Enterprise Edition, Unlimited Edition, and Performance Edition, you can schedule dashboards to refresh daily, weekly, or monthly.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=rd_dashboards_run_read.htm&language=en_US
Release
202.14
