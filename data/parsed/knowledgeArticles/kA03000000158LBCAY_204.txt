### Topic: MobileConnect has a number validation check to ensure the phone numbers being imported are valid and sometimes while importing users will get an error that says "InvalidPhoneForCountryCode" for specific numbers.
A customer will will report that they are receiving an "InvalidPhoneForCountryCode" error for contacts being imported to MobileConnect within the import results file. We can check the validity of the numbers by using a Phone Number Parser.
Resolution
1. Ask the client to provide you with the import results file or some example numbers that are receiving the error.
2. In a browser tab, navigate to the below URL:
http://libphonenumber.googlecode.com/svn/trunk/javascript/i18n/phonenumbers/demo-compiled.html
  
3. Enter one of the numbers receiving the error in the "Specify a Phone Number" field.
4. Enter the country code being used in the "Specify a Default Country" field, if you are unsure of what value needs placed here, we hold all country codes on the SystemDB database on a table named "countrycode".
5. Click 
Submit
.
6. If the number is not valid an error message will display that says, "Invalid country calling code."
7. Share your findings with the client and let them know MobileConnect is intentionally not allowing these to be imported because they are not valid and cannot be sent to. 
