### Topic: Yes, users will be able to apply workflow and engagement actions on this content (similar to regular FB Posts, Comments, and Replies).
Can workflow and engagement actions be performed on comments made on unpublished (dark) posts?
Resolution
Yes, users will be able to apply workflow and engagement actions on this content (similar to regular Facebook Posts, Comments, and Replies).
