### Topic: Tagging Records | Salesforce
Tagging Records | Salesforce
Tagging Records
Available in: Salesforce Classic
Available in: 
All
 Editions except 
Database.com
User Permissions Needed
To edit tags on a record:
“Read” on the record
To rename or delete public tags:
“Tag Manager”
On the top right corner of the record detail page, click 
Add Tags
. If the record already has associated tags, click 
Edit Tags
.
In the 
Personal Tags
 or 
Public Tags
 text boxes, enter comma-separated lists of the 
tags
 that you want to associate with the record. 
Tags can only contain letters, numbers, spaces, dashes, and underscores, and must contain at least one letter or number.
As you enter new tags, up to 10 tags that have already been defined are displayed as auto-complete suggestions. As you type, the list of suggestions changes to show only those tags that match the prefix you have entered. To choose a suggestion, click it or use your keyboard arrow keys to select it and press the TAB or ENTER key.
Click 
Save
.
Tip
When you create or edit tags, you can press the ENTER key to save your changes or the ESC key to discard them.
Note
There are limits on the number of personal and public tags you can create and apply to records. For all editions, if you attempt to tag a record with a new tag that exceeds one or more of these limits, the tag isn't saved. If you reach a limit, you can go to the Tags page and delete infrequently used tags.
See Also:
Tags on Records
Removing Tags from Records
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tag_records.htm&language=en_US
Release
202.14
