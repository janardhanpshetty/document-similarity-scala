### Topic: This article would help you with the possible reason of error that you get while trying to subscribe/unsubscribe objects using S2S connection.
Why am I getting an error trying to subscribe/unsubscribe to objects using Salesforce to Salesforce?
Resolution
If there is an error upon trying to subscribe or publish objects to/from another Org using Salesforce-to-Salesforce, be sure to check whether State & Country picklists are enabled. Currently, Salesforce-to-Salesforce will not work with this enabled in one of the two Orgs.
