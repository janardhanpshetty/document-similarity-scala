### Topic: Escape Sequences | Salesforce
Escape Sequences | Salesforce
Escape Sequences
You can use the backslash character (\) to escape characters in column names and string values in a predicate expression.
You can use the \’ escape sequence to escape a single quote in a column name. For example:
‘Team\’s Name’ == "West Region Accounts"
You can use the following escape sequences for special characters in string values.
Sequence
Meaning
\b
One backspace character
\n
New line
\r
Carriage return
\t
Tab
\Z
CTRL+Z (ASCII 26)
\”
One double-quote character
\\
One backslash character
\0
One ASCII null character
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_datasets_predicate_escapesequences.htm&language=en_US
Release
202.14
