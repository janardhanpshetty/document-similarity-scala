### Topic: Disconnect Salesforce Authenticator (Version 2 or Later) from a User’s Account | Salesforce
Disconnect Salesforce Authenticator (Version 2 or Later) from a User’s Account | Salesforce
Disconnect Salesforce Authenticator (Version 2 or Later) from a User’s Account
Only one Salesforce Authenticator (version 2 or later) mobile app can be connected to a user’s account at a time. If your user loses access to the app by replacing or losing the mobile device, disconnect the app from the user’s account. The next time the user logs in with two-factor authentication, Salesforce prompts the user to connect a new authenticator app.
Available in: Both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To disconnect a user’s Salesforce Authenticator app:
“Manage Two-Factor Authentication in User Interface”
From Setup, enter 
Users
 in the 
Quick Find
 box, then select 
Users
.
Click the user’s name.
On the user’s detail page, click 
Disconnect
 next to the 
App Registration: Salesforce Authenticator
 field.
Click 
Disconnect
 next to the 
App Registration: One-Time Password Generator
 field.
Note
If you don’t click 
Disconnect
 for this field, the inaccessible app still generates valid verification codes for the account.
Users can disconnect the app from their own account on the Advanced User Details page. In personal settings, the user clicks 
Disconnect
 next to both the 
App Registration: Salesforce Authenticator
 and 
App Registration: One-Time Password Generator
 fields.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=disconnect_salesforce_authenticator_v2_or_later.htm&language=en_US
Release
202.14
