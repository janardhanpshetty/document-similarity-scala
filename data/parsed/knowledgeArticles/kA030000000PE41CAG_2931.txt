### Topic: This article attempts to explain why a user might not get Content or Chatter alerts and steps you can take to troubleshoot the issue locally.
Users don't receive Content Alerts (or Daily Digest) when content is updated or comments are added
Resolution
Sometimes users will state that they not receiving Content email alerts. This is usually an issue of education/training/awareness as this area of functionality may confuse new users who often do not notice the content alert emails or may incidentally have them delivered to a junk mail folder.
Be sure that your users are aware of the following:
 
Content Alert emails are sent from the following address: 
content@salesforce.com
Please have the user reporting not receiving the alerts to search their inbox and junk mail box for email address that content alerts are sent from e.g. 
content@salesforce.com
 .
Consider adding a whitelist for 
content@salesforce.com
 to the junk mail monitor for your organization or the individual reporting the issue.
Be aware that users with "Receive Salesforce CRM Content emails as daily digest" will only receive one email a day with any updates relevant to the user. This sometimes causes users to miss the updates as they are not sent with as much frequency as the live updates.
 
Users reporting issues receiving Content Alerts should verify that they understand how the feature is triggered. 
 
If you subscribe to a file
, you are notified when a viewer adds comments to the file or when a new version of the file is published.
If you subscribe to a tag
, you are notified of any newly published content that is associated to the subscribed tag (not when a new version is added to an existing document).
If you subscribe to an author
, you are notified when the author publishes new content (not when a new version is added to an existing document).
If you subscribe to a Library
, you are notified when new content is added to the workspace, including existing content that has been newly linked to your subscribed workspace (not when a new version is added to an existing document).
 
If you are still worried that your users are not receiving Alerts for content perform these simple test for verification- 
Step 1.
 
Edit the user record to meet the following:
 
Check " 
Receive 
Salesforce CRM Content
 email alerts
"
Uncheck " Receive Salesforce CRM Content emails as daily digest"
 so that email updates are sent upon live edits.
Step 2.
 Isolate 2 documents that the user is subscribed to and do the following tests
:
Add a new version to one document.
Add a comment to the other document.
Step 3.
 
Isolate a Library that the user is subscribed to and do the following test:
 
Add a new document to the workspace.
Step 4.
 
Have the user check their email looking for the Content Alert (be sure to have them check their junk mail folder as well) and run an Email Log for the dates when alerts should have been sent.
 
Email Log files will capture Content Alert emails and the delivery status.
To run an email log go to Setup > Administration Setup > Monitoring > Email Log files
Search the log file for the email address of the user who hasn't received content alerts,in the log files there should be two entries for the user and each content alert sent out for the user-- as the first send goes to the internal mail server--and the second is sent out to the user's mail server.
 
See the links below for more information on the following:
Requesting an Email Log
Viewing Email Logs
