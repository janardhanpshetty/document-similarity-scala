### Topic: A Chatter feed post trigger called feedPostSwarm on most SDOs causes Goals V2 migration to fail during step 2 of the migration.
Internal Only: Do not share with customers.
A Chatter feed post trigger called feedPostSwarm on most SDOs (Simple Demo Orgs - used by Sales Engineers for sales demos) causes Goals V2 migration to fail during step 2 of the migration. 
Resolution
Customers are unlikely to run into this problem since they don't have this trigger.
The team is working on the issue, but in the meantime, the workaround is to deactivate the trigger, complete migration and then re-activate the trigger. 
