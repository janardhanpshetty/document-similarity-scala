### Topic: When attempting to use a global action from within Lightning Experience user's see a message reading, "We couldn't find the record you're trying to access. It may have been deleted by another user, or there may have been a system error. Ask your administrator for help." This may occur if your organization uses record types and the action is assigned the Master or placeholder record type which was assigned to the actions when your organization was created.
Users may find that they cannot create records when using the standard global actions in Lightning Experience such as New Task, New Event or Log a Call for example.
After selecting the global action users are presented with the following error, "
We couldn't find the record you're trying to access. It may have been deleted by another user, or there may have been a system error. Ask your administrator for help.
"
 
Resolution
This is typically caused by the global actions being assigned the default or placeholder --Master-- record type. See 
Quick Actions and Record Types
 for more details.
To resolve this an administrator can select an available record type for each affected global action:
1. From Setup, enter Actions in the Quick Find box, then select Global Actions.
2. Click the Edit action link for the affected Actions
3. Change the Record Type field's selection from --Master-- to another available record type.
4. Save
See Also:
Actions Best Practices
Why can't I see my global Publisher Actions?
Lightning Experience: No new Task/Event link when hovering over the no open activities alert
 
