### Topic: We have enabled the new User Interface Theme, and this is visible on all tabs except the Console tab. The Agent Console still displays the old user interface theme.
We have enabled the new User Interface Theme, and this is visible on all tabs except the Console tab. The Agent Console still displays the old user interface theme.
Resolution
The Console tab does not support the new user interface theme, and will display the old theme. This is expected behavior at this time. If you would like to see this behavior changed in a future release, we encourage you to promote the existing request for this on the IdeasExchange: 
http://success.salesforce.com/ideaView?c=09a30000000D9xtAAC&id=087300000007bkUAAQ
.
NOTE: This applies only to the Agent Console, not the new Service Cloud Console.
