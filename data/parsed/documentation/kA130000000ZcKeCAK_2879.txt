### Topic: Edit Custom Permissions | Salesforce
Edit Custom Permissions | Salesforce
Edit Custom Permissions
Edit custom permissions that give users access to custom processes or apps.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
In Group and Professional Edition organizations, you can’t create or edit custom permissions, but you can install them as part of a managed package.
User Permissions Needed
To edit custom permissions:
“Manage Custom Permissions”
From Setup, enter 
Custom Permissions
 in the 
Quick Find
 box, then select 
Custom Permissions
.
Click 
Edit
 next to the permission that you need to change.
Edit the permission information as needed.
Label
—the permission label that appears in permission sets
Name
—the unique name that’s used by the API and managed packages
Description
—optionally, a description that explains what functions the permission grants access to, such as “Approve time-off requests.”
Connected App
—optionally, the connected app that’s associated with this permission
Click 
Save
.
See Also:
Custom Permissions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=custom_perms_edit.htm&language=en_US
Release
202.14
