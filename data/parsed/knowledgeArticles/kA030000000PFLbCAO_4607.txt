### Topic: Portal Users are unable to see Fields from the Cases Object when viewing a report on Activities with Cases. Similar issue happens when using a Cases with Activities custom report type. In this report, the Portal Users will not see the Activity Fields.
Portal Users are unable to see Fields from the Cases Object when viewing a report on "Activities with Cases". Similar issue happens when using a "Cases with Activities" custom report type. In this report, the Portal Users will not see the Activity Fields.
 
Resolution
You will need to enable visibility of the 'Related To' field of the Event entity. 
*Note, you can make this visible AND read only for the portal profile. 
 
This resolves the issue as the portal users will then have access rights to the WhatId field which is required for the "Activity with Cases" and "Cases with Activities" report types. 
