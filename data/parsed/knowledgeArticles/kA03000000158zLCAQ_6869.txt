### Topic: This article explains what the 'Log a Call' button is used for.
This article explains what the 'Log a Call' button is used for.
Resolution
The Log a Call button is a standard button on the page layout that is in the 'Activity History' related list for standard and custom objects.
The purpose of logging a call is to create a completed task/activity record that was achieved via a phone call. After selecting the record type (depending on your organization's Task Record Types) you are brought to a page as displayed in the screenshot below.
Above the '
Schedule follow-up task
' section is for the user to enter information regarding the call. This information will be captured upon saving the page and result in a completed activity in the Activity History for the record.
Below the
 
'
Schedule follow-up task
' section is for the user to enter information for a follow-up task(optional) and can also assign it to a different user. Furthermore if the user wants a reminder to pop up for that user, they may select the date/time in the Reminder section. 
See Also: 
Activities
Events and Calendars
Tasks
Work on Open Activities and View Activity History in Salesforce Classic
 
