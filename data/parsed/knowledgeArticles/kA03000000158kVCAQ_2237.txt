### Topic: Sharing button for Leads and Opportunities are not available for standard Objects in Professional Edition
For Organizations on Professional Edition - Users might see that the Sharing Button for admins to troubleshoot sharing issues is available for several Standard and Custom Objects, but not for Leads, Opportunities, Cases, or several other Standard Objects:
 
 
Resolution
This is working as designed - Outside of Accounts, Contacts, Campaigns, Assets, and Users - Sharing Rules are not available for Standard Objects in Professional Edition - This means there will be no Sharing Button available to add to page layouts.
This includes Leads, Opportunities, Cases, and any other items you do not see listed as an option for Sharing Rules in your Sharing Settings:
Sharing Rules for the other Standard Objects are available in Developer, Enterprise, Unlimited, and Performance Editions.
