### Topic: Why do I receive the error "No standard price defined for this product"?
Why do I receive the error "No standard price defined for this product?"
Resolution
The No standard price defined for this product error message occurs when adding products to custom Price Books using the Data Loader. This error appears because the Product has not yet been added to the standard Price Book. In order to add a product to a custom Price Book it first needs to be in the standard Price Book.
A quick way to resolve this is to export a list of all of your Products. Your export file should include the Product ID and the Unit Price. You should then add an extra column with the ID of the standard Price Book. You can then insert into the Price Book Entry object to ensure that all of your Products are in the standard Price Book.
If the same error message is still appearing then you will need to ensure that the products being added to the custom Price Book are active.
