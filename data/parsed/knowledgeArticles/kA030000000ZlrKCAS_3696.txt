### Topic: Excerpt from Admin Guide to help create SFO publisher actions
To set up your users to work with the side panel publisher, you’ll create the Publisher Actions that you want to appear in your users’ publisher menus, add those publisher actions to the appropriate publisher layout, and assign the publisher layouts to the right users.
Your Salesforce edition determines which side panel features your users can access and how you’ll set up your users to use the side panel publisher.
Verify that you’ve set up Create a Record global actions for each publisher action you want to be available in your users’ side panel publisher menus.
Add the publisher actions to the appropriate publisher layout.
For Group, Contact Manager, or Professional Editions, you'll add publisher actions to your users’ side panel publisher menus with the global publisher layout editor. If you’re already using the global publisher layout for other Salesforce features, you’ll notice that any publisher actions you add for the side panel are displayed for all features. The side panel publisher, however, displays only actions with the action type Create a Record.For Group, Contact Manager, or Professional Editions, you’ll assign your global publisher layout to user profiles to give users access.
For Enterprise, Performance, Unlimited, or Developer Editions, you’ll add publisher actions to your users’ side panel publisher menus with the side panel publisher layout editor. You can also create multiple side panel publisher layouts to give different users access to different side panel publisher menus.  
For Group, Contact Manager, or Professional Editions
, you’ll assign your side panel publisher layout to user profiles to give users access.
 
Creating Publisher Actions for the Salesforce Side Panel Publisher
Available in: All Editions (after Summer '14)
User Permissions needed to set up global actions: “Customize Application”
To display the Salesforce Side Panel in Outlook:  “Side Panel” in Outlook configurations
 
The Salesforce Side Panel Publisher is a menu that lets users create accounts, cases, contacts, leads, opportunities or custom objects records directly from the side panel. You’ll choose which Publisher Actions appear in users’ side panel publisher menus by creating global Create a Record actions for each kind of record.
Note: You can use global actions for several types of publisher menus found in Salesforce, like the ones for the Home page or the Chatter tab. If you’ve already set up global Create a Record actions for the kinds of records you want available to your users, there’s no need to set up the actions again—you’ll find these actions already available to add to all publisher layouts. If you haven’t created global Create a Record actions for the kinds of records you want available to your users, you’ll have to do so now.
To create new Publisher Actions
1. From the Salesforce application, click Setup | Create | Global Actions.
2. Click "New Action"
3. Select the action type "Create a Record"
4. Select the target object to be the desired Object
5. If the Case object has Record Types, a Record Type field will show up: select a record type other than Master.
6. Indicate a label for the action, such as "Outlook Person Account Creator"  (to Publish new Person Account records)
7. Click "Save"
8. Click on "Edit Layout" to drag and drop fields that you want to show or hide on the action
9. Save the layout.
 
Customizing  Side Panel Publisher Menu Layouts for Different Salesforce for Outlook Users
 
Available in: Enterprise, Performance, Unlimited, and Developer Editions
User Permissions Needed to  view side panel publisher layouts: “View Setup”
 
Before you can customize side panel publisher layouts for your users, make sure you’ve set up Create a Record global actions for each kind of record you’d like users to appear in their side panel publisher menus.
Certain Salesforce editions let you create multiple publisher layouts so that users with different job responsibilities can have access to publisher menus with different sets of publisher actions. This lets different types of users create only the records that apply to their job responsibilities. For example, sales representatives may need to create only contacts and opportunities, but sales managers may need to create contacts, opportunities, cases, and leads. This means that you’d need to set up different side panel publisher menus for sales representatives and sales managers by customizing publisher layouts for the groups of users with different sets of global actions.
 
1. From Setup, click Desktop Administration > Outlook Configurations.
2. From the Outlook Publisher Layouts related list, click New. Give your new layout a name, and then click Save.
3. From the Outlook Configurations page, click Edit to add the global actions to your new layout.
4. From the Outlook Side Panel Publisher Layout Editor, drag the Create a Record global actions you want to use into the Publisher Actions section.
After you add publisher actions to a publisher layout, the final step to setting up the side panel publisher for users is to give them access by assigning publisher layouts to the appropriate user profiles:
For Group, Contact Manager, or Professional Editions, you’ll assign your global publisher layout to user profiles to give users access.
For Enterprise, Performance, Unlimited, or Developer Editions, you’ll assign your side panel publisher layout to user profiles to give users access.
 
 
Resolution
