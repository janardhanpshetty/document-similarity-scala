### Topic: View Where a File is Shared | Salesforce
View Where a File is Shared | Salesforce
View Where a File is Shared
Find out who can see a file, and what level of access they have. From the File Detail page, you can access a full list of every person or object the file is shared with.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
The 
Shared With
 list on a file detail page shows you who the file is shared with, and their level of access.
In Salesforce Classic, 
click 
Show All
 to open Sharing Settings, where you can:
See who the file is shared with and their 
permission
: owner, collaborator, viewer, or no access.
Click 
People
 
on the Sharing Settings dialog box
 to 
share the file with specific people
.
Click 
Groups
 on the Sharing Settings dialog box to 
share the file with specific groups
.
Click 
Library
 to share the file with a library.
Click 
Anyone with link
 on the Sharing Settings dialog box to 
create and share a file link.
Click 
Make private
 to remove the file and file link, if applicable, from everywhere it's been shared.
 Or click 
Restrict access
 if the file originated in a Salesforce CRM Content library to remove the file from everywhere it's been shared except the library.
 Only owners and users with the “Modify all Data” permission can 
make a file private (
)
 or restrict access
.
On the Sharing Settings dialog box, click 
 next to a person's or group's name to stop sharing the file with them.
See Also:
Make a File Private
Who Can See My File?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_files_shared_with.htm&language=en_US
Release
202.14
