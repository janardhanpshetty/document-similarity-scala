### Topic: User sometime may notice different Geo in URLs and sometimes in emails. Provided his local time zone is different.
User sometime may notice different Geo in URLs and sometimes in emails. Provided his local time zone is different.
For Example:-
1. Customer based in North America eastern/Florida Orlando should have (GMT-04:00) Eastern Daylight Time (America/New_York) and Signup Country / Div US / USA (208000000000000) but its not updated correctly 
2. User trying to import some Leads. When the system emailed him back saying the leads were imported, it came from "support@emea.salesforce.com" provided customer is US based
 
Resolution
The customer is probably seeing this because his Time Zone / Signup Country under company profile is incorrect
For Example:- Customer based in North America eastern/Florida Orlando should have (GMT-04:00) Eastern Daylight Time (America/New_York) and Signup Country / Div US / USA (208000000000000), however, its showing as Signup Country / Div IN / EMEA (208000000000001)
You can find this on the org Black Tab, changing it to whatever country they are in along with the division should fix this issue.
Escalate case to T3 to make the required changes.
