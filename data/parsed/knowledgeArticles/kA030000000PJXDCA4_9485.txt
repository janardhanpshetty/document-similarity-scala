### Topic: VisualForce page URL parameter is removed if a page is refreshed
If a VF page with a parameter is invoked, the parameter is removed if a page is refreshed.
This can be reproduced with the current syntax:
Apex code:
===============
public class sfdcController {

    private final Account account;

    public sfdcController() {
        account = [SELECT Id, Name,  Site FROM Account 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('accountId')];
    }

    public Account getAccount() {
        return account;
    }

    public PageReference save() {
        update account;
        return null;
    }
    
     public PageReference saveInProgress(){
        return null;  //command buton to refresh the page
     }   
}
 
 
VF code:
===============
<apex:page controller="sfdcController" tabStyle="Account">
    <apex:form >
        <apex:pageBlock title="Congratulations {!$User.FirstName}">
            You belong to Account Name: <apex:inputField value="{!account.name}"/>
            <apex:commandButton action="{!save}" value="save"/> 
            <apex:commandButton action="{!saveinProgress}" value="Save In Progress"/>
        </apex:pageBlock>
    </apex:form>
</apex:page>
 
Goto the page 
https://c.na1-blitz03.visual.soma.force.com/apex/paraIssue?accountId=001D000000JSvkT
 and refresh it a few times, the parameter 
accountId=001D000000JSvkT
 will be removed from the url.
Resolution
This is current expected behavior. The only exception is Developer mode is ON for the running user. If developer mode is off, it will always be removed. R&D has logged a user story to make the change to be consistent in the future. The expect behavior is it should be consistent regarding the Developer mode is ON or not.
This can be tracked - https://gus.salesforce.com/a07B0000000UynpIAC
 
