### Topic: Default Field Value Considerations | Salesforce
Default Field Value Considerations | Salesforce
Default Field Value Considerations
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Default field values automatically insert the value of a custom field when a new record is created. A default value can be based on a formula for some types of fields or exact values such as 
Checked
 or 
Unchecked
 for checkbox fields. Review the following considerations before incorporating default field values in your organization.
If a default value is based on the value of a merge field, Salesforce uses the value of the merge field at the time the default value is executed. If the value of the merge field changes later, the default value is not updated.
Users can change or remove the default field value on a record.
Don’t assign default values to fields that are both required and unique, because uniqueness errors can result.
If you make an activity custom field universally required, you must also provide a default value.
If an activity custom field is unique, you cannot provide a default value.
Default field values are different from formula fields in the following ways: they are only executed once, at record creation; they are not read only; and the user can change the value but cannot restore the default field value.
Since the default value is inserted before users enter any values in the new record, you cannot use the fields on the current record to create a default field value. For example, you cannot create a default field value on a contact that uses the first initial and last name because those values are not available when you click 
New
 to create a contact record. However, you can use the record type because it is selected before the record edit page displays.
To apply a different default value for different record types, use the record type as a merge field in a CASE function within the default field value setup.
Fields that are not visible to the user due to field-level security are still available in the formula for a default field value.
Connect Offline and Salesforce for Outlook do not display default values. However, Salesforce inserts the default values when a user syncs unless the user entered a value.
Default field values are not available in the Self-Service portal.
Lead conversion, Web-to-Lead, and Web-to-Case do not execute default field values.
Note
You can define a formula for default values only where appropriate. For example, the default value options for picklist and checkbox fields are limited to the options available for those types of fields, such as 
Checked
, 
Unchecked
, or 
Use first value as default value
.
See Also:
Define Default Field Values
Useful Default Field Value Formulas
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_default_field_value_considerations.htm&language=en_US
Release
202.14
