### Topic: Executing jQuery.noConflict() mandatory when using custom jQuery in Visualforce
If you are using a custom jQurey version somewhere on the page (like sidebar components) then following Visualforce code to add 
Tab Panel
 does not work:
 
<apex:page id="thePage">
    <apex:tabPanel switchType="client" selectedTab="name2" id="theTabPanel">
        <apex:tab label="One" name="name1" id="tabOne">content for tab one</apex:tab>
        <apex:tab label="Two" name="name2" id="tabTwo">content for tab two</apex:tab>
    </apex:tabPanel>
</apex:page>
 
Resolution
This is because when custom jQuery is used in VF page, different versions can conflict with each other. To resolve this jQuery.noConflict() should be used. To fix above issue, following code can be used:
<apex:page id="thePage">
<script type="text/javascript" language="javascript">
    if(jQuery) {
        jQuery.noConflict();
    }
</script>
    <apex:tabPanel switchType="client" selectedTab="name2" id="theTabPanel">
        <apex:tab label="One" name="name1" id="tabOne">content for tab one</apex:tab>
        <apex:tab label="Two" name="name2" id="tabTwo">content for tab two</apex:tab>
    </apex:tabPanel>
</apex:page>
Reference Links:
https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_compref_tab.htm
https://developer.salesforce.com/page/Developing_Apps_with_jQuery
https://developer.salesforce.com/blogs/developer-relations/2011/01/adding-jquery-to-your-salesforce-application.html
