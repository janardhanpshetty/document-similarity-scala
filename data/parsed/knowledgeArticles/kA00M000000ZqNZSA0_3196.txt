### Topic: The Marketing Cloud Utilization Report has historically been to review messages sent from your account. Learn how you can now access this information.
The Marketing Cloud Utilization Report used to be available in 3sixty, and now you'd like to review that same data after 3sixty was discontinued. Learn how below. 
Resolution
To find the details of your current Marketing Cloud Utilization, you can now contact your Account Executive, Renewal Manager or Success Manager directly. 
