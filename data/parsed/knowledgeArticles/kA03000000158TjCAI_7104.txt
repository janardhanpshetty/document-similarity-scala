### Topic: Improved Sandbox Copy Engine for Developer and Developer Pro Sandboxes is Generally Available
We’ve rebuilt our sandbox copy engine to optimize performance, scalability, and customer success. The new engine impacts Developer and Developer Pro sandboxes as they are created and existing Developer and Developer Pro sandboxes as they are refreshed.
Several enhancements to the copy engine make sandbox creation and refresh faster and minimize sandbox inconsistencies. These improvements are available on a rolling basis during the Winter ’16 release.
 
Resolution
Intelligent Routing
The copy engine selects the best instance for your sandbox based on available storage space, CPU consumption, and import activity on each sandbox instance.
New Data Copy Framework
The copy engine divides your organization’s data into small chunks for exporting and copying to the sandbox. Copying is faster, because chunks are processed in parallel. Importing begins as soon as the first data chunk is exported. Export and import are independent, so the system is robust and flexible.
New Post-Copy Framework
If a production organization changes during sandbox creation or refresh, the sandbox can contain data inconsistencies. The new automated post-copy process quickly fixes these inconsistencies after copying is complete and tracks the fixes.
Progress Tracking
You can monitor the progress of your sandbox creation or refresh.
From Setup, click Sandboxes or Data Management > Sandboxes to view a list of your sandboxes. The list displays a progress bar for sandboxes in the queue, in process, or recently completed. Hover over the progress bar to view the percentage completed of a copy in progress. Click a name to go to a detail page showing information about the sandbox, including how much time before the next available refresh. If your sandbox is suspended or stopped for more than one hour, contact Salesforce Customer Support.
