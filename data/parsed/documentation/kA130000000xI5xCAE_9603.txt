### Topic: Find Your Way Around Lightning Experience | Salesforce
Find Your Way Around Lightning Experience | Salesforce
Find Your Way Around Lightning Experience
Feeling a little discombobulated by all the Lightning Experience newness? Perfectly understandable. Diving into a redesigned app can be disorienting—especially when you’re able to work in the original interface with your eyes closed. To hone your Lightning Experience chops, let’s look at where some of the most common Salesforce features landed in the new interface.
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Find Profile and Personal Settings in Lightning Experience
In Salesforce Classic, your profile and personal settings are located in the menu with your name. It’s not too different in Lightning Experience. Find links to these options by clicking your profile picture. Want to switch between Lightning Experience and Salesforce Classic? You can find links to do so from here, too.
Find Your Stuff in Lightning Experience
In Salesforce Classic, you use tabs to access objects, like accounts and leads. Tabs also serve up other features, such as your Home page or Chatter. Lightning Experience replaces the tab bar with a collapsible navigation menu that includes objects, apps, and other goodies. Looking for connected apps? If an item isn’t in the navigation menu, look in the App Launcher.
Find List Views in Lightning Experience
Lightning Experience improves the Salesforce Classic list view experience with a more intuitive layout, convenient navigation, and seamless list creation and editing.
Find Your Way Around Records in Lightning Experience
In Salesforce Classic, record details, related lists, and the record feed display on the same page. This layout can lead to a lot of scrolling, especially in records with a lot of data. In Lightning Experience, find record details, related information, and the record feed in dedicated tabs that are easy to switch between.
Find Your Way Around Lightning Experience Setup
Lightning Experience Setup makes it faster and easier than ever to customize Salesforce to meet your company’s needs.
Find Actions and Buttons in Lightning Experience
While Salesforce Classic displays actions in the Chatter publisher, and buttons on a record’s details page, Lightning Experience blurs the distinction between these elements. The new interface intermingles actions and buttons, locating them in different areas, based on function.
Find Related Lists in Lightning Experience
Records in Salesforce include details and links to other related records. Salesforce Classic displays related records in lists that appear near the bottom of the page. In Lightning Experience, related information appears in related list cards. For leads and opportunities—the objects that include a workspace—access related list cards from the Related tab. For reference objects like accounts and contacts, and on groups and people, related list cards display on the right side of the page.
Find Chatter Features in Lightning Experience
With Lightning Experience, you can connect with people and groups and share business information securely and in real time, just like you’re used to in Salesforce Classic. The way you access Chatter feeds, groups, and profiles is a bit different in the new interface. But with this quick overview, you’ll feel right at home.
Find Help in Lightning Experience
Help is here! And it’s better than ever in Lightning Experience, where the contextual Help menu serves up targeted content like videos, walkthroughs, help topics, and trails. Bonanza!
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=lex_find_your_way_around.htm&language=en_US
Release
202.14
