### Topic: Explanation of lead conversion error when using Lightning Experience "Error on loading Sales Path: This lead was already converted to the contact"
If you are using trigger to convert a lead to Account, opportunity or contact and you change the field based on which lead convert trigger will fire, it will cause the following error message to show:
"Error on loading Sales Path 
This lead was already converted to the contact TEST_CMP on 28/03/2016 . " 
 
Resolution
The error message is thrown when converting lead on lightning experience but the same scenario works fine on Classic view. 
Although lead is converted, you would get the above mentioned error message. 
1. If trigger is setup to convert the lead, that lead convert is triggered on save. After save, it will show Lead is converted to XXX at XXX.
It is by design. The generic message is not replaceable. 
2. In SFX, after the save operation and lead conversion by trigger, it will not show an internal error or show any generic message. It should show Lead is converted to XXX at XXX message. " 
3. We see a message "Error on loading Sales Path" which indicates that the lead could not be loaded since the last update converted the lead. 
Why would the same behavior not occur on Classic UI ? 
The code path is different in Classic. This new behavior only applies to one.app. 
