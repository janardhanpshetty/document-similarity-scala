### Topic: Cannot change Community Domain name on BT
Customer requested to change their community domain name. Changing the domain name on BT would give the following error:
 
Insufficient Privileges
You do not have the level of access necessary to perform the operation you requested. Please contact the owner of the record or your administrator if access is necessary.
 
Click here to return to the previous page.
 
 
 
Resolution
 
Followed the workaround on GUS BUG : W-2332161
https://gus.my.salesforce.com/apex/adm_bugdetail?id=a07B000000104fd&sfdc.override=1&srKp=a07&srPos=0
 
GUS investigation:
https://gus.my.salesforce.com/apex/adm_investigationdetail?id=a07B0000000zg2U&sfdc.override=1&srKp=a07&srPos=0
Steps taken:
 
Login with SU Admin 
Goto Setup-Communities-Manage communities
Click on Site.com next to the community
There can be pending changes which were not publish (Do'not publish without customer consent)
On the top left corner of the studio, click on the community drop down list and click on "Exit Site.com studio"
Go back to BT , and try changing the "Unique Chatter Community Subdomain".
You wills still see the Insufficient privileges error . 
Follow the above steps again and login into the site.com studio, Under Site configuration, Check for Domains and you will see the change.
