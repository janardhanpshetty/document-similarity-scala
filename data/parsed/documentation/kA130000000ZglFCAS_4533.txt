### Topic: How Predefined Actions Are Ordered in the Salesforce1 Action Bar and List Item Actions | Salesforce
How Predefined Actions Are Ordered in the Salesforce1 Action Bar and List Item Actions | Salesforce
How Predefined Actions Are Ordered in the Salesforce1 Action Bar and List Item Actions
Your org’s page layouts and publisher layouts control the order in which actions appear in the Salesforce1 action bar and list item actions. If you don’t customize the actions in the action bar on a page layout or global publisher layout, the location of key actions is predefined by Salesforce.
Important
This predefined ordering chart applies to the Salesforce1 action bar only if you 
haven’t
 customized the Salesforce1 and Lightning Experience Actions section of an object’s page layout or on a global publisher layout.
The quick actions in the predefined set are derived from the actions in the “Quick Actions in the Salesforce Classic Publisher” section of the object page layout or global publisher layout.
On object page layouts, when the Salesforce1 section isn’t customized:
If you’ve customized the actions in the Quick Actions in the Salesforce Classic Publisher section, the quick actions in the action bar reflect those customizations.
If neither section is customized, the quick actions you see in the action bar come from the Quick Actions in the Salesforce Classic Publisher section of the global publisher layout.
On global publisher layouts, when the Salesforce1 section isn’t customized:
If the Quick Actions in the Salesforce Classic Publisher section is customized, the quick actions in the action bar inherit those customizations.
If neither section is customized, the quick actions in the action bar for global pages default to a Salesforce predefined set.
The predefined actions in the action bar, list item actions, and associated action menus are divided into groups. The arrangement of these groups is fixed. The order of actions within the groups can vary based on the object and the actions that are present on the global publisher layout or on an object’s page layout. Not every object or page displays every group.
Note
Actions on list view items reflect only the predefined set of actions for that object. For example, let’s say you’re viewing the All Accounts list in Salesforce1. If you swipe left on an account item in the list, you see a set of actions. Those actions come from the predefined list of actions for accounts in this chart. You always see Call, Edit, and Delete. The other actions on the list view item follow the order and rules defined for the action groups in the chart.
Here’s the breakdown of which actions are contained in each group for each object or page.
Object or Page
Action Group 1
Action Group 2
Action Group 3
Action Group 4
Action Group 5
Action Group 6
Account
1. Call, 2. New Task, 3. New Event, 4. Post
5. Edit
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
View Website (if the 
Website
 field is populated)
Case
Actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Edit
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
Contact
1. Call, 2. Send Email, 3. New Task, 4. New Event
5. Edit
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
Custom Object
First four actions in the order defined on the page layout. If the Quick Actions in the Salesforce Classic Publisher section isn’t customized, then the first four actions in the order defined on the 
global publisher layout
.
5. Edit
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
Event
Quick actions in the order defined on the layout. Standard Chatter actions aren’t supported.
Edit, Delete
Feed
Quick actions in the order defined on the global publisher layout
Group
Actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
Lead
1. Log a Call, 2. New Task, 3. Convert (if enabled), 4. Post
5. Edit
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
Call, Send Email
“App Page” Lightning Page
Global actions in the order defined in the Lightning Page
List View
New
Object Home Page (Tablet Only)
1. New, 2. Sort
Opportunity
1. Log a Call, 2. New Task, 3. New Event, 4. Post
5. Edit
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
People
1. Call, 2. Send Email, 3. Post
Remaining actions in the order defined on the global publisher layout
Person Account
1. Call, 2. Send Email, 3. New Task, 4. New Event
5. Edit
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
Map, Read News, View Website
Related List (for standard objects)
1. New
Salesforce Today—Main Page
Quick actions in the order defined on the global publisher layout
Salesforce Today—Mobile Calendar Event
1. Quick Message, 2. Join Conference Call, 3. Map
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
.
Task
1. Edit Comments, 2. Change Date, 3. Change Status, 4. Change Priority
5. Edit
Remaining quick actions from the Quick Actions in the Salesforce Classic Publisher section. If that section isn’t customized, remaining quick actions are inherited from the Quick Actions in the Salesforce Classic Publisher section of the 
global publisher layout
. Standard Chatter actions aren’t supported.
Custom buttons that are supported in Salesforce1, in the order defined on the page layout.
*
Remaining standard buttons that are supported in Salesforce1, in the order defined on the page layout.
As we mentioned, some actions are in fixed positions. In places where you see a numbered list in the table, this is the fixed order that those actions appear in on the action bar, list item actions, and in the respective action menus.
For example, for the Account object, the standard Chatter Post action is in the fourth position. This is fixed. Regardless of where you put the Post action in the account page layout, Post always displays in the fourth position.
However, deletion of actions is always respected. So in our example, if you delete the Post action from the account page layout, the remaining actions move up and you see Edit in the fourth position.
*
 Custom buttons that are added to the Button section of a page layout and that define the content source as 
URL
 or 
Visualforce
 are supported in Salesforce1. Remember that Visualforce pages must be enabled for use in Salesforce1. Custom links, custom buttons that are added to list views, and custom buttons that define the content source as 
OnClick JavaScript
 aren’t available in Salesforce1.
See Also:
How Actions Are Ordered in the Salesforce1 Action Bar
Salesforce1 Action Bar
Considerations for Actions in Salesforce1
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=actionbar_how_actions_are_ordered.htm&language=en_US
Release
202.14
