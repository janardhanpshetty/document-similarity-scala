### Topic: Information on who can activate Insights within the Analysis Dashboard
Who can activate Insights on a Topic Profile within the Analysis Dashboard?
Resolution
The owner of the Topic Profile is the only user that can apply the Free Insights (Basic Demographics and Radian6 Insight) within the Analysis Dashboard.
If the message "This Topic Profile exceeds the maximum estimated monthly volume for self serve insights activation. To subscribe this insights package on your topic profile, please contact your Account Manager"appears when applying the free Insights to your Topic Profile, your Topic Profile's Estimated Monthly Volume (EMV) is over 300,000. For tips on reducing your Topic Profile's EMV, please review the 
Topic Profile Optimization user guide
.
 
 
