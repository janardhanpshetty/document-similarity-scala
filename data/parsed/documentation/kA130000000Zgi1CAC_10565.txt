### Topic: Background Information about Dashboards | Salesforce
Background Information about Dashboards | Salesforce
Background Information about Dashboards
Here’s some general information about Wave Analytics dashboards to supplement the instructions for planning, sketching, and building dashboards.
What Is a Dashboard?
A dashboard is a vehicle for telling a specific, focused story about a dataset that your audience can act on. It consists of a curated set of charts, metrics, and tables based on the data in a lens.
What Kind of Dashboard Should I Create?
Dashboards can support many kinds of knowledge seeking: operational, strategic, or analytical. Sometimes a dashboard serves a combination of purposes, but it’s a good idea to keep your dashboard focused on a single purpose.
Wave Keyboard Shortcuts
You can do some basic actions from your keyboard. The dashboard designer shortcuts vary based on whether you are using the original or flex dashboard designer.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_dashboard_concepts.htm&language=en_US
Release
202.14
