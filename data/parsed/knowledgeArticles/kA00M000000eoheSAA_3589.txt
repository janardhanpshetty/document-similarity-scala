### Topic: This article is applicable to the services branded as Buddy Media (“Buddy Media Services”) and provides links to documentation describing a number of trust and compliance-related topics.
This article is applicable to the services branded as Buddy Media (“
Buddy Media
 Services
”) and provides links to documentation describing:
the architecture of, the security and privacy-related audits and certifications received for, and the administrative, technical and physical controls applicable to the Buddy Media Services;
features, restrictions and notices associated with any:
    *  information sourced from third parties or public sources and provided to customers via the Buddy Media Services;
    *  Buddy Media Services functionality that allows customers to interact with social media and other websites; and
    *  desktop and mobile device software applications provided in connection with the Buddy Media Services; and
the salesforce.com External-Facing Services Policy.
Resolution
The following articles and their attachments are available on 
help.salesforce.com
.
Buddy Media Security, Privacy, and Architecture Documentation (attached to this article)
Buddy Media Notices and License Information (attached to this article)
Marketing Cloud Sub-processors List (attached to this article)
External-Facing Services Policy
Salesforce Service Organization Control (SOC) reports
 (SOC-1, SOC-2, or SOC-3)
ISO 27001 certification
 
