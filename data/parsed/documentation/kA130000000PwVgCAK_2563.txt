### Topic: Searchable Fields: Salesforce CRM Content | Salesforce
Searchable Fields: Salesforce CRM Content | Salesforce
Searchable Fields: Salesforce CRM Content
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 editions
Neither sidebar search nor advanced search are designed to find content. To find content, use global search (results appear as files) or the search tools on the Content tab.
Searchable Fields
Sidebar Search
Advanced Search
Global Search
Content Tab
Body
Description
File
Owner
Title
Version
All custom auto-number fields and custom fields that are set as an external ID
(You don't need to enter leading zeros.)
All custom fields of type text, text area, long text area, rich text area, email, and phone
See Also:
Searchable Fields by Object in Salesforce Classic
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_fields_content.htm&language=en_US
Release
202.14
