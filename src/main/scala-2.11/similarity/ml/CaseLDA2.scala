package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.clustering.LDA
import org.apache.spark.ml.feature.{CountVectorizer, RegexTokenizer, StopWordsRemover}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


/**
 * Created by jshetty on 9/13/16.
 * Topic modeling using subject + description
 * spark-submit --class similarity.ml.CaseLDA2 --master local[12] --driver-memory 8G /Users/jshetty/spark-applications/documentsimilarity/target/scala-2.11/documentsimilarity_2.11-1.0.jar
 */
object CaseLDA2 {

  def main(args: Array[String]): Unit = {

    val getConcatenated = udf( (first: String, second: String) => { first + " " + second } )

    val spark = SparkSession.builder.master("local[12]").appName("Topic modeling using case data").getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext
    sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    // Read Input
    //val casePath = "/Users/jshetty/spark-applications/documentsimilarity/data/casedata/case_apr_sep_16_subject_english.csv"
    //val casePath = "/Users/jshetty/casedata/data/reason_subject_description/caseclosed_1year.csv"
    val casePath = "/Users/jshetty/casedata/data/reason_subject_description/proddata/caseclosed_fromSep2015.csv"
    val caseInput = spark.read.option("header","true").option("delimiter","^").csv(casePath)
    val textInput = caseInput.withColumn("text", getConcatenated(col("SUBJECT"), col("DESCRIPTION"))).cache()
    //val caseInput = caseInputP.select("_c1").withColumnRenamed("_c1", "text").cache()

    // Preprocessing
    // Get the custom stopwords files
    val stopWordsPath = "/Users/jshetty/spark-applications/documentsimilarity/input/stopwords/english_custom_stopwords.txt"
    val stopwords = sc.textFile(stopWordsPath).collect()

    // Setup Documents pipeline
    val numTopics = 20
    val numIterations = 500
    val vocabSize = 100000
    val minDocs = 3
    val maxTermsPerTopic = 20
    val documentTokenizer = new RegexTokenizer().setInputCol("text").setOutputCol("subWords").setPattern("\\W")
    val docTokFiltered = new StopWordsRemover().setStopWords(stopwords).setInputCol("subWords").setOutputCol("subWordsFiltered")
    val docPipeline = new Pipeline().setStages(Array(documentTokenizer, docTokFiltered))
    val docDF = docPipeline.fit(textInput).transform(textInput)
    val docVectorizer = new CountVectorizer().setInputCol("subWordsFiltered").setOutputCol("features").setVocabSize(vocabSize).setMinDF(minDocs).fit(docDF)
    val vocabList = docVectorizer.vocabulary
    val docVecDF = docVectorizer.transform(docDF)

    // LDA model
    val docLda = new LDA().setK(numTopics).setMaxIter(numIterations)
    val docLDAModel = docLda.fit(docVecDF)

    // Save the Model
    docLDAModel.save("/Users/jshetty/casedata/topicModeling/models/topicModel2")

    // Review results of Topic modeling
    println(s"$numTopics numTopics:")
    val topicIndices = docLDAModel.describeTopics(maxTermsPerTopic = maxTermsPerTopic)
    val getVocabWord = udf { (indexList: Seq[Int]) => indexList.map(vocabList(_)).toSeq}
    val topicWords = topicIndices.withColumn("termWords", getVocabWord(topicIndices("termIndices")))
    val topics = docLDAModel.transform(docVecDF)

    topicWords.select("topic", "termWords").show(false)
    // display transformed words
    //topics.show(false)

    spark.stop()
  }

}
