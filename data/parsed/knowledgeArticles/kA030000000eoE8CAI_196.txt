### Topic: Lists all the possible values used to tag a partner case.
This document is used to provide agents with a resource which lists all possible case reason, general application area, functional area, and sub-functional area fields used to route a partner case. 
Resolution
- Partner support can be broken up into two main groups - 
Partner Technical Support
 and 
Partner Program Support
.
- Within Partner Program Support, there are three queues currently - 
Alliances Technical Tier 1, Alliances Shared Services, SI Support
.
- Within Partner Technical Support, there are three queues - 
Salesforce Frontline Queue, Cognizant Frontline Queue, and HCL Frontline queue
.
- Note, Partner Technical Support has skillgroups to further dissect the types of cases each group handles like usage, or features activation.
All partner program support fields can be found on this spreadsheet: 
https://docs.google.com/a/salesforce.com/spreadsheets/d/1_p0cfOAZcP6dHTiH_qhiXVxgm47fMrNvIIvaiqU9hNk/edit?usp=sharing
File: 
https://org62.my.salesforce.com/06930000005iuMP
- The queues labeled "Technical Support" will route to the appropriate skillgroup in technical support, those case types will not be supported by any partner program support team.
All partner technical support fields can be found on this spreadsheet: 
https://docs.google.com/a/salesforce.com/spreadsheets/d/10h-4OuQSwUEZ2ZVaQJDnTOqwdIK8tdm9R7IKoS-oqKI/edit?usp=sharing
File: 
https://org62.my.salesforce.com/06930000005iuMU
 
