### Topic: Delete Items Syncing with Salesforce for Outlook | Salesforce
Delete Items Syncing with Salesforce for Outlook | Salesforce
Delete Items Syncing with Salesforce for Outlook
Remove an item syncing with Salesforce for Outlook from both Salesforce and Outlook by deleting the item in one system only. Take care to give special treatment to recurring tasks and recently-updated items you want to delete.
This feature available to manage from: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To delete items syncing between Salesforce and Outlook
“Delete” on all objects that users want to delete
First, check the sync direction for the contact, event, or task you want to delete. Next, delete the item from the system in which the item is set to flow from. If your items are set to sync in both directions, you can delete items from either system.
As a result, Salesforce for Outlook automatically removes the corresponding item in the other system.
Note
Recurring tasks only sync from Salesforce, so there’s no need to check the sync direction before deleting those items: always delete recurring tasks in Salesforce.
From your personal settings, enter 
Salesforce for Outlook
 in the 
Quick Find
 box, then click 
View My Configuration
.
Review the sync direction setting for the contact, event, or (non-recurring) task that you want to delete.
Delete the item in the system from which sync is flowing.
For example, if you want to delete a contact, and your contacts are set to sync from Outlook to Salesforce, delete the contact in Outlook.
The item you deleted is removed from both systems during the next sync cycle.
If the item was not removed from both systems, you—or another sales rep—may have updated that record within the same sync cycle and Salesforce for Outlook gave preference to the update over the delete. This behavior happens as a result of 
Conflict Behavior
 set in someone’s Salesforce for Outlook configuration, which would have let the update in one system “win” over the delete in the other. In this case, delete the record again to remove it from both systems. Or maybe check with your coworker first, who obviously cares about that item!
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=outlookcrm_delete_item.htm&language=en_US
Release
202.14
