### Topic: Chatter Desktop Troubleshooting Tips | Salesforce
Chatter Desktop Troubleshooting Tips | Salesforce
Chatter Desktop Troubleshooting Tips
Find solutions for troubleshooting Chatter Desktop.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Chatter Desktop uses Adobe® Integrated Runtime (AIR®). This table describes solutions to common AIR-related issues. For more information, consult the Adobe® knowledge base.
Problem
Solution
The Chatter Desktop installation fails
Delete the Shared Local Object (SLO) and Encrypted Local Store (ELS) directories and reinstall Chatter Desktop.
The SLO stores persistent information like last screen position and size and is located at
Mac OS:
 
/[USER]/Library/Preferences/sfdc-desktop*
Windows Vista, Windows 7, Windows 8.x:
 C:\Users\<USER>\AppData\Roaming\sfdc-desktop*
Windows XP:
C:\Documents and Settings\<USER>\Application Data\sfdc-desktop*
The ELS stores the Oauth2.0 identities for users and is located at
Mac OS:
 
/[USER]/Library/Application Support/Adobe/AIR/ELS/sfdc-desktop*
Windows Vista, Windows 7, Windows 8.x:
C:\Users\<USER>\AppData\Roaming\Adobe\AIR\ELS\sfdc-desktop*
Windows XP:
C:\Documents and Settings\<USER>\Application Data\Adobe\AIR\ELS\sfdc-desktop*
Chatter Desktop on Linux freezes, presents an empty window, doesn’t save your password, or malfunctions in another way
If you’re using the managed installation, verify
The 
.chatterdesktop
 file is well-formed and contains valid XML
The URLs in the 
.chatterdesktop
 file are valid and each URL begins with 
https://
 and ends with 
salesforce.com
Also troubleshoot your Encrypted Local Storage (ELS) keystore. For more information, see 
http://kb2.adobe.com/cps/492/cpsid_49267.html
Chatter Desktop doesn’t preview PDF files
Chatter Desktop uses Adobe® Acrobat® Reader to preview PDF files. Before previewing files with Chatter Desktop, download Adobe Acrobat from 
Adobe's website
, install it, and open it at least once to complete the installation.
Photos with comments don’t refresh
Click the sync icon.
See Also:
Configuring Chatter Desktop
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_desktop_troubleshooting.htm&language=en_US
Release
202.14
