### Topic: Learn how to clear up issues with the Integration that stem from upgrading the Connector package.
Some issues that might come up after an update include unexpected inability to send from within Salesforce, Users not having access they're expected to have as either Salesforce System Administrators or Marketing Cloud Administrators, or error messages indicating a specific Business Unit is not correctly configured.
Resolution
Prerequisites to complete below steps:
 
Username/Password for Marketing Cloud API User used by the Integration.
User performing the steps is a System Administrator in Salesforce.
Add Configurations Tab
1. Click 
Setup
. 
2. Under the "Create" category, select 
Tabs
.
3. Click 
New
.
4. Choose "Configurations" from the Object drop-down menu.
5. Pick any Tab style.
6. Click 
Next
 | 
Next
 | 
Save
.
    
Delete Stored Configuration
1. Click the 
Plus symbol
 on the tab bar.
2. Click on the 
Configurations
 link.
3. Choose 
All
 under the "View" drop-down, and then click 
Go
.
4. Delete the existing Configuration Object.
Reconfigure Marketing Cloud Connect
1. Click the 
Marketing Cloud
 Tab.
2. Enter the Marketing Cloud API Username and Password.
3. Select values for Send Types, Target Audience, Exclusion Audience, Support Ticket Recipient, and Tracking Preferences.
4. Click 
Marketing Cloud
 
Tab
 once more, re-integrate individual Users as needed.
