### Topic: Using the StartURL Parameter | Salesforce
Using the StartURL Parameter | Salesforce
Using the StartURL Parameter
Send your user to a specific location after authenticating or linking.
Available in: Lightning Experience and Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view the settings:
“View Setup and Configuration”
To edit the settings:
“Customize Application”
AND
“Manage Auth. Providers”
To direct your users to a specific location after authenticating, you need to specify a URL with the 
startURL
 request parameter. This URL must be a relative URL; passing an absolute URL results in an error. If you don’t add 
startURL
, the user is sent to either 
/home/home.jsp
 (for a portal or standard application) or to the default sites page (for a site) after authentication completes.
Example
For example, with a 
Single Sign-On Initialization URL
, the user is sent to this location after being logged in. For an 
Existing User Linking URL
, the “Continue to Salesforce” link on the confirmation page leads to this page.
The following is an example of a 
startURL
 parameter added to the 
Single Sign-On Initialization URL
, where:
orgID
 is your Auth. Provider ID
URLsuffix
 is the value you specified when you defined the authentication provider
https://login.salesforce.com/services/auth/sso/
orgID
/
URLsuffix
?startURL=%2F005x00000000001%3Fnoredirect%3D1
See Also:
Using Request Parameters with Client Configuration URLs
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sso_provider_addl_params_start.htm&language=en_US
Release
202.14
