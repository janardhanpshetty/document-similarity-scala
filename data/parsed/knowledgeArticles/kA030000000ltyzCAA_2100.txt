### Topic: End of support for RC4 encryption algorithm in TLS
End of support for RC4 encryption algorithm in TLS
Resolution
RC4 Encryption Algorithm in TLS End of Support
Support for RC4 encryption algorithm has come to the end. RC4 was disabled on the following date:
- Sandbox: April 25th, 2015
- Production: July 25th, 2015
 
Frequently Asked Questions:
1. What exactly will happen after the End of Support date passes?
a. After the change, all Salesforce supported browsers and integrations configured to support only the RC4 algorithm will lose access to Salesforce. In order to regain access, the browser or integration will need to be configured with a Salesforce supported cipher suite.
b. For a list of supported TLS cipher suites, refer to the “
List of Salesforce Supported Cipher Suites
” Knowledge article.
 
2. What action must I take?
a. If your browsers or integrations are configured with a Salesforce supported cipher suite, you do not need to take action.
b. If you have software (including integrations) explicitly configured to support only RC4, you will need to change the configuration to also allow AES and/or Triple DES (3DES) encryption or, preferably, return your software to the default configuration. This would allow it to select an appropriate algorithm automatically.
c. For additional information on what browsers (or which users) are using the RC4 encryption algorithm TLS, please log a case via the Help & Training portal.
 
3. What is the recommended replacement or workaround for customers?
We recommend customers use a different TLS cipher suite from the list Salesforce currently supports, which can be found in the “
List of Salesforce Supported Cipher Suites
” Knowledge article.
 
4. Where can I find additional information?
For more in-depth technical information on the RC4 encryption algorithm, see the article “
On the Security of RC4 in TLS and WPA
” at this link 
http://www.isg.rhul.ac.uk/tls/
