### Topic: Using Chatter Desktop | Salesforce
Using Chatter Desktop | Salesforce
Using Chatter Desktop
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
In the Chatter Desktop window:
Click:
To:
Your profile photo
Change your chat status. Select 
Available
, 
Away
, or 
Offline
. People can’t chat with you when you’re 
Offline
.
See feed updates from everyone and everything you follow, and post an update.
For any feed post, click 
Comment
 to add a comment, 
Like
 to receive email notifications if others comment on that post, or 
Delete
 to delete the post, if you have access.
To post a file from your computer, drag the file to 
, or click 
, which appears when you type a post.
To post a link, type the link in the text box at the top of your feed, or click 
, which appears when you type a post.
Use the 
Sort By
 filter at the top of a feed to sort updates:
Post Date—Sorts feeds by post date. To see comments, you must click 
Show All Comments
 below a post.
Post and Comment Date—Sorts feeds by post and comment date. Each comment appears again when you click 
Show All Comments
 under its associated post.
See and comment on updates directed to you.
Chat with people and see their updates. Chat messages are displayed in a separate window.
Click 
Chat Favorites 
to see people or chats you’ve added as favorites. To add a favorite, click 
 next to someone’s name or in any active chat with one or more people.
Click 
Following
 or 
Followers
 then click a person’s name to see their updates or post to their feed.
Hover over someone’s name:
Click 
 to send them a private message.
Click 
 to add them as a favorite chat. Their name appears in your 
Chat Favorites 
list.
Click 
 to follow someone or 
 to stop following them.
Click 
 to start a chat. You can then add up to 10 people to the same chat by clicking 
 in the chat window and searching for people to add. You can’t chat with people who are 
 
Offline
.
Note
Chat is only available if it’s enabled in your organization.
Read and reply to private Chatter messages. Click a message to view it, or click 
New Message
 to send a new message.
Note
Messages are only available if they are enabled in your organization.
See feed updates from groups:
Click a group name or search for a group to view its feed or to post updates. You can post in all public groups and any private groups that you belong to.
Click 
Join
 to join a group, or 
Member
 to leave the group.
Click 
 to view the group description.
See and comment on records you follow. From the drop-down menu at the top, select the type of record you want to view, for example, Account. If you’re not following records, there won’t be any records in the list. Click a record’s name to view its details. Chatter Desktop shows the first eight fields on a record.
Edit your 
Chatter Desktop settings
 or 
switch your connection
.
Instantly synchronize your data. While Chatter Desktop synchronizes your data automatically, it does not always happen instantly.
See Also:
Chatter Desktop Troubleshooting Tips
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_desktop_using.htm&language=en_US
Release
202.14
