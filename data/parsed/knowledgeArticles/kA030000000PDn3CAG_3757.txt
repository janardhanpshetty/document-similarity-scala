### Topic: Notifications coming from the notification option on Case assignment rules comes from the Case creator.
Where does the senders email come from in a Notification trigger using Case Assignment rules that came from Email to Case?
Resolution
Notifications coming from the notification option on Case assignment rules comes from the Case creator.
The Case creator is the user that is configured to connect to Salesforce CRM using the Salesforce Email to Case Agent. This user is set in the sfdcConfig.txt under the Salesforce Login.
To change the name, you have to change the name that is used in the sfdcConfig.txt. Change the users first and last name and/or email address.
