### Topic: Report on Relationship Groups | Salesforce
Report on Relationship Groups | Salesforce
Report on Relationship Groups
You can report on relationship groups and relationship group members if your administrator has enabled custom report types for those custom objects.
Available in: Salesforce Classic
Available in: Salesforce for Wealth Management
User Permissions Needed
To create or update custom report types:
“Manage Custom Report Types”
To delete custom report types:
“Modify All Data”
Custom report types are the only way to make relationship group reports available for your users—Salesforce does not provide sample relationship group reports or a standard report folder for relationship groups.
Administrators can create a public folder of relationship group reports as follows:
Create a custom report type for relationship group objects.
Tip
To create a report type about the members of relationship groups, select 
Relationship Groups
 as the primary report type object and add 
Relationship Group Members
 as an object relationship. Alternatively, to create a report type about the accounts that are primary on a relationship group, select 
Accounts
 as the primary report type object and add 
Relationship Groups (Primary Account)
 as an object relationship.
Create a new public folder for relationship group reports. This step requires the “Manage Public Reports” permission.
Using your custom report type, create one or more new custom reports for relationship groups. Assign the reports to the new relationship groups reports folder you created.
After completing these steps, a folder of relationship group reports will be available to users on the Reports home page.
Parent topic:
 
Pre-Designed Custom Report Types
Previous topic:
 
Report on Partners
Next topic:
 
Enable the Account Owner Report
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=relgroups_reports.htm&language=en_US
Release
202.14
