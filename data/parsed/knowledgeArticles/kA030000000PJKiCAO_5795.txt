### Topic: Learn how to access the Radian6 Summary Dashboard from your browser and the Engagement Console.
The Summary Dashboard leverages information from the Radian6 platform and provides a high level view of results from your Topic Profile and applied insights. You can access the Summary Dashboard in a few different ways, outlined below. 
Resolution
Access the Summary Dashboard from your browser
 
1. Go to 
https://summary.radian6.com/login.action
.
2. Enter your Radian6 Username and 
password
.
3. Click 
Login.
 
Access the Summary Dashboard from the Engagement Console
 
1. Log in to the 
Engagement Console
 with your Radian6 Username and password.
2. Click the gauge icon in the top-right corner.
Tip:
 If you hover over the icon, a tool tip shows "Launch Summary Dashboard."
 
 
3. From the login in window, enter your Username and 
password
.
4. Click
 Login.
