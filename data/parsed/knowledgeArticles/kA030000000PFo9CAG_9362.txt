### Topic: This article explains about as to why Developer Console does not load for non-admin users via subscriber support.
Issue:
====
Logging in via subscriber support as a user who does not have system administrator or "manage users" or "view all data" permissions and trying to open the developer console results in a blank page. 
 
On the backend the dev console is making a call to /services/data/v25.0/query/ which fails with a HTTP 400 error and the resulting XML: 
<Errors> 
<Error> 
<errorCode>INVALID_SESSION_ID</errorCode> 
<message>Session expired or invalid</message> 
</Error> 
</Errors> 
 
On firebug the error says:
====================
GET https://na12.salesforce.com/services/data/v25.0/qu...20ScopeId%3D'005U0000000PPYuIAO'&_dc=1341854128954 
400 Bad Request 
[{"message":"sObject type 'TraceFlag' is not supported.","errorCode":"INVALID_TYPE"}] 
Resolution
Steps to Reproduce: 
=================== 
1. Login to LMO Org 
2. Go to Subscribers Tab
3. Click on any "Test" Org 
4. Go to "Login Access Granted" section and login for "Std User" ( Login access already granted in sub Org for the subscriber support)
5. Go to Dev Console >> Blank Screen 
 
This is WAD and is not properly documented yet for non-sysadmin customers granting login access to partner via subscriber support (Partner BT) and it only seems to work for admin users.
 
These are the steps that should be followed: 
 
1) Standard user in sub org grant login access to the partner 
2) Standard user in sub org asks his/her admin to grant login access to the partner 
3) The partner logs in using partner black tab as the admin and add the standard user to the list of monitored users 
4) The partner logs in using partner black tab as the standard user and he can see the logs 
 
Kindly follow investigation for details:
https://gus.salesforce.com/a07B0000000FmJIIA0
