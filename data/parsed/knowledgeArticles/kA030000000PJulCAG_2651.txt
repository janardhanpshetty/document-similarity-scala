### Topic: The method Limits.getScriptStatements() always returns 0 when it is called within a call to the batch execute() method whereas we expect it to returns number of rows i.e. number of times it executed.
Can we use limit function in Apex Batch.
The method Limits.getScriptStatements() always returns 0 when it is called within a call to the batch execute() method whereas we expect it to returns number of rows i.e. number of times it executed.
/////////CODE SNIPPET
public class BatchLimitTest implements Database.Batchable<SObject> { 
          public database.Querylocator start(Database.BatchableContext bc) { 
                 return Database.getQueryLocator('select name from account limit 1'); 
          } 
          public void execute(Database.BatchableContext bc, Sobject[] result) { 
                 for(integer i=0; i < 10; i++) { 
                    Integer a= 0; 
                    a++; 
                 } 
                 system.debug(Limits.getScriptStatements()); 
          } 
          public void finish(Database.BatchableContext bc) { } 
}
 
Resolution
We cannot use the  Limit.getScriptStatements() in apex batch because this is using async process to go through the record. The whole point of doing async is so that you do not hit the governor limits.
S
cript statement limits do not get counted in any of these batch methods -
start,execute or finish
This is working as designed.
