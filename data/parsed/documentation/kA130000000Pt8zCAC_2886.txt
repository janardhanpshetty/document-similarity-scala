### Topic: Page Layouts | Salesforce
Page Layouts | Salesforce
Page Layouts
Page layouts control the layout and organization of buttons, fields, s-controls, Visualforce, custom links, and related lists on object record pages. They also help determine which fields are visible, read only, and required. Use page layouts to customize the content of record pages for your users.
Available in: both Salesforce Classic and Lightning Experience
Page layouts are available in: 
All
 Editions
Creation and deletion of page layouts is available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, and delete page layouts:
“Customize Application”
Page layouts can include s-controls and Visualforce pages that are rendered within a field section when the page displays. You can control the size of the s-controls and Visualforce pages, and determine whether or not a label and scroll bars display.
Salesforce has two drag-and-drop tools for editing page layouts: the original page layout editor and an enhanced page layout editor. The enhanced page layout editor is enabled by default, and provides all the functionality of the original editor, as well as additional functionality and an easier-to-use WYSIWYG interface.
You can enable the original page layout editor in the User Interface settings. Your Salesforce org can use only one page layout editor at a time.
From within a page layout, you can access a mini page layout. The mini page layout defines the hover details that display when you mouse over a field on an object’s detail page in the Agent console or in the Recent Items section of the sidebar in Salesforce Classic.
For Personal, Contact Manager, Group, and Professional Edition orgs, every user views the same layout. Professional, Enterprise, Unlimited, Performance, and Developer Edition orgs can create different page layouts for use by different profiles and record types and set field-level security settings to further restrict users’ access to specific fields.
In Professional, Enterprise, Performance, Unlimited, and Developer Editions, you can set the mini page layouts and related objects that appear in the Console tab.
See Also:
Page Layouts and Field-Level Security
Edit Page Layouts for Standard Objects
Edit Page Layouts for Custom Objects
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_layout.htm&language=en_US
Release
202.14
