### Topic: Manage Libraries | Salesforce
Manage Libraries | Salesforce
Manage Libraries
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create libraries:
“Manage Salesforce CRM Content”
OR
“Create Libraries”
To edit libraries:
“Manage Salesforce CRM Content”
OR
Manage Libraries
 checked in your library permission definition
On the 
Libraries
 tab, access your private library, create new libraries, choose libraries to view or edit, and analyze library usage and activity. For details about the publishing options at the top of the page, see 
Upload and Publish Content
. The libraries home page has two tabs: Shared Content, which provides information about shared libraries, and My Private Files, which contains information about your private library.
Shared Content
The Shared Content tab on the libraries home page contains the following sections:
My Libraries
This section lists all the libraries to which you have access. Click a library name to view details about that library or click 
Browse
 to view a list of all the content in the library. Click the 
New
 button to create new libraries, add users to a library, or assign library permissions to users.
Featured Content
This section lists the five pieces of content in your libraries most recently designed as “featured.” Featured content receives a higher priority than similar content in search results; for example, if 100 files contain the search criteria term 
sales asset
, any featured files with that term will appear at the top of the search results list. To see all featured content, click 
Show All
. To toggle a piece of content's feature status on or off, go to its 
content details page
.
Top Content
This section includes lists that summarize content activity across all your libraries. 
Each list sorts content according to specific criteria. Within a list, click a file icon to download content or click a title to open the associated content details page. In the Top Content section you can choose from the following categories:
 
Publication Date
—This content is sorted in descending order according to the most recent publication date. Choose the number of records you want to view from the accompanying drop-down list, or click the 
Show All
 button to list all the published files, Web links, and Google docs.
Num Downloads
—This content is sorted in descending order according to the highest number of downloads. The bar graphic indicates how one record compares to another. Choose the number of records you want to view from the accompanying drop-down list, or click the 
Show All
 button to list all the downloaded content.
Rating
—This content is sorted in descending order according to the highest number of thumbs-up votes. Green and red in the bar graphic represent positive and negative votes, respectively. Choose the number of records you want to view from the accompanying drop-down list, or click the 
Show All
 button to list all the content with votes.
Num Comments
—This content is sorted in descending order according to the highest number of viewer comments. The bar graphic indicates how one record compares to another. Choose the number of records you want to view from the accompanying drop-down list, or click the 
Show All
 button to list all the content with associated comments.
Popular Tags
This section, commonly referred to as a “tag cloud,” shows you how the content in your libraries has been labeled. 
Tags are descriptive terms assigned during upload or revision that help classify and organize content. Click a tag name to view search results containing all the files, Web links, and Google docs with that tag. The tag names increase in size within the tag cloud according to popularity, meaning that the largest tags have been assigned to the most content. You can choose to sort the tags alphabetically or by popularity. The tag cloud contains the 30 most popular tags.
Recent Activity
This section is a snapshot of activity within your libraries. 
It shows the most recent files, Web links, and Google docs to receive comments, votes, or subscriptions. Featured content and newly published content are also included, but new versions of existing content, archived content, and deleted content do not appear in the Recent Activity section. Use the 
Older
 and 
Newer
 buttons to scroll through records. The Recent Activity section contains a maximum of 100 records.
Most Active Contributors
This section shows the authors who have published content into your libraries most frequently. 
The names increase in size according to activity, so the largest names are the authors who have contributed the most content. 
My Private Files
The My Private Files tab on the libraries home page is your private library. When you upload or create content and do not select a public library, your content is stored in your private library. You can publish content to a public library at any time or leave content in your private library indefinitely. Content in your private library can be assembled in content packs. It can also be sent to leads and contacts outside your organization using the content delivery feature. See 
Set Up Content Deliveries
 for more information. The My Private Files tab has the following sections:
Private Library
If you choose the 
Save to my private library
 option when publishing a file, Web link, content pack, or Google doc, your content is saved here. You can publish or delete files from this list or click the file's name to view its content details page. The following options that are available on the content details page for shared content are not available for private-library content: tagging, rating, subscribing, tracking downloads, tracking subscriptions, or using custom fields. If you publish a file from the Private Library list and click 
Cancel
 during the publishing process, your file is deleted.
Upload Interrupted
If an error occurs when you are uploading a new file, for example your browser crashes or your session times out, the file you were uploading is saved here. Click 
Publish
 to publish the file to a public library or save it to your private library. If you click 
Cancel
 on the Save or Publish Content dialog, your file will be deleted.
Revision Upload Interrupted
If an error occurs when you are uploading a new version of a file, the file you were uploading is saved here. Users can continue to access the original version. Click 
Publish
 to publish the file to a public library or save it to your private library. If you click 
Cancel
 on the Save or Publish Content dialog, your file will be deleted.
See Also:
Set Up Content Deliveries
Create and Modify Content Packs in Salesforce CRM Content
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=content_workspace.htm&language=en_US
Release
202.14
