### Topic: Let Users Respond to Approval Requests by Email | Salesforce
Let Users Respond to Approval Requests by Email | Salesforce
Let Users Respond to Approval Requests by Email
If the email notification includes all the information that an approver needs to decide, enable email approval response. That way, a user can simply reply to the email notification.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Considerations for Email Approval Response
Before you enable the ability to act on approvals via email, review how email works with your approval processes.
Default Template for Email Approval Response
When you enable email approval response, Salesforce uses a default email template for approval processes—unless you specify a custom email template.
Enable Email Approval Response
After you’ve reviewed the considerations and prepared the right template, flip the switch that lets users respond to approval requests directly from their email.
See Also:
Prepare Your Org for Approvals
Let Users Respond to Approval Requests from Chatter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_email_parent.htm&language=en_US
Release
202.14
