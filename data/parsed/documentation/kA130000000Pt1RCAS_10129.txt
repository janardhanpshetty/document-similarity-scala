### Topic: Standard and Enhanced Lookups in Salesforce Classic | Salesforce
Standard and Enhanced Lookups in Salesforce Classic | Salesforce
Standard and Enhanced Lookups in Salesforce Classic
Salesforce objects often include 
lookup fields
 that allow you to associate two records together in a relationship. For example, a contact record includes an 
Account
 lookup field that associates the contact with its account. Lookup fields appear with the 
button on record edit pages. Clicking the icon opens a lookup search dialog that allows you to search for the record that you want to associate with the record you’re editing. There are two main types of lookups: standard and enhanced.
Available in: Salesforce Classic
Available in: 
All 
Editions 
except Database.com
Standard Lookups in Salesforce Classic
Standard lookups search through a limited set of searchable fields per object and users can’t refine search results.
Enhanced Lookups in Salesforce Classic
Enhanced lookups offer more search capabilities than standard lookups. In addition, enhanced lookups allow search result sorting and filtering.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_lookupdialog.htm&language=en_US
Release
202.14
