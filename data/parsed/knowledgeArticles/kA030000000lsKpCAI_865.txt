### Topic: This article provides the list of steps to follow to sign up an org on a preview instance using internal tools and publicly accessible tools.
How to sign up an org on a preview instance?
Resolution
The process to sign up a preview instance is straight-forward. There are 3 ways in which you can easily signup on a preview instance for either training or regression testing purposes.
-BT Signup- 
(Restriced to certain BT profiles)
You should first identify the instance with the release version desired. You then should access this identified instance via BT and finally sign up directly in the instance in question. The recommended steps are as follows:
1. Use 
Version O Matic
 for the full list of instances and the latest versions they are currently on
2. Identify an instance with the latest release applied, for example Summer '15 (196.<minor>.<patch>)
3. Once an instance has been identified, access the instance directly via BT
4. On the BT page for the required instance, click '
Signup
' / 
Check out the video guide on the usage of this page
 (Password = FasterSignup)
5. You will be presented with a sign up template for the required instance, fill out all the required template fields, taking particular care on the username and password fields. For convenience, un-check the 'Require password reset' field
6. Finally ensure the field '
Force signup on local instance
' is checked
-Pre-Release Org Signup-
 (Publicly Accessible) 
1. Using the Summer '15 release as an example the signup URL will be 
https://www.salesforce.com/form/signup/prerelease-summer15.jsp
 which will sign you up an org on GS0 instance which will be the first instance to receive a new release.
2. This URL will change each release as it becomes available while the naming scheme does stay the same you can modify the URL accordingly. e.g. the next release, Winter '16, would be 
https://www.salesforce.com/form/signup/prerelease-winter16.jsp
-Blitz Orgs-
 (Internal only, similar to BT Signup)
1. Login to SFM via Aloha to become authenticated. 
2. Navigate to Luna Environment listing page 
https://build.soma.salesforce.com/build/Environment/list
 and check which Blitz is on the newest release (note that there are 4 instances and it's usually on the previous release, the current release, the next release and the main build). e.g. in the example of Summer '15 we look for which Blitz has 196 on it and then use the login required. Note that these logins and URLs can change from time to time.
Please log a KBFeedback if any are failing with the login information but also note that the stability of the blitz instances is not guaranteed and they can be offline at times. 
Blitz01
Blitz02
Blitz03
Blitz04
(Blitz Login Information Updates will show here - 
https://sites.google.com/a/salesforce.com/newbt/new-bt-users-for-blitz
 )
3. Once you are logged into the correct Blitz instance go to the SysAdmin Tab
4. On the SysAdmin BT page, click 'Signup'
5. You will be presented with a sign up template, fill out all the required template fields, taking particular care on the username and password fields. For convenience, un-check the 'Require password reset' field
6. Finally ensure the field 'Force signup on local instance' is checked
