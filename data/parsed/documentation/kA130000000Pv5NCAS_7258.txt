### Topic: Stacked Column Charts | Salesforce
Stacked Column Charts | Salesforce
Stacked Column Charts
Use a stacked column chart when you have multiple groupings and you’re interested in the proportions between values in each grouping, as well as each grouping's total.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
For example, to compare the number of opportunities created each month by campaign source in a report, and also to compare the totals for each month, set record count as the 
Y-axis
, created month as the 
X-axis
, and source as the 
Groupings
 value. The chart displays a single bar for each month, broken down by source, with each source shown in a different color.
The proportion of each source in each month is easy to compare, as are the monthly totals, but comparing a single source’s contribution to different months, or to the total, may be difficult.
Parent topic:
 
Column Charts
Previous topic:
 
Grouped Column Charts
Next topic:
 
Stacked Bar Charts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=chart_column_stacked.htm&language=en_US
Release
202.14
