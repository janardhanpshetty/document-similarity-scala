### Topic: Can I follow someone from my Twitter Managed Account in the Engagement Console?
Can I follow someone from my Twitter Managed Account in the Engagement Console?
Resolution
To follow a Twitter account from a Twitter Managed Account in the Engagement Console: 
1. Right click the Twitter user's avatar in a Twitter Managed Account stack.
2. Click 
Follow
. 
3. Choose which of your accounts you want to follow the Twitter user's account.
 
