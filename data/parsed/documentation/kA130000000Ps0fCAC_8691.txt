### Topic: Define Custom Settings | Salesforce
Define Custom Settings | Salesforce
Define Custom Settings
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Developer
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Database.com
 Editions.
Packages are not available in 
Database.com
.
User Permissions Needed
To manage, create, edit, and delete custom settings:
“Customize Application”
To create or edit a custom setting:
From Setup, enter 
Custom Settings
 in the 
Quick Find
 box, then select 
Custom Settings
.
Click 
New
 to create a new custom setting, click 
Edit
 next to the name of a custom setting, or click 
Edit
 while viewing the details of a custom setting.
Note
A 
icon indicates that the custom setting is in an installed managed package. You can’t edit or delete a custom setting installed from a managed package.
Define the following:
Label
—Enter the label displayed in the application.
Object Name
—Enter the name to be used when the custom setting is referenced by formula fields, validation rules, Apex, or the SOAP API.
Note
Salesforce recommends using ASCII for the 
Object Name
. The name can't exceed 38 ASCII characters. If you use double byte, there are additional limits on the number of characters allowed.
Setting Type
—Select a type of List or Hierarchy. The List type defines application-level data, such as country codes or state abbreviations. The Hierarchy type defines personalization settings, such as default field values, that can be overridden at lower levels in the hierarchy.
Important
After you save a custom setting, you cannot change this value.
Visibility
—Select a visibility of Protected or Public.
Protected—If the custom setting is contained in a managed package, subscribing organizations can't see the custom setting: it doesn't display as part of the package list. In addition, subscribing organizations can't access the custom setting using either Apex or the API, however, developer organizations can. If the custom setting is contained in an unmanaged package, the custom setting is available through the Enterprise WSDL like any custom object (as if the 
Visibility
 was Public.)
Public—The custom setting is available through the Enterprise WSDL like any custom object. You can package custom settings defined as public. The subscribing organizations can edit the values, as well as access them using Apex and the API, regardless of the type of package (either managed or unmanaged).
Important
After you save a custom setting, you cannot change this value.
Enter an optional description of the custom setting. A meaningful description will help you remember the differences between your custom settings when you’re viewing them in a list.
Click 
Save
.
Note
Only custom settings definitions are included in packages, not data. If you need to include data, you must populate the custom settings using a standard Apex or API script run by the subscribing organization after they have installed the package.
After you create a custom setting, you must also add fields to the custom setting.
See Also:
Add Custom Settings Fields
Add Custom Settings Data
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cs_define.htm&language=en_US
Release
202.14
