### Topic: Steps on editing a Site.com Guest User Profile to have the Person Accounts enabled.
In order for Salesforce.com Support to enable the Person Accounts feature, all Profiles that have 
Read 
access to Accounts must also have 
Read 
access to Contacts.
This is already set for Standard Profiles however some organizations with an early configuration of 
Site.com
 have a 
Guest User Profile
 that may not have this set correctly. Additionally, these Profiles do not display under 
Setup > Manager Users>Profiles
 which makes it difficult to locate them to make appropriate changes.
They can be accessed via the following steps.
Resolution
1. Click Setup -> Build -> Develop -> Sites 
2. Click the site on the Site Label column 
3. Public Access Settings 
4. Edit 
