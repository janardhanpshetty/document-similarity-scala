### Topic: We can have debug logs where status is showing as 'Success' even if the operation failed, In the logs detail we can find "Governor limits exception" (eg: System.LimitException: Too many SOQL queries: 101)
We can have debug logs where status is showing as 'Success' even if the operation failed, In the logs detail we can find "Governor limits exception" (eg: System.LimitException: Too many SOQL queries: 101). 
 
Resolution
Governor limits are not catchable, otherwise we would be able to work around them. Issue might be due to the execution context, if manage package is involved in between the functionality & moreover if manage package code is causing issue then we can have this situation. 
If we run below code in our Developer Console, Status reflects the exception's message ("Too many SOQL queries: 101") as expected.
--
for(integer i = 0; i<105;i++) 
system.debug([select id from lead limit 1]);
--
For more details we can check by enabling manage package debug logs.
Note : For enabling debug logs for manage package please log a case with Salesforce.
