package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{StopWordsRemover, RegexTokenizer}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
 * Created by jshetty on 9/15/16.
 * spark-submit --class similarity.ml.Cleaning --master local[12] --driver-memory 8G /Users/jshetty/spark-applications/documentsimilarity/target/scala-2.11/documentsimilarity_2.11-1.0.jar
 * word2vec instead of hashingTF
 */
object Cleaning {

  def main(args: Array[String]): Unit = {

    val getConcatenated = udf((first: String, second: String) => { first + " " + second })

    val spark = SparkSession.builder.master("local[12]").appName("Prediction of Case Reason").getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext
    sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val inputPath = "/Users/jshetty/casedata/data/reason_subject_description/reason_subject_description_1yearCapEnglish.csv"
    val input = spark.read.option("header","true").option("delimiter", "^").csv(inputPath).cache()

    val stopWordsPath = "/Users/jshetty/spark-applications/documentsimilarity/input/stopwords/english_custom_stopwords.txt"
    val stopwords = sc.textFile(stopWordsPath).collect()

    val textInput = input.withColumn("text", getConcatenated(col("SUBJECT"), col("DESCRIPTION")))

    val tokenizer = new RegexTokenizer().setInputCol("text").setOutputCol("words").setPattern("\\W")
    val textFiltered = new StopWordsRemover().setStopWords(stopwords).setInputCol("words").setOutputCol("wordsFiltered")
    val stringMaker = new StringMaker().setInputCol("wordsFiltered").setOutputCol("tex")

    val pipeline = new Pipeline().setStages(Array(tokenizer, textFiltered, stringMaker))
    val model = pipeline.fit(textInput)
    val result = model.transform(textInput)

    result.select("REASON", "tex").coalesce(1).write.option("delimiter", "^").csv("/Users/jshetty/casedata/data/reason_subject_description/result10")
    spark.stop()
  }
}
