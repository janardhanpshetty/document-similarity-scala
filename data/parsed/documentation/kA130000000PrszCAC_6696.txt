### Topic: Export Data | Salesforce
Export Data | Salesforce
Export Data
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To export records:
“Read” on the records
To export all records:
“Read” on the records
You can use the Data Loader export wizard to extract data from any Salesforce object. When you export, you can choose to include (
Export All
) or exclude (
Export
) soft-deleted records.
Open the Data Loader.
Click 
Export
 or 
Export All
. These commands can also be found in the File menu.
Enter your Salesforce username and password. Click 
Log in
 to log in. After your login completes successfully, click 
Next
. (Until you log out or close the program, you will not be asked to log in again.)
If your organization restricts IP addresses, logins from untrusted IPs are blocked until they’re activated. Salesforce automatically sends you an activation email that you can use to log in. The email contains a security token that you must add to the end of your password. For example, if your password is 
mypassword
, and your security token is 
XXXXXXXXXX
, you must enter 
mypasswordXXXXXXXXXX
 to log in.
Choose an object. For example, select the Account object. If your object name does not display in the default list, check 
Show all objects
 to see a complete list of objects that you can access. The objects will be listed by localized label name, with developer name noted in parentheses. For object descriptions, see the 
SOAP API Developer's Guide
.
Click 
Browse...
 to select the CSV file to which the data will be exported. You can enter a new file name to create a new file or choose an existing file.
If you select an existing file, the contents of that file are replaced. Click 
Yes
 to confirm this action, or click 
No
 to choose another file.
Click 
Next
.
Create a SOQL query for the data export. For example, check 
Id
 and 
Name
 in the query fields and click 
Finish
. As you follow the next steps, you will see that the CSV viewer displays all the Account names and their IDs. SOQL is the Salesforce Object Query Language that allows you to construct simple but powerful query strings. Similar to the SELECT command in SQL, SOQL allows you to specify the source object, a list of fields to retrieve, and conditions for selecting rows in the source object.
Choose the fields you want to export.
Optionally, select conditions to filter your data set. If you do not select any conditions, all the data to which you have read access will be returned.
Review the generated query and edit if necessary.
Tip
You can use a SOQL relationship query to include fields from a related object. For example:
Select Name, Pricebook2Id, Pricebook2.Name, Product2Id, Product2.ProductCode FROM PricebookEntry WHERE IsActive = true
Or:
Select Id, LastName, Account.Name FROM Contact
When using relationship queries in Data Loader, the fully specified field names are case-sensitive. For example, using 
ACCOUNT.NAME
 instead of 
Account.Name
 does not work.
Data Loader doesn’t support nested queries or querying child objects. For example, queries similar to the following return an error:
SELECT Amount, Id, Name, (SELECT Quantity, ListPrice,
PriceBookEntry.UnitPrice, PricebookEntry.Name,
PricebookEntry.product2.Family FROM OpportunityLineItems)
FROM Opportunity
Also, Data Loader doesn’t support queries that make use of polymorphic relationships. For example, the following query results in an error:
SELECT Id, Owner.Name, Owner.Type, Owner.Id, Subject FROM Case
For more information on SOQL, see the 
Force.com SOQL and SOSL Reference
.
Click 
Finish
, then click 
Yes
 to confirm.
A progress information window reports the status of the operation.
After the operation completes, a confirmation window summarizes your results. Click 
View Extraction
 to view the CSV file, or click 
OK
 to close. For more details, see 
Reviewing Data Loader Output Files
.
Note
Data Loader currently does not support the extraction of attachments. As a workaround, we recommend that you use the weekly export feature in the online application to export attachments.
If you select compound fields for export in the Data Loader, they cause error messages. To export values, use individual field components.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=exporting_data.htm&language=en_US
Release
202.14
