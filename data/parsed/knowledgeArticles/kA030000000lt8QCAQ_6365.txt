### Topic: The article will provide information on what changes are required for Salesforce for outlook Side Panel for searching / matching emails
With the Spring 15 release and Salesforce for Outlook 2.7 the email address matching between the email addresses in your Outlook emails and the ones in all email fields for your Salesforce contacts and leads has improved significantly
Now, the side panel matches email addresses in Outlook emails to all email address fields for contacts and leads in Salesforce - even the custom ones, which means the Side Panel has better chances of finding relevant contacts and leads related to the emails that matter to you
Resolution
No changes are needed, this feature is already enabled by default.
 
