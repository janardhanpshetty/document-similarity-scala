### Topic: Set Up Reputation Levels | Salesforce
Set Up Reputation Levels | Salesforce
Set Up Reputation Levels
Update the default reputation levels to meet your community’s needs and help motivate your members.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To update reputation levels:
“Manage Communities”
AND
Is a member of the community whose Community Management page they’re trying to access.
When reputation is enabled for a community, 10 default levels are added. You can add or remove levels, give each level a name, and update the point range and image for each level.
Note
A community must have at least three reputation levels and can have up to 50.
Open Community Management
.
Click 
Reputation
 | 
Reputation Levels
.
From here you can:
Upload your own image for each reputation level. Click the default image to browse to an image file and upload a new image.
Note
You can’t revert to the default reputation level images from the Salesforce user interface. Use the Salesforce Chatter REST API to do this.
Give each level a name, such as “Beginner,” “Intermediate,” and “Expert.” If you don’t assign a name, the default is used. For example, “Level 1,” “Level 2,” “Level 3.”
Edit the point range for a level.
When you update the higher value of a level’s point range, the lower value for the next level is automatically adjusted when you save.
Add more levels by clicking 
Add a level
, located underneath the list of levels.
Remove a level by clicking 
 next to the level.
Click 
Save
 to apply your changes.
You can now update the point system for the community. The point system determines how many points a user gets when they perform certain actions or when others recognize their contributions by commenting, liking, or sharing their posts. Reputation level increases are posted to member feeds.
You can 
translate
 reputation level names so that international community members can view their reputation levels in the appropriate language. In Translation Workbench, select the 
Reputation Level
 setup component and then expand the node next to your community.
See Also:
Set Up Reputation Points
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_reputation_level_setup.htm&language=en_US
Release
202.14
