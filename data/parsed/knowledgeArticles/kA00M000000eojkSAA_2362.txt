### Topic: Learn why you're not seeing the envelope icon in Salesforce for Outlook for adding emails.
If you have the Add Email option enabled in your Outlook Configuration, and you're not seeing the Add Email envelop icon
, we'll cover how to solve that problem. 
Resolution
This issue is typically profile permission related.  Check to see if the 
'Edit Tasks'
 perm is disabled on the custom profile. If it is, you won't have access to create Tasks.  
If this article didn't solve your problem, you'll want to read our help article 
Add Email envelope icon is missing from the Salesforce for Outlook in the Side Panel
.
