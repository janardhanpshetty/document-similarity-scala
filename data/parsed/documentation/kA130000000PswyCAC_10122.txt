### Topic: Sending Chatter Messages | Salesforce
Sending Chatter Messages | Salesforce
Sending Chatter Messages
Send messages to communicate privately in Chatter.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
To send a Chatter message:
Start the message from one of these locations:
Click the 
Messages
 link
 on the Chatter tab
, then click 
New Message
 in My Messages.
Click a person's name anywhere in Chatter to view their profile and click 
Send a message
.
Click 
Send a message
 on a person's hover.
When viewing the full message history of a conversation, use the text box under the most recent message to send a reply.
When viewing the email notification about a message you've received, 
if email replies to Chatter are enabled, 
reply to the email.
Sharing a file with people is another way to send a Chatter message. When you share a file, recipients automatically receive a message that lets them know the file has been shared, as well as any additional information you provided.
If you started in the Send a Message dialog box, you can add people's names to the recipient list. 
Type a name in the 
To
 field and click the name to select it. Add more people to the conversation by entering additional names.
If you're replying within a conversation or via email, the conversation participants can't be changed.
Write your message. Messages can be up to 
10,000
 characters.
Submit your message using the appropriate method:
In the Send a Message dialog box, click 
Send
.
If you're replying within a conversation, click 
Reply
 (or 
Reply All
 if the conversation involves multiple recipients).
If you're replying via email, use the Send option in your email application.
If your message continues an existing conversation, your reply is added to the conversation and appears at the top of the list in My Messages. If your message starts a new conversation because you haven't previously exchanged messages with that unique combination of recipients, your message appears at the top of the list in My Messages as a brand new conversation.
See Also:
Replying to Chatter Email Notifications
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_message_send.htm&language=en_US
Release
202.14
