### Topic: Communities Statuses | Salesforce
Communities Statuses | Salesforce
Communities Statuses
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Communities can have one of the following statuses.
Status
Description
Preview
Customization of the community isn’t complete, and the community has never been activated. Once you activate a community, you can’t go back to 
Preview
 status.
Users with “Create and Set Up Communities” can access communities in 
Preview
 status if their profile or permission set is associated with the community. They can also share a link to these communities with users whose profiles or permission sets are associated with the community. The link for sharing a 
Preview
 community is located on the Community Management page.
No welcome emails are sent even if 
Send welcome email
 is selected.
Note
If your organization’s access to Communities is suspended for non-payment of fees due, all of your communities are deactivated, including those in 
Preview
 status. When Communities is enabled again, all communities are in 
Inactive
 status. You can activate these communities, but can’t return them to 
Preview
 status.
Inactive
The community was previously 
Active
 but was deactivated.
You may want to deactivate a community if you need to:
Add or remove members
Add, remove, or change the order of tabs
Change the color scheme
Change the community URL
When you deactivate a community , it no longer appears in the drop-down menu. Users with “Create and Set Up Communities” can still access the setup for 
Inactive
 communities regardless of membership. If members try to access 
Inactive
 communities using a direct link, they see an error page.
Active
The community is active and available to members.
Welcome emails are sent to new members if 
Send welcome email
 is selected.
See Also:
Customize Communities
Make Your Communities Active
Deactivate a Community
Share a Link to Your Community
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_status.htm&language=en_US
Release
202.14
