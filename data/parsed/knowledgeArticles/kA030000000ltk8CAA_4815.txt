### Topic: Coaching metrics define measurable results to track as part of the coaching conversation.
Metrics are used to define measurable results to track as part of the coaching conversation. For metrics to show up in the coaching record, create goals and metrics associated with those goals, and the metrics tied to the coachee's goals will be displayed in the coaching space.
 
For each metric, you can specify a name, the type, target and due date. You can also link the metric to a Salesforce report so that the metrics are updated based on data that’s already in Salesforce, such as Closed Opportunities or Cases.
 
Resolution
