### Topic: Continual requests to Salesforce servers will be seen upon examining web traffic when push notifications in the Salesforce Console for Sales or Service are enabled. The resource /cometd is leveraged via the push notification's functionality.
When examining web traffic or calls sent and received via Chrome Developer console or Fiddler for example, you may find repeated requests being made to the resource /cometd.
Resolution
Several repeated requests polling the Salesforce servers from the cometd resource is expected when push notifications are enabled for the Salesforce Console for Service (formerly known as the Service Cloud Console) or Salesforce Console for Sales.
The requests associated to the push notification's functionality should be recorded as POST methods to the resource /cometd and be accompanied by the current API version, in the example below for illustration of the behavior it's version 28.0.
This is expected behavior and should not be a cause for concern.
See Also:
View Push Notifications in a Salesforce Console
