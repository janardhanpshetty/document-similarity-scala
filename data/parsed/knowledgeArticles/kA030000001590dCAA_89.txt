### Topic: Add field Primary Campaign Source to opportunity page layout.
On Opportunity Record when we click on 
"Add To Campaign"
 on the Campaign Influence related list, 
Primary Campaign Source
 checkbox field is not available, even though on opportunity page layout for 
Campaign Influence
 related list, 
Primary Campaign Source
 checkbox is selected.
Secondly, the 
"Automatic Association"
 is disabled from setup on campaign influence.
Click path to check whether 
"Automatic Association"
 is Enabled or Disabled: 
Click on Setup | Customize | Campaigns | Campaign Influence | Automatic Association | Disable.
 
Resolution
Add the field 
Primary Campaign Source
 to opportunity page layout. 
Below is the step to add the field from page layout:
Click Opportunity Tab | Click on any opportunity record | Click Edit Layout | Drag and Drop 
Primary Campaign Source field 
| Click Save.
 
