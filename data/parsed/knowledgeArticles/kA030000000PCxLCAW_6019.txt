### Topic: Learn if you can develop Apex code in the Developer Edition and implement it in Professional Edition.
Resolution
Yes, you can 
develop Apex code in Developer Edition and deploy it to Professional Edition.
 You can accomplish this by installing a managed package that was created in a Developer Edition, and contains the desired apex code in a Apex Class or Apex Trigger in a Professional or Group Edition.
 
Considerations before deploying code to another edition
 
The managed package must 
go through the AppExchange Security Review
.
If you pass the security review, let them know you want to authorize your managed package.
After authorization is complete install your managed package with apex code in Group Edition and Professional Edition.
