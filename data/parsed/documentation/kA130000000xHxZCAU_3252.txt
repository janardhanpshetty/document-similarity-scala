### Topic: Guidelines for Creating Opportunities | Salesforce
Guidelines for Creating Opportunities | Salesforce
Guidelines for Creating Opportunities
Many of your Salesforce org’s opportunities can be generated when you convert qualified leads, but you can also create opportunities manually in both Salesforce Classic and Lightning Experience.
Available in: Salesforce Classic and Lightning Experience
Opportunity Splits available in: Salesforce Classic
Available in: 
All
 Editions for orgs activated before Summer ’09
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions for orgs activated after Summer ’09
Opportunity Splits and territory management available in: 
Performance
 and 
Developer
 Editions and in 
Enterprise
 and 
Unlimited
 Editions with the Sales Cloud
Contacts and Contact Roles
To associate an opportunity with an account or contact, you must have at least read access to the account or contact.
In Salesforce Classic, when you create an opportunity from a contact record, that contact is automatically listed as the primary contact in the Contact Roles related list of the opportunity. In Lightning Experience, when you create an opportunity from a contact record, you’re prompted to assign a contact role to that contact.
Divisions
If your organization uses Divisions, the division of a new opportunity is automatically set to the division of the related account.
Forecasts
In Professional, Enterprise, Unlimited, Performance, and Developer Editions, opportunities you create are automatically given the forecast category that corresponds to the opportunity stage you assign. Your administrator correlates opportunity stages and forecast categories when editing the 
Stage
 picklist values.
If the opportunity is set to close in a given month, as determined by the 
Close Date
, the opportunity is automatically added to the forecast for that particular month, unless the forecast category on the opportunity is set to 
Omitted
.
Multicurrency
If your organization uses multiple currencies, your opportunity amounts are initially shown in your personal currency. Change the 
Opportunity Currency
 picklist to track the sales revenue in another currency.
Opportunity Splits
When team selling and splits that total 100% are enabled, you’re automatically added to the opportunity team and initially assigned 100% of the split.
Territory Management
If your organization uses 
territory management
, opportunities that you create are automatically assigned a territory when both of the following conditions are met.
You belong to or have edit privileges on the same territory as the account on the opportunity.
You have no other territories in common with that account. (Note that if you have the “Manage Territories” permission, you have access to all territories, including the parent territories of the account. In this case, you 
do
 have other territories in common with the account, unless the account is only assigned to a single top-level territory. No territory is assigned to the opportunity.) 
For example, if you are in the territories Berkeley and San Francisco and the account is in the territories Portland, San Francisco, and Seattle, the opportunity is assigned to the San Francisco territory.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=opp_guidelines_aloha.htm&language=en_US
Release
202.14
