### Topic: An overview of the technical architecture of MicroSoft Sharepoint 2010.
Foreword
This article provides a basic overview of Sharepoint's architecture. It is intended for Support agents or other technical staff who need to have an idea of how Sharepoint is set up or simply want to look up a terminology on this product.
 
 
 
Resolution
What is Sharepoint?
SharePoint is a set of products that organizations often use as a business collaboration platform. SharePoint is a multi-tier application that is built on several other Microsoft technologies. For example, SharePoint requires a database and uses SQL Server for that database. SharePoint requires a web server and uses Internet Information Services
(IIS) for that web server.
Sharepoint is also assigned to a specific category of products: content management system (CMS). A CMS is a system used to add, modify, approve, and delete contents using a thin or fat client. Although it is true that Sharepoint is a CMS, SharePoint includes several features that take it far beyond the typical CMS. These features include:
 
Access to external databases and data sources
Built-in calendaring functions
Discussion forum support capabilities
File sharing through document libraries
Advanced search functions and customization
Microsoft Office application integration
Customizations and feature additions through simple web parts
Social tagging capabilities
Business process automation through workflows
 
 
Sharepoint logical architecture
2.1 
The server farm
The server farm is the top-level component in the Sharepont logical architecture. It includes every component that build up a Sharepoint environment: Databases, Application Severs and Web Front Ends.
Many organizations will implement a single-server farm and require no further deployments. However, some scenarios require the use of more than one server farm.
2.2 
The Database server
A server farm consists of many databases. Following are the three most important ones.
Configuration database
The Configuration database contains information about the other SharePoint databases: the IIS websites and web applications; trusted solutions; Web Part packages; site templates; and web application and farm settings for SharePoint products, including default quotas and blocked file types. This database is relatively small compared to other SharePoint 2010 databases, and it must be stored on the same database server as the Central Administration Content database. One Configuration database is allowed per server farm.
Central Administration Content Database
This database contains the content for the CA site. One CA Content database is allowed in the server farm, and it must be stored on the same database server as the Configuration database.
Content Database
The Content databases are among the most important databases in a SharePoint farm . They contain all of the site content. This includes files and documents in document libraries, list data, properties of web parts, audit logs, usernames and rights, and sandboxed solutions. The complete data set for a single site is contained in a single Content database; however, more than one site can be contained in a single content
database. If Office Web Applications is in use, the data for this service is also stored in the Content database.
Content databases will vary in read and write operations, depending on use. In some implementations, Content databases are write-heavy with extensive data entry operations in lists. In other implementations, the site content remains mostly static and for every change made, hundreds of view or read operations take place.
2.3 
The application server
The second server type at the server-farm level is the application server. The application server runs the service applications used by the SharePoint sites within the server farm. Multiple application servers are supported and may be used for performance improvements or for fault tolerance.
 
2.4 
The Web Front end server
The Web Front end (WFE) server provides the actual websites that users see when using the SharePoint 2010 services. The WFE servers run Internet Information Services (IIS) as the web server software. IIS serves up the ASP .NET web pages that comprise the SharePoint sites. It also drives the engine used for uploading and downloading documents to and from document libraries. In the end, IIS does most of the actual processing work that delivers content to users.
In summary, a SharePoint farm typically runs two or more Web applications. The first Web application is created automatically and is used to run SharePoint Central Administration. Each Web Front end hosts all the Web applications in the Sharepoint farm. This can be illustrated in the following figure:
 
Figure 2.1 - A typical Sharepoint farm
2.5 
Site collections
Every SharePoint site must be provisioned within the scope of an existing Web application. However, a site can’t exist as an independent entity within a Web application. Instead, every
site must also be created inside the scope of a site collection. A site collection is a container of sites. Every site collection has a top-level site. In addition to the top-level site, a site collection can optionally contain a hierarchy of child sites. Figure 2.2 shows a Web application with three site collections. The first site collection contains just a top-level site. The second contains one level of child sites below the top-level site. The third contains a more complex hierarchy with three levels.
 
Figure 2.2 -Each site collection has a top-level site and can optionally contain a hierarchy of child sites
 
 
