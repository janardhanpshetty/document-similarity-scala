### Topic: Considerations for Setting Up Enhanced Email | Salesforce
Considerations for Setting Up Enhanced Email | Salesforce
Considerations for Setting Up Enhanced Email
Consider a few tidbits of wisdom before using Enhanced Email, including limitations, details for Email-to-Case customers, and information about working with EmailMessage and Task records.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Enhanced Email is automatically enabled for most organizations except those that use Email-to-Case.
If you use Email-to-Case, you can already use custom fields, workflows, and triggers with emails without enabling Enhanced Email. If you want to store emails using the EmailMessage object and relate them to other objects, then enable Enhanced Email.
With Enhanced Email, emails sent from Salesforce are saved as both EmailMessage records and Task records. However, only the EmailMessage record is shown on the email detail page in the UI.
Even if your organization uses Enhanced Email, emails added from Web-to-Lead, Salesforce for Outlook, and Lightning for Outlook will still be just stored as Task records.
You can’t use custom currency fields with the EmailMessage object.
EmailMessage records can have only one record type.
Workflow rules can use EmailMessage records only to update fields on Case records.
Before using Enhanced Email, you should:
Review triggers and workflow rules that use the EmailMessage object. For EmailMessage records associated with cases, the 
ParentID
 field is always populated. With Enhanced Email, EmailMessage records may be associated with other records and the 
ParentID
 field may be blank. As a result, you may need to update your triggers and workflow rules to handle email messages with a blank 
ParentID
 field.
Review custom business logic that incorporates tasks and emails. You should:
Recreate any custom Task object fields on the EmailMessage object.
Migrate any Task object triggers to the EmailMessage object.
Test it in a sandbox with any workflows and customizations, including Email-to-Case customizations.
See Also:
Improve Email in Salesforce with Custom Fields, Customized Layouts, and a Better Email Detail Page
Set Up Enhanced Email
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=emailadmin_enhanced_email_considerations.htm&language=en_US
Release
202.14
