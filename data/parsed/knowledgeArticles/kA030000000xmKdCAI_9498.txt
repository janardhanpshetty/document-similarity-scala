### Topic: When your email client (gmail, yahoo, AOL, etc.) is configured in language other than English, email association from Email-to-Salesforce might not work as expected.
When your email client (gmail, yahoo, AOL, etc.) is configured in language other than English, email association from Email-to-Salesforce might not work as expected. 
Particularly, your forward emails might go to the Unresolved Items folder, even if a matching record (lead or contact) exists.
Resolution
Assume your email client is configured in Spanish. 
When forwarding an email to your email-to-salesforce address, the headers appear in the following format: 
 
=================================== 
---------- Mensaje enviado ---------- 
De: John Doe <johndoe@test.test>  -- (From)
Fecha: 3 de septiembre de 2013 17:55  -- (Date)
Asunto: Re: Fwd: Salesforce Test  -- (Subject)
Para: John Smith <jsmith@122test.test>   -- (To)
=================================== 
 
Normally, you would expect the email to associate to the Lead or Contact record with the email address of 'johndoe@test.test', yet, the email is sent to the Unresolved Items folder.
 
This is currently working as designed. When email-to-salesforce was designed, it was based off of the currenct RFC, that stated that email headers should be in English. Since the "from" header displays as "De", the association fails and the email is sent to the Unresolved Items folder.
If customers want this behavior to be enhanced/changed, they should log an idea on ideas.salesforce.com
 
