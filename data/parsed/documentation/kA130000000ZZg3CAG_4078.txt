### Topic: Allocate API Limit App Quotas | Salesforce
Allocate API Limit App Quotas | Salesforce
Allocate API Limit App Quotas
Allocate quotas for various API limits to individual connected apps. 
App quotas for API limits enable you to reserve API capacity for mission-critical connected apps or set a ceiling for API usage of non-critical connected apps.
Available in: both Salesforce Classic and Lightning Experience
Connected Apps can be created in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Connected Apps can be installed in: 
All
 Editions
User Permissions Needed
To read:
“Customize Application”
To create, update, or delete:
“Customize Application” AND either
“Modify All Data” OR “Manage Connected Apps”
To update all fields except Profiles, Permission Sets, and Service Provider SAML Attributes:
“Customize Application”
To update Profiles, Permission Sets, and Service Provider SAML Attributes:
“Customize Application” AND “Modify All Data”
To uninstall:
“Download AppExchange Packages”
Note
API limit app quotas are currently available through a pilot program. For information on enabling this feature for your organization, contact Salesforce.
To allocate a quota for an API limit for a connected app:
From Setup, enter 
Limit Allocation
 in the 
Quick Find
 box, then select 
Limit Allocation Per App
.
Click 
New
.
Select a 
Connected App
.
Select a 
Limit Type
 for which you’re setting a quota.
TotalRequests—Total API requests per 24-hour period
ApiBatchItems—Number of Bulk API batches per 24-hour period 
StreamEventsPerDay—Number of Streaming API events per 24–hour period
GenStreamingEventsPerDay—Number of generic streaming events per 24–hour period
Note
Generic streaming is currently available through a pilot program. For information on enabling generic streaming for your organization, contact Salesforce.
Select a 
Percentage
 of the API limit reserved for this connected app. The app can’t consume more than the selected percentage of the API limit.
Warning
If you allocate 100% of an API limit to one or more connected apps, other API client apps or connected apps will not be able to make requests controlled by the API limit.
Click 
Save
.
Help Site URL
Release
202.14
