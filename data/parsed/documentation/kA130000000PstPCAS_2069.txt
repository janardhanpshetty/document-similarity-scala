### Topic: Using Ideas | Salesforce
Using Ideas | Salesforce
Using Ideas
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view the Ideas tab:
“Read” on ideas
To view and vote for ideas:
“Read” on ideas
To create ideas and add comments to ideas:
“Create” on ideas
To edit ideas and edit comments on ideas:
“Edit” on ideas
Ideas is a community of users who post, vote for, and comment on ideas. An Ideas community provides an online, transparent way for you to attract, manage, and showcase innovation. You can:
Post ideas
View ideas
 or 
Idea Themes
Search for ideas
Vote for ideas
Comment on ideas
View recent activity and replies
Subscribe to syndication feeds
Each time you click the Ideas tab, the Popular Ideas subtab displays ideas in all categories. To change your current view, click one of the other subtabs like Recent Ideas or Top All-Time. Click 
List
 to toggle back to the list view.
See Also:
Ideas Overview
Administrator setup guide: Salesforce Ideas Implementation Guide
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=ideas_using.htm&language=en_US
Release
202.14
