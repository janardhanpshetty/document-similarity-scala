### Topic: This article provides a resolution for certificates that have expired in Single Sign-On Settings
How do I replace an expired certificate in Single Sign-On settings?
Resolution
1) Edit the Single Sign-On settings by going to Setup->Administer->Security Controls->Single Sign-On Settings->Edit
2) Click the "Browse" button to upload a new certificate in "Identity Provider Certificate" field.
3) Save the changes after uploading the new certificate.
Please find the screenshot below explaining the above resolution -
