### Topic: Create Text Email Templates | Salesforce
Create Text Email Templates | Salesforce
Create Text Email Templates
You can create plain-text email templates to send to recipients who can’t read HTML email.
Available in: Salesforce Classic
Available in: 
All
 Editions
Mass email not available in: 
Personal
, 
Contact Manager
, and 
Group
 Editions
HTML and Visualforce email templates not available in: 
Personal
 Edition
User Permissions Needed
To create or change public email template folders:
“Manage Public Templates”
Do one of the following:
If you have permission to edit public templates, from Setup, enter 
Email Templates
 in the 
Quick Find
 box, then select 
Email Templates
.
If you don’t have permission to edit public templates, go to your personal settings. Enter 
Templates
 in the 
Quick Find
 box, then select 
Email Templates
 or 
My Templates
—whichever one appears.
Click 
New Template
.
Choose the 
Text
 template type, and click 
Next
.
Choose a folder in which to store the template.
To make the template available for use, select the 
Available For Use
 checkbox.
Enter a name in 
Email Template Name
.
If necessary, change the 
Template Unique Name
. This unique name refers to the component when you use the Force.com API. In managed packages, this unique name prevents naming conflicts in package installations. This name can contain only underscores and alphanumeric characters, and must be unique in your org. It must begin with a letter, not include spaces, not end with an underscore, and not contain two consecutive underscores. With the 
Template Unique Name
 field, you can change certain components’ names in a managed package and the changes are reflected in a subscriber’s organization.
If desired, choose a different character set from the 
Encoding
 drop-down list.
Enter a 
Description
 for the template. Both template name and the description are for your internal use only.
Enter a 
Subject
 for the message.
Enter the text of the message.
If desired, enter merge fields in the template subject and text body. When you send an email, these fields are replaced with information from your records.
Click 
Save
.
Tip
View a sample of the template populated with data from records you choose and send a test email by clicking 
Send Test and Verify Merge Fields
.
Attach Files to Templates
To add an attachment to a template:
Do one of the following:
If you have permission to edit public templates, from Setup, enter 
Email Templates
 in the 
Quick Find
 box, then select 
Email Templates
.
If you don’t have permission to edit public templates, go to your personal settings. Enter 
Templates
 in the 
Quick Find
 box, then select 
Email Templates
 or 
My Templates
—whichever one appears.
Select a template.
Click 
Attach File
 from the email template detail page.
Choose a folder and click the document name, or search for a document by clicking 
Search in Documents
 and entering the file name.
Alternatively, click 
My Computer
 to attach a file from your computer.
The attached file is included in every email that uses the template. Attachments in mass emails are sent as links rather than as files (see 
Send Mass Email
).
See Also:
Manage Email Templates
Create Visualforce Email Templates
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=creating_text_email_templates.htm&language=en_US
Release
202.14
