### Topic: Enable Enterprise Territory Management | Salesforce
Enable Enterprise Territory Management | Salesforce
Enable Enterprise Territory Management
Starting with Winter ’15, Enterprise Territory Management is ready to be enabled by administrators in new Salesforce organizations.Organizations created before Winter ’15 need to call salesforce.com to enable the feature. Enterprise Territory Management cannot be enabled in existing organizations that have Customizable Forecasting enabled.
Available in: Salesforce Classic
Available in: 
Developer
 and 
Performance
 Editions and in 
Enterprise
 and 
Unlimited
 Editions with the Sales Cloud
User Permissions Needed
To enable Enterprise Territory Management:
“Customize Application”
Note
This information applies to Enterprise Territory Management only, not to previous versions of Territory Management. Enterprise Territory Management and Collaborative Forecasts can both be enabled and used at the same time in your Salesforce organization, but the two features are not currently integrated to work together.
From Setup, enter 
Territories
 in the 
Quick Find
 box, then select 
Settings
.
Click 
Enable Enterprise Territory Management
. 
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tm2_enable_tm2.htm&language=en_US
Release
202.14
