### Topic: Complete Your Chatter Profile | Salesforce
Complete Your Chatter Profile | Salesforce
Complete Your Chatter Profile
Your profile communicates who you are to your coworkers and the patients you communicate with. It features your photo and basic information like your professional title and contact details. Set up your Chatter profile soon after you begin using Chatter, and make sure to keep it up to date. Your patients will appreciate it!
Health Cloud is available in Salesforce Classic
Available in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
In the Health Cloud header, click the profile image placeholder. Find the placeholder profile image on the left side of the page, and select 
My Profile
.
To update your profile photo, hover over the placeholder image and click 
Add Photo
.
Tip
Use a photo of you and not a group of people or a pet. The recommended resolution for photos is 200 x 200 pixels.
To edit your contact information, click 
 in the Contact section.
On the Overview tab, add any other information about yourself. 
Tip
Complete your profile with details about the department you work in, your experience, and so on.
Click 
Save All
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=create_chatter_profile.htm&language=en_US
Release
202.14
