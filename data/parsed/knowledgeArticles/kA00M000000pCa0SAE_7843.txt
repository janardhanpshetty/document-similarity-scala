### Topic: This is INTERNAL INFORMATION from our Email Design team.
Where are some an internal resources for Email Design and Coding for an email built within Email Studio in the Marketing Cloud?
Resolution
Email Design Best Practices with sample code:
Our Design Team sends this to customers when they ask for help but don't buy services hours. The design team doesn't send this out publically, so please use this as an internal resource. 
Relevant items to support: a sample responsive email and sample responsive template. 
https://pages.exacttarget.com/mobile-inbox
Email Coding google doc: 
Generally meant for new coders and our offshore team - there may be internal language in here - you can take any and all of this to publish if someone ensures its client-readiness.  This is a coder's resource for building good email.
https://docs.google.com/document/d/1IxSXHqmqnVGOJKCuOOdJC2zy2xvMB1XOx4ZbWE1JX74/edit
This video is a good start for the new editor - see if there are more of these and pull them together and embed the most relevant ones in 
this page
.
  T
here may be more videos linked from here: 
https://www.marketingcloud.com/blog/five-steps-to-better-email-marketing/
