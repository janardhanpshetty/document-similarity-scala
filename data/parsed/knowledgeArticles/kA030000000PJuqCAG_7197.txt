### Topic: Customer is unable to see the Object under the "Create New" Sidebar component in the side bar. This is because the user has customized the tabs by deselecting the Tab in the Custom App.
Customer is unable to see the Object under the "Create New" Sidebar component for quick creation of records. Although previously the object was available in the side bar.
Resolution
Objects inside the "Create New" Sidebar component only appears if they are included in the personal list of tabs shown.
You can customize personal tabs by performing the following steps:
Click on Setup.
Go to Personal Setup> My Personal Information.
Click on Change My Display.
Click on 
Customize My Tabs.
Choose respective Custom App where in the Object is missing.
Choose missing object from available tabs.
Click on 
Add 
that will add the object to the Selected Tabs.
Click on Save.
This will make the object to be available in the "Create New" Sidebar component.
Alternative click path is
1. Your name (top right hand side) >> My Settings >> Display and Layout
2. Follow from Step 4 as mentioned above
