### Topic: Choosing a Custom Fiscal Year Template | Salesforce
Choosing a Custom Fiscal Year Template | Salesforce
Choosing a Custom Fiscal Year Template
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions except 
Database.com
.
User Permissions Needed
To change your fiscal year:
“Customize Application”
When defining a new custom fiscal year, your first step is to choose a custom fiscal year template. These templates are available to make it easier for you to define your custom fiscal year. They create a simple custom fiscal year that you can customize to meet your exact needs.
Note
If you choose a template and realize that it is not the best one for your fiscal year definition, you can reset it at any time using the 
Reset Fiscal Year Structure
 option.
Choose one of three types of templates:
4 Quarters per Year, 13 Weeks per Quarter
Choose one of these templates for your fiscal year if you want each quarter to have the same number of weeks per quarter. These templates all have 4 quarters, 12 periods, and 52 weeks per year. Each quarter is 13 weeks long and is composed of three periods. Two of the periods in each quarter are 4 weeks, and one is 5 weeks. In a 4-4-5 template, for example, the first and second period of a quarter are 4 weeks long, and the third period is 5 weeks long. Weeks are always 7 days long. A typical customization for these templates is to add extra weeks for leap years.
4-4-5
Within each quarter, period 1 has 4 weeks, period 2 has 4 weeks, and period 3 has 5 weeks
4-5-4
Within each quarter, period 1 has 4 weeks, period 2 has 5 weeks, and period 3 has 4 weeks
5-4-4
Within each quarter, period 1 has 5 weeks, period 2 has 4 weeks, and period 3 has 4 weeks
13 Periods per Year, 4 Weeks per Period
Choose one of these templates if your fiscal year has more than 12 periods and if one quarter is longer than the other quarters. These templates all have 4 quarters per year, 13 periods per year, 3 or 4 periods per quarter, 53 weeks per year, and 4 weeks per period (5 weeks in the final period). Weeks generally have 7 days, but will include a short week at the end of a year. The most common customization for this type of template is to create or change the length of a short week.
3-3-3-4
Quarter 1 has 3 periods, quarter 2 has 3 periods, quarter 3 has 3 periods, and quarter 4 has 4 periods
3-3-4-3
Quarter 1 has 3 periods, quarter 2 has 3 periods, quarter 3 has 4 periods, and quarter 4 has 3 periods
3-4-3-3
Quarter 1 has 3 periods, quarter 2 has 4 periods, quarter 3 has 3 periods, and quarter 4 has 3 periods
4-3-3-3
Quarter 1 has 4 periods, quarter 2 has 3 periods, quarter 3 has 3 periods, and quarter 4 has 3 periods
Gregorian Calendar
12 months/year, standard Gregorian calendar.
Unlike the other template styles, you cannot do advanced customization of a fiscal year that has been created from a Gregorian calendar template. You should only use this template if you want to create a fiscal year that follows the Gregorian calendar. This template mimics the functionality of standard fiscal years.
See Also:
Fiscal Years
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_cfy_template.htm&language=en_US
Release
202.14
