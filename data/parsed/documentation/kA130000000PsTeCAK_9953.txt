### Topic: Working with Apex Class Access in Permission Sets | Salesforce
Working with Apex Class Access in Permission Sets | Salesforce
Working with Apex Class Access in Permission Sets
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view Apex class access settings:
“View Setup and Configuration”
On the Apex Class Access page in permission sets, you can:
Change the Apex class access settings by clicking 
Edit
Change the permission set label, API name, or description by clicking 
Edit Properties
Create a permission set based on the current permission set
View and manage the users assigned to the permission set
Go to the permission set overview page by clicking 
Permission Set Overview
Switch to a different settings page by clicking the down arrow next to the Apex Class Access name and selecting the page you want
See Also:
Permission Sets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=perm_sets_apex_access.htm&language=en_US
Release
202.14
