### Topic: Edit Trialforce Source Organization | Salesforce
Edit Trialforce Source Organization | Salesforce
Edit Trialforce Source Organization
Available in: Salesforce Classic
Available in: 
Developer
 Edition
User Permissions Needed
To define package branding:
“Package Branding”
To edit a Trialforce source organization:
Enter the source organization name and select the branding.
Click 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=trialforce_edit_source_organization.htm&language=en_US
Release
202.14
