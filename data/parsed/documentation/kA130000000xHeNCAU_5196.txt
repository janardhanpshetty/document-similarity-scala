### Topic: Find Object Management Settings in Lightning Experience | Salesforce
Find Object Management Settings in Lightning Experience | Salesforce
Find Object Management Settings in Lightning Experience
Salesforce lets you customize your object model with features like custom fields, page layouts, and validation rules. Most objects are available from the Object Manager in Setup.
Available in: Lightning Experience
Available in: 
All
 editions
Standard Objects and Custom Objects
A 
standard object
, such as Account or Contact, comes out of the box with your Salesforce organization. A 
custom object
 is an object that you or another administrator created.
From Setup, enter 
Object Manager
 in the 
Quick Find
 box, then select 
Object Manager
. Select one of the objects in the list, and then scroll to the section for the specific customization.
For example, to add a custom field to the Account object, enter 
Object Manager
 in the 
Quick Find
 box, then select 
Object Manager
. Next, select 
Account
, and then scroll to Fields & Relationships.
Other Standard Objects
Some standard objects aren’t housed in the Object Manager. To access customization settings for one of those objects, from Setup, enter the object name in the 
Quick Find
 box, then select the customization setting.
From Setup, enter the object name in the 
Quick Find
 box, then select the customization.
For example, to add a trigger to the Groups object, enter 
Group
 in the 
Quick Find
 box, then select 
Group Triggers
.
External Objects
An 
external
 object is similar to custom objects, except that it maps to data that’s stored outside your Salesforce organization.
From Setup, enter 
External Objects
 in the 
Quick Find
 box, then select 
External Objects
. Next, click one of the external objects in the list. Then scroll to the section for the specific customization.
For example, to add a custom field to the Orders external object, enter 
External Objects
 in the 
Quick Find
 box, then select 
External Objects
. Click 
Orders
, and then scroll to Custom Fields and Relationships.
See Also:
Point-and-Click Customization: What’s Not in Lightning Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=extend_click_find_objectmgmt_lex.htm&language=en_US
Release
202.14
