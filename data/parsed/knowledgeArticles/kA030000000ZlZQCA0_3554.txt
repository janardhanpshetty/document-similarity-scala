### Topic: The article will provide the steps on how to get the logs for Salesforce for outlook
1. Close 
Salesforce for Outlook
 by right clicking the red Salesforce for Outlook icon in the system tray and selecting "Exit".
2. In a Run or Search window or in the address bar of Windows explorer navigate to 
%appdata%\salesforce.com\Salesforce for Outlook\logs
3. In this Folder select and delete all Files - 
DO NOT SELECT AND DELETE ANY FOLDERS.
4. Restart 
Salesforce for Outlook
 from the short cut on the desktop or from the "Programs Menu"
5. Replicate the issue at hand.
6. Navigate to  
%appdata%\salesforce.com\Salesforce for Outlook\logs
 and select all files - 
DO NOT SELECT FOLDERS
. Right click and select "Send to" Compressed (Zipped) Folder
Note log location for versions prior to 2.5 is 
%appdata%\salesforce.com\Salesforce for Outlook
7. Attach the zipped folder to the support case or email to the support representative you are working with. 
Resolution
