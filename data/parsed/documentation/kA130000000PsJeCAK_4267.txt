### Topic: What’s the difference between the Forecasting versions? | Salesforce
What’s the difference between the Forecasting versions? | Salesforce
What’s the difference between the Forecasting versions?
Collaborative Forecasts
 includes much of the same functionality as 
Customizable Forecasting
.
See which features are available in each version of Forecasting. 
If you’re migrating to Collaborative Forecasts, see 
Considerations for Migrating from Customizable Forecasting to Collaborative Forecasts
.
Feature
Customizable Forecasting
Collaborative Forecasts
API Access
Automatic Rollups
Chat in Real Time
Custom Fiscal Years
Custom Opportunity Currency Field Forecasts
(Not available in Professional Edition)
Default Forecast Currency Setting
Individual Forecast Range Selection—Controlled by User
Map Forecast Categories to Opportunity Stages
Monthly Forecasts
Multiple Currency Support
Opportunity-level Adjustments
Can use a custom opportunity currency field to save adjusted amounts and a custom field forecast to track the rollup of that field.
Opportunity List Filters and Sort
Opportunity Splits Forecasts
(Not available in Professional Edition)
Opportunity Stage
Overlay Splits Forecasts
(Not available in Professional Edition)
Override (Adjust) Forecasts
Override (Adjustment) Notes
Partner Opportunities in Forecasts
(Not available in Professional Edition)
Product Family Forecasts
Product Schedule Forecasts
Quotas
Quantity Forecasts
Quarterly Forecasts
Rename Categories
Reports and Dashboards
Resize Forecast Table Columns
Revenue Forecasts
Share Forecasts
(Pilot, API only)
Snapshots and Forecast History
Sortable Opportunities List From the Forecast Page
Submit Forecasts
Territory Management
(The original Territory Management feature only. Enterprise Territory Management is not supported.)
See Also:
Forecasts FAQ
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_forecasts3_whats_difference_between_versions.htm&language=en_US
Release
202.14
