### Topic: This article explains why a document attached to a record is garbled with strange characters when using a Firefox browser.
The garbled characters are caused by the document mimetype (Multipurpose Internet Mail Extensions) being set as TEXT/HTML when using Firefox.
The reason for this is because Firefox is detecting the mime type of the document as TEXT/HTML because the upload tool is a HTML type. This Firefox bug comes and goes as it does not occur on all releases.
Resolution:
a) You will need to open the document detail page and right click on the link and choose "Save As" . Click on save document. Once document is downloaded, re-upload the document in Internet Explorer and delete the original.
b) You can also use the API with Dataloader to extract a list of the documents and the "ContentType". Once it has been exported, you can thenupdate the mimetypes in Excel then use Dataloader to update records.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
August 31, 2015 at 7:37 PM
  
Harihara Suthan
Done 
@Arianne Mae Cruz
Show More
Like
Unlike
 
  ·  
 
September 1, 2015 at 10:30 AM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Configuration
The garbled characters are caused by the document mimetype (Multipurpose Internet Mail Extensions) being set as TEXT/HTML when using Firefox.
The reason for this is because Firefox is detecting the mime type of the document as TEXT/HTML because the upload tool is a HTML type. This Firefox bug comes and goes as it does not occur on all releases.
Resolution
To correct issue
a) You will need to open the document detail page and right click on the link and choose "Save As" . Click on save document. Once document is downloaded, re-upload the document in Internet Explorer and delete the original.
b) You can also use the API with Dataloader to extract a list of the documents and the "ContentType". Once it has been exported, you can then update the mime types in Excel then use Dataloader to update records.
Workaround:
a) Use Internet Explorer to upload these documents.
