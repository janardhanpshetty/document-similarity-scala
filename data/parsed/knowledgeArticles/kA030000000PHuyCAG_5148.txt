### Topic: Different Common Error messages that can easliy be resolved
There are many error messages that can easily be resolved by just correctly interpreting them. Please read through the following examples to see if your Error Message can be easily resolved and / or how to proceed .
Resolution
 
1) 
Please see your System Administrator or Administrator Error Message
 - This refers to your company's Internal System Administrator for your machine and NOT Salesforce.
2) 
Insufficient Privileges Error Message
 - This Error message means you do not have the correct privileges assigned to your Profile to be able to perform the tasks.  You will need to reach out to your Internal Salesforce System Administrator in order to resolve this issue as it is an internal decision to allow this type of permission.
3) 
I cannot login - Error message in login history shows: "IP Restricted"
 - This Error Message is due to an imposed implementation of IP Login Restrictions to limit login access from unknown locations as decided by your Internal Salesforce System Administrator. This helps to ensure the security of data by identifying approved IP ranges for your organization and users. You will have to contact your Internal Salesforce System Administrator in order to resolve this issue.
4) 
Error message 'Duplicate Username'
 - This Error Message is because this username is currently used, whether active or inactive, within an organization in all of Salesforce. All Salesforce usernames are unique for all users across all orgs.  In order to resolve this Error Message you will need to find what Organization used the Username and if it is your Org,change and save the Username as a different one than you want to use. This will free up the Username to be used in your Organization.  If you do not have the privileges to do this please contact your Internal Salesforce System Administrator. If you cannot find the user name in your org then please create a case from Help & Training on the issue or call Salesforce Support for assistance.
5) 
Dashboard: Error: The running user for this dashboard does not have permission to run reports. Your system administrator should select a different running user for this dashboard
 -
The System Administrator will need to follow the steps below:
Step 1:  Check to see who the running user is for the Dashboard.
             Go to the Dashboard with the error message and click on the "Edit Properties" button. 
             Check the "Running User" in the Dashboard Security Settings.
Step 2. Search for the running user and look at the Profile the running user is assigned to
Step 3.  Click on the Profile and go to the Profile's "General user Permissions"sections "Run Reports".
If you would like to provide this Profile with Run Reports - enable Run Reports.  If not, update the user's profile with a profile that has the Run Reports permission.
6) 
'Org Administration Locked'  Error Message
 -
Administrators attempting to make changes to the sharing setup or custom field/object setup may receive an "Org Administration Locked" message. This occurs when a similar setup change that affects a large number of records is still being processed. In order to maintain data integrity, the relevant setup area is locked until the change is complete. Sharing changes will only lock sharing setup and custom field/object changes will only lock custom field/object setup
7) 
Salesforce for Outlook Sync Error: "Your Windows Time Zone doesn't match" 
-
 
Check the time in the computer, if it is wrong proceed to change it:
1. Click on Start / Control Panel / Date and Time
2. Click on Change Time Zone and select the appropriate time zone from the drop down menu and click OK
3. Click on OK to close the Date and Time window
* If the machine's time is correct, confirm the information in Salesforce
1. Click on Your Name / Setup / My Personal Information
2. Click on Edit  and find the Locale Settings
3. From the Time Zone select the appropriate time zone from the drop down menu
4. Click on Save
Attempt to sync again by right-clicking the system tray icon on your desktop and select Sync / Sync Now
Note: Please pay attention to the differences between Daylight and Standard time zones.
8) 
Salesforce for Outlook Sync Error "Unable to Sync Data"
 - 
 
1. Right-click the system tray icon on your desktop and select Exit.
2. Close Outlook.
3. Using your Windows Add or Remove Programs tool, remove:
• Salesforce for Outlook
• Microsoft Office 20xx Primary Interop Assemblies
• Microsoft Visual Studio 20xx Tools for Office Runtime
4. Since you plan to reinstall Salesforce for Outlook, you'll have to first remove the old Salesforce for Outlook database files. Otherwise, Salesforce for Outlook will use your old sync settings. Default database file locations:
• (Microsoft Windows 7 and Vista) C:\Users\username\AppData\Roaming\salesforce.com\Salesforce for Outlook\DB\
• (Microsoft Windows XP) C:\Documents and Settings\username\Application Data\salesforce.com\Salesforce for Outlook\DB\
5. In Salesforce, click Your Name | Setup | Desktop Integration | Salesforce for Outlook.`
6. Click on the “Download” and the run the file.
7. Run the installer and finish it.
8. Next Open Microsoft Outlook and Salesforce for Outlook.
9) 
When trying to load an Activity List View from the Home Tab, the List View gives a Timeout Error Message
 -
This is usually due to the filter criteria being too broad so that the List View would return too many records.
To rectify this perform the following steps:
1. Run the list view and wait for the time out error message
2. In the error message body you should find something like:
"Caused by: java.sql.SQLException: ORA-01013: user requested cancel of current operation: select /*ReportActivity.runSharingModelStage1*/ * from (select /*ActivityFilter 00B20000004qjkh*/ "WHO_ID", "WHAT_ID", "te.ID"
3. From the error message extract the ID: e.g. 00B20000004qjkh
4. Then Paste the ID on the default Edit URL as here:
https://na2.salesforce.com/ui/list/FilterEditPage?id=00B20000004qjkh&retURL=/007?fcf=00B20000004qjkh&rolodexIndex=-1&page=1
(change the instance to whichever one the you are on Example: na2 above or the characters right after https:// to the first dot)
5. When you are in the edit page you will be able to edit the view filter criteria so it doesn't time out or delete it.
10) 
Mobile error: SOAP Fault - LIMIT_EXCEEDED: Too much data for the sync
 -
Symptoms:
When using the Salesforce Mobile application, the error message “SOAP Fault - LIMIT_EXCEEDED: Too much data for the sync” is returned.  This error typically occurs during a mobile activation, but can also occur after the user has successfully used mobile for an extended period.
Cause:
All Salesforce Mobile users are associated to a mobile configuration and most mobile configurations have a specific Data Size Limit set.  If the amount of data generated on a mobile activation (or a full mobile update) exceeds the Data Size Limit, the above error is returned to the mobile user / device.  And until corrected, the device will no longer receive updates.
Resolution:
The resolution to this issue is to reduce the amount of data that is being sent to the mobile device.  This can be addressed by:
• Adjusting the mobile configuration, leverage Mobile Object Properties to reduce the number of fields (and associated metadata) that are delivered to the device.
• Reevaluate the mobile data requirements, and if possible, modify the mobile configuration data sets such that only the most critical Salesforce information is delivered to the device.
If neither of the above reduces the amount of data to the point that the issue is resolved, it may be possible to simply increase the Data Size Limit on the mobile configuration.
 
***NOTE:  All Mobile Lite users are associated to the “default” mobile configuration and the default mobile configuration CANNOT be modified as noted above.  Thus if this error is encountered by a Mobile Lite user, the only resolution may be to upgrade to a full mobile license.
11) 
Internal Server Errors
 - Please call and log these Errors immediately to Salesforce Support, you will need to grant login access, attach a screen shot of error and include the exact steps to reproduce the Error Message so that Salesforce can respond as fast as possible.
12)
 Chatter Desktop - Installation Error #0
 - This error message is related to a previous install of Chatter Desktop that has not been removed fully before the new installation, we recommend to use a MSI cleanup utility and attempt to download it again.
13) 
Email-to-Salesforce sending the error message 
'user@domain.com
 not authorized to send emails'
 - This error message means the sender's email address is not included in the list of authorized/acceptable addresses. 
To resolve this issue follow the steps below:
1. Click on Setup | Personal Setup | Email | My Email to Salesforce
2. Under My Acceptable Email Addresses list, add you email address.  You can add multiple email address, just separate them using a comma (no spaces).
3. Click on Save
This list is a mandatory setting and cannot be left blank.
14) 
Apex throws a validation error when assigning a queried field value to a String
 -
Sometimes, fields can contain invalid characters. This can happen if you load the field values through the API and the validation rules miss some characters. The issue can be fixed by reloading these fields with the correct values.
15) 
Error Message Value does not exist or does not match filter criteria
 -
The default error message for invalid filter lookup data is "Value does not exist or does not match filter criteria".  Users will be presented with this error message whenever the user attempts to save a record with any value if a lookup field does not meet the filter criteria.
For example:
You have a Custom Lookup Filter field on the User object that has a filter requiring the User to be active. Someone attempts to edit the record and the User that is listed in your lookup field has become inactive since the last time this record has been edited. Then the error will display.
** Please note: Filter Lookup validation triggers even if the field is not on the page layout.  Therefore, if a user cannot see the field due to field level security or page layout security the Lookup Filter Validation will still trigger.
Resolution 
1) Correct the data entry validation with valid data or remove the data in the filter field altogether.
 
2) Remove or modify the lookup filter to meet your business needs.
There are more error messages in Salesforce than this small sample can contain. If you do not see the Error Message you are receiving, please try to look up the Error Message itself on the Help and Training Web Site.
 
