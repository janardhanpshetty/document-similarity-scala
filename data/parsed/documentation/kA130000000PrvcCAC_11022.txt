### Topic: Predefine Case Teams | Salesforce
Predefine Case Teams | Salesforce
Predefine Case Teams
After you define case team roles, you can predefine case teams so that support agents can quickly add people who they frequently work with to cases.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set up case teams:
“Customize Application”
AND
“Manage Users”
To add team members:
“Edit” on cases
From Setup, enter 
Predefined Case Teams
 in the 
Quick Find
 box, then select 
Predefined Case Teams
.
Click 
New
, and enter the team’s name.
Add team members.
Choose a team member type: User, Contact, or Customer Portal User. Contacts can access cases only when they’re enabled as customer portal users and assigned to case page layouts.
Click 
Lookup
 (
) and select a member.
Choose a role for the member.
Click 
Save
.
Note
To delete a predefined case team, remove it from assignment rules first. If you delete a predefined case team, it’s removed from all cases it’s on, and you can’t retrieve it from the Recycle Bin. When you remove members from a predefined case team, they’re removed from all cases in which they were members of the team.
See Also:
Case Teams
Set Up Case Teams
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=caseteam_predefine_team.htm&language=en_US
Release
202.14
