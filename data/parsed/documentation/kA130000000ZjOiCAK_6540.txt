### Topic: Salesforce Adoption Manager | Salesforce
Salesforce Adoption Manager | Salesforce
Salesforce Adoption Manager
Quickly turn your mobile employees into Salesforce1 power users with Salesforce Adoption Manager. This tool trains and engages your users with intelligent email journeys aimed at driving adoption of the Salesforce1 mobile app. After inviting users to download the mobile app, Adoption Manager follows up with tips that help users get the most out of Salesforce1. It also encourages dormant Salesforce1 users to try using the app again.
Is Salesforce Adoption Manager Available for All Orgs?
Adoption Manager is currently available for orgs in the United States, the U.K., and Australia. Adoption Manager determines your country by the billing country for your Salesforce account. Note that Adoption Manager is not available for customers on the NA21 instance of Salesforce.
What Kind of Results Can I Expect from Salesforce Adoption Manager?
With customized tips and feedback, this program is designed to help you and your users get more out of Salesforce. Currently, this program is about making your users more effective with the Salesforce1 mobile app.
For example, here are some amazing results from our customers when effectively using Salesforce1:
40% increase in employee satisfaction
29% faster time to find information
26% increase in sales productivity
What Is User Data Used for When Salesforce Adoption Manager Is Enabled?
The only change when you enable Salesforce Adoption Manager is your users receive email messages from the program, based on their usage of Salesforce1. You can review our 
privacy statement
 for more detail.
What Happens After I Activate Salesforce Adoption Manager for My Users?
Once you activate the program, it starts with a personalized invite to users to download the Salesforce1 mobile app. All emails are optimized for desktop and mobile devices. If users access the email from a desktop, they can text a download link to their mobile devices.
After users downloaded Salesforce1, they receive emails based on their actual usage of the mobile app. These emails suggest top actions to take and also keep track of actions already taken. The goal is to get users up to speed with Salesforce1 so your company can start realizing more benefits from the product.
In the future, we plan to expand this program to help users become more effective with all Salesforce products.
Will My Users Get Notifications or Other Types of Messages in Addition to Emails?
Initially, Salesforce Adoption Manager sends email messages only. We plan to add mobile notifications in the future so users can get the tips they need while using Salesforce1.
What Do the Emails from Salesforce Adoption Manager Look Like?
Check out this video
 to see for yourself!
Can I Customize the Content of the Salesforce Adoption Manager Emails?
No.
Who Receives the Salesforce Adoption Manager Emails? How Frequently Are Emails Sent Out?
Emails are delivered to users with full Salesforce licenses only. Community, Partner, and Chatter users aren’t included.
Adoption Manager is intelligent about who receives emails.
The invitation to download Salesforce1 isn’t sent to users who don’t have permission to access the mobile app. Nor is it sent to users who have already installed the app.
Five separate tips are sent to all users who downloaded Salesforce1 within the last 60 days.
A single reminder to use Salesforce1 is sent to users who haven’t accessed the mobile app for 30 days.
Are Salesforce Adoption Manager Emails Counted Against My Org’s Limits?
No. The emails are sent from Salesforce Marketing Cloud servers instead of from your org.
How Can I Confirm That Salesforce Adoption Manager Emails Are Actually Going Out?
The Marketing Cloud Support team can help confirm that the emails are being sent. Contact Salesforce Customer Support for more information.
Can I Configure Salesforce Adoption Manager to Send Emails to a Specific Group of Users Only?
No. When you enable Adoption Manager, it’s turned on for all users in your org. But users can opt out of receiving future messages from the footer of any email from the program.
Can Users Opt Back into Receiving Salesforce Adoption Manager Emails After Opting Out?
Yes. The first Adoption Manager email includes a link that allows users to opt back into receiving future emails. Consider encouraging your users to save this email, just in case.
If I Turn on Salesforce Adoption Manager, Can I Opt Out Later?
Yes. From Setup in the full Salesforce site, enter 
Adoption
 in the 
Quick Find
 box, select 
Adoption Manager
, and then deselect 
Enable Salesforce Adoption Manager
.
See Also:
Salesforce1 Mobile App
Get the Salesforce1 Mobile App
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=user_journey_faq.htm&language=en_US
Release
202.14
