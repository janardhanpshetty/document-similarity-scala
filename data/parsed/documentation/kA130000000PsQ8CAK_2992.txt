### Topic: Create and Modify Category Groups | Salesforce
Create and Modify Category Groups | Salesforce
Create and Modify Category Groups
Category groups are used by Salesforce Knowledge (articles), answers (questions), or ideas. In all cases, category groups are containers for individual data categories. For example, a Contracts category group might contain Fixed Price, Cost Reimbursement, and Indefinite Delivery categories.
Available in: Salesforce Classic
Data categories and answers are available in 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions.
Salesforce Knowledge is available in 
Performance
 and 
Developer
 Editions and in 
Unlimited
 Edition with the Service Cloud.
Salesforce Knowledge is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions.
User Permissions Needed
To view the Data Categories page:
“View Data Categories”
To create, edit, or delete data categories:
“Manage Data Categories”
From Setup, enter 
Data Category
 in the 
Quick Find
 box, then select 
Data Category Setup
.
To create a category group, click 
New
 in the Category Groups section.
By default, you can create a maximum of five category groups and three active category groups. To edit an existing category group, hover your cursor over the category group name and then click the 
Edit Category Group
 icon (
).
Specify the 
Group Name
. This name appears as the title of the category drop-down menu on the Article Management and Articles tabs, and, if applicable, in the public knowledge base. The 
Group Name
 does not appear on the Answers tab.
Optionally, modify the 
Group Unique Name
 (the unique name used to identify the category group in the SOAP API).
Optionally, enter a description of the category group.
Click 
Save
.
You receive an email after the save process completes.
Activating Category Groups
When you add a category group, it's deactivated by default and only displays on the administrative setup pages for Data Categories, Roles, Permission Sets, and Profiles. 
Keep your category groups deactivated to set up your category hierarchy and assign visibility. Until you manually activate a category group, it does not display in Salesforce Knowledge or your answers community
. In addition to activating the category group, for answers communities you must assign the category group to a zone before the categories are visible on the Answers tab.
To activate a category group so it is available to users, move the mouse pointer over the name of the category group and click the 
Activate Category Group
 icon (
).
You can now 
add categories
 to your category group. When you create a category group, Salesforce automatically creates a top-level category in the group named 
All
. Optionally, double-click 
All
 to rename it.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=categorygroup_create.htm&language=en_US
Release
202.14
