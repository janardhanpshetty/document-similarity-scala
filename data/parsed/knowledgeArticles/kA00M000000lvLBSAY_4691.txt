### Topic: While sending the email from case feed an error will prompt : Case customer does not want to be contacted by email.
When trying to send an email from Case using
 Feed Layout
 an 
error
 will prompt 
"Case customer does not want to be contacted by email."
• Go to Case Tab
• Open a Case record
• Click on Feed
• Try sending email on Case Feed
Error:
"Case customer does not want to be contacted by email."
Resolution
The error is expected if the 
"Email Opt Out" 
field of the Contact associated to the Case is 
checked
. It could be the customer requested not to receive any email from the support.
