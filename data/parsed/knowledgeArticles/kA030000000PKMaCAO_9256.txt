### Topic: Why am I missing Facebook Comments in the Analysis Dashboard and/or in the Engagement Console even though I am using a Social Account?
Why are new Facebook Comments not being captured by Radian6 crawlers when previous comments were?
Why are Facebook comments missing when the customer has a Facebook Social Account?
 
Resolution
Comments are ingested on a momentum-based schedule. If a particular post has no comments made on it for a few weeks, crawlers will stop checking it for new comments. Any new comments that are made on it after that few week period will not be ingested.
If this scenario occurs, no troubleshooting is required. Gather the following information to send to Data Integrity:
Client ID
Confirm client is using a Facebook Social Account to capture this content.
Comment link(s): Some examples of comments that were missed and if possible, posts that were captured. Try to export a CSV of comments captured on original post so you can see when the comments stopped.
Facebook Page URL
INTERNAL NOTE: You will send this case to Data Integrity. However,  Data Integrity must involve Database Support  to complete this request.  Therefore, it can take 5-7 days to complete due to Database Support's schedule to complete changes. Please set expectations with customers that the request to re-crawl a post to ingest  missing comments may take 5-7 days to complete. 
Sending to Data Integrity
Use the following template to send to Data Integrity:
Hi Data Integrity,
<Client> is experiencing an issue with Facebook Comments no longer being pulled in. They have a Facebook Managed Account and the last comment that was captured was on <Date and Time.> Is this issue occurring because of the time passed since the last comment was made causing the crawlers to no longer check for new comments? If so, can we manually re-crawl this post again to ingest this content?
Client ID:
Client:
Last Comment Captured: <Link>
Example of Comment Missed: <Link>
Facebook Page URL:
*Note if the CSV is attached if you exported it.
Thanks!
Note: There was a bug with Facebook previously that would not upload comments to the Graph API after 3000 comments were added to a thread. This bug has been fixed and more information can be found in the 
Facebook Bug update.
