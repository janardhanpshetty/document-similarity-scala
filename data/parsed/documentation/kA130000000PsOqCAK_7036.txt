### Topic: Set Visualforce Page Security from Profiles | Salesforce
Set Visualforce Page Security from Profiles | Salesforce
Set Visualforce Page Security from Profiles
Set Visualforce security directly from a profile to give that profile’s users access to the specified Visualforce page.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set Visualforce page security:
“Manage Profiles and Permission Sets”
From Setup, enter 
Profiles
 in the 
Quick Find
 box, then select 
Profiles
.
Click the name of the profile you want to modify.
Go to the Visualforce Page Access page or related list and click 
Edit
.
Select the Visualforce pages that you want to enable from the Available Visualforce Pages list and click 
Add
. You can also select the Visualforce pages that you want disabled from the Enabled Visualforce Pages list and click 
Remove
.
Click 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=users_profiles_visualforce_access.htm&language=en_US
Release
202.14
