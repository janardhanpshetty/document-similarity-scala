### Topic: When setting up a Chatter Answers zone that will be accessed by external users, be aware that Guest User License users will need to have the same data category visibility as defined in the 'Data Category for Top-Level Topics' field on the Chatter Answers Zone.
When setting up a Chatter Answers zone that will be accessed by external users, be aware that Guest User License users will need to have the same data category visibility as defined in the 'Data Category for Top-Level Topics' field on the Chatter Answers Zone. 
For example, assume the following Data Category Hierarchy
-IT
----Cloud Computing
----Virtualization
If 'IT' is defined as the 'Data Category for Top-Level Topics' on the Chatter Answers Zone, Guest User License users must have 'IT' as their assigned top level category. If for example you assign them the 'Virtualization' data category as their main category, they will not have access to the Chatter Answer Zones, because 'Virtualization' is not defined as the 'Data Category for Top-Level Topics'.
 
Resolution
