### Topic: With the release of Summer '14 the Salesforce for Outlook Configuration in regards to the Side Panel will have some changes
With Summer '14 Salesforce for Outlook will have a default configuration for all users in addition to the already existing default configurations for Contact Manager and Group Editions.  Furthermore Contact Manager and Group Edition default configuration will be modified.
Resolution
Salesforce for Outlook default Configuration changes by editions:
Contact Manager and Group Editions:
Salesforce for Outlook Side Panel will now be enable on the default configuration associated to users on these editions.  The "Add Email" and "Send and Add" will no longer be available for these Editions.
Professional, Enterprise and Performance Editions:
Salesforce for Outlook Side Panel (without the add email feature) will be enabled including the new Publisher actions.  This will allow all users to download Salesforce for Outlook and install it on their machines without having a configuration assign and grant you some limited functionality in Outlook.
In order to prevent the users from using the Side Panel or any Salesforce for Outlook features a System Administrator will have to create a configuration with all features disabled and assign the users that are not intended to
