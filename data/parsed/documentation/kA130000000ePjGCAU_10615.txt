### Topic: Monitor Login Activity with Login Forensics | Salesforce
Monitor Login Activity with Login Forensics | Salesforce
Monitor Login Activity with Login Forensics
Login forensics helps administrators better determine which user behavior is legitimate to prevent identity fraud in Salesforce.
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Companies continue to view identity fraud as a major concern. Given the number of logins to an org on a daily—even hourly—basis, security practitioners can find it challenging to determine if a specific user account is compromised.
Login forensics helps you identify suspicious login activity. It provides you key user access data, including:
The average number of logins per user per a specified time period
Who logged in more than the average number of times
Who logged in during non-business hours
Who logged in using suspicious IP ranges
There’s some basic terminology to master before using this feature.
Event
An event refers to anything that happens in Salesforce, including user clicks, record state changes, and taking measurements of various values. Events are immutable and timestamped.
Login Event
A single instance of a user logging in to an organization. Login events are similar to login history in Salesforce. However, you can add 
HTTP
 header information to login events, which makes them extensible.
Login History
The login history that administrators can obtain by downloading the information to 
.cvs
 or 
.gzip
 file and that’s available through Setup and the API. This data has indexing and history limitations.
Administrators can track events using the LoginEvent object. There’s no user interface for login forensics. Use the Force.com IDE, Workbench, or other development tools to interact with this feature.
Note
Login forensics isn’t available on government pods.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=event_monitoring_faq.htm&language=en_US
Release
202.14
