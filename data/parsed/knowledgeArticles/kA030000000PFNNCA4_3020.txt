### Topic: INVALID_FIELD_FOR_INSERT_UPDATE:Attempting to update (as part of an upsert) parent field = An error caused by trying to update a parent record through a relationship field. This article will explain how to avoid this problem.
You may encounter a Salesforce customer getting an "INVALID_FIELD_FOR_INSERT_UPDATE: Attempting to update (as part of an upsert) parent field"  error when attempting to insert / update / upsert records via the API using Dataloader, Informatica, Workbench, Etc....
This error occurs because the customer has mapped values to a parent field, which may not be allowed.
Resolution
Please check the mapping file for the API tool, such as the dataloader sdl or the Informatica mapping file. If the customer is attempting to update another object through the relationship field, this may be the problem. The resolution for this problem will be to try and update the parent object in a separate job.
Example:
Contact.firstname =Ok!
Contact.Accountid = Ok!
Contact.Account.AccountName = BAD!
Another example: Customer is trying to upsert Agreement__c object and receiving the error "INVALID_FIELD_FOR_INSERT_UPDATE:Attempting to update (as part of an upsert) parent field Customer__c with new value 001V000000ALga6IAD, current value is 001V000000AP1wkIAD:Customer__c --". 
Resolution: Agreement__c has a master-detail relationship with Account. As part of the upsert Customer__c field was getting updated which is an ID field on Account object. Removing this ID field during upsert was the resolution.
By default, records can’t be reparented in master-detail relationships. Administrators can, however, allow child records in master-detail relationships on custom objects to be reparented to different parent records by selecting the "Allow reparenting" option in the master-detail relationship definition as an alternative means to resolve this error. See 
Relationships Among Objects
 for more details.
 
