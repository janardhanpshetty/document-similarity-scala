### Topic: How to update the headers of the custom report sections due to the Custom Object name changed
When you change the name of a custom object the section headers of the custom report associated to that report do not update, how can the headers be updated?
Resolution
Once you have changed the Name of a custom object, you will find that the Custom Report types will not update the Section Headers to update these you will have to:
Go to Setup
Create
Report type
Then click on each of the custom reports for that object
Then click the Edit Layout button
Scroll to the section and click Edit in the Header
Enter the New Object Name
Click Save
