### Topic: How to prevent lead or case assignment rules from triggering every time a case is edited?
Lead or Case assignment rules trigger every time a lead or case is edited because the "assign using active assignment rules" checkbox is checked as default.
Resolution
To resolve this:
Click on your name | Setup 
Go to Customize | Cases | Page Layouts 
Click EDIT next to the relevant layout. 
On the Layout console click on "Layout Properties" 
Un-check on the "Select by default" property against 'Case Assignment Check box' to disable. 
Click on OK and save the changes to the page layout
Note
: The same steps can be used for Leads object. In step 2 above, select 'Leads' in place of 'Cases' 
Remember
: With Summer '15 release, 
The options available for the Case Assignment Checkbox 
and Lead Assignment Checkbox 
now respect both the 
Default 
setting and the 
Show on edit page
 checkbox independently. Previously, these options were not independent of each other. 
So you need to be careful while selecting these options. 
For example: I
f you want to
 force case assignment rules, select 
Default
 under Case Assignment Checkbox
. This means end users won’t see any option to turn off case assignment when editing or creating a case. You can also decide to include the Assign using active Assignment Rules
 
checkbox by selecting 
Show on edit page
. If you select both options, the assignment checkbox is displayed and is checked by default.
The default setting will force the assignment rule to run unless you also display the checkbox and a user manually deselects the checkbox.
