### Topic: Edit Picklists for Record Types and Business Processes | Salesforce
Edit Picklists for Record Types and Business Processes | Salesforce
Edit Picklists for Record Types and Business Processes
Customize the values in record type or business process picklists based on your organization’s unique needs.
Available in: both Salesforce Classic and Lightning Experience
Record types are available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Business processes are available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create or change record types:
“Customize Application”
To create or change business processes:
“Customize Application”
Select a record type or business process and click 
Edit
 next to the picklist field to change its values.
Add or remove values as needed. Users can choose from these values when creating or editing records.
Optionally, choose a default picklist value. Some picklists require a default value. The default value in a dependent field is ignored.
Click 
Save
.
See Also:
Custom Fields
Considerations When Creating Picklists
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=editing_picklists_for_record_types_and_business_processes.htm&language=en_US
Release
202.14
