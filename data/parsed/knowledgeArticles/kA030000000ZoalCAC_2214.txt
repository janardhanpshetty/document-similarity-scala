### Topic: 'Set default sync setting to Sync All Outlook Items' option under Outlook Configurations explained
With Summer '15 release, Salesforce for Outlook, introduced a new option for Outlook Configurations, 
'Set default sync setting to Sync All Outlook Items'
, under the Email Settings options.  
Setup | Desktop Administration | Outlook Configurations
 
If the setting is unchecked, the 
default 
selection during the Settings Wizard will = 
'Sync Only the Outlook Items I Select' 
(
manual
)
If the setting is checked, the 
default
 
selection during the Settings Wizard 
will 
= 
'Sync All Outlook Items'
 (
automatic
)
*With the setting 
'Don't let users change their sync setting'
 unchecked, SFO users have the ability to modify the 
default
 sync options in the Settings Wizard
 
Resolution
With the new option, 
all SFO clients will respect the value set on the server's Outlook Configuration each time the Settings Wizard executes.  
For example:
With the above configuration - 
- During the initial setup, the Settings Wizard will default to: 
'Sync Only the Outlook Items I Select' 
(
manual
)
- The SFO user opts to modify the default selection and selects
 'Sync All Outlook Items'
, and saves the initial setup
- Upon executing the Settings Wizard subsequently, the 
default 
value of 
'Sync Only the Outlook Items I Select'
 will show as default again.
**
Note:
 this does not indicate that 
'Sync All Outlook Items'
 was not selected and used initially, rather the results returned pull from the 
default 
server settings**
 
