### Topic: Documents Home | Salesforce
Documents Home | Salesforce
Documents Home
Available in: Salesforce Classic
Available in: 
All
 Editions except 
Database.com
User Permissions Needed
To view Documents tab:
“Read” on documents
To view documents:
“Read” on documents
To upload new documents:
“Create” on documents
Clicking on the Documents tab displays the documents home page.
Note
If the Documents tab is not visible, you can customize your display to show it.
Under 
Find a Document
, enter keywords to search for a document.
In the 
Document Folders
 section, select a folder to view all the documents contained in that folder.
The 
Recent Documents
 section displays the last ten or twenty-five documents you viewed, with the most recently-viewed document listed first. This list is derived from your recent items and includes records owned by you and other users. Toggle the 
Show 25 items
 and 
Show 10 items
 links to change the number of items that display.
In the 
Recent Documents
 section, click 
New
 to upload a new document.
Note
The Documents tab is not part of Salesforce CRM Content.
See Also:
Upload and Replace Documents
View Document Lists
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=docs_home.htm&language=en_US
Release
202.14
