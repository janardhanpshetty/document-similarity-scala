### Topic: Skills Overview | Salesforce
Skills Overview | Salesforce
Skills Overview
Skills allow users to share information about their professional expertise. With skills, users can discover, collaborate with, and endorse others based on their knowledge.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
The Skills feature makes it easier to identify experts in different areas. Users can add or remove skills on their Chatter profile page or on record detail pages, and other users can endorse those skills. Use the global search bar to search for experts with specific skills.
Add a Skill Via Record Detail Pages
Add skills to share your professional expertise.
Remove a Skill Via Record Detail Pages
Remove a skill if it no longer applies.
Endorse a Skill Via Record Detail Pages
Endorse a user’s skill to promote their expertise in a specific area.
See Also:
Enable or Disable Work.com Settings
Skills Customization
Skills Limitations
Add a Skill Via Record Detail Pages
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_skills.htm&language=en_US
Release
202.14
