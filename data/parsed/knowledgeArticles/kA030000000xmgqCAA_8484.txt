### Topic: This article provides the steps on how to mass update your users' email address and username through the Data Loader.
Administrators have the ability to mass update the users' email address through the Data Loader (latest API version). This is useful if your company changes their email domain or if you need to update the users' username instead of having them do it manually. 
 
Resolution
There are 3 different email address that you can update in your user records:
Username
: Login username as seen under Manage Users | Users
Email Address
: Email address to which you receive emails. This address is found under Manage Users | Users
Return Email Address
: The email that appears on your outbound emails. This can be found at Your name | Setup | Email | My Email Settings | Email Address or Your Name | My Settings | Email | My Email Settings | Email Address
All these, can be updated through the Data Loader by following these steps:
1) Export the users' Ids.
a) Log in to the Data Loader
b) Click Export
c) Select Users
d) Select the fields you need to export (ideally, you'd export the fields you want to eventually update), make sure to include the user Id.
e) Click Finish
2) Update the information on the exported csv. file
a) Be aware of the naming convention, since you need to be careful when mapping the fields to update.
You should use these names on your csv. file's columns.
Username = Username
Email Address = Email
Return Email address = SenderEmail
b) Update the information as desired
c) Save the file
3) Update the user records with the Data Loader
a) Log in to the Data Loader
b) Click Update
c) Select Users
d) Select Browse and find your csv. file, click Next
e) Click Create or Edit a Map
f) Map the fields
g) Click Next and Select the drive/location to save success and error files and then Finish
Expected Behavior 
When these fields are updated, you can expect the following:
Username: The field is updated and the user receives an email notification, notifying them of the change.
Email Address: An email is sent to the new email address so that the change can be confirmed. The field is not updated until the email address owner confirms the change. An email is also sent to the older email address informing the change. 
Return Email Address: An email is sent to the new email address so that the change can be confirmed. The field is not updated until the email address owner confirms the change.
