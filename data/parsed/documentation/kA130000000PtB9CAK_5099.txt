### Topic: Changing Your Working Division | Salesforce
Changing Your Working Division | Salesforce
Changing Your Working Division
You can change which records you are viewing by selecting the division you are currently working in.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
You can change the division you are working in at any time and override the default division you were originally assigned for some searches and reports.
From the Divisions field in the sidebar, select the division you want to work in.
Note
Records you create are assigned to you default division, not to your working division. You can explicitly set a division other than your default division when you create the record.
See Also:
Divisions Overview
Reporting With Divisions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=home_division.htm&language=en_US
Release
202.14
