### Topic: SOAP API-describeSObject() and describeSObjectResult()
As of Spring '15 SOAP API calls describeSObject() and describeSObjectResult() have been updated
https://www.salesforce.com/developer/docs/api/Content/sforce_api_calls_describesobject.htm
http://www.salesforce.com/developer/docs/api/Content/sforce_api_calls_describesobjects_describesobjectresult.htm
Resolution
describeSObject() and describeSObjectResult 
 have changed in API version 33.0 
The following field has been added to the Field subtype:
highScaleNumber
—
Indicates whether the field stores numbers to 8 decimal places regardless of what’s specified in the field details (
true
) or not (
false
). Used to handle currencies for products that cost fractions of a cent and are sold by the thousands. If high-scale unit pricing isn’t enabled in your organization, this field isn’t returned.
Reference:
http://releasenotes.docs.salesforce.com/spring15/spring15/release-notes/rn_api_calls.htm
