### Topic: During the initial load of Connect for Offline, the user may see the following warning notification bar in Internet Explorer browser:
Behavior:
During the initial load of Connect for Offline, the user may see the following warning notification bar in Internet Explorer browser:
"To help protect your security, Internet Explorer has restricted this webpage from running scripts or ActiveX controls that could access your computer.  Click here for options..."
The user will then have 3 options once they click the warning bar:
1) Allow blocked content
2) What's the risk?
3) Information
Once 'Allow blocked content' is selected, the user is then prompted if they are sure they wish to proceed.
Desired Results:
End-user does not want to see the standard Microsoft Internet Explorer ActiveX warning message
Resolution
Follow the steps below to allow ActiveX content:
1. Open Internet Explorer , go to Tools >> Internet Options 
2. Click on Advanced Tab and scroll down to find the Security section. 
3. Enable the checkbox under the Security section labeled:
“Allow active content to run in files on My Computer” 
and 
“Allow Active Content from CDs to run on My Computer”. 
4. Click Ok, close Internet Explorer and restart Connect for Offline.
 
**This has been confirmed in Microsoft Internet Explorer version 7 and 8 / Offline Edition does NOT support Internet Explorer 9**
