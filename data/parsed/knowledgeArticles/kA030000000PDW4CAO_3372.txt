### Topic: If you create a new contact from within an account record, some information from the account will automatically transfer to the new contact record.
What account information will auto-populate when creating a contact from an account record?
Resolution
If you create a new contact from within an account record, some information from the account will automatically transfer to the new contact record.
To create a new contact from an account:
             
1. Choose a specific account and click into the account detail screen.
            
 
2. Scroll down to the contact section of the account detail screen.
             
3. Click "
New
" to create a new linked contact record.
The following information will automatically transfer over to the new contact record:
             
1. The billing address on the account record to the mailing address in the new contact record.
              
2. The account name on the account record to the account field in the new contact record.
              
3. The phone and fax fields from the account record to the phone and fax fields in the new contact record.
Note:
 You will see the same behavior when running a report of Accounts & Contacts, if the contacts do not have a Mailing Address or Phone/Fax, this information will also be auto populated while seeing the report. A custom Report Type for only Contacts, will not show this behavior. If you click on a Contact record, the Mailing Address and Phone/Fax fields will still be blank. 
