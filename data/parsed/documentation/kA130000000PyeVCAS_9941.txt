### Topic: Load Quota Data for Forecasts | Salesforce
Load Quota Data for Forecasts | Salesforce
Load Quota Data for Forecasts
Load quota data for forecasts with the Data Loader or the API.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Available in: 
Professional
 Edition with the “API Enabled” permission
User Permissions Needed
To view Forecast Setup:
“View Setup and Configuration”
To edit Forecast Settings:
“Customize Application”
To enable Forecasts users:
“Manage Internal Users”
AND
“Customize Application”
To manage quotas:
“Customize Application”
AND
“Manage Quotas”
To upload quota data to Salesforce:
“Manage Quotas”
AND
“View All Forecasts”
Note
This information applies to Collaborative Forecasts and not to 
Customizable Forecasts
.
To upload quotas for users in your organization, you can use either the Data Loader application or the Salesforce APIs. The Data Loader provides a simple point-and-click method for adding, inspecting, and editing data in your organization. The APIs provide additional flexibility but require you to write code. To use either method, your organization must have API access enabled. See 
Data Loader
 for more details about Data Loader.
Add Quotas with the Data Loader
Before you upload quotas, consider which version of the Data Loader you are using. For Data Loader v.30 and later, you’ll need to specify the forecast type of each quota you plan to upload.
If you are using Data Loader v.30 or later:
If you are using Data Loader v.29 or earlier:
1. Query the User object to get the IDs of your users.
1. Query the User object for your users’ IDs.
2. Make note of the names and IDs of the users for whom you want to upload quotas.
2. Make note of the names and IDs of the users for whom you want to upload quotas.
3. Query the ForecastingType object to get the IDs of your forecast types.
3. Prepare a .csv file with user names, IDs, and quotas (either amount or quantity), and the start date of each quota’s forecast period. If you use both revenue and quantity forecasts, specify the quotas for each on separate rows of your .csv file.
4. Make note of the IDs of the forecast types your organization is using.
4. Upload your quotas .csv file using the Data Loader.
5. Prepare a .csv file with user names, user IDs, forecasting type names, Forecasting Type IDs, quotas (either amount or quantity), and the start date of each quota’s forecast period.
6. Upload your quotas .csv file using the Data Loader.
Query the User object for your users’ IDs.
Use the Data Loader to retrieve your users’ IDs.
From Setup, enter 
Data Loader
 in the 
Quick Find
 box, then select 
Data Loader
.
Download and install the Data Loader application.
Launch the Data Loader.
Click 
Export
.
Enter your user name and password and click 
Log in
.
Click 
Next
.
Select the User object from the list.
Choose a file name and destination for the exported data.
Click 
Next
.
Select the 
Id
 and 
Name
 fields.
Click 
Finish
 and then click 
Yes
.
Click 
View Extraction
.
Click 
Open in external program
.
Save the file in the .csv format. You’ll add user names and IDs to your quotas spreadsheet before uploading it.
If you are using Data Loader v.30 or later, query the ForecastingType object to get the IDs of your forecast types.
If you haven’t already, from Setup, enter 
Data Loader
 in the 
Quick Find
 box, then select 
Data Loader
, then download and install the Data Loader application.
Launch the Data Loader.
Click 
Export
.
Enter your user name and password and click 
Log in
.
Click 
Next
.
Click 
Show all Salesforce objects
.
Select the Forecasting Type object from the list.
Choose a file name and destination for the exported data.
Click 
Next
.
Select the 
Id
 and 
DeveloperName
 fields.
Click 
Finish
 and then click 
Yes
.
Click 
View Extraction
.
Click 
Open in external program
.
Save the file in the .csv format.
Note the 
DeveloperName
 and 
Id
 values for each of your active forecast types. Your organization may not be using all of the forecast types that appear in the results. See the list below if you’re not sure which forecast type a specific 
DeveloperName
 refers to.
OpportunityRevenue
 : Opportunities - Revenue
OpportunityQuantity
 : Opportunities - Quantity
OpportunitySplitRevenue
 : Opportunity Revenue Splits - Revenue
OpportunityOverlayRevenue
 : Opportunity Overlay Splits - Revenue
OpportunityLineItemRevenue
 : Product Families - Revenue
OpportunityLineItemQuantity
 : Product Families - Quantity
The name of a custom opportunity split type that has been enabled as a forecast type. Custom split types are based on currency fields, which can only contain revenue amounts.
Add columns for 
DeveloperName
 and 
Id
 to your quota spreadsheet and add the name and ID of the forecast type of your quotas to each row. The name is not necessary for uploading quotas, but will help you know which forecast type you are working with in each row.
Prepare your quota spreadsheet for upload.
If you are using Data Loader v.30 or later, create a .csv file with columns for User Name, User ID, Forecast Type Name, Forecast Type ID, Quota Amount, Quota Quantity, Currency Code, and forecast period Start Date [YYYY-MM-DD].
If you are using Data Loader v.29 or earlier, create a .csv file with columns for User Name, User ID, Quota Amount, Quota Quantity, Currency Code, and forecast period Start Date [YYYY-MM-DD]. If you use both revenue and quantity forecasts, specify the quotas for them on separate rows of your .csv file.
You don’t actually need the User Name or Forecast Type Name columns, but including them makes it much easier for you to understand the contents of your .csv file.
User Name
User ID
Forecast Type Name (for Data Loader v.30 or later)
Forecast Type ID (for Data Loader v.30 or later)
Quota Amount
Quota Quantity
Currency Code
Start Date
Kevin Bailey
00599000000Hofh
OpportunityRevenue
0DbD00000001eQBKAY
250000
USD
2012–03–01
Kevin Bailey
00599000000Hofh
OpportunityRevenue
0DbD00000001eQBKAY
250000
USD
2012–04–01
Kevin Bailey
00599000000Hofh
OpportunityRevenue
0DbD00000001eQBKAY
250000
USD
2012–05–01
Kevin Bailey
00599000000Hofh
OpportunityQuantity
0DbD00000001eQAKAY
500
2012–03–01
Kevin Bailey
00599000000Hofh
OpportunityQuantity
0DbD00000001eQAKAY
500
2012–04–01
Kevin Bailey
00599000000Hofh
OpportunityQuantity
0DbD00000001eQAKAY
500
2012–05–01
If your forecast 
Data Source
 is product families, include a column for Product Family as well.
User Name
User ID
Forecast Type Name (for Data Loader v.30 or later)
Forecast Type ID (for Data Loader v.30 or later)
Product Family
Quota Amount
Quota Quantity
Currency Code
Start Date
Kevin Bailey
00599000000Hofh
OpportunityLineItemRevenue
0DbD00000001eQ9KAI
Hardware
250000
USD
2012–03–01
Kevin Bailey
00599000000Hofh
OpportunityLineItemRevenue
0DbD00000001eQ9KAI
Software
150000
USD
2012–03–01
Kevin Bailey
00599000000Hofh
OpportunityLineItemRevenue
0DbD00000001eQ9KAI
Services
50000
USD
2012–03–01
Kevin Bailey
00599000000Hofh
OpportunityLineItemQuantity
0DbD00000001eQ8KAI
Hardware
500
2012–03–01
Kevin Bailey
00599000000Hofh
OpportunityLineItemQuantity
0DbD00000001eQ8KAI
Software
300
2012–03–01
Kevin Bailey
00599000000Hofh
OpportunityLineItemQuantity
0DbD00000001eQ8KAI
Services
100
2012–03–01
Use the Data Loader to upload your quota information to Salesforce.
If you haven’t already, launch the Data Loader.
Click 
Insert
.
Log in with your user name and password.
Click 
Next
.
Click 
Show All Salesforce Objects
.
Select the Forecasting Quota object from the list.
Click 
Browse
 and choose the .csv file you want to upload.
Click 
Next
.
Click 
OK
 in the Data Selection dialog box that opens.
Click 
Create or Edit a Map
.
Map these columns to fields in the ForecastingQuota object as shown in the table.
Column headers in CSV file
ForecastingQuota fields
User ID
QuotaOwnerID
Quota Amount
QuotaAmount
Quota Quantity
QuotaQuantity
Currency Code
CurrencyIsoCode
Start Date
StartDate
Product Family (needed only when the forecast data source is Product Families)
ProductFamily
Forecast Type ID (needed only for Data Loader v.30 or later)
ForecastingTypeID
Click 
OK
.
Click 
Next
.
Click 
Browse
 and choose the directory where you want to save the log file containing messages about the success or failure of the upload.
Click 
Finish
.
Click 
Yes
 to proceed with the upload.
Click 
OK
.
As a best practice, load quota data in the quota owner’s 
personal currency
. Note that you can still upload quota data using the API even if 
Show Quotas
 is disabled. If your Data Loader time zone setting is ahead of quota owners’ time zones, the month can be off by one. To avoid this problem, use a date greater than or equal to the third day of each month when inserting quotas.
Uploading Quotas with the API
When uploading quota information with the API, be sure to use the correct API version, depending on the type of quota data you are working with. If your organization has more than one type of forecast enabled, each forecast type maintains its own separate quota information.
When importing...
Use API version...
Revenue quotas for opportunity-based forecasts
25.0 or later
Quantity quotas for opportunity-based forecasts
28.0 or later
Revenue quotas for opportunity splits-based forecasts
29.0 or later
Revenue or quantity quotas for product family-based forecasts
29.0 or later
Quotas in organizations with more than one forecast type enabled
30.0 or later
See Also:
Enable Quotas in Forecasts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=forecasts3_loading_quota_data.htm&language=en_US
Release
202.14
