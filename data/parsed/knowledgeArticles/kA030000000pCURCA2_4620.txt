### Topic: Process Builder evaluates cross-object references to lookup fields on Person Accounts as null when triggered via the API
When a Process is triggered on a record related to a Person Account via the API, and the Process has a cross-object reference to a lookup field on that person account, the process will see that lookup field as null.
Example Scenario:
- Create a Process on Opportunity, with no criteria, and an action to set the Opportunity Owner to the owner of the Account (i.e. [Opportunity].Account.OwnerId).
Results:
- Process works as expected on all records in the User Interface, and on Opportunities related to Business Accounts when triggered via the API
- When triggered on Opportunity related to a Person Account via the API, the process sees the Account.OwnerId as null and tries to set the Opportunity Owner to null (which produces an error because a record owner cannot be null).
 
Resolution
This is an expected behavior of Person Accounts; In the API, Person Accounts are handled in the context of Contacts, while in the User Interface they are handled in the context of Accounts. This results in Lookup Fields on Person Accounts being treated as unavailable (null) by Process Builder. 
To work around this, have the Process call a Flow. The Flow can in turn query the Account record, store the lookup field values in flow variables, and perform any desired actions (using decision to check if the Lookups on the Account meet certain criteria, record update element to update records, etc).
 
