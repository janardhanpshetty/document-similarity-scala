name := "documentsimilarity"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= {
  val sparkVersion = "2.1.0"
  Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
    "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
    "org.apache.spark" %% "spark-streaming" % sparkVersion % "provided",
    "org.apache.spark" %% "spark-mllib" % sparkVersion % "provided",
    "edu.stanford.nlp" % "stanford-corenlp" % "3.4",
    "edu.stanford.nlp" % "stanford-corenlp" % "3.4" classifier "models",
    "edu.stanford.nlp" % "stanford-parser" % "3.4",
    "org.scalatest" %% "scalatest" % "2.2.6" % "test"
  )
}

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF")           => MergeStrategy.discard
  case PathList("javax", "servlet", xs @ _*)         => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".html" => MergeStrategy.first
  case "application.conf"                            => MergeStrategy.concat
  case "unwanted.txt"                                => MergeStrategy.discard

  case x => val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)

}