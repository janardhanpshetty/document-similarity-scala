### Topic: Is there any way to send email from Apex code using existing email template? Yes ! we can do the same using sample code provided here.
Send email from Apex code using Email Template already created in the org. Also, to fetch email address from target objects (
Lead, Contact
).
Resolution
Yes ! we can do the same using code below.
 
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 

List <Contact> Con= [select id, name from Contact where name =:'Mithilesh Kumar']; 

mail.setTargetObjectId(Con[0].id); 

mail.setSenderDisplayName('Salesforce Support'); 

mail.setUseSignature(false); 

mail.setBccSender(false); 

mail.setSaveAsActivity(false); 

EmailTemplate et=[Select id from EmailTemplate where Name=:'SFDC TEST']; 

mail.setTemplateId(et.id); 


Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 



