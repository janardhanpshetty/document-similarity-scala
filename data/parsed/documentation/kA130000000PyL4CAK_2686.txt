### Topic: Updating Orders | Salesforce
Updating Orders | Salesforce
Updating Orders
After you’ve created an order, make changes as needed to update the status, add or edit its products, or create a reduction order.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Reduction orders available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Edit
 the order to update its details.
Add
 and 
edit
 order products to identify the products or services requested.
Activate
 the orders.
Deactivate
 orders.
Reduce
 an activated order.
See Also:
Orders
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=order_update.htm&language=en_US
Release
202.14
