### Topic: When attempting to save any Apex class or Triggers through Salesforce UI, you may face this "Record Not Available " error
Developers 
may sometimes encounter 
Record Not Available
 Errors
 when attempting to save any apex classes  or triggers through UI. This may be due to other developers in the org attempting to save related apex classes or triggers at the same time which results in classes to be invalidated and recompiled.
When Developer use UI to modify APEX Classes Or Triggers, the save process is synchronous and it tries to take out an ORG-Wide Compile Lock and then Compile the class and all of it's dependencies. This can be extremely time consuming in Orgs with a lot of metadata. This may also result in Organization Administration Locked Errors. 
Other Possible reasons for this error:
An Individual User could conflict with other users making modifications to the related apex classes
An Individual Conflicting with themselves , if using multiple tabs
If the user hits "Quick Save" multiple times before their Last compile finishes 
Resolution
Recommendation:
Each developer to use their own dev sandbox 
and migrate the changes to a single sandbox 
Use Developer Console to modify code. 
As mentioned by the Product Management here : 
https://success.salesforce.com/ideaView?id=08730000000kuJtAAI
"
Moving forward, the Developer Console will be the supported tool for browser-based development on the platform.Over the next few releases, we will be retiring the old setup pages entirely.
" (
Safe Harbor)
