### Topic: This article gives the details of steps to be followed for handling MERRILL LYNCH cases
1.) 
PLEASE READ AND REVIEW SHI 
and follow what it says.
2.) If you are part of the Merrill Lynch team, please Start at step 4.
3.)
 For all other teams, please follow SHI and use "Assign using active assignment rules" check box to assign the cases to the right queue.
 
Resolution
4.) Confirm if this is a new team or FA just needs to be added to a team.
5.) If the Fa needs to be added to a team, please email the 
Gatekepper
 and get appproval to add the FA to the team.
6.) If this is a new FA team setup, find out if the fa needs to roll to a CA already created or if one needs to be created for this team.
7.) After the roles have been created add the FA to the FA role. 
8.) Email the FA letting them now its done and close the case. 
 
