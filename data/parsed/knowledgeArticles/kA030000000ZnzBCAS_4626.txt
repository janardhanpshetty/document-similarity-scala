### Topic: This article is to address the issue when attempting to delete picklist value, you might get the error "The picklist value you attempted to delete is the last value for the following record types. This picklist must contain at least one value. Either add another value to the picklist or to the following record type."
ISSUE:
When attempting to delete picklist value,  you might get the error "The picklist value you attempted to delete is the last value for the following record types. This picklist must contain at least one value. Either add another value to the picklist or to the following record type."
 
Resolution
CAUSE:
The reason is this pick list value must have been referenced by one or more record types of the object and this value is the only one selected value of these record types  
Resolution:
Go to Record Types of associated the object in Setup
Enter into each record type, find the picklist in the section of "
Picklists Available for Editing
" and click edit
If the picklist value you want to delete displays under the column of "selected values", move this value to the left panel 
Because at least one value must be selected, 
you need to add at least one value from left panel rather than the one you just move back to the left panel
Click Save
After you carry out the above steps,  you will be able to delete the picklist value in the field definition. 
See Also:
Editing Picklists for Record Types and Business Processes
 
