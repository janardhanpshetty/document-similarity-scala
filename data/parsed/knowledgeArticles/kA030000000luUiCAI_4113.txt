### Topic: The article explains how users with permissions to Transfer Leads can transfer them to any account or User accessible to them.
How can Users who have "transfer record"/"transfer Leads" permissions be stopped from transferring Leads to certain Accounts or Users?
Resolution
If a User has the appropriate permissions to transfer Leads, they can transfer the Leads into any Account they can view or create. They can also automatically transfer Leads to any User or Partner they can view who can own Lead records
"Transfer Leads" and "Transfer Records" are not available in Profile for Community Users however by default they can transfer lead if they are the owner of the record
