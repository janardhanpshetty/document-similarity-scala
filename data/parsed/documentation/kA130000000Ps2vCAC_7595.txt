### Topic: Viewing the Queue Membership Related List | Salesforce
Viewing the Queue Membership Related List | Salesforce
Viewing the Queue Membership Related List
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view user details:
“View Setup and Configuration”
To view the queues a user is a member of, from Setup, enter 
Users
 in the 
Quick Find
 box, then select 
Users
 and select the user. In the Queue Membership related list, you can:
Click 
New Queue
 to create a queue.
Click a queue name to view its details.
Help Site URL
Release
202.14
