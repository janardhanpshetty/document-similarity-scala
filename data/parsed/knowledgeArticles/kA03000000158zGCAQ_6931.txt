### Topic: When attempting to create a new or deleted picklist value in sandbox you may encounter a duplicate values error despite the picklist value not being listed in the field's setup section.
This can occur in Developer or Developer Pro sandboxes when the user who created or last modified the picklist value is not copied over to the sandbox as a part of the refresh, such as portal or community users as outlined in the 
Sandbox Setup Tips and Considerations
,
"
The copy process doesn’t copy Contact data to Developer or Developer Pro sandboxes. Therefore, Customer Portal users aren’t copied. However, the copy process does copy the Customer Portal licenses, so you can create Customer Portal users in these sandboxes as needed.
"
If a record is created with a picklist value that doesn't exist, that picklist value is automatically added to a list of inactive picklist values for the field. This is outlined in the SOAP API Developer's Guide documentation for the 
Picklist Field Type
,
"
The API does not enforce the list of values for advisory (unrestricted) picklist fields on create() or update(). When inserting an unrestricted picklist field that does not have a PicklistEntry, the system creates an "inactive" picklist value. This value can be promoted to an "active" picklist value by adding the picklist value in the Salesforce user interface.
"
So in this circumstance, a user who does not typically have permission to directly create a picklist value via setup may be able to create one by inserting or creating a record that contains a non existent picklist value.
If the inactive value is then made official or active by adding it to the field's setup it will maintain the portal or community user's details in the created by system field for the picklist value.
Resolution
To correct this behavior, you will need to log a case with Salesforce Support with reference to this knowledge article.
Support will need to run a fix to address this unexpected issue in the affected sandbox(es). To help expedite case handling please provide and confirm the following details in your case's description:
- Sandbox org Id:
- Link to affected picklist's setup section:
- Picklist values you're unable to add due to the duplicate value error:
- Steps to reproduce the behavior and your permission for Support to do so in the affected environment
- Permission to run the fixer in the affected and provided sandbox org Id:
- Affected sandbox's related production org Id:
- Permission to turn the feature, DOT Enabled on in your production organization:
Note:
 See the below explanation for this feature's requirement.
- Reference Internal Article # 000232969 for Support
- Grant login access as a System Administrator in the affected sandbox(es) and their related production org. See 
How to Grant Login Access to a Salesforce Success Agent
To resolve the underlying cause of the issue and to prevent future sandboxes from being affected, it's necessary to delete, replace, and re-create the affected picklist values in the field's setup section within the sandbox's related production environment. However, under normal circumstances when you delete a picklist value, the value is not actually deleted from Salesforce and instead is retained as an inactive picklist value. When you re-create it or add it again after it has been 'deleted' the inactive picklist value is then made active again.
So to effectively delete the value to properly re-create it and input a proper Salesforce user's details into the created by audit field for the picklist value, we need to enable an org feature in production called, "DOT Enabled." 
DOT is short for default org template and it's a feature that's typically reserved for only Partners in order to create Trialforce templates but in this circumstance we'll be using it for a different purpose. When the feature is enabled it exposes inactive picklist values (that you usually wouldn't see otherwise) in the field's setup section and allows for actual deletion. 
Before we can enable the DOT feature in your production environment we will need your explicit approval and permission to do so documented in the Support case. The feature will be enabled for as short a time as possible and turned off once deletion and recreation of the affected picklist values is completed and may not remain enabled.
Once enabled we can create a placeholder picklist value, delete the affected picklist value and choose to replace it with the placeholder value, then recreate the affected value and replace placeholder value with it.
