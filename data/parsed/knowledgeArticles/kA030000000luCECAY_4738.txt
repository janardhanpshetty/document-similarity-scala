### Topic: This article would help you understand why a user gets "URL no longer exists" error message when attempting to access a record and how to resolve it.
There may be several reasons to see a "URL no longer exists" error message when attempting to access a record, such as an Account. However, when accessing a record using standard functionality, users should see something like https://xx#.salesforce.com/<recordId>. For this error, often when you view a record, it will initially start loading, but then the page suddenly reloads, and you are presented with this error.
 
Resolution
When a user reports, these kind of errors, check whether the page layout it concerns, contains a Visual Force component. If so, make a note of which VF component(s) it concerns, remove it temporarily from the Page Layout, save and check whether you, and or the affected colleague are now able to view the record.
Mind you, when you and the affected user have different page layouts, to check the correct one.
If removing the VF component from the page layout resolves the issue, the issue is probably be with that component, you should contact the developer of that component, to see whether they are aware of the issue, and if they can solve the issue for you.
