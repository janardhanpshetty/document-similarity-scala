### Topic: Learn why Smart URL's don't prompt the viewer to "Like" the Facebook page if opened on a mobile device or tablet.
Learn why when clicking the same 
Smart URL
 as a PC (desktop/laptop), but using your mobile device (smartphone/tablet), there's no prompt to "Like" the Facebook page and the content is immediately displayed. 
Resolution
 Important: 
The Fan Gate will NOT appear to Users who access your tab on mobile/tablets via the 
Smart URL
. The reason for this is important. It will still appear to any Users who click the Smart URL and are accessing it from a desktop or laptop computer. 
In order to "fan-gate" a mobile version of the tab, you have to know whether that person is a fan or not. This isn't as simple as when a User comes to your tab on a desktop or laptop. It would require a Facebook Auth dialog and 3-4 clicks, including before a User can finally "Like" your page and access the "fan-only" content, which is usually the sweepstakes, promotion, coupon, or email signup that you want them to access in the first place. Since this would initiate a high drop-off rate and less than optimal experience for Users, accessing it from a mobile device, we deactivated the Fan Gate for anyone who accesses the tab from a mobile device or tablet.
We hope this will ensure as many conversions and as much interactions as possible with your tabs. 
