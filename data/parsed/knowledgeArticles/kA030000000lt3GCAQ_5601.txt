### Topic: How to install and collect network traffic information using Fiddler 4
1. Download Fiddler 4 on the local machine from 
http://www.telerik.com/download/fiddler
2. Walk through the installation procedure
3. On the local machine navigate to "All Programs" and start Fidder 4
4. Once Fiddler is running navigate to the tab Tools | Fiddler Options | Https 
5. Here check "Decrypt Https" and agree to any prompts to install the needed certificate for https decryption
6. Select "Ignore Server Certificate errors" and "ok" out of the Fiddler options.
7. Ensure F12 (Capture traffic is selected)
8. Run the application or processes that needs debugging and do regular work until the issue seems to have re-occurred.
9. In Fiddler navigate to File | Save | All sessions and save them as a .saz file
10. Zip the file and attach to the support case along with the information the exact time the issue occurred.
Resolution
