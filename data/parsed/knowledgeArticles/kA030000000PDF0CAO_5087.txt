### Topic: How can I add members to a campaign based on Opportunity information. If the Contact Roles feature is being used, an Opportunity with Contact roles report can be created based on the specific Opportunity criteria needed.
How can I add members to a Campaign based on Opportunity information.
Resolution
If the Contact Roles feature is being used, an Opportunity with Contact roles report can be created based on the specific Opportunity criteria needed.
Once run, the "Add to Campaign" button is displayed, and this will add all the Contacts listed under the indicated Opportunities.
Note, all Contacts listed under the Contact roles related list are added. The system tries to add Private Contacts as well and if the user doesn't have access to that Contact then the process will error. See 
How can I report on Private Contacts?
 for more information.
