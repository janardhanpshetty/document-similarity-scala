### Topic: This formula allows you to verify if a phone number is 10 digits with an extension included in the same field. the extension has to be preceded by an X or x
This validation rule checks a few things:
1.) If the phone number is 10 digits long without an extension
2.) If the phone number is 10 digits with an extension of up to 4 digits
3.) If the phone number is just 10 repeating digits
Resolution
Follow These steps:
1.) Go to 
Setup
2.) Go to 
Customize
3.) Go to 
Contacts
4.) Go to 
Validation Rules
5.) Make a 
New rule
 and put this as the criteria
 
AND(
 
/* This part is to make sure the rule doesn't fire on a blank value Not() is used because the validation rule fires when it is true, we want it to fire when it is not blank */
 
NOT(ISBLANK(Phone)),
 
OR(
 
/* This First Part is an If Statement to ensure the number is 10 digits long and logically checks if it has an extension in the format of xdddd or x dddd only 4 digits is possible */
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,5), "x[0-9]{4}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x[0-9]{4}")),
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,4), "x[0-9]{3}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x[0-9]{3}")),
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,3), "x[0-9]{2}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x[0-9]{2}")),
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,2), "x[0-9]{1}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x[0-9]{1}")),
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,6), "x\\ [0-9]{4}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x\\ [0-9]{4}")),
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,5), "x\\ [0-9]{3}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x\\ [0-9]{3}")),
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,4), "x\\ [0-9]{2}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x\\ [0-9]{2}")),
 
IF(
 
AND(OR(Contains(Phone, "x") , Contains(Phone,"x")),REGEX(RIGHT(Phone,3), "x\\ [0-9]{1}")),
 
NOT(REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}\\-[0-9]{4} x\\ [0-9]{1}")),
 
IF(
 
LEFT(Phone,1)="(",
 
NOT( REGEX(Phone, "\\([0-9]{3}\\) [0-9]{3}-[0-9]{4}|\\d{10}")),
 
NOT(REGEX(Phone, "\\D*?(\\d\\D*?){10}"))
 
))))))))),
 
/* This part is to ensure that the number is not XXXXXXXXXX. NOTE: this does not work with 1111111111 */
 
mid(Phone, 2, 1) + mid(Phone, 2, 1) + mid(Phone, 2, 1) + mid
 
(Phone, 2, 1) + mid(Phone, 2, 1) + mid(Phone, 2, 1) + mid(Phone, 2, 1) + mid
 
(Phone, 2, 1) + mid(Phone, 2, 1) + mid(Phone, 2, 1) = mid(Phone, 2, 3)+ mid(Phone, 7, 3)+ mid(Phone, 11, 4),
 
/* This is to ensure the phone is not 1111111111 */
 
Phone = "1111111111"
 
))
