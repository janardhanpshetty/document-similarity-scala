### Topic: When using Predictive Intelligence to set triggered messages to your customers, it is important to understand "the why" and "the what". Why are triggers essential for driving customer engagement and re-engagement with your brand? What types of triggers are available for the use-case and how to maximize their effectiveness?
MARKETING CLOUD
Predictive Intelligence Triggers
Best Practices
Predictive Intelligence (PI) is a powerful tool that allows you to use your customers behavioral data to recommend products both on your website and through email communication.
When using Predictive Intelligence to enhance your Email and Web Recommendations - it is important to understand the different ways recommendations can impact your performance and what is the best scenarios to use given the situation.
Just like any content or messaging to customers, we also recommend that you test different scenarios within your emails to ensure you are providing the most influential recommendations for your customers. 
How much is too much PI?
There can be too many recommendations given and overwhelm your customer. Adversely - you can also not have recommendations present in enough places to really make an impact and see an increase in revenue.
What we recommend for ‘how much’ is simple: 5-5-2
5 - Pages on your site with recommendations (Web Recommendations)
5 - Marketing Emails that include recommendations (Email Recommendations)
2 - Triggers using behavior rules (Email Recommendations)
Help Terms to know as you read more about PI:
Zone - Location block on a page. These items can be horizontal and/or vertical. 
Items - Number of Products to be recommended within each zone
Product Attributes - Attributes from the feed that should be returned with a product image.
Scenarios - Algorithms built within the Customer Intelligence Engine (CIE) to provide personalized recommendations. Personalization is generated from the customer profile and Wisdom of the Crowd (WoC) behavior, such as views, purchases and cart items.
Other Best Practices to consider:
Zones should always be ‘above the fold’ on an email or web page. This means that when a browser is open, the recommendations will be visible without scrolling.
Test your scenarios - do this in two week increments to get enough data to measure against, but testing two scenarios against each other will ensure you are providing the correct type of recommendations for your customers.
Product Attributes - the following attributes should be included within every zone (when available)
Product Name
Regular Price
Sale Price
Star Ratings
There are links to more Best Practices for PI Web Recommendations, Email Recommendations and Triggers if you want to keep digging into how to maximize your use of our predictive tool.
 
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
