### Topic: How to update a Connection Owner and the information under the "Their company is represented by..." fields in the Salesforce to Salesforce connection settings..
When modifying the Connection Owner under "Our company is represented by...", this will change what the company you are connected to sees under their "Their company is represented by..." section, and vice versa.
 
 
Resolution
If the information needs to be updated that you see under the "Their company is represented by..." you need to contact the company of that connection and they will need to update their end.
There is a new person that I need to change my connection with:
If the person listed at the connection Company no longer is active - they need to change their sides "Connection Owner" by going to the Connections tab and edit the Connection Owner. This will then reflect what you see for those fields after the systems synchronize, which happens automatically after the change.
The connection is the same person, but I need to update their email address:
If the Connection Owner is the same, but they have had an email change, that information needs to be updated in the person's own org within their User Record under Setup | Manage Users | Users. After the email address is updated in their user record, they will then receive a verification email that they will have to acknowledge. Once that is done, then the address will be corrected in your connection automatically.
​
 
