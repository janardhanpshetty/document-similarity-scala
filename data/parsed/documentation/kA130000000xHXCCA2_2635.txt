### Topic: Performance Chart | Salesforce
Performance Chart | Salesforce
Performance Chart
Use the performance chart on the Home page in Lightning Experience to track your sales performance or the performance of your sales team against a customizable sales goal.
Available in: Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
The performance chart displays data based on your sales team’s opportunities if you have an associated team. Otherwise, the chart displays opportunities you own. 
Only opportunities for the current sales quarter that are closed or open with a probability over 70% are displayed.
Closed—The sum of your closed opportunities.
Open (>70%)—The sum of your open opportunities with a probability over 70%. The blue line in the chart is the combined total of the closed opportunities and open opportunities with a probability over 70%.
Goal—Your customizable sales goal for the quarter. This field is specific to the performance chart and has no impact on forecast quotas or any other type of goal. Click 
 to set the goal.
Hover over the chart to see the closed and committed opportunity amounts for different dates. If you hover over a date when an opportunity was closed or set to a probability over 70%, a blue dot appears. Click the dot to see a window with more opportunity details.
See Also:
Lightning Experience Home Permissions and Settings
The Assistant
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=home_performance_chart_lex.htm&language=en_US
Release
202.14
