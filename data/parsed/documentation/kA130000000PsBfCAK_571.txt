### Topic: Why isn’t Salesforce for Outlook adding emails to the right contact or lead in Salesforce? | Salesforce
Why isn’t Salesforce for Outlook adding emails to the right contact or lead in Salesforce? | Salesforce
Why isn’t Salesforce for Outlook adding emails to the right contact or lead in Salesforce?
One of the following may be occurring.
If you see
Salesforce for Outlook may be adding your emails to
You should
Duplicate records in Salesforce Side Panel
The wrong records
Consider merging the duplicate records
No matching record in Salesforce side panel
My Unresolved Items
Add the matching contacts or leads to Salesforce and associate the unresolved emails with your new contacts or leads
Remove your company’s domain from 
Excluded Domains
 in your My Email to Salesforce settings
See Also:
Edit Your Salesforce for Outlook Email Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_sfo_why_cant_add_emails_to_right_record.htm&language=en_US
Release
202.14
