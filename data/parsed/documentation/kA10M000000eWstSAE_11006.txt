### Topic: Considerations for Calendars Created from a Salesforce Object | Salesforce
Considerations for Calendars Created from a Salesforce Object | Salesforce
Considerations for Calendars Created from a Salesforce Object
Considerations for calendars created from a Salesforce object include support for list view filtering, a limit on the number of items that can be displayed, no support for subscribing to calendars, and support for standard objects.
Available in: 
Enterprise
, 
Unlimited
, and 
Developer
 Editions
List View Filtering
No list views are available for filtering calendars based on the Events or Tasks objects. For calendars created from other objects, list views with the scope My and All are supported, but list views with the scope My Team are not.
Calendar Item Limit
You can view up to 150 calendar items total in the day or week view, including Salesforce events and the items on calendars you create. Suppose that you select a calendar containing enough items to push the total past the limit in that view. Lightning Experience displays an alert and hides items on other calendars.
Subscribing to Calendars
You can’t subscribe to calendars that other people create. Each calendar can be accessed only by the person who created it.
Standard Objects Supported
Account
Asset
Badge Received
Campaign
Case
Contact
Contract
Email
Endorsement
Event
Holiday
Idea
Lead
Opportunity
Order
Partner
Price Book
Product
Report
Skill
Skill User
Social Persona
Social Post
Solution
Task
Topic
User
User Provisioning Request
Work Badge
Work Order
Work Thanks
For more information, see 
Activities: Events and Calendars
 in What’s Not in Lightning Experience.
Parent topic:
 
Use Calendars to Track and Visualize Dates in Salesforce Objects
Previous topic:
 
Create a Calendar to View Open Cases by SLA
See Also:
Create a Calendar
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=calendar_create_limitations.htm&language=en_US
Release
202.14
