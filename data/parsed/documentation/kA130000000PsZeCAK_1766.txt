### Topic: How can I be sure that cases won't be lost? | Salesforce
How can I be sure that cases won't be lost? | Salesforce
How can I be sure that cases won't be lost?
If your organization exceeds its daily Web-to-Case limit, the Default Case Owner (specified in the Support Settings) will receive an email containing the additional case information. If a new case cannot be generated due to errors in your Web-to-Case setup, Customer Support is notified so that we can assist you in correcting it.
If your organization is using On-Demand Email-to-Case, Salesforce ensures that your cases won't be lost if users submit them during a scheduled Salesforce downtime.
See Also:
Web-to-Case FAQ
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_cases_how_can_i_be_sure.htm&language=en_US
Release
202.14
