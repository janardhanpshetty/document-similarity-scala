### Topic: Customize the Self-Service Style Sheet | Salesforce
Customize the Self-Service Style Sheet | Salesforce
Customize the Self-Service Style Sheet
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set up the Self-Service portal:
“Manage Self-Service Portal”
To modify Self-Service pages:
“Manage Self-Service Portal”
AND
“Customize Application”
Note
Starting with Spring ’12, the Self-Service portal isn’t available for new orgs. Existing orgs continue to have access to the Self-Service portal.
Select a predefined color theme, or download a sample Self-Service color theme so you can customize it. This color theme allows you to incorporate your organization’s branding into your Self-Service portal.
Note
To customize the Self-Service color theme using a point-and-click editor, see 
Customizing Your Self-Service Fonts and Colors
.
From Setup, enter 
Self-Service Portal
 in the 
Quick Find
 box, then select 
Settings
.
Click 
Self-Service Setup
.
Click the 
View Color Theme Options
 link in the page settings section.
Find a set of fonts and colors you like and click 
Download This Color Theme
.
To use a predefined color theme without customizing it, simply click 
Select This Color Theme
.
Save the color theme you downloaded and give it to your webmaster if it needs more customization. The downloaded color theme is a CSS style sheet that your webmaster can edit.
Store the downloaded style sheet in a publicly accessible location and enter the URL for your style sheet in the 
Style Sheet URL
 field.
Click 
Save
.
See Also:
Setting Up Self-Service
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_the_style_sheet.htm&language=en_US
Release
202.14
