### Topic: It is possible to remove the Marketing Cloud logo from the footer of Salesforce integration sends.
It is possible to remove the Marketing Cloud logo from the footer of Salesforce integration sends.  This applies to V2 and V3 versions of the integration only. 
Resolution
Modify the Brand Tag 
default_sf_html_footer for the client to remove the image for the logo. 
