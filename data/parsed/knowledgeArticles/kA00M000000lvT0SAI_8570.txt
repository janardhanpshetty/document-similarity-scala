### Topic: Navigation Menu in Lightning Experience (LEX) is blank or missing on left-hand side.
Lightning Users are able to switch over to the Lightning Experience successfully but, after doing so, find that the Navigation Menu along the left-hand side is missing expected items or is altogether blank.  There may be an error message in the center of the screen stating:
"
You don't have access to any items. Please contact your Salesforce admin for access to the items you need
.
"
This signifies that there is likely a Custom Navigation Menu in existence with 
limited 
or 
zero 
Menu Items selected for it, which is why affected users can switch to Lightning but are unable to navigate.
 
Resolution
Only one Navigation Menu can be assigned to a User Profile at a given time.  The Default Navigation Menu is still available but it's effectively being overwritten by a custom assignment.
There are 2 options for resolving this issue:
Option 1
: Using *Setup* from within Lightning, 
add the missing Menu Items to the custom Navigation Menu
.
Option 2
: Delete the custom Navigation Menu, which will automatically revert assigned/affected users back to the Default menu.
For additional reference: 
Considerations for Custom Navigation Menus
 
