### Topic: Creating a Signup Request | Salesforce
Creating a Signup Request | Salesforce
Creating a Signup Request
User Permissions Needed
To create or view signup requests:
“Signup Request API”
Select 
Signup Request
 from the Create New drop-down list in the sidebar, or click 
New
 next to 
Recent Signup Requests
 on the signup requests home page.
Enter the information for the signup request.
Click 
Save
 when you’re finished, or click 
Save & New
 to save the current signup request and add another.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=signup_request_new.htm&language=en_US
Release
202.14
