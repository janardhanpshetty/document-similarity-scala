### Topic: Create or Edit Custom Matching Rules | Salesforce
Create or Edit Custom Matching Rules | Salesforce
Create or Edit Custom Matching Rules
Use matching rules to determine how two records are compared and identified as duplicates.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, or delete matching rules:
“Customize Application”
To activate and deactivate matching rules:
“Customize Application”
To view matching rules:
“View Setup and Configuration”
Watch a Demo
 
(3:39)
From Setup, enter 
Matching Rules
 in the 
Quick Find
 box, then select 
Matching Rules
.
If editing an existing matching rule, make sure the rule is inactive.
Click 
New Rule
 or 
Edit
 next to the existing rule you want to edit.
Select which object this matching rule will apply to.
Enter a name and description for the rule.
Enter the 
matching criteria
. 
The matching criteria is where you define which fields to compare and how. To add additional fields (up to 10 total) click 
Add Filter Logic...
 and then 
Add Row
.
If you need to adjust the matching equation, click 
Add Filter Logic...
. Here you can, for example, manually change an AND expression to an OR expression.
Save the rule.
Activate the rule.
The activation process may take some time, so we’ll send you an email when the process is complete and your matching rule is ready to use.
After the matching rule is active, it’s available to use with other Data.com Duplicate Management tools. For example, using a matching rule with a 
duplicate rule
 tells Salesforce to take certain actions when users try to save a record the matching rule has identified as a duplicate.
See Also:
Matching Rules
Matching Rule Reference
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=matching_rules_create.htm&language=en_US
Release
202.14
