### Topic: Create Compact Layouts | Salesforce
Create Compact Layouts | Salesforce
Create Compact Layouts
Use compact layouts to customize the fields that display for object records when viewed in Salesforce1 and Lightning Experience.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
User Permissions Needed
To customize compact layouts:
“Customize Application”
To view compact layouts:
“View Setup”
The first four fields on your compact layout populate the record highlights section at the top of each record view in Salesforce1. The record highlights section in Lightning Experience uses the first five fields on the compact layout.
From the management settings for the object that you want to edit, go to Compact Layouts.
Create a new compact layout and give it a label.
Add up to 10 fields.
Tip
Put the object’s 
Name
 field first to provide context for your users when they view a record.
Sort the fields by selecting them and clicking 
Up
 or 
Down
.
The order you assign to the fields determines the order in which they display.
Save the layout.
To set the compact layout as the primary compact layout for the object, click 
Compact Layout Assignment
.
Example
This image shows a sample edit compact layout page for the standard Account object:
Here is the detail page for a sample account in the Salesforce1 mobile app. You can see that the account’s name, site, phone number, and account number are concisely displayed at the top of the page.
See Also:
Assign Compact Layouts to Record Types
Notes on Compact Layouts
Compact Layouts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=compact_layout_create.htm&language=en_US
Release
202.14
