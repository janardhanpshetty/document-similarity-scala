### Topic: Special Characters | Salesforce
Special Characters | Salesforce
Special Characters
Certain characters have a special meaning in Wave Analytics.
Character
Name
Description
‘
Single quote
Encloses a dataset column name in a predicate expression.
Example predicate expression:
'Expected_Revenue' >= 2000.00
“
Double quote
Encloses a string value or field value in a predicate expression.
Example predicate expression: 
'OpportunityOwner' == "Michael Vesti"
( )
Parentheses
Enforces the order in which to evaluate a predicate expression.
Example predicate expression:
('Expected_Revenue' > 4000 || 'Stage Name' == "Closed Won") && 'isDeleted' != "False"
$
Dollar sign
Identifies the Salesforce object in a predicate expression.
Note
You can only use the User object in a predicate expression.
Example predicate expression:
'Owner.Role' == "$User.UserRoleId"
.
Period
Separates the object name and field name in a predicate expression.
Example predicate expression:
'Owner' == "$User.UserId"
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_datasets_predicate_specialcharacters.htm&language=en_US
Release
202.14
