### Topic: Links in the Salesforce for Outlook side panel may not open if the default browser is not available or the file extensions associated with it are changed.
Links in the Salesforce for Outlook side panel will open with the default web browser selected in the Microsoft Windows settings.  The default program may need to be changed if the links do not open in a browser or open in the wrong browser.
Resolution
Links in the Salesforce for Outlook side panel should open a web browser, this may not happen if the default browser is not available or the file associations are changed.
One way to address this issue would be to set a different browser as your Default browser temporarily and then make your desired browser as default again. This will reset all the file extensions associated with your browser.
How to make Internet Explorer as your default browser
How to make Fire Fox as your default web browser
How to make Chrome as your default web browser
Note*
Before making a browser your default browser, close and exit all your applications. Make sure to close SFO from the system tray as well.
If switching your default browser did not address the issue, try the following. You would change the Default program for the following files to your default browser
.htm 
.html 
.shtml 
.webp 
.xht 
.xhtml
1- Close all your web browsers and other applications. Specially Salesforce for Outlook by right clicking on the Salesforce for Outlook system tray icon at the bottom right corner of your screen and click on Exit
2- Go to the 
Windows Control Panel\Programs\Default Programs 
3- Click on 
Associate a file type or protocol with a specific program
4- On the next page, scroll down and locate each one of the 
Extensions
 below and then click on 
Change Program
... button
5- On the 
Open with 
page, select your default browser (IE, FF or Chrome) and click on 
OK 
if you do not see your default program you need to click on 
Browse 
and look for it
6- Try Step 4 and 5 for each file type
7- After you are done, reopen your default browser and then Outlook and Salesforce for Outlook and try to reproduce the issue
 
