### Topic: Searching for Campaigns | Salesforce
Searching for Campaigns | Salesforce
Searching for Campaigns
Available in: 
Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view campaigns:
“Read” on campaigns
Enter your search terms in the sidebar or header search box.
Click 
Search
.
From the search results, click an item to open it or click 
Edit
, if available.
You can also create a campaign list view to find campaigns that match specific criteria.
See Also:
Campaigns Home
Displaying and Editing Campaigns
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=campaigns_search.htm&language=en_US
Release
198.17
