### Topic: You can only assign posts across workspaces if the Social Accounts are added to both the workspaces. Otherwise, you will get an email notification, but not be able to see the post in your Workspace.
 
You receive an email notification that is a post is assigned to you, but when you open Social Studio Engage, the assigned post is not there.
 
Resolution
There currently isn't a column in Social Studio Engage similar to the My Tasks stack in the Engagement Console that shows you all of the posts assigned to you across your organization. Because Social Studio functions on a workspace basis, social accounts registered in your organization are added to a workspace so users can publish or engage with those social accounts. 
If you want to assign posts across workspaces, users must have access to the social account and have permissions within the workspace. For example, if you want to assign a post for a social account in your workspace to another user, the other user must have access to social account in a workspace or have access to your workspace. If they are in a different workspace that doesn't contain the social account, they won't see the post assignment. 
You can only assign posts across workspaces if the social accounts are added to both the workspaces.
 
