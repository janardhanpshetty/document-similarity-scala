### Topic: Salesforce does not provide official support for the Safari Mobile browser on iPad nor other mobile tablets via browsing experience. Rather, our focus is to support the Salesforce1 product. NOTE: Salesforce does support Safari via MAC OS.
What level of support is expected around Mobile Safari operation when viewing the full site on the iPad device?
Resolution
Salesforce will make our best attempt to provide a "reasonable" experience for customers when utilizing the iPad Mobile Safari browser. With this said, salesforce does not provide official support for Mobile Safari on iPad devices. Our sole focus currently, is on providing the best, most stable experience via mobile usage on the Salesforce1 supported devices/platforms.
Please refer to the official salesforce supported browsers page:
http://na1.salesforce.com/help/doc/en/getstart_browser_overview.htm
As part of this policy, there has been no change to the salesforce externally facing browser support policy. Accessing the salesforce.com desktop UI via Safari on iOS/iPad remains unsupported.
 
