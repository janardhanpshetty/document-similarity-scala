### Topic: This article describes that Salesforce does not have a functionality that will allow the filtering of Dependent Picklists in search at this time.
Dependent lookup field does not work on search filters 
If you have added a dependent lookup field to a search filter, when you perform a global search and expand "Show Filters" on results. 
Clicking on the dependent lookup field icon will not bring up any search popup window 
If you go and edit the dependent lookup field, deactivate the filtered component the same lookup on search filter will work perfectly ok.
Resolution
 This is working as designed as in our current release, Salesforce does not allow the filtering on dependent lookup.
To post an Idea for improving the Salesforce functionality please visit the 
Success Community Ideas
 page. Or perhaps vote on this existing idea: 
Include pick list values in search
