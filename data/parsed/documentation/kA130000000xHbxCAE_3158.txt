### Topic: Flow Picklist Choice Resource | Salesforce
Flow Picklist Choice Resource | Salesforce
Flow Picklist Choice Resource
Represents a set of choices that’s generated from the values of a picklist or multi-select picklist field.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Field
Description
Unique Name
The requirement for uniqueness applies only to elements within the current flow. Two elements can have the same unique name, provided they are used in different flows. A unique name is limited to underscores and alphanumeric characters. It must begin with a letter, not include spaces, not end with an underscore, and not contain two consecutive underscores.
Description
Helps you differentiate this resource from other resources.
Value Data Type
Data type of the choice’s stored value.
Object
The object whose fields you want to select from.
Field
The picklist or multi-select picklist field to use to generate the list of choices.
Sort Order
Controls the order that the choices appear in.
Example
In a flow that simplifies the process of creating an account, users need to identify the company’s industry.
Rather than creating one choice for each industry, you add a picklist choice to the flow and populate a drop-down list with it. When users run this flow, the picklist choice finds all the values in the database for the Industry picklist field 
(1)
 on the Account object 
(2)
.
On top of being easier to configure than the stand-alone choice resource, picklist choices reduce maintenance. When someone adds new options to the Account Industry picklist, the flow automatically reflects those changes; you don’t have to update the flow.
Usage
Unlike with dynamic record choices, you can’t:
Filter out any values that come back from the database.
The flow always displays every picklist value for that field—even if you’re using record types to narrow down the picklist choices in page layouts.
Customize the label for each option.
The flow always displays the label for each picklist value.
Customize the stored value for each option.
The flow always stores the API value for each picklist value.
The following picklists aren’t supported.
Global picklists
Picklists for Knowledge Articles
See Also:
Flow Screen Element: Choice Fields
Options for Choice Fields in Flow Screen Elements
Flow Resources
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_designer_resources_picklistchoice.htm&language=en_US
Release
202.14
