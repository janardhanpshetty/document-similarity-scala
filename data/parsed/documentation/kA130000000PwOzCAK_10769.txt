### Topic: Guidelines for Prospecting for Companies, Contacts, and Leads in Salesforce | Salesforce
Guidelines for Prospecting for Companies, Contacts, and Leads in Salesforce | Salesforce
Guidelines for Prospecting for Companies, Contacts, and Leads in Salesforce
Check out some guidelines to follow when prospecting for new companies and contacts with Data.com Prospector.
Available in: Salesforce Classic
Available with a Data.com Prospector license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
A green dot (
) appears in the search results next to records already in Salesforce. These records can’t be added again unless they have first been deleted from Salesforce, or your organization allows duplicates.
Tip
If a record is already in Salesforce, hover over the green dot and click the link to go directly to the record.
A blue checkmark (
) appears in search results next to records you’ve already purchased (added or exported) from Data.com. When you add Data.com contacts or accounts, each one counts against your record addition balance. If you add records you’ve already exported from Data.com, the added records don’t count against your record addition balance.
Regardless of your record addition limit or your record addition balance, you can’t manually select more than 1,000 records at a time.
Regardless of your record addition limit or your record addition balance, you can’t add more than 100,000 records at a time.
Records are considered duplicates when they have the same Data.com ID number in the 
Data.com Key
 field. This includes records that were added from Data.com as both contacts and leads. When a duplicate record is added, it will 
not
 be counted against your record addition balance.
If you can’t add one or more records to Salesforce because of errors, we’ll notify you and provide the error details in a .csv file.
If your search produces a large number of results, you won’t see any data past page 10. If this happens, you can refine your search to get the results you want. See 
How can I see complete, relevant information for Data.com search results?
If you export Data.com contacts or accounts 
instead of
 adding them to Salesforce, each exported record counts against your record addition balance. If you export records you’ve already added to Salesforce or re-export records you previously exported, the exported records don’t count against your record addition balance.
If you export a group of Data.com contacts, you can still add those contacts back into Salesforce as contacts or leads without affecting your record addition balance.
Regardless of your addition limit or the number of records you have left to add, you can’t export more than 100,000 records at a time. Records will be exported in batches of 50,000. If two batches are needed, the records will be sent in a .zip file.
If numeric values, such as phone numbers, are cut off in the .csv file, change the format of the field’s column in the .csv file to 
General
 or 
Text
, or open the file in a text editor such as WordPad.
You can import a list of exported records back into Salesforce using Data Loader. Unless your Data.com preferences are set to allow duplicate records, the import process checks for duplicates using the Data.com ID number from the 
Data.com Key
 field.
These records won’t be imported from Data Loader.
Any record that has the same Data.com ID (from the 
Data.com Key
 field) as another record within the .csv file.
Any record that has the same Data.com ID (from the 
Data.com Key
 field) as a record already in Salesforce.
Narrow Your Prospecting Search Results to Target What You’re Interested In
With a Data.com Prospector license, you can use search fields and modifiers to target just the companies, industries, and locations that you’re interested in. Then, it’s easy to save your searches.
Using Data.com FAQ
Check out answers to some frequently asked questions about Data.com for users.
See Also:
Prospect for Companies, Contacts, and Leads Right in Salesforce
Considerations for Prospecting for Companies, Contacts, and Leads in Salesforce
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=jigsaw_int_adding_guidelines.htm&language=en_US
Release
202.14
