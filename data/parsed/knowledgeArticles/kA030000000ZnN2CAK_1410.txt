### Topic: Thought it is possible for system admin of a suspended org to receive the product and service notification, there are few checks that need to be done
Is it possible for system admin of a suspended org to receive product and service notification
Resolution
Yes it is possible for system admin of a suspended org to receive product and service notification. However the following needs to be checked
Ensure that the email address is not being used elsewhere other than the suspended org. For this check for the email in Black Tab
If the same email is used in more than one org isolate to which org the email pertains to. The org ID is indicated in the product and service notification email that user receives. So this can be a good reference point
If the email is for the suspended org then check the date on which the org got suspended. This can be checked in BT by navigating to the org and scrolling down to see the history
The Product and service notification is sent by Technology communications team
The list of users is updated atleast once in a month (although no set timeline is available)
But if still any question, you can at mention the Technology Communication Chatter group. User will eventually stop getting the Product and Service notification email
