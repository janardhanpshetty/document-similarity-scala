### Topic: Site.com Bug: Sometimes even when a customer’s org has enough “Number of publishable sites” org value and the user has a Publisher role for a site, the studio still doesn’t show the button for publishing or adding domains.
Site.com bug:
Sometimes even when a customer’s org has enough “Number of publishable sites” org value
and the user has a Publisher role for a site,
the studio still doesn’t show the button for publishing or adding domains.
Resolution
******************************************************************************************************************************
NB: Developer Edition Orgs do 
NOT
 include the ability to publish site changes or to manage custom domains. This is by design and 
NOT
 a bug. 
Before proceeding with the steps below, please ensure that the customer is not using a Developer Edition Org.
******************************************************************************************************************************
 
Workaround:
Change the user’s role on that site to Designer, save it, and to Publisher again, then reload the studio.  
That will reset the roles and the studio will recognize the Publisher role again.
Steps
1. Open site
2. Goto Site Configurtation > Manage User Roles
3. Select affected publisher user and select "Change Roles" and switch to contributor.
4. Repeat, switching back to publisher.
Attach bug
W-1196860 to case if workaround resolves issue.  Otherwise, escalate to Tier3 Dev.
