### Topic: This article explains why administrators are unable to select fields as instructions on the Macro and includes the resolution on how to make them available.
When going through the Macro to add new instructions, there are only limited fields listed available to be used. Why can't I see the other fields when setting instructions? 
Resolution
Only those fields added on the layout of the action will show when setting up instructions for Macro. Make sure that the fields you need to use are added on the layout by following these steps: 
1. Click Setup > Customize > Object where the action is (e.g. Cases, Leads)
2. Buttons, Links, and Actions > click "Layout" before the action name
3. Add/Remove fields that you would like to include in your Macro instructions
4. Save
After following the steps above, you should be able to define what fields to use when creating Macros. 
If you need to setup Macros from the scratch please check 
Getting Started with Macros
.
