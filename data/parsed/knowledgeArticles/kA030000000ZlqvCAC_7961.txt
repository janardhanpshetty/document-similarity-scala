### Topic: This article will explain you that How To Associate An Existing TSO With An Existing TMO.
A Partner may want to move an existing TSO to an existing TMO.
Resolution
This information is from the ISVforce Enablement site:
https://sites.google.com/a/salesforce.com/trials/Home/trials-scrum-team/trialforce-management-source-orgs-tmos-tsos?pli=1
 
How To Associate An Existing TSO With An Existing TMO
- Log into BT on the instance that hosts the TMO
- Navigate to the TMO's BT Trialforce page (Setup -> Trialforce). You should see an Add Existing Source Organization button, if you have the View Setup and isDoingSysAdmin user perms. Click it.
- Fill in the necessary fields in the resulting page, and hit Add.
