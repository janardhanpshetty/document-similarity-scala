### Topic: Changes to Permissions page after new version (version 2.0.8) of Salesforce Classic
There was a new version (version 2.0.8) of  Salesforce Classic that has been released on A
pril 15th 2014. What has changed, as the perms page seems to include some new items?
Resolution
Salesforce Classic for Android version 2.0.8 released on April 15th 2014 utilizes Android's account manager functionality for authentication management & storage.
 
As a result, users upgrading from prior versions of Salesforce Classic will see a prompt to allow new app permissions.
 
"NEW: Add or remove accounts, create accounts and set passwords, use accounts on the device"
 
 
PLEASE NOTE:
Salesforce Classic does not interact with any other accounts or applications on the device.  The prompt for new app permissions is simply a result of how Salesforce Classic is now handling its own account information.
