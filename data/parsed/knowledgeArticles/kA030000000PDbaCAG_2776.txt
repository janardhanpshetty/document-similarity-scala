### Topic: Customers that have activated the "Show Group Calendar Details" preference may see a very wide display of calendar dates and events, which can force users to scroll horizontally across the page to see the entire calendar.
Why is my Group Calendar View so wide?
Resolution
Customers that have activated the "Show Group Calendar Details" preference may see a very wide display of calendar dates and events, which can force users to scroll horizontally across the page to see the entire calendar.
Below is a solution to reduce the need for scrolling.
Remove the preference to display event details by default on group calendars:
1. As a System Administrator, click on:
Setup | App Setup | Customize | Activities | Activity Settings
2. De-select the check box for "Show Event Details on Multi-User Calendar View" and click "Save".
This setting also reduces the need for users to scroll to the right to see all calendar dates.
After making this change users will no longer see the details of each event displayed by default on group calendar views, but they will still have access to these details by hovering the mouse over event time blocks. The event details are provided in a pop-up message.
