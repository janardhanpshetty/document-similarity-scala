### Topic: Considerations for Setting Approvers | Salesforce
Considerations for Setting Approvers | Salesforce
Considerations for Setting Approvers
When you specify approvers for a given approval step—or for the only step if you’re using the jump start wizard—keep some things in mind.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Make sure that the assigned approver has access to read the records for the approval requests. For example, a user who can’t view expense records can’t view expense approval requests.
Approval processes that let users select an approver manually also let users select themselves as the approver.
You can assign an approval request to the same user multiple times in a single step. However, Salesforce sends the user only one request.
After a record enters an approval step, the approvers for that step don’t change—even if the approval process returns to that step and the values of fields that designate the approvers have changed.
For example, an approval process’s first step requests approval from a user’s manager. If the approval request is rejected in the second step and sent back to the first step, the original manager receives the approval request again, even if the user’s manager has changed.
Assigning Approval Steps to Queues
You can assign approval requests to a queue only if the associated object supports queues. Email approval response isn’t supported for approval processes that assign approval to a queue.
When the assigned approver is a queue:
Any queue member can approve or reject an approval request that is assigned to the queue.
Approval request emails are sent to the queue email address. If the queue is set up to send email to members, approval request emails are also sent to the queue members, unless their approval user preferences are set to never receive approval request emails.
Because email notifications to a queue aren’t intended for an external audience, 
{!ApprovalRequest.External_URL}
 returns the equivalent internal URL.
Salesforce1 notifications for approval requests aren’t sent to queues. For each approval step involving a queue, we recommend adding individual users as assigned approvers, so at least those individuals can receive the approval request notifications in Salesforce1. To have both queues and individual users as assigned approvers, select 
Automatically assign to approver(s)
 instead of 
Automatically assign to queue
 in the approval step.
When an approval request is rejected and returned to the previous approver and the previous approver was a queue, the approval request is assigned to the user who approved it instead of the queue.
The Approval History related list displays the queue name in the 
Assigned To
 column and the actual user who approved or rejected the approval request in the 
Actual Approver
 column.
See Also:
Identify Assigned Approvers for an Approval Step
Considerations for Approvals
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_considerations_approver.htm&language=en_US
Release
202.14
