### Topic: Viewing Custom Links on the Home Tab | Salesforce
Viewing Custom Links on the Home Tab | Salesforce
Viewing Custom Links on the Home Tab
Available in: Salesforce Classic
Available in: 
All
 Editions except 
Database.com
This section appears on the Home tab depending on your customized home page layout.
The Custom Links section of the Home tab contains links to websites or Salesforce pages that are useful for everyone in your organization. Your administrator sets which links appear in this section.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=home_links.htm&language=en_US
Release
202.14
