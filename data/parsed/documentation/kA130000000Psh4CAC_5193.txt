### Topic: Can I connect multiple Salesforce organizations using Chatter? | Salesforce
Can I connect multiple Salesforce organizations using Chatter? | Salesforce
Can I connect multiple Salesforce organizations using Chatter?
Chatter is only available on a per-organization basis. Only users within the same Salesforce organization can communicate with each other using Chatter. 
However, you can add people outside of your company's email domains to use Chatter in your organization as a customer in groups you own or manage.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_faq_administering_can_i_connect.htm&language=en_US
Release
202.14
