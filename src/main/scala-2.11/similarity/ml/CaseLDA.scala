package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.clustering.LDA
import org.apache.spark.ml.feature.{CountVectorizer, RegexTokenizer, StopWordsRemover}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


/**
 * Created by jshetty on 9/13/16.
 * Case Subject only
 */
object CaseLDA {

    def main(args: Array[String]): Unit = {

      val getConcatenated = udf( (first: String, second: String) => { first + " " + second } )

      val spark = SparkSession.builder.master("local[8]").appName("Document Similarity for Knowledge articles").getOrCreate()
      val sc = spark.sparkContext
      val sqlContext = spark.sqlContext
      sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

      // Read Input
      val casePath = "/Users/jshetty/spark-applications/documentsimilarity/data/casedata/case_apr_sep_16_subject_english.csv"
      //val caseDescriptionPath = "/Users/jshetty/spark-applications/documentsimilarity/data/casedata/case_apr_sep_16_description_req.csv"
      val caseInput = spark.read.option("header","true").csv(casePath).cache()


      // Preprocessing
      // Get the custom stopwords files
      val stopWordsPath = "/Users/jshetty/spark-applications/documentsimilarity/input/stopwords/english_custom_stopwords.txt"
      val stopwords = sc.textFile(stopWordsPath).collect()

      // Setup Documents pipeline
      val numTopics = 10
      val numIterations = 100
      val vocabSize = 10000
      val minDocs = 3
      val maxTermsPerTopic = 10
      val documentTokenizer = new RegexTokenizer().setInputCol("SUBJECT").setOutputCol("subWords").setPattern("\\W")
      val docTokFiltered = new StopWordsRemover().setStopWords(stopwords).setInputCol("subWords").setOutputCol("subWordsFiltered")
      val docPipeline = new Pipeline().setStages(Array(documentTokenizer, docTokFiltered))
      val docDF = docPipeline.fit(caseInput).transform(caseInput)
      val docVectorizer = new CountVectorizer().setInputCol("subWordsFiltered").setOutputCol("features").setVocabSize(vocabSize).setMinDF(minDocs).fit(docDF)
      val vocabList = docVectorizer.vocabulary
      val docVecDF = docVectorizer.transform(docDF)

      // LDA model
      val docLda = new LDA().setK(numTopics).setMaxIter(numIterations)
      val docLDAModel = docLda.fit(docVecDF)

      val docTransformed = docLDAModel.transform(docVecDF)

      // Review results of Topic modeling
      println(s"$numTopics numTopics:")
      val topicIndices = docLDAModel.describeTopics(maxTermsPerTopic = maxTermsPerTopic)
      val getVocabWord = udf { (indexList: Seq[Int]) => indexList.map(vocabList(_)).toSeq}
      val topicWords = topicIndices.withColumn("termWords", getVocabWord(topicIndices("termIndices")))

      // Display
      topicWords.show()
      //docTransformed.select("SUBJECT", "topicDistribution").show(false)
      //topicWords.write.csv("/Users/jshetty/documentsimilarity/caseTopics")
      spark.stop()
    }




}
