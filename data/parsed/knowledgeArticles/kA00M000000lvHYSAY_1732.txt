### Topic: Some orgs may not have the "Enable Community Case Feed" and "Enable Email Notifications for Case Posts" options, Tier 3 can resolve this issue.
When navigating to 
Setup | Cases | Support Settings
, customers may notice that they don't have checkboxes to "
Enable Community Case Feed
" and "
Enable Email Notifications for Case Posts
". This doesn't allow them to fully configure their Community or their email notifications.
If you navigate to the Blacktab of the Organization you'll notice that the "
Enhanced Community Case Feed
" feature is disabled.
Resolution
Tier 1 / 2
Please make sure the customer is in Professional Edition or above (also valid if the customer has a Developer Edition) and if that's the case document your Escalation Template with the Instance, Organization Id, Login Access as an Affected User and steps to reproduce. Once you have all details, please escalate the case to Tier 3.
If the customer has an edition lower than PE (e.g. Group Edition) this feature isn't available for them.
Tier 3
Please activate the feature "
Enhanced Community Case Feed
" in the customer's org. This will make both "
Enable Community Case Feed
" and "
Enable Email Notifications for Case Posts
" available.
More Info (GUS)
: 
User Story to make this GA (W-2447078)
.
