### Topic: There's a limit of 50 fields on the Price Book Entry Object. Here are some suggestions to avoid hitting the limit.
There is a limit of 50 fields in the 
Price Book Entry
 Object.
This can be verified under 
Setup | Customize | Price Books | Price Book Entries | Limits
.
Is this limit hard coded? can it be increased?
Resolution
This Object is one of the rare Standard Objects that does not permit an increase. The limit of 50 custom fields cannot be changed.
As a solution you should consider creating Custom Fields on the Product level for any data that will be the same for all PriceBookEntries for a given Product. Likewise, you can create fields on Price Books to hold data that will be the same for all PriceBookEntries related to a given PriceBook. 
