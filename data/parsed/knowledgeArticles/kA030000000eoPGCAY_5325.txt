### Topic: I am encountering an issue with the AppExchange App Ringcentral. Who do I reach out to for support?
Troubleshooting and support for RingCentral or other third party application or package functionality is not provided through Salesforce Support due to the custom nature and functionality of these applications. Third party applications may require unique implementation and setup for which Salesforce does not have official resources, training or materials.
For assistance with RingCentral specifically, please reach out the application's designated team via the following options:
Phone: 1-888-898-4591
Knowledge Base
 
After the application's team investigates and if it's determined that they require Salesforce Support's assistance to resolve your issue, the partner will need to log a case on your behalf via their own designated Partner Support channels to engage Salesforce Support.
 
Resolution
If you're unable to reach the RingCentral application's team via the details above please refer to their application listing's DETAILS tab for up to date contact and support information here:
https://appexchange.salesforce.com/listingDetail?listingId=a0N30000009x2WFEAY
See Also:
Installing Packages
Does Salesforce provide support for the third party apps or installed packages?
