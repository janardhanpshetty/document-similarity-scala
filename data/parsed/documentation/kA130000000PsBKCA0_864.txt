### Topic: Data Settings for Dashboard Line Chart Components | Salesforce
Data Settings for Dashboard Line Chart Components | Salesforce
Data Settings for Dashboard Line Chart Components
Line charts are useful for showing data over time.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
For example, to see the numbers of leads created each month in a report, set record count as the 
Y-axis
 and created month for the 
X-axis
. The chart displays a line connecting the record count totals for each month. 
Setting
Description
Y-Axis
Choose what values to display on the vertical axis of your bar, column, scatter, or line chart. Depending on the chart type, axis values can be summary fields or groupings. To use the second grouping or summary field defined in the source report’s chart, select 
Auto
. When the source report has a chart, Auto picks the values used by the chart. If the Y-axis corresponds to a custom summary formula that has the 
Where Will this Formula Be Displayed?
 option set to a grouping level other than 
All summary levels
, then the X-axis and Groupings selection must correspond to that custom summary formula's grouping level.
X-Axis
Choose what values to display on the horizontal axis of your bar, column, scatter, or line chart. Depending on the chart type, axis values can be summary fields or groupings. To use the second grouping or summary field defined in the source report’s chart, select 
Auto
. 
When the source report has a chart, Auto picks the values used by the chart.
 If the X-axis corresponds to a custom summary formula that has the 
Where Will this Formula Be Displayed?
 option set to a grouping level other than 
All summary levels
, then the Y-axis and Groupings selection must correspond to that custom summary formula's grouping level.
Group By
Choose how to group information on your chart. This option is available only if the underlying report has more than one grouping. 
To use the second grouping or summary field defined in the source report’s chart, select 
Auto
.
Combination Chart
Select this option to 
plot additional values
 on this chart. The chart type you chose must allow combination charts.
Display Units
Choose a scale for displaying your chart values. For table components, this setting applies only to the first column. For best results, choose Auto to let Salesforce select appropriate units.
Drill Down to
Select where users go when they click a dashboard component: the full source report for the dashboard component; the source report filtered by the group, X-axis value, or legend entry they clicked; the detail page for a chart or table element, axis value, or legend entry; or a URL that you specify. (You can't use URLs that begin with “mailto:” or “javascript:”.) 
Filtered and record detail page drill-down are disabled when viewing dashboard charts with more than 200 values.
Parent topic:
 
Data Settings for Dashboard Chart Components
Previous topic:
 
Data Settings for Dashboard Gauge Components
Next topic:
 
Data Settings for Dashboard Metric Components
See Also:
Formatting Settings for Dashboard Line Chart Components
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dashboards_component_line_chart_data_settings.htm&language=en_US
Release
202.14
