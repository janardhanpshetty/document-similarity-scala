### Topic: The key to creating a snapshot of your pipeline as of some historical point in time is to use the "As of" option reporting.
How do I create a report to show a historical pipeline view?
Resolution
The key to creating a snapshot of your pipeline as of some historical point in time is to use the "As of" option reporting. This can be done in multiple ways.
1. 
Go to the Reports tab and create a new report
Expand the Opportunity folder and click on 
Opportunity Trends
.  Or search for "Trend" .
Select the date filter for "As of Date".
Add fields and Run the report.
NOTE:
 You can use other reports on historical opportunity data from the "Opportunity Trend Information" section such as Historical Close date etc.
2.
 Use the canned built in report already in the system: From the reports tab, search for an existing report called "
Opportunity Pipeline
".
NOTE:
 in these reports you can also see historical charts of your pipeline.
3.
  Create a Snapshot Reporting that captures data at a point in time.  This is only for going forward when this feature is used.  The above reports will report on historical data.
NOTE:
  See Report on 
Historical Data with Reporting Snapshots
 for more information
4.
 Enable the Historical Trend Reporting functionality.
NOTE:
 for more information, please review the 
Track Changes in Your Sales Pipeline
 article
