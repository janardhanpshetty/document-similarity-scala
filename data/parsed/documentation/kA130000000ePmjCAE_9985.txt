### Topic: Set Up Milestones | Salesforce
Set Up Milestones | Salesforce
Set Up Milestones
Milestones represent required steps in your support management process, like first response times. Set up and customize milestones in your org so they can be added to entitlement processes and applied to support records like cases and work orders.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions with the Service Cloud
Customize Milestone Page Layouts
Milestones appear in the Case Milestones related list on cases, and the Object Milestones related list on work orders. Customize your page layouts to help support agents and supervisors track support progress.
Enable Milestone Feed Items
Help support agents monitor support activity by enabling milestone feed items. This option posts a notification to the feed and the record owner’s profile page when a milestone is completed or violated.
Set Up the Milestone Tracker
The milestone tracker gives support agents a complete view of upcoming and closed milestones, and displays countdowns for active and overdue milestones. Add it to the case feed, work order feed, a custom page, or the service console.
Limit User Updates to Milestones
Add validation rules to milestones to prevent users from updating milestones unless certain criteria are met.
Create a Milestone
Milestones represent required steps in your support process, such as case resolution time and first response time. You’ll create “master” milestones in your org, then add them to entitlement processes to enforce different service levels on support records like cases and work orders.
Auto-Complete Case Milestones
Create an Apex trigger that automatically marks milestones Completed on cases that match unique criteria. In your trigger, define which events and related case criteria must be satisfied for a milestone to be marked Completed. You can implement a similar trigger to auto-complete work order milestones.
See Also:
Milestones
Entitlement Management Setup Checklist
Milestones: Supported Objects
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=entitlements_milestones_setup.htm&language=en_US
Release
202.14
