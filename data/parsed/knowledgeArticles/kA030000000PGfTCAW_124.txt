### Topic: How to create Blitz org for internal reproductions
Most of the times T3 & R&D uses Blitz orgs to reproduce the customer issues. That way it helps to simply the issues and no need to wait for customer to extend their expired login access.
Resolution
- To go to following link, you need to first:
-- Go to Aloha; 
https://aloha.my.salesforce.com
-- Then click on "Technology R&D" tile
- Otherwise following link won't work
The URL: 
https://cscbuild-sfm.soma.salesforce.com/
  tells the status of all blitz orgs.
1) Pick the blitz tab that you require: e.g Blitz02 (198)
2) Once you click on the tab, look for a stable build with the following structure: blitz02_<instance>_servers
**NOTE: Beside each instance there is a Success circle icon and a weather indicator. We are looking for a 
Blue
 success icon and 
Sunny
 weather icon
3) Now that you found a stable Blitz environment, click on the link and you should see the VIP link, for example if Blitz02 NA7:
          "VIP: https://na7-blitz02.soma.salesforce.com"
4) Click on the above link and depending on the instance, use the login information from here:  
https://sites.google.com/a/salesforce.com/newbt/new-bt-users-for-blitz
5) Go to Black Tab and click on Signup link and create a new Blitz Org for your use.
 
After you have signed up for an org and logged in, you might need to go to 
https://<instance>/qa/.hosemyorgpleasesir.jsp
.
You can use this link to assign your org license like console or even setup perms like normal Black Tab.
