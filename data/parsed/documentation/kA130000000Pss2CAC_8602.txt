### Topic: Designing a Custom SoftPhone Layout | Salesforce
Designing a Custom SoftPhone Layout | Salesforce
Designing a Custom SoftPhone Layout
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view, create, edit, or delete a SoftPhone layout:
“Manage Call Centers”
In a 
SoftPhone layout
 you can control the call-related fields that are displayed and the Salesforce objects that are searched for an incoming call. To design a custom SoftPhone layout:
From Setup, enter 
SoftPhone Layouts
 in the 
Quick Find
 box, then select 
SoftPhone Layouts
.
Click 
New
 to create a new SoftPhone layout definition, or click 
Edit
 next to the name of an existing layout definition to view or modify it.
In the 
Name
 field, enter a label that uniquely identifies your SoftPhone layout definition.
In the 
Select Call Type
 picklist, choose the type of call for which the currently displayed SoftPhone layout should be used. Every SoftPhone layout definition allows you to specify different layouts for inbound, outbound, and internal calls. These three layouts are grouped together in a single SoftPhone layout definition.
In the 
Display these call-related fields
 section, click 
Edit
 to add, remove, or change the order of fields in the currently-displayed SoftPhone layout:
To add a field to the SoftPhone layout, select it in the Available list and click 
Add
.
To remove a field from the SoftPhone layout, select it in the Selections list and click 
Remove
.
To change the order of a field in the SoftPhone layout, select it in the Selections list and click 
Up
 or 
Down
.
Any changes that you make are automatically updated in the SoftPhone layout preview image on the right side of the page. To hide the Available and Selections lists, click 
Collapse
.
Phone-related fields only appear in a user's SoftPhone if a valid value for that field is available. For example, if you add a Caller ID field to the layout of an outbound call, Caller ID will not appear.
In the 
Display these Salesforce Objects
 section, click 
Add/Remove Objects
 to add, remove, or change the order of links to call-related objects.
Below the list of selected objects, click 
Edit
 next to each 
If single 
<Object>
 found, display
 row to specify the fields that should be displayed in the SoftPhone layout if a single record for that object is the only record found. You can add, remove, or change the order of fields.
In the 
Screen Pop Settings
 section (for inbound call types), click 
Edit
 next to each type of record-matching row to specify which screens should display when the details of an inbound call match or don't match existing record(s) in Salesforce. The following table describes each record-matching row and its screen pop options:
Record-Matching Row
Description
Screen Pop Options
Screen pops open within
Use to set where screen pops display.
Existing browser window
Select to display in open browser windows.
New browser window or tab
Select to display in new browser windows or tabs.
Users' browsers may handle these settings differently:
Internet Explorer 6.0 always displays screen pops in new windows.
Internet Explorer 7.0 displays screen pops based on what users select in its tabs settings.
Firefox 3.5 displays screen pops based on what users select in its tabs settings.
No matching records
Use to set the screen pop options for when the details of an inbound call don't match any existing Salesforce records.
Don't pop any screen
Select if you don't want any screen to display.
Pop to new
Select to display a new record page you specify from the drop-down list.
Pop to Visualforce page
Select to display a specific Visualforce page.
The CTI adapter passes data from the call to the Visualforce page via a URL. This includes at least 
ANI
 (the caller ID) and 
DNIS
 (the number that the caller dialed). The URL can pass additional data to the Visualforce page if necessary.
Single-matching record
Use to set the screen pop options for when the details of an inbound call match one existing Salesforce record.
Don't pop any screen
Select if you don't want any screen to display.
Pop detail page
Select to display the matching record's detail page.
Pop to Visualforce page
Select to display a specific Visualforce page.
The CTI adapter passes data from the call to the Visualforce page via a URL. This includes at least 
ANI
 (the caller ID) and 
DNIS
 (the number that the caller dialed). The URL can pass additional data to the Visualforce page if necessary.
Multiple-matching records
Use to set the screen pop options for when the details of an inbound call match more than one existing Salesforce record.
Don't pop any screen
Select if you don't want any screen to display.
Pop to search page
Select to display a search page.
Pop to Visualforce page
Select to display a specific Visualforce page.
The CTI adapter passes data from the call to the Visualforce page via a URL. This includes at least 
ANI
 (the caller ID) and 
DNIS
 (the number that the caller dialed). The URL can pass additional data to the Visualforce page if necessary.
To hide expanded record-matching rows, click 
Collapse
.
This section only displays if your CTI adapter was built using the 
CTI Developer's Toolkit 2.0 or higher
.
Configure SoftPhone layouts for any remaining call types in the 
Select Call Type
 picklist.
Click 
Save
.
Note
Some Salesforce CRM Call Center features that are described in this help system might not be available with your SoftPhone because of customizations that have been made for your organization or the 
CTI Toolkit
 with which your SoftPhone was built. See your administrator for details.
See Also:
Setting Up Salesforce CRM Call Center
Assigning a SoftPhone Layout to a User Profile
Administrator tip sheet: Getting Started with Setting Up Call Centers
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cti_admin_phonelayoutscreate.htm&language=en_US
Release
202.14
