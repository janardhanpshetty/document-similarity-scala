### Topic: Console Component Implementation Tips | Salesforce
Console Component Implementation Tips | Salesforce
Console Component Implementation Tips
Before you create a component for a Salesforce console, review these tips.
Salesforce console available in Salesforce Classic and App Launcher in Lightning Experience. Setup for Salesforce console available in Salesforce Classic.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Behavior and Access Tips
You can’t add canvas app components to AppExchange packages.
When you update records on primary tabs or subtabs, components don’t refresh automatically.
If you change the 
Height
 or 
Width
 of a console component, refresh your browser to see your updates.
Report chart components aren’t available to developers using the Salesforce Metadata API or for migration between sandbox and production instances.
Page Layout Tips
You can add components to page layouts if you’re assigned to a Sales Cloud User Permission or Service Cloud User feature license.
After you turn off the highlights panel or interaction log on a page layout, you can add a component to the Top Sidebar or Bottom Sidebar of primary tabs.
If you want a component to display across all the subtabs on a primary tab, add the component to the Primary Tab Components section on page layouts. If you add a component to the Subtab Components section on page layouts, the component displays on individual subtabs.
If 
Salesforce Knowledge is enabled
 and the 
Knowledge sidebar is turned on
, don't add a component to the Right Sidebar of case page layouts, otherwise the Knowledge sidebar displays over components.
Visualforce Tips
For each page layout, you can use a Visualforce page as a component once.
If Visualforce pages are assigned to page layouts as components, you can’t delete them.
Visualforce pages don’t refresh when you click 
 and select 
Refresh all primary tabs
 or 
Refresh all subtabs
.
If you enable clickjack protection for Visualforce pages in your organization’s security settings, Visualforce pages won’t display correctly.
Unlike other Visualforce pages, you don't have to set the standard controller on components to the object whose page layout you're customizing.
If you add 
showHeader=true
 to a Visualforce page, you can add a 15-pixel gap to the right and left sides of a component to visually indicate its location in a sidebar. For example, 
<apex:page standardController="Contact" showHeader="false" title="List of Cases">
.
When Visualforce pages are used as components, two URL parameter values are passed automatically:
Parameter Name
Parameter Value
Description
id
A case-sensitive 15-character alphanumeric string that uniquely identifies a record.
The ID of the detail page in which the component displays.
This parameter is only passed if the ID is available.
inContextPane
true
Indicates the Visualforce page displays as a component in a Salesforce console.
See Also:
Console Components
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=console2_components_impl_tips.htm&language=en_US
Release
202.14
