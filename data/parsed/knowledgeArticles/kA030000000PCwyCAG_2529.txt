### Topic: Salesforce sync folder options may be grayed out because of the following reasons. Folders in the wizard are greyed out
ISSUE
When a user installs Salesforce For Outlook (SFO - any version) and starts the wizard by entering their username and password, on the next screen on the Salesforce or Outlook Settings window, where wizard prompts to select a folder in Outlook, the Contacts, Calendar and Task folders are grayed, despite their Outlook being open 
 
 
 
Please pay attention to the folder icon that is grayed out or missing since this could be related to the 
following article.
 
 
CAUSE
This issue can be caused by one or all of the following
 
- Microsoft Outlook is not running
- Microsoft Outlook or SFO are running as Administrator
- Missing Outlook data file (.OST) or corrupted data file
- Duplicated Redemption keys
- The Redemption keys in the registry are corrupted
- Duplicated Office Library files that point to different version of Outlook
- If user is on Windows 7, the User Account Control is set to high (see KB 000019209 )
- Different versions of Office are installed and the older version was installed or changed recently
.
 
 
Example 1
, User has 
Outlook 2010
 and 
Office 2007 
and 
Office 2007 
was updated/changed/installed recently after 
Outlook 2010
 was installed
 
Example 2:
 Office Business Edition (including 
Outlook 2007 or 2010
) and Office Home Edition ( Home Edition was updated/changed/installed after Office Business Edition)
 
In a 
Windows Vista, 7 and Windows 8
 operating systems you can check the date on the Programs and Features windows ( See a sample screen shot below)
 
 
 
Resolution
1- Microsoft Outlook is not running
Simply Open Outlook or if it is open, close it and wait a few seconds then reopen it
 
2- Microsoft Outlook or SFO are running as Administrator
Exit SFO and Outlook first, then right click on SFO and click on 
Properties
 and click on 
Compatibility 
tab and make sure none of the boxes are checked.
Do the same thing for Outlook, again make sure Outlook is closed and you need to hold down the 
SHIFT 
key on your keyboard and right click on the Outlook icon on the Desktop or 
Taskbar 
and click on 
Properties
 and click on 
Compatibility 
tab and make sure none of the boxes are checked.
 
 
3- Missing Outlook data file (.ost) or corrupted data file
Make sure Outlook is running in Exchange cached mode and if it is already running in the cache mode and there is a data file (.ost) file on the machine, you may need to recreate it.
See 
Article Number     000188551
Sometimes a duplicate or corrupted Outlook profile also can cause this issue. SFO gets confused and is unable to connect to the correct profile
 
4- Corrupted or problematic Outlook profile
You don't need to make any changes to the existing Outlook profile. To test and make sure the problem is not related to Outlook profile, you can create a new no email profile in Outlook and try to run the wizard and see if the folders will display correctly
To do this, follow these steps
 
Close 
Outlook 
and then from 
Control Panel
 double click on the 
Mail icon
 ( you can also search for it using the 
Search 
box on top corner)
On the 
Mail Setup
 pop up box, click on 
Show Profiles...
Make sure the 
When starting Microsoft office Outlook, use this profile:
  radio button is set to 
Prompt for a profile to be used
 Click on 
Add..
. and type in a name such as Test-profile in the 
New Profile 
window and click on 
OK
The 
Add New E-mail Account wizard will start automatically
, you can click on 
Cancel
You will see the pop up box below
Click 
OK
, click on 
OK
 again to close the Mail window
 
 
 
 
Open Outlook and select the new profile and then run SFO and retry the wizard and see if the folders are still grayed out. If they are not and you are able to see and select a folder, then the user's existing Outlook profile is corrupted and you should have the user or their IT team recreate it for them
 
Important: 
You can also reach out to your (customer's) IT team to create a new Outlook profile 
 
5- Duplicated Office Library files that point to different version of Outlook
You need to check the registry for any duplicated key.
To open the 
Registry Editor 
press the 
Win(key) +
 R 
key combination on your keyboard (see image below)  and type 
REGEDIT 
and press Enter
Win key on your keyboard
 
 
In the Registry Editor navigate to
HKEY_CLASSES_ROOT\TypeLib\{00062FFF-0000-0000-C000-000000000046}
Under this key you can should only have one key since each key references to version of MS Outlook that is installed on your computer. Keys for corresponding version of Microsoft Outlook installed on your computer
 
9.2       for       Office 2003
9.3       for       Office 2007
9.4       for       Office 2010
9.5       for       Office 2013
9.6       for       Office 2016


 
 
For example, if you had Office 2007 on your computer and then upgraded or installed Outlook (or Office) 2010 a second key will get created here. Then, you change your mind and you uninstall the Office 2010 version and continued using the Outlook 2007. Sometimes the uninstaller does not remove the corresponding key. So you would end up with 2 keys one for Outlook 2007 and another for 2010. You would need to keep the 9.3 key and remove the 9.4 key.
 
If there is only one entry here,  skip to the next step. ** DO NOT close the Registry Editor yet
 
Example: User has only Outlook 2007 installed hence the 9.3 key
 
 
Example: Incorrect entry, user has Outlook 2010 but there are two entries. You need to right click on the 9.3 key (that refers to Outlook 2007) and click on Delete
 
 
6- The Redemption keys in the registry are corrupted
Next delete the following key(s) from the registry as well and then close the Registry (There might be more than 1 Redemption entry, delete them all) No need to save them
 
HKEY_CURRENT_USER\Software\Redemption
HKEY_CURRENT_USER\Software\Redemption *
HKEY_CURRENT_USER\Software\Redemption * *
HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Redemption
 
 
7- Missing or duplicated Redemption keys
When you install SFO, a redemption key that indicates the version of the Redemption is inserted in the registry.
A missing or duplicated version could cause a problem, a missing key indicates a failed installation you may need to register the sfdcrdm.dll or sfdcrdm64.dll. Refer to 
Article Number     000004339
Navigate to 
HKEY_CLASSES_ROOT\TypeLib\{2D5E2D34-BED5-4B9F-9793-A31E26E6806E}
and make sure you have only 1 key starting with 5.x
Example 1 : This is what you should see. 5.8 is the SFO 
v2.7 and later, you may have 5.10 and 5.a
 
Example 2 : This example shows you a duplicate entry. You have 2 entries starting with 5. 
One is 5.2 and another 5.8
You need to right click on the 5.2 or any other key that you may see starting with 5 and click on 
Export
, to save it on your computer then right click on it again and click on 
Delete
 
Again, the 5.8 key is the key you want to keep You can leave the 4.5 key alone.
If the 5.x key is missing it is possible that the installation has failed. You can either try to manually register the Sfdcrdm.dll file or reinstall the SFO application
How to register the sfdcrdm.dll file
 
 
 
 
8- If user is on Windows 7, the User Account Control is set to high (
see KB 000019209
 )
If STEP 8  
(Setting the Windows 7 UAC (User Account Control) to Never Notify (KB 000019209) ) 
did not fix the issue skip to STEP 9
 
9- Different versions of Office are installed and the older version was installed or changed recently.
A-
 Make sure Outlook is closed
 
B-
 Uninstall 
Salesforce for Outlook
 as well as the following applications
 
       - 
Visual Studio® 2010 Tools for Office Runtime (x64)
 (
VSTO
)
 
If you have 
Outlook 2010
:
      - Outlook 2010 users: Uninstall the ‘
Office 2010 Primary Interop Assemblies
’ 
 
If you have 
Outlook 2007
:
      - Outlook 2007 users: Uninstall the ‘
Office 2007 Primary Interop Assemblies
’
 
For Outlook 2013 and later, PIA is included as part of Office installation
 as part of .NET Programmability option
Refer to the 
Article Number 000193384
 
for SFO installation if user has Office 2013
Windows 7
/8/10
:   Control Panel\All Control Panel Items\Programs and Features
 
*NOTE
These may be slightly different depending on the version of your Windows and Outlook
 
C-
 Run a repair on Office installation. Make sure you repair the version that includes your Microsoft Outlook. From 
Control Panel
, list of programs select the version of Office you would like to repair and click on ‘
Change
’ and make sure ‘
Repair
’ is selected and finish the process
**NOTE
If you have 
Office 2013 Click to Run
 or 
Office 365,
 you must perform an 
Online Repair
 - Do not do a 
Quick 
Repair
 
D-
 Wait till repair process is done and then reboot your computer. Do not skip this step please
E-
 After you restart, navigate to
 
          For Windows 7, 8, 8.1 and 10 users:   
%AppData%\salesforce.com\Salesforce for Outlook\
 and delete everything
 
 
10- Refer to 
Article 
0
00167727
 to install SFO
Now try to reproduce the issue
 
 
 
