### Topic: What’s New in Salesforce? | Salesforce
What’s New in Salesforce? | Salesforce
What’s New in Salesforce?
Current Release
Learn about the newest features for sales, support, marketing, and Chatter users, as well as enhancements to the platform. 
You can also visit the 
Summer ’16 community page
.
Our release notes include complete details about new features, as well as implementation tips and best practices.
Summer ’16 Release Notes
Salesforce for Outlook Release Notes
Force.com Connect for Office Release Notes
Force.com Connect Offline Release Notes
Past Releases
Our archive of release notes includes details about features we introduced in previous releases.
Spring ’16 Release Notes
Winter ’16 Release Notes
Summer ’15 Release Notes
Spring ’15 Release Notes
Winter ’15 Release Notes
Summer ’14 Release Notes
Spring ’14 Release Notes
Winter ’14 Release Notes
Summer ’13 Release Notes
Spring ’13 Release Notes
Winter ’13 Release Notes
Summer ’12 Release Notes
Spring ’12 Release Notes
Winter ’12 Release Notes
Summer ’11 Release Notes
Spring ’11 Release Notes
Winter ’11 Release Notes
Summer ’10 Release Notes
Spring ’10 Release Notes
Winter ’10 Release Notes
Summer ’09 Release Notes
Spring ’09 Release Notes
Winter ’09 Release Notes
Summer ’08 Release Notes
Spring ’08 Release Notes
Winter ’08 Release Notes
Summer ’07 Release Notes
Spring ’07 Release Notes
Force.com Mobile 7.0 for BlackBerry Release Notes
Force.com Mobile 6.1 for Windows Mobile 5 Release Notes
Winter ’07 Release Notes
Summer ’06 Release Notes
Winter ’06 Release Notes
Force.com Mobile 6.0 Release Notes
Summer ’05 Release Notes
Winter ’05 Release Notes
Summer ’04 Release Notes
Spring ’04 Release Notes
Winter ’04 Release Notes
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=whats_new.htm&language=en_US
Release
202.14
