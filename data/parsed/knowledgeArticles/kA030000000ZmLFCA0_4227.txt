### Topic: When the report had Hierarchy Level Saved before, then removing it and trying to put it back, will result in Save Hierarchy Level being greyed out
When the report had Hierarchy Level Saved before, then removing it and trying to put it back, will result in Save Hierarchy Level being greyed out.
Resolution
If you are using custom report type the option to "Save Hierarchy Level" will be set by default and it cannot be changed. 
The hierarchy level can only be set when using standard report types. 
This is working as designed. 
 
If you are using 
Standard Report Type
, in order for you to uncheck Save Hierarchy Level you need to open the report, run it, then change the role hierarchy and click on save. Notice that the save hierarchy level check-box can now be checked or unchecked.
Note
For Custom Report Types, running the report and clicking Save or Save As can provide the option to un-tick the "Save Hierarchy Level" check-box if it has been previously saved with hierarchy level. 
However the behavior will not be the same for Standard Report types wherein the records that are visible regardless of role hierarchy will display in the results, instead the results will drill down automatically to the role of the logged in user who is running the report. 
For both Standard and Custom Report Types, selecting a role and saving the hierarchy level will drill down the results by default to the role that was selected, regardless of role of the user running the report.
