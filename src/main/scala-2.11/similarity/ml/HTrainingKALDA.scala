package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.clustering.LDA
import org.apache.spark.ml.feature.{CountVectorizer, RegexTokenizer, StopWordsRemover}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
 * Created by jshetty on 9/13/16.
 */
object HTrainingKALDA {

    def main(args: Array[String]): Unit = {

      val spark = SparkSession.builder.master("local[8]").appName("Document Similarity for Knowledge articles").getOrCreate()
      val sc = spark.sparkContext
      val sqlContext = spark.sqlContext
      import sqlContext.implicits._
      sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

      // Read Input
      val knowledgeArticlesPath = "/Users/jshetty/spark-applications/documentsimilarity/data/parsed/knowledgeArticles/*"
      val knowledgeArticlesInput = sc.wholeTextFiles(knowledgeArticlesPath)

      // Preprocessing
      // Get the custom stopwords files
      val stopWordsPath = "/Users/jshetty/spark-applications/documentsimilarity/input/stopwords/english_custom_stopwords.txt"
      val stopwords = sc.textFile(stopWordsPath).collect()
      // Trim file names for readability and cache
      val knowledgeArticles = knowledgeArticlesInput.map(line => (line._1.split("/").last, line._2)).toDF("ArticleName", "ArticleContent").cache()

      // Setup Documents pipeline
      val numTopics = 10
      val numIterations = 100
      val vocabSize = 10000
      val minDocs = 3
      val maxTermsPerTopic = 20
      val documentTokenizer = new RegexTokenizer().setInputCol("ArticleContent").setOutputCol("articleWords").setPattern("\\W")
      val docTokFiltered = new StopWordsRemover().setStopWords(stopwords).setInputCol("articleWords").setOutputCol("articleWordsFiltered")
      val docPipeline = new Pipeline().setStages(Array(documentTokenizer, docTokFiltered))
      val docDF = docPipeline.fit(knowledgeArticles).transform(knowledgeArticles)
      val docVectorizer = new CountVectorizer().setInputCol("articleWordsFiltered").setOutputCol("features").setVocabSize(vocabSize).setMinDF(minDocs).fit(docDF)
      val vocabList = docVectorizer.vocabulary
      val docVecDF = docVectorizer.transform(docDF)

      // LDA model
      val docLda = new LDA().setK(numTopics).setMaxIter(numIterations)
      val docLDAModel = docLda.fit(docVecDF)

      val docTransformed = docLDAModel.transform(docVecDF)

      // Review results of Topic modeling
      println(s"$numTopics numTopics:")
      val topicIndices = docLDAModel.describeTopics(maxTermsPerTopic = maxTermsPerTopic)
      val getVocabWord = udf { (indexList: Seq[Int]) => indexList.map(vocabList(_)).toSeq}
      val topicWords = topicIndices.withColumn("termWords", getVocabWord(topicIndices("termIndices")))

      // Display
      topicWords.show()

      topicWords.write.csv("/Users/jshetty/documentsimilarity/knowledgeArticlesTopics")

      spark.stop()
    }



}
