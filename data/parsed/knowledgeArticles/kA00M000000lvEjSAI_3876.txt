### Topic: Users may notice the Flags an item as Inappropriate setting is missing from most Email Settings pages as noted in the Tip in the documentation
When setting up a user to Moderate chatter posts in your community, you may wish to allow the user to receive an email notification whenever someone Flags an Item as Inappropriate.
The following article indicates that there is a setting called "Flags an item as inappropriate" found in your Email Settings page:
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_moderator_manage_items.htm&language=en_US
This setting appears to be missing from most email setting pages such as:
Setup>Customize>Chatter>Email Settings
Setup>Administer>Email Administration
[Name]>My Settings>Email>My Email Settings
[Name]>My Settings>Chatter>Email Notifications
Resolution
This checkbox is located on the user's email settings from 
within
 the community. In order to see it, take the following actions:
1) Log into the community
2) Navigate to the affected user's Chatter Profile within the community
3) Click on the small "down arrow" located to the far right on the chatter profile page
4) Click My Settings
5) Click Email Settings
 
