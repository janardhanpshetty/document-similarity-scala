### Topic: Workflow Rule with a Workflow Task action does not create a Task when triggered.
Scenario:
A workflow has been created with Workflow Task.
That Workflow Task is defined to assign to a Record Owner
When the Workflow Rule is triggered on some records, no Task is created.
 
Resolution
This is an expected behavior if the Task would be assigned to an Inactive User
For example, if the Workflow Task is defined to assign to "Record Owner", and the Owner of the record which triggers the rule is not an active user, the Task will not be created.
Solution: If there are Workflow Tasks that will assign to Record Owners, ensure all records in the Object related to that workflow are owned by active users.
