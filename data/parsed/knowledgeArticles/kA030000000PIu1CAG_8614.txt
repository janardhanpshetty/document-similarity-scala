### Topic: SOAP UI is a tool which is used to test requests and responses to a web service
- SOAP UI is a tool which is used to test the requests and response to a web service in a standalone fashion
SOAP UI can be downloaded from 
http://www.soapui.org/Downloads/latest-release.html
NOTE: SOAP-UI is a non Salesforce application. Salesforce does not own any responsibility for any changes made to the application
.
- How to Debug Web service callouts using SOAP UI?
Resolution
- If we are making web service callouts to or from salesforce and having issues, then please perform the following tests
- If the scenario is 
external system making calls to salesforce
:
- Please download the Salesforce WSDL file and make the call through SOAP UI and see if it successful, if so then it might be some issue with external system else the issue might be within salesforce
- If the scenario is 
salesforce making external web service callouts
- Please download the WSDL of the external system, make the call with all the required parameters and see if you get a valid response, if so then it might some issue with salesforce making callout , else the problem is with a external system
 
