package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{Word2Vec, MinHashLSH, NGram, RegexTokenizer}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, udf}

import scala.collection.immutable.HashSet

/**
 * Created by Janardhan Shetty on 12/19/16
 * spark-submit --class similarity.ml.CaseSimilarity --master local[12] --driver-memory 8G /Users/jshetty/spark-applications/documentsimilarity/target/scala-2.11/documentsimilarity_2.11-1.0.jar
 */

 object CaseSimilarityLSH {

  val getConcatenated = udf((first: String, second: String) => { first + " " + second })

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder.master("local[12]").appName("Case Similarity").getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext
    sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val numGrams = 4
    val seed = 32
    val minThreshold = 87.0
    val maxThreshold = 100.0

    // Read as RDD
    //val casePath = "/Users/jshetty/case-similarity/data/3months/support/support.csv"
    val casePath = "/Users/jshetty/case-similarity/data/3months/support/support_201609.csv"
    val resultPath = "/Users/jshetty/case-similarity/results/res1"
    val caseInput = spark.read.option("header", "True").option("delimiter", "^").csv(casePath)

    // Concat subject + description
    val cases = caseInput.withColumn("caseText", getConcatenated(col("SUBJECT"), col("DESCRIPTION")))

    // Tokenize
    val caseTokenizer = new RegexTokenizer().setInputCol("caseText").setOutputCol("caseWords").setPattern("\\W")

    // Approach-1 : 4-gram + MurmurHashing + Jaccard Similarity
    val caseTokGram = new NGram().setInputCol("caseWords").setOutputCol("caseGrams").setN(numGrams)

    // Hashing
    val word2Vec = new Word2Vec().setInputCol("caseGrams").setOutputCol("caseGramsVec").setVectorSize(5).setMinCount(0)

    // Pipeline
    val casePipeline = new Pipeline().setStages(Array(caseTokenizer, caseTokGram, word2Vec))

    // Fit
    val wordRec = casePipeline.fit(cases).transform(cases)

    val reqCases = wordRec.select("ID", "caseGramsVec")

    // LSH
    val caseLSH = new MinHashLSH().setNumHashTables(65393).setInputCol("caseGramsVec").setOutputCol("caseLSHHashed").setSeed(seed)

    val model = caseLSH.fit(reqCases)

    // Self Join
    model.approxSimilarityJoin(reqCases, reqCases, 0.6).show()

    val key =

    // Approximate nearest neighbor search
    model.approxNearestNeighbors(reqCases, key, 2).show()

    // Cross product
    val caseJoined = caseResult.crossJoin(caseResult)
    // columns renamed:
    val columnsRenamed = Seq("CaseID1", "caseNumber1Hashed", "CaseID2", "caseNumber2Hashed")
    val resultDF = caseJoined.toDF(columnsRenamed: _*)

    // Do from here not complete
    val jaccardDF = resultDF.withColumn("score",jaccard(col("caseNumber1Hashed"), col("caseNumber2Hashed")))

    //jaccardDF.show(5)
    //val result = jaccardDF.select("CaseNumber1", "CaseNumber2", "score").filter(s"score > $threshhold").sort($"score".desc).coalesce(1)
    val result1 = jaccardDF.select("CaseID1", "CaseID2", "score").filter(s"score > $minThreshold")
    val result2 = result1.select("CaseID1", "CaseID2", "score").filter(s"score < $maxThreshold")
    result2.limit(100).write.csv(resultPath)
    sc.stop()

  }
}
