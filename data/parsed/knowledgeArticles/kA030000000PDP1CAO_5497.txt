### Topic: Why do I recieve a 'Transfer Requires Read' error when transferring an account that I own to another user?
Why do I receive a 'Transfer Requires Read' error when transferring an account to another user?
Resolution
This error may show up when an Account record you own is transferred to another User. This may appear even if the User that is being specified as the new owner has read access to Accounts, Contacts or Opportunities.
Reason is that when an Account is transferred 
Contacts
, 
Contracts in “Draft” status
, 
Attachments
, 
Notes
, and 
open Activities
 are also automatically transferred to the new owner.
If the new owner's Profile does not have access to one of the above objects then you will get an error when attempting the transfer.
To resolve this you would either need to remove the affected record (e.g. Draft Contract, open Activity...) from the Account or make sure the status changes (e.g. activate the Contract, close the Activity...). By making those changes the records will remain with its existing owner, thus allowing the transfer to go ahead.
Note
: 
This error is very common if the ownership of the Account is being changed to a Chatter Only User because they do not have access to Opportunity and if that Account has associated Opportunity
 record it will not allow you to change the ownership of that Account with same error message.
Similarly, if a Chatter Only User or Force.com App Subscription User tries to change ownership of an Account which has open Opportunities, they will get the same error message since Chatter Only users does not have access to Opportunity Object.
 
