### Topic: You cannot make the solution checkbox default on, or required via setup and you cannot affect it via workflow.
Can I make the solution checkbox default on/checked by default? Where does Close Case Body information go when Submit to Public Solutions is not checked? Can the check box change to TRUE when someone types in the BODY like it does when someone types in the "Solution Title"? Can we set the default for the Submit to Public Knowledge check box to TRUE?
Resolution
You can close a case from several locations and, depending on the settings for your organization, create a solution or article while closing the case.
If your organization uses solutions, you can fill in the 
Solution Details
 for the case. Remove the check from 
Submit to public solutions
 if you do not want to submit the solution for review by your solution managers. Leaving this field checked automatically links the case to the solution.
You cannot make the solution checkbox default on, or required via setup and you cannot affect it via workflow. There is a warning message that pops up when the checkbox is unchecked that essentially says if this checkbox is not checked your data will be lost. As a workaround you can affect the solutions checkbox via API or Trigger (visualforce) and/or post an idea on the idea exchange regarding this issue.
If you want to remove ¨Solution Information¨ section:
Steps : Go to set up | App Setup(original interface)/Build(improved interface) | Customize | Cases | Page layouts | Case close page layouts | Edit |
Click on Top right hand side, where it says layout properties | In that box you will see Solution information section | Uncheck Show section on edit page | OK
Remember : this will remove the whole Solutions section in closed cases
