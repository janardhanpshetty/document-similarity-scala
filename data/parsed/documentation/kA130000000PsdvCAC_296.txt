### Topic: Custom Object Record Overview | Salesforce
Custom Object Record Overview | Salesforce
Custom Object Record Overview
Important
You might be viewing this page because your Salesforce administrator didn’t create custom help. If you need information on a specific custom object, contact your administrator about 
creating custom help
 for your custom objects. The Salesforce Help covers only the standard objects provided with the initial Salesforce integration.
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Custom objects records store information that’s unique and important to you and your organization. For example, your organization may use a custom object called “Merchandise” to store data about your company's merchandise. You can also use custom objects for custom applications, such as tracking software enhancements in a development life-cycle.
Your administrator first defines the custom object and its properties, such as custom fields, relationships to other types of data, page layouts, and a custom user interface tab. Once the custom object is created and deployed to users, you can enter data to create individual custom object records. If your administrator has created a tab for the custom object, the tab displays a home page that lets you quickly create and locate custom object records. You can also sort and filter your custom object records using standard and custom list views. In addition, the tab lets you view and edit detailed information on each custom object record to which you have access.
Administrators, and users with the “Modify All Data” permission, can import custom objects.
See Also:
Custom Objects Home
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=co_def.htm&language=en_US
Release
202.14
