### Topic: This is a broad outline of MobilePush workflows.
This is an outline of how the MobilePush application and a client's app integrate. 
Link to 
base
.
Resolution
MobilePush Workflows for Support
MobilePush Message Workflow
User creates message in MobilePush app inside the Salesforce Marketing Cloud.
The Salesforce Marketing Cloud
 sends message to appropriate platform (Apple or Android).
Message received on device.
Message delivered to specific mobile application.
MobilePush Consumer Workflow
Consumer downloads the app.
Consumer opts-in to push notifications via mobile app.
Consumer receives message/notification.
Consumer taken to app for one or more actions:
More information
Make purcahse
Watch video
Other actions
MobilePush Setup - Getting Started
Create a push-enabled app.
Register app with Apple and Android
.
Customer uploads any applicable GMC keys and iOS certifications.
Client receives next-steps 
email message
 from Jen Mavis and the Salesforce Marketing Cloud.
Client registers 
Code@
 account and requests App Center access.
App Center automatically approves client access.
Client creates a Fuel app for the push-enabled app.
The Salesforce Marketing Cloud automatically creates an account for the client:
App Key
Secret
Master Secret
Code@ generates an app ID and Access Token (used to integrate a MobilePush SDK).
Download the Salesforce Marketing Cloud MobilePush SDK from GitHub.
Android
iOS
Update mobile app using the Salesforce Marketing Cloud Journey Builder for Apps SDK.
Submit app updates to Apple (process should only take a few minutes)
Import current push subscriber data to Contacts (if applicable):
The Salesforce Marketing Cloud automatically captures Contact data when clients update their app to the version using the MobilePush SDK.
Clients can also export existing client records from their current provider and import that data into Contacts. This process assumes the current provider captures the device token from the platform and does not generate a unique ID.
Client sends push notification to Contacts.
MobilePush API Workflow
Key MobilePush Metrics
