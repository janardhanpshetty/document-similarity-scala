### Topic: This article helps how to trigger the email alert on the object with a Lookup Relationship.
Email Action
 will work on the objects with a 
Master Relationship
 however there is the workaround to trigger the
 Email Alert
 for a 
Lookup Relationship.
Resolution
To trigger the 
Email Alert
 for a 
Lookup Relationship
, Please follow the below steps:
 
1. Create a 
Custom
 field on the Child object (Data Type - 
Email
)
2. Update the same custom field on the child object with the help of a Workflow Rule.
         a) Evaluation Criteria: 
When record is created or edited
.
         b) Rule Criteria: 
TRUE
         c) Create a
 New Field Update
 action.
         d) Update the 
Custom Email 
field with the
 Email 
of the User's (in the Lookup)
         e) 
Activate
 Workflow Rule
 
3. Create Second Workflow Rule for
 Email Alert
 to be triggered on Child object:
        a) Evaluation Criteria: 
When record is Created or Edited.
        b) Rule Criteria: 
TRUE
        c) Now select
 New Email Alert
 under 
Immediate Workflow Actions
.
        d) Give a 
 and 
Unique Name
.
        e) Select 
Email Template
.
        f) Select Recipient Type as 
EMAIL FIELD
.
        g) Now you should be able to see the email field which you created earlier. Select this under 
Selected Recipients
.
        h) 
Save
 and
 Activate
 workflow.
 
The email Alert will now be triggered for a 
Lookup Relationship 
on the objects.
 
