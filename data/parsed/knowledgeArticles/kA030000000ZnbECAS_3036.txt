### Topic: Automation Studio returns an error indicating "403 Not Authorized" when you're creating an Automation.
You'll receive a "403 Not Authorized" error when you build an Automation within Automation Studio if you don't have sufficient permissions to create one. The error states: 
"403 Not Authorized 
You don't have the correct roles or permissions enabled to access this information. Contact your administrator to acquire the correct permissions. If the problem persists, contact Global Support."
 
Only Users with the 
Marketing Cloud Channel Manager
 or 
Marketing Cloud Administrator
 User Role have the correct permission set to create an Automation. If you're an Administrator and another User is receiving the error when creating an Automation, verify their permissions and update them as appropriate.
Resolution
 
Verify User Role and Permissions
If a Role doesn't have "Allow" set for a permission or if two Roles have conflicting values for a particular permission ("Allow" and "Deny"), access to that permission is denied. As an Account Administrator, follow these steps to verify the User permissions are correct:
 
1. In the Salesforce Marketing Cloud, hover over your name in the top-right corner.
2. Click 
Administration.
3. Hover over 
Account
 | 
Users.
4. Type the User's name in the search box.
5. Click 
Search.
6. Select the checkbox next to the User's name.
7. Click 
Manage Roles.
8. Click 
Edit Permissions.
9. Next to "Automation Studio," click the 
+
 to expand the permissions.
10. Next to "General, Automation, and Migration Utility," click the 
+
 to expand the permissions.
The grid to the right shows all of the permissions assigned to the User for each Role.  If any Role has a value of "Deny" for a particular permission or if the Role selected for the User doesn't explicitly allow a permission, that permission is set to "Deny." 
 
Manage User Roles
As an Account Administrator, you can edit the Roles assigned to the User so they can create the Automation. Here's how: 
 
1. In the Salesforce Marketing Cloud, hover over your name in the top-right corner.
2. Click 
Administration.
3. Go to 
Account
 | 
Users.
4. Type the User's name in the search box.
5. Click 
Search.
6. Select the checkbox next to the User's name.
7. Click 
Edit Roles.
8. Enable Roles by selecting the checkbox or remove Roles by clearing the checkbox next to the Role name.
9. Click 
Save.
 
