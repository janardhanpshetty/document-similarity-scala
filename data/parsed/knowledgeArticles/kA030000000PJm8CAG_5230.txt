### Topic: This article provides links to the trust and compliance information for each Salesforce service. Each article is available on help.salesforce.com.
This article provides links to the trust and compliance information for each Salesforce service.  Each article is available on 
help.salesforce.com
.
Resolution
The following articles and their attachments are available on 
help.salesforce.com
. Within each section, articles noted with a '*' require login, contain confidential information of salesforce.com, inc. (along with their attachments), and are subject to the help.salesforce.com 
Confidentiality Notice and Confidentiality Terms
.
Salesforce Services
 
(services branded as Force.com, Site.com, Database.com, Sales Cloud, Service Cloud, Communities, and Chatter)
Analytics Cloud Services
 
(services branded as Analytics Cloud)
​​
Buddy Media Services
 
(services branded as Buddy Media)
Data.com Services
 
(services branded as Data.com)
Desk.com Services
 
(services branded as Desk.com)
ExactTarget Services
 
(services branded as ExactTarget, not including the services branded as Predictive Intelligence (formerly iGo Digital))
Financial Services Cloud
 
(services branded as Financial Services Cloud)
Health Cloud Services
 
(services branded as Health Cloud)
Heroku Services
 
(services branded as Heroku and the Heroku API)
​
IoT Cloud Services
 
(services branded as IoT Cloud)
Predictive Intelligence (formerly iGo) Services
 
(services branded as Predictive Intelligence (formerly iGoDigital))
Pardot Services
 
(services branded as Pardot)
Radian6 Services
 
(services branded as Radian6)
SalesforceIQ Services
 
(services branded as SalesforceIQ)
Social.com Services
 
(services branded as Social.com)
Social Studio Services
 
(services branded as Social Studio)
SteelBrick Services
 
(services branded as SteelBrick)
Work.com Services
 
(services branded as Work.com)
 
