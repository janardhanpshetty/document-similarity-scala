### Topic: Account History | Salesforce
Account History | Salesforce
Account History
Available in: Salesforce Classic
Business accounts available in: 
All
 Editions except 
Database.com
Person accounts available in 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view accounts:
“Read” on accounts
The Account History related list of an account detail page tracks the changes to the account. Any time a user modifies any of the standard or custom fields whose history is set to be tracked on the account, a new entry is added to the Account History related list. For person accounts, this includes any relevant contact fields that are set to be tracked. All entries include the date, time, nature of the change, and who made the change. Modifications to the related lists on the account are not tracked in the account history.
See Also:
Accounts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=account_history.htm&language=en_US
Release
202.14
