### Topic: Assign Home Tab Page Layouts to Profiles | Salesforce
Assign Home Tab Page Layouts to Profiles | Salesforce
Assign Home Tab Page Layouts to Profiles
Your home page layouts are only visible to users after you assign them to a user profile.
Available in: Salesforce Classic
Available in: 
All
 Editions
User Permissions Needed
To assign home page layouts:
“Customize Application”
From Setup, enter 
Home Page Layouts
 in the 
Quick Find
 box, then select 
Home Page Layouts
.
Click 
Page Layout Assignment
.
Click 
Edit Assignment
.
Choose the appropriate page layout for each profile.
Initially, all users, including Customer Portal users, are assigned to the Home Page Default layout.
Click 
Save
.
Tip
Users can customize the dashboard settings on their Home tab in their personal settings.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=assigning_home_page_layouts_to_profiles.htm&language=en_US
Release
202.14
