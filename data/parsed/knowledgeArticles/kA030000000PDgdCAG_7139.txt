### Topic: Why is the org limit for scheduled dashboard refreshes 200, when I can only schedule a maximum of 48 in any 24 hour period?
Why is the org limit for scheduled dashboard refreshes 200, when I can only schedule a maximum of 48 in any 24 hour period?
Resolution
This can be easily explained if we use meetings as a metaphor.
So then we can schedule 200 recurring meetings and we can have at most two meetings scheduled for any hour of and recurring day.
The recurrence can differ for each meeting, i.e meetings can re-occur every day, every week or every month. If we schedule all meetings to occur daily then the 2 per hour limit will constrain us, and it results in only being able to schedule 48 (2x24) in total. If we schedule them with a lower frequency, that gives us flexibility to schedule up to 200 in total.
Note: the limits are not per user, they are per org. Unlimited edition customers can schedule a max of two refreshes per hour per day, while Enterprise Edition customers can schedule one refresh an hour per day.
