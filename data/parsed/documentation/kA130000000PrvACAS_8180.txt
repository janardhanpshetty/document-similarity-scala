### Topic: Create Mail Merge Templates with Microsoft® Word | Salesforce
Create Mail Merge Templates with Microsoft® Word | Salesforce
Create Mail Merge Templates with Microsoft® Word
The Connect for Office Word add-in provides a complete list of valid merge fields for you to insert. When Salesforce disables TLS 1.0, we’re ending support for Connect for Office.
Important
When 
Salesforce disables TLS 1.0
, we’re ending support for Connect for Office. This change means that there’s no guarantee that Connect for Office can establish a connection between Salesforce and Microsoft® Word or Excel. Even if sales reps can establish a connection, Salesforce no longer provides support or maintenance when there’s an issue.
Available in: Salesforce Classic
Available in: 
All
 Editions
Important
Each mail merge field label you use must be unique.
Open Microsoft Word.
If you use
You’ll
Word 2003 or earlier
Select 
Log In
 from the 
Salesforce
 drop-down menu on the toolbar.
Word 2007
Open the Salesforce tab on the Ribbon, click the 
Merge Fields
 drop-drown menu, and then select 
Log In
.
Enter your Salesforce username and password.
Click 
Login
.
Note
When you are successfully logged in and have an active session, the 
Log In
 command in the drop-down button is disabled. To log out, click 
Log Out
.
Create your mail merge template from an existing or blank Word document.
Place your cursor where you want to insert a Salesforce merge field. To replace an existing merge field, select the entire merge field in your template. Mail merge fields must be unique.
In Word 2003 and earlier, select 
Insert Merge Field
 from the 
Salesforce
 drop-down menu. In Word 2007, click the 
Merge Fields
 drop-drown menu at the Salesforce tab on the Ribbon, and then select 
Insert Merge Field
.
In the Insert Merge Fields dialog, select a field type and then select the merge field you want to insert. If you use products in Salesforce, see 
Including Opportunity Product Data in Mail Merge Templates
.
Click 
Insert
 to add the merge field at your current cursor position in your Word document.
Insert additional merge fields as desired to build your mail merge template.
After your mail merge template is complete, upload it to Salesforce from Setup by entering 
Mail Merge Templates
 in the 
Quick Find
 box, then selecting 
Mail Merge Templates
.
Including Opportunity Product Data in Mail Merge Templates
Products are available in Professional. Enterprise, Unlimited, Performance, and Developer Editions. To add opportunity product data to your mail merge template, in the Insert Merge Fields dialog select Opportunity Line Item Fields and then select the fields to insert. This inserts a table with the selected merge fields and special “start” and “end” fields. (You can download sample 
default mail merge templates
 which include a table of opportunity product data.) You can modify this table as needed, but the 
Opportunity_LineItem_Start
 merge field must be before all opportunity product merge fields and the 
Opportunity_LineItem_End
 merge field must be after all opportunity product merge fields. These start and end fields trigger Salesforce to list all of the products on the opportunity.
If you also want to insert product merge fields into the table of opportunity product merge fields, you can do so manually without using the Salesforce Word add-in:
In your Word document, place your cursor where you want to insert the merge field.
In Word 2003 and earlier, select 
Insert
 and then 
Field
 from the Word menu bar. In Word 2007, select the Insert tab on the Ribbon, click 
Quick Parts
 in the Text group, and then click 
Field
.
Select 
Mail Merge
 in the Categories drop-down list.
Select 
MergeField
 in the Field names box.
In the Field name box in the Field Properties area, enter the merge field name manually, such as 
Opportunity_LineItem_ProductName
.
Click 
OK
.
See Also:
Install Connect for Office
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=office_word.htm&language=en_US
Release
202.14
