### Topic: Limits on Report Charts in Pages | Salesforce
Limits on Report Charts in Pages | Salesforce
Limits on Report Charts in Pages
Consider these limits when embedding charts in detail pages.
Available in: Salesforce Classic
Available in: 
All
 editions except 
Database.com
You can have two report charts per page.
You can only add report charts from the enhanced page layout editor. The mini console and the original page layout editor are not supported.
On detail pages, users can refresh up to 100 report charts every 60 minutes.
Your org can refresh up to 3,000 report charts every 60 minutes.
Parent topic:
 
Overview of Embedded Report Charts
Previous topic:
 
Customizing a Report Chart in a Page Layout
See Also:
Add a Report Chart to a Page Layout
Example of Report Charts on an Account Page
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_embed_limits.htm&language=en_US
Release
202.14
