### Topic: User can no longer see the left side panel containing Report folders.
When clicking on the Reports tab, the User does not see the left side panel containing the Report folders. The expander becomes visible, but when you expand, the page does not adjust and the panel covers the name of the Reports. 
Resolution
This occurs when Users have "Accessibility Mode" checked on their User record. Unchecking this will resolve the issue. 
To change this, go to My Settings|Personal|Advanced User Details (or Setup|My Personal Information|Personal Information) and click Edit.
Uncheck the "Accessibility Mode" checkbox and click Save. Issue should be resolved. 
