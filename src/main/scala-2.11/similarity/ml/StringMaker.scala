package similarity.ml

/**
 * Created by jshetty on 9/9/16.
 */
import org.apache.spark.ml.UnaryTransformer
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.types.{DataType, StringType}

/**
 * Created by jshetty on 8/9/16.
 */
class StringMaker(override val uid: String) extends UnaryTransformer[Seq[String], String, StringMaker] {
  def this() = this(Identifiable.randomUID("stringMaker"))


  override protected def createTransformFunc: (Seq[String]) => String = {
   _.mkString(" ")
  }

  override protected def outputDataType: DataType = StringType
}
