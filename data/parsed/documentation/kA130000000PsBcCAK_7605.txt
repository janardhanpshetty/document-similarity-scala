### Topic: Which Salesforce Editions is My Domain available in? | Salesforce
Which Salesforce Editions is My Domain available in? | Salesforce
Which Salesforce Editions is My Domain available in?
Performance, Unlimited, Enterprise, Developer, Professional, and Group editions.
See Also:
What is My Domain?
What are My Domain's advantages?
Does My Domain work differently in different Salesforce Editions?
Does My Domain work in sandboxes?
What are the differences between the Redirect Policy options?
How does My Domain work with single sign-on?
Is My Domain available for the API?
Is the subdomain for My Domain related to the subdomain for Sites?
Is there a limit on how long our subdomain can be?
After we set up My Domain, will we still be able to log in from https://login.salesforce.com?
Will we still be able to log in from a URL that includes a Salesforce instance, like https://yourInstance.salesforce.com/?
Can we still use our old Salesforce bookmarks?
Will our Visualforce and content (files) page URLs change?
Can I rename or remove my custom domain name?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_domain_name_which_salesforce_editions.htm&language=en_US
Release
202.14
