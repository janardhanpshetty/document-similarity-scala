### Topic: Top 10 Reporting Questions for Admin + Dashboard Questions for Admin (what Admin needs)
Dear Customer -
 
Thank you for contacting Salesforce Premier + Admin Support to assist you with this request. I have just received your request to create Reports and/or Dashboards.   Based on the information I received, I was not able to create the reports outlined.   I will need the following additional information in order to begin this request.
 
 
First we will have to build all of the reports and graphs on the reports, then, add them to the Dashboard.   For right now it would be easier to create the reports first, and after all of them have been verified as correct for you, we can plug them into dashboards to wrap up the request.
 
Resolution
Top Ten Report Questions
(note that this is 10 questions for every report. Please section your responses in groups of 1-10, one batch being for each report.)
1) what Object (s) are you pulling information from ? (Example: Customer Support Reports: Cases)
2) Would you like this report to be Tabular, Summary, or a Matrix Report (Example: Summary Report)
3) Would you like any fields totaled? (Example: Record Count)
4) If the report is a Tabular, skip this step. If the Report is a summary what are the three levels that you would like to group by? You do not need to group by all three, but must have at least one option. (Example: Case Owner, then by Case Number). If the report is a Matrix Specify your Row Headings, and Specify your Column Headings. You can have up to 2 Row Headings and 2 Column Headings. You cannot have 3 in either. The limit is 2.
5) What columns would you like in this report? (Example (Object:field name): Case:Case Owner, Case: Number, Contact: Contact Name, Account: Account Name)
6) What is the order you would like to see the columns in? (Example: Case Owner, Case Number, Contact Name, Account Name)
7) Select your Report Critieria. Any filters or things that you want to only include x, or exclude all of y, add here. Specify if you would like all records in this object, your records, or your team's records (Example: All cases with a opened date of THIS MONTH, add a filter that pulls all Cases with Account Name that starts with the letter "a").
8) What kind of chart would you like? You are limited to the options salesforce has. Please look at the options (Example: Chart type: Vertical Column, Y-Axis Record Count, X-Axis Case Owner).
9) What is the name of the Report? (Example: SFDC CASES TEST)
10) What folder should this be saved in? (Exampled: Unfiled Public Reports).
 
Dashboard Questions:
When asking for an Admin Analytics (Dashboard ) request please answer all of the following information
1) What Name should the Dashboard be?
2) What folder should it be saved in?
3) Who should have access to this folder (public group, role, or role and subordinates only. If these need to be created please log a separate case to our setup team to create the public group)
4) Who is the running user of this dashboard?
5) Should this have 2 columns or 3?
6) How many components in which columns?
7) What order should the components be in?
For each component you will have to answer the following
1) What should the Title, footer, etc. be?
2) What is the source report?
3) What graphical representation would you like? Gauges have to have break point values, and you are limited to how the report is created. If these reports have not been created, please log a separate case to have the reports created. It may be easier to have a chart/graph on the source report so you can see what it would look like if it were on the Dashboard.
4) Any other information pertaining to the component itself.
5) What column is this component in?
6) What number down is it? (example this component is in the third column and should be the fourth down).
Note you are limited to 20 components per dashboard.
