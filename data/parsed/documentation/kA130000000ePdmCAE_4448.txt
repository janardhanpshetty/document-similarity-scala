### Topic: Metrics in Login Forensics | Salesforce
Metrics in Login Forensics | Salesforce
Metrics in Login Forensics
Forensic investigations often begin with a roll-up of login events, and metrics are roll-up aggregations of login events over time.
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Login forensics can be:
A count.
A count followed by an aggregation.
Each time-series metric contains:
A query that uses aggregate functions.
An hourly sampling interval.
A frequency time frame, such as from 01-01-2015 12:00 to 01-01-2015 1:00.
With this information, you can find anomalies by viewing summary data and then following up with more detailed queries to perform additional investigations. All metrics are generated once per hour and are accessed using the PlatformEventMetrics object.
In the following example, the total number of logins is four, but the aggregated number of logins by each user differs. The user with an ID that ends in “122” logged in once, and another user logged in three times.
Login Time
User Id
12:00
005000000000122
12:15
005000000000123
12:31
005000000000123
12:47
005000000000123
This table shows the collected metrics for the 12:00-1:00 time frame.
Metric Type
Aggregation Field Name
Aggregation Field Value
Value
NumLogins
Null
Null
4
NumLoginsByUser
UserId
005000000000122
1
NumLoginsByUser
UserId
005000000000123
3
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=monitor_login_forensics_metrics.htm&language=en_US
Release
202.14
