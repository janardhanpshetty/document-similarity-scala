### Topic: Outbound Change Sets | Salesforce
Outbound Change Sets | Salesforce
Outbound Change Sets
An 
outbound change set
 is a change set created in the Salesforce org in which you are logged in and that you want to send to another org. You typically use an outbound change set for customizations created and tested in a sandbox and that are then sent to a production org.
User Permissions Needed
To create, edit, or upload outbound change sets:
“Create and Upload Change Sets,”
“Create AppExchange Packages,”
AND
“Upload AppExchange Packages”
Watch a Demo: 
Release Management: Deploying Changes Using Change Sets
Note
A change set can have up to 10,000 files with a total file size of 400 MB. Change set components are represented as metadata XML files. Make sure that your change set doesn’t exceed approximately 5,000 components.
Sending an outbound change set to another org doesn’t guarantee that the changes are implemented in that org. The change set must be deployed by the target org before the changes can take effect.
Tip
To help ensure a smooth deployment, review information about permission sets and profile settings in change sets.
After you upload a change set, its status becomes closed, and you can’t make changes to its components. Also, the components in a closed change set don’t get refreshed. To redeploy the same set of components, clone the change set. The cloned change set includes the latest changes to its components source. You can add and remove components in the cloned change set. Cloning change sets is helpful during the iterative phases of a project.
See Also:
Select Components for an Outbound Change Set
Create an Outbound Change Set
Permission Sets and Profile Settings in Change Sets
Inbound Change Sets
Change Sets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=changesets_about_outbound.htm&language=en_US
Release
202.14
