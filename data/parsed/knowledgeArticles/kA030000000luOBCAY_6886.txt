### Topic: This is the process that should be taken to have this limit increased.
Available in:  Contact Manager, Group, Professional, Enterprise, Performance, Unlimited and Developer Editions
This limit represents the amount of Custom Apps available in an organization based on the edition.
The default values are:
Contact Manager: 1
Group: 1
Professional: 255
Enterprise: 260
Unlimited and Performance: Unlimited Edition
Developer: 10
The custom Apps contained in a managed package publicly posted on the AppExchange don't count against the limits for your Salesforce Edition.
For further information about the standard limits, please check the article: 
Salesforce Features and Edition Limits
Resolution
If you've reviewed all relevant documentation and would like to get this limit increased please be informed that this limit increase must be handled by your 
Account Executive.
Support will not be able to assist you with this increase.
If the organization that need the increase is owned by a Partner:
Have a System Administrator log a Case with Salesforce Partner Support
Please include all important details including the organization ID and a business case for the request
Partner Support will review the Case and action it as needed.
