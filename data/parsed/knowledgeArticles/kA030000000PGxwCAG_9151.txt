### Topic: Solution for error ' SObject row was retrieved via SOQL without querying the requested field' when extending standard controller with custom controller in Visualforce page
The error "SObject-row-was-retrieved-via-SOQL-without-querying-the-requested-field" can occur when accessing a Visualforce page with a standard controller and a custom controller extension. 
When a standard controller queries an object for a Visualforce page, it only retrieves the fields from the object that are referenced in the page. 
This improves efficiency by not retrieving fields that are not needed by the Visualforce page. 
When a custom controller references a field that is not included in the page, the field data is not available, and the error occurs. 
Resolution
Two workarounds to resolve this issue are
You can add a SOQL query to the custom controller that queries the missing fields 
You can add a hidden reference to the field in the Visualforce page. 
For example: 
<apex:outputText value="{!Condition__c.Criterion__c}" rendered="false"/>
 
