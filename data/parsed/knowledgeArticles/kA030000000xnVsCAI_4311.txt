### Topic: Normally upon creation of a new user by a full system administrator, the Role defaults to blank (<None Specified>), but for a delegated admin it will be the first in the alphabetical list of roles and subordinates they are allowed to use.
Delegated administrators are restricted to administer only certain Roles and their Sub-ordinates. These roles will show up in the pick-list for Roles when creating a new user. Alphabetically the first role always defaults during creation
A Delegated Administrator might overlook this since it is pre-populated and create a user with incorrect role.
Resolution
As a workaround, in the role hierarchy under the roles which can be accessed by delegated administrators, create a new role, which would act as a dummy role. Example the "AAA-Dummy Role". This name will sort first in the list.
Next setup a validation rule which will prevent the creation of an user with this role:
CONTAINS( UserRole.Name , "
AAA-Dummy Role
")
This rule will kick in whenever the delegated admin leaves the role to default thereby throwing an error message.
The error message can be made meaningful like "Proper Role needs to be selected". This will catch the attention of the delegated admin trying to create a new user and chances of creating an user with wrong role is greatly reduced.
 
