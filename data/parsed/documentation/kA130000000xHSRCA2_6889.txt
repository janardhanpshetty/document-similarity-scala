### Topic: Considerations for Email Notification of Task Assignments | Salesforce
Considerations for Email Notification of Task Assignments | Salesforce
Considerations for Email Notification of Task Assignments
When the setting 
Enable user control over task assignment notifications
 is enabled in Activity Settings, users see the option 
Email me when someone assigns me a task
 in their personal settings. Both the setting and the option are enabled by default. Enabling and disabling the setting affects users in different ways. Before migrating a Salesforce org, avoid excessive email notifications by disabling the setting. You can re-enable it after migration.
Available in Salesforce Classic in: 
All
 Editions Except 
Database.com
Table 1. Effects of Enabling and Disabling the Feature in Setup
User Type
Effects of Enabling the Feature
Effects of Disabling the Feature
Salesforce1 users, including Communities users
In 
My Settings
 | 
Notifications
, the 
Assigns you a task
 checkbox is displayed.
In 
My Settings
 | 
Notifications
, the 
Assigns you a task
 checkbox isn’t displayed.
Lightning Experience users
In Lightning Experience, the 
Email me when someone assigns me a task
 checkbox isn’t displayed. However, users who have turned on that option in Salesforce Classic still receive email notifications.
No user receives an email notification when another user assigns a task.
Salesforce Classic users, including Communities users
On the Create Task page, the 
Send email notification
 and 
Make this the default setting
 checkboxes aren’t displayed.
On the Reminders & Alerts page in the user's personal settings, the 
Email me when someone assigns me a task
 checkbox is displayed, and it’s selected by default.
On the Create Task page, the 
Send email notification
 and 
Make this the default setting
 checkboxes are displayed, and email notifications are sent according to whether the checkboxes are selected.
The 
Email me when someone assigns me a task
 checkbox isn’t displayed.
Partner portal users
On the Create Task page, the 
Send email notification
 and 
Make this the default setting
 checkboxes aren’t displayed.
Email notifications are sent whenever someone assigns a task to a user (partner portals have no settings page for admins or users).
On the Create Task page, the 
Send email notification
 and 
Make this the default setting
 checkboxes are displayed, and email notifications are sent according to whether the checkboxes are selected.
Users who are assigned tasks that are generated by workflow task rules
On the New Task page for workflow tasks, the 
Notify Assignee
 checkbox isn’t displayed.
If a Salesforce admin selected the 
Notify Assignee
 checkbox before Winter ’15, task assignees who deselect the option don’t receive notifications.
Email notifications are sent to task assignees according to workflow task rules.
Users who are assigned tasks through an API
Use an API to set the 
triggerUserEmail
 value. The default is false.
If 
triggerUserEmail
 = true, notifications are sent according to individual users’ settings.
If 
triggerUserEmail
 = false, email notifications aren’t sent when a task is assigned.
If 
triggerUserEmail
 is true, email notifications are sent to all users when tasks are assigned.
If 
triggerUserEmail
 is false, email notifications aren’t sent when a task is assigned.
Users who are assigned tasks through Apex
Notifications that are sent through Apex are sent in the context of the request header.
If a value in the request header is set so that email notifications are sent, email notifications are sent to users when tasks are assigned. Apex processes the header sequentially from the first line to the last. If a statement in the request header indicates that a task notification is required, an email notification is sent regardless of statements later in the header.
See Also:
Activity Reminders in Salesforce Classic
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tasks_control_email_notifications_considerations.htm&language=en_US
Release
202.14
