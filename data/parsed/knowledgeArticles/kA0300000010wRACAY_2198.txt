### Topic: Weekly Knowledge Update table with information about the Top Trending Articles in CRM Config.
Update - 
3rd of June of 2016
Top Trending 
Config 
Knowledge Base Articles 
from the 
13th of May until the 3rd of June
.
See full post in the 
Support - 'CRM Config' Skill Group
.
Archive of previous posts: 
https://org62.my.salesforce.com/kA0300000010wTG
Resolution
Title
IfYou'reReadingThis,Hello,I'mDoingItToKeepTheColumnWide
Link
Created By...
Case Count
View Count
 
NEW: 
Live Agent server (endpoint URL) has changed and now Live Agent Chat is no longer working
https://org62.my.salesforce.com/ka00M000000angt
Charles Burkard
58 (+58)
929 (+929)
New
Lightning Experience cannot be enabled - "Enable" button automatically switches back to "Disabled"    
https://org62.my.salesforce.com/kA030000001591C
Vicki Sliwa    
33 (+3)
588 (+35)
-
INTERNAL: Duplicate Username Error 
https://org62.my.salesforce.com/kA0300000015920
Charles Burkard
36 (+11)
402 (+112)
+3
Manage Your Workflow Rules
https://org62.my.salesforce.com/kA0300000019Ktr
Vaishnavi Gaikwad
46 (+4)
360 (+33)
-1
INTERNAL Cannot deactivate the default workflow user error message in Professional Edition
https://org62.my.salesforce.com/kA030000000eoZG
Prasad Pimpalgaonkar
16 (+3)
377 (+64)
-1
Unexpected Delays with Salesforce-to-Salesforce (S2S) Forwarding    
https://org62.my.salesforce.com/kA030000000eoTm
Josh Weltman   
11 
308 (+33)
-1
NEW: 
Omni-Channel Limit Exceeded and Troubleshooting
https://org62.my.salesforce.com/kA00M000000lvJy
Adrian Alvarez
13 (+13)
301 (+301)
New
NEW: 
INTERNAL - Omni-Channel BT Limits
https://org62.my.salesforce.com/kA0300000019KsU
Adrian Alvarez
9 (+9)
232 (+232)
New
NEW:
 Unable to assign Sales Console permission to user
https://org62.my.salesforce.com/kA00M000000ZqDj
Brittinie Harper
16 (+16)
222 (+222)
New
NEW: 
Enable Shared Contacts
https://org62.my.salesforce.com/kA00M000000eoiN
Joshua Gates
10 (+10)
225 (+225)
New
Please note
- These are Articles first published in the 
last 90 days
. In the past it was 30 days.
- The 
"NEW"
 remark means that it wasn't in the list the previous week. It doesn't necessarily mean that the Article has been recently created.
