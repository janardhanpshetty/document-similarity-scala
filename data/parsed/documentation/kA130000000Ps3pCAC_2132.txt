### Topic: Permission Sets and Profile Settings in Change Sets | Salesforce
Permission Sets and Profile Settings in Change Sets | Salesforce
Permission Sets and Profile Settings in Change Sets
Developers can use permission sets or profile settings to specify permissions and other access settings in a change set. When deciding whether to use permission sets, profile settings, or a combination of both, consider the similarities and differences.
Available in: both Salesforce Classic and Lightning Experience
Available in 
Enterprise
, 
Performance
, 
Unlimited
, and 
Database.com
 Editions
Permission sets available in: 
Contact Manager
, 
Professional
, 
Group
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Behavior
Permission Sets
Profile Settings
Included permissions and settings
Standard object permissions
Standard field permissions
User permissions (such as “API Enabled”)
Note
Assigned apps and tab settings are 
not
 included in permission set components.
Tab settings
Page layout assignments
Record type assignments
Login IP ranges
User permissions
Included permissions and settings that require supporting components
Custom object permissions
Custom field permissions
Apex class access
Visualforce page access
Assigned apps
Custom object permissions
Custom field permissions
Apex class access
Visualforce page access
Added as a component?
Yes
No. Profiles are added in a separate setting.
For custom object permissions, custom field permissions, Visualforce page access, and Apex class access, always include supporting components in the change set. For example, object permissions for the custom object 
Items
 are included only if the Items object is also included.
Note
Login IP ranges included in profile settings overwrite the login IP ranges for any matching profiles in the target org.
See Also:
Inbound Change Sets
Outbound Change Sets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=changesets_perm_sets_profiles.htm&language=en_US
Release
202.14
