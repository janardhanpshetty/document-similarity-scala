### Topic: Enabling HTTPS in a Call Center | Salesforce
Enabling HTTPS in a Call Center | Salesforce
Enabling HTTPS in a Call Center
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To enable HTTPS in a call center:
“Customize Application”
AND
“Manage Call Centers”
With CTI adapters built with version 4.0 of the CTI Toolkit, you can specify a secure URL, or one that uses the secure hypertext transfer protocol (HTTPS), for your call center. Using HTTPS provides added security for your call center, and also helps prevent the Mixed Content warnings that can appear in your browser if your Salesforce organization uses the HTTPS protocol but your call center does not.
To enable HTTPS:
From Setup, enter 
Call Centers
 in the 
Quick Find
 box, then select 
Call Centers
.
Click the name of a call center.
Click 
Edit
.
Type the secure URL for your adapter in 
CTI Adapter URL
. For example, 
https://localhost:11000
.
Click 
Save
.
Important
In addition to specifying a secure URL on the Call Center Settings page, you also need to make changes to the CTI adapter's configuration file, and create and install a new certificate for the CTI adapter. For more information, see the 
CTI Toolkit Developer's Guide
.
Note
Previous versions of CTI are secure but use Windows® technologies that are different than those in CTI 4.0.
See Also:
Salesforce CTI Toolkit Overview
Setting Up Salesforce CRM Call Center
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cti_admin_enable_https.htm&language=en_US
Release
202.14
