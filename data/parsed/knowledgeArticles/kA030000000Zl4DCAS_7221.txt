### Topic: When configuring state and country picklists I used the Configure States and Countries page to rename "Chinese Taipei" to "Taiwan". This change was saved and accepted. However, when I go to the next step of the conversion process, Convert Countries, Taipei isn't in the picklist and Chinese Taipei is still there.
When configuring State and Country Picklists  I used the Configure States and Countries page to rename "Chinese Taipei" to "Taiwan". This change was saved and accepted. However, when I go to the next step of the conversion process, Convert Countries, Taipei isn't in the picklist and Chinese Taipei is still there.
Example Steps To Replicate:
1. Go to Setup | Data Management | State and Country Picklists
2. Click on Configure states and countries
3. Edit Chinese Taipei (code TW)
4. Change name and integration value to Taiwan and save
6. Scan
7. Click on Convert now
8. Look for Taiwan in the Change To drop-down menu
 
Expected result: Taiwan can be selected
Actual result: Taiwan is not found; the text for TW is still Chinese Taipei
Resolution
R&D has investigated this matter and logged a Bug for it to be repaired. 
Please note that the value WILL be changed upon completing the setup process--this is a visual bug that only affects the "Change To" picklist when setting up State & Country picklists. 
Unfortunately, we cannot provide a timeline as to when this repair will be implemented due to Safe Harbor constraints.
