### Topic: Archived activities impact reports as the default archive days is 365 days.
If a customer is requesting to Report on activities older than 1 year, then use this article: 
Increasing Archive Days for your Organization
 
Resolution
Tasks are Archived when a Task is in a Closed Status and the Due Date is older than Black Tab Setting displayed in "Archive Days."
Tasks are Archived if they have been closed, don't have a due date, and were created more than 365 days ago.
Events are Archived when the due date is greater than 365 days old.
Data Export function extracts archived Activities.
The default Black tab Setting value is 365 Days.
It is not triggered by the Task Creation Date.
Apex Code, Triggers, etc. would be affected by this process if the query runs through the API and doesn't queryall(). (See below information).
Activity date is not a trigger, it is triggered by Due Date.
Archived Tasks are available on related lists.  They are not available through reports or API Queries.
The archival background process currently runs once a week every Saturday.
 
Salesforce
 archives activities (tasks and events) that are over a year old.  The following is outlined in the API documentation. Additionally, Data Loader version 21 and greater have an "Export All" feature that uses the queryAll() command described below. Running the following command using the "Export All" command in Data Loader will return all archived tasks.
SELECT Id FROM Task WHERE IsArchived = true
You can use 
q
ueryAll()
 to query on all 
Task
 and 
Event
 records, archived or not. You can also filter on the 
isArchived
 field to find only the archived objects. You cannot use 
q
uery()
 as it automatically filters out all records where 
isArchived
 is set to 
true
. You can update or delete archived records, though you cannot update the 
isArchived
 field. If you use the 
API
 to insert activities that meet the criteria listed below, the activities will be archived during the next run of the archival background process.
 
Older 
Event
s
 and 
Task
s
 are archived according to the criteria listed below. In the 
Salesforce
 user interface, users can view archived activities only in the 
Printable View
 or by clicking 
View All
 on the Activity History related list or by doing an advanced search. However, in the 
API
, archived activities can only be queried via 
q
ueryAll()
.
Activities are archived according to the following criteria:
Event
s
 with an 
ActivityDateTime
 or 
ActivityDate
 value greater than or equal to 365 days old
Task
s
 with an 
IsClosed
 value of 
true
 and an 
ActivityDate
 value greater than or equal to 365 days old
Task
s
 with an 
IsClosed
 value of 
true
, a blank 
ActivityDate
 field, and a create date greater than or equal to 365 days ago
Reference: http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_guidelines_archive.htm
