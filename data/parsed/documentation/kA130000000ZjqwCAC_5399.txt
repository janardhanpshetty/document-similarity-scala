### Topic: Getting Started with Organization Sync | Salesforce
Getting Started with Organization Sync | Salesforce
Getting Started with Organization Sync
With Organization Sync, you can set up a secondary, synced Salesforce organization which users can access whenever your primary organization is experiencing downtime or maintenance.
Available in: Salesforce Classic
Organization Sync is available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Organization Sync
At Salesforce, we recognize that you need to access your data at a moment’s notice—even during our downtimes and maintenance. With Organization Sync, you can set up a secondary, synced Salesforce organization where users can work on your most business-critical processes and data whenever your primary organization is experiencing downtime or maintenance.
Considerations for Setting Up Organization Sync
Before you start to set up Organization Sync, review this essential information. The way you currently use auto-numbered fields, Apex triggers, audit fields, and other Salesforce features determines how you approach your implementation.
Set Up the Organization Sync Environment
As an Organization Sync administrator, you have a primary and a secondary Salesforce organization, each with a different administrator username and password. Before you can add data to the secondary organization, you must complete a few setup tasks and create a replication connection between the two organizations.
Define the Organization Sync User Experience
Understand the options and limitations behind user rerouting, logins, and data in Organization Sync.
Publish and Subscribe to Objects in Organization Sync
Once the primary and secondary organizations are connected via the Organization Sync connection, it’s time to perform a publish and subscribe process that allows record inserts, updates, and deletes to flow between the organizations.
Perform a Bulk Sync
A bulk sync triggers the copying of any new record whose object type is published and subscribed, making the record available in both organizations. A bulk sync copies records in one direction, so performing a bulk sync in the primary organization copies new records to the secondary organization.
Managing Organization Sync
Stay informed about the health of your Organization Sync environment.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=org_sync_doc_overview.htm&language=en_US
Release
202.14
