### Topic: Formatting may cause an "Error: No matches found." message when creating Contact within Account.
By default, when creating a Contact or Activity (Task/Event) within an Account, the Account Name lookup field will automatically populate. Upon trying to save the record, the user might encounter an issue that the Account doesn't exist or 
"Error: No matches found".
Resolution
There are some situations when this may occur:
1. This may occur if Account Name was copied from an HTML-based text editor and some extra format was carried over when pasting the value in Salesforce. In this case, try removing the extra format by copying the Account name (or the field in question) on Notepad and then copy it again to Salesforce. 
2. This may occur if the Account was recently imported (such as via the Data Import Wizard) and the Account Name field contains an unexpected character (such as a line break). In some views, such as list views or recent items, the line break will be visible. And in other situations, such as the lookup field name, the line break character will be automatically removed. To correct this, try a null edit of the Account record (Click Edit, then immediately Click Save with no changes). 
