### Topic: PDF rendering for double-byte and right-to-left characters and languages by breaking up the words with spaces.
There is a limitation-of-PDF-rendering-for-double-byte-and-right-to-left-characters-and-languages
Resolution
Workaround here is to break up the words with spaces to get it to render correctly.
Also please find below the sample code:
++++++++++++++
private String addLf(String value, Integer px){
// Following is an example of a manual word-wrap.
// The Coefficient needs to be adjusted accordingly.
// Coefficient = # of Chars / # of Pixels
final Double COEFFICIENT = 0.09;
Integer posLength = (px * COEFFICIENT ).intValue();
String rtnValue = value.substring(0, posLength ) + '\n';
for(Integer i = posLength; i < value.length(); i++){
if(Math.mod(i, posLength ) == 0){
if(value.length() > i + posLength){
rtnValue += value.substring(i, i + posLength ) + '\n';
}else{
rtnValue += value.substring(i,value.length());
}
}
}
return rtnValue;
}
++++++++++++++
Usage: Before rendering it with pdf, replace the original text(string) value by passing it through the function above.
In the example below, the text will be returned with line breaks inserted at every 400 pixels.
e.g. Test__c.PDFValue__c = addLf( Test__c.PDFValue__c, 400);
