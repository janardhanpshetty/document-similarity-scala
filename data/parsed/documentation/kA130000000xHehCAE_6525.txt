### Topic: How Do I Refine Search Results in Lightning Experience? | Salesforce
How Do I Refine Search Results in Lightning Experience? | Salesforce
How Do I Refine Search Results in Lightning Experience?
Use our guidelines for navigating the search results page to quickly find records.
Available in: Lightning Experience
Available in: 
All
 Editions 
except Database.com
Sort your results
You can order the results using the sort drop-down. The menu lists all the columns in the search layout in addition to 
Relevance
. Sort by choosing one of the items in the drop-down. Or, click the column headers.
Sorting results helps if there are too many records on the search results page.
If the search layout includes all non-sortable fields, the sort button is disabled.
See the most relevant results on one page with Top Results
The Top Results search results page lists the most relevant record results for each of your most frequently used objects.
This page is a good choice when you want to review results for a search term but you aren't looking for a specific object type. For example, you search for 
Acme Inc
. and look at the Top Results page for accounts, leads, and opportunities. Each person's Top Results page is different depending on which objects they use the most. Use the 
View More
 link in case you want to see more results for an object.
See All Company posts under Feeds
If enabled by your administrator, the Feeds search results page lists posts from the All Company feed that include matches on the search term entered.
Using Feeds is helpful when searching for a post that you want to review again. For example, you saw a post about a new training video, but you don't remember who posted the information.
Filter results by object with the search scope bar
Quickly click through filtered results by object using the search scope bar beneath the global search box. Searchable objects are shown in the same order they appear in the navigation menu. To see results for the object, click the name.
Let's say that you search for a contact from the Accounts home page and landed on the Accounts search results page. Click the Contacts object to see results for contacts.
See all available objects under More
If you don't see an object in the search scope bar, select 
More
 to see a list of all objects that are available to you. Searchable objects listed in the navigation menu that don’t fit in the search scope bar appear at the top of the 
More
 object list.
More Guidelines
Check your spelling and that you entered the full search term. Or, try entering a more specific search term.
If you recently created the record or the record was changed multiple times, the record won’t appear in the search results right away. The process for making a record searchable, called indexing, can take 15 minutes.
Search in Salesforce Lightning Experience
See Also:
Why Can’t I See Some Features?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_refine.htm&language=en_US
Release
202.14
