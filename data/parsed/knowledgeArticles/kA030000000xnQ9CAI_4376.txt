### Topic: The "Add to Campaign" button will only appear in a "Accounts with Assets" report type when there are fields linked to both the Account and the Assets (i.e. if you create a report with the "Account Name" only you won't have the "Add to Campaign" button. Sometimes you could notice that using the "Add to Campaign" functionality won't add Person Accounts.
I've created a Report using the "Accounts with Assets" Report Type. I'm using a filter to import only Person Accounts.
When I use the "Add to Campaign" standard button in the Report nothing happens, no new Campaign Members are being created.
Resolution
Assets need to have a relationship with either an Account, a Contact or both.
The Account part of it is not being considered in the "Add to Campaign" functionality and, what is likely to be happening is that your Assets don't have a Contact linked to them. Only an Account.
When using the "Add to Campaign" button we will filter the following:
- Filter out Accounts and Person Accounts without an Asset, as per the Report Type definition.
- Filter out Accounts, leave only Person Accounts, as per the Article linked above.
- Filter out Assets that are not related to Contacts. Person Accounts are in both Account and Contact tables.
You can reproduce this easily in any organization.
1. Create two person Accounts: PA1 and PA2.
2. Create an Asset on PA1, give values to both the "Account" and "Contact" fields in the Asset record.
3. Create an Asset on PA2, give only values to "Account" field, leave the "Contact" field blank.
4. Create a Report with that two records.
5. Try to add them to a Campaign.
Results: The record with the Asset associated to both Account and Contact table will be added. The other won't.
Resolution
: You need to populate both the Account and Contact fields in the Assets that are related to Person Accounts in order to avoid undesired behavior.
