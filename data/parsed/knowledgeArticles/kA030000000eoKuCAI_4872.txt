### Topic: When using the findOrCreate methods in the Deployment API, you can specify criteria to search for records or create a new record if none are returned. In some instances you may find the results are not what you expected when using doFind.
As per our 
documentation
, you use the findOrCreate method to find existing records or create new ones based on certain criteria. The 
findOrCreate
 method begins the API call that finds existing records or create new records when an agent begins a chat with a customer. The results of which are displayed to the accepting Agent.
When using the map method, it searches for or creates records that contain customer data specified by the 
addCustomDetail
 Deployment API method. This method maps the value of the custom details to the fields on the specified record in the Salesforce console. When using this against an entity, you may find the results to be different than expected or to return all records you require.
 
Resolution
The findOrCreate methods utilize SOQL as part of the searching methods to return records. While using these methods, if the results are not what you expected, simply build the same criteria as a SOQL statement to compare the result. You should see that the results are the same.
For example:
liveagent.addCustomDetail("First Name", "John");
liveagent.addCustomDetail("Last Name", "Doe");
liveagent.addCustomDetail("Email Address", "john.doe@customer.com");
liveagent.findOrCreate("
Lead"
).map("
LastName"
, "
Last Name"
, 
true
, 
true
, 
true
).map("
Email"
, "
Email Address
"
, 
true
, 
false
, 
true
)
would be the same as:
SELECT Id, Email from Lead WHERE LastName = 'Doe' AND Email like 'john.doe@customer.com%';
