### Topic: Data Loader Third-Party Licenses | Salesforce
Data Loader Third-Party Licenses | Salesforce
Data Loader Third-Party Licenses
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
The following third-party licenses are included with the installation of Data Loader:
Technology
Version Number
License
Apache Jakarta Commons BeanUtils
1.6
http://www.apache.org/licenses/LICENSE-2.0
Apache Commons Collections
3.1
http://www.apache.org/licenses/LICENSE-2.0
Apache Commons Database Connection Pooling (DBCP)
1.2.1
http://www.apache.org/licenses/LICENSE-2.0
Apache Commons Logging
1.0.3
http://www.apache.org/licenses/LICENSE-1.1
Apache Commons Object Pooling Library
1.2
http://www.apache.org/licenses/LICENSE-2.0
Apache Log4j
1.2.8
http://www.apache.org/licenses/LICENSE-2.0
Eclipse SWT
3.452
http://www.eclipse.org/legal/epl-v10.html
OpenSymphony Quartz Enterprise Job Scheduler
1.5.1
http://www.opensymphony.com/quartz/license.action
Rhino JavaScript for Java
1.6R2
http://www.mozilla.org/MPL/MPL-1.1.txt
Spring Framework
1.2.6
http://www.apache.org/licenses/LICENSE-2.0.txt
Note
Salesforce is not responsible for the availability or content of third-party websites.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_loader_licenses.htm&language=en_US
Release
202.14
