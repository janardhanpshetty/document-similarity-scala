### Topic: This article addresses how to unblock a customer when Territory Assignment Rules are stuck in processing. This is specific to Enterprise Territory Management.
Occasionally, customers with Enterprise Territory Management (TM 2.0) will report that Territory Assignment Rules are stuck in processing for an extended amount of time with no email notification of completion.
When this occurs, the following tools can be used to determine whether the rules are legitimately processing or if a failure has occurred.
Splunk queries: 
https://sites.google.com/a/salesforce.com/tm2-0/tm2-0-resources/splunk
Radio (Active Requests): 
https://pal.dmz.ops.sfdc.net/pal/index.html
Background Jobs: in org 
Setup | Jobs | Background Jobs
Processing is stuck when the "Run Assignment Rules" button is grayed out AND:
  =====
- Background Jobs show "Territory Rule Realign Chunk Job" as Completed
- Radio shows no Active Requests
- Splunk shows Failures
  =====
Resolution
We have added a tool in Blacktab (BT) that can be used to reset the status of the Territory Model in the event processing becomes stuck.
[BT user must have advanced dev diagnostics permission]
Note
: Before proceeding, please obtain customer permission to proceed, as this can have an impact on sharing status.
1. Access the customer's org in BT.
2. Go to 
Setup | Manage Territories | Territory Models.
3. Click the 
Reset Model
 link for the model where rules are stuck.
4. Check the box for '
Reset flag indicating assignment rules are running
' and 
Save
.
Once this is done, Territory Assignment Rules can be run again by clicking the 
Run Assignment Rules
  button.
