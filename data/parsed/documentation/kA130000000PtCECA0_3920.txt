### Topic: Prospect for Key Contacts at Your Accounts | Salesforce
Prospect for Key Contacts at Your Accounts | Salesforce
Prospect for Key Contacts at Your Accounts
Need to find the right contact at one of your accounts? With a Data.com Prospector license, it’s a cinch to search for contacts at your companies by title, location, and industry. Then, with a click, you can add those records to Salesforce.
Available in: Salesforce Classic
Available with a Data.com Prospector license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To get Data.com contacts from an account:
“Read” on accounts
AND
“Create” on contacts
Before you begin, make sure the 
Get Contacts
 button has been added to the Account page layout.
Go to the detail page of the account you want to add contacts for.
Click 
Get Contacts
. From the drop-down, either:
Select a filter to narrow your results by level or department, or
Select 
All...
 to see all contacts for the account.
The Data.com tab opens and displays a list of contacts that match the account name and any filters you selected. For example, if you want to get manager contacts at Universal Telco, go to Universal Telco's account detail page. Click 
Get Contacts
 and under 
By Level
, select the 
Manager-Level
 filter. The Data.com tab opens with a list of managers at Universal Telco.
Select more filters if you need a more targeted list. Expand filter categories as needed. Enter a range or select the checkbox next to each filter you want to apply.
Add all contacts or a selection to Salesforce.
To add all contacts, click 
Add to Salesforce
.
To add selected contacts, first select the ones you want, then click 
Add to Salesforce
 and choose 
Selected
.
Select 
Contacts
 to add the records as contacts associated with the account you started from. 
They'll be associated with the account you started from.
Click 
Continue
 to add the records.
If your selection includes duplicates, we’ll add them if duplicate contact records are allowed in your Salesforce organization. Adding more than 200 records can take a few minutes. If you add that many, we'll send you an email when the process is complete.
Click 
Go to Account
 to return to the account record.
See Also:
Prospect for Contacts Fast Right In Salesforce
Prospect for Leads Fast Right In Salesforce
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=jigsaw_int_adding_contacts_from_account.htm&language=en_US
Release
202.14
