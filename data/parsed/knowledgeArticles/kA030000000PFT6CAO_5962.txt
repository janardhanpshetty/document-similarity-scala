### Topic: It is not actually possible to customize the Notes and attachment related list nor to set the permission on the profile as it's dependent on the parent object.
Is it possible to remove the right to delete an Attachment/Note to all Users or to a Profile? I do not see this option in the permissions.
We would like to have a more flexible control over the accountability of our approvals based on Attachments/Notes. These Notes should not be deleted from the records associated even if the User has edit abilities to the record.
Resolution
Because the permission to delete Notes and Attachments is tied to the Edit permission on the object of  record itself, you will need to take the edit permission from the object as unfortunately it's not possible to modify permissions to Notes & Attachments itself.
Please visit our 
Idea Exchange
 to promote the following Ideas:
Delete Notes
Disallow file deletion based on user profile permission
A few possible workarounds:
- Create a Visualforce page that displays the Notes and Attachment related list, but without displaying the delete link to the Notes and Attachment records.
- Create a Trigger that would prevent the deletion of the Notes and Attachment records.
- Install the following package from the 
Appexchange
: 
AttachmentSaver 1.1.1
Important: Please note that the Package is a third party application that is not supported by salesforce.
 
