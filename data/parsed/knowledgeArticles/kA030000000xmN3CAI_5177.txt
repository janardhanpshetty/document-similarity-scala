### Topic: Approvals submitted to you are now included in the in-app notifications. Viewing an approval will give you the option to Approve or Reject the request. End user's can submit a record for approval.
Submitting Approval in Salesforce1
Users can submit records for Approval from the Salesforce1 downloadable app. The app displays the 
Submit for Approval 
button in the Action Bar of the record, when:
There is an active Approval Processes 
Page layouts include the 
Submit for Approval 
button
 
**Limitations to keep in mind**
​
Users can’t manually select approvers in Salesforce1. For approval processes that require manual selection, the submitter needs to log in to the full Salesforce site to submit a record for approval. 
Approving via Salesforce1
Users can also receive approvals from both the Full Site and those submitted via Salesforce1 as notifications that display as both In-App and as Push Notifications. Viewing an approval will give you the option to Approve or Reject the request.  
You can also add comments to your approval or rejection of the request.
**Limitations to keep in mind**
There are currently no options to re-assign the approval request.
You can access the approvals via the notifications menu however there is no way to access a full list of records to approve at this time. 
Notifications for approval requests aren’t sent to queues.
Unlike notifications for approval requests in email, notifications for approval requests in Salesforce1 are sent only to users who have access to the record being approved. Assigned approvers who don’t have record access can still receive email approval notifications, but they can’t complete the approval request in Salesforce1 unless someone grants them record access.
Individual users can opt in or out of approval request notifications in both email and Salesforce1 via the Receive Approval Request Emails user field.
For more information on on the Above topics:
Salesforce1 Push Notifications
Approval Process Overview
Approvals in Salesforce1: Limits and Differences from the Full Salesforce Site
Resolution
