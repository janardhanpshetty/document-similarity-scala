### Topic: View Bookmarks or Recently Used Tabs in a Salesforce Console | Salesforce
View Bookmarks or Recently Used Tabs in a Salesforce Console | Salesforce
View Bookmarks or Recently Used Tabs in a Salesforce Console
If set up by your Salesforce admin, you can quickly access any primary tabs that you’ve bookmarked or recently used in a console without having to search Salesforce.
Salesforce console available in Salesforce Classic and App Launcher in Lightning Experience. Setup for Salesforce console available in Salesforce Classic.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Note
With Summer ’15, Most Recent Tabs was renamed History and bookmarks were added to it to give you a more streamlined experience for finding and retrieving tabs. These features aren’t available in Internet Explorer® 7 and 8.
To view any bookmarked or recently used primary tabs, click 
History
 in the console’s footer. Then click the Bookmarks or Recent Tabs column and select a tab. You can get each tab’s URL to send it to another user, or remove bookmarks and clear recently used tabs to start from scratch. Next to each recent tab, you can access up to ten subtabs when they’re available. Items that don’t display as tabs, such as meeting request overlays, don’t display in the History footer.
See Also:
Guidelines for Working with Salesforce Console Tabs
Use a Salesforce Console
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=console2_most_recent.htm&language=en_US
Release
202.14
