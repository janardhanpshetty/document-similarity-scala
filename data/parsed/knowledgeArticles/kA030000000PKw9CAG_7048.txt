### Topic: Create a workflow rule on the Case or Lead object(s) to trigger every time a record is created/edited with the following formula syntax as the trigger criteria.
When the new record owner is a queue, you cannot check the "Send Notification" check-box by default. 
Resolution
WORKAROUND:  Create a workflow rule on the Case or Lead object(s) to trigger every time a record is created/edited with the following formula syntax as the trigger criteria: 
AND( 
ISCHANGED(OwnerId), 
LEFT(OwnerId, 3) <> "005") 
This says that when you re-assign a Case or Lead record, AND the owner is not a user (i.e a Queue), the workflow will trigger (since all user ID's start with "005"). 
Then, create an email alert (using the same notification template as in your Case/Lead settings) to be sent to the Case Owner (which will be the queue). 
**NOTE: the only downside to this work-around is that if a user checks the "Send Notification" checkbox when assigning a case to a queue, the members will receive both emails (1 from the email alert, 1 from the case notification feature).
Reference and Vote! for this posting on the Idea Exchange: 
Add option to default "Send Notification Email" to checked when changing owner
