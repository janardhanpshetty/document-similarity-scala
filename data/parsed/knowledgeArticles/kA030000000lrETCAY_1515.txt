### Topic: Why might a site be pulling content, but certain articles could be missed and not saved in our system.
Why is a post from an RSS Feed missing if the Feed is crawled by Radian6?
Resolution
1. The post may never have been on the feed.
2. The post may be on a different feed on the same site.
3. The feed may only contain a subset of the article text is in the feed. If the keyword was not in the partial content, it can not be keyword matched.
4. The feed may be update its content so frequently that content that was on the feed yesterday may not be on it tomorrow. Sometimes feeds post frequently during the day but may have a limit on how many posts are on the RSS feed at one time. The new post will replace the old ones and if this happens frequently, the content that has been rolled over and moved off the feed may not have been captured since the last time the crawlers visited the Feed.
5. The customer's keyword was not entered in the system until after the post was published to the RSS Feed. If the crawler already visited the Feed but the keyword was not yet entered into a Topic Profile, that content won't be captured unless the crawler visits that Feed again and the post is still there.
