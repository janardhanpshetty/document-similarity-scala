### Topic: This will allow Administrator to run a User report that will show the User License type of each user in their org.
How to create a report on 
User License
 type assigned to each user's?
Resolution
As per the 
Standard Report Type
, License type will display as 
Standard
 if
 Salesforce license
 is assigned to users.
To display 
Salesforce 
as a 
User License type
 on report, Below is the workaround:
1) Create a 
Custom Formula
 
Text
 field under the 
User object
:
        a) Go to Setup | Customize | Users | Fields
        b) Click on 
New
 and select 
Formula
 field type and then 
Text
        c) Use the formula: 
Profile.UserLicense.Name
        d) Click on
 Save.
2) Create a New 
Administrative User
 report:
        a) Go to the 
Report
 tab
        b) Click on 
New report
        c) Under the 
Administrative 
reports | Select 
Users
        d) Add the New 
Custom Formula field
 in the report
        e) 
Run the Report
 and 
Save.
