### Topic: Detail view buttons in Feed-based page layouts in Lightning Console are larger/look different
When viewing a Feed-based page layout in Lightning Console, the detail view buttons (edit, save, close case, etc.) are larger/look different.
Feed-based page layout buttons (detail view) in Lightning Console:
Non Feed-based page layout buttons (detail view) in Lightning Console:
 
Resolution
This is part of our new look and design of Lightning Console.
Note:
 When viewing Feed based page layout buttons (detail view) outside of Lightning Console, the classic buttons will appear.
Related To:
Winter' 16 release notes "Introducing the Lightning Service Console
