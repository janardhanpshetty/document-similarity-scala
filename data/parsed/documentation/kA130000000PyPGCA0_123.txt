### Topic: Price Book List Views and Detail Pages | Salesforce
Price Book List Views and Detail Pages | Salesforce
Price Book List Views and Detail Pages
The price book list view and price book detail pages offer access to different features for managing price books or price book entries.
Available in: 
Salesforce Classic
 and 
Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view price books:
“Read” on price books
AND
“Read” on products
Your price books can be accessed from multiple locations within Salesforce. Each location provides access to different features.
The 
price book list view
 is accessible on the Price Books page. From the list view, you can manage entries in your price books. You can also create custom list views for your price books, or filter or sort existing list views.
The 
price book detail page
 is also accessible on the Price Books page. From the details page, you can edit the price book’s properties, including its name, description, or state; create or duplicate entire price books; or edit price book entries.
See Also:
Products, Price Books, and Schedules Overview
Add Products to Price Books
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=pricebooks_view.htm&language=en_US
Release
198.17
