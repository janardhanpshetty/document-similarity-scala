### Topic: An error occurred when attempting to parse HtmlEmailBody content for HTML content: "Email is a canceled send."
An error occurred when attempting to parse HtmlEmailBody content for HTML content:
"Email is a canceled send."
Resolution
Check for any <![CDATA[ tags and if they are null.
Dreamweaver will sometimes null out the  tag when paired with design view.
