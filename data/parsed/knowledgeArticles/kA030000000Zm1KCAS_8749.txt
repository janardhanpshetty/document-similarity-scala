### Topic: Users are unable to add an email from the Side Panel by clicking on the small envelope icon next to a record. Side Panel displays an error message in red and envelope never turns green. Nothing happens.
ISSUE
Users are unable to add an email from the Side Panel by clicking on the small envelope icon next to a record. Side Panel displays an error message in red and would try to add the email but then it stops and the envelope never turns green. 
 
After clicking on the envelope you may see an error like this
 
The screen shot above is from version 2.4.2
 
This error maybe different with different versions of SFO
 
CAUSE
The 
Name
, 
Comment 
and
 Due Date
 fields of the 
Task 
Object are not visible for the affected user's profile and the FLS (Field Level Security) is set to Hidden
Resolution
To fix this issue you need to ask your Salesforce Administrator to:
1- Login to Salesforce as Administrator and click on 
Setup | Customize | Activities | Tasks Fields
2- Click on 
Name
 under the Field Label column then click on 
Set Field Level Security
 
3- You will see that the field is not visible for the affected user's profile
4- You need repeat the steps in STEP 3 above for the "
Comment
" and "
Due Date
" fields as well
5- Also if you click on New Task from the affected user's 
Home 
tab, you probably wont see the 
Name, Comment
 and 
Due Date
 fields
6- After you make this change, close and reopen SFO by right clicking on the SFO system tray icon at the bottom right corner of the screen and click on Exit and run it from the Desktop and try to reproduce the issue
