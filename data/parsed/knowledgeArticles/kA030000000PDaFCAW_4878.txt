### Topic: If your browser's popup blocker settings are configured for maximum security, you will not be able to view any popup windows within Salesforce.
Why can't I view Salesforce pop-up windows?
Resolution
If your browser's popup blocker settings are configured for maximum security, you will not be able to view any popup windows within Salesforce. This includes all popup windows, even those that provide necessary functionality such as the calendar pop-up for choosing a date on an activity, confirmations prior to saving or deleting, lookup dialogs for selecting a record, the Help & Training window, and more.
To allow pop-up windows for Salesforce, add salesforce.com as a trusted site within your browser's popup blocker settings.
You can also add the following Wild Cards for sites that you may receive a pop-up from Salesforce:  
*.salesforce.com
*.force.com
*.content.force.com
*.staticforce.com
Also you may be prompted at the top of your browser when a pop-up attempts to populate.  When this takes place select the option to "Allow Pop-ups From This Site"
Otherwise follow the instructions bellow to add the domains above so that you will receive pop-ups from Salesforce.
For Internet Explorer:
1. Log in to Salesforce.
2. Choose: Tools | Pop-up Blocker | Pop-up Blocker Settings.
3. Add one at a time the sites listed above.
For Mozilla Firefox:
1. Choose: Tools | Options | Web Features.
2. Click "Allowed Sites" next to Block Pop-up Windows.
3. Enter "salesforce.com", etc.
4. Click "Allow" and then click "OK" to finish.
V.26 or Higher
1. Click on Firefox button on the upper right > Options > Options
2. Select the Content Panel.
3. If "Block pop-up windows" is ticked, click on the Exceptions button.
4. Type 
salesforce.com
 in the "Address of website:" field, click on Allow, then click close.
5. Click OK on the Options window. Restart Mozilla Firefox
For Google Chrome
1. Click the Chrome menu on the browser toolbar
2. Select 
Settings
.
3. Click 
Show advanced settings
.
4. In the "Privacy" section, click the 
Content settings
 button.
5. In the "Pop-ups" section, click 
Manage exceptions
.
For Netscape:
1. Log in to Salesforce.
2. Click the site controls icon (a checkmark on a green shield) in the tab bar.
3. Select the "Trust Settings" tab and select "I Trust This Site".
4. Click "Done".
For Safari for Mac:
1. Click Safari>Preferences
2. Click on Security
3. Uncheck the box "Block Pop-up windows" to disable the feature
Some browser add-ons, like the Google toolbar, also have popup blocking. Consult your software documentation on those products for details on how to configure them to allow popups from Salesforce.
NOTE: A pop up is not generated if a Due date is not selected for the activity. If all browser issues are ruled out this might potentially be the issue.
