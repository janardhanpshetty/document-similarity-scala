### Topic: Unable to assign Contacts when the Case is created through Email-to-Case.
The default behavior of Email-to-Case (E2C) is to create Cases and associate them with a Contact if the sender's email address exists on one Contact in the Organization. However, if the sender's email exists on multiple Contacts the automatic association will not happen as the process cannot determine which one to attach the newly created case to. In this case, the Case will need to be manually associated.
Good to know:
 If the Contact Name field is not writable on the Case object for the contextual user of the Agent, the expected default association will fail.  The contextual user is the user whose credentials are running the agent, based on the details supplied in sfdcConfig.txt.
 
Resolution
Ensure the Contact association works for the E2C Agent
 
Check that either the Contact Name 
field
 is not "Read Only" for the User running the Agent for the Record Type of the Cases being created by the Agent or the Running User's 
profile
 has the "Edit Read only Fields" permission checked.
Note: 
These permissions aren't needed for "On Demand" Email-to-Case as it works in the context of the Automated Case User (ACU) that has a modified routine to bypass any issues.
