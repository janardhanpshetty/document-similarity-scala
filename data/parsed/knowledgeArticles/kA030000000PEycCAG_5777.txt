### Topic: Sometimes it is necessary to change page layout assignments for profiles like External Who which is normally not visible to the user, this will detail how. NOTE - This information is provided as a courtesy to our customers and Salesforce Support does not typically support in any way URL Changes (HACKS)
Sometimes it becomes necessary to change the page layout assignment for user profiles that are normally hidden due to default assignments being set to installed packages that need to be uninstalled or due to a page layout that needs to be deleted that was assigned to a hidden profile like External Who.
Resolution
NOTE - This information is provided as a courtesy to our customers and Salesforce Support does not typically support in any way URL Changes (HACKS) 
To make these changes do the following:
1. Get the profile id of the hidden profile, typically by viewing it through an error link that says it is in use for example: https://na7.salesforce.com/00eA0000000RhWn
 
2. Go the edit page layout assignment page for the affected object, in this example Campaigns: https://na7.salesforce.com/_ui/common/config/layout2/LayoutMappingUI/e?type=Campaign&e=1&pageNum=0&pid=00eA0000000RhWn&
 
3. Replace the <profileID> part with the hidden profile value and hit enter: https://na7.salesforce.com/_ui/common/config/layout2/LayoutMappingUI/e?type=Campaign&e=1&pageNum=0&pid=<profileID>&
 
The page will reload and you will not see the profile listed but, you should see that x number of cells are selected, typically equal to the number of record types.  At that point use the dropdown to choose a new layout and hit save to make the changes to that profile.  Repeat the process above with any other profiles you might need to change.
