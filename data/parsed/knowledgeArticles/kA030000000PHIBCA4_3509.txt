### Topic: This article describes about the API Call basics and delete()
Deleting One or More Records from Organization's Data
Resolution
API Call Basics
API calls represent specific operations that your client applications can invoke at runtime to perform tasks, for example:
1)Query data in your organization.
2)Add, update, and delete data.
3)Obtain metadata about your data.
4)Run utilities to perform administration tasks.
 
Characteristics of API Calls
 
All API calls are:
1)
Service Requests and Responses
—Your client application prepares and submits a service request to the Force.com Web Service via the API, the Force.com Web Service processes the request and returns a response, and the client application handles the response.
2)
Synchronous
—Once the API call is invoked, your client application waits until it receives a response from the service. Asynchronous calls are not supported.
3)
Committed Automatically Versus Rollback on Error
—By default, every operation that writes to a Salesforce object is committed automatically. This is analogous to the AUTOCOMMMIT setting in SQL. For create(), update(), and delete() calls that attempt to write to multiple records for an object, the write operation for each record is treated as a separate transaction. For example, if a client application attempts to create two new accounts, they’re created using mutually exclusive insert operations that succeed or fail individually, not as a group.
Note:
For API version 20.0 and later, there is an AllOrNoneHeader header that allows a call to roll back all changes unless all records are processed successfully. This header is supported by the create(), delete(), undelete(), update(), and upsert() calls.
 
Factors that Affect Data Access
When using the API, the following factors affect access to your organization’s data:
Access
>>Your organization must be enabled for API access.
 
>>Objects may not be available until you contact salesforce.com and request access. For example Territory is visible only if territory management has been enabled in the application. Such requirements are in the “Usage” section for each object.
 
>>Sometimes a feature must be used once before objects related to it can be accessed with the API. For example, the recordTypeId is available only after at least one record type has been created for your organization in the user interface.
 
>>To investigate data access issues, you can start by inspecting the WSDL:
               
Enterprise WSDL:
 The generated enterprise WSDL file contains all of the objects that are available to your organization. By using the API, a client application can access objects that are defined in your enterprise WSDL file.
               
Partner WSDL:
 When using the generated partner WSDL file, a client application can access objects that are returned in the describeGlobal() call.
 
 
Object-Level and Field-Level Security
The API respects object-level and field-level security configured in the user interface. You can access objects and fields only if the logged-in user's permissions and access settings allow such access. For example, fields that are not visible to a given user are not returned in a query() or describeSObjects() call. Similarly, read-only fields can't be updated.
 
User Permissions
 
A user attempting to access the API must have the permission “API Enabled” selected. It’s selected by default.
Your client application logs in as a user called a logged-in user. The logged-in user's permissions grant or deny access to specific objects and fields in your organization:
Read—Users can only view objects of this type.
Create—Users can read and create objects of this type.
Edit—Users can read and update objects of this type.
Delete—Users can read, edit, and delete objects of this type.
 
User permissions do not affect field-level security. If field-level security specifies that a field is hidden, users with “Read” on that object can view only those fields that are not hidden on the record. In addition, users with “Read” on an object can view only those records that sharing settings allow. The one exception is the “Edit Read Only Fields” permission, which gives users the ability to edit fields marked as read only via field-level security.
Sharing
For most API calls, data that is outside of the logged-in user’s sharing model is not returned. Users are granted the most permissive access that is available to them, either through organization-wide defaults or manual record sharing, just as in the application.
User Permissions that Override Sharing
View All—Users can view all records associated with this object, regardless of sharing settings.
Modify All—Users can read, edit, delete, transfer, and approve all records associated with this object, regardless of sharing settings.
Modify All Data—users can read, edit, delete, transfer, and approve all records regardless of sharing settings. This permission is not an object-level permission, unlike “View All” and “Modify All.”
 
To protect the security of your data, give the logged-in user only the permissions needed to successfully execute all the calls made by the application. For large integration applications, “Modify All Data” may speed up call response times. If you are loading a large number of records, use the Bulk API instead.
 
Related Objects
 
>>Some objects depend on other objects for permission. For example, AccountTeamMember follows sharing on the associated permission-assigned object such as the Account record. Similarly, a Partner depends on the permissions in the associated .
>>Ownership changes to a record do not automatically cascade to related records. For example, if ownership changes for a given Account, ownership does not then automatically change for any Contract associated with that Account—each ownership change must be made separately and explicitly by the client application.
 
Object Properties
 
>>To create an object with the create() call, the object's createable attribute must be set to true. To determine what operations are allowed on a given object, your client application can invoke the describeSObjects() call on the object and inspect the properties in the DescribeSObjectResult. Note
replicatable allowsgetUpdated() and getDeleted() calls.
Page Layouts and Record Types
 
Requirements defined in the Salesforce user interface for page layouts and record types are not enforced by the API:
>>Page layouts can specify whether a given field is required, but the API does not enforce such layout-specific field restrictions or validations in create() and update() calls. It’s up to the client application to enforce any such constraints, if applicable.
 
>>Record types can control which picklist values can be chosen in a given record and which page layouts users with different profiles can see. However, such rules that are configured and enforced in the user interface are not enforced in the API. For example, the API does not validate whether the value in a picklist field is allowed per any record type restrictions associated with the profile of the logged-in user. Similarly, the API does not prevent a client application from adding data to a particular field simply because that field does not appear in a layout associated with the profile of the logged-in user
Referential Integrity
To ensure referential integrity, the API forces or prevents certain behaviors:
>>ID values in reference fields are validated in create() and update() calls.
>>If a client application deletes a record, then its children are automatically deleted as part of the call if the cascadeDelete property on ChildRelationship for that child has a value of true. For example, if a client application deletes an Opportunity, then any associated OpportunityLineItem records are also deleted. However, if an OpportunityLineItem is not deletable or is currently being used, then deletion of the parent Opportunity fails. For example, if a client application deletes an Invoice_Statement, then any associated Line_Item records are also deleted. However, if a Line_Item is not deletable or is currently being used, then deletion of the parent Invoice_Statement fails. Use DescribeSObjectResult to view the ChildRelationship value if you want to be sure what will be deleted.
>>There are certain exceptions that prevent the execution of a cascadeDelete. For example, you can't delete an account if it has associated cases, if it has related opportunities that are owned by other users, or if associated contacts are enabled for the Customer Portal. In addition, if you attempt to delete an account that has closed/won opportunities owned by you or has active contracts, then the delete request for that record will fail.
To Delete one or more records from your organization’s data.
 
Syntax
DeleteResult[] = connection.delete(ID[] ids);
Usage
Use delete() to delete one or more existing records, such as individual accounts or contacts, in your organization’s data. The delete() call is analogous to the DELETE statement in SQL.
Consider the following when deleting Objects:
 
>>
When deleting objects, Your client application must be logged in with sufficient access rights to delete individual objects within the specified object. For more information, see Factors that Affect Data Access.
>>
In addition, you might also need permission to access this object’s parent object. For special access requirements, see the object’s description in Standard Objects.
>>
To ensure referential integrity, the delete() call supports cascading deletions. If you delete a parent object, you delete its children automatically, as long as each child object can be deleted. For example, if you delete a Case, the API automatically deletes any CaseComment, CaseHistory, and CaseSolution objects associated with that case. However, if a CaseComment is not deletable or is currently being used, then the delete() call on the parent Case will fail.
 
>>
Certain objects cannot be deleted via the API. To delete an object via the delete() call, its object must be configured as deletable (deletable is true) . To determine whether a given object can be deleted, your client application can invoke the describeSObjects() call on the object and inspect its deletable property.
>>
You can't delete records for multiple object types in one call if one of those types is related to a feature in the Setup area in Salesforce. The only exceptions are the following objects:
                               
>
 Custom settings objects, which are similar to custom objects.
                               
>
GroupMember
                               
>
Group
                               
>
User
 
Rollback on Error
 
The AllOrNoneHeader header allows you to roll back all changes unless all records are processed successfully. This header is available in API version 20.0 and later. The default behavior is to allow partial success of a call: records without errors are committed, while records with errors are marked as failed in the call results.
 
Basic Steps for Deleting Records
Deleting records involves the following basic steps:
>>
Determine the ID of each record that you want to delete. For example, you might call query() to retrieve a set of records that you want to delete based on specific criteria.
>>
Construct an ID[] array and populate it with the IDs of each record that you want to delete. You can specify the IDs of different types of objects in the same call. For example, you could specify the ID for an individual Account and an individual Contact in the same array. For information on IDs, see ID Field Type.
>>
Call delete(), passing in the ID[] array.
Process the results in the DeleteResult[] to verify whether the records have been successfully deleted.
 
