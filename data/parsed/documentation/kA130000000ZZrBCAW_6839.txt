### Topic: Install Salesforce Files Sync | Salesforce
Install Salesforce Files Sync | Salesforce
Install Salesforce Files Sync
Install the Salesforce Files Sync client on your desktop to sync files between your computer, Salesforce, and Salesforce1 on mobile devices.
From your personal settings, enter 
Files Sync
 in the 
Quick Find
 box, then select 
Salesforce Files Sync
. Multiple results? Select the one under Personal Setup.
Click 
Download
.
Follow the installation instructions for your operating system.
Log in with your Salesforce credentials.
After Salesforce Files Sync is installed, you’re notified automatically when an update is available.
Note
Salesforce Files Sync client must run on a local drive. External locations such as network drives or mounted devices are not supported.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=files_install.htm&language=en_US
Release
202.14
