### Topic: This article will explain the Dashboard limitations on the Home Page Dashboard Section.
Why is it not possible to add more than three Dashboard sections on my Salesforce Homepage?
Resolution
At this time, customers are only able to put up to three dashboards components on their Homepage at a time. 
As a workaround customers can customize their Homepage by selecting a different dashboards to display on their Homepage via the following click path: Name > Setup > My Personal Information > Change My Display.
However, if the customer would like to add multiple dashboards to the Homepage they can vote on this idea on the IdeaExchange:
https://success.salesforce.com/ideaView?id=08730000000g5DWAAY
 
