### Topic: The web server URL used as the SOAP service endpoint seen in Enterprise WSDL in an org with managed packages contains an ID (called "VersionSet") that identifies which managed package versions will be available in API calls using said web server URL.
In the Enterprise WSDL there is an 0DF id required in the SOAP service endpoint. Is this required when consuming and using the Enterprise WSDL?
Enterprise WSDL 
<!-- Soap Service Endpoint -->
<service name="SforceService">
<documentation>Sforce SOAP API</documentation>
<port binding="tns:SoapBinding" name="Soap">      
<soap:address location="https://login.salesforce.com/services/Soap/c/36.0/
0DF90000000DLDv
"/> 
</port>
</service>
Resolution
a) 0DF is the key prefix of VersionSet, an internal object.
b) Every time a managed package is installed a VersionSet record is created. 
c) When generating the WSDL you select which version of the managed packages to include in the WSDL and you'll see this in the URL:
ver_<namespace1>=<version1>&ver_<namespace2>=<version2>&ver_<namespace3>=<version3>&...
d) This allows you to generate a WSDL that includes sObjects, fields, etc. from the provided versions of the corresponding managed packages.
e) To understand the implications, imagine you have installed 2 packages with namespaces "geopointe", and "loop", and versions 12.15, and 9.95, respectively. You could get your Enterprise WSDL including these two packages by navigating to:
https://<instance>.salesforce.com/soap/wsdl.jsp?type=*&ver_geopointe=12.15&ver_Loop=9.92
=> WebServerURL = https://login.salesforce.com/services/Soap/c/36.0/0DF360000008Rc3
=> VersionSet = 0DF360000008Rc3
and you could get the WSDL including "loop" specific features by going to 
https://<instance>.salesforce.com/soap/wsdl.jsp?type=*&ver_Loop=9.92
=> VersionSet = 0DF360000008Rc8
=> WebServerURL = https://login.salesforce.com/services/Soap/c/36.0/0DF360000008Rc8
f) Now, the geopointe package adds a custom field on Accounts called, geopointe__Geocode__c, so if you use the second WebServerURL, you won't be able to get access to this field (e.g. a describe call wouldn't return the field geopointe__Geocode__c).
