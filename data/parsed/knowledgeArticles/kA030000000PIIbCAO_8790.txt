### Topic: You may need to reset your password if you have forgotten your password and cannot log in to Buddy Media, you know your current password but would like to set a new one, or your password has expired and you must set a new one as a security precaution.
You can reset your password on the Buddy Media login page or within Buddy Media when you are already logged in. Resetting your password is helpful if you have forgotten your password or if you need to change it for security purposes.
Resolution
You may need to reset your password if:
You have forgotten your password and cannot log in to Buddy Media
You know your current password, but would like to set a new one
Your password has expired. As a security precaution, every Buddy Media user must change their password every 90 days.
Note: You cannot reset your password following the steps below if you have not first set up a password through the Buddy Media Welcome Email. If your Welcome Email confirmation link expired prior to setting up a password, please contact your Channel Admin or Marketing Cloud Support to have a new 
Welcome Email sent to you.
 
There are two places to change your Buddy Media password:
Reset from the login page
Reset when logged into Buddy Media
 
Reset your password from the login page
1. Go to your Buddy Media channel.
This is typically  https://[
yourcompanyname
].buddymedia.com.
2. Click 
Reset Password? 
on the login page.
3. Enter your email address and click 
Submit
. 
A confirmation message will confirm that the password reset email has been sent.
4.  Click the link contained in the email to go to the password reset page in your browser.
Note: The email will come from Buddy Media (marketingcloudsupport@salesforce.com) with the subject line "Details on how to change your [Channel Name] password". 
5.  Enter your new password and re-enter that new password again to confirm it.
The password must be a minimum of 8 characters long and contain 3 of the 4 of the following: capital letter, lower case letter, number, and symbol. Your password cannot be one of your most recent 6 passwords. If your new password does not meet these requirements, a warning message will display and you will be unable to save your new password. 
6. Click 
Save
. 
7. Click on the hyperlinked text 
login 
in the confirmation to be redirected back to your channel to log in. 
 
Reset your password from within the Buddy Media
Note: You must know your current password in order to change your password from within the Buddy Media Suite.
1. Go to your Buddy Media channel.
This is typically https://[
yourcompanyname
].buddymedia.com. 
2. Log in using your current username and password.
Your username is typically your email address.
3. Click 
[
Your Name
] 
|
 My Account
.
4. Click 
Basic Info
. 
5. Under the heading 
Password
, enter your current password, new password, and re-enter your new password to confirm it.  
The password must be a minimum of 8 characters long and contain 3 of the 4 of the following: capital letter, lower case letter, number, and symbol. Your password cannot be one of your most recent 6 passwords. If your new password does not meet these requirements, a warning message will display and you will be unable to save your new password.
6. Click 
Save
. 
A notification message will appear confirming that the update was successfully saved.
 
