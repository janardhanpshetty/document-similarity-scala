### Topic: Event merge fields is only limited to workflow (event object) email alerts
Why does Mail merge fields for Events not populate when sending it through "Send an email", Test and verify merge fields or Mass emails?
Resolution
Merge Fields for Events only populates if the email template is used under an email alert for a workflow created under the Events Object. 
Creating Email Templates: 
Creating HTML Email Templates 
Creating Text Email Templates
=======================
Sample Workflow to test your Event merge field
Setup | Workflow and Approvals | Workflow Rules | New Rule
Step 1: Select object = Event
Step 2: Configure Workflow Rule = fill out the fields
ex.
Evaluation Criteria = created, and every time it’s edited
Rule Criteria
Field = Evet : Created Date 
Operator = not equal to 
Value = null or just leave it blank
Step 3. Click Save & Next
Then 
Under Immediate Workflow Actions
1. Select New Email Alert
2. Type in a Description
3. Select the template
4. Select recipient
5. Save
==========================
 
