### Topic: This article explains the linking between Partner / Customer Communities and compares this to the normal functionality
Linking the chatter feeds between partner/customer communities with the Internal chatter feed.
Resolution
Currently, there is not a way to link chatter feeds from a community with the Internal Community. Therefore, partner users and customer users are only able to see chatter feed information from the community that they are accessing and they will not be able to view the Internal chatter feed, as they are not a Full Salesforce user with the Salesforce license.
