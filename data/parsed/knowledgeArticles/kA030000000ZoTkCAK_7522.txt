### Topic: With Summer 15' users can edit existing chatter posts however, this feature is not available for all types of posts.
Users may notice that they do not have option to edit specific chatter posts while they are able to edit other posts.
Resolution
The Edit option isn’t available on posts with link or file attachments and system-generated posts like feed tracked record updates. For community users, feed post editing is only available if the community was created using Salesforce Tabs + Visualforce or the Napili template.
Please consider promoting the Idea, 
Ability to edit a file or link chatter post after posting
 if you would like to see the ability to edit file or link posts added with a future release of Salesforce.
See Also:
Edit Feed Posts and Comments
