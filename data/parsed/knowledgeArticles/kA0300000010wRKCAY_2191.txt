### Topic: Weekly Knowledge Update table with information about #KBFeedback that resulted on a new version of the Article in CRM Config.
Update - 
3rd of June of 2016
#KBFeedback
 that resulted on New Articles from the 
13th of May until the 3rd of June.
See how to 
submit 
feedback on existing Articles / Documentation
.
See full post in the 
Support - 'CRM Config' Skill Group
.
Archive of previous posts: 
https://org62.my.salesforce.com/kA0300000010wTG
Resolution
Article
Link
Feedback by...
How to Create/Edit Key fields in Lightning Experience?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-to-Create-Edit-Key-fields-in-Lightning-Experience&language=en_U
Aldrin Reyes
Not able to add custom field in process from process builder    
https://help.salesforce.com/apex/HTViewSolution?urlname=Not-able-to-add-custom-field-in-process-from-process-builder&language=en_US
Aldrin Reyes
INTERNAL: Duplicate Management not available in instances upgraded to Spring 15   
https://org62.my.salesforce.com/kA030000000xnimCAA
Amanda Stam
User profile permission descriptions    
https://help.salesforce.com/apex/HTViewSolution?urlname=What-does-each-permission-on-a-user-s-profile-mean&language=en_US
Amit Kumar
Enabling Create Audit Fields for Professional Edition    
https://help.salesforce.com/apex/HTViewSolution?urlname=Enabling-Create-Audit-Fields-for-Professional-Edition&language=en_US
Angelica May Montañez
External community portal user can't see contact information    
https://help.salesforce.com/apex/HTViewSolution?urlname=External-Community-Portal-User-Cannot-see-Contact-Information&language=en_US
Archie Pangalinan
Contact fields on Case not visible in Lightning    
https://help.salesforce.com/apex/HTViewSolution?urlname=Contact-fields-on-Case-not-visible-in-Lightning&language=en_US
Archie Pangalinan
How to resolve the "undefined" error when syncing Quote to Opportunity    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-to-resolve-the-undefined-error-when-syncing-Quote-to-Opportunity&language=en_US
Ardy Dacanay
Why does the Lead History report not display results?    
https://help.salesforce.com/apex/HTViewSolution?urlname=Why-does-the-Lead-History-report-not-display-results&language=en_US
Ardy Dacanay
How to Identify Leads Created by Web-To-Lead    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-to-Identify-Leads-Created-by-Web-To-Lead&language=en_US
Ardy Dacanay
How do I associate an object and its related 'Notes' since they do not appear in the same file?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-do-I-associate-an-object-and-its-related-Notes-since-they-do-not-appear-in-the-same-file-1327109115588&language=en_US
Ardy Dacanay
Top Salesforce questions answered    
https://help.salesforce.com/apex/HTViewSolution?urlname=Top-Getting-Started-Articles-by-Trending-Issue&language=en_US
Ardy Dacanay
How do I change the name of our org in Salesforce?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-do-I-change-the-name-of-our-org-in-Salesforce&language=en_US
Ardy Dacanay
How do negative values display in a currency field?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-do-negative-values-display-in-a-currency-field&language=en_US
Ardy Dacanay
Can I edit a contract after it has been activated?    
https://help.salesforce.com/apex/HTViewSolution?urlname=Can-I-edit-a-contract-after-it-has-been-activated-1327109094716&language=en_US
Barry Greenlees
Process Builder: Error: Unfortunately, there was a problem. for a deployed Process    
https://help.salesforce.com/apex/HTViewSolution?urlname=Process-Builder-Error-Unfortunately-there-was-a-problem-for-a-deployed-Process&language=en_US
Charlene Grace Buenafe
Enable Shared Contacts    
https://help.salesforce.com/apex/HTViewSolution?urlname=Enable-Shared-Contacts&language=en_US
Darrick Williams
Converting Lead to an existing Contact    
https://help.salesforce.com/apex/HTViewSolution?urlname=Converting-Lead-to-an-existing-Contact&language=en_US
Eric Van Horssen
Is there a limit on the number of characters for the "Specify From Address(es)" values on Case Feed layouts?    
https://help.salesforce.com/apex/HTViewSolution?urlname=Is-there-a-limit-on-the-number-of-characters-for-the-Specify-From-Address-es-values-on-Case-Feed-layouts&language=en_US
Eric Van Horssen
How do I create custom fields for tasks or events?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-do-I-create-custom-fields-for-tasks-or-events&language=en_US
Fernando Caamano
Where are the various places within Salesforce where a User can be set as a Default User?    
https://help.salesforce.com/apex/HTViewSolution?urlname=Where-are-the-various-places-within-Salesforce-where-a-User-can-be-set-as-a-Default-User&language=en_US
Irene Ibanez
User can't add product to an opportunity - error: insufficient privileges?    
https://help.salesforce.com/apex/HTViewSolution?urlname=User-can-t-add-product-to-an-opportunity-error-insufficient-privileges-1327108679728&language=en_US
Irene Ibanez
How do I add Campaign Members based on Opportunity info?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-do-I-add-Campaign-Members-based-on-Opportunity-info-1327108292623&language=en_US
Jerome Espejo
Unable to change logo on Communities login page    
https://help.salesforce.com/apex/HTViewSolution?urlname=Unable-to-change-logo-on-Communities-login-page&language=en_US
Jerome Espejo
How can I map the Lead Description standard field to Description fields on other objects?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-can-I-map-the-Lead-Description-standard-field-to-Description-fields-on-other-objects-1327109097794&language=en_US
Joanne Orila
INTERNAL: Live Agent chat button is not appearing on the customer's website    
https://org62.my.salesforce.com/kA030000000pCUWCA2
Manmeet Singh
Process Builder cannot access Contact fields on a Person Account record when making a spanning reference from another object    
https://help.salesforce.com/apex/HTViewSolution?urlname=Process-Builder-cannot-access-Contact-fields-on-a-Person-Account-record-when-making-a-spanning-reference-from-another-object&language=en_US
Mariel Valera
Why can't I edit some fields with inline editing?    
https://help.salesforce.com/apex/HTViewSolution?urlname=Why-can-t-I-edit-some-fields-with-inline-editing&language=en_US
Poorva Shrivastava
Blank or Underscore values when generating Mail Merge documents. Duplicate or Doubled values when generating Mail Merge Documents.    
https://help.salesforce.com/apex/HTViewSolution?urlname=Blank-or-Underscore-values-when-generating-Mail-Merge-documents&language=en_US
Poorva Shrivastava
Milestones not auto-removing when criteria no longer met    
https://help.salesforce.com/apex/HTViewSolution?urlname=Milestones-not-auto-removing-when-criteria-no-longer-met&language=en_US
Robert Bannerman
Display Roll-Up Summary Currency Fields on Accounts and Opportunities with Advanced Currency Management enabled    
https://help.salesforce.com/apex/HTViewSolution?urlname=Display-Roll-Up-Summary-Currency-Fields-on-Accounts-and-Opportunities-with-Advanced-Currency-Management-enabled&language=en_US
Saurabh Sharma
How Are Chatter Free Impacted By User Sharing?    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-Are-Chatter-Free-Impacted-By-User-Sharing&language=en_US
Sherry Lee Gondra
How can I report on the change in User License Type    
https://help.salesforce.com/apex/HTViewSolution?urlname=How-can-I-report-on-the-change-in-User-License-Type&language=en_US
Sherry Lee Gondra
Why do I receive an error running the Macro saying that the action is not available?    
https://help.salesforce.com/apex/HTViewSolution?urlname=Why-do-I-receive-an-error-running-the-Macro-saying-that-the-action-is-not-available&language=en_US
Shyrla Mae Garcia
Prepare Your Org to Continue Using Salesforce for Outlook After TLS 1.0 Is Disabled    
https://help.salesforce.com/apex/HTViewSolution?urlname=Prepare-Your-Org-to-Continue-Using-Salesforce-for-Outlook-After-TLS-1-0-Is-Disabled&language=en_US
Sohan Rawat
