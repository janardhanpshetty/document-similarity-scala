### Topic: Describes troubleshooting steps for the Dataflow error "No fields specified. Aborting." in Wave Analytics
A Wave Analytics Dataflow may fail with the following error message:
Error executing node 
sfdcDigest_Node
: No fields specified. Aborting.
This indicates that the specified 
sfdcDigest_Node
 does not include any fields.
Resolution
Retrieve the dataflow JSON
1. Gear Icon > Data Monitor
2. Change view to Dataflow View
3. Click the dropdown options on the right side and select Download
4. Open the resulting file in your favorite JSON editor
Review and correct the specified node
The 
sfdcDigest_Node
 will appear similar to this:
 
"
sfdcDigest_Node
": {
    "action": "sfdcDigest",
    "parameters": {
      "fields": [ 
      ],
 
If the node is necessary, you can include additional fields in the node.
If the node is unnecessary, you can remove it from the dataflow definition file.
Either way, be sure to update any related nodes as needed.
Upload the corrected Dataflow Definition
1. Gear Icon > Data Monitor 
2. Change view to Dataflow View 
3. Click the dropdown options on the right side and select Upload
4. Locate and select the modified definition file
