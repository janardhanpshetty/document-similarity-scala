### Topic: Identity Connect SSO login button does not show on Salesforce domain login screen.
This article assumes Identity Connect SSO (Single Sign On) has already been configured and verified to be working via the Identity Connect SSO (Single Sign On) url (https://hostname:port/connect). Please refer to the Salesforce Identity Connect Implementation Guide for additional information on the setup and configuration of Identity Connect SSO (Single Sign On)
 
Once Identity Connect SSO (Single Sign On) has been configured when going to the Salesforce domain set up for a Salesforce Org for example (https://mydomain.my.salesforce.com/) a button will display at the bottom of the screen to login via Identity Connect SSO (Single Sign On). When this button is clicked it brings the user to the Identity Connect SSO (Single Sign On) login page (https://hostname:port/connect).
 
 
 
This article goes over the scenario where the Identity Connect button described above does not show on the login screen for a clients custom domain.
Resolution
Login to Salesforce Org where the custom domain was configured as a Salesforce Administrator and navigate to Setup>>Domain Management>>My Domain. Once the "My Domain" page loads scroll down to the section titled "Login Page Branding" and click edit. Next to the "Authentication Service" section there are two check boxes Login Page and IdentityConnect. Ensure the IdentityConnect checkbox is checked if not check it and Save. Checking this box enables the Identity Connect SSO (Single Sign On) button to show on the domain login screen.
 
