### Topic: When customers upgrade from Professional Edition to Enterprise Edition there are rare cases where some permissions or settings might not get updated or might not behave as expected.
List of Possible Discrepancies When Upgrading from PE to EE
When customers upgrade from Professional Edition to Enterprise Edition there are rare cases where some permissions or settings might not get updated or might not behave as expected. Here is a list of known issues from past upgrades:
Enabling Organization Admins Can Login as Any User
 not being enabled
Field Level Security being set to "Hidden" on all fields
Standard Forecast Categories being removed
Rename Standard Labels - BT permission deselected
Custom Labels - BT permission deselected
Mass Mail Recipients Limit - BT limit not being increased from 250 to 500
Duplicate Management might not be activated
To resolve these issues, you will need to open cases with support. Some of these are known issues with specific fixes as identified below:
Field Level Security being set to "Hidden" on all fields - See 
INTERNAL: Upgrade from PE to EE sets FLS on all fields to 'hidden' in some rare cases
 for instructions.
Standard Forecast Categories being removed - See 
INTERNAL ONLY - Customer upgraded from PE to EE and now is missing standard Forecast Categories
 for instructions.
Rename Standard Labels BT permission deselected - Open case with support to have permission enabled.
Custom Labels BT permission deselected - Open case with support to have permission enabled.
Massmail Recipients Limit BT limit not being increased from 250 to 500 - Open case with support to have permission enabled. Reference 
this
 document for new limit.
Resolution
