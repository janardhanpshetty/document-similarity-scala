### Topic: INTERNAL ONLY knowledge article that summarizes the technology involved in the CVM Supplier Central - Diversity Edition
AppExchange Listing Information:
 
Partner:
  
CVM Solutions (PARTNER MAIN)
Application Name: 
 CVM Supplier Central - Diversity Edition
 
**AppExchange Listing Name / URL: 
CVM Supplier Central Diversity Edition
 
https://appexchange.salesforce.com/listingDetail?listingId=a0N30000003IfTwEAK
 
**Partner Success Team: 
Segun De Silva (SAM) / Manishi Singh (TE) / Brent Floyd  (AE)
 
**PPS Entitlement:  
https://org62.my.salesforce.com/a0a30000000t4ze
 
 
Application Overview:
 
Combined Product Description:
 
CVM Supplier Central™ Diversity Edition addresses supplier diversity management needs with a comprehensive solution empowering supplier diversity managers to create and improve diversity program results, drive adoption and achieve corporate objectives. 
 
Systematically monitor and manage diverse supplier certification status 
Access the largest and most accurate supplier database in the industry 
Proactively identify opportunities for increased diversity spend in the organization 
CVM Supplier Central™ Diversity Edition addresses supplier diversity management needs with a comprehensive solution empowering supplier diversity managers to create and improve diversity program results, drive adoption and achieve corporate objectives. CVM Solutions expands the relevance and influence of your supplier diversity program by reducing your time spent on administrative tasks so that your organization can focus on value-added activities.
 
Key benefits:
Systematically monitor and manage diverse supplier certification status
Access the largest and most accurate supplier database in the industry
Proactively identify opportunities for increased diversity spend in the organization
 
Distribution Details:
Target Market Segment: 
GB (Large business); ENTR (Enterprise -- 3,500+ employees)
Sold to: Net-new customers
Distribution Mechanism: D
irect Sales Force. No channels today.
 
Target User Demographic:
 (pulled from licensing - user subscription) related list in ISV TE org 
Supplier Diversity Manager
 -
 Create and improve diversity program results, drive adoption and achieve corporate objectives. Systematically monitor and manage diverse supplier certification status
 
Partner Portal (Supplier) - 
On-board and register as a certified  "diversity" supplier with CVM to enable exposure to CVMs clientelle needing to identify minority owned businesses to include in their supply chain
 
Administrator- 
This new license is being requested that will be used to satisfy a dependency created in the CVM org that requires a single  Customer Portal license to be activated in each org.
 
 
Application Technology Overview:
 
Marketecture Diagram:
 
 
Solution Components:
Standard Objects Referenced: 
Accounts, Contacts, Users
Number of Custom Objects: 100+
Number of Reports: 
41-50
Number of Classes: 100+
Number of Triggers: 41-50
Number of Visualforce Pages: 76-100
ISVforce technology leveraged: 
Managed Package; Aloha; Push Upgrades; LMA; Trialforce; Branded Login
Is Force.com primary system of record: Yes
Other platforms used: 
Has managed package: Yes
Has composite Web App: No
Has native mobile app?: No
100% native on Force.com Platform No
Type of Application: 
95% Native. Couple of components are off platform.
Supported Editions: 
Enterprise Edition; Unlimited Edition
Approved for install into existing orgs? Yes
Has Extension Packages? No 
Has Composite Web Services?: Yes 
Has Desktop App: No
 
UI Architecture:
Supported UI Types: Desktop Browser-Based
Mobile Devices Supported - N/A 
Developer tools used for browser UI: 
Visualforce/Apex; JS frameworks (JQuery, ExtJS, etc.)
% of app not using SFDC standard look / feel: 30%
 
Integration Methods:
Integration at UI layer: No
UI Integration Methods: NA
Realtime integration required: Yes
Realtime integration methods: 
Inbound
Avg number of API calls in 24 hour period: Hundreds
Batch Integration Required: Yes 
Batch Integration Frequency: 
Once a day; Once a month
Avg number of records per batch: Thousands
Batch ETL Methods: 
Custom code hosted by ISV; Custom code hosted by SFDC (Apex Batch)
 
Large Data Concerns:
More than 20MB of data per user: Yes 
Largest data set stored on single object: Millions
Describe queries run on large data sets: NA
Reporting on large data sets with SFDC tools? No
Data Volume Concerns? 
None. The millions of records are for reporting purposes only which they generate on VF pages.
Sample UI Screenshots:
Resolution
