### Topic: Case account name will not change when changing the related contact, if the user making the change has access to Case: Account Name field
When a case is related to a contact (via the standard 'Contact Name' field ) the account name is automatically populated. However, when a User has Edit access to the Case
:
 Account Name field, any subsequent changes to the related contact will not automatically change the related account. The related account would need to be manually changed instead.
Resolution
Alternatively, you could set up the Process on the Case object as described below to automatically populate account associated with the contact under Account name field on case record:
To Create Process: Go | Setup | Create | Workflow & Approvals | Process Builder | New
1.Create the Process on
 Case
 object .
2.Start the process
:
 When a record is created & edited.
3.Criteria for Executing Actions
:
 Conditions are met.
4.Set conditions:
[Case].ContactId(Field) IsNull (operator) Boolean (Type) False (Type)
[Case].ContactId(Field) Ischanged(operator) Boolean (Type) True (Type)
5.Conditions
:
 All the Conditions are met (AND)
6.Select Immediate Actions.
7.Action Type
:
 Update Records
8.Record Type
:
 Select the Case record that started your process
9.Criteria for updating records
:
 No Criteria-Just update the records!.
10.Set new field values for the records you update
:
Account ID (Field) Reference (Type) [Case].Contact.AccountId (Value)
11.Save the Process.
 
