### Topic: Information on how to start a Work.com demo/test org.
Internal Only:
 Do not share with customers
If the customer is looking to start a demo/test org, advised him/her to create a new developer org via the following link > 
https://developer.salesforce.com
.
Once he/she signs up for the Dev org, she/he can follow the 
Quick Start Admin Guide
 to set Work.com.
Here is also a link to a 14 days Work.com free trial that the Sales team shared with us. However, it is preferred to use the first alternative > 
https://www.salesforce.com/form/signup/freetrial-workdotcom-02.jsp?fromEmail=1
 
 
Resolution
