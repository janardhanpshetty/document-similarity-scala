### Topic: The only standard field that is currently visible from the Campaign related list in Salesforce1 (SF1) is the Status. You can modify this behavior with custom formula fields and adjusting your related list fields within the page layout.
Currently in Salesforce1 when you view the related list Campaign Members will display only the Status field as seen below.
NOTE: If you have a custom fields on Campaign Members that are on the Campaign Members related list they will show up as well. We will be using this specific behavior as a workaround shown below.
 
Resolution
You can modify this by adjusting your related list page layout on Campaigns within your organization and creating formula fields on Campaign Members to pull the details you want to see.
Note: The related list will only show the first 4 related list items that it can display within Salesforce1.
1. Below is an example Formula you can use in a custom formula field that pulls the First and Last Name of a Lead or Contact associated to the Campaign Member and places a space between them:
IF(ISBLANK(Contact.LastName),Lead.FirstName + " " + Lead.LastName,Contact.FirstName + " " + Contact.LastName)
Phone number or email can also be pulled using this formula field format.
IF(ISBLANK(Contact.Phone), Lead.Phone, Contact.Phone)
2. Now that you have the fields available you will need to put them on the related list and order them how you want them in reference to the status field. The screenshot below shows how it would look from the Campaign Member related list in the full site.  (For this example the fields that start with Member are our custom fields).
3. Once this is complete your related lists will look something similar to the following.
Note: This workaround is provided to assist with the current behavior of Campaign Members related list in Salesforce1. Should this behavior change in a future release, you will be able to remove these changes as needed.
 
