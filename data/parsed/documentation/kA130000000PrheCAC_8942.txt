### Topic: Why isn’t Data Loader importing special characters? | Salesforce
Why isn’t Data Loader importing special characters? | Salesforce
Why isn’t Data Loader importing special characters?
If Data Loader fails to import special characters such as ö, ñ, or é, your source data file might not be properly encoded. To ensure the file is properly encoded:
Make any modifications to your source data file in .xls format.
In Microsoft® Excel®, save a copy of your file as a Unicode Text file.
Open the Unicode Text file you just saved with a text editor.
Click 
File
 | 
Save As
 to change the following file settings:
File name extension—
.csv
Save as type—
All Files
Encoding—
UTF-8
Click 
Save
, and close the file.
Note
Don’t open the file after you have saved the settings or you may revert the encoding changes.
Import the data using Data Loader as you normally would, and select the newly created .csv file.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_import_dataloader_specialchars.htm&language=en_US
Release
202.14
