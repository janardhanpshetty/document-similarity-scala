### Topic: Compare Salesforce for Outlook with Connect for Outlook | Salesforce
Compare Salesforce for Outlook with Connect for Outlook | Salesforce
Compare Salesforce for Outlook with Connect for Outlook
Now that we’ve retired 
Connect for Outlook
, we’re focusing development efforts on our latest Microsoft® email integration products, Email Connect and
Salesforce for Outlook
. Compare the features offered by 
Connect for Outlook
 with our most established email integration product,
Salesforce for Outlook
, to see how your sales reps can increase their productivity when they upgrade.
Salesforce for Outlook
 and Email Connect are available to manage from: both 
Salesforce Classic
 and 
Lightning Experience
Email Connect is available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Salesforce for Outlook
 is available in: 
All
 Editions
Important
Connect for Outlook
 was retired in Winter ‘16. This means the product no longer saves your emails or syncs your contacts, events, and tasks between Microsoft® Outlook® and 
Salesforce
. But don’t worry! You’ll be even more productive when you uninstall 
Connect for Outlook
 and migrate to one of our latest email integration features, Email Connect or 
Salesforce for Outlook
. Talk to your 
Salesforce
 administrator to learn which feature is compatible with your environment.
I want to know
Salesforce for Outlook
Connect for Outlook
What syncs
Lets users choose sync methods—users either can sync all their Outlook items to 
Salesforce
, or only the items they select. If users choose to sync all Outlook items, 
Salesforce for Outlook
 syncs it all, except the items users:
Mark as Private in Outlook, and exclude them from syncing in 
Salesforce for Outlook
 Settings
Assign to the Outlook category 
Don’t Sync with 
Salesforce
Required users to select the contacts, events, and tasks users want to sync.
How it syncs
Automatically syncs users’ data. Users can also manually sync at any time.
Required users to manually sync. Users may be configured to automatically sync.
Whether users can work with 
Salesforce
 records directly in Outlook
Includes the 
Salesforce
 Side Panel. When users select an email in Outlook, the side panel:
Displays up to 10 related 
Salesforce
 contacts and leads, including up to four activities, cases, and opportunities related to them.
Lets users search for additional 
Salesforce
 records.
Includes links to records, so users can view them directly in 
Salesforce
.
See it in action: 
Using the 
Salesforce
 Side Panel to Work with Records in Microsoft® Outlook®
Didn’t have anything similar.
Whether users can create 
Salesforce
 records directly in Outlook
Lets you create 
Salesforce
 records, like accounts, cases, contacts, leads, opportunities, and custom object records directly from the 
Salesforce
 Side Panel.
Didn’t have anything similar.
How Outlook contacts, events, and tasks are added to 
Salesforce
Associates contacts, single events, and single tasks to related 
Salesforce
 records automatically.
To associate recurring events and recurring tasks, you’ll go to My Unresolved Items, and select the related records.
Required users to specify related Outlook records for each contact, event, and task users are syncing.
How Outlook emails are added to 
Salesforce
Lets you add emails to multiple 
Salesforce
 contacts, and to one other record with which you can associate tasks, such an account, a lead, or a case. You’ll use the 
Salesforce
 Side Panel to do this.
Let users specify related Outlook records for each email users are sending.
Where administration settings are managed
You manage settings or your users in 
Salesforce
. You can grant your users permissions to customize some settings, or prevent them from changing others.
Settings were managed by users in Outlook.
Are You Interested Yet?
We hope so! Your former 
Connect for Outlook
 users can be even more productive when they migrate to 
Salesforce for Outlook
 or Email Connect. We provide resources to help you and your reps migrate to the product that best fits your needs.
See Also:
How Salesforce for Outlook Syncs Between Microsoft® Outlook® and Salesforce
Getting Microsoft® Outlook® and Salesforce in Sync
Connect for Outlook End of Life Frequently Asked Questions
How do I uninstall Connect for Outlook?
Which Microsoft® Email Integration Product Is Right for My Company?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=outlookcrm_vs_cfo.htm&language=en_US
Release
198.17
