### Topic: Prepare to Convert State and Country Data | Salesforce
Prepare to Convert State and Country Data | Salesforce
Prepare to Convert State and Country Data
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions except Database.com
If your Salesforce organization includes text-based state and country values, you can convert that data to standardized picklist values. Converting existing data allows you to keep working with the data after you switch to picklists. Say, for example, you have a report that culls all of your sales reps’ leads in Washington state, and the report is generated from state picklist value Washington. To ensure that records with text-based state values such as Wash., WA, and Washington are included in the report, convert text-based state data to standardized picklist values.
Converting existing state and country text data into standardized picklist values helps ensure data integrity after you enable picklists in your organization. Your users encounter validation errors when saving records that contain state or country values not in your picklists. Also, reports become unreliable when records created before you enable state and country picklists contain different state and country values than records created using picklists.
When you convert data, Salesforce starts with countries, then goes on to states. As you go through the conversion process, here are a few things to keep in mind:
Save frequently. You can exit the conversion tool and return to it at any time.
You can continue to work normally in your organization while converting data.
You can’t convert data while you’re scanning for affected data and customizations, or while state or country picklists are being deployed.
Steps can be repeated and undone at any time until you enable the picklists for users. After the picklists are enabled, you can’t undo the conversion.
If you use Data.com Clean, we recommend that you suspend Clean jobs until the conversion is finished.
See Also:
Convert State and Country Data
State and Country Picklists
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_state_country_picklists_convert_overview.htm&language=en_US
Release
202.14
