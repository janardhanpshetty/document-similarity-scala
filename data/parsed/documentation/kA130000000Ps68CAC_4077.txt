### Topic: Publishing Extensions to Managed Packages | Salesforce
Publishing Extensions to Managed Packages | Salesforce
Publishing Extensions to Managed Packages
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create packages:
“Create AppExchange Packages”
To upload packages:
“Upload AppExchange Packages”
An 
extension
 is any package, component, or set of components that adds to the functionality of a managed package. An extension requires that the base managed package be installed in the organization. For example, if you have built a recruiting app, an extension to this app might include a component for performing background checks on candidates.
The community of developers, users, and visionaries building and publishing apps on Force.com AppExchange is part of what makes Force.com such a rich development platform. Use this community to build extensions to other apps and encourage them to build extensions to your apps.
To publish extensions to a managed package:
Install the base package in the Salesforce organization that you plan to use to upload the extension.
Build your extension components.
Note
To build an extension, install the base package and include a dependency to that base package in your package. The extension attribute will automatically become active.
Create a new package and add your extension components. Salesforce automatically includes some related components. 
Upload the new package that contains the extension components.
Proceed with the publishing process as usual. For information on creating a test drive or registering and publishing your app, go to 
http://sites.force.com/appexchange/publisherHome
.
Note
Packages cannot be upgraded to Managed - Beta if they are used within the same organization as an extension.
See Also:
Prepare Your Apps for Distribution
Understanding Dependencies
Manage Versions
Publish Upgrades to Managed Packages
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=publish_extensions.htm&language=en_US
Release
202.14
