### Topic: Find and View Records and Data | Salesforce
Find and View Records and Data | Salesforce
Find and View Records and Data
Use tabs, search, or lists to look at your data.
Available in: Salesforce Classic
Available in: All Editions
When you’re working with records, it’s important to remember that the types of records you can create, view, edit, and delete are determined by administrator settings, such as a user profile or permission set. Your access to individual records may be determined by other configurations, such as sharing settings. And your access to fields on types of records may be controlled by field-level security. Work with your administrator to make sure you have access to the records and data you need.
There are a few easy ways to find and view your records.
From a tab, such as Accounts or Contacts, start from the default view and click 
Go!
 or select a different view. 
The resulting page you see is called a 
list view
.
Search for a record using keywords, such as a name or address stored in the record. Use the search bar in the banner displayed on most pages.
On many records, below the main page sections, look for related lists, which identify records that are associated with the record you’re currently viewing. For example, an account record probably has a related list of contacts at that account.
Granting Access to Records
You can use manual sharing to give specific other users access to certain types of records, including accounts, contacts, and leads. In some cases, granting access to one record includes access to all its associated records.
Editing or Deleting Record Access
Edit or remove access to a record.
Viewing Which Users Have Access to Your Records
After you have granted access to a record you own, you can view a list of users who have access to the record and its related information and records, including their access level and an explanation. The list shows every user who has access that’s greater than the org-wide default settings.
Record Access Levels
List Views
List views give you quick access to your important records. You can find preconfigured views, such as a list of recently viewed records, for every standard and custom object. Create customized list views to display records that meet your own criteria. Scroll through preconfigured views and list views that you created, and use list view charts to see a graphical display of your list view data.
How does the sharing model work?
Which kinds of records can I print list views for?
Open Items You’ve Recently Viewed
In the Recent Items section of the Salesforce sidebar, you’ll find a list of up to 10 items (records, documents, custom objects, and the like) you’ve most recently added, edited, or viewed.
See Also:
Salesforce Pages
Get to Know Your Salesforce Admin
Sharing Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=basics_viewing_data.htm&language=en_US
Release
202.14
