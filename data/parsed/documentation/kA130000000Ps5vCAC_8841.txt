### Topic: Data.com Clean | Salesforce
Data.com Clean | Salesforce
Data.com Clean
Data.com Clean provides a number of ways to keep your Salesforce CRM records up to date by leveraging company information from D&B and millions of crowd-sourced contacts. There are two versions of the Data.com Clean product: Corporate Clean and Premium Clean. They differ based on the D&B fields that are available when you clean account or lead records.
Available in: Salesforce Classic
Available with a Data.com Clean license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Clean compares your Salesforce account, contact, and lead records with records from Data.com and creates a link between your Salesforce record and its matching Data.com record. Clean also provides 
clean status information for Salesforce accounts, contacts, and leads
.
Users with a Clean license can:
Manually compare individual Salesforce records side by side with matched Data.com records, and update Salesforce records field by field.
Select account, contact, and lead records from a list, and clean them all at once.
Manually refresh D&B Company records linked to accounts (Premium Clean only).
Your organization
 can also:
Configure and run automated Clean jobs to flag field value differences on Salesforce records, fill blank fields, overwrite field values.
Use the 
Data.com Match API
 to create a custom solution for specific business purposes like cleaning leads as they are created and cleaning custom objects.
Data.com Clean does not support person accounts.
There are two versions of Data.com Clean: Corporate Clean and Premium Clean. They differ based on the D&B fields that are available when you clean account or lead records, either manually or via automated jobs.
Data.com Clean Version
D&B Fields
Data.com Corporate Clean
Basic set of D&B fields.
Data.com Premium Clean
Basic set of D&B fields, plus the
 D&B Company
 field. This field links to an associated D&B Company record with over 70 more D&B fields that are accessible on the D&B Companies tab. If the Salesforce records’s 
D&B Company
 field is blank, a link is created, if available, when the record is cleaned.
Note
Prospector or Clean Corporate licenses can’t be purchased for organizations that already have Prospector or Clean Premium licenses. If you want to purchase Corporate licenses, you’ll need to convert your Premium licenses.
Prospector or Clean Premium licenses can’t be purchased for organizations that already have Prospector or Clean Corporate licenses. If you want to purchase Premium licenses, you’ll need to convert your Corporate licenses.
Data.com Clean must be purchased 1:1 for Sales Cloud and Service Cloud seats.
Keep Your Salesforce Records Complete and Up-to-Date
Your business is only as good as its data. Spend less time updating data and more time growing your business, using Data.com Clean. Maximize valuable company information from D&B and millions of crowd-sourced contacts to keep your Salesforce data clean and up to date.
See Also:
Using Data.com FAQ
Clean Individual Salesforce Records
Clean a List of Salesforce Records
Set Up Data.com Clean
Confirm That Data.com Clean Is Enabled
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_clean_overview.htm&language=en_US
Release
202.14
