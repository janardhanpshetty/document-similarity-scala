### Topic: One or more users in the account receive a teal-colored error screen indicating a 500 Internal Server Error. At the bottom of the screen, it mentions Stackato.
One or more users in the account receive a teal-colored error screen indicating a 500 Internal Server Error. At the bottom of the screen, it mentions Stackato.
Resolution
Stackato is a third-party platform which hosts the Journey Builder application.  
This error is returned when the Stackato server is unavailable. 
To resolve the issue, submit a formal escalation of the case to Product Operations, and request that the Stackato server be restarted.  Include in the request the stack and database upon which the customer's account resides.
Add a post to the 
Global Support Parent Cases
 chatter group in NA7, including all required information specified by the group's template.  This is located in the group's description field.
