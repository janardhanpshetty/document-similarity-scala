### Topic: This is the process to get nominated for the Customer service Pilot programs: Google+, Instagram, LinkedIn, Sina Weibo.
Social Customer Service integrates with Radian6 and Social Studio so service agents and sales representatives can engage customers by responding to cases and leads created from Facebook, Twitter, and other social networks.
 
Resolution
 
Contact your Salesforce account executive 
for more information about joining this pilot. This is only available for organizations that have a paid Radian6 account.
The Social Customer Service Pilot is the name that includes 4 different Pilot programs:
1. Google+
2. Instagram
3. LinkedIn
4. Sina Weibo
Request to be part of any of those pilot programs singularly or all of them or a combination of them.
If you have trouble contacting your Account Executive, in order to have Salesforce.com Support nominate you for the Social Customer Service Pilot programs, take the following steps:
1. Verify that the Organization where you want the feature to be enabled is on an edition where the feature is available.
2. Have a System Administrator submit a case to Salesforce Support.
3. Submit mark as "feature activation" on the General Application Area. 4. On the case description, specify:
Feature requested
: Social Customer Service Pilot: Google+ and/or Instagram and/or Linkedin and/or Sina Weibo (please specify)
Organization ID
 (where you want the feature to be enabled)
:
A statement confirming you're in charge of this feature
:  "I am the system admin in charge of this feature."
Also answer the following questions:
1. Why are you a good fit for this pilot program? 
2. Who is the main contact for this pilot program? (must be available to provide feedback)
3. Org Ids where feature should be enabled? (must be an Enterprise, Unlimited or Performance edition org)
4. Do you have Social Customer Service already configured?
5. Do you have a paid Radian6 contract? (if not, please note that you cannot be accepted)
6. Do you have or want to begin a social customer care practice on Google+, Instagram, LinkedIn, or Sina Weibo?
Once we have all of the requested information, our Support Team will review the case and action as needed.
