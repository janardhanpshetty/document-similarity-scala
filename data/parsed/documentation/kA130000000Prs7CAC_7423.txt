### Topic: Using the Portals Tab | Salesforce
Using the Portals Tab | Salesforce
Using the Portals Tab
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set up the Self-Service portal:
“Manage Self-Service Portal”
To modify Self-Service pages:
“Manage Self-Service Portal”
AND
“Customize Application”
Note
Starting with Spring ’12, the Self-Service portal isn’t available for new orgs. Existing orgs continue to have access to the Self-Service portal.
The Portals Tab is where you set up an online support channel for your Self-Service customers - allowing them to resolve their inquiries without contacting a customer service representative.
Clicking on the Portals tab displays the portals home page. From there, you can:
View your customer Self-Service portal home page.
Click on your Self-Service portal pages to see how your customers will interact with them.
Under 
Reports
, click any report name to jump to that report.
Select any of the links under 
Tools
 to access utilities for managing your Self-Service portal and Self-Service users.
Note
The Portals tab does not include the Customer Portal.
See Also:
Displaying the Portals Tab
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=portals_using.htm&language=en_US
Release
202.14
