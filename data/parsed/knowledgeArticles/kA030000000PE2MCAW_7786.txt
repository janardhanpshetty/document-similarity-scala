### Topic: This article would help you know if Sites can be enabled for a Developer Edition Org.
The Sites functionality can be activated for Developer Edition organizations by the Activations group within Support. The request should be made through the generation of a case.
Resolution
When this feature is activated, two permissions need to be set for the organization.
Custom Domains Enabled should be enabled.
Maximum number of Sites should be increased to 1.
 
