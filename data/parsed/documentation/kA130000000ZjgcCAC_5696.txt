### Topic: Rotate Your Encryption Keys | Salesforce
Rotate Your Encryption Keys | Salesforce
Rotate Your Encryption Keys
You should regularly generate a new tenant secret and archive the previously active one. By controlling the lifecycle of your organization’s tenant secrets, you control the lifecycle of the data encryption keys that are derived from them.
Available as add-on subscription in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions. Requires purchasing Salesforce Shield. Available in 
Developer
 Edition at no charge for organizations created in Summer ’15 and later.
Available in both Salesforce Classic and Lightning Experience.
User Permissions Needed
To manage tenant secrets:
“Manage Encryption Keys”
Consult your organization’s security policies to decide how often to rotate your tenant secret. You can rotate it once every 24 hours in a production organization, and every four hours in a sandbox environment.
The key derivation function itself uses a master secret, which is rotated with each major Salesforce release. This has no impact on your encryption keys or your encrypted data, until you rotate your tenant secret.
To check the status of your organization's keys, go to Setup and use the 
Quick Find
 box to find the Platform Encryption setup page. Keys can be active, archived, or destroyed.
ACTIVE
Can be used to encrypt and decrypt new or existing data.
ARCHIVED
Cannot encrypt new data. Can be used to decrypt data previously encrypted with this key when it was active.
DESTROYED
Cannot encrypt or decrypt data. Data encrypted with this key when it was active can no longer be decrypted. Files and attachments encrypted with this key can no longer be downloaded.
In Setup, use the 
Quick Find
 box to find the Platform Encryption setup page.
Click 
Generate New Tenant Secret
.
If you want to re-encrypt existing field values with a newly generated tenant secret, contact Salesforce support.
Get the data to update by exporting the objects via the API or by running a report that includes the record ID. This triggers the encryption service to encrypt the existing data again using the newest key.
Note
This page is about Shield Platform Encryption, not Classic Encryption.
See Also:
Generate a Tenant Secret
Back Up Your Tenant Secret
Destroy A Tenant Secret
Turn Shield Platform Encryption Off
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_pe_rotate_keys.htm&language=en_US
Release
202.14
