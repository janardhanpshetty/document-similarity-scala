### Topic: Considerations for Installed Flows | Salesforce
Considerations for Installed Flows | Salesforce
Considerations for Installed Flows
Keep these considerations in mind when you distribute, upgrade, or remove a flow that you installed from a package.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
The Cloud Flow Designer can’t open flows that are installed from managed packages.
If you install a package that contains multiple flow versions in a fresh destination organization, only the latest flow version is deployed.
If you install a flow from a managed package, error emails for that flow’s interviews don’t include any details about the individual flow elements. The email is sent to the user who installed the flow.
If you install a flow from an unmanaged package that has the same name but a different version number as a flow in your organization, the newly installed flow becomes the latest version of the existing flow. However, if the packaged flow has the same name and version number as a flow already in your organization, the package install fails. You can’t overwrite a flow.
Status
An active flow in a package is active after it’s installed. The previous active version of the flow in the destination organization is deactivated in favor of the newly installed version. Any in-progress flows based on the now-deactivated version continue to run without interruption but reflect the previous version of the flow.
Distributing Installed Flows
When you create a custom button, link, or Web tab for a flow that’s installed from a managed package, include the namespace in the URL. The URL format is 
/flow/namespace/flowuniquename
.
When you embed a flow that’s installed from a managed package in a Visualforce page, set the name attribute to this format: 
namespace.flowuniquename
.
Upgrading Installed Flows
Upgrading a managed package in your organization installs a new flow version only if there’s a newer flow version from the developer. After several upgrades, you can end up with multiple flow versions.
Removing Installed Flows
You can’t delete a flow from an installed package. To remove a packaged flow from your organization, deactivate it and then uninstall the package.
You can’t delete flow components from Managed - Beta package installations in development organizations.
If you have multiple versions of a flow installed from multiple unmanaged packages, you can’t remove only one version by uninstalling its package. Uninstalling a package—managed or unmanaged—that contains a single version of the flow removes the entire flow, including all versions.
See Also:
Flows in Change Sets and Packages
Considerations for Deploying Flows with Packages
Install a Package
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_considerations_admin_install.htm&language=en_US
Release
202.14
