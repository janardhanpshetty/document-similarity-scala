### Topic: Elements of a Formula | Salesforce
Elements of a Formula | Salesforce
Elements of a Formula
A formula can contain references to the values of fields, operators, functions, literal values, or other formulas.
Use any or all of these elements to build a formula.
Element Name
Description
Literal Value
A text string or number you enter that is not calculated or changed. For example, if you have a value that’s always multiplied by 2% of an amount, your formula would contain the literal value of 2% of that amount:
ROUND((Amount*0.02), 2)
This example contains every possible part of a formula:
A function called ROUND used to return a number rounded to a specified number of decimal places.
A field reference called Amount.
An operator, *, that tells the formula builder to multiply the contents of the 
Amount
 field by the literal value, 0.02.
A literal number, 0.02. Use the decimal value for all percents. To include actual text in your formula, enclose it in quotes.
The last number 2 in this formula is the input required for the ROUND function that determines the number of decimal places to return.
Field Reference
Reference the value of another custom or standard field using a merge field. The syntax for a merge field is 
field_name
 for a standard field or 
field_name__c
 for a custom field. The syntax for a merge field on a related object is 
object_name__r.field_name
. Use the 
Insert Field
 button or the drop-down list to insert a merge field in your formula where necessary.
Function
A system-defined formula that can require input from you and returns a value or values. For example, 
TODAY()
 does not require input but returns the current date. The 
TEXT(value)
 function requires your percent, number, or currency input and returns text.
Operator
A symbol that specifies the type of calculation to perform or the order in which to do it. For example, the + symbol specifies two values should be added. The open and close parentheses specify which expressions you want evaluated first.
Comment
An annotation within a formula that begins with a forward slash followed by an asterisk (
/*
). and concludes with an asterisk followed by a forward slash (
*/
). For example,
/*This is a formula comment*/
Comments are ignored when processing a formula.
Comments are useful for explaining specific parts of a formula to anyone viewing the formula definition. For example:
AND( 
/*competitor field is required, check to see if field is empty */
LEN(Competitor__c) = 0, 
/* rule only enforced for ABCD record types */
RecordType.Name = "ABCD Value",
/* checking for any closed status, allows for additional closed picklist values in the future */
CONTAINS(TEXT(StageName), "Closed") 
)
You can also use comments to 
comment out
 sections of your formula when debugging and checking the syntax to locate errors in the formula.
Note
Nesting comments causes a syntax error. For example, you cannot save a formula that has the following:
/* /* comment */ */
Commenting out a whole formula causes a syntax error.
Comments count against the character and byte size limits in formulas.
See Also:
Build a Formula Field
Quick Reference Guide: Formula Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=elements_of_a_formula.htm&language=en_US
Release
202.14
