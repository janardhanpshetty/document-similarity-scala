### Topic: Compact Layouts | Salesforce
Compact Layouts | Salesforce
Compact Layouts
Compact layouts are used in Salesforce1 and Lightning Experience to display a record’s key fields at a glance.
Available in: Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
In the full Salesforce site, compact layouts determine which fields appear in the Chatter feed item that appears after a user creates a record with a quick action.
Note
To avoid inadvertent sharing of information through the feed, the Task page layout determines the fields displayed in the Chatter feed items for tasks created using a quick action.
In Salesforce1, the first four fields that you assign to a compact layout appear in:
An object’s record highlights area
Expanded lookup cards on a record’s related information page
In Lightning Experience, the first seven fields that you add to a compact layout display in an object’s record highlights panel.
Note
If a user doesn’t have access to one of the fields that you assign to a compact layout, the next field on the layout is used.
If you don’t create custom compact layouts for an object, all the object’s record highlight fields, preview cards, and action-related feed items are driven by a read-only, system default compact layout that contains a predefined set of fields. After you create one or more custom compact layouts, you can set one as the primary compact layout for the object. The primary compact layout is then used as the default for that object.
Primary compact layouts determine which fields are shown in Chatter personal digest emails.
If you have record types associated with an object, you can override the primary compact layout assignment and assign specific compact layouts to different record types.
Compact layouts support all field types except:
text area
long text area
rich text area
multi-select picklist
See Also:
Create Compact Layouts
Notes on Compact Layouts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=compact_layout_overview.htm&language=en_US
Release
202.14
