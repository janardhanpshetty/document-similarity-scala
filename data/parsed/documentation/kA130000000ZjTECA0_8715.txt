### Topic: Bulk Sync Guidelines | Salesforce
Bulk Sync Guidelines | Salesforce
Bulk Sync Guidelines
A bulk sync triggers the copying of any record whose object type is published and subscribed, making the record available in both organizations. Once a record is copied in a bulk sync, any future updates to it are synced.
Available in: Salesforce Classic
Organization Sync is available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
When should I perform a bulk sync?
When you complete the publish-and-subscribe process as part of the Organization Sync setup, the secondary organization doesn’t yet contain any records. Perform an initial bulk sync to quickly populate the secondary organization with records so users can access their data during downtime. In addition, perform a bulk sync whenever you publish and subscribe to new objects.
How do I select a cutoff date on the Bulk Sync page?
A bulk sync triggers the copying of any records that were created or modified after a certain date within the last year. Select this date on the Bulk Sync page. For the initial bulk sync, consider syncing your records in groups based on date to minimize the amount of data being processed at once.
For example, you might first perform a bulk sync with a cutoff date of three months ago, and then perform a second bulk sync with a start date of six months ago. Grouping records doesn’t take additional time because the system doesn’t recopy records that have already been synced.
How do I monitor the progress of a bulk sync?
View the total number of records remaining to be synced in the Organization Sync Record Queue on your Organization Sync connection’s detail page. While the sync is in progress, the 
Bulk Sync
 button is disabled. Logging out of Salesforce won’t affect the sync, and when the sync completes, the 
Bulk Sync
 button is clickable again.
Can a bulk sync fail?
If a record included in a bulk sync has an issue—for instance, an invalid lookup—that record and its child records are not copied, but records that are unrelated to failed records will still be copied in the bulk sync. Replication failures are logged in the Organization Sync Log, while metadata-related failures are logged in the Connection History log.
How long does a bulk sync take?
The length of a bulk sync depends on the number of records being copied. A bulk sync may take hours or days.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=SA_bulk_sync_guidelines.htm&language=en_US
Release
202.14
