### Topic: Malformed HTML elements on a page can block records from loading in a list view
Issue:
Sometimes when a particular user clicks on a list view for an object, such as accounts or opportunities, they will notice that no records load, and the screen will just say "loading". This may happen for some users, and not for other users, which can be confusing for a sys admin. Typically this will happen for all list views, so will clearly not be a filter issue
There are a couple of reasons why this can occur, (including browser issues). One of the less common reasons, though, is malformed HTML elements on the page blocking the records from loading. This is especially likely if one user can view the list view correctly and another can't
A malformed HTML component is basically a piece of HTML that has been added to the page by a Salesforce customer, where the HTML coding is not correct. The most common example is when a HTML area is added to the sidebar for a home page layout (as the sidebar can be visible on all pages).
Resolution
Answer:
If you have some users that can load records correctly in a list view and some that cannot:
1. Note the profile of a user who can load list views correctly, and the profile of a user who cannot load the list views
2. Open the set-up menu and navigate to "Customise -> Home -> Home page layouts"
3. Click the "Page layout assignment" button and click "edit assignment"
4. Change the home page layout for the profile of the user who cannot load the list views, to that assigned to the profile of the user who can load them correctly
5. Click save, then ask the user to check again if they can see the records correctly
If no users can load records correctly in a list view
1. Note the profile of any user who definitely cannot load the list views - we will use this for testing
2. Open the set-up menu and navigate to "Customise -> Home -> Home page layouts"
3. Click the "Page layout assignment" button and check which page layout is assigned to this profile"
4. Click the back button in your browser to return to the list of home page layouts. Click to open the layout that was assigned to the profile we are using for testing
5. Click "Edit" and remove all custom components from the "Select Narrow Components to show" section
6. Ask the user to test the page - if the list view records now load correctly add the custom components back to the layout one-by-one until they stop loading. This is the component that is causing the problem, and should be replaced or removed
What if this doesn't work?
If the cause is not in the sidebar then there are other areas that can cause this. Check if you are using a Visualforce page, as there may be a HTML element in the VF causing an issue. (Look at the address bar to see the URL for the page, if it includes the word "apex" you are probably on a VF page). Also check the page layout to see if there is a Visualforce component included as part of the page layout.
 
