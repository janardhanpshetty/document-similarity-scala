### Topic: This Article details how Organizations can remove Salesforce1 access for all Users.
Due to compliance guidelines or security protocols, administrators may be asked to prevent all User from accessing the Salesforce1 platform. To completely eliminate your Users' ability to access the Salesforce1 interface, you will need to make the following changes:
Resolution
1. Go to Setup| Manage Apps| Connected Apps. From here, click Edit next to the 'Salesforce1 for iOS' Connected App. On the resulting screen, ensure that the 'Permitted Users' value is set to 'Admin approved Users are pre-authorized', and click Save.
You will then want to complete this for the 'Salesforce1/Chatter for Android' and 'Salesforce1 for Windows' Connected Apps as well.
2. Go to Setup| Manage Users| Profiles and edit the User's Profile to ensure that the 'Salesforce1/Chatter for Android', 'Salesforce1 for iOS', and 'Salesforce1 for Windows' permissions are all disabled under the Connected App Access section. Once all of these have been disabled, save the changes for the Profile and repeat these steps for all Profiles in your organization. 
3. To ensure that your Users are not able to access the browser based version of Salesforce1, you will want to go to Setup| Mobile Administration| Salesforce1| Settings. From here, ensure that the 'Enable the Salesforce1 mobile browser app' option is unchecked, and click Save. 
Once all of these steps have been completed, your Users will no longer be able to access the Salesforce1 interface through their mobile device. Please note that this does not restrict their ability to log in through the full site interface in their mobile browser. To eliminate full site access as well, you will need to implement IP restrictions for the organization. For more details on IP restrictions, please see 
Set Trusted IP Ranges for Your Organization
.
