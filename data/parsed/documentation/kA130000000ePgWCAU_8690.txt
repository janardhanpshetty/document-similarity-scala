### Topic: Customize Opportunities and Products | Salesforce
Customize Opportunities and Products | Salesforce
Customize Opportunities and Products
Get the most out of opportunities and products by setting up these additional features.
Available in: Salesforce Classic and Lightning Experience
Available in: 
All
 Editions for organizations activated before Summer ’09
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions for organizations activated after Summer ’09
Enable and Configure Similar Opportunities
Allow users to find Closed/Won opportunities that match the attributes of an opportunity they're currently working on, so they can quickly access information that might help them close their open deals.
Make It Easy to Add Products to Opportunities
Set up Salesforce to prompt your users to add products when they create an opportunity. When you select 
Prompt users to add products to opportunities
 in Opportunity Settings, the label for the save button in a new opportunity record is “Save & Add Product.”
Enable Opportunity Update Reminders
Updated and accurate opportunities drive precise forecasts. Enabling update reminders lets managers send their direct reports automated emails with a report on the team’s open opportunities.
Activate and Deactivate Product Prices with Their Products Simultaneously
Set up Salesforce so that when you activate or deactivate a product, all related prices for that product are activated or deactivated at the same time.
Restrict Price and Quantity Editing on Opportunity Products
Control whether users can update the 
Price
 and 
Quantity 
fields on opportunities.
Activate Big Deal Alerts
Your organization can use alerts that automatically send an email notification for opportunities with large amounts. You can activate one opportunity alert for your organization. The alert message resembles the opportunity detail page including the page layout and language from a selected user.
Customize Big Deal Alerts
Your organization can use alerts that automatically send an email notification for opportunities with large amounts. Customize this alert to send an email when an opportunity reaches a threshold.
See Also:
Opportunities
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=opp_admin_task_map.htm&language=en_US
Release
202.14
