### Topic: Log a call in case feed has different options then Log a Call in Activity feed. What does the "Log a Call" in Case Feed do?
Log a call in case feed has different options then Log a Call in Activity feed.
The differences:
On the standard page, the "Log a Call" button is a standard button on the page layout that is in the 'Activity History' related list for standard and custom objects.
More details on the "Log a Call" button on 'Activity History' can be found here: 
https://developer.salesforce.com/docs/atlas.en-us.case_feed_dev.meta/case_feed_dev/case_feed_dev_guide_log_call_publisher.htm
I would like to know what Log a Call in the case feed (details) does for my user.
Resolution
The Log a Call action lets support agents record notes and information about customer calls. You can add a quick note and it is by default assigned to the creator of the logged call. 
The "schedule a follow up" task is not available via the case feed "log a call". However, it is available in the regular "log a call" button on the detail view but not on the case feed. 
Note:
 If you would like to customize this option you can utilize a Visual Force page more details can be found here: 
https://developer.salesforce.com/docs/atlas.en-us.case_feed_dev.meta/case_feed_dev/case_feed_dev_guide_log_call_publisher.htm 
More details on the Case Feed and Related Lists can be found here: 
https://help.salesforce.com/HTViewHelpDoc?id=case_interaction_related_lists.htm&language=en_US 
