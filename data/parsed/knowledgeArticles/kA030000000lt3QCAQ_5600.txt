### Topic: The article will explain How to run Remote Connectivity Analyzer on the Exchange Sever. Remote Connectivity Analyzer to test Exchange Sync Parameters using Microsoft
1. Visit Microsoft’s website and search for the Remote Connectivity Analyzer or go to: 
https://testconnectivity.microsoft.com
2. On the Exchange Servertab, select the Service Account Access (Developers).
3. Click Next.
4. Complete the test fields:
Target mailbox email address
 -- specify
 t
he Exchange email address of an Exchange
Service Account User Name (Domain\User Name or UPN)
 -- specify
 t
he service account domain\user name or 
UPN you’ve set up for Exchange Sync
Service Account Password
 --  specify t
he service account password you’ve set up for Exchange Sync
5. Select
 Use Autodiscover
 to detect server settings
6. Select 
Test predefined folder.
7. Choose 
Contacts
 if you’d like to test sync connectivity for this user’s contacts, or Calendar for this user’s events.
8. Check
 Use Exchange Impersonation
.
9. In the Impersonated User field, t
ype the same Exchange email address you specified for Target mailbox email
address.
10. For Impersonated user identifier, select 
SMTP Address
.
11. Check the I understand that I must use the credentials of a working account...acknowledgment.
12. Type the analyzer verification code and click verify.
13. At the top of the page, click Perform Test.
Running the test usually takes less than 30 seconds. When the test is complete, the analyzer displays a summary evaluating the sync connectivity for the Exchange user you’ve specified.
14. Take action based on the test results.
If the test summary displays “Connectivity Test Successful” or “Connectivity Test Successful with Warnings,” it’s likely that you’ve set up your service account correctly, and you’re ready to provide your service account credentials to Salesforce.
If the test summary displays “Connectivity Test Failed,” review the test details to troubleshoot setup issues. You might want to return to instructions on how to set up Exchange Sync correctly.
If you still are unable to set up Exchange Sync, or if you complete setup successfully but your users are still unable to sync their records, run the test again, and save the test details as HTML. You can provide these details to Salesforce Customer Support to evaluate your setup if you need to file a case.
Resolution
