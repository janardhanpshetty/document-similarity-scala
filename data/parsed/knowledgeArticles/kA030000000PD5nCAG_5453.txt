### Topic: This is a supplementary article to the known issue found here: https://success.salesforce.com/issues_view?id=a1p30000000SupPAAS. This article will be removed from publication when the known issue is finally addressed/fixed.
This is a supplementary article to the known issue found here 
The import wizard fails with a generic error
.
 
Customers using the import wizard may encounter a generic error when importing a file even though the data has been correctly formatted. These types of errors can occur in the old version of the Salesforce Data Import Wizard.
 
Resolution
The 
Unified Data Import Wizard
, introduced with Spring '14, is more robust than the previous import tools and won't encounter these types of generic errors.  Look for it in Setup at Data Management | Data Import Wizard.  The old separate versions of the Import Wizard remain available though they will eventually be retired.  Customers should use the new Import Wizard to perform all future import jobs.  
Below are some tips on addressing common errors when using the import wizard to help ensure your file is properly formatted.
 
Unique fields
: Find out whether you have unique fields in the object in question and if so, make sure that the import file does not contain a value that already belongs to an existing record in salesforce.
Required fields
: Find out whether there are required fields in the object in question and if so, make sure that they are included in the import file. You might also see a "Data already exists" error message when some required fields are not included.
State and Country Picklist
: If this feature is enabled, the customer 
must 
import values that are predefined in the State and Country Picklist configuration page. If the import file contains States that are not yet in Salesforce, the import will fail. You will need to either deactivate this feature or make sure that the import file only contains States that are already created in salesforce.
Empty rows
: Make sure that the import file doesn't have any empty rows (before the last row of the file).
Split the file
: You can try splitting the file in smaller files (fewer records per file) and try to import them separately.
Validation rules
: You can also deactivate validation rules on the object in question if you are not sure whether a validation rule is evaluating to true. This would however, affect the data quality.
With the Unified data import Wizard you can review your import jobs by accessing the
 
Bulk Data Load Jobs
 section as outlined in 
Determine which records are imported using Data Import Wizard
.
Note:
 The Monitoring Bulk Data Load Jobs setup section is not available in all 
Salesforce Editions
 see 
What Editions Have API Access?
 for more details.
In addition to the Import Wizard and depending on your edition, other import tools may be available to you.
Data Loader
Third party apps from the Appexchange
