### Topic: Adding a Form to the Page | Salesforce
Adding a Form to the Page | Salesforce
Adding a Form to the Page
Use forms to collect data from your site visitors and submit the data to standard or custom Salesforce objects. Create web-to-lead forms, capture customer details, or gather feedback on your products or services.
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To add a form to the page:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned in Site.com Studio
To edit the guest user profile:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned in Site.com Studio
AND
“Manage Users”
AND
“Customize Application”
To add a form to a page:
Drag a 
Form
 from the Page Elements pane onto the page.
Select the Salesforce object that you want to submit data to. 
Note
For Site.com users, the drop-down list only displays objects that are available to guest users because site visitors access your public site via the Guest User license. To make other objects available, go to the 
guest user profile
, enable the relevant object's “Create” permission, and refresh the list.
For Communities users, the drop-down list displays objects that may not be available to site visitors. For authenticated visitors, object access on public and private pages is controlled by their user profiles. For unauthenticated visitors, object access on public pages is controlled by the site’s guest user profile.
Add available fields to the form by double-clicking a field, or selecting it and clicking 
. 
All required fields are automatically added to the list of selected fields. However, you can 
hide required fields
 after you add the form to the page.
Reorder the list of selected fields by clicking 
Move Up
 or 
Move Down
.
Click 
Save
.
Note
When adding forms to authenticated community pages in Site.com, set the current user for Salesforce objects that require the Owner ID field. Setting the current user (as opposed to the default guest user) lets you identify the authenticated user when the form is submitted. To set the current user for the Owner ID field, select the field in the form, click 
Configure
 under Field Properties in the Properties pane, select 
Global Property
 as the source, and select 
Current userID
 as the value.
After you add a form to the page, you can’t change the object it’s connected to. If you need to connect to a different object, you must replace the form.
You can use the form’s Properties pane to:
See which object the form is connected to.
Add a title to the top of the form.
Specify what occurs 
when a user successfully submits the form
.
Change the appearance of the form by 
selecting a different theme
.
See Also:
Adding Input Fields to Forms or Pages
Input Field Types
Editing Input Fields in a Form
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_data_form.htm&language=en_US
Release
202.14
