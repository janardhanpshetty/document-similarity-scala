### Topic: There is currently a regression with Winter 16 and some cases not loading: Time Limit Exceeded. Please note this is very specific, so please escalate to T3 to confirm
There is currently a regression with Winter 16 and some cases not loading: Time Limit Exceeded.
Please note this is very specific, so please escalate the case to T3 to confirm and process accordingly.
Instructions for Tier3:
If a customer has logged a Case as the result of the following KI:
Loading Cases with feeds can cause time limit exceeded error
https://success.salesforce.com/issues_view?id=a1p300000008aIwAAI
...and they need 
immediate
 access to affected Case Records, you will need to schedule a GoToMeeting with the customer, yourself, and R&D.
You can verify that this is the same bug by checking details the Bug Record:
[ REGRESSION ] Loading Cases with feeds can cause CRM time limit exceeded error (and cause our DB connection to get killed) due to strange email parsing issues
https://gus.my.salesforce.com/apex/adm_bugdetail?id=a07B0000001hhoDIAQ&sfdc.override=1
Mention Jim Pan (if possible) to assist.
Resolution
