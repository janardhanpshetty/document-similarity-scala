### Topic: This article has information that using outputText the Time format does not change by design.
To see the difference in date format , you need to change the locale of user but even after changing the locale, outputtext does not affect the user context.
Scenario :-
---------------------------------
Locale of Admin user is "Eastern European Time (Europe/Bucharest)" & for any non-admin user, is is "Pacific Standard Time (America/Los_Angeles)" ; both shows the date format as "Tue Nov 13 00:00:00 GMT 2012" ; if below mentioned code has been used :-
 
<apex:page standardController="Object" showHeader="false"> 
<apex:outputtext value="{!Object.DateField}"> /apex:outputtext> 
</apex:page>
Resolution
It is WAD as outputText does not respect user context & you need to use outputField. For e.g. you need to change the mentioned code to this   :-
 
<apex:page standardController="Object" showHeader="false"> 
<apex:outputField value="{!Object.DateField}"> /apex:outputtext> 
</apex:page>
