### Topic: Learn why a System Administrator will be unable to add more than 5 IP addresses to a profile.
If the "
IP Restrict Requests
" feature is enabled under the profile, the System Administrator cannot add more than 5 IP restrictions to a profile. 
Here's how to check if this feature is enabled in your Organization: 
 
Salesforce Classic UI: 
1.
 
Go to 
Setup | Manage Users | Profiles. 
2. Click on the Profile, and then click 
Edit. 
3. Under "
Administrative Permissions," please check if "
IP Restrict Request
" is enabled.
New Lightning UI:
1. Click on the top-right 
Gear icon | Setup Home | Users | 
Profiles. 
2. Click on the Profile, and then click 
Edit. 
3. Under "
Administrative Permissions," please check if "
IP Restrict Request
" is enabled.
Resolution
 
Disable the feature
To resolve the limit, please disable 
"
IP Restrict Requests,
" 
under the profile Administrative Permissions. 
 
Salesforce classic UI:
 
 
1. 
Setup
 | 
Manage Users
 | 
Profiles
. 
2. Click on the 
Profile
, and then click 
Edit
. 
3. Under "Administrative Permissions," please uncheck the "IP Restrict Request." 
New Lightning UI: 
 
1. Click on top-right 
Gear icon
 | 
Setup Home
 | 
Users
 | 
Profiles
 
2. Click on the 
Profile, 
and then click 
Edit. 
3. Under "Administrative Permissions," please uncheck the "IP Restrict Request." 
