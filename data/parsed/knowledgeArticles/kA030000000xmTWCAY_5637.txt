### Topic: A description of Dashboard Reports and Engagement Reports with resources on how to create them in Radian6.
Learn how to create a Dashboard Report and an Engagement Report in the Analysis Dashboard in Radian6. 
Resolution
There are two types of reports can be built within the Analysis Dashboard, Dashboard Reports and Engagement Reports. 
Dashboard
This type of report displays up to six widgets and their daily data in a report delivered by email. 
Engagement
An 
Engagement report
 shows the Workflow assigned to posts in both the Analysis Dashboard and in the Engagement Console. The Engagement Report is delivered by email. See 
this article
 for steps to create an Engagement Report.
 
Build a report
Learn how to build both types of reports in the 
Reporting User Guide
, this 
reporting video
 and from the 
Analysis Dashboard Quick Start page
. 
