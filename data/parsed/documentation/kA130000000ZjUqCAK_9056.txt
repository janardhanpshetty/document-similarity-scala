### Topic: Manage Additional Sharing Settings | Salesforce
Manage Additional Sharing Settings | Salesforce
Manage Additional Sharing Settings
Besides configuring the organization-wide defaults and sharing rules, you can configure the following items on the Sharing Settings page.
Report visibility available in: Salesforce Classic and Lightning Experience
Manual sharing, manager groups, and communities available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Control standard report visibility.
Control manual sharing for user records.
Control how users can share records with their managers and manager subordinates groups.
Control who community or portal users can see
See Also:
Controlling Who Community or Portal Users Can See
Organization-Wide Sharing Defaults
Working with Sharing Rules
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=other_sharing_settings.htm&language=en_US
Release
202.14
