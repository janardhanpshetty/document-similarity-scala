### Topic: Delete a Process Version | Salesforce
Delete a Process Version | Salesforce
Delete a Process Version
If you no longer need to use a process version that you’ve defined, delete it.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To delete processes:
“Manage Force.com Flow”
AND
“View All Data”
To delete an active process, you must first deactivate it. You can’t delete process versions with an Active status. If a process has any scheduled actions, it can’t be deleted until those pending actions have been executed or 
deleted
.
From Setup, enter 
Process Builder
 in the 
Quick Find
 box, then select 
Process Builder
.
Click the name of the process whose version you want to delete.
For the version that you want to delete, click 
Delete
.
If your process has only one version and you delete that version, the whole process is deleted.
Click 
OK
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=process_delete.htm&language=en_US
Release
202.14
