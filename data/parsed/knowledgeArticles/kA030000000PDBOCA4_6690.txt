### Topic: This article explains why a user might not see the option to schedule dashboards.
A user is not seeing the option to schedule dashboard refreshes. 
Resolution
If the arrow next to the Refresh button on a dashboard is not available, then the dashboard is using the "Run as logged in user" functionality (Dynamic dashboards).  Scheduling dashboards is not available while using this functionality.
To change this, edit the dashboard.  Then click on the drop down arrow on the far right side where it displays:  "
View dashboard as".  
Change this to "Run As Specified User".  Save and Close.  The Refresh button will now show the options to schedule.
*Note:
The "Schedule Dashboard Refreshes" profile permission must also be enabled for the scheduling user's profile.
Scheduled dashboards are available in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
