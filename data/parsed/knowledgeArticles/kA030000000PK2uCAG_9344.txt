### Topic: LastActivityDate is only available if the "Allow activities" checkbox under Optional Features is ticked in the custom object's setup page. If it is not checked, this system field will not be available through the API.
I am able to see the Last Activity Date field for one of my custom objects in a sandbox, but for some reason this field is not available for the same object in another sandbox.
Resolution
LastActivityDate is only available if the "Allow activities" checkbox under Optional Features is ticked in the custom object's setup page. If it is not checked, this system field will not be available through the API. 
 
