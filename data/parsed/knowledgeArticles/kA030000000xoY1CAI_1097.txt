### Topic: When a client reports that they sent an email and it has not been received, we can check and see if the e-mail is still on the MTAs. We can look by IP address and domain.
When a client reports that they sent an email and it has not been received, we can check and see if the e-mail is still on the MTAs. We can look by IP address and domain. 
 
Resolution
First check the job via Job Explorer and make sure it has completed. When marked completed the e-mails have been sent to our MTAs.
In Job Explorer the MTA the job went to can be determined by the Slots tab you can also copy the IP address next to that column. Use the following tool to locate all MTAs utilized by a sending IP: http://pages.exacttarget.com/dop/ipqueue
If you know the domain of the e-mail (exacttarget.com) you can go to the Domains tab in the MTA screen and at the bottom put in the domain. This will show any e-mails queued up by that domain and IP address. It shows the last error received from the domain. If there are none listed the e-mail is not on that MTA.
If the delay is not for any specific domain you can look up information on the IP address. Click the Queues tab. Near the bottom of the screen is the View options. Replace */* with */IP_ADDRESS where IP_ADDRESS is the IP address seen in slots. This will show all mail that is queued up to be sent for that IP address by domain. If there is none listed the e-mail is not on the MTA. 
For both above it will show the last error received from the domain by that IP Address. You can look at the error messages and attempt to determine why the mail failed. Mail on the MTAs will be retried every 15 minutes for 72 hours, which totals 288 tries.
