### Topic: Locale and Language Support for Salesforce Authenticator (Version 2 or Later) | Salesforce
Locale and Language Support for Salesforce Authenticator (Version 2 or Later) | Salesforce
Locale and Language Support for Salesforce Authenticator (Version 2 or Later)
Salesforce Authenticator works with almost all Salesforce supported locales and the fully supported end-user languages. Salesforce Help includes a list of supported locales and languages. Languages that are read right to left, including Arabic and Hebrew, aren’t supported in this release.
See Also:
Requirements for Salesforce Authenticator (Version 2 or Later)
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=salesforce_authenticator_language_support.htm&language=en_US
Release
202.14
