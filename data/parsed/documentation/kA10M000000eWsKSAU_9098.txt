### Topic: Work with Posts | Salesforce
Work with Posts | Salesforce
Work with Posts
Share information and collaborate with other people inside and outside your organization.
Posting Overview
Use the Chatter publisher to make posts, delete posts, ask questions, and create polls.
Post Visibility
Can I prevent people from seeing my posts?
Do posts that I make on a group appear on my profile where others can see them?
Mute a Post
Control what appears in your news feed and mute posts you’re no longer interested in.
Who Can See My Attached Files and Links?
Share files and links with people by attaching them to posts or comments.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_posts_parent.htm&language=en_US
Release
202.14
