### Topic: Distribution Limits for Visual Workflow | Salesforce
Distribution Limits for Visual Workflow | Salesforce
Distribution Limits for Visual Workflow
Keep these limits in mind when you distribute flows to your users via URLs, 
Visualforce
 pages, change sets, or packages.
User Permissions
For flows that interact with the 
Salesforce
 database, make sure that your users have permission to create, read, edit, and delete the relevant records and fields. Otherwise, users receive an insufficient privileges error when they try to launch a flow. For example, a flow looks up and updates a case record’s status. The flow users must have “Read” and “Edit” permissions on the 
Status
 field of the Case object.
Distributing Flows via URL
You can't redirect flow users to a URL that’s external to your 
Salesforce
 organization.
When you distribute a flow, don’t pass a currency field value from a 
Salesforce
 record into a flow Currency variable with a URL parameter. When a currency field is referenced through a merge field (such as 
{!Account.AnnualRevenue}
), the value includes the unit of currency’s symbol (for example, $). Flow variables of type Currency can accept only numeric values, so the flow fails at run time.
 
Instead, pass the record’s ID to a flow Text variable with a URL parameter. Then in the flow, use the ID to look up that record’s value for the currency field.
Distributing Flows to External Users
When you make a flow available to site or portal users, point them to the 
Visualforce
 page that contains the embedded flow, not the flow itself. Site and portal users aren’t allowed to run flows directly.
Deploying Flows via Change Sets
If you plan to deploy a flow with change sets, consider limitations in migration support. Make sure your flows reference only fields and components that are available in change sets.
You can include only one version of a flow in a change set.
Deploying Flows via Packages
You can’t include flows in package patches.
If you plan to deploy a flow with packages, consider limitations in migration support. Make sure your flows reference only packageable components and fields.
You can package only active flows. The active version of the flow is determined when you upload a package version. If none of the flow’s versions are active, the upload fails.
In a development organization, you can’t delete a flow or flow version after you upload it to a released or beta managed package.
Installing Flows from Packages
If you install a package that contains multiple flow versions in a fresh destination organization, only the latest flow version is deployed.
You can’t delete a flow from an installed package. To remove a packaged flow from your organization, deactivate it and then uninstall the package.
If you have multiple versions of a flow installed from multiple unmanaged packages, you can’t remove only one version by uninstalling its package. Uninstalling a package—managed or unmanaged—that contains a single version of the flow removes the entire flow, including all versions.
If you install a flow from an unmanaged package that has the same name but a different version number as a flow in your organization, the newly installed flow becomes the latest version of the existing flow. However, if the packaged flow has the same name and version number as a flow already in your organization, the package install fails. You can’t overwrite a flow.
You can’t delete flow components from Managed - Beta package installations in development organizations.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_visual_workflow.htm&language=en_US
Release
198.17
