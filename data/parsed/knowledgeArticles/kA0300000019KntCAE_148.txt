### Topic: This is related to the known issue "User using cloudforce are required to verify on every login" https://success.salesforce.com/issues_view?id=a1p30000000jgjkAAA where uses have to perform identify verification for every login.
####################################################
SUMMARY:
Spring 16 release doesn't trust verified IP any more which means even IP was activated, if it doesn't has login IP cookie(sfdc_lv2), we will let user go through Identity Verificatin again.
Login from .salesforce.com(including my domain) works fine. As long as browser enable cookie, login from same IP same browser will no longer require Identity Verification after first time.
But cloudforce user require Identity Verification for every login.
####################################################
REPRODUCER:
1. Set a my domain org. Set Network Access in Org so your IP will be out of IP range. Verify login IC works as I described above.
sfdc_lv2 cookie is under .salesforce.com domain.
2. Switch domain suffix from my domain to cloudforce domain.
In BT company info page, set "My Domain Prod Suffix"  from a 0 to 2 
If domain in step1 is 
https://danlexpool-dev-ed.my.salesforce.com
, it will change to 
https://danlexpool-dev-ed.cloudforce.com
3. Clear all cookies in browser, reopen browser.
4. Login from the cloudforce.com domain. You will be challenged, enter verification code and login.
Check browser cookie under ".cloudforce.com" and "
danlexpool-dev-ed.cloudforce.com
", there is no sfdc_lv2 cookie
5. Logout and login to same cloudforce.com domain, you will be challenged again.
####################################################
CAUSE:
This feature depends on sfdc_lv2 cookie.
We should set sfdc_lv2 cookie for cloudforce domain.
####################################################
WORKAROUND:
Enable Org perm "Validate IP during Identity Confirmation".
####################################################
Resolution
Escalate to Tier 3 Security team.
Tier 3 Security to enable "Validate IP during Identity Confirmation" per 
W-2930241
.
