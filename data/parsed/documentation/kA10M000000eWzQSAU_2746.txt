### Topic: Merge Fields for Approvals | Salesforce
Merge Fields for Approvals | Salesforce
Merge Fields for Approvals
Approval merge fields include 
{!ApprovalRequest.
fieldName
}
 and 
{!ApprovalRequestingUser.
fieldName
}
. They’re supported in certain email templates and return different values based on the status of the approval process instance.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Tip
The submitter isn’t always the current user. For custom email templates, use 
{!ApprovalRequestingUser.
fieldName
}
 instead of 
{!User.
fieldName
}
.
Where Are Approval Merge Fields Supported?
You can use approval process merge fields in email templates, but not mail merge templates. Except for 
{!ApprovalRequest.Comments}
, approval merge fields named 
{!ApprovalRequest.
field_name
}
 in email templates return values only in approval assignment emails and email alerts for approval processes. When used in other emails—including email alerts for workflow rules—the approval merge field returns 
null
.
What Values Does a Merge Field Provide?
The generated value of an ApprovalRequest merge field depends on which step the approval process is in.
In the approval request email, a merge field returns the submitter’s name and the name of the first step.
When the request is approved, the merge field returns the most recent approver’s name and the name of the second step, if applicable.
For subsequent actions, a merge field value returns the previous completed step.
For an approval step that requires unanimous approval from multiple approvers, 
{!ApprovalRequest.Comments}
 returns only the most recently entered comment in emails.
See Also:
Default Template for Email Approval Response
Manage Email Templates
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_considerations_mergefields.htm&language=en_US
Release
202.14
