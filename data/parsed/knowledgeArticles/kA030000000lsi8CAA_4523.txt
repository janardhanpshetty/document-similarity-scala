### Topic: Detailed steps on how to give access to a badge.
How to provide access to a badge to another user?
Resolution
Follow the steps below to give access to a badge:
1. Go to the Badge tab
2. Click on the badge you would like to edit
3. On the Badge detail page, look for the Access related list
4. Click on "Edit List" 
5. Add/remove users access to the badge
6. Click Save
Note:
 If the badge is set as a company badge, you won't see the "Edit List" button. Remove the Company Badge check box in order to see the "Edit List" button.
 
