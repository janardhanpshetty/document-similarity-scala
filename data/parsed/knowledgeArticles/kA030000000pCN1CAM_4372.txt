### Topic: This article details the steps to change the currency displayed in your Forecast. This is applicable to Customizable Forecasting only.
How do I change the currency displayed in the Forecast?
Resolution
By default, your Quota amounts display in your personal currency. You can change the currency of your Quota to any of your organization’s active currencies.
Please note this Article refers to 
Customizable Forecasting 
only, if you're using Collaborative Forecasting your Quota amounts will display in your display currency and the following steps will not apply.
When you change the currency, the corresponding Forecast is also changed to the new currency.
 All Quota and Forecast amounts are converted to the new currency using your organization’s conversion rates.
To change your quota currency:
1. From your personal settings, enter Advanced User Details in the Quick Find box, then select 
Advanced User Details
.
No results? Enter Personal Information in the Quick Find box, then select 
Personal Information
.
2. Click 
Change Currency
 next to the quota in the Quotas related list.
3. Set the new currency and click 
Save
.
If you are using Customizable Forecasting, please update your Quota using your new personal currency. Forecasts are still displayed in the old currency until you update your Quota using the new currency.
To change your Quota currency in Customizable Forecasting:
1. In the Quotas related list, select the Forecast Range Start and Range Length for the Forecast period you want to change.
2. Click Edit.
3. Select the new currency and update the associated Quota amounts.
4. Click Submit.
