### Topic: 40 characters limit for multi-picklist should be resolved with Summer '16 release
We now allow users to exceed 40 characters for values in Multi-Picklists, however, if adding values via the API - users may sometimes still hit an error that the maximum characters have been exceeded.
Resolution
This should be resolved for our Summer '16 release - in the meantime, the workaround is to lower the character count being added to under 40.
