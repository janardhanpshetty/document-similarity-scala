### Topic: Steps for composing and publishing posts to Twitter using ConversationBuddy.
In ConversationBuddy, you can compose and publish posts from your registered social media Profiles. This article describes how to compose and publish tweets using ConversationBuddy. You can select to schedule posts immediately, or set a date and time to publish. You can also Seed content out to Profile owners for editing or approval before publishing.
Resolution
Compose
To compose a tweet in ConversationBuddy, follow these steps:
1. Log in to Buddy Media. In the top navigation bar, select 
ConversationBuddy
.
2. In the top right corner, click the green 
Compose 
button to open the Compose Message window.
3. The Compose Message window opens. In the 
Add Profiles or Distribution Lists
 field, start typing the name of a Profile or Distribution List.
4. The drop-down list populates with applicable Profiles and Distribution Lists. Select the one from which you want to post.
Note
: A Twitter Distribution List enables you to send tweets to a selected group of Twitter accounts at one
time. You can 
create Distribution Lists
 in the Tools section of ConversationBuddy You can only apply one
Distribution List per tweet and applying it will override all previous Profile selections.
5. If you added a Distribution List, a warning screen will populate. Click 
Yes 
to continue.
6. In the 
Enter a Message
 field, type your message.
Note
: The Compose Message box has a character count for Twitter starting at 140 characters. This counts
down as characters are typed.
7. To add an image, under the Enter a Message field, click the 
Image 
icon. Click 
Choose File 
and select the correct image.
8. If you want to add a label to help sort and track posts, click in the 
Add Label
 field and type a 
label name
. From the drop-down menu, select a pre-existing label or click 
Create New
.
9. Decide when you want to publish the Tweet:
Send Now: Publish a post live on the selected Twitter Profile.
Schedule: Select a future date and time to publish the post.
Seed: Send the post to other Profile owners for editing and/or approval before it is published
 
Publish
To publish a post immediately, follow these steps:
1. Click 
Send Now
.
2. Click 
Publish
.
To schedule a post, follow these steps:
1. In the Compose Message window, click 
Schedule
.
2. In the Calendar, click on a date to select it.
3. In the Time of Day drop-down menu, select the time.
4. Click 
Schedule
.
Note: The post will display in the Outbox column of ConversationBuddy. From the Outbox, you can edit the post
content and the scheduled date and time.
 
Seed
To Seed a post, follow these steps:
1. In the Compose Message window, click 
Seed
.
2. Check the
 Lock Message Content and Targeting
 box. This prevents users from editing the post content.
3. Check the 
Choose a Date
 box. This enables you to set a specific date and time to publish the post, or
to set a window of time during which the post will be published.
4. If you want to set a specific date and time, click 
Set Specific Date
. Select a date to publish the post and use the drop-down menu to specify the time.
5.  If you want to specify a time range for publishing, click 
Set Window
. Select the Earliest Publish Date and the Latest Publish Date.
4. Click 
Seed
.
Note: The post will display in the Outbox column of ConversationBuddy. From the Outbox, you can edit the post
content and the selected date and time.
