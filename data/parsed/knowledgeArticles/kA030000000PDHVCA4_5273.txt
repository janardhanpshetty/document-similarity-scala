### Topic: Success Plans deliver the training, support, and success resources needed to ensure you get the most out of your Salesforce investment.
Which Premier Success Plan is Right for Me?
Salesforce customers tell us that having access to the right combination of training, support, and resources is critical for ensuring high return on their investment.
With that goal in mind, we've created a variety of Success Plans to fit your unique needs.
 
Everyone needs a Success Plan — which one will you choose?
For an overview of the Success Plan available for each Salesforce cloud, please go to 
www.salesforce.com/premier
Select the Success Plan that’s right for you - 
From online tools and training to best practice coaching, 24/7 support, technical help for app development, and a team of experts to extend your team's capacity, Success Plans give you everything you need to make sure you're getting the most out of your Salesforce investment.
 
Premier Success Plan
The Premier Success Plan includes:
Continuous coverage
: 24/7 toll-free phone support
Prioritized queue
: priority access to our support team and fast 2-hour response time
Comprehensive training catalog
: unlimited access to 130+ online training courses for all roles, with learning paths and sample training plans
Customizable training templates
: downloadable course content, including storyboards and scripts, for you to customize and deliver in your preferred format
Premier Developer Support
: error-related troubleshooting and code reviews plus best practices to help you build apps on the Force.com platform
The Premier Success Plan can be purchased with Enterprise or Professional Editions.
 
Premier+ Success Plan
The Premier+ Success Plan includes all the benefits of the Premier Success Plan, plus:
Administration services
: unlimited access to dozens of administration services, covering more than 100 tasks, for set up and ongoing application maintenance
Administration team
: work with our team of Salesforce-certified administration experts, who understand your business goals, share best practices, and connect you to technical resources, when necessary
The Premier+ Success Plan is included with Salesforce CRM and Force.com Unlimited Edition, or can be purchased with Enterprise and Professional Editions.
 
Standard Success Plan
All Salesforce products come with a Standard Success Plan that includes customer support and access to online resources. The Standard Success Plan offers unlimited support cases and 2-business-day response time for cases submitted online or via 12/5 phone support. You also get anytime access to our online resources, including Help, where you can view Getting Started training courses, submit support cases, and browse FAQ, documentation, and the knowledge base.
 
Get Started on the Path to Success
For more information and a detailed list of services covered, please go to 
www.salesforce.com/premier
 and click on the "Learn More" links.
 
For more help to decide which Success Plan is best for you, please contact your Salesforce Account Executive.
 
 
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
