### Topic: .ICO Image file types do not show up in Salesforce1 when used in image formulas
If an Image Formula references a .ICO image file type that image is not visible in the Salesforce1 mobile application.  The images will not be visible in List Views but are still visible in record detail views.
Resolution
Replacing the .ICO image with a supported image type such as .jpg and .gif images will resolve this.
Please note: the visible area for an Image Formula in Salesforce1 app list view is 16 X 16 pixels.  Using images larger than 16 X 16 will result in the image being cut off.
 
