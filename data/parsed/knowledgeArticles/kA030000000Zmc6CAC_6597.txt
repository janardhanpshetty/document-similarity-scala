### Topic: Post-chat pages let share information with customers at the end of a chat session.
Post-chat Page Overview
Post-chat pages let share information with customers at the end of a chat session. For example, you can direct your customers to another web page after they complete a chat with an agent or forward them to a survey about their chat experience.
You can also create a Visualforce page to host your post-chat page, or can develop a page on your own. You can find information on creating a custom post-chat form page using Visualforce 
here
.
Post-chat Survey Integration 
To implement a Post-chat page follow the click path below:
Setup-> Chat Buttons & Invitations-> Either setup a 
New 
or you can use an existing one
. 
When implementing a survey into the Post-chat page, you will want to paste the link to the survey into the 
Post-Chat URL 
field, which is located at the bottom of the page.
What Survey Applications Are Available?
To find the right survey for your company please refer to the salesforce.com AppExchange: 
Click Here
 
Resolution
