### Topic: This article is for custom Indexing steps for T2's to follow before escalating to T3.
Custom Indexing:
The following fields are indexed by default: primary keys (Id, Name and Owner fields), foreign keys (lookup or master-detail relationship fields), audit dates (such as SystemModStamp), and custom fields marked as External ID or Unique.
A custom index can't be created on these types of fields: multi-select picklists, currency fields in a multicurrency organization, long text fields, and binary fields (fields of type blob, file, or encrypted text.) Note that new data types, typically complex ones, may be added to Salesforce and fields of these types may not allow custom indexing.
Typically, a custom index won't be used in these cases:
           
    
             The value(s) queried for exceeds the system-defined threshold mentioned above
The filter operator is a negative operator such as NOT EQUAL TO (or !=), NOT CONTAINS, and NOT STARTS WITH
The CONTAINS operator is used in the filter and the number of rows to be scanned exceeds 333,000. This is because the CONTAINS operator requires a full scan of the index. Note that this threshold is subject to change.
                          When comparing with an empty value (Name != '')
 
Gathering information from customer:
SOQL query with bind values and login access to check the issue from salesforce end
Why you need an index ? a. Non selective query error in trigger context b. Performance issue
Do they have the same issue in full copy sandbox ?
Do they have any workarounds rather than indexing these fields. 
Replication steps for the performance issue. 
Does they consider adding more filters to the query ?
 
Resolution
Troubleshooting steps:
Using Workbench perform the analysis on the SOQL query provided with bind values
Gather the report id if they have issues with report performance/timeout.
Do they have any soft deleted records ? Check in workbench using include and exclude options
Can they add more filters if they have performance issue?
Did you check in BT SOQL query planner?
Escalate the case with above details to Tier 3 support
What is the Maximum custom index per entity limit in their org?
Resources
:
https://help.salesforce.com/apex/HTViewSolution?urlname=Custom-indexes-for-an-organization-to-help-improve-performance-1327109991720&language=en_US
https://help.salesforce.com/apex/HTViewSolution?urlname=Maximum-Number-of-Custom-Indexes-per-Entity&language=en_US
https://help.salesforce.com/help/pdfs/en/salesforce_query_search_optimization_developer_cheatsheet.pdf
INTERNAL: Advanced How To: SOQL Query Analysis To Determine If Fields Are Custom Index Candidates: 
https://org62.my.salesforce.com/articles/Knowledge_Article/INTERNAL-Advanced-How-To-SOQL-Query-Analysis-To-Determine-If-Fields-Are-Custom-Index-Candidates?popup=true​
INTERNAL: How do I custom index fields for an org that has hit their limit of indices/object? : 
https://org62.my.salesforce.com/articles/Knowledge_Article/INTERNAL-How-do-I-custom-index-fields-for-an-org-that-has-hit-their-limit-of-indices-object?popup=true​
Skinny Table Considerations : 
https://org62.my.salesforce.com/articles/Knowledge_Article/Skinny-table-and-their-limitations?popup=true
Listview performance ananlysis: 
https://sfstew.secure.force.com/eureka/eureka_querywizard?topic=a0Mi0000009khKQEAY​
Skinny Tables: 
https://sfstew.secure.force.com/eureka/eureka_querywizard?topic=a0Mi000000DUjJhEAL​
 
