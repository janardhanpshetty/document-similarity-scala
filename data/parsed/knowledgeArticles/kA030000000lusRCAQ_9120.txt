### Topic: Drive better adoption utilizing a balanced combination of management mandates and user incentives.
Driving Adoption with Carrots and Sticks
Early adoption is the key factor for a successful CRM project which provides measurable return-on-investment (ROI).  Getting it right from the start improves your chances of success and quickly drives a valuable set of data in the system.  Success relies on a combination of factors, including executive sponsorship, training, data quality, and incentives/mandates.  This article details the types of incentives/mandates that provide the best chance of success.
Carrots
Driving adoption through user incentives is referred to as using carrots.  
These are the most important and effective methods, and the methods most specific to your individual users.  
Salesforce usage should be improving your reps performance and productivity.
 Understanding how its usage will accomplish that and communicating it loud and clear are going to be the biggest bang for your buck adoption drivers.  
Examples of carrots include rep-specific reports that show 
exactly
 what the rep's are looking for, workflows and automation which reduce manual work, streamlined request/approval processes, improved collaboration, and rewards for strong usage.  
Get creative and use what will be most important to your reps.  
Understand their pain points and daily troubles; look to alleviate them.  
Add carrots continually as you grow your implementation.
Sticks
Driving adoption through management mandates is referred to as using sticks.  
These are less effective than carrots, but are necessary as they will further enforce adoption and fill in the gaps that the carrots miss. 
These should be rolled out slowly, carefully and gradually; and should be driving usage, not frustrating the reps.
 Make sure that reps are trained and comfortable with the tasks they must perform, and that these tasks are providing value for both management and the user.  
Only when the system has quality data and a variety of simple mandates are already successfully in place should larger mandates be enforced, such as commission restrictions for deals not in the system.
A great example of a stick is the usage of sales management dashboards by both Sales managers 
and
 executives.  Reps will be inclined to adopt the system when they know they'll either be positively recognized or punished by the information on the dashboards.  
Show the reps what the execs and managers are looking at, and what metrics they're looking for the reps to be hitting.  
Give the reps their own dashboard so they can see how they're performing.
Critical to this is the ongoing usage of the sticks.  
They should not be a one and done request, such as add all of your opportunities to the system this week, or scheduled 10 meetings in Salesforce.  They should be ongoing requirements which reinforce regular and consistent usage of the tool.  
For additional information on this topic check out: 
Achieve More with Salesforce: Reduce Sales Rep Ramp Time 
 
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
