### Topic: Email to Case Looping detection details.
The Automated Case User (configured for Email to Case) may see the following email notification indicating that a Case record was not created from an inbound message:
============================
-----Original Message----- 
 
From: support@salesforce.com [ mailto:support@salesforce.com ] 
Sent: Tuesday, May 15, 2012 11:34 AM 
To: Test User
Subject: Email-to-Case: Error(s) encountered while processing 
 
The following errors were encountered while processing an incoming email: 
 
LIMIT_EXCEEDED : A loop was detected in Email-to-Case processing 
 
From: test@test.com
Sent: Tue May 15 16:33:42 GMT 2012 
To: [sforce@a-z00jkdxxh0kjl0jk0000lqf.xxxx.u.case.salesforce.com] 
Subject: Attached File
 
File
============================
CAUSE:
Salesforce has set measures to detect an email loop from an external email system.  This is set in place to prevent a continual loop that could cause a large amount of erroneous inbound messages into a Salesforce.com Org.
The Email to Case looping algorithm looks at Subject, Sender and Body to detect looping. After receiving more than 15 emails in 60 seconds with the same content in the three fields, the loop detection will trigger and stop creating Cases within that time span.
*Note that environment processing variables could affect the 15 inbound email governing limit, and the number of cases created could vary to be slightly more than the limit.
Some causes of email loops could range from:
- Out of Office replies to improperly created Workflow and Email Alerts that send to a Email to Case Routing Address. 
- Case Auto Response Rule's "Reply to" and/or "Sender's email" address set as the Case Routing Email address.
- 
Owner of the Case (either a Queue or a User) email address is an active Email to Case routing address.
Resolution
Modify configuration settings to not utilize the Routing Address.  
Also see - 
Email-to-Case: Error(s) encountered while processing - NULL Body
