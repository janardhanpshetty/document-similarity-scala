### Topic: This article explains how to uninstall managed package that doesn't show uninstall button.
There are certain managed packages that don't show the uninstall button on the package page. Its possible to uninstall a package using the link listed below:
https://naX.salesforce.com/
_ui/core/mfpackage/uninstall/PackageUninstallerUi/e?p1=<All Package ID>&retURL=%2F0A3%3Fsetupid%3DImportedPackage%26retURL%3D%252Fui%252Fsetup%252FSetup%253Fsetupid%253DStudio 
The all package ID starts with 033 and can be found from BT by clicking on the install package thats needed to be uninstalled. The all package ID will be listed on the page. Fill in the appropriate instance, All package ID and the user can use that link to remove a package. 
 
Resolution
Change the instance in the link where the customer org is located and locate the all package ID for the package that needs to be uninstalled. It can be found by clicking on the package name logged out of the customer org. Simply click on installed packages from blacktab and click on the package name. The next page will show the all package ID and other details. Plug in the information in the link, login to the customer org and paste the link. Follow the default prompts and uninstall the package. 
