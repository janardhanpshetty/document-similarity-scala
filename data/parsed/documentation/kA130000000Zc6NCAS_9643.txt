### Topic: Navigation and Actions in Salesforce1: Limits and Differences from the Full Salesforce Site | Salesforce
Navigation and Actions in Salesforce1: Limits and Differences from the Full Salesforce Site | Salesforce
Navigation and Actions in Salesforce1: Limits and Differences from the Full Salesforce Site
Navigation
The 
Salesforce1
 mobile app is supported in portrait orientation only. In the downloadable apps, the interface doesn’t rotate when a device is switched to landscape orientation. The mobile browser app interface does rotate but isn’t guaranteed to work correctly in this orientation.
Actions
Most actions, including quick actions, productivity actions,and standard and custom buttons, are displayed in the action bar or list item actions in 
Salesforce1
.
There are a few differences between the Send Email quick action in 
Salesforce
 and the standard Email action in Case Feed:
Users can’t switch between the rich text editor and the plain text editor in a Send Email action.
Templates aren’t supported in the Send Email action.
Quick Text isn’t available in the Send Email action.
The Send Email action doesn’t support attachments.
Users can’t save messages as drafts when using the Send Email action.
Users can’t edit or view the From field in the Send Email action.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_mobile_sf1_publisher_and_actions.htm&language=en_US
Release
200.20
