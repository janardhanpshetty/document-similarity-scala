### Topic: RecordTypeId will not throw SObjectException while querying SObjects without the RecordTypeId on a Visuaforce page as this is working as designed
RecordTypeId do not throw SObjectException while querying SObjects without the RecordTypeId on a Visuaforce page , however it gives SObjectException when executing the same code on Developer console.
It should throw SObjectException and expected to  behave consistently the way it is throwing exception on Developer console.
 
Resolution
This is working as expected. 
The recordTypeid field is added implicitly as it is required to support Visuaforce and  this is currently an intended behavior when displaying it on Visualforce page. 
To reproduce this scenario you can follow the steps mentioned below:
Steps 1:   Create a Apex controller:
Apex class code: 
============================= 
public class Debugsobj {
public String recordtypeid{get;set;}
public String Status{get;set;}
public PageReference testcode() {
    List<case> cases = [SELECT id FROM case LIMIT 1]; 
    case  x  =cases[0]; 
    recordTypeId = 'RecordType ID for Case is ' + x.recordTypeID;
    return null; 
   }
public PageReference testStatus() {
    List<case> cases = [SELECT id FROM case LIMIT 1]; 
    case  x  =cases[0]; 
    Status= 'Status of this Case is ' + x.status;
    return null; 
   }
}
Step 2: 
Create a visual force page to display recordtypeid and status with querying in the SOQL:
<apex:page controller="Debugsobj" > 
<apex:form id="caseForm"> 
<apex:commandButton value="Get Record Type ID " action="{!testcode}" rerender="text"/> 
<apex:outputPanel id="text">
<apex:outputText > {!recordtypeid}</apex:outputText> 
</apex:outputPanel>
<apex:commandButton value="Get Status " action="{!teststatus}" rerender="text1"/> 
<apex:outputPanel id="text1">
<apex:outputText > {!status}</apex:outputText> 
</apex:outputPanel>
</apex:form> 
</apex:page>
NOTE :- 
Now preview the VF page and click on the command button "Get Record Type ID". The recordtypeid is displayed on the Visualforce page. However, running the same code in Dev Console will throw the below SObjectException:
System.SObjectException: SObject row was retrieved via SOQL without querying the requested field: Case.RecordTypeId 
 
