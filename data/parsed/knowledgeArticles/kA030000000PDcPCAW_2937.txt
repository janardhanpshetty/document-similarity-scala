### Topic: Chatter Desktop toaster alerts pop a small rectangular icon in the bottom right of the screen when new posts or comments are made to your feed or to people you follow or groups that you belong to.
Chatter Desktop toaster alerts pop a small rectangular icon in the bottom right of the screen when new posts or comments are made to your feed or to people you follow or groups that you belong to. Some things to note are:
1. They are enabled by default and can be disabled in Chatter Desktop settings by unchecking the Enable Notifications check box
2. If you check the option in Chatter Desktop settings  "Show comments in feed" you will only get toaster alerts for new posts and not comments.
3. Toaster Alerts will only pop up if Chatter Desktop is not the active application/window or if it's running in the background (minimized to the dock/systray or the taskbar)
4. The alerts are polled for every 5 minutes and you will get only one alert at polling time regardless of how many posts or comments are made in that period.
5. Toaster alerts are not displayed for posts and comments you made yourself.
Resolution
