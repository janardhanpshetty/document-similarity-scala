### Topic: This is the new multi-Currency activation process, as of Spring'15
1) The case is created
2) The case owner sends the revised template to the customer
3) The customer will reply back providing:
- Org ID confirmation
- checking the "Allow Support to Activate Multiple Currencies" checkbox
- confirming the currency
- confirming the username and granting login access
4) The agent will run the estimator tool, If no email received, please check the following :
                     Check the "Allow to send" setting for the org if it is set to 0 (zero)
                     Another option for finding the estimator information is to run a splunk query like the example below: 
                     index=naxx organizationId=00D3xxxxxxxxxxx logRecordType=shmce,
          Then using a private case comment will copy and paste the result (green/Yellow/Red) and the  number of chunks. If the estimation is:
A) 
GREEN
, the agent will confirm all the information (Org ID, system admin username, currency, preferred date/time if existing) and then he/she will be able to proceed
 without the 2 persons “concur” process
 and without flagging the “legacy” checkbox. 
Please note that all the time reference must be on UTC/GMT time zone.
 
B) 
YELLOW
, the agent needs to contact the customer and confirm the preferred time outside the customer’s business hours during the week or during weekends and verify all the information on the template (Org ID, system admin username, currency). The agent will be then able to proceed
 using the 2 persons “concur” process 
without flagging the “legacy” checkbox at the designated date/time. 
Please note that all the time reference must be on UTC/GMT time zone.
C) 
 RED
, the agent needs to contact the customer and confirm the preferred time during a weekend and verify all the information on the template (Org ID, system admin username, currency), then schedule the activation at the latest on the Wednesday prior to the weekend of the activation. The Tier 3 agent will be able to proceed 
without 
using the 2 persons “concur” process
 without flagging the “legacy” without flagging the "protected mode” checkbox at the designated date/time.  Please note that all the time reference must be on UTC/GMT time zone.
Resolution
EMAIL TEMPLATE: 
Multi-Currency Activation Approval Request
CASE COMMENT TEMPLATE:
We have received your request for the activation of the Multi-Currency feature for your environment. There are some key implications in doing so that I want to make sure you are aware of. Please reply to this email OR submit a case comment answering the following questions: 
​
1. What is the Organization ID of the production or sandbox instance (please specify if sandbox or production) where you need the feature to be activated?  (Navigate to Setup > Company Profile > Company Information):  
2. You must be an active system administrator authorized on behalf of this organization to request this feature, please specify the username you are using for this organization. If you are a partner logging this case on behalf of a customer, please also grant login access for 3 days as a proof of authorization (please note that support is not going to use the login access for this activation but is just to validate the request).
3. Please confirm that you understand that once multi-currency is activated, it CAN'T be deactivated? (we encourage testing this first in a trial/DEV org or a sandbox):   
4. Please confirm the date and time (including timezone) you would prefer this activation to take place.
5. Please confirm that you consent to the lockout of this org for a certain period of time as described below (please note that there estimations are only indicative)?
6. Please note that activating the multi-currency feature will stamp all existing records with one currency code that will be the default currency stamp you have selected. This organization default can be altered after the process is completed, but will only impact existing records if done so through record updates.
Have you set the default currency to the desired value? (This can be done by navigating to Setup > Company Profile > Company Information > Edit > Currency Locale)
Please review the supported currencies and ISO codes in the article "Supported currencies":  and confirm the ISO code you select.
7. Have you followed the click path below and allowed support to activate multi-currency checking the box?
Setup | Administration Setup | Company Profile | Company Information | Populate “Allow Support to Activate Multiple Currencies”  and save.
 By enabling this check box, you are acknowledging that you are ready for multi-currency to be activated, but does not override the designated date/time for activation.
****************************************************************************************************
THIS PROCESS LOCKS THE ORG when we activate Multi-Currency (preventing users from logging in or any integrations from processing.)
**For small orgs, we can activate anytime as the org lock time will be typically less than 20 minutes.
**For medium sized orgs, we can activate after 5pm your time zone and the org lock time can be typically up to an hour. We will coordinate with you before proceeding.
**For large orgs, we can activate after Friday 5pm your time zone and the org lock time can be up to several hours.  We must schedule these activations before end of day Wednesday before the weekend of activation. We will coordinate with you before proceeding.
 
**PLEASE NOTE** We cannot guarantee that any particular activation time frame will be available, especially on short notice. We will do our best to accommodate the time requested, but be aware that the activation process may be initiated up to 90 minutes after the scheduled time. To ensure a smooth transition to multiple currencies, some activations for larger organizations may need to be scheduled a week (or more) ahead of time to avoid release windows, maintenances, etc. The case owner will work with you on scheduling the activation for a time that works for your business needs while also avoiding taking any risks by activating during a patch, maintenance window, etc. Since we are a global organization, we can accommodate activations during off-business hours as well. Please communicate all scheduling inquiries to the case owner.
****************************************************************************************************
Please review the below article and confirm that you are aware of the implications in the article below to ensure a smooth transition for your organization.
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_enable_multicurrency_implications.htm&language=en_US
I look forward to hearing from you.
Thank you,
[AGENT NAME]
 
