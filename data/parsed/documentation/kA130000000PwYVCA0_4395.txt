### Topic: View the CommunityBranding Style Sheet | Salesforce
View the CommunityBranding Style Sheet | Salesforce
View the CommunityBranding Style Sheet
The 
CommunityBranding
 style sheet contains a set of branded styles from your community.
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
Community branding options, such as headers, footers, and page colors, are set from the 
Administration
 | 
Branding
 section on the Community Management page.
To see the Community styles in the 
CommunityBranding
 style sheet, on the Site.com Overview tab, click Style Sheets, and click the 
CommunityBranding
 style sheet. The Community styles are listed on the left. To see the code for the style sheet, click 
Edit Style Sheet Code
.
A total of fourteen Community class styles are provided. These are the default contents of the style sheet:
.brandZeronaryBgr {
	background-color: {!Network.zeronaryColor} !important;
}
.brandZeronaryFgr {
	color: {!Network.zeronaryComplementColor} !important;
}
.brandPrimaryBgr {
	background-color: {!Network.primaryColor} !important;
}
.brandPrimaryFgr {
	color: {!Network.primaryComplementColor} !important;
}
.brandPrimaryBrd2 {
	border-color: {!Network.primaryComplementColor} !important;
}
.brandPrimaryFgrBrdTop {
	border-top-color: {!Network.primaryComplementColor} !important;
}
.brandPrimaryBrd {
	border-top-color: {!Network.primaryColor} !important;
}
.brandSecondaryBrd {
	border-color: {!Network.secondaryColor} !important;
}
.brandSecondaryBgr {
	background-color: {!Network.secondaryColor} !important;
}
.brandTertiaryFgr {
	color: {!Network.tertiaryComplementColor} !important;
}
.brandTertiaryBgr {
	background-color: {!Network.tertiaryColor} !important;
	color: {!Network.tertiaryComplementColor} !important;
	background-image: none !important;
}
.brandTertiaryBrd {
	border-top-color: {!Network.tertiaryColor} !important;
}
.brandQuaternaryFgr {
	color: {!Network.quaternaryComplementColor} !important;
}
.brandQuaternaryBgr {
	background-color: {!Network.quaternaryColor} !important;
}
See Also:
Getting Started With Communities
Create Branded Pages Overview
Create Branded Pages from the Community Template
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_communities_branding_stylesheet.htm&language=en_US
Release
202.14
