### Topic: Choose Where Users Go When Clicking a Dashboard Component | Salesforce
Choose Where Users Go When Clicking a Dashboard Component | Salesforce
Choose Where Users Go When Clicking a Dashboard Component
You can edit a dashboard component so that when users click the component, they can drill down to the source report, filtered report, record detail page, or other URL.
Edit a component and set the 
Drill Down to
 option on the Component Data tab. Choose one of these options:
Source Report—Takes the user to the full source report for the dashboard component.
Filtered Source Report—When users click individual groups, X-axis values, or legend entries, they are taken to the source report filtered by what they clicked.
For example, if you had a stacked vertical column chart of opportunities grouped by stage, with months as the X-axis, you could click an individual stage in a bar, a month on the X-axis, or a legend entry for a stage to drill down to the filtered source report. (Not available for gauges, metrics, or tables.)
Record Detail Page—When users click chart or table elements, axis values, or legend entries, they are taken to the detail page for that record. 
You can only choose this option for tables and charts that use a source report grouped by record name, record owner, or feed post. (Not available for gauges or metrics.)
Other URL—Takes the user to the URL that you specify. You can't add URLs that begin with “mailto:” or “javascript:” to dashboard components.
Parent topic:
 
Modify a Dashboard Component
Previous topic:
 
Make Fields Available in a Dashboard Component
Next topic:
 
Custom Four-Column Table
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_dashboards_how_control_where_users_go.htm&language=en_US
Release
202.14
