### Topic: How to find out if your Bulk API schema is correct
I want to find out if my Bulk API schema file is valid?
Resolution
You can download the schema file for an API version by using the following URI:
 
Web_Services_SOAP_endpoint_instance_name/services/async/APIversion/AsyncApi.xsd
 
For example, if your organization is on the na5 instance and you're working with version 33.0 of the Bulk API, the URI is:
 
https://na5.salesforce.com/services/async/33.0/AsyncApi.xsd
 
The instance name for your organization is returned in the LoginResult serverUrl field.
 
The schema file is available for API versions prior to the current release. You can download the schema file for API version 18.0 and later. For example, if your organization is on the na2 instance and you want to download the schema file for API version 18.0, use the following URI:
 
https://na2.salesforce.com/services/async/18.0/AsyncApi.xsd 
