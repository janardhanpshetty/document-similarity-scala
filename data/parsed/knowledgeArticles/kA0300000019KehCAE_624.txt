### Topic: Highlights Panel might be editable for certain objects but not available for use in the object itself. This is due to a decision made during the release of the functionality itself.
There are a couple of Objects identified where the Highlights Panel is available in the Page Layout Editor but the functionality is not available in the feature itself.
For example:
1. Open a "Live Chat Transcript" page layout. 
2. Check that the "Highlight Panel" section is available and editable. 
3. Click the "Layout Properties" 
or
1. Click Setup | Knowledge | Article Types | click on one 
2. Click the "Edit Layout" button
You'll see that the Highlights Panel isn't really configurable for those objects.
Resolution
This is likely to be a Working as Designed scenario and a feature request that should be posted to Idea Exchange.
In the Summer '13 184 Release Highlights Panel was pushed to the Layout Editor but then it was the R&D Team working on the Object itself that had to white-list it and make it usable.
Because of this you can see the Highlights Panel in pretty much every object but you cannot configure it (or it doesn't appear in the Layout Properties) in certain Objects like Article Types or Live Agent Transcript.
You can see that, even though was originally logged as a Bug (
Known Issue
) it is set as "No Fix" because R&D deemed this situation as working as designed.
Tier 1 / Tier 2
: If you receive a case where the object isn't Live Agent Transcript and where the customer justifies (e.g. business impact, misleading documentation...) the need of using Highlights Panel, you might escalate to Tier 3 and they could consult with the specific team in R&D that owns that particular Object. Please note that this should come with a clear business justification.
