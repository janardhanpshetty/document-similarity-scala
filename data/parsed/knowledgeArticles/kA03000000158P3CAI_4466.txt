### Topic: Inline editing is not available for orgs with Lightning Experience enabled in Winter '16. This is on the roadmap for future release
For Orgs with Lightning Experience enabled in Winter '16 - users will not be able to inline edit list views or records directly. 
 
Resolution
This is on the roadmap for a future release.
