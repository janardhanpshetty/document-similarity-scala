### Topic: Tickets need to be escalated to a development team in GUS for BUG, INVESTIGATION, ToDo
Tickets need to be escalated to a development team in GUS for: 
BUG - a bug can be reproduced successfully
INVESTIGATION - an issue is not reproducible but needs to be investigated outside of Lev1 support access
ToDo - production access needed to complete task
Flow:
GUS
 - Go to GUS
Work > New > Choose Bug, Investigation or ToDo (Or click the custom link within the case)
Fill in required fields
Include "WDCS Case #" and a specific issue in the subject
Example: WDCS Ticket #0610275 - Test Description"
Add: "Assigned to" and "Team"
Product Area: Work.com...
Add Submit bug text:
62Org
 - Go to the Case
Choose the appropriate drop down for "GUS Record Type"
Add the GUS Bug# (starts with W usually)
If this is the second escalation on an existing GUS case, make sure to add the GUS ID to the 62Org case and add the customer names to the GUS subject.
Resolution
