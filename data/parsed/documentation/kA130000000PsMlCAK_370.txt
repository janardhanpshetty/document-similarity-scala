### Topic: About External Authentication Providers | Salesforce
About External Authentication Providers | Salesforce
About External Authentication Providers
Available in: Lightning Experience and Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view the settings:
“View Setup and Configuration”
To edit the settings:
“Customize Application”
AND
“Manage Auth. Providers”
You can enable users to log into your Salesforce organization using their login credentials from an external service provider such as Facebook© or Janrain©.
Note
Social Sign-On
 
(11:33 minutes)
Learn how to configure single sign-on and OAuth-based API access to Salesforce from other sources of user identity.
Do the following to successfully set up an authentication provider for single sign-on.
Correctly configure the service provider website.
Create a registration handler using Apex.
Define the authentication provider in your organization.
When set up is complete, the authentication provider flow is as follows.
The user tries to login to Salesforce using a third party identity.
The login request is redirected to the third party authentication provider.
The user follows the third party login process and approves access.
The third party authentication provider redirects the user to Salesforce with credentials.
The user is signed into Salesforce.
Note
If a user has an existing Salesforce session, after authentication with the third party they are automatically redirected to the page where they can approve the link to their Salesforce account.
Defining Your Authentication Provider
We support the following providers:
Facebook
Google
Janrain
LinkedIn
Microsoft Access Control Service
Salesforce
Twitter
Any service provider who implements the OpenID Connect protocol
Adding Functionality to Your Authentication Provider
You can add functionality to your authentication provider by using additional request parameters.
Scope
 – Customizes the permissions requested from the third party
Site
 – Enables the provider to be used with a site
StartURL
 – Sends the user to a specified location after authentication
Community
 – Sends the user to a specific community after authentication
Authorization Endpoint
 – Sends the user to a specific endpoint for authentication (Salesforce authentication providers, only)
Creating an Apex Registration Handler
A registration handler class is required to use Authentication Providers for the single sign-on flow. The Apex registration handler class must implement the 
Auth.RegistrationHandler
 interface, which defines two methods. Salesforce invokes the appropriate method on callback, depending on whether the user has used this provider before or not. When you create the authentication provider, you can automatically create an Apex template class for testing purposes. 
For more information, see 
RegistrationHandler
 in the 
Force.com Apex Code Developer's Guide
.
Configuring a Facebook Authentication Provider
Configure a Google Authentication Provider
Let users log in to a Salesforce organization using their Google accounts.
Configure a Janrain Authentication Provider
Configure a Salesforce Authentication Provider
You can use a connected app as an authentication provider.
Configure an OpenID Connect Authentication Provider
You can use any third-party Web application that implements the server side of the OpenID Connect protocol, such as Amazon, Google, and PayPal, as an authentication provider.
Configure a Microsoft® Access Control Service Authentication Provider
You can use Microsoft Access Control Service as an authentication provider, using the OAuth protocol. Authorization is typically done by a Microsoft Office 365 service like SharePoint® Online.
Configure a LinkedIn Authentication Provider
Let users log in to a Salesforce organization using their LinkedIn account.
Configure a Twitter Authentication Provider
Let users log in to a Salesforce organization with their Twitter accounts.
Using Salesforce-Managed Values in Auth. Provider Setup
You can choose to let Salesforce automatically create key values when setting up a Facebook, Salesforce, LinkedIn, Twitter, or Google Auth. Provider. This allows you to skip the step of creating your own third-party application.
Create a Custom External Authentication Provider
Create a single sign-on (SSO) authentication provider to let admins and users use their non-Salesforce SSO credentials for your Salesforce orgs.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sso_authentication_providers.htm&language=en_US
Release
202.14
