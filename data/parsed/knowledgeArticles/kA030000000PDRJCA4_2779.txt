### Topic: The "Add Email" button does not load in Microsoft Outlook
Salesforce for Outlook
 "
Add Email
" button does not load in Microsoft Outlook
- Salesforce for Outlook
 
Applies to:
 
 
 
Resolution
A few things to check on the Salesforce side before starting to troubleshoot  on the computer
STEP 1:
Make sure 
Email to Salesforce
 is enabled in Salesforce:
Click on 
Your Name
 | 
Setup 
| 
Email Administration
 | 
Email to Salesforce
 
and make sure the 
Active
 box is checked.  If it is not please enable it.  For more information please see 
Enabling Email to Salesforce.
STEP 2: Try Resetting the Outlook customization
First confirm with the user that it is okay to reset their Outlook toolbar. To reset Outlook customization,
Get to the Customize the Ribbon window
Several options exist for this
1. Click File, then Options next select either Customize ribbon or Quick Access Toolbar
or
2. Hover the mouse over Ribbon and right click and select  "Customize the Ribbon"
 
Export a customized ribbon
You can export your ribbon and Quick Access Toolbar customizations into a file that can be imported and used by a coworker or on another computer.
 
1- In the Customize the Ribbon window, click Import/Export.
NOTE :
 To get to the Customize the Ribbon window, see the “
Get to the Customize the Ribbon window
” section.
   
    
2- Click Export all customizations.
Reset the ribbon :
You can choose to reset all tabs on the ribbon or only the selected tabs to their original state. When you reset all tabs on the ribbon, you also reset the Quick Access Toolbar to show only the default commands.
Reset the ribbon to the default settings
you reset both the ribbon and the Quick Access Toolbar to the default settings.
IMPORTANT   When you click Reset all customizations, 
 
1- In the Customize the Ribbon window, click Reset.
NOTE    
To get to the Customize the Ribbon window, see the “
Get to the Customize the Ribbon window
” section
2- Click Reset all customizations.
 
Reset only the selected tab
You can only reset default tabs to their default settings.
1- In the Customize the Ribbon window, select the default tab that you want to reset to the default settings.
NOTE    
To get to the Customize the Ribbon window, see the “
Get to the Customize the Ribbon window
” section.
2- Click Reset, and then click Reset only selected Ribbon tab.
 
Import a customized ribbon
You can import customization files to replace the current layout of the ribbon and Quick Access Toolbar. By being able to import the customization, you can keep Microsoft Office programs looking the same as your coworkers or from computer to computer.
IMPORTANT   When you import a ribbon customization file, you lose all prior ribbon and Quick Access Toolbar customizations. If you think that you might want to revert to the customization you currently have, you should export them before importing any new customizations.
 
 
1- In the Customize the Ribbon window, click Import/Export.   
NOTE: 
To get to the Customize the Ribbon window, see the “Get to the Customize the Ribbon window”section. 
          2- Click Import customization file.
 
STEP 3:
Confirm that the 
Add Email
 is enable for your Outlook Configurations (Salesforce Group and Contact Manager Edition has it enabled within the default Outlook Configuration):
Click 
Your Name
 | 
Setup 
| 
Desktop Integration 
| 
Salesforce for Outlook 
(Alternative click-path: 
Your Name
 | 
My Settings 
| 
Desktop Add-Ons 
| 
Salesforce for Outlook
, click on 
View my configuration)
, and verify that the 
Add Email 
box is checked. You must be a Salesforce System Administrator to be able to view or edit this setting.  NOTE:  Enabling the "Side Panel" option in the Outlook Configuration stops the displaying of the "Add Email" button in the Outlook ribbon by design.
In order to make the Add Email button work and add emails to Salesforce , add the user's email address to My Acceptable Email Addresses.
For more information please see 
Enabling Email Options for Salesforce for Outlook.
STEP 4:
Make sure the 
Salesforce for Outlook
 icon is running in System Tray and it is red and not gray.  The 
Add Email
 button will never display if Salesforce for Outlook is not running in system Tray or it is in the gray state (not connected).
*** Please perform all the steps in the same order listed below and do not open Outlook or SFO during this process. Once you are done with 
Step 6
 then go ahead and open Outlook and SFO and try to reproduce the issue ***
 
STEP 5:
Once you have checked all the settings above, consider that it is possible that either the add-in was disabled by Outlook, the installation failed due to a security software or the user who installed the application did not have local administrator permission on the computer. 
Make sure the add-in is not listed under the Outlook Disabled items, look for
 
- Salesforce for Outlook Add to Salesforce Toolbar
 
- 
Salesforce for Outlook Side Panel
.
 
 
Take the following steps to enable the 
Salesforce for Outlook 
Add-In in 
Microsoft Outlook 2007
    - In Outlook, click on 
Help | Disabled Items…
    - If the Salesforce add in is listed, select it and click Enable.
    - You need to restart Outlook and Salesforce for Outlook for changes to take effect
Take the following steps to enable the 
Salesforce for Outlook 
Add-In in 
Microsoft Outlook 2010/2013
:
    - Click
 File | Options | Add-Ins.
    - Click the 
Manage 
drop-down menu and select 
Disabled Items
 and click on Go...
 
    - If the Salesforce add in is listed, select it and click Enable.
    - You need to restart Outlook and Salesforce for Outlook for changes to take effect
 
** You can click on each of the SFO add-ins in the list of Add-ins and then make sure the location points to the folder that you installed the application. For instance if you have installed it for Everyone (per machine) then the location should point to 
C:\Program Files (x86)\salesforce.com\Salesforce for Outlook 
 and if it is installed with 'Me Only' option (per user) then it should be under user's %LocalAppData%\Programs\salesforce.com\Salesforce for Outlook
  
folder
STEP 6 - Turning off User Account Control 
If the issue persist, and Salesforce for Outlook is listed as Active Application Add-ins in the Outlook Trust Center,  please check the following settings and make the necessary changes:
For Windows 7, Windows 8, and Windows 10 users 
 
1-Click on 
Start 
and type in 
MSCONFIG
2-P
ress the 
Launch 
button
3-Slide it all the way down to "
Never Notify
"
 
4-Restart your computer
5-Open Outlook and Salesforce for Outlook and see if the Add Email button loads
6-
Click on the 
Tools 
tab. Locate the "
Disable UAC
" and select it by clicking 
 
You can also turn off UAC from the registry which is the best way, or even check this entry after you perform the steps above and to make sure the UAC is off correctly.
- Open the Windows Registry Editor and navigate to the following key
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System
- Click on the System folder on the left and on the 
right-hand side, you should see a setting for EnableLUA, which you’ll want to customize as follows:
UAC Enabled: 1
UAC Disabled: 0
 
- Change it to 0 and click on Okay and then restart
 
If Salesforce for Outlook is running, make sure that you have synced with Salesforce using Salesforce for Outlook. The add-in will get its configuration after Salesforce for Outlook finishes sync and ‘Add Email’ button will load in Outlook.  If you have recently made any changes to your configuration, you must sync using Salesforce for Outlook for add-in to pickup latest configurations. You can do this by manually initiating Sync or by waiting for Auto Sync to initiate sync to get latest configurations. To sync manually: Right click on the Salesforce for Outlook system tray icon in the Task bar in the right bottom corner of the screen and click on sync and sync now
 
If none of the steps above resolved the issue for you and the Add Email button is still missing, continue with the steps below
STEP 7 - Finding and deleting the Resiliency key in the Registry
 - Click on 
Start 
and type in 
REGEDIT 
in the Search box or in Windows XP click on 
Start | Run
 and then type in 
REGEDIT 
to open the 
Registry Editor
 - On the left side, click on the Computer icon on top and click on Edit | Find
 - In front of "
Find what
: "  type in 
Resiliency
 and make sure to check the box for "
Match whole   string only
" and click on 
Find Next
 - If the search finds anything, select it, right click on it and click on Export
 - Save it to a folder (You can create a new folder or save it on your Desktop)
 - After you Export it, right click on the yellow 
Resiliency 
folder and click on Delete
 - Press F3 on your keyboard to continue the search until you see "
Finished searching through the registry
"
 - Click on OK
 - Next locate the following registry keys and delete them. Make sure you click on each and then right click and click on Delete (you may see 1 or 2, if none exist skip to step 7)
HKEY_CURRENT_USER\Software\Redemption
HKEY_CURRENT_USER\Software\Redemption**
HKEY_LOCAL_MACHINE\SOFTWARE\Redemption
HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Redemption
 
While you are in the Registry Editor, check to make sure the "
LoadBehavior
" value is set to 3. Sometimes this value changes to 2 that causes the Add Email not to load
If it is set to 2 or any other value but 3, double click on the 
LoadBehavior
 key and change its value to 3 and press Enter
 
HKEY_CURRENT_USER\Software\Microsoft\Office\Outlook\AddIns\SalesforceForOutlook
 
 
- Close the Registry Editor
STEP 8 - Installation and pre-requisites 
These steps need to be performed by your IT department or a user who is a Local Administrator on the computer
This issue may be resolved by uninstalling and reinstalling the 
Windows 7
's or 
Windows 8 
out of the box 
.NET
 installation. On 
Windows XP
 machines, the .NET may need to be uninstalled/reinstalled from the list of installed programs but in Windows 7 and 8, .NET 3.x is listed under Windows Features
 1- Make sure 
Outlook 
is closed
 2- Uninstall 
Salesforce for outlook
 as well as the following applications:
 
*Note
Previous versions of SFO (Version 2.4.2 and earlier) should be uninstalled and installed by the computer Administrator or a user with Local Admin rights on the computer as well as all the prerequisites (Such as VSTO, PIA, etc).
The SFO version 2.5.x through 2.7.x must be installed by the actual user, not a system or admin user.
Please have their IT or Local Administrator to login to the computer and to uninstall SFO 2.4.2 and then have the end user login and install SFO 2.5.x
 
Microsoft Visual Studio® 2010 tools for Office Runtime
Outlook 2010 users: Uninstall the ‘Office 2010 Primary Interop Assemblies’
Outlook 2007 users: Uninstall the ‘Office 2007 Primary Interop Assemblies’
 
3- Then while in the 
Control Panel\All Control Panel Items\Programs and Features
 folder, from the left menu click on the “
Turn Windows Features on or off 
“
4- In the pop-up list, uncheck the box for “
Microsoft .NET Framework 3.5
” and click on ok
5- Wait until uninstallation is finished and then restart the machine.
6- Windows 7 users, log back in as a local admin and turn the box back on to reinstall Microsoft .NET Framework 3.5
Windows XP
 users may need to download and install 
Microsoft .NET Framework’
 the best would be running a windows Update but if you company policy does not allow that you may need to talk to your IT or download it from Microsoft website directly
For Windows XP with Service Pack 2 – 32bit
Microsoft .NET Framework Version 2.0 Redistributable Package (x86)
 
8- After you are done installing the .NET. restart the computer one more time
9- Click on Start and type in %appdata%\salesforce.com\Salesforce for Outlook\ and delete everything in that folder
**NOW apply STEP 6 again -  Keep Outlook and SFO closed during this process **
Note**
 It is possible that the AppData and Application Data folder are hidden by default.
To view hidden files in 
Windows XP
, see this 
Microsoft link
To view hidden files in 
Windows 7, 8, 10 
see this 
Microsoft link 
If you are unable to open any of the links above, here is how you can display hidden files and folders.
- Open Folder Options by clicking the Start button 
, clicking Control Panel, clicking Appearance and Personalization, and then clicking Folder Options.
- Click the View tab.
- Under Advanced settings, click Show hidden files, folders, and drives, and then click OK.
 
 10- Download the Salesforce for Outlook plug-in and run the installer while logged in as local admin
 11- On the installation prerequisites step make sure you check the boxes for all the prerequisite items and click on NEXT
 12- Finish the installation and make sure the Salesforce for Outlook runs in the Taskbar
 13- Open Outlook and configure Salesforce for Outlook using its Wizard (if the wizard does not start automatically, you can start it by right clicking on the Salesforce for icon system tray icon at the bottom right corner of the screen and click on Settings) and the sync should start (you will see the Salesforce for Outlook icon will start spinning in the bottom right corner of the screen)
Change how icons appear in the notification area
Note***
 For 
Windows 7
 users, if the icon is not visible you may have to change your settings, please see this link by from Microsoft to learn how to 
14-During the sync, or after sync is complete you should see the ‘
Add Email
’ button loading in Outlook
 
**Another troubleshooting tip would be to create a new test profile in Outlook under local admin or the affected user login with a no-mail data file and see if SFO works in the new profile.
** Useful links
Create a new e-mail profile
 
15- If it loads, simply log off local admin account and login as the user and see if the button loads
