### Topic: Daily Limit for Email Alerts | Salesforce
Daily Limit for Email Alerts | Salesforce
Daily Limit for Email Alerts
The daily limit for emails sent through email alerts is 1,000 per standard Salesforce license per org—except for free Developer Edition and trial orgs, where the daily workflow email limit is 15 . The overall org limit is 2,000,000. This limit applies to emails sent through email alerts in workflow rules, approval processes, flows
, processes,
 or the REST API.
Available in: Lightning Experience and Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
The limit restriction is based on activity in the 24-hour period starting and ending at midnight GMT. Adding or removing a user license immediately adjusts the limit's total. If you send an email alert to a group, every recipient in that group counts against your daily workflow email limit.
After your org has reached its daily workflow email limit:
Any emails in the workflow queue left over and not sent that day are discarded. Salesforce doesn't try to resend them later.
If a workflow rule with an action and an email alert is triggered, only the email action is blocked.
Final approval, final rejection, approval, rejection, and recall email actions are blocked.
An error message is added to the debug log.
The following items don't count against the workflow email limit:
Approval notification emails
Task assignment notifications
Lead assignment rules notifications
Case assignment rules notifications
Case escalation rules notifications
Force.com sites usage alerts
When workflow email alerts approach or exceed certain limits, Salesforce sends a warning email to the default workflow user or—if the default workflow user isn't set—to an active system administrator.
When...
Salesforce Sends...
Warning Email Includes...
An email alert isn't sent because the number of recipients exceeds the limit for a single email
A warning email for each unsent email alert
The unsent email alert’s content and recipients
The organization reaches 90% of the limit of emails per day
One warning email
The limit and the organization's usage
The organization reaches 90% of the limit of workflow emails per day
One warning email
The limit and the organization's usage
An email alert isn't sent because the organization reaches the limit of emails per day
A warning email after every 100 attempted email alerts over the limit
The limit and the organization's usage
An email alert isn't sent because the organization reaches the limit of workflow emails per day
A warning email after every 100 attempted email alerts over the limit
The limit and the organization's usage
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workflow_email_limits.htm&language=en_US
Release
202.14
