### Topic: Describes expected behaviour and workaround for Rich Text fields within Wave Analytics.
Wave Analytics does not support Rich Text fields.
Resolution
Rich Text fields can be included in datasets, but will behave unpredictably when queried/displayed.
If you need to include the contents of a Rich Text field, we recommend creating a custom field that contains the same plaintext content and including the plaintext field in Wave datasets.
