### Topic: Households list views are managed by adding or updating reports in the Household Reports folder.
When you would like to add or update a list view for Households (used in the Financial Services Cloud Package), simply add a new report or update the existing report in the Household Reports folder. 
Note that it is not necessary to add a filter for Account Record Type = Household as this filter is applied automatically. 
Resolution
If you are seeing results in the report, but not in the list view, it is likely that the filter for Account Record Type = Household has been added to the report. Remove this filter, and then the list view will return the same results as the report. 
