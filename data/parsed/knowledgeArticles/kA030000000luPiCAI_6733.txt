### Topic: The new sandbox copy architecture is being deployed in stages, beginning in Summer '15.
The new sandbox copy architecture is only deployed to Full sandbox copies in Summer 15. Future releases will include Developer, Developer Pro, and Partial Copy sandboxes.
The NA0 production instance will not receive the new sandbox copy architecture.
 
Resolution
