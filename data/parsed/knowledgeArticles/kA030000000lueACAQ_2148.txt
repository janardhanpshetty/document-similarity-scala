### Topic: What are Super Users (aka Salesforce Champions)? Why do you need Super Users? What should the structure look like? This article answers these questions to help you achieve high adoption and satisfaction Salesforce.
Super Users, or Champions, are essential to any Salesforce project as their involvement and commitment to the project will help to achieve high user adoption and user satisfaction.
The attached document describes the following:
What is a Super User / Champion Program?
Why do you Need a Super User Program?
Development of your Champion Strategy & Structure
Deciding on Goals & Objectives
A Super User's Role
A Super User's Qualifications
Different Champion Program Structures
Lessons Learned
 
Resolution
