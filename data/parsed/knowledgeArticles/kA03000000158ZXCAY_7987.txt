### Topic: Running a report on hashtag topics does not show some of the expected records. This may occur when the hashtag topic is edited or one of the comment's hashtag is edited.
When running a report on Topics, it does not show some of the expected records. Topics are using the hashtags and the record feed has the correct tagging.
Resolution
This is due to the topic being edited on the record.
For example, if you edited a feed post or comment and change the previous topic you included there, then the post will not be recognized in the report.
In order to correct this, you will need to edit the post/comment and post it again that includes the updated Topic so it will show up on the report.
This information applies to Hashtag topic and not Topics
