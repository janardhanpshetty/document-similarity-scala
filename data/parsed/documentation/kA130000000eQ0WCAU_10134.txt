### Topic: Home Helps You Take Charge of Your Day | Salesforce
Home Helps You Take Charge of Your Day | Salesforce
Home Helps You Take Charge of Your Day
The Home page gives you instant access to the most pressing items on your to-do list. What are your priorities today? What’s next? What’s left to do? The Assistant prepares you for the most productive day possible, with proactive alerts about newly assigned leads, and opportunities requiring attention.
Available in Lightning Experience for an extra cost in: 
Enterprise
 and 
Unlimited
 Editions
The dashboard (1) shows how opportunities look for the quarter.
The Assistant (2) helps you be proactive with alerts about leads assigned to you today, and opportunities requiring attention.
The Tasks component (3) shows upcoming tasks. Mark them complete right on the page. As long as you remain on the page, you can always reopen completed tasks. Refreshing the page removes completed tasks from view.
Upcoming Events shows the next five items on your calendar today. Past events drop off as the day progresses.
Birthdays lists the clients celebrating birthdays in the next seven days.
Opportunities and Leads components help you track ways to deepen and grow your book of business.
For now, the Home page layout isn’t customizable.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fsc_home.htm&language=en_US
Release
202.14
