### Topic: Extend Wave Everywhere | Salesforce
Extend Wave Everywhere | Salesforce
Extend Wave Everywhere
Incorporate Wave Analytics throughout your business. The Wave Analytics visualizations you’ve built are more powerful when you share them across your Salesforce experience by integrating them into custom pages, Visualforce pages, community sites, and more. More ways to share include packaging assets, presentation mode, annotations, and downloading filtered data from Wave. In addition, custom menus in lenses and dashboards allow you to perform common Salesforce actions directly from Wave.
Embed Dashboards Everywhere
Embed Wave dashboards in every Salesforce experience. From embedded dashboards, users can explore and click to linked assets. If you set them up with a Share icon, embedded dashboards offer Post to Feed and Download sharing options.
Wave Migration, Packaging, and Distribution
You can migrate Wave assets using change sets, bundle them together in managed packages, distribute and track packages through AppExchange and the License Management App, and use the metadata API to manage customizations for your org.
Collaborate with Dashboard Annotations
Annotate dashboard widgets with comments posted in the dashboard and in Chatter. With annotations, you can hold conversations about your data and how it’s visualized, with the dashboard right there for reference.
Present Live Dashboards and Lenses
Run meetings directly from Wave with the new full-screen presentation mode. By presenting Wave assets instead of slides with static images, you have real-time access to your data, and you can showcase dynamic visualizations.
Enable Actions and Links With Custom Actions Menus in Lenses and Dashboards
Custom menus in Wave Analytics lenses and dashboards let users take advantage of Salesforce actions and open records from Salesforce or other websites.
Download Images and Filtered Data
Download filtered data from lens explorations and dashboard widgets. Download formats include image (
.png
), Microsoft® Excel® (
.xls
), and comma-separated values (
.csv
) files. This feature downloads the results of a displayed query (or step).
Incorporate Visualizations through Chatter Posts and Sharing Links
Share a visualization with your colleagues by posting to Chatter, or by getting its unique URL. A Chatter post provides an image and a link to the asset—lens, dashboard, or app—in Wave Analytics. Colleagues with the link and access to the asset can drill in and explore the information that’s presented.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_incorporate.htm&language=en_US
Release
202.14
