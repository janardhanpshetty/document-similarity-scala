### Topic: How to check if there is a delay at Salesforce end while creating cases using Email to Case.
Customer sends an email to their Email to Case address for example : support@acme.com and notice that the case gets created with a long delay.
Resolution
Try sending an email directly to their 'Email Services Address' and verify if the case gets created without a delay. (Also check the Email-to-Case Routing Information to verify if this email is set to accept emails only from some particular emails addresses defined under 'Accept Email From' field. If yes, include your email address in the list and remove it after testing is done (With customer's approval))
'Email Service Address' is the long email address that you will find next to the Email to Case Email Address.
* If a case gets created without any delays, you may inform the customer to check if they are any delays on their Mail Server delivering emails to Salesforce.
* If the case gets created with delays, there could be some delays at our end. This can be verified by testing the same using your test org / Checking if anything is posted on TRUST / Verifying with the Backline team of any known delays at that particular time or Running a query in Splunk to get the timestamp on which we received the email in Salesforce and created the case.
