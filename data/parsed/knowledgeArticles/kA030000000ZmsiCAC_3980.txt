### Topic: A guide on how to acquire the value stored in the Campaign Member Report labeled,"Member First Associated".
When running Reports on Campaign Members you may notice a field labeled"Member First Associated Date". As one can probably guess ,based on the field name, this field displays the Date the Lead/Contact was first added as a Campaign Member. This field only displays within Reports but often times it may be useful for additional tracking or external Usage. 
Resolution
To access this field value:
Create a Custom Formula Field on the Campaign Member object.
Specify Date Field as the Formula Type.
When inserting a field to reference, simply select the "Created Date" field available under Campaign Member fields.
This field will display the Created Date of the Campaign Member Record itself. Which is the same Date value stored in the Member First Associated field within Reports. 
