### Topic: This article contains which requires modification in the Email Agent Folder to be able to connect Email to case to a public folder.
By default, Email-to-Case Agent allows you to connect to your inbox folder. You can modify the sfdcConfig.txt file to connect the agent to a public folder
Resolution
To connect Email-to-Case Agent to a public folder, you would need to modify the Email2case.txt file that is located in the Email Agent folder.
 
It is a text file and you can simple edit it using the Notepad application (Start | All programs | Accessories | Notepad)
Locate the following 3 lines in the Email2case.txt file
 
Default values
<inbox>Inbox</inbox>
<readbox>ReadBox</readbox>
<errorbox>ErrorBox</errorbox>

  
Change them to the following

<inbox>
Public Folders/PublicFolderName
</inbox>
<readbox>Public Folders/PublicFolderName/
Read
</readbox>
<errorbox>Public Folders/PublicFolderName/
Error
</errorbox>
Basically, you need to type in the word 
Public Folders 
and then / and then type in the name of the public folder. You would also need to create 2 child folders inside that public folder. (The <readbox> and <errorbox> folder names are case sensitive)
 
In the example above, I have used 1 public folder called ‘PublicFolderName’ with 2 sub folders
If you decide to use a different public folder on the root level instead of a child folder in your public folder, you can use something like this
 
<inbox>Public Folders/PublicFolderName_1</inbox>
<readbox>Public Folders/PublicFolderName_1_Read</readbox>
<errorbox>Public Folders/PublicFolderName_1_Error</errorbox>
 
In the example above, I have used 3 different public folders that are on the root level. Again keep in mind these names are case sensitive and you need to type the name the exact same way you created them in Exchange or Outlook.
 
Sample Email2case.txt file:
<configFile>
   <server1>
       <url>mail.company.com</url>
       <protocol>IMAP</protocol>
       <port>143</port>
       <userName>routing address email user</userName>
       <password>password</password>
       <interval>1</interval>

       <inbox>Public Folders/Support</inbox>
       <readbox>
Public Folders/Support/Processed_Email
</readbox>
       <errorbox>
Public Folders/Support/Error_Email
</errorbox>
  
   </server1>
</configFile>
 
 
