### Topic: This article explains why a customer hits the error "This Group Unique Name already exists or has been previously used" and how to resolve.
I am trying to create a Data Category group with a particular name and I am getting the error "This Group Unique Name already exists or has been previously used. Please choose a different name." I have created a data category group and deleted it as I created the wrong children. How can this issue be resolved?
Resolution
For T2 : 
This issue can happen when an administrator user tries to create and delete the same category group name more than two times.
This is what happens. When the category is deleted the developer name becomes suffixed with "_del". 
1) Create category ABC
2) Delete it. It becomes ABC_del
3) Create category ABC. It works because there is no group named 'ABC' anymore since the first one got renamed. 
4) Delete it. It stays 'ABC' because ABC_del exists.
5) Create category 'ABC'. It fails because 'ABC' already exists. 
The issue will persist even if the administrator purges the deleted the category group in the UI. The same unique name can be used only after 
Physical Delete process
 completely removes the deleted entities from database. Following would be the options for the customer.
1) Choose a different name for "Group Unique Name", or
2) Wait for some time so that our periodic Physical Delete process removes the previously deleted category group entitiy from the database. It can take 1 ~ a few days.
3) If the customer can't wait and they have to use the same unique name, then escalate to T3 to run the manual physical Delete.
For T3 : 
Run PD for the entity "
0D1
" and verify that this entity is purged after PD completed by drilling down "DeleteExpiredSoftDeletedData".
