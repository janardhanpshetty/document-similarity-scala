### Topic: This is for taking users of Salesforce off of our mailing lists. Usually when they leave Salesforce.
When a user of Salesforce asks to be removed from our mailing lists, this is what we can do:
 
Resolution
Updated as of 1/21/2015: 
**EXTERNAL**
Refer customer to preference center to update their subscription preferences: https://org62.my.salesforce.com/ka230000000ZVno
**INTERNAL**
Click the “Opt-Out from Sales & Marketing Email" custom link towards the bottom of the Lead/Contact record.  The link is an auto-generated form submission that processes the unsubscribe request.  The update may take up to a few hours to reflect on the record.  
 
For any questions, please contact Hollie Bohmaker 
hbohmaker@salesforce.com
 or Michelle Yass michelle.yass@salesforce.com
[Do contribute to this article if there are any other ways to remove customers from Salesforce mailing lists]
