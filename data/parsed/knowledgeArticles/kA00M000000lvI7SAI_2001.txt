### Topic: This article helps adding more 255 charater using Multi - Select Picklist field in worklow rule criteria.
Characters can hold by a multi-select picklist field when using it as a criteria in Workflow Rule. 
I'm creating a Workflow Rule and I will use a multi-select picklist field on the Rule Criteria but when saving it, I can't add all of the values in the multi-picklist field. 
Resolution
The limit of characters that can hold by a Workflow Rule criteria is
 255 characters
 including spaces. You can use these practice if you will encounter this situation when adding a value on the rule criteria.
1. Since there are 10 rows that you can use in Workflow Rule. You can start using the field multiple times.
For example :
Multi-Picklist equals 
(the values of the field up to 255 characters)
 If the first criteria can't hold all of the values that you'd like to add. Add another criteria.
Multi-Picklist equals (the values from the first criteria that is not added due to limit of the characters)
Then use a filter logic OR statement 
(1 OR 2)
.
Or 
Use the
 Formula
 to declare of the values that you want to use:
OR(INCLUDES(multi-picklist, "value1",
INCLUDES(multi-picklist, "value2",
See Also:
Set the Criteria for your Workflow Rule
 
