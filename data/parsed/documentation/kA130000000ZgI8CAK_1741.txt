### Topic: Configure Thanks in the Chatter Publisher and Salesforce1 Action Bar | Salesforce
Configure Thanks in the Chatter Publisher and Salesforce1 Action Bar | Salesforce
Configure Thanks in the Chatter Publisher
 and Salesforce1 Action Bar
You can change the position of the Thanks action in the Chatter publisher and the Salesforce1 action bar, or even hide Thanks for specific profiles.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set up actions:
“Customize Application”
To modify the Chatter publisher, actions in the publisher must be enabled.
From Setup, enter 
Chatter Settings
 in the 
Quick Find
 box, then select 
Chatter Settings
.
Confirm that 
Enable Actions in the Publisher
 is selected in the Actions in the Publisher section. If it isn’t enabled, click 
Edit
, select 
Enable Actions in the Publisher
, and click 
Save
.
From Setup, enter 
Publisher Layouts
 in the 
Quick Find
 box, then select 
Publisher Layouts
.
Click 
Edit
 next to the Global Publisher Layout.
Drag the 
Thanks
 action to where you want it to appear in the Chatter publisher or the Salesforce1 action bar.
Click 
Save
.
You can hide Thanks for specific users by changing profile visibility settings.
See Also:
Thanks and Skills Features
Assign Publisher Layout to Profiles
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workcom_impl_thanks_publisher.htm&language=en_US
Release
202.14
