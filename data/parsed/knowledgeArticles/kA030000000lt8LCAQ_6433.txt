### Topic: This article provides an overview about the Spring 15 feature "Add Emails to Multiple Salesforce Records That Accept Tasks".
Add Emails to Multiple Salesforce Records That Accept Tasks
With SFO version 2.7.0 and newer you can now add emails to multiple Salesforce records that accept tasks, such as 
Accounts
, 
Opportunities
, 
Custom Objects
 and 
Cases
—not just 
Contacts.
This means that you are no longer limited to adding emails to only one record that accepts tasks.
Before Spring 15, Salesforce Administrators could enable the "
Allow Users to Relate Multiple Contacts to Tasks and Events
" feature from 
Setup | App Setup | Customize | Activities | Activity Settings
 to allows users to relate up to 50 Contacts to a Task or Event (except a recurring one) but with Spring 15 Release, the above feature does not even have to be enabled if you decide to enable the perm below. Users will be able to associate emails to multiple records.
Salesforce creates separate tasks for each record to which users add emails. 
 
Resolution
This feature is not enabled by default. To get this feature enabled, the "
Lets Salesforce Side Panel users add an Outlook email to multiple Salesforce records that accept tasks. Salesforce creates a task for every record to which users add emails
" perm needs to be enabled on the back end by Salesforce support
 
