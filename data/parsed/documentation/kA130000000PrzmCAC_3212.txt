### Topic: Working with Assigned Apps in Permission Sets | Salesforce
Working with Assigned Apps in Permission Sets | Salesforce
Working with Assigned Apps in Permission Sets
Available in: Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Professional
, 
Group
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view assigned app settings:
“View Setup and Configuration”
In permission sets, the Assigned Apps page shows the apps that are visible to users with the selected permission set. On this page, you can:
Change the assigned apps
Search for permissions and settings
Create a permission set based on the current permission set by clicking 
Clone
If it's not assigned to any users, remove the permission set by clicking 
Delete
Change the permission set label, API name, or description by clicking 
Edit Properties
View and manage the users assigned to the permission set by clicking 
Manage Assignments
Go to the permission set overview page by clicking 
Permission Set Overview
Switch to a different settings page by clicking the down arrow next to the Assigned Apps name and selecting the page
See Also:
Permission Sets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=perm_sets_assigned_apps.htm&language=en_US
Release
202.14
