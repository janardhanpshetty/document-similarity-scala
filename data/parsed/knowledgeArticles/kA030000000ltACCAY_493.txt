### Topic: INTERNAL ONLY: Knowledge article that summarizes the technology involved in the Model N Revvy CPQ application
These articles were created to give a high level overview of a PPS partner's package(s) and application(s), with the hope that partner support agents don't have to ask partners for this information before working on their support cases, and to help increase resolution times and CSAT. For more specifics and usage FAQs, click 
here
. 
AppExchange Listing Information:
 
Partner:
 Model N (PARTNER MAIN)**
Application Name: 
Revvy CPQ
 
AppExchange Listing Name / URL:
 
REVVY CPQ
https://appexchange.salesforce.com/listingDetail?listingId=a0N3000000B4fMxEAJ
  
 
62 Org Partner Account: 
https://org62.my.salesforce.com/00130000016k5MM
 
PPS Entitlement:  
https://org62.my.salesforce.com/a0a3000000EEGdh
 
 
Application Overview:
 
Combined Product Description:
 
Application Overview
Revvy is an Config, Price, Quote application - it is mainly designed to allow Sales Reps to create quotes for opportunities based on company's pre-determined pricing and product configuration rules.
Admin configures the Prices and Configuration information for the Catalog, there is a entire editor for them to setup up the price books (not related SFDC), pricing rules, product to help setup the catalog.
Sales Reps can then start a quote from the quote object (not SFDC related) or Opportunity (not dependent) and create a quote using a shopping-cart style interface.
As the reps creates quote and determines disscounts, they may encounter the need for approvals, which is when Manager would need to approve
Architecture
Managed Package
Uses Accounts and Contacts; optionally, it can be setup to used with Oppty, but it is not dependent
Apex Callout and REST API used for integrating with a Config/Validation engine built by ModelN hosted on AWS - details below
API Calls shouldn't be a concern because it is only used when querying the data of the pricing, catalogs, and rules during a "model" configuration; it won't be used during when Sales Reps are quoting.
VF/Apex used with many Custom Objects
It uses a very custom UI leveraging require.js and hooks into SFDC using Javascript Remoting
No Batch
Config Engine Description
Built on Node.js hosted on AWS
It is stateless, so when an Admin builds the model of the pricing rules, the engine calls SFDC for the raw data to "compile" the model in a special format only this engine can read.
The "model" file is then sent back to SFDC to be stored.
When a use initiates a configuration, the "model" file is sent to the config engine and initialized.
Upon each configuration and change (adding product, optionals, pricing, etc...) it calls the engine to see if it is following the correct rules.
Once the configuration is done, the instance of the "model" will eventually be destroyed, therefore, then engine doesn't persist any SFDC data.
 
Distribution Details:
 
Target Market Segment: N/A
Sold to: Both: existing customers & net-new customers
Distribution Mechanism: Trialforce, AppExchange
 
 
 
Target User Demographic:
 (pulled from licensing - user subscription) related list in ISV TE org 
 
Administrator - User creates products, creates pricing for products, creates configuration models (i.e. create rules for restricting configuration of sub-items in a quote)
Sales Manager - User approves a quote submitted for approval, views reports
Sales Rep - User creates a quote and adds line items to a quote for pricing information. User configures quote line items per customer's requirements and is restricted by rules created by Admin users. User provides a PDF proposal to customer
 
Application Technology Overview:
 
Marketecture Diagram:
 
Solution Components:
 
Standard Objects Referenced: Contact, Account, Opportunity (NOT dependent, can be optionally referenced)
Number of Custom Objects: 31-40
Number of Reports: 1-10
Number of Classes: 100+
Number of Triggers: 21-30
Number of Visualforce Pages: 41-50
ISVforce technology leveraged: Managed Package; Aloha; API Token; Push Upgrades; LMA; Trialforce; Branded Login
Is Force.com primary system of record: YES
Other platforms used: N/A
Has managed package: YES
Has composite Web App: NO
Has native mobile app?: NO
100% native on Force.com Platform: NO
Type of Application: 80%
 
Force.com
 with callouts to a Config engine
Supported Editions: Enterprise, Unlimited / PXE
Approved for install into existing orgs? YES
Has Extension Packages? NO
Has Composite Web Services?: YES
Has Desktop App: NO
 
UI Architecture:
 
Supported UI Types: Desktop Browser-Based; Mobile Browser-Based
Mobile Devices Supported - N/A
Developer tools used for browser UI: Visualforce/Apex; JS frameworks (JQuery, ExtJS, etc.)
% of app not using SFDC standard look / feel: 90%
 
Integration Methods:
 
Integration at UI layer: YES
UI Integration Methods: N/A
Realtime integration required: YES
Realtime integration methods: Inbound; Outbound; Visualforce UI + Apex callouts
Avg number of API calls in 24 hour period: Thousands
Batch Integration Required: YES
Batch Integration Frequency: Once a month
Avg number of records per batch: Millions
Batch ETL Methods: Custom code hosted by ISV
 
Large Data Concerns:
 
More than 20MB of data per user: YES
Largest data set stored on single object: Thousands
Describe queries run on large data sets: N/A
Reporting on large data sets with SFDC tools? NO
Data Volume Concerns? N/A
 
Sample UI Screenshots:
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Resolution
