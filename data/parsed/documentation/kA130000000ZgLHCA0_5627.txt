### Topic: Custom Opportunity Field Forecasts Overview | Salesforce
Custom Opportunity Field Forecasts Overview | Salesforce
Custom Opportunity Field Forecasts Overview
Track and predict revenue beyond the standard opportunity Amount field.
Available in: Salesforce Classic
Available in: 
Performance
, 
Unlimited
, 
Enterprise
, and 
Developer
 Editions
Note
This information applies to Collaborative Forecasts and not to 
Customizable Forecasts
.
You and your sales teams can forecast on custom currency fields on opportunities. For example, your sales teams might use a custom field such as Margin, Monthly Recurring Revenue, Annual Contract Value, or any other currency field that your organization requires. Your administrator can easily set up forecasts based on any of those custom fields, which sales managers can then view in the Forecasts tab like any other forecast.
If you use opportunity splits with your custom field, your forecast incorporates those splits. You can use a custom field forecast by itself or with other forecasts, such as an opportunity-revenue forecast based on the opportunity Amount field.
 In addition to custom currency fields, your administrator can also set up a forecast for the standard Expected Revenue field.
Note
If the forecast manager or sales rep viewing the forecast does not have access to the custom opportunity field, they can see forecast amounts but they won’t be able to see any of the custom field values in the opportunity list.
Forecasting on Annual Contract Value—a Simple Custom Field Forecast
Here we see an opportunity owned by sales rep Anna Bressan. Anna’s organization has a lot of multi-year deals, so they use a custom Annual Contract Value field to track the revenue received from the deal each year.
Anna owns this opportunity.
This is the custom Annual Contract Value field.
The Annual Contract Value field in all of Anna’s opportunities rolls up into her Annual Contract Value forecast, which appears in the Forecasts tab along with any other forecasts that are set up in your organization.
This is the Annual Contract Value forecast for Anna.
This forecast has the same name as the custom field.
The Forecasted Amount is the value of the Annual Contract Value field that rolls up into Anna’s forecast.
This is the Forecasted Amount from the Annual Contract Value field on Anna’s Acme – Premium Support opportunity.
Note
To enable a custom field forecast, your administrator also has to enable opportunity splits for the custom field. However, the opportunity owner automatically receives a 100% split for the field, so if your organization doesn’t plan to use splits with the field, the rollup is the same as if there were no splits.
A Custom Field Forecast with Opportunity Splits
If you use opportunity splits on your custom field, your custom field forecast rolls up those splits. For example, here we can see that sales representative Anna owns an opportunity, which includes a custom split on the Annual Contract Value field for Kevin, a sales engineer responsible for increasing overall Annual Contract Value for the company. In this case, Kevin’s 25% split will roll up into his Annual Contract Value forecast.
Anna owns this opportunity.
The opportunity contains a custom currency field called Annual Contract Value.
Kevin receives a 25% split on the Annual Contract Value field.
Kevin’s manager can view the Annual Contract Value forecast where he can see the deal to which Kevin is contributing, along with all of the other opportunities his sales engineer team is working on. Kevin’s manager can see that Kevin is receiving a 25% credit for all of his opportunities, which contribute to a total of $1,026,000 revenue in his Best Case forecast. If Kevin’s manager thinks the deal will ultimately close for a higher amount, his manager can adjust the Best Case forecast amount upward. Anna’s manager can see Anna’s 75% split in her forecast.
This is Kevin’s Annual Contract Value split forecast.
This is Kevin’s 25% split on Anna’s Acme – Premium Support opportunity.
Forecasts from the Standard Expected Revenue Field
The standard Expected Revenue field is useful for opportunities that are likely to produce more or less revenue than indicated in the Amount field. If your organization uses the Expected Revenue field, your administrator can set up a forecast for it as well. The Expected Revenue forecast then appears in the Forecast Type menu in the same way as a custom field forecast.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=forecasts3_understanding_custom_field_forecasts.htm&language=en_US
Release
202.14
