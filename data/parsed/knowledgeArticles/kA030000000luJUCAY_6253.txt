### Topic: This is the process to get support to enable Case Duration/Age in Business Hours feature.
Available in:  All Editions
The activation of this feature exposes the "
Business Hours Age
" field to applicable Case object reports.
This feature allows a field to be included within case reports that supplies case age based on the organization's defined business hours instead of a full 24 hour day.
Resolution
To request Salesforce.com Support to enable Case Duration/Age in Business Hours feature, please take the following steps:
1-
 Have a System Administrator to log a Case with Salesforce Support
2-
 Please mark as "feature activation" on the General Application Area
On the case description, please specify:
Feature requested: Case Duration/Age in Business Hours
Organization ID where you want the feature to be enabled (Navigate to Setup > Company Profile > Company Information):
I am the system admin in charge of this feature.
3- 
Our Support Team will review the Case and action the request as needed.
Please note: 
If a user has their access to the Closed Date/Time field limited through Field Level Security or Field Accessibility then this field will not be available for use.
Also, this will only calculate the amount of time for the Default Business Hours, regardless of which Business Hours is chosen on the Case.
