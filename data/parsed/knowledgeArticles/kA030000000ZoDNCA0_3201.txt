### Topic: Learn why Marketing Cloud Inbox Tools Email Client Monitor has no new Data Points entering the tool.
Data Points have stopped loading into Email Client Monitor, even though the messages have the pixel included in the HTML. 
Resolution
Email Client Monitor has a strict limit of 2 million data points per month. Once 2 million data points have been reached, then no more will be aggregated for the remainder of that calendar month. 
This is a strict limit in place by Return Path, and cannot be altered. It's not possible to purchase additional monthly data points.
