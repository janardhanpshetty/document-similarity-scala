### Topic: When accessing certain list views through Saelsforce1 a user might receive error: Error received: "The Listview with Id {XXXX} and label 'XXXX' has the following fields are null: SobjectType"
When accessing certain list views through Saelsforce1 a user might receive error:
Error received: "
The Listview with Id {XXXX} and label 'XXXX' has the following fields are null: SobjectType
"
This happens on certain orgs  most likely due to timing reasons.
Resolution
**For Tier2:
There are two options:
1) Customer self fix: For a listview whose sobject type is null an error message is displayed in Lightning. The List View can be edited and saved in Aloha to populate that value. The customer needs to switch Aloha, view the List View, edit the List View, and Save the List View. No other changes need to be made. This will fix the issue. 
2) Escalate to tier 3 to have a new scrutiny run:
Scrutiny - the ability for our tier 3 support folks to fix the sobject type null to non-null value
**Tier 3 background info below
: 
SObjectType is a field brought in 2014 fall release. All new orgs created after this release automatically populate SObejctType according to cListView.sql. There was a db script run during 2014 fall release, to back fill SObjectType for all existing orgs created before the release. 
For some reason, most likely timing reason, the db script wasn't run on these some orgs. 
To fix this, a scrutiny was created to fix orgs that report these issues:
Lists->SObjectNullScrutiny. You must provide an OrgId in the Organization Id field.
example of return:
Organization Id: XXXXXXXXXXXXX
Maximum quick rows: 100
Tasks:
    SObjectNullScrutiny: Run-fix-COMMIT
SObjectNullScrutiny: found 64, fixed 64, remaining 0, commit in 0 sec
From BUG: W-3025330
