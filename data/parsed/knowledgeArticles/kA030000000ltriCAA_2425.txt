### Topic: Push Upgrade fails with "References to documents in personal folders and unfiled public documents are not supported. Note that documents are not copied to sandbox" error. This is working as designed when trying to push upgrades to Dev/Dev-Pro Sandboxes if the package has document references from Production.
Issue:
When trying to use push upgrades for a patch version of a package, the following error can occur:
"
Referenced document "
(document name)
" not found. References to documents in personal folders and unfiled public documents are not supported. Note that documents are not copied to sandbox."
The problem does not occur in a Full Sandbox but will/does occur in a Developer or Developer-Pro Sandbox. 
Explanation
: 
The underlying issue is that by-design both Developer and Developer-Pro Sandbox copies do not include documents during a sandbox copy whereas the Full version does. 
Therefore if a pre-installed package gets copied to a Developer Sandbox and it happens to reference any documents, those references will be unresolved from within the Sandbox. 
Thus, when the Developer Sandbox was copied from the source org, the package was copied to the Sandbox org; but the supporting documents for the package were not copied since it is a Developer Sandbox. 
During attempts to upgrade the package within the Sandbox, it fails because the document ids cannot be found within the Sandbox. (i.e. these files do not exist in Sandbox) 
This behavior is considered 
working as designed
 since the Sandbox copy being used is a Developer licensed Sandbox which does not include documents during the Sandbox copy. 
Later upgrades of the package from within the sandbox are likely to fail if any of the package components are referencing required documents that don't exist within the Sandbox. 
 
Resolution
Workaround
Uninstall the previous version of the package from the Developer Sandbox and subsequently re-install the later desired version of the same package. 
This re-installation will also install all required documents along with the package. 
Subsequently, further package upgrades of the package should work since the documents will then be available in the Sandbox during the package upgrade. 
As mentioned above, they can use a Full Sandbox instead to avoid these types of problems in the future.
