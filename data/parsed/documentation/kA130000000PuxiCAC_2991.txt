### Topic: Sort List Views | Salesforce
Sort List Views | Salesforce
Sort List Views
Lots of objects let you view records in lists, also called “list views.”
 If your list is long, you can sort the records by one of the field columns. For example, you can sort the All Accounts list view by the 
Account Name
 field column, 
Billing State/Province
 field column, and others. You can also sort custom list views. Sorting is alphanumeric.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions
User Permissions Needed
To sort a list view:
“Read” on the records in the list
Open the list view. 
Click the header for the field column you want to sort by. 
An arrow appears indicating how the list is sorted: from the column’s first record (
) (alphanumerically) or its last (
).
Note
Starting in Spring ’13, you can’t sort list views for the Users object in Salesforce orgs that have more than two million users.
See Also:
Create a List View in Salesforce Classic
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=basics_sorting_list_views.htm&language=en_US
Release
202.14
