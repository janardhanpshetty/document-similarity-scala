### Topic: Emails sent to the routing address do not create cases
ISSUE
Sending an email to your Salesforce Email-to-Case On Demand routing address does not create a case in your Salesforce org. 
Resolution
Before you can determine the root cause of this issue, you need to try a few things to see what exactly is causing these emails not to create new cases
When dealing with On-Demand Email to Case, you have 2 different email address for each Routing address. One is the actual email address that own and another one that is created in your Salesforce org for your email address called the Email Services address
You can see these under 
Setup | Customize | Cases | Email-to-Case
 in the Routing Addresses section
Step 1
We need to find out if these sent emails reach Salesforce servers, to do this send an email to the Email-to-Case long Email Services address to confirm if cases are created.  Simply compose a new email and copy paste the long email services addresses and send the email with a Test or similar subject with a time stamp in the body of the email so it would be easier to identify the email. Wait a few seconds and then login to your Salesforce org and
 go to Cases tab and look for the new case.
 
Step 2
A new case is created
Check your email client settings to confirm that the forwarding is setup properly and all the emails sent to your Email-to-Case Routing Address are forwarded to the Email-to-Case Email Services Address.
 
No new case is created
**Note: This customization is only available in: Professional, Enterprise, Unlimited, and Developer Editions.
 
If no case is created then most probably it is a configuration issue rather than an email delivery or forwarding issue. You also need to check with your Salesforce Administrator to make see if it ever worked or stopped working at some point.
 
1. First thing to check would be to verify if the 
Automated Case User
 has access to all the record types used for the Email-to-Case configuration:
To do this, login to your Salesforce org with a Salesforce Administrator account
- We need to find out what record types are being used by each the routing addresses so simply click on 
Setup | Customize | Cases | Email-to-Case
 in the Routing Addresses section and click on each routing address and take note of the Record type
- Next navigate to 
Setup | Customize | Cases | Support Settings
 and click on the username link in front of the 
Automated Case User 
to view the user detail page. We need to view that user's Profile and there are different ways to view it depending on your org settings
- 
On the Profile page for th
is user, scroll down to the 
Record Type Settings 
section
 and verify that all the Record Types used with Email to Case routing addresses are assigned to this profile for the Cases object. If not simply click on the [ 
Edit
 ] link in front of the Cases and add the necessary or all the record types.
- Click on Save and try to send another email and check to see if a case gets created
 
Step 3
In addition, check the Automated Case User's mailbox for any failure emails received that explains why Email to Case failed to create a case in salesforce. The failure email notifications will be sent from 
support@salesforce.com
 as its sender and will give a short explanation of the issue, for instance the name of the Validation Rule or Trigger that prevent the email from creating a case due to a missing value for a required field on the Case page layout.
Step 4
You can use the information in Step 3 above to check the settings in your org to find the problem.
1- For instance, start with checking for any Validation Rules, Workflow Rules and Triggers.
If you encounter an error message, confirm if it’s related to a validation rule. If it does, deactivate the rule for a minute and try to create the case using the Email-to-Case Service Email address. 
2-
 
Check for any required fields that could possible prevent the email to create a case. An email sent to the Email to Case routing address would only contain values for certain fields. You can see these fields under 
Case Settings
 in the
 Email-to-Case Routing Information 
page
The 
Case Priority
 field, 
Case Origin
, and 
Case Record Type
 values are prepopulated once the email hits the Salesforce server based on your Case Settings. Any other required fields on the Case page layout would cause problems. 
You can try and create a case manually to replicate the scenario and leave the Contact and Account information blank. 
 
3- You can also run a 
Debug log
 to see what Workflow and/or Triggers are being triggered during the process of creating a case.
- 
Setting up Debug Logs
 
 
