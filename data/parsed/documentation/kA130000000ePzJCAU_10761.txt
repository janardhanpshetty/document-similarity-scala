### Topic: Add Holdings to an Investment Account | Salesforce
Add Holdings to an Investment Account | Salesforce
Add Holdings to an Investment Account
Enter purchase history and details about financial holdings in an investment account.
Available in Lightning Experience for an extra cost in: 
Enterprise
 and 
Unlimited
 Editions
When you add a holding, you can select from existing securities records only. If the securities record doesn’t exist yet, create it first.
On the Related tab of the client or household profile, select 
New
 in the Financial Holdings section.
Enter a name, such as 
Salesforce shares
.
Select the ticker symbol.
Select the financial account (investment account) for this holding.
Enter purchase details.
Save the record.
See Also:
Create Securities Records
View Holdings Within an Investment Account
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fsc_add_holding.htm&language=en_US
Release
202.14
