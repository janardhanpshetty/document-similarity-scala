### Topic: CSRF vulnerabilities were identified within Flow URLs that could have led a user into being tricked to execute unwanted DML operations. This article outlines the vulnerabilities and the changes made to protect against possible attacks.
Flows & Cross-Site Request Forgery
  
CONFIDENTIAL
This information is for internal consumption only. Do not share with customers.
What is CSRF?
A form of attack where somebody hides a GET request inside of an element on a webpage. For example, they create an image tag that when selected hits some URL. If you own the URL (in Salesforce) and doing so causes a DML operation, it's a security concern. A DML operation is something that updates the database–by creating, updating, or deleting records. It’s not technically DML, but looking up records (SOQL queries) is also a concern.
The only place we accept a GET request is from the flow servlet (/flow requests). Behind the scenes, the servlet redirects to an apex page we own. If an external user somehow know that, you could hit the interview.apexp page directly. 
Flow Vulnerabilities
The security concern is this: we don’t want users to be tricked into doing something that performs DML operations. Flows are vulnerable to this kind of trickery in two instances.
Starting a flow: only if the flow performs DML or SOQL operations before the first screen. If a hacker somehow knows the interview ID and the version ID, a user could be tricked into starting a flow that does something in the database. 
Resuming an interview: When users resume a paused interview, the interview is unpaused and the interview record automatically deleted from the database. If a user accidentally resumes (or is tricked into resuming) an interview, they need to know to click Pause again to re-persist the interview to the database. 
Resolution
Solutions for Vulnerabilities
We have nothing in place to protect users from accidentally starting flows that happen to have DML operations before the first screen. We did, however, add protections against CSRF vulnerability for resuming interviews.
We added a security token to the internal implementations of the resume URL (such as from the Paused Interviews component on the home page). If a resume URL doesn’t include our security token, the user is directed to the interview record detail. This solution forces the user to choose to resume the interview.
What Does this Mean for Customers?
The standard links in the app (such as from the Paused Interviews component on the home page) still unpauses the interview, loads it for the user, and deletes the paused interview record from the database.
If a customer was distributing a resume URL externally, such as through an email, the experience changes slightly. Before, when a user clicked that link, the interview resumed and loaded for the user. (The interview was technically unpaused and the paused interview record was deleted from the database. If users accidentally clicked the link, they’d have to click Pause to re-persist the interview into their list of Paused Interviews.) Now, when a user clicks that link, they’re directed to the record detail page for that persisted interview.
Refer to GUS Work Record: 
W-2950325
