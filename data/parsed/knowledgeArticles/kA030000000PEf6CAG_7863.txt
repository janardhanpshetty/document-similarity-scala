### Topic: The most important thing to note is that all development is focused on PRM 3.0 and PRM 2.0 will be phased out. So if you have a customer interested in PRM, pursuade them to use PRM 3.0.
INTERNAL: What is the difference between PRM 3.0 and PRM 2.0?
Resolution
The most important thing to note is that all development is focused on PRM 3.0 and PRM 2.0 will be phased out. So if you have a customer interested in PRM, pursuade them to use PRM 3.0.
Some key differences-
 
PRM 2.0
 runs through the API.  You have to download it through the AppExchange (it is no longer available for download).
    Customization has to be done through CSS (Cascading Style Sheets)
    Users can login through a generic website ( apps.salesforce.com) with their username and password
 
Partner Portal 3.0 does not support natively these features in Portal 2.0
:
    Lead Inbox, approval request history and channel manager name are available through visualforce components (see: http://blogs.salesforce.com/prm/2008/07/new-and-improve.html)
    Current Stats on homepage
  
PRM 3.0
 runs just like salesforce.  It has the same look and feel as salesforce and can be configured by going to:
       | Setup | Customize | Partners | Settings |
 
S-Controls, Custom Links and Buttons, and VisualForce are supported
A list of Tasks on the Homepage
Web Tabs
Emailed Reports
Ideas
Content
Reports can be exposed to Partners
Login as Portal User (Summer 08)
Delegated Portal User
HTML Messaging (Winter 09)
Partners can send email (Spring 09)
Partners can submit cases (Spring 09)
3rd party service providers can solve cases (Spring 09)
Ability to delete contacts that were once Partner Users (Summer 09)
Note- With PRM 3.0, there is no generic login site.  In order for a Partner User to access their portal, they must login through their Portal's URL.  See Solution 00008127  (https://na1.salesforce.com/50130000000Lluh)
Upgrade considerations:
I do not see the logout link in partner portal 3.0
Partner Portal 3.0 Help Files
