### Topic: Automate Two-Factor Authentication from a Trusted Location with Salesforce Authenticator (Version 2 or Later) | Salesforce
Automate Two-Factor Authentication from a Trusted Location with Salesforce Authenticator (Version 2 or Later) | Salesforce
Automate Two-Factor Authentication from a Trusted Location with Salesforce Authenticator (Version 2 or Later)
Enable location services in the Salesforce Authenticator mobile app, and then use the app to automate two-factor authentication from a trusted location. Examples of trusted locations can include your home or office.
Available in: Both Salesforce Classic and Lightning Experience
Two-factor authentication with Salesforce Authenticator available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Contact Manager
 Editions
Before you can use the app for two-factor authentication, connect the app to your account. The first time you use Salesforce Authenticator to verify your account activity, the app asks for access to your location. Either respond to allow access, or go later to your mobile device’s settings to allow Salesforce Authenticator to access your location. Your mobile device’s location data does not leave the app. Allowing access to your location lets you automate two-factor authentication when you’re working at your office, home, or other trusted location.
Respond to a notification from Salesforce Authenticator by opening the app on your mobile device.
The app displays details of your account activity, including your username, the service you’re trying to access (such as Salesforce), and information about the device used for the activity. If location services are enabled and available, the app shows your current location. Owing to the nature of geolocation technology, the accuracy of geolocation fields (for example, street address, country, city) can vary.
Check the details.
If you recognize the details, and you’re in a location you trust, like your office or home, switch on 
Always verify from here
.
Tap 
Approve
.
In Salesforce, you’re logged in or granted access to the desired resource. The next time you take the same action from the same location, the Salesforce Authenticator app automatically verifies the activity for you. You need your mobile device with you, but you don’t have to respond to an app notification.
See Also:
Verify Account Activity with Salesforce Authenticator (Version 2 or Later) for Two-Factor Authentication
Stop Location-Based Automated Verifications in Salesforce Authenticator (Version 2 or Later)
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=salesforce_authenticator_trust_location_and_automate.htm&language=en_US
Release
202.14
