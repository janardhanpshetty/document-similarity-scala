### Topic: Creating URL Redirects in Site.com | Salesforce
Creating URL Redirects in Site.com | Salesforce
Creating URL Redirects in Site.com
If you move or reorganize pages in your site, search engines may have trouble finding the new page locations. To avoid this, set up URL redirects to inform users and search engines that site content has moved.
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To build, edit, and manage Site.com sites:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
You can use URL redirects to:
Maintain search engine ranking. For example, if you change a page’s name from “Gadgets” to “Widgets,” creating a redirect rule from 
/Gadgets
 to 
/Widgets
 lets you restructure the site without affecting your page ranking.
Make URLs more readable and memorable. For example, site visitors will find long or numeric page names, such as 
/widget65AD890004ab9923
, difficult to remember. Instead, you can provide them with a short, friendly URL, such as 
/widget
, and create an alias that redirects to the correct page when the user uses the short URL alias.
Assist with migration from another system to Site.com if you’re still using the same domain. For example, if your old site ran on PHP, you can create a redirection rule from an old page, such as 
/index.php
, to a new page in Site.com, such as 
/myNewPage
.
To assign a redirect to a site page:
On the Overview tab, click 
Site Configuration
 | 
URL Redirects
.
Click 
Create a Redirect
.
Specify the 
Redirect type
:
Option
Description
Permanent (301)
Select this option if you want users and search engines to update the URL in their systems when visiting the page. Users visiting a page redirected with this type are sent seamlessly to the new page. Using a permanent redirect ensures that your URLs retain their search engine popularity ratings, and that search engines index the new page and remove the obsolete source URL from their indexes.
Temporary (302)
Select this option if you want users and search engines to keep using the original URL for the page. Search engines interpret a 302 redirect as one that could change again at any time, and though they index and serve up the content on the new target page, they also keep the source URL in their indexes.
Alias
Select this option if you don’t want the URL to change in the user’s browser, but you want to redirect to a different page. Search engines won’t be aware of the change or update their records.
Alias redirects only work when you redirect from one Site.com page to another. You can’t create an alias to an external address.
Specify the former page location in the 
Redirect from
 field.
The page location must be a relative URL.
The page can have any valid extension type, such as 
.html
 or 
.php
, and can contain parameters. Parameter order is irrelevant.
The URL can’t contain anchors, such as 
/siteprefix/page.html
#target
.
You can create just one redirection rule 
from
 a particular URL. If you create a new rule with the same Redirect From information, the old rule is overwritten.
Specify the new page location in the 
Redirect to
 field. This can be a relative URL or a fully-qualified URL with an 
http://
 or 
https://
 prefix. Unlike pages you’re redirecting from, pages you’re redirecting to can contain anchors.
To immediately enable the redirection rule, ensure 
Active
 is selected. To enable it at a later stage, deselect the property.
Click 
Save
.
The URL Redirects section displays all URL redirection rules you've created for your site.
Edit an assigned redirect rule by clicking 
| 
Edit Redirect
.
Delete a redirect rule by clicking 
| 
Delete Redirect
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_redirects.htm&language=en_US
Release
202.14
