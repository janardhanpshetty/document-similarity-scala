### Topic: Mapping record owner on import results in an error, "Your import failed: There was an unexpected problem while importing. The import did not succeed." When this occurs it is recommended to include User Ids in your import file when mapping record ownership with the Unified Data Import Wizard.
By default, record ownership is set to the user performing the import operation unless a record owner field is included in your file and explicitly mapped on import.
It is possible to set record ownership outside of the user performing the import by specifying an owner's full name or email address in the import file and then mapping it to the Salesforce record owner field on import. However, if multiple users share the same name and/or email address or the values in your file are not an exact match for values contained on your user's records, you will encounter a generic error, stating "Your Import Failed: There was an unexpected problem while importing. The import did not succeed." after clicking the Start Import button.
 
 
When mapping the Salesforce record owner field to name or email values in our import file it will result in the error above and the job may not be processed at all upon checking the import status via Setup, click Jobs | Bulk Data Load Jobs.
  
Note
The Bulk Data Load Jobs page is not available in Professional Edition. Additionally, only administrators have access to the Bulk Data Load Jobs page in Salesforce Setup. If you’re not an administrator, you can check the status of your upload by monitoring the relevant tabs in Salesforce.
Resolution
When mapping lead owner, account owner, contact owner, or a custom object's record owner while using the 
Data Import Wizard
 it is recommended to use the owner's user Id in your import file instead of their full name or email.
Preparing your import with user Ids ensures that record ownership is properly set on import and prevents potential occurrences of the import failed error message. One incorrect value for a record owner in your import file may cause the entire operation to fail. The following are example scenarios that may result in the error and prevent the import from being processed:
- Full name or email values contains a leading space in your file
- Incorrect values in your import file that do not match the desired owner's user record's current name or email address
- Two user records exist with the same email address or full name
To find your current User's Ids to populate their values into your import file:
1. Generate a 
users report
 and then selecting to 
Export Report Data
. You can also get specific user Ids as outline in the article, 
How do I locate a Salesforce User ID?
 You may also reference the video: 
Data Import: Owner IDs and Parent IDs
 for more details.
2. Export User details including their Name, Id and any additional fields that you may need for reference via the API using Data Loader for example. See 
Exporting Data
 for more details.
