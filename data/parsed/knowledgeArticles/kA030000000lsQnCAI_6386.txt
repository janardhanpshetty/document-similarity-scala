### Topic: A non admin user get "Insufficient Privileges" when accessing some custom setting in an org. Admin can redeploy the custom setting to resolve the issue.
A non admin user get 
"Insufficient Privileges" when accessing some custom setting in an Org.
 
Suggestion: A non admin user gets OR non admin users get
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
February 22, 2016 at 2:02 PM
  
Attach File
 
Click to comment
 
 
Jerome Espejo
 to salesforce.com Only
#KBFeedback
 Please update the subject.
 
Non admin 
user get
 "Insufficient Privileges" error when accessing some Custom Setting
 
Suggestion: user gets OR users get
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
February 22, 2016 at 2:01 PM
  
Attach File
 
Click to comment
 
 
Kazuyuki Igarashi
 to salesforce.com Only
#KBFeedback
 ,
Hi 
@Bernard Spencer
,
The click path in this article is incorrect.
It says:
Go to Setup | Develop - Click on "custom setting name"
However it should be:
Go to Setup | Develop - Click on "Custom Settings"
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
December 3, 2014 at 5:12 PM
  
Mitchel Ortiz
Done! Also reworked the font 
Show More
Like
Unlike
 
  ·  
 
December 4, 2014 at 1:15 AM 
Kazuyuki Igarashi
Thank you, Mitchel.
Show More
Like
Unlike
 
  ·  
 
December 4, 2014 at 4:05 PM 
Attach File
 
Click to comment
 
 
Kanako Yokozeki
 to salesforce.com Only
Translation Request in Japanese
@Kazuyuki Igarashi
san 
@Ryoichi Saito
san
#JPTranslate
Show More
Topics:
   
JPTranslate
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
November 27, 2014 at 8:04 PM
  
Show All 
5
 Comments
Kanako Yokozeki
@Kazuyuki Igarashi
san 以下のエラーになります。
-----
アクセス権がありません
アクセス権がないため要求を実行できません。データの所有者また
は、システム管理者にお問い合わせください。
-----
よろしくお願いします。
Show More
Like
Unlike
 
  ·  
 
December 3, 2014 at 4:47 PM 
Kazuyuki Igarashi
@Kanako Yokozeki
 san,
ありがとうございます。
Show More
Like
Unlike
 
  ·  
 
December 3, 2014 at 4:51 PM 
Kazuyuki Igarashi
Published
Show More
Like
Unlike
 
  ·  
 
December 3, 2014 at 7:06 PM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Usage
A non admin users get "Insufficient Privileges" when accessing some custom setting in an Org.
Go to Setup | Develop - Click on "Custom Settings", and ideally it should open the Custom Setting object, but it throws an error:
Insufficient Privileges
You do not have the level of access necessary to perform the operation you requested. Please contact the owner of the record or your administrator if access is necessary.
But there is no issue accessing other custom setting object. 
Resolution
This happens most likely because the custom setting is not in "deployed" status, but it is in "
In development" status.
Though the status is not visible to admin in UI or in the metadata, the issue can be resolved by redeploying same custom setting, it will change its status to "deployed".
 
