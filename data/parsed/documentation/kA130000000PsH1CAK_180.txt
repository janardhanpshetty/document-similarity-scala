### Topic: Editing an Environment Hub Member’s Details | Salesforce
Editing an Environment Hub Member’s Details | Salesforce
Editing an Environment Hub Member’s Details
Available in: 
Salesforce Classic
Available in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
To edit details of a member organization in the Environment Hub:
Click 
Edit
 next to the organization’s name in the Environment Hub main page, or on its detail page.
In the page that displays, edit the name and description for the organization. It’s helpful to specify a meaningful name and description. This lets you easily recognize the organization in the list of members in the Environment Hub.
Optionally, specify one or more single sign-on methods (see below for details).
Click 
Save
.
To match users in the member and hub organizations for single sign-on, you can use any of three methods.
SSO Method
Description
Mapped Users
Match users in the hub organization to users in a member organization manually. This method is on by default if you’ve defined any SSO user mappings from the member detail page. For details, see 
Defining a SSO User Mapping
.
Federation ID
Match users who have the same Federation ID in both organizations. To enable this method, select the checkbox next to it.
User Name Formula
Define a custom formula for matching users in the hub and member organizations. This allows you the most flexibility. To enable this method, type a custom formula into the text box provided. For example, the following formula matches the first part of the user name (the part before the “@” sign) with an explicit domain name.
LEFT($User.Username, FIND("@", $User.Username)) & ("mydev.org")
If you specify more than one single sign-on method, they're evaluated in the order of precedence listed above at the time a user tries to log in. The first method that results in a match is used to log the user in, and the other methods are ignored. If no matching user can be identified, you’re directed to the standard salesforce.com login page.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=environment_hub_edit_organization_details.htm&language=en_US
Release
198.17
