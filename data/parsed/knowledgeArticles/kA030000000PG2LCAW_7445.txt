### Topic: Here are some common questions from customers regarding API connectivity and design.
Questions:
=================
1. What would be the actual SOAP endpoints? (e.g How many different endpoints, domain name and URLs)
2. Does the Salesforce.com domain name resolve to a single IP address or the whole range of IP addresses you have provided? How often does this change?
3. Why does Salesforce recommend whitelisting the entire IP range? Can we whitelist just one IP address?
Resolution
Here are the answers:
1. What would be the actual SOAP endpoints? (e.g How many different endpoints, domain name and URLs) 
========== 
>There are two types of Salesforce WSDL call endpoints, namely:
'/c' for enterprise endpoint, e.g.: 
https://test.salesforce.com/services/Soap/c/12.0 
https://www.salesforce.com/services/Soap/c/12.0 
'/u' for partner endpoint, e.g.: 
https://test.salesforce.com/services/Soap/u/12.0 
https://www.salesforce.com/services/Soap/u/12.0 
The WSDL document that you specify might contain a SOAP endpoint location that references an outbound port. 
For security reasons, Salesforce restricts the outbound ports you may specify to one of the following: 
80: This port only accepts HTTP connections. 
443: This port only accepts HTTPS connections. 
1024–66535 (inclusive): These ports accept HTTP or HTTPS connections. 
>We can split the org instances, and can make some changes 
Please follow the link mentioned below which has all the details for the best practices when Referencing Server Endpoints:
>
http://wiki.developerforce.com/page/Best_Practices_When_Referencing_Server_Endpoints
2.
Does the Salesforce.com domain name resolve to a single IP address or the whole range of IP addresses you have provided? How often does this range change?
===============
The domain name resolves to the entire IP range. There is no set time that this range will change, but it does change. This mostly occurs when a new data center is added or an older data center is removed. Salesforce will communicate these changes in release notes or emails to org administrators.
3. Why does Salesforce recommend whitelisting the entire IP range? Can we whitelist just one IP address?
============
Due to the multi-tenant nature of Salesforce, we cannot guarantee that all API connections will occur from a single IP. In a disaster recovery or failover situation, the IP address will change. Whitelisting a single IP address can expose your company to a longer downtime.
 
