### Topic: When a job is created, Salesforce sets the job state to Open. You can abort those jobs since those are not processing.
Scenario-
When view our bulk data load jobs in production there are a large amount of jobs that have open status although they were submitted few days before. Do these need to be aborted or is there something going on that is preventing these from processing?
Resolution
When a job is created, Salesforce sets the job state to Open.
You can abort those jobs since those are not processing.
Batches and jobs that are older than seven days are removed from the queue regardless of job status. The seven days are measured from the youngest batch associated with a job, or the age of the job if there are no batches.
You can't create new batches associated with a job that is more than 24 hours old.
The client has to send the close connection to change the status of the bulk data load from "Open" to "Close".
Documentation links:
http://www.salesforce.com/us/developer/docs/api_asynch/index_Left.htm#StartTopic=Content/asynch_api_jobs_lifespan.htm
 
