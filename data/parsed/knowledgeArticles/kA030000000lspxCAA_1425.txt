### Topic: With the Spring ’15 release*, we will retire Legacy Activa Live Agent for Salesforce. We encourage you to migrate to Live Agent in the Salesforce Console. It is important for you to know that after the Spring ‘15 release, you will no longer be able to access Legacy Activa Live Agent for Salesforce.
Summary:
 With the Spring ’15 release*, we will retire Legacy Activa Live Agent for Salesforce. We encourage customers to migrate to Live Agent in the Salesforce Console. It is important for customers to know that after the Spring ‘15 release, they will no longer be able to access Legacy Activa Live Agent for Salesforce.
Internal FAQ
 
1. What is the difference between the original and recommended replacement?
a. Live Agent is a core component and native feature of the Service Cloud, in the Salesforce Console, that leverages the power and security of the 
salesforce
 platform. This is an upgrade from Legacy Activa Live Agent, which was hosted outside the platform.
 
2.What will happen if the customer doesn’t take the recommended action? I.e. What will be the customer experience? An error message?
a. If the customer does not take action and migrate to Live Agent in the Salesforce Console, then they will not be able to login or to chat.
 
b. If the customer does not export their data prior to Spring ‘15, we may be able to help customers on a case by case basis, but do not offer unless it is absolutely necessary.
 
3. What happens if the customer’s contract surpasses the retirement date? Can they renew?
a. After the Spring ‘15 release, customers will no longer be able to access Legacy Activa Live Agent for Salesforce and their annual subscription will not be renewed. Customers are encouraged to migrate to Live Agent in the Salesforce Console.
 
b. To give customers more time to migrate, their account will have access to Live Agent for the Salesforce Console from now until February 9, 2015, regardless if their contract expires prior to that date. After February 9, 2015, their access will depend on converting their contract and license(s).
 
c. If their contract date surpasses February 9, 2015, customers have two options:
i.  If they would like to migrate to Live Agent in the Salesforce Console, they must reach out to their Account Executive or Renewal Manager who will help convert the customer’s contract and license(s).
 
ii. If they do not wish to migrate to Live Agent in the Salesforce Console, effective February 9, 2015, we will cancel the customer’s existing Live Agent subscription(s) and issue a prorated credit or refund for any service billed for period after February 9, 2015, prorated to the date the customer's contract expires.
 
4. How does the Account Executive or Renewal Manager swap the customer’s contract and license(s)?
a. The following instructions are specifically for the AEs and RMs:
 
i. The customer has already been given their new product (Live Agent in the Salesforce Console). You must issue a Swap Order for the customer to sign, in order to contractually move each customer off of the Legacy Activa Live Agent product and onto the new Live Agent in the Salesforce Console product.
 
ii.  For discounts, obtain a blanket approval to swap the license(s) at no incremental fee from the legacy product to the new product.
 
iii. If the customer’s contract is up for renewal prior to February 9, 2015, notify your Sales Operations Support team so as to not add product to the quote.
 
b. AEs/RMs: If you need assistance, please work with your Sales Operations Support team.   
 
5. My prospect/existing customer hasn’t yet used the feature, but had the intent to set it up.  What should I tell them?
a.  Legacy Activa Live Agent is not available for new orgs. It has been replaced with Live Agent in the Salesforce Console.
 
6. What is the escalation process for dissatisfied customers?
a. If there are account level issues that need to be escalated, you can use the existing red account process.
 
7. Is any of this information shareable with customers?
a. Please use the links and information presented in the external FAQs 
Knowledge Article
. 
 
Resolution
