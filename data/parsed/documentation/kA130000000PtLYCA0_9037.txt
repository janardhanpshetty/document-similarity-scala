### Topic: What’s the difference between private items in both Salesforce and Microsoft® Outlook®? | Salesforce
What’s the difference between private items in both Salesforce and Microsoft® Outlook®? | Salesforce
What’s the difference between private items in both Salesforce and Microsoft® Outlook®?
Private items in Microsoft Outlook and Salesforce differ in unexpected ways. Before choosing to sync private events, consider how they’re displayed to colleagues in each system.
Item
Private in Outlook
Marked private in Salesforce
Contacts
Your Exchange administrator can see your contacts. Other users cannot.
Other Salesforce users can see contacts that are associated with accounts.
Contacts that aren’t associated with accounts are private and aren’t visible to other Salesforce users except for Salesforce administrators who can view, edit, and report on contacts.
Events
Your Exchange administrator can see your events. Other users cannot.
Only blocks of time, and not event details are visible to other Salesforce users, except for Salesforce administrators who can view, edit, and report on contacts.
Tasks
Your Exchange administrator can see your tasks. Other users cannot.
Salesforce doesn’t include any means for keeping tasks private. So other Salesforce users can see your tasks.
See Also:
Desktop-Based Salesforce for Outlook
Syncing Between Microsoft® Outlook® and Salesforce Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_sfo_private_items.htm&language=en_US
Release
202.14
