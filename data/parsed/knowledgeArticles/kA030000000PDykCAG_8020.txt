### Topic: This article would help you understand what kind of form field validation, cross-site scripting and SQL injection protection are available on the web-to-lead form processed by Salesforce.
I would like to know if there is any kind of form field validation, cross-site scripting and SQL injection protection on the web-to-lead form processed by Salesforce.
Resolution
INTERNAL ONLY
In general, the inputs are strongly typed and subject to the field definitions and validation rules of the customer's org.  All data goes through context specific output escaping when it is displayed on the Lead object.  SQL Injection is not an issue, as we use prepared statements and bind variables.
The customer is welcome to perform their own penetration testing of their W2L form through our Security Assessment Agreement (SAA) process.  Its available in the SE workspace in Content if they're interested.
