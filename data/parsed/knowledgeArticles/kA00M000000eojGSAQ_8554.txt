### Topic: Validation Status
Some of our users have intermittent problems using some Visualforce pages that they have historically had no issue with before. When completing forms on some Visualforce pages, and only sometimes, an Insufficient Privileges error message will be displayed. If they refresh the page in their browser the page loads as expected.
Resolution
This may occur when a user has multiple Salesforce tabs open in which they have logged into Salesforce each time they open a new tab. For example If a user opens a Visualforce page containing a form tag in one browser tab and then before completing the form opens another browser tab and logs into Salesforce again and then a some point comes back to the original form and saves this form the Insufficient Privileges error occurs. Please note users can have as many Salesforce tabs open as they like however they should avoid logging into Salesforce each time they open new Salesforce tabs as this will cause this error.
This change to now throw the insufficient privileges error was introduced in Spring 16 as part of a security enhancement. 
 
