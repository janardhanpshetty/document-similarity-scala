### Topic: Learn how to populate the desired fields within a Custom Report Type.
Fields you expect to have available when creating a report based on a Custom Report Type don't appear. 
 
For Example:
 Activities with Accounts and Contacts report type is missing fields from the "Select Columns" step in Report Wizard or the list of fields in the left-hand column of the Report Builder.
Resolution
 
Edit the Report Type Page Layout
The following steps will help you add missing fields to your Report:
1.  In the Original Setup User Interface g
o to 
Setup | App Setup | Create | Report Types
.  
     In the Improved Setup User Interface go to 
Setup | Build | Create | Report Types
.
2.  Click the name of the Report Type you're using. 
3.  From the 
"Fields Available for Reports section, c
lick 
Edit Layout. 
4.  Drag and drop the missing fields from the "Not in Page Layout" section, then click 
Sa
ve
.
When you go back to the report, you will find that the fields are available to add to the report.
Good to know: 
When you create a new custom field after the report type exists, you'll need to follow these steps to get the field to appear on the report type.
