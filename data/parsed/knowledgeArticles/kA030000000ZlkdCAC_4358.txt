### Topic: The accept button is present only when the list consists of records that are still in queue and not assigned to any specific owner. This is expected behavior
When looking at a Lead list view, the accept button is missing
Resolution
The accept button is present only when the list consists of records that are still in queue and not assigned to any specific owner. This is expected behavior. Even cases list view work the same way wherein "Accept" button is available only when the list view consist of records that are still in queue.
Steps to Reproduce:
Create a Test Lead Record
Assign it to a Lead queue
Create a list view with the criteria to list only the newly created lead
You will see the accept button
This is because the new lead is still in queue and to take ownership the user can use the accept button.
Note: If the list view has mix of leads assigned and leads in queue the Accept button will still not be available
 
