### Topic: "You can't have more than 5 matching rules in activation or deactivation process" error when deploying a change set.
Customers receive an error "You can't have more than 5 matching rules in activation or deactivation process" when deploying a change set, which includes more than 5 matching rules.
This problem occurs when ACTIVE rules are added to a change set. 
If a change set include less than 6 active matching rules, it won't cause any problem with deployment, otherwise a user will get an error "You can't have more than 5 matching rules in activation or deactivation process". 
Resolution
To resolve this problem user attempting the deployment needs to create a change set with less then 6 matching rules or the rules need to be deactivated beforehand.
It is possible to deploy large number of matching rules, however they need to be deactivated before adding them to the change set.
 
