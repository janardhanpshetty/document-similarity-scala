### Topic: Percentages do not add up to 100% on Dashboards or Reports
 
Sometimes you may notice that when referencing "Counts" or "SUMs" in a Custom Summary formula on Reports, your calculations are not correct or maybe your percentages are not adding up to 100%.  This occurs when you have a Report Type with more than one Object and you have a grouping you are trying to total or "count", rather than trying to reference the Record Count.  The Report Functionality does not currently provide us with that count.  For example:
 
I am running a Report on Accounts and Contacts (more than one object). I am grouping my Contacts by Account Name, and I want to get a total number of Accounts for whatever reason (simple total, for use in a calculation or Custom Summary Formula).  I will not be able to reference the Record Count for that total.  
 
 
 
Resolution
Referenced from the IdeasExchange: 
 
"The Power of One"
 
https://success.salesforce.com/ideaView?id=087300000006tde
The way to do this easily is to create a Formula Field (Number with zero decimal places) on the Account Object that is equal to "1" in the text box portion of the formula.  In doing this, you will have a summarizable Field that you can use for the count on the Report.
