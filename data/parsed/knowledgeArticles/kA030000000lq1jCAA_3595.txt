### Topic: Error "Impossible to load apex plug-ins. Reference ID:xxxxxxxxxxxxx"
The following Flow error message can sometimes appear when opening a flow: Impossible to load apex plug-ins. Reference ID:xxxxxxxxxxxxx
It is caused when a managed package is in a suspended status.
Resolution
Either uninstall the suspended managed package or re-activate it.
