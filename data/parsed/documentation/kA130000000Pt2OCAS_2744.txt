### Topic: Create an Outbound Message Notification | Salesforce
Create an Outbound Message Notification | Salesforce
Create an Outbound Message Notification
Request that up to five users receive a notification listing all outbound messages that have failed for at least 24 hours. A fresh notification is sent every 24 hours until you cancel the request.
Available in: Lightning Experience and Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create an outbound message notification:
“Modify All Data”
From Setup, enter 
Outbound Message Notifications
 in the 
Quick Find
 box, then select 
Outbound Message Notifications
.
Click 
New
.
Enter a full username, or click the icon to select it from a list of usernames.
Save the request.
Note
If you don't have this option, your organization doesn't have outbound messages enabled. Contact Salesforce to enable outbound messages.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=creating_workflow_om_notification_requests.htm&language=en_US
Release
202.14
