### Topic: Scope of Salesforce support for WS security in Web service callouts.
Does salesforce supports WS security in Web service callouts?
Resolution
Currently there is no support for WS security in the WSDL.
As a workaround, consider manually modifying the proxy class generated via the WSDL2APEX to include the security headers in the SOAP request.
