### Topic: Develop Your Lightning Experience Rollout Strategy | Salesforce
Develop Your Lightning Experience Rollout Strategy | Salesforce
Develop Your Lightning Experience Rollout Strategy
One of the best investments you can make in your Lightning Experience rollout is to have a clear plan. A plan helps you do things in the right order, identify key resources, communicate with everyone, and have a clear end date in mind.
Depending on the size of your organization, you could be working with a Change Management department or have a project manager assigned to the rollout. Or perhaps you’re the one charged with organizing and executing the rollout from start to finish.
 However the work gets divvied up, use the 
Lightning Experience Rollout
 module in 
Admin Trail - Migrating to Lightning Experience
 from Trailhead to help with this process.
Your rollout will likely fall into these phases.
Learn
Learn about Lightning Experience
Identify stakeholders and an executive sponsor
Educate your company with presentations and demos
Review 
Compare Lightning Experience and Salesforce Classic
 and 
Lightning Experience Considerations
Conduct a gap analysis to confirm you’re getting what you need
Launch
Identify users for a pilot of Lightning Experience
Identify and activate super users
Create the project schedule
Define measures for success
Create and execute your marketing and training strategies
Customize and test, ideally in a sandbox environment
Go live!
Iterate
Evaluate how things are working to see if you’re hitting your success metrics
Survey your users for satisfaction and pain points
Use reports and dashboards to track metrics and feedback
Deliver an executive summary
To jump-start your planning, download the 
Lightning Experience Customer Enablement Pack
 that’s part of the 
Lightning Experience Rollout
 module. The Enablement Pack is chock-full of valuable resources, like a presentation deck, a risk severity matrix, a project schedule template, a sample email drip campaign, and a rollout checklist to keep you focused and on track.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=lex_rollout_strategy.htm&language=en_US
Release
202.14
