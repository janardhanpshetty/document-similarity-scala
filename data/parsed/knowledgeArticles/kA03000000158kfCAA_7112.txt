### Topic: This article explains why customers cannot import GTM times for the Date/Time fields on Knowledge Articles when using the article import tool.
Customers might be interested in importing GMT values for the Date/Time field. However when they try to import articles with those values, they will encounter error  
"Invalid value(s) for import setting: DateTimeFormat."
Resolution
This is working as design. The article importer 
only support Java Date formats.
 It does not support Java Date/Time format.
We take the value as given based on the format specified in the properties file and the value in the csv. We parse the value based on the properties and csv to it's corresponding Date/Time accordingly but then we compare the parsed Date/Time back to a string and compare it to the original. If it is not the same then we throw an error, which in this case, the original is not the same as the parsed. That is why you see the error 
"Invalid value(s) for import setting: DateTimeFormat."
