### Topic: Customers may have issues with signing up for sessions and the sessions will appear and then at some point the sessions will disappear. These cases need to be handled by Registration Support. You may need to reset your Dreamforce Registration Password.
Customers may experience issues when signing up for Dreamforce. Here is but one example:
I am signing up for sessions and reserving them for Dreamforce. But the sessions are starting to disappear, even after I reserved them. For example the Gala on Tuesday was reserved, and I did see it in my agenda, but after some time it disappeared?
Resolution
Please follow the ‘reset password’ instructions on the page or you may also access the link below to reset your password, 
 
https://success.salesforce.com/apex/ev_forgotpwd
If you are still having issues, please contact our 
Registration Headquarters
 for further assistance.
They are open 
Monday through Friday, 6 am – 6 pm (PST)
 at:
 
U.S./Canada: +1-866-855-3818
International: +1-650-226-0793
Registration: 
registration@dreamforceSF.com
