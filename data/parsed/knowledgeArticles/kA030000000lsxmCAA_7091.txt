### Topic: API Enabled organizations can utilize the Data Loader or similar client tools to import Person Accounts using the attached CSV template as a starting point. They can also use Data Import Wizard
With Spring '16 it is now possible to 
Import Person Accounts With the Data Import Wizard
 via Setup, Data Management | Data Import Wizard however, you can also import person accounts via the Data Loader as an alternative.
 
Resolution
If your organization is API Enabled (Enterprise Edition and above by default) Administrators can also leverage the Data Loader to import Person Accounts.
Whether an Account is a Person or Business Account is set on import by specifying the Account's record type.  Add a RecordTypeId column in your import file and populate it with the value of the desired Person Account record type's Id which can be found via the record type setup section's URL.  Navigate to Setup, Customize | Accounts | Person Accounts | Record Types and click the desired Record Type Label.  The corresponding URL should look something like the example below where 012o00000008ets is the record type Id:
https://
<YourInstanceOrMyDomainHere>
.salesforce.com/setup/ui/recordtypefields.jsp?id=012o00000008ets&type=PersonAccount&setupid=PersonAccountRecords
For a list of additional fields you can include when specifying an Account as a Person Account via a Person Account record type, reference the IsPersonAccount Fields section of the 
SOAP API Developer's Guide
.  Attached you will find the file, PersonAccountImportTemplate.csv containing column headers labeled to match and automatically map to standard person account fields in Salesforce.  Row 2 contains example data for illustration purposes and row 3 contains notes which can be expanded to show relevant details.
See Also:
Implementing and Importing Person Accounts
What is the IdeaExchange?
Inserting, Updating, or Deleting Data Using Data Loader
