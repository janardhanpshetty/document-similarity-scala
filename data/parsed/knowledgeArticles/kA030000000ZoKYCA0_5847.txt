### Topic: Contact and lead records that have the current user's email address will not come up in the Side Panel when an email message is selected, which is expected behavior, but the same records will be displayed in the Side Panel when composing a new email message.
Contact and lead records that have the current user's email address will not come up in the Side Panel when an email message is selected, which is expected behavior, but the same records will be displayed in the Side Panel when composing a new email message.
It seems strange that the Side Panel's inconsistent behavior depends on whether the auto-search happens on an existing email or a new one.  
Steps to replicate
Create lead and/or contact records with your and a co-worker's email address (eg, myaddress@acme.com and mycoworker@acme.com)  
From an external email account (Yahoo, Google, etc.), send an email to yourself and cc your co-worker 
Select the email message when it arrives in your Outlook inbox. Notice records with your email address are excluded from the auto-search results in the Side Panel
In Outlook, compose a new email. 
In the TO line, add your email address and your co-worker's 
Notice the auto-search now displays records that have either or both email addresses 
Resolution
This is a known discrepancy 
in the auto-search behavior that does not impact the search functionality or any other aspect of the Side Panel.
The auto-search is working as designed.
