### Topic: This article explains why a user is present with the error "Canvas App URL: Invalid URL" in the Connected App menu.
When working with Canvas apps, you would come across the error:  "Canvas App URL: Invalid URL" in the Connected App menu.
 
Resolution
This error message occurs because of you're trying to save information in Canvas URL that can not be located. The URLs that the Canvas parser accepts are in the format of a properly structured HTTPS URL. Below are some examples that the parser in the Canvas App will allow to become saved and the ones that will generate an error when you attempt to save the URL. 
Good Examples:
 https://localhost:8443/
 https://localhost:8443/filename.fileextension?sometypeof_action=name&ui.action=name
 https://example.com/folderpath/
 
Bad examples:
https://localhost:8443/folderpathlocation/folder[@name='foldername']
https://example.com/name="test"
ftp://username:password@host.com/
