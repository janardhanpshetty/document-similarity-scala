### Topic: Investor Relations contact methods
Some callers may inquire about 
Investor Relations
, this is a department from Salesforce.com that handles CRM investments with non-customers.
If a caller requests information on Investor Relations please refer him/her to the following contact information:
How can I contact salesforce.com Investor Relations?
Telephone: 
415.536.6250
Fax: 
415.901.7040
Email: 
investor@salesforce.com
Web Site: 
http://www.salesforce.com/investor
Resolution
