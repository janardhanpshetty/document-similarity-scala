### Topic: How to execute batch apex from developer console?
How to execute a batch class from developer console?
Resolution
From Developer Console:
1. Click Debug -> Open Execute Anonymous Window
2. Execute the following  code 
Id <variable name>= Database.executeBatch(new <Class name>(), batch size);
For example:  
Id batchJobId = Database.executeBatch(new RunThisBatch(), 200);
*batchJobId will contain the job Id of the batch.
