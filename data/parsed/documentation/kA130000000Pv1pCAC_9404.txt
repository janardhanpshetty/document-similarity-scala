### Topic: View Connected App Details | Salesforce
View Connected App Details | Salesforce
View Connected App Details
Available in: both Salesforce Classic and Lightning Experience
Connected Apps can be created in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Connected Apps can be installed in: 
All
 Editions
User Permissions Needed
To read:
“Customize Application”
To create, update, or delete:
“Customize Application” AND either
“Modify All Data” OR “Manage Connected Apps”
To update all fields except Profiles, Permission Sets, and Service Provider SAML Attributes:
“Customize Application”
To update Profiles, Permission Sets, and Service Provider SAML Attributes:
“Customize Application” AND “Modify All Data”
To uninstall:
“Download AppExchange Packages”
The Connected App Detail page shows you information about the connected app, including its version and scopes. You can edit and check usage of the connected app, and associate profiles and permissions with the app.
Click 
Edit
 to change the app configuration on the Connected App Edit page.
Click 
Download Metadata
 to get the service provider SAML login URLs and endpoints that are specific to your community or custom domain configuration. This button only appears if your organization is enabled as an Identity Provider, and only with connected apps that use SAML.
Instead of downloading metadata, you can access the metadata via a URL in Metadata Discovery Endpoint. Your service provider can use this URL to configure single sign-on to connect to Salesforce.
Click 
View OAuth Usage
 to see the usage report for connected apps in your organization.
You can enable user provisioning for a connected app on this page. Once enabled, use the User Provisioning Wizard to configure or update the user provisioning settings. After you run the User Provisioning Wizard, the User Accounts section lets you manage the linkage between user accounts and their account settings on the third-party system, individually.
Click 
Manage Profiles
 to select the profiles for the app from the Application Profile Assignment page. Select the profiles to have access to the app (except in Group Edition).
Important
This option won’t appear if the OAuth policy for 
Permitted Users
 is set to 
All users may self-authorize
 because this option isn’t needed when users can authorize themselves.
Click 
Manage Permission Sets
 to select the permission sets for the profiles for this app from the Application Permission Set Assignment page. Select the permission sets to have access to the app.
Important
This option won’t appear if the OAuth policy for 
Permitted Users
 is set to 
All users may self-authorize
 because this option isn’t needed when users can authorize themselves.
Click 
New
 in Service Provider SAML Attributes to create new attribute key/value pairs. You can also edit or delete existing attributes.
Only the users with at least one of the selected profiles or permission sets can run the app if you selected 
Admin-approved users
 for the 
Permitted Users
 value on the Connected App Edit page. If you selected 
All Users
 instead, profiles and permission sets are ignored.
See Also:
User Provisioning for Connected Apps
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=connected_app_view_details.htm&language=en_US
Release
202.14
