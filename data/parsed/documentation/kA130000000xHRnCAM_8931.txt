### Topic: Lookup Search in Lightning Experience | Salesforce
Lookup Search in Lightning Experience | Salesforce
Lookup Search in Lightning Experience
Lookup fields allow you to associate two records together in a relationship. For example, a contact record includes an account lookup field that associates the contact with its account. In the lookup search for searchable objects, terms are matched against all searchable fields within a record, not just the name of the record. If you don’t see a record in the auto-suggested instant results, perform a full search. If you don’t see a new record in search results, wait 15 minutes and try again. Check out some more answers to frequently asked questions about lookups.
Available in: Lightning Experience
Available in: 
All
 Editions 
except Database.com
Why Can’t I See Any Results in the Picklist?
If an object hasn't been made searchable in your org or if you haven't previously viewed the record, the drop-down is empty.
Can I Use a Partially Matching Search?
To find the correct record, enter the full search term. The lookup search does not use automatic partially matching searches with trailing wildcards. This behavior is to avoid returning unwanted results to you.
What Determines the Secondary Field Under the Record Name?
When available, there's a secondary field displayed under the primary record name in lookups that provide more contextual information. For example, the location 
San Francisco
 appears under the company 
Acme, Inc.
 to help you distinguish similar record names and pick the right record. Your admin customizes secondary fields when setting which record fields display and in which order.
Which Fields are Searched in Lookup Dialog Search?
When you start typing in the lookup field, you see a list of auto-suggested results. These instant results are matches for the search term to the name type field of the record. However, if you perform a full search, the returned results match the search term to all searchable fields for the object. This behavior is similar to global search results.
See Also:
How Do I Refine Search Results in Lightning Experience?
What Are the Results I See As I Type in the Search Bar?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_lookup_lex.htm&language=en_US
Release
202.14
