### Topic: Developers using versioning systems keep in mind that LastModifiedDate was not intended to be used with the purpose of comparing pre- and post-deploy results, as its value changes even though the class being deployed is not updated.
When an Apex class/trigger is deployed, the LastModifiedDate on the Apex class/trigger in the organization gets updated even if the Apex class/trigger being deployed is identical to the existing one in the organization.
Resolution
This is working as designed. LastModifiedDate was not intended to be used as a guarantee of similarity when comparing pre-deploy and post-deploy results, which should be taken into consideration by developers using versioning systems.
