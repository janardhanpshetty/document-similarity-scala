### Topic: Does Live Agent have the ability to support screen readers for the visually impaired?
Third party screen reader applications, such as JAWS, are unable to read chat messages received by Live Agent Users. 
Resolution
Screen readers are currently not supported by Live Agent. At this time, this is an expected limitation. 
