### Topic: Set Up an Approval Process | Salesforce
Set Up an Approval Process | Salesforce
Set Up an Approval Process
If Approvals is the right automation tool for your business process, follow these high-level steps to create one for your org.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create approval processes:
“Customize Application”
Prepare to Create an Approval Process
Plan each approval process carefully to ensure a successful implementation.
Choose the Right Wizard to Create an Approval Process
Before you create an approval process, determine which wizard is best for your needs.
Add an Approval Step to an Approval Process
Approval steps define the chain of approval for a particular approval process. Each step determines which records can advance to that step, who to assign approval requests to, and whether to let each approver’s delegate respond to the requests. The first step specifies what to do if a record doesn’t advance to that step. Later steps specify what happens if an approver rejects the request.
Add Automated Actions to an Approval Process
You can associate actions to approval steps, initial submission, final approval, final rejection, or recall. Approval processes support four automated actions.
Review an Approval Process
Generate a graphical representation of your approval process before you activate it. Use the Process Visualizer to gain buy-in from step owners and reinforce your company’s policies by documenting the decisions you reached when the approval process was designed.
Activate an Approval Process
After you’ve created at least one step for the approval process, activate the process.
See Also:
Approval Process Terminology
Sample Approval Processes
Prepare Your Org for Approvals
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_getting_started.htm&language=en_US
Release
202.14
