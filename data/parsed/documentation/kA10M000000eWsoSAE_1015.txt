### Topic: Considerations for Importing Contacts from a Mobile Device | Salesforce
Considerations for Importing Contacts from a Mobile Device | Salesforce
Considerations for Importing Contacts from a Mobile Device 
When importing contacts into Salesforce from a mobile device’s contact lists, there are a few things to be aware of.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
When importing contacts from an Android or iOS device using Salesforce1 into a Salesforce org that has State and Country picklist fields enabled, the 
State
 field isn’t populated.
If you have multiple mobile phone fields in your mobile device’s contact, and the 
Phone
 field in Salesforce is empty:
The first mobile phone field maps to the 
Phone
 field in Salesforce.
The second mobile phone field maps to the 
Mobile
 field in Salesforce.
When you select emails from the mobile device, Salesforce1 typically pulls the second email address location. If labels are created on the device, the integration tries to map from the local work email address to the standard contact email address in Salesforce.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=contacts_import_contacts_from_mobile_considerations.htm&language=en_US
Release
202.14
