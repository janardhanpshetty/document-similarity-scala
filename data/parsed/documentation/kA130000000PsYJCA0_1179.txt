### Topic: Rename a Block | Salesforce
Rename a Block | Salesforce
Rename a Block
You can rename blocks to provide more user-friendly descriptions of the information they contain.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, and delete reports:
“Create and Customize Reports”
AND
“Report Builder”
When you add a block to a joined report, it’s named automatically based on the report type and the number of blocks in the report. For example, if your report contains two blocks and you add a new block that’s based on the Opportunities report type, it’s named Opportunities block 3.
Click the block’s name.
The name now appears in an editable text box.
Enter the new name.
Press Enter or click outside the block to apply the name.
Parent topic:
 
Work with Blocks
Previous topic:
 
Reorder Blocks
Next topic:
 
Show and Hide the Record Count for a Block
See Also:
Work with Blocks
Combine Different Types of Information in a Joined Report
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_renaming_blocks.htm&language=en_US
Release
202.14
