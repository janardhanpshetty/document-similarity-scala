### Topic: Edit Dependent Picklists | Salesforce
Edit Dependent Picklists | Salesforce
Edit Dependent Picklists
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To edit field dependencies:
“Customize Application”
From the management settings for the picklist’s object, go to Fields.
Click 
Field Dependencies
.
Click 
Edit
 next to the field dependency relationship you want to change.
Use the 
field dependency matrix
 to specify the dependent picklist values that are available when a user selects each controlling field value.
Optionally, click 
Preview
 to test your selections.
Save your changes.
See Also:
Find Object Management Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_editing_field_dependencies.htm&language=en_US
Release
202.14
