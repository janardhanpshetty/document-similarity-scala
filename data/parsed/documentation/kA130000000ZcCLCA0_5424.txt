### Topic: Profile-Based Rollout Considerations | Salesforce
Profile-Based Rollout Considerations | Salesforce
Profile-Based Rollout Considerations
In organizations that use profile-based rollout of Chatter, certain limitations apply to user profiles, permissions sets, and the interaction between users with and users without Chatter access.
The Enable Chatter option isn’t available unless profile-based rollout of Chatter has been enabled for the organization.
Permission sets persist over user profile settings when you enable Chatter. All users with a Chatter-enabled permission set have access to Chatter, even if Chatter isn't enabled in their user profile.
Permission sets don't persist over user profile settings when you disable Chatter. All users with a Chatter-enabled user profile have access to Chatter, even if Chatter is disabled in their permission set. To prevent that person from accessing Chatter, disable Chatter in both the person's user profile and permission set.
Selecting the Enable Chatter permission in a person's user profile or permission set doesn't automatically turn on Chatter for the organization. Turn on Chatter for your organization after you modified user profiles and permission sets.
Standard Salesforce user profiles have Chatter enabled by default. You can’t disable Chatter for these standard profiles.
Don't deselect Enable Chatter permission on a cloned Chatter External user profile. The user sees an error message and can’t log in.
Don’t deselect the Enable Chatter permission on a cloned Chatter Free or Chatter Moderator profile. You get an error message and can’t save the profile.
Enabling profile-based rollout of Chatter and Chatter for an organization also enables a set of dependent Chatter user permissions. If Chatter is disabled for the organization, these permissions are also disabled at the same time. However, if Chatter is then enabled again, these permissions aren’t automatically re-enabled and the administrator must enable them explicitly.
When profile-based rollout of Chatter is enabled for an organization, Chatter is automatically enabled for standard profiles. For all custom profiles and permission sets, Chatter is automatically enabled, if any of the following user-level permissions were already enabled, either manually or as part of a license:
“Create and Own New Chatter Groups” (ChatterOwnGroups)
“Create and Share Links to Chatter Files” (ChatterFileLink)
“Invite Customers To Chatter” (ChatterInviteExternalUsers)
“Manage Chatter Messages” (ManageChatterMessages)
“Moderate Chatter” (ModerateChatter)
“Moderate Chatter Feeds” (ModerateNetworkFeeds)
“Use Case Feed” (ViewCaseInteraction)
“View All Data” (ViewAllData)
In organizations that have Work.com enabled, users with access to Chatter can’t thank users who don’t have access to Chatter.
If your organization already has Chatter enabled and switches to profile-based rollout of Chatter, manually enable Chatter in all existing custom profiles and permission sets. Otherwise, users that are assigned these custom profiles and permission sets lose access to Chatter.
When profile-based rollout of Chatter is enabled, users who can’t use Chatter because of their profile or permissions also can’t use global search. Advanced search is available to these users.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_pbr_limitations.htm&language=en_US
Release
202.14
