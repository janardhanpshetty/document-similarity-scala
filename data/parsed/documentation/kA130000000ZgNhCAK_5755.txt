### Topic: Process Scheduled Actions Considerations | Salesforce
Process Scheduled Actions Considerations | Salesforce
Process Scheduled Actions Considerations
Before you add scheduled actions to a process, understand the limitations and guidelines.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
To add scheduled actions to your process, you have two options:
Start the process only when a record is created (
1
). Select this option when you choose an object for your process.
Start the process when a record is created or edited (
2
). In addition, select the advanced option to execute actions only when specified changes are made (
3
) when you add criteria to your process.
If an action group contains scheduled actions, you can't continue evaluating the next criteria in your process after executing those actions. For details about evaluating additional criteria after executing actions, see 
Execute Actions on More Than One Criteria
.
If scheduled actions fail to execute—for example, because the user who caused the process to start is inactive—the admin who created the process receives an email with details about the failure. Salesforce makes additional attempts to execute failed scheduled actions before removing them from the queue. For details about error notifications, see 
Errors Received after a Process Starts Evaluating a Record
.
After you deactivate a process, any scheduled actions continue as usual.
You can’t delete a process if it has unexecuted groups of scheduled actions. For details about deleting unexecuted groups of scheduled actions, see 
Delete Unexecuted Scheduled Actions
.
An organization can process up to 1,000 groups of scheduled actions per hour. Each group of scheduled actions is associated with a schedule, such as “3 days from now.” When a schedule is processed, the associated actions are executed. If an organization exceeds this limit, Salesforce processes the remaining schedules in the next hour. For example, if an organization has 1,200 groups of pending actions scheduled to be processed between 4:00 PM and 5:00 PM, Salesforce processes 1000 groups between 4:00 PM and 5:00 PM and the additional 200 groups between 5:00 PM and 6:00 PM.
An organization can have up to 30,000 pending schedules and waiting flow interviews at one time.
For processes that are set to run when a record is created or edited, scheduled actions remain in the queue only as long as the criteria for those actions are still valid. If a record no longer matches the criteria, Salesforce removes the scheduled actions for that record from the queue.
For processes that are set to run when a record is created, Salesforce never reevaluates the record with that process. Scheduled actions remain in the queue, even if the record no longer meets the associated criteria when the scheduled actions are executed.
If you schedule an action for 
0
 
Days
 
Before
 a date, save the process, and then reopen the process, the schedule in the process changes to 
0
 
Days After
 the date. The process still executes at the specified time.
If you schedule an action for 
0
 
Days After
 a date, there may be a delay from the time represented by the date field before the action group executes.
Transactions and Scheduled Actions
Similar to workflow rules, immediate actions in processes are executed in the same transaction as the operation that caused the process to be triggered—when a user created or edited a record. For details about the operations that are included in that transaction, see Triggers and Order of Execution in the 
Force.com Apex Developer’s Guide
. Scheduled actions are included in a separate transaction.
Groups of scheduled actions aren’t performed independently. They’re grouped into a single batch that starts performing within one hour after the first group enters the batch. The batch can also include flow interviews that are resumed after a specified time occurs. This behavior can cause you to exceed your Apex governor limits if any actions in the group execute DML operations or SOQL queries. A DML operation is used each time a Salesforce record is created, updated, or deleted, such as when a process executes a “Create a Record” action. A SOQL query is used each time Salesforce looks up information about an existing record, such as when a process executes an “Update Records” action. For details on Apex governor limits, see 
Process Limits
.
If your process contains scheduled actions, make sure that the actions don’t perform more DML operations or SOQL queries than are permitted by the Apex governor limits.
If a group of scheduled actions fails to be executed:
Prior groups of scheduled actions in that batch’s transaction are successful.
The immediate actions for that process are successful.
All the scheduled actions in that group fail to be executed.
The remaining groups of scheduled actions in that batch are tried.
Example
Salesforce is going to process a batch of 25 groups of scheduled actions in the same transaction. Up to 100 DML operations can be used across this batch. The first 22 groups each use five (5) DML operations, and the last three (3) groups don’t use any DML operations.
The 21st group violates the Apex governor limits by trying to execute the 101st through 105th DML operations for this transaction. Only this group of scheduled actions fails to execute. This means that the first 20 groups of scheduled actions are successfully executed and the 21st group of scheduled actions fails. None of the actions in the 21st group is successful, no matter which action in the group violated the limit. Salesforce then tries to execute the remaining groups of scheduled actions in the batch. Because the 22nd group uses five (5) DML operations and the transaction has already used all of its allowed DML operations, the 22nd group also fails. Because the last three (3) groups don’t use any DML operations, those groups are successfully executed.
Schedule Limitations
If actions are scheduled for a time in the past, Salesforce executes that group of scheduled actions within one hour.
For example, if a process is configured to email an opportunity owner seven days before the close date and the process runs for an opportunity with the close date set to today, Salesforce executes the scheduled actions within an hour.
If actions are scheduled based on the current time (i.e. 3 days from now): 
The schedule is evaluated based on the timezone of the user who created the process.
If actions are scheduled based on a field value (i.e. 3 days after a case’s 
Created Date
): 
The schedule is evaluated based on the organization's timezone.
If a deactivated process still has pending scheduled actions and the record whose field the schedule is based on is changed, Salesforce recalculates the schedule for those actions. Once a process is deactivated, Salesforce ignores all other changes to associated records.
Across all of your processes and flow versions, your organization can have up to 20,000 schedules based on a field and relative time alarms.
The referenced field can’t be a:
DATE or DATETIME field that contains automatically derived functions, such as 
TODAY
 or 
NOW
.
Formula field that includes related-object merge fields.
If you change the referenced field value and the schedule hasn’t been processed, Salesforce recalculates the schedule associated with that field.
For example, if a process is configured to email an opportunity owner seven days before the opportunity close date and the close date is set to 2/20/2014, Salesforce processes the schedule on 2/13/2014 and sends the email. If the close date is later updated to 2/10/2014 and the schedule hasn’t been processed yet, Salesforce recalculates the schedule and sends the email on 2/3/2014. If Salesforce recalculates the schedule to a date in the past, Salesforce executes the associated actions shortly after you save the record.
Salesforce ignores schedules that reference null field values.
If the record or the object that the schedule is associated with is deleted, the schedule is never processed.
The following limitations apply for converted leads.
You can’t convert a lead if an unexecuted schedule is based on one of the lead’s fields.
If 
Validation and Triggers from Lead Convert
 is enabled, scheduled actions on leads aren’t executed during lead conversion.
If a campaign member based on a lead is converted before scheduled actions that are associated with that record finish, Salesforce still executes the scheduled actions.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=process_limits_scheduled.htm&language=en_US
Release
202.14
