### Topic: A Salesforce white paper focused on multi-org strategy and best practices
Multi-Org Strategy And Best Practices
The large majority customers will do best with a single org for their CRM uses.  However, there are situations where a multi-org strategy is the best course of action, such as:
Geographically independent business units
Regulatory needs for data separation
Extremely complex orgs where maintenance overhead is an issue
The attached white paper details the considerations, strategy, and best practices when dealing with a multi-org setup. 
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
