### Topic: After the NA1 split instance, which is scheduled for the weekend of April 1st 2016, our prior production SalesforceA native app versions will not be able to connect to the new salesforce org instances. Users on SalesforceA would see a "Can't connect to Salesforce" message. Users can recover from this issue via a 'force quit' on iOS, Android or if that does not work, simply upgrading to the latest SalesforceA app version on AppStore or Google Play. NOTE: It's OK to upgrade to the latest SalesforceA version prior to the NA1 split.
After the NA1 split instance, which is 
scheduled for the weekend of April 1st 2016
, our previous production SalesforceA native app 
versions prior to iOS v3.1 and Android v3.0,2
 
will not be able to recover by itself
 and connect to the salesforce org.
Users on SalesforceA would see a "
Can't connect to Salesforce
" message. Users can easily recover from this issue via a 'force quit' on iOS or Android. However, if that does not work, salesforce highly recommends upgrading to the latest SalesforceA app version on AppStore or Google Play (v3.1 iOS, v3.0.2 And).
NOTE: It's OK to upgrade to the latest SalesforceA version prior to the NA1 split. Available since MArch 30th.
Resolution
Please follow the Solution Steps below: 
1) Force quit / kill and resume the app 
to retrieve latest/new instance session URL (your orgs NEW instance reference)
                                                                               OR
2) Upgrade to latest version 
available from AppStore / Google Play Store
iOS:
 Go to AppStore from iOS device | search for 
“SalesforceA” 
-> 
will see "Update" button | Tap on "Update" button (version is 3.1)
Android:
 Go to Google Play Store from device | search for 
"SalesforceA"
 
->
 will see  "Update" button -> tap on "Update" button (version is 3.0.2)
NOTE: If solution step #1 above does not work, please upgrade the SalesforceA app via step #2
 
Note: If solution 1 won’t recover the app then please try 2nd solution.
