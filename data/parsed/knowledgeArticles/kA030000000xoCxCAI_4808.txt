### Topic: Instructions to send a Welcome Email to new subscribers who didn't receive an email after filling out a Smart Capture form.
Resolution
Use a 
Triggered Send
 to welcome new subscribers who didn't receive an email after completing a Smart Capture form (Smart Capture doesn't support the Welcome Email function used by subscriber lists).
Review the steps in our 
"Send an email via Triggered Send" article
 for set up instructions.
