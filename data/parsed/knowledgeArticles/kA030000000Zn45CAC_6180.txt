### Topic: Learn how to Enable Communities in your Sandbox for testing.
Are you interested in Communities, but you'd first like to test it in your Sandbox Organization? No problem. Learn whom to contact to request the feature in your Sandbox below. 
Note: 
Support is unable to activate this feature for you.
Resolution
First, you'll need to contact your Account Executive (AE) to have them set you up with a Communities Trial. Once this has been provisioned by your AE, you can perform either of the following steps:
 
Option 1:
Match the provisioned licenses to your Sandbox using the Match Production Licenses Tool.
 To do so, your System Administrator will need to login to the Sandbox and follow these steps:
 
1. Navigate to 
Setup
 | 
Company Profile
 | 
Company Information
.
2. Click 
Match Production Licenses
.
Option 2:
Refresh the Sandbox
 you wish to test Communities in.  
See Also:
Communities Implementation Guide
Portal Migration Cheatsheet
 
