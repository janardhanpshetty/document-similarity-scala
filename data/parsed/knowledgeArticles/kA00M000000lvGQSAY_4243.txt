### Topic: Company Community License don't have an associated Contact so we can not use the Manage External user button. Learn how to log-in as such a user.
Log-in as for Company Community License is not done using a related Contact and the manage external user button. Since Company Community Users are internal you will log in from the User record but in doing so the Admin will land in the internal organisation first, we will explain below how to change to Community.
Company Community License, is an Internal user license, which also allows access to a Community. You can read more about it here: 
Communities User Licenses
The users themselves can either log in at login.salesforce.com or use the log in URL of the Community, where they use the option to log in with internal user credentials.
Resolution
When a System Administrator needs to log in as a Customer Community user it'll need to be done, just like for other users with an internal license, by using the Login link/button on the Users list or User's record.
More about 
Log In as Another User
.
After logging in as the Customer Community User, one will then be shown the internal Salesforce organisation. It is not possible to log-in- as directly to the Community URL.
To be able to switch between the internal organisation and the Community, we will need to have the 
Global Header
 enabled for the user's profile, it can either be given with a permission set or edited on the existing profile.
More info on how to do so: 
Enable the Global Header for Communities
Once you have the View Global Header permission enabled for that User you'll be able to switch between the Internal Org and the Community at will using the dropdown available.
