### Topic: Search in Salesforce1 is different then the fullsite.
When utilizing search in Salesforce1 the search results are different depending on the terms searched for.
Resolution
There can be many causes for the search results to be different in the Native app for Android and iOS devices. A few things to keep in mind when leveraging the search options for Salesforce1 (Shown in the screenshot below) is to make sure you are clicking the search option before looking for your record on the list of available results.
If the 
Search for ""
 text is not clicked the app does not query the system for records and will only show prior cached records previously open on the app which can give the appearance that the app is not showing all the same records as the fullsite. 
Limitations:
With the global and record specific search Wildcards will work and bring up any records with the text provided. If wildcards are utilized in Field lookups they will not function as they are not supported.
 
