### Topic: You'll need the permissions in your User account in order to access Discover in Marketing Cloud.
Discover is sold on a per-user license basis. This means that only an approved number of Users may access Discover at a given time, depending on the contract.  
If you have Admin access  in your client account you can make updates to the users who do or do not have permission to access the application. 
Resolution
Manage Users and Permissions
If you don't have enough licenses, an Administrator can disable an unused license and add it for a new User. Here's how: 
 
1. Log in to the Admin account. 
2. Hover over the 
Welcome [User Name]
  in the top-right corner.
3. Click 
Administration
.
4. On the "Overview" tab, click 
Users
. 
5. If you would like to remove Discover access for a specific User, select the checkbox next to the User's name. 
6. Click 
Manage Roles
. 
7. Click 
Edit Permissions
. 
8. Ensure that 
Discover
 is checked as "Deny" or deselect the permission of 
Discover
. 
9. Click 
Save
. 
10. When you have removed any necessary Users from having access to
 
Discover, complete the same steps to enable Discover for the User who you would like to grant access by selecting the 
Allow
 check box. 
11. Click 
Save.
