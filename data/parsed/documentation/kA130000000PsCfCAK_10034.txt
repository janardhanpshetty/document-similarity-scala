### Topic: Creating and Editing Folders | Salesforce
Creating and Editing Folders | Salesforce
Creating and Editing Folders
Available in: 
All
 Editions except 
Database.com
Report folders not available in: 
Contact Manager
, 
Group
, and 
Personal
 Editions
Document folder restriction not available in: 
Developer
 Edition
User Permissions Needed
To create, edit, or delete public document folders:
“Manage Public Documents”
To create, edit, and delete public email template folders:
“Manage Public Templates”
To create, edit, and delete public report folders:
“Manage Reports in Public Folders”
To create, edit, and delete public dashboard folders:
“Manage Dashboards” AND “View All Data”
Click 
Create New Folder
 or 
Edit
 from most pages that list folders.
Enter a 
Folder Label
. The label is used to refer to the folder on user interface pages.
If you have the “Customize Application” permission, enter a unique name to be used by the API and managed packages.
Choose a 
Public Folder Access
 option. Select read/write if you want users to be able to change the folder contents. A read-only folder can be visible to users but they can't change its contents.
Select an unfiled report, dashboard, or template and click 
Add
 to store it in the new folder. Skip this step for document folders.
Choose a folder visibility option:
This folder is accessible by all users, including portal users
 gives folder access to all users in your organization, including portal users.
This folder is accessible by all users, except for portal users
 gives folder access to all users in your organization, but denies access to portal users. 
This option is only available for report and dashboard folders in organizations with a partner portal or Customer Portal enabled. If you don't have a portal, you won't see it.
This folder is hidden from all users
 makes the folder private.
This folder is accessible only by the following users
 allows you to grant access to a desired set of users:
Choose “Public Groups”, “Roles,” “Roles and Subordinates,” “Roles, Internal and Portal Subordinates” (if you have portals enabled), “Territories,” or “Territories and Subordinates” from the 
Search
 drop-down list. The choices vary by Edition and whether your organization has territory management.
Note
When you share a folder with a group, managers of the group members have no access to the folder unless those managers are also members of the group.
If the 
Available for Sharing
 list does not immediately display the desired value, enter search criteria and click 
Find
.
Select the desired value from the 
Available for Sharing
 list and click 
Add
 to move the value to the 
Shared To
 list.
Note
You can use enhanced folder sharing to give your users more detailed levels of access to reports folders and dashboard folders. For more information, see 
Turn On Enhanced Sharing for Reports and Dashboards
 and 
Share a Report or Dashboard Folder
.
Click 
Save
.
See Also:
Managing Folders
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=creating_and_editing_folders.htm&language=en_US
Release
202.14
