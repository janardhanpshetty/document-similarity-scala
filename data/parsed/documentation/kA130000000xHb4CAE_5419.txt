### Topic: Turn on Lightning Experience for Your Org | Salesforce
Turn on Lightning Experience for Your Org | Salesforce
Turn on Lightning Experience for Your Org
You've knocked off the “Learn” and “Launch” items in your rollout plan. You've set up the Salesforce features that optimize the new user interface. You've enabled the right users and designated who gets the new interface right away and who stays in the classic interface. You're ready to go live! Fortunately, it’s easy to turn on Lightning Experience using the Migration Assistant.
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view the Lightning Experience Migration Assistant:
“View Setup and Configuration”
To edit the Lightning Experience Migration Assistant:
“Customize Application”
From Setup in Salesforce Classic, click 
Lightning Experience
.
On the Lightning Experience Migration Assistant page, click the 
Lightning Experience
 button to set it to 
Enable
.
That’s it! The users you’ve enabled and set up to immediately switch to the new interface automatically start enjoying Lightning Experience when their current session refreshes or when they log in. The users you’ve enabled but opted to leave in Salesforce Classic now have access to the Switcher and can switch to Lightning Experience whenever they want.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=lex_enable_turn_on.htm&language=en_US
Release
202.14
