### Topic: Organizations that have purchased the Products module can make products required on opportunities.
How do you make Products required on Opportunities?
Resolution
Organizations that have purchased the Products module can make products required on opportunities.
To make products required on opportunities, follow the instructions below:
1. When logged in as an Administrator, click on:
Go to Setup | Customize | Opportunities | Opportunity Settings.
2. Check the box labeled "Prompt users to add products to opportunities" and click "Save".
In certain situations, this setting can be overridden by user behavior, which may result in opportunities without related products. This is a known issue and can happen in any of the following situations:
A lead is converted and an opportunity is created at the time of conversion. The user is not brought to the "Product Selection" screen for the opportunity.
An opportunity is cloned without checking the box labeled "Clone products and associated schedules." The new opportunity is created without any products
A user creates a new opportunity and clicks "Save and Add Product." On the Product Selection screen, the user clicks cancel and is brought back to the opportunity, which will be created without products
A user creates a new opportunity and clicks "Save and Add Product." The user then closes the browser window or clicks the "Back" button in the browser. The opportunity has been saved in the system, but the user has manipulated browser behavior to workaround the system design
The first three situations above are currently being evaluated by our development team and we are actively working toward resolution on these issues. The fourth situation above cannot be controlled by Salesforce since the user is working around the logic structure of the application.
Salesforce recommends using the following workaround to "enforce" product entry on each opportunity: simply make the "Amount" field read-only on opportunities. Products will have to be entered on an opportunity in order for the Amount field to be populated with a value.
Please see the following Link for information on 
Field Access
.
 5. You can also use a validation rule based on opportunity line item. Example validation rule
AND(
OR(
ISPICKVAL(StageName,"Needs Analysis"),
ISPICKVAL(StageName,"Qualified"),
ISPICKVAL(StageName,"Proposal Submitted"),
HasOpportunityLineItem = FALSE
))
It can also be used if the business requirement is to enforce adding a product only in a certain stage is reached. Sample:
AND(
ISPICKVAL(StageName,"Needs Analysis"),
HasOpportunityLineItem = FALSE
)
Note: This validation rule works good only if an Opportunity already has a Pricebook added to it.
