### Topic: Managing Multiple Business Processes | Salesforce
Managing Multiple Business Processes | Salesforce
Managing Multiple Business Processes 
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create or change business processes:
“Customize Application”
Use multiple business processes to display different picklist values for users based on their profile. Multiple business processes allow you to track separate sales, support, and lead lifecycles.
Sales Processes
Create different sales processes that include some or all of the picklist values available for the opportunity 
Stage
 field.
Lead Processes
Create different lead processes that include some or all of the picklist values available for the 
Lead Status
 field.
Support Processes
Create different support processes that include some or all of the picklist values available for the case 
Status
 field.
Solution Processes
Create different solution processes that include some or all of the picklist values available for the 
Status
 field.
After creating a sales, support, lead, or solution process, assign the process to a record type. The record type determines the user profiles that are associated with the business process.
To view a list of business processes, from Setup, enter 
Processes
 in the 
Quick Find
 box, then select the appropriate link.
Click 
New
 to create a new business process.
Click 
Edit
 to change the name or inactivate the business process.
Click 
Del
 to delete an unused business process.
Click the name of the business process to edit the picklist values associated with it.
See Also:
Edit Picklists for Record Types and Business Processes
Administrator tip sheet: Tips & Hints for Multiple Business Processes
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_processes.htm&language=en_US
Release
202.14
