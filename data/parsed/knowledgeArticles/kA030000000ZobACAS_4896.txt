### Topic: Learn if the DEManager can be used in Web Collect to Add_Update or upsert existing Data Extension records in Marketing Cloud Data Management.
Resolution
No. DEManger only supports Add or Update. For an add_update or upsert action, it's recommended to use the SOAP API data extension functionality for forms that are hosted outside of the Marketing Cloud, or Smart Capture for forms in Marketing Cloud landing pages.
