### Topic: This is expected behavior. An email template which is sent by Workflow/Approval automation will populate all merge fields in the template (provided there is available data for those fields), regardless of the Field Level Security of the user who causes the email to be sent.
Automated emails, such as Workflow or Approval Email Alerts or Approval Process Notifications, populate merge fields in the associated Template when those fields are hidden through Field Level Security.
Resolution
This is expected behavior. An email template which is sent by Workflow/Approval automation will populate all merge fields in the template (provided there is available data for those fields), regardless of the Field Level Security of the user who causes the email to be sent.
For example:
* 'Email Template A' contains the {!Account.
AnnualRevenue} merge field
* 'User X' cannot see Account Annual Revenue due to Field Level Security
** If 'User X' tries to manually send this email template, the {!Account.
AnnualRevenue} merge field will not be populated.
** However if 'User X' triggers a Workflow Rule with Email Alert using 
'Email Template A', the generated email will populate the  {!Account.
AnnualRevenue} merge field. This allows automated emails defined by Administrators to include data which the user triggering the email cannot access them self. (For example, we might want to automate an alert with pertinent Account data - including Annual Revenue - to a Manager, while still keeping the field hidden for the end users who would be generating these emails).
