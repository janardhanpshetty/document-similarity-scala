### Topic: Choose a Report Type | Salesforce
Choose a Report Type | Salesforce
Choose a Report Type
A report type is a set of rules that determine which records and fields appear in a report. You can start with one of the available standard report types or use a custom report type provided by your administrator.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
User Permissions Needed
To create, edit, and delete reports:
“Create and Customize Reports”
AND
“Report Builder”
Choosing the right report type is one of the most important steps in creating a report.
From the Reports tab, click 
New Report
.
Select the report type, and then click 
Create
.
Note
You can’t change the report type after the report is created.
For more information, review information about standard and custom report types.
Standard Report Types
Salesforce provides a rich collection of standard report types that you can tailor to your unique requirements. You rarely need to create a brand-new report.
Pre-Designed Custom Report Types
Some Salesforce features come with custom report types that are designed for you in advance, so you don't have to create a new report.
Parent topic:
 
Build a New Report
Next topic:
 
Choose a Report Format
See Also:
Set Up a Custom Report Type
Why doesn't my report return the data I expect?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_builder_selecting_a_report_type.htm&language=en_US
Release
202.14
