### Topic: Examples of Advanced Formula Fields | Salesforce
Examples of Advanced Formula Fields | Salesforce
Examples of Advanced Formula Fields
Review examples of formula fields for various types of apps that you can use and modify for your own purposes.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To view formula field details:
“View Setup and Configuration”
To create, change, or delete formula fields:
“Customize Application”
This document contains custom formula samples for the following topics. For details about using the functions included in these samples, see 
Formula Operators and Functions
.
Sample Account Management Formulas
Sample Account Media Service Formulas
Sample Case Management Formulas
Sample Commission Calculations Formulas
Sample Contact Management Formulas
Sample Data Categorization Formulas
Sample Date Formulas
Sample Discounting Formulas
Sample Employee Services Formulas
Sample Expense Tracking Formulas
Sample Financial Calculations Formulas
Sample Image Link Formulas
Sample Integration Link Formulas
Sample Lead Management Formulas
Sample Metrics Formulas
Sample Opportunity Management Formulas
Sample Pricing Formulas
Sample Scoring Calculations Formulas
See Also:
Formulas: How Do I ... ?
Tips for Building Formulas
Quick Reference Guide: Formula Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=useful_advanced_formulas.htm&language=en_US
Release
202.14
