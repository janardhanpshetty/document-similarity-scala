### Topic: Can I rename or remove my custom domain name? | Salesforce
Can I rename or remove my custom domain name? | Salesforce
Can I rename or remove my custom domain name?
You can’t change your custom domain name, or reverse its deployment, once deployed. If you have a special need to change it, contact Salesforce Customer Support.
See Also:
What is My Domain?
Which Salesforce Editions is My Domain available in?
What are My Domain's advantages?
Does My Domain work differently in different Salesforce Editions?
Does My Domain work in sandboxes?
What are the differences between the Redirect Policy options?
How does My Domain work with single sign-on?
Is My Domain available for the API?
Is the subdomain for My Domain related to the subdomain for Sites?
Is there a limit on how long our subdomain can be?
After we set up My Domain, will we still be able to log in from https://login.salesforce.com?
Will we still be able to log in from a URL that includes a Salesforce instance, like https://yourInstance.salesforce.com/?
Can we still use our old Salesforce bookmarks?
Will our Visualforce and content (files) page URLs change?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_domain_name_rename.htm&language=en_US
Release
202.14
