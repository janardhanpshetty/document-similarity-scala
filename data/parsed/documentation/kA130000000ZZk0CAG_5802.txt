### Topic: Find and Endorse People Knowledgeable about a Topic | Salesforce
Find and Endorse People Knowledgeable about a Topic | Salesforce
Find and Endorse People Knowledgeable about a Topic
Discover and recognize experts using the Knowledgeable People section of topic pages and the Overview tab of user profile pages.
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
The topic page shows the five most knowledgeable people based on their activity on the topic and recognition received for the topic. For example, Chatter considers how often people:
Were mentioned in posts or in comments on posts with the topic
Received likes on comments on posts with the topic
Received endorsements for the topic
Endorsements are a strong signal of knowledge, so if someone endorses you, you’re automatically included in the list of knowledgeable people.
Note
Knowledgeable people are calculated and updated once a day.
To change endorsements:
Endorse someone you think is knowledgeable about a topic by clicking 
Endorse
 under the person’s name. If the person you want to endorse doesn’t appear in the Knowledgeable People section, click 
Endorse People
 to see the full list and to search for other people. (You can’t endorse yourself, Chatter Free users, or customers.)
Remove your endorsement of someone at any time by clicking 
 next to Endorsed under the person’s name. Removing your endorsement of someone doesn’t necessarily remove them from the knowledgeable people list; they might be endorsed by other people or have significant activity on the topic.
To remove yourself from the knowledgeable people list:
Opt out by clicking 
Hide me
 under your name. No one can see you in the list after you hide yourself. If you change your mind, you can opt back in by clicking 
Endorse People
 and then 
Show me
 next to your name.
To see a person’s full range of expertise:
Navigate to their user profile page, and click the Overview tab. Then click topic names to see topic detail pages, or click thumbs-up icons to add or remove your endorsements.
Important
If Work.com Skills are enabled, they replace knowledgeable topics on profile pages. (For Salesforce Communities users, skills replace topics only in the default community.) Self-declared skills are considered when calculating knowledge levels.
See Also:
Skills Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_topics_knowledgeable_people.htm&language=en_US
Release
202.14
