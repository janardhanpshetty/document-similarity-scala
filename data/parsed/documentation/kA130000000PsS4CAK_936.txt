### Topic: Tips and Additional Considerations for Connection Finder | Salesforce
Tips and Additional Considerations for Connection Finder | Salesforce
Tips and Additional Considerations for Connection Finder
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Create a custom list view or custom report to track your survey responses in one location. Add the 
Uses Salesforce
 field to contact list views and reports. Add the 
Salesforce Customer
 field to account list views and reports.
Create translated versions of the default connection finder template to send to international partners. To do so, clone the default template, make the necessary language changes, and include the survey URL: 
{!Contact.PartnerSurveyURL}
. You can then choose the appropriate template when sending out requests to your partners.
Note
The survey page is in English, but includes a 
Language
 drop-down so that recipients can view the survey in their native language.
When creating validation rules for the 
Uses Salesforce
 field, use the API value, not the label seen in the user interface. For example:
CONTAINS(TEXT(UseSalesforce ), 'YesNotAdmin')
Uses Salesforce
 drop-down value
API value
No
No
No Response
NoResponse
Not Sure
NotSure
Yes, admin user
YesAdmin
Yes, not admin user
YesNotAdmin
If a partner provides details for their Salesforce administrator, Salesforce checks to see if there is already a contact in your Salesforce organization with that email address. If so, the 
Uses Salesforce
 field on the contact record is set to 
Yes, admin user
. If the field is already set to 
No
, it is not updated.
Note
A partner can only add an administrator if one does not already exist as a contact in your organization.
The 
Salesforce Customer
 field is dependent upon the values in its contacts' 
Uses Salesforce
 field.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=partner_survey_tips.htm&language=en_US
Release
202.14
