### Topic: There are times when your Total Leads value will be different in the campaign records versus a report of campaign members. In these instances you should try running a report of campaigns with leads and converted lead information report to verify the numbers are accurate.
@Kanchan Rohlania
 FYI
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
May 26, 2015 at 9:44 AM
  
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Configuration
Why do I see different values for Total Leads on the Campaign record and in a report?
Resolution
This happens when you have duplicate leads attached to the same campaign and then convert the lead into an existing contact record that is also associated to the same campaign record. You can verify that this is what has happened by running a "Campaign with Leads and Converted Leads Report". Group the results by the contact ID and you should see contacts with multiple leads associated to them.
This is currently the expected behavior for these fields since the leads were part of the campaign and then were converted into an existing contact so the lead record is still listed in the campaign as a converted campaign but there is one less contact record since it was converted into an existing contact record.
