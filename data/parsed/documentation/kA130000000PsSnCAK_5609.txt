### Topic: Get Started with Dashboards | Salesforce
Get Started with Dashboards | Salesforce
Get Started with Dashboards
After you’ve found the data that you need, use a dashboard to find patterns, stay up-to-the-minute on changes, and share knowledge that you and your coworkers can act on in real time.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view and refresh dashboards:
“Run Reports” AND access to dashboard folder
To create dashboards:
“Run Reports” AND “Manage Dashboards”
To edit and delete dashboards you created:
“Run Reports” AND “Manage Dashboards”
To edit and delete dashboards you didn’t create:
“Run Reports,” “Manage Dashboards,” AND “View All Data”
Users with a Salesforce Platform or Salesforce Platform One user license can only view a dashboard if the dashboard running user also has the same license type. Consider creating separate dashboards for users with different license types.
Dashboards in Group Edition organizations are view-only.
Clicking the Dashboards tab displays the dashboard you viewed most recently. The top of the page shows the time the dashboard was refreshed last and the user whose permissions determine what data is visible on the dashboard. If you can’t access a dashboard, verify your folder permissions.
Parent topic:
 
Dashboards Help You Visualize Complex Information
Next topic:
 
Use Dashboards on the iPad
See Also:
Dashboards Help You Visualize Complex Information
Create a Salesforce Classic Dashboard
Refresh Dashboard Data
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dashboards_display.htm&language=en_US
Release
202.14
