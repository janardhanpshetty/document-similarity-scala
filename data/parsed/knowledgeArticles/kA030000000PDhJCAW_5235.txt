### Topic: Learn why you're experience delays with data updating and to do to fix the problem.
If you're having 
trouble with data updating in Salesforce Classic on your iOS device
, it could be because of network issues or it could be an issue of needing to manually initiate a refresh. We'll show you how to fix the problem, to make sure you're getting the data you need on the go.
Resolution
The easiest way to update the connection is to Refresh All Data from the AppInfo page.
1. Tap the 
More...  
button at the bottom right
2. Tap 
App Info
.
3. On the "Application Information screen", tap 
Sync 
and then tap 
Refresh All Data
.
This should revive the connection. You can confirm by adding a test activity or calendar event from the device and saving it OR adding a test activity from the Home online and then completing a "Refresh All data" once again.
 
