### Topic: Custom and standard objects can be added to the Salesforce consoles.
With the exception of forecasts, ideas, answers, connections, and console all objects can be added to the console. See 
Creating a Salesforce Console App
 for more information on setting up the console.
Resolution
