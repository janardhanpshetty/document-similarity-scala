### Topic: When using the Community Builder, you may notice that when you select the 'Case' page, you are instead routed to the 'Error' page and therefore you are unable to edit the 'Case' page from within the Community Builder.
When using the Community Builder, you may notice that when you select the 'Case' page, you are instead routed to the 'Error' page and therefore you are unable to edit the 'Case' page from within the Community Builder.
Resolution
This can happen when the 'Case' page includes related lists that are not available in the page layout assigned to you. For example, if the 'Case' page includes the Attachments related list but this related list is not available in your profile-specific page layout, the error will occur.
To fix this:
1- Confirm that the page has related lists that are not available to you. To do this, from the Community Builder page click Go To Site.com Studio
2- Go to Site Pages and double click on 'main'
3- Click the pencil-like icon on the 'Case' page on the left sidebar 
There you can see the related lists available, and you can compare this with your page layout. For example, the page below contains both the Case Comments and Attachments related lists.
- Either remove these components from the 'Case' page in the Site.com Studio (assuming that community users will not need these); this can be done by clicking the component on the left side bar and selecting 'Delete'
or
- add these related lists to the page layout assigned to your profile. 
