### Topic: Review this article if you need to understand how Visual Workflow can be enabled for Customer Migration purpose.
NOTE: THIS IS AN INTERNAL ARTICLE NOT TO BE PUBLISHED EXTERNALLY
Resolution
Visual Workflow must be provisioned into the customer's active production org via the contract and order process.
The Account Owner (Sales representative) for the customer is responsible for using the standard product swap process to replace the customer's existing Firefly/VPM products with the new Visual Workflow product.  To set the right expectation, this process can take days or sometimes weeks to complete since it involves executing a contract amendment and requires customer signatures at the appropriate authorization levels.
If customer urgently needs the Visual Workflow functionality, the sales operations team can be contacted to provide a temporary 30-day courtesy trial while the product swap process is happening in parallel.
