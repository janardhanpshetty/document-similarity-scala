### Topic: Grant Login Access | Salesforce
Grant Login Access | Salesforce
Grant Login Access
To assist you, your administrator or a customer support representative can log in to the application using your login. By default, your company’s administrators can access your account without any action from you. If your organization requires users to grant login access to administrators, you can grant access for a specified duration. You can also grant access to representatives of support organizations.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
Granting administrator access available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view setup audit trail history:
“View Setup and Configuration”
Watch a Demo: 
Letting Your Salesforce Administrator Access Your Account
For security reasons, the maximum period for granting access is one year. During the time you granted access, administrators or support representatives can use your login and access your data to help you resolve problems.
From your personal settings, enter 
Login Access
 in the 
Quick Find
 box, then select the option to grant login access.
Set the access expiration date by choosing a value from the picklist.
Click 
Save
.
If an administrator, support representative, or publisher makes setup changes using your login, the setup audit trail lists the changes and the username. In some organizations, records of clicks made by an administrator logged in as you are also kept for auditing purposes.
Note
You can’t grant access to certain support organizations if your administrator set up restrictions, or if a packaged application’s license prevents it.
See Also:
Get to Know Your Salesforce Admin
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=granting_login_access.htm&language=en_US
Release
202.14
