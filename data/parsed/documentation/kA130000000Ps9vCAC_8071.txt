### Topic: Use a Tabular Report in a Dashboard | Salesforce
Use a Tabular Report in a Dashboard | Salesforce
Use a Tabular Report in a Dashboard
You can use a tabular report as the source report for a dashboard table or chart component, if you limit the number of rows it returns.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
User Permissions Needed
To create, edit, and delete reports:
“Create and Customize Reports”
AND
“Report Builder”
Click 
Add
 | 
Row Limit
.
Set the 
Row Limit
 to 
10
, 
25
, or 
Custom
. If you choose custom enter a number between one and 99.
Set the 
Sort By
 and sort order options. If you chose 
Limit Rows by this Field
 for a column, these options are already set.
Click 
OK
.
Click 
Dashboard Settings
 in the toolbar.
Choose a 
Name
 and 
Value
 to use in dashboard tables and charts. Tables show both name and value. Charts are grouped by name.
Click 
OK
. You can now use this tabular report as the source report for a dashboard component.
Tip
When you create a dashboard component to display your tabular report, you can use the dashboard component editor to override the settings you chose in 
Dashboard Settings
.
Parent topic:
 
Show Report Data in Tables
Previous topic:
 
Highlight Data Ranges
Next topic:
 
Limit Report Results
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_tabular_in_dashboards.htm&language=en_US
Release
202.14
