### Topic: Displaying Alerts | Salesforce
Displaying Alerts | Salesforce
Displaying Alerts
This example creates a button that opens a popup dialog with a welcome message containing the user's first name.
User Permissions Needed
To create or change custom buttons or links:
“Customize Application”
Available in: Salesforce Classic and Lightning Experience
Custom buttons and links are available in: 
All
 Editions
Visualforce pages and s-controls are available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Define a button with the following attributes.
Display Type
─Detail Page Button
Behavior
─Execute JavaScript
Content Source
─OnClick JavaScript
Use the following sample code.
alert ("Hello {!$User.FirstName}");
Add the button to the appropriate page layout.
See Also:
Custom Button and Link Samples
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=links_useful_custom_buttons_displaying_alerts.htm&language=en_US
Release
202.14
