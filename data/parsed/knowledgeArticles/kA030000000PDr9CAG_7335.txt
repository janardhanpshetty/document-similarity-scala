### Topic: This Article will help you to Display Base64 Data on a Visual force page
How can I Display Base64 Data on a Visual force page?
Resolution
<apex:page controller="ViewImage" cache="true">
      <img src="data:{!att.ContentType};base64,{!image}" /> </apex:page>
 
public class ViewImage {
      public Attachment att {
            get {
                  if (att == null) {
                        String id = ApexPages.currentPage().getParameters().get('AttachmentID');
                        att = [SELECT Body, ContentType, Name FROM Attachment WHERE ID = :id];
                  }
                  return att;
            }
            private set;
      }
      public String image {
            get {
                  return EncodingUtil.Base64Encode(att.body);
            }
      }
}
