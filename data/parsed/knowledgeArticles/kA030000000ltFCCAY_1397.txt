### Topic: Detailed steps on how to reassign goals and metrics in the new Goals available with Summer '15 release.
How can I reassign the Goal and the Metrics I created to another user?
 
Resolution
Reassign a Goal:
1. Navigate to the 
Goals
 tab
2. Click on the image of the Goal that you would like to reassign
3. Next to the 
Owner Name
, click the
 [Change]
 link
4. Type the name of the user that you would like to reassign the Goal to
5. Click 
Save
Reassign a Metric:
1. Navigate to the 
Goals
 tab
2. Click on the image of the Goal that the metric is part of
3. Click on the 
Metric
4. Next to the 
Owner Name
, click the
 [Change]
 link
4. Type the name of the user that you would like to reassign the Metric to
5. Click 
Save
 
