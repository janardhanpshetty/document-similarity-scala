### Topic: Manage Tasks | Salesforce
Manage Tasks | Salesforce
Manage Tasks
In Salesforce, tasks are a powerful tool to help you track your work and close deals. Relate them to opportunities, leads, accounts, and contacts, and manage them on those records, in lists, and in reports.
Manage Tasks in Lightning Experience
Create, update, and monitor your own tasks, tasks delegated to others, and tasks on an opportunity, a lead, an account, or a contact. Use the Home page, the task list, or the activity timeline. You can also track tasks in reports.
Manage Tasks in Salesforce Classic
You can track, create, and update your own and others’ tasks in different locations in Salesforce. Tasks are displayed in activities list views and calendar views; Chatter feeds; and records that tasks are related to, such as contacts and accounts. You can also track tasks in reports.
Parent topic:
 
Tasks
Next topic:
 
Considerations for Using Tasks
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=viewing_tasks.htm&language=en_US
Release
202.14
