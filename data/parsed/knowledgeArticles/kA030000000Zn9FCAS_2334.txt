### Topic: It is known that Triggers on feedItems do NOT Fire when Send Email Action is used to send an email from Case Feed, although the Feed Item is logged.
** R&D HAS CONFIRMED THIS IS WAD AND THERE IS NO PLAN TO FIX SINCE ALTERNATE WAYS ARE AVAILABLE TO ACHIEVE DESIRED FUNCTIONALITY **
It is known that Triggers on feedItems do NOT Fire when Send Email Action is used to send an email from Case Feed, although the Feed Item is logged.
This does not fire Trigger on feedItem:
trigger MyFeedItemTrigger on FeedItem (after insert) {
for (FeedItem fi : trigger.new) {
String parentId = fi.parentId;
System.debug('****************
****************** Trigger fired - feedItem '+ parentID);
}
}
NOTE
To see 'Send Email' Action on Case Page Layout
1. Email to Case should be enabled
2. You have to over ride Global Publisher Actions on CASE Page layout.
3. Send Email should be in Actions
 
Resolution
*** WORKAROUND ***
This works:
trigger MyFeedTaskTrigger on Task (after insert) {
for (Task fi : trigger.new) {
String parentId = fi.WhatId;
System.debug('****************
****************** Trigger fired - TASK '+ parentID);
}
}
