### Topic: Embed Wave Dashboards in Salesforce Classic Pages | Salesforce
Embed Wave Dashboards in Salesforce Classic Pages | Salesforce
Embed Wave Dashboards in Salesforce Classic Pages
Add a Wave Analytics dashboard to a detail page layout. On an account detail page, for example, you can include a dashboard of service issues associated with the account. Users can drill in, apply filters, and explore in the dashboard as if they were viewing it in a Wave Analytics window.
Available in Salesforce Classic.
Available for an extra cost in 
Enterprise
, 
Performance
, and 
Unlimited
 Editions. Also available in 
Developer
 Edition
.
User Permissions Needed
To customize page layouts:
“Customize Application”
To view embedded Wave dashboards:
Analytics Cloud - Wave Analytics Platform permission set license with “Use Wave Analytics” permission
In the enhanced page layout editor, select Wave Analytics Assets in the left column of the palette.
Drag an item from the list of available dashboards to a detail section on the page layout.
After the dashboard is positioned in the layout, you can change properties such as height by double-clicking the element or clicking the wrench icon next to it (
).
To set up the dashboard to show only the data that’s relevant for the record being viewed, use field mapping. Field mapping links data fields in the dashboard to the object’s fields. Refer to the table to understand the building blocks for field mapping.
Building Block
What It Is
Where To Find It
Dataset System Name
Wave dataset
On the Wave home page, select Edit on the dataset. The System Name is in the left panel of the edit page for the dataset. (If your org has namespaces, include the namespace prefix and two underscores before the dataset system name.)
Field
Dimension
 in the Wave dataset
Click the Explore icon to open the widget. Then select 
Show SAQL
 from the Options menu. Look for dimension names in the “group by” statements.
Value
Field in the Salesforce object, or a specific value
In Setup, find the object you want, and select Fields. Use the 
Field Name
 (also known as the API name).
The JSON field mapping follows this format:
{
    "
dataset1_system_name
": {
        "
field1
": ["
$value1
"],
        "
field2
”: ["
$value2
"]
    },
    "
dataset2_system_name"
: {
        "
field1
": ["
$value1
"],
        "
field2
”: ["
$value2
"]
    }
}
For example, if the dashboard shows data from a dataset named Service, with the dimensions Account and Industry, the field mapping would be defined as:
{
    "service": {
        "account": ["
$Name
"],
        "industry”: ["
$Industry
"]
    }
}
Note
To filter by a dimension, be sure to create a list widget on the dashboard for the dimension. Only dimensions that have a list selector on the dashboard can be included in the 
Field Mapping
 JSON.
Additional properties of Wave Analytics Assets:
The 
Show title
 checkbox gives you control over the visibility of the dashboard title.
The 
Show Sharing Icon
 option lets you include the Share icon on the dashboard. If the icon is present, users can click to open the Share dialog, where they can post to Chatter and download images and data.
The 
Hide on error
 checkbox gives you control over whether the Wave Analytics asset appears if there is an error (such as the dashboard can’t be found).
Dashboards include the date and time when they were last refreshed.
Be aware of these limits and limitations:
You can add one dashboard per page layout.
Wave Analytics dashboards aren’t supported in the original page layout editor. If you open and then save a layout with a Wave dashboard in the original page layout editor, the dashboard is deleted.
For information about working with the enhanced page layout editor, see 
Customizing Page Layouts with the Enhanced Page Layout Editor
.
See Also:
Trailhead
: Place Dashboards Where Users Are Looking
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_dashboard_embed.htm&language=en_US
Release
202.14
