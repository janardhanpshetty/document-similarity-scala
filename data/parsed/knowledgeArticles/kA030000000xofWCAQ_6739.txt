### Topic: This article describes how to enable the "Edit My Own Posts" and "Edit Posts on Records I Own" profile permissions that are associated with the Summer '15 feature Feed Post Editing.
I have enabled the 
Allow users to edit posts and comments
 Chatter option for my organization, and users with standard profiles are able to edit their posts and comments, but users with custom profiles are not.
How can I give them this ability?
Resolution
By default, feed post editing is disabled on all custom profiles. Modify your custom profiles to enable the feature for your users.
 
1. Navigate to 
Setup | Manage Users | Profiles
.
2. Click on the custom profile for which you want to enable feed post editing.
3. In the "System Permissions" section, click 
Edit
.
4. Select the edit feed post permissions that you want to enable.
Select 
Edit My Own Posts
 to allow users that are assigned this custom profile to edit their own posts and comments.
Select 
Edit Posts on Records I Own
 to allow users that are assigned this custom profile to edit posts and comments on records they own, including posts and comments made by other users.
5. Click 
Save
.
 
Requirement: The "
Manage Profiles and Permission Sets
" user permission is required to create custom profiles.
To remove one of these permissions that was previously granted to a user profile, simply repeat the steps above and at Step 4, deselect the desired feature.
See Also:
How can I enable Chatter Post Feed Editing for my organization
?
How do I disable Feed Post Editing?
