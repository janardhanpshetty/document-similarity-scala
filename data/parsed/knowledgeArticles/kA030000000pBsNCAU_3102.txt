### Topic: Learn if Marketing Cloud supports Digital Signatures.
Resolution
Yes, the Marketing Cloud can support signing emails using SMIME. This features identifies you as the authenticated sender of the message, and it verifies that the message hasn't been changed since the email Send. Enabling this feature will allow you to upload your key (no password) into the Marketing Cloud and then attach that key to a delivery profile. At send time the signature is generated and inserted into the body of the email.
