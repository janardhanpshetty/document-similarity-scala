### Topic: The article will provide steps to follow for MCS monitoring request.
Please follow this request when requesting mcs monitoring request.
 
Resolution
These are the steps that you need to setup monitoring for mcs orgs.
Steps to request Monitoring
1. Create a case  (not visible in portal)
Fill out 
https://docs.google.com/a/salesforce.com/forms/d/10afChVHSEkP1RK5ChWMV3kQvqr4DYHSEzvk54DqKtuM/viewform
Please include case link
 
2. Include business case on the request for approval from Brad using this link:
https://docs.google.com/a/salesforce.com/forms/d/1DjSq2tDWnsIFocQWQTarh6RgfXAzKLmWSir5AzKE8hA/viewform
**************** Internal only to the engineer setting this up *********************
1. If you are the case owner and have completed setting up the monitor /stew query please update the sheet below with the details, also record the Alerts in sheet2
https://docs.google.com/spreadsheets/d/1YgijzlQjNfcPT38f6nH_GwEFPQRRIQGSVoLsWaDX2-o/edit#gid=0
