### Topic: Security Infrastructure | Salesforce
Security Infrastructure | Salesforce
Security Infrastructure
Salesforce utilizes some of the most advanced technology for Internet security available today. When you access the application using a Salesforce-supported browser, Transport Layer Security (TLS) technology protects your information using both server authentication and Classic Encryption, ensuring that your data is safe, secure, and available only to registered users in your organization.
One of the core features of a multi-tenant platform is the use of a single pool of computing resources to service the needs of many different customers. Salesforce protects your organization's data from all other customer organizations by using a unique organization identifier, which is associated with each user's session. Once you log in to your organization, your subsequent requests are associated with your organization, using this identifier.
In addition, Salesforce is hosted in a secure server environment that uses a firewall and other advanced technology to prevent interference or access from outside intruders.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_overview_infrastructure.htm&language=en_US
Release
202.14
