### Topic: Due to capacity planning best practices, we will lock Salesforce Developer Edition (DE) orgs that are inactive for 6 months or more. For more information on this policy, see the About the Force.com Developer Edition Environments blog post.
Frequently Asked Questions
​
 
What if my Developer Edition org gets locked and I want to regain access?
If your DE org is locked and you would like to regain access, you may request to have it unlocked by opening a case with Customer Support via the Help & Training portal.
When you unlock your Developer Edition org, it will have all the same data and configurations that it had before the org was locked.
If you are a partner and your DE org is locked, log a case in the Partner Community at p.force.com/support.
 
If my Developer Edition org gets locked, how long do I have to request to unlock it?
​
At this time, we do not have any plans to delete inactive Developer Edition orgs.
 
If my Developer Edition org gets locked, can I get a new one to replace the old one?
Yes, you may obtain a new Developer Edition org at 
developer.salesforce.com
.
Any usernames previously locked will be unavailable for reuse.
 
If my Developer Edition org gets locked, what do I need to do in order to continue using my Success Community profile associated with this particular DE org?
If you would like to continue to use this DE org, and associated Success Community profiles, simply log in at least once every 6 months.
 
Will you lock inactive packaging orgs?
An inactive DE org will not be locked if the user has created and uploaded a managed package, and that package is installed in another org. In other words, packaging orgs are exempt from this initiative.
 
Why are you locking inactive Developer Edition orgs?
Due to capacity planning best practices, we will lock DE orgs that are inactive for 6 months or more. For more information on this policy, see the 
About the Force.com Developer Edition Environments
 
b
l
o
g
 
p
o
s
t
.
Resolution
