### Topic: This article goes over the configuration changes needed in Identity Connect to allow users in Active Directory with duplicate email addresses to be synced into Salesforce. This will also give a working example of Changing the Default Associating Rules and using JavaScript Transformation scripts within Identity Connec.
For further information on any of the items mentioned below please refer to the 
Identity Connect Implementation Guide (identity_connect_impl_guide.pdf).
When testing in Salesforce Sandbox Orgs often when creating test users you may create several users with different usernames but the same email address.
If users are set up in Active Directory with duplicate email addresses out of the box Identity Connect will not sync these users.  The reason, is because in the out of the box configuration for Identity Connect it sets the username for Salesforce to the email specified in Active directory for that user automatically.  Therefore, when running a sync in Identity Connect as soon as it finds a duplicate Username it will give a error stating duplicate username exists.
 
 In this example, the first step that needs to be done is to change default association rule to remove the duplicate check on email.  In the below screenshot  the 
Email
, 
Phone
, 
Mobile Phone
 and 
Title
 have been removed from the rule. The only check left is to check for duplicate 
FirstName
 and 
LastName
. These changes removes any conflicts with users being synced from Active directory to Salesforce with duplicate email addresses.
 
 
The next steps will be to remove the default mapping value for Salesforce 
Username
 field which is currently set to default to the email specified for the Active Directory user.  In the 
Identity Connect Administrator
 navigate to the 
Salesforce Org
 tab and click on the 
Mapping
 tab.  
 
 
In the 
Sample User
 text box type the full or partial name of a current AD user and select them from the values that come up in this example that user is 
ICTestUser7
.
 
 
Scroll down until you get to the 
Username
 field in the 
Salesforce
 column. Notice in the 
Active Directory
 column the value is 
email
. This means the email specified for the Active directory user will be the default value the 
Username
 field. Notice in the 
Sample
 column you will see the value of the email address given for this sample Active Directory user 
ICTestUser7
 in this example that value for Username is currently 
IdenityConnect2013@gmail.com
.
 
 
Double click on the 
Username
 column and this will bring up the 
Salesforce Attribute
 screen for the 
Username
 field.  Select the 
Attribute List
 tab and in the drop down list box delete any values you see blanking out the field.
 
 
Click on the 
Transformation Script
 tab. In this example JavaCcript added sets the Salesforce 
Username
 field to the Active Directory 
givenName
 field appending a
 ‘@gmail.com’
 to the end of the Active Directory 
givenName
 value.  
 
Java script:
source.givenName + '@gmail.com'
 
 
After making this change examine the 
Example Result
 and then select the 
Update
 button this will bring you back to the mapping screen. Notice the Sample value for the
 Username
 is now 
ICTestUser7@gmail.com
 which is reflective of the given JavaScript rule.
 
In the 
Identity Connect Administrator
 navigate back the 
Salesforce Org
 Sync tab and select the 
Analyze Associating Now
 button and wait for the screen to refresh.
 
From the 
Sync
 tab check the box next to the test user being used in this example I
CTestUser7
 and then select the 
Sync Selected Record
 button this will bring up the 
Single User Data Synchronization
 screen.
 
 
From the 
Single User Data Synchronization
 screen select the 
Sync Now
 button. The sync should complete now without the duplicate username error and instead return back to the 
Single User Data Synchronization
 screen showing the 
Current
 , 
Before Sync
 and 
After Sync
 values.
 
Resolution
