### Topic: Opportunities with Quotes and Quote Line Items Report | Salesforce
Opportunities with Quotes and Quote Line Items Report | Salesforce
Opportunities with Quotes and Quote Line Items Report
View details about the quotes associated with opportunities, and the line items for each quote. The default settings provide the most commonly used information from each object, but you can customize the report to see any opportunity, quote, or quote line item field.
Quotes are available in: Salesforce Classic
Quotes are available in: 
Performance
 and 
Developer
 Editions and in 
Professional
, 
Enterprise
, and 
Unlimited
 Editions with the Sales Cloud
If your organization uses multicurrency or advanced currency management, you have additional options for customizing this report. When you select report columns, you can select the “converted” version of an amount or total column to show its value converted to a different currency. Select the currency you want to convert to under Advanced Settings when you select your report criteria.
The default settings for this report are:
Format
Summary
Summary Fields
Amount
 (sum)
Quote Discount
 (sum)
Groupings
The default report shows you results grouped first by 
Opportunity Name
 and then by 
Quote Name
. Each quote line item is listed beneath its associated quote.
Selected Columns
Object Information Type
Columns
Opportunity Information
Opportunity Name
Amount
Quote Information
Quote Name
Discount
Syncing
Status
Quote Line Item Information
Quote Line Item: Discount
Product: Product Name
Line Item Number
Sales Price
List Price
Quote Line Item: Subtotal
Quote Line Item: Total Price
See Also:
The Report Run Page
Limit Report Results
Opportunity Reports
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_opps_with_quotes_qlis.htm&language=en_US
Release
202.14
