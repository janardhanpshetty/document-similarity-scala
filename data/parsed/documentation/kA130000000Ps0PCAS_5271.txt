### Topic: Importing Package Data | Salesforce
Importing Package Data | Salesforce
Importing Package Data
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To import Force.com AppExchange package data:
The permissions required to use the import tool you choose, such as the import wizard or Data Loader.
When you uninstall an AppExchange package, Salesforce automatically creates an export file containing the package data as well as any associated notes and attachments. If you choose to install the package again, you can import this data. 
To import your AppExchange package data, use one of the following tools that is available for your Edition:
For Group Edition, use the appropriate import wizard.
For Professional Edition, use the appropriate import wizard or any compatible Salesforce ISV Partner integration tool.
For Enterprise, Developer, Performance, and Unlimited Edition, use the Data Loader.
Notes on Importing AppExchange Package Data
Salesforce converts date fields into date/time fields upon export. Convert the appropriate fields into date fields before you import.
Salesforce exports all date/time fields in Greenwich Mean Time (GMT). Before importing these fields, convert them to the appropriate time zone.
The value of auto number fields may be different when you import. To retain the old values, create a new custom auto number field on a custom object before importing the data.
Salesforce updates system fields such as 
Created Date
 and 
Last Modified Date
 when you import. To retain the old values for these fields, contact Salesforce support.
Relationships are not included in the export file. Recreate any master-detail or lookup relationships after importing your data.
Record type IDs are exported but not the record type name.
Field history is not exported.
Recreate any customizations that you made to the package after installation.
See Also:
View Installed Package Details
Manage Installed Packages
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=distribution_reimport_package_data.htm&language=en_US
Release
202.14
