### Topic: If a customer is calling in to get the Account executive's information you must open the customer's organization by clicking on the organization's name in the black tab | Click on the "Account ID" | Hoover on the Account owner field.
How do I look-up the account executive's information?
Resolution
These are the steps that you just follow in order to find the account executive's information:
If a customer is calling in to get the Account executive's information you must open the customer's organization by clicking on the organization's name in the black tab
Click on the "Account ID" | Hover on the Account owner field.
If you are not in Blacktab, you can also search for the Account in Global Search
 
