### Topic: Here's what you need to do in order to fix your Calibrations reports
Users may have noticed that the Calibration table returns incorrect results.  A couple of known issues include:
Request State column doesn't show the correct state of the performance summary
Link to the performance summary redirects to the self summary
Changes to a user's rating are not saved
Resolution
To resolve the issue you need to add a filter on the 
Question Set Type
 field to your underlying Calibration reports:
1. Go to the 
Calibration
 tab
2. Select your report from the picklist
3. Click on the 
View/Edit Report
 link
4. Click the 
Customize
 button
5. Add the following filter: 
Question Set Type
 equals 
Manager Summary
 and save the summary
After doing that navigate to the Calibration tab and it should be returning accurate results and behaving as expected.
