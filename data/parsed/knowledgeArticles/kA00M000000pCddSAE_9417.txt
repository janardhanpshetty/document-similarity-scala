### Topic: Learn what to do if Salesforce for Outlook Side Panel is showing <<Unknown Label:loginButton>> and you've been signed out based on Org's security settings.
If you're getting logged out of Salesforce for Outlook (SFO) after a while, or in some cases immediately after you install and configure the wizard, we'll cover what you need to do to fix the issue. 
Resolution
The cause of this is having the option "Immediately expire refresh token." To prevent this from happening on a regular basis, you'll need to change your settings for your Orgs Refresh Token Policy.
 
Update Refresh Token Policy
The steps outlined need to be followed by your organization's admin. 
1. Login to your Salesforce org.
2. Click 
Setup
.
3. Using the 
Quick Find / Search 
search for 
Connected Apps
.
4. Under Manage Apps, click 
Connected Apps
. 
5. On the "Connected Apps" page, locate "Salesforce for Outlook" and click on 
Edit
.
6. Under "Oauth Polices,"
 
click the radio button for 
Immediately expire refresh token 
next to
 "Refresh Token Policy
."
7. Click 
Save
.
Exit and restart the SFO app from the system tray.
Identify the problem
 
You get the error message, "You've been logged out of Salesforce based on your organization's security settings. Click this message to log in again."
 
 
Or, you see <<Unknown Label:loginButton>> message in your Side Panel.
 
 
You can also identify the problem by reviewing the 
AddinTrace
 and 
Trace
 log files and they show the following:
Trace.log
2015-04-24 15:17:11,290 [13152     ] ERROR sfdcsession     LogOn failed with exception - token validity expired
2015-04-24 15:18:55,635 [3604      ] DEBUG general         Application status changed to SalesforceNotConnected, SalesforceCredentialsInvalid
2015-04-24 15:21:06,567 [3364      ] DEBUG general         Not running Synchronize: cannot sync now Status:SalesforceNotConnected, SalesforceCredentialsInvalid
AddinTrace.log
 
2015-04-24 15:18:50,362 [4816      ] DEBUG adxsidepanel    NavigateTo : url= file:///C:/Users/xxxx/AppData/Roaming/salesforce.com/Salesforce for Outlook/sidepanelcontainer.htm%3FloggedOutLabel=<<Unknown Label:loggedOut>>&loggedOutDetailLabel=<<Unknown Label:oauthPolicy>>&buttonLabel=<<Unknown Label:loginButton>>
 
