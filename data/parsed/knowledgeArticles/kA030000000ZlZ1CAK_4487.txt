### Topic: The issue is usually caused when the role hierarchy is saved in the report. The workaround is to create a custom report and ensure that role hierarchy is not selected.
Community users unable to see Activities in custom report type getting an error: “No available columns! You may have lost access to the fields”
Resolution
Please follow the below steps to get the issue fixed
Create report using standard report type which has activities
Save report without selecting a check box which says “Save role Hierarchy"
Note: Community User cannot view the Activity Custom report type and hence the report must be build on standard report type.
