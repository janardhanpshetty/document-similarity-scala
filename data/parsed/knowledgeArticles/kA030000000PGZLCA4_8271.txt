### Topic: This article describes how to find the all package version ID and other package details.
This article describes how to find the all package version ID and other package details.
Resolution
How to find the All Package Version ID and package details
Go to Developer Org BT Link where the package was created. Review the package details – Create > Packages > Click on the package Name
Click on the 
versions 
tab
Click on the 
version 
link and obtain the “
All Package Version ID
” which is at the end of the package install link. It starts with 04t...
Go to NA1 BT (or any BT) and click on the 
All Package Versions 
link
Click on the "Edit" link or "Create New View" and create a filter that sets the  All Package Version ID field equal to the 04t... value. Save the view.
On the page that loads, click on the 
All package version ID
 link (again this is the 04t... value) to see basic details about the package (
if the package is marked for Aloha, Apex Certified etc
).
Click on 
Show Manifest 
to view the XML component manifest of the package.
Click on the "Display Related lists" to see what orgs have installed the package.
OR
6. On the page that loads, click on the 
Edit 
link at the left side of the package information row in the list view to review package components, the LMA it's associated with, its Dev org ID etc.
