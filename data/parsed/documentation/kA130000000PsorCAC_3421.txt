### Topic: Guidelines for Using the Enhanced Page Layout Editor | Salesforce
Guidelines for Using the Enhanced Page Layout Editor | Salesforce
Guidelines for Using the Enhanced Page Layout Editor
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To customize page layouts:
“Customize Application”
To view page layouts:
“View Setup”
Elements that are already on the page layout still appear on the palette but are inactive. When you click an inactive element on the palette, Salesforce highlights the element on the page layout.
Removing a field from a page layout doesn’t remove it from the object’s compact layout. The two layout types are independent.
If the original page layout editor is enabled, users can click the page layout name to access the detail page of the page layout. The enhanced page layout editor doesn’t have detail pages, as all the detail page functionality is always available on the enhanced editor. Salesforce displays a read-only version of the enhanced page layout editor to users with the “View Setup” permission.
Note
The read-only view of the page layout doesn’t display field types and lengths in hover details.
The Custom Links, Custom S-Controls, and Visualforce Pages categories only appear in the palette if you have defined those types of elements for the object for which you are defining a page layout. When you create a custom link for an object, you add it to the Custom Links section on that object’s page layout. In non-English Salesforce organizations, the “Custom Links” section title is not translated from English automatically for the Territory and Territory Model objects, but you can edit the section title.
The Canvas Apps category appears in the palette only if you defined at least one canvas app with a location of Visualforce Page.
The Components category appears in the palette only if the available components are supported by the object for which you are defining a page layout. For example, the Twitter component is supported only on account, contact, and lead page layouts.
When editing a person account page layout:
If you add 
Shipping Address
 next to 
Billing Address
 in the Address Information section, a link displays on the person account edit page that allows you to copy the billing address to the shipping address. Also, an equivalent link appears if you add 
Other Address
 to the Address Information section.
Contact fields and related lists are available on person account page layouts, but contact custom links and custom buttons are not.
This table lists standard objects that have checkboxes that are specific to page layouts for that object. To configure how Salesforce displays the checkboxes, click 
Layout Properties
 when customizing the page layout. Use the 
Select by default
 checkbox associated with a checkbox if you want Salesforce to automatically select the option when a user accesses the edit page.
Object
Checkboxes
Account
Evaluate this account against territory rules on save
 checkbox — Displays the 
Evaluate this account against territory rules on save
 checkbox on account edit pages.
Territory assignment rules run automatically when the 
Select by default
 checkbox is selected.
If both 
Show on edit page
 and 
Select by default
 are selected, users can uncheck the 
Evaluate this account against territory rules on save
 checkbox on the account edit page, and territory assignment rules will not be run.
Case
Case assignment checkbox
 — Displays the 
Assign using active assignment rules
 checkbox on case edit pages.
Case assignment rules run automatically when the 
Select by default
 checkbox is selected.
If both 
Show on edit page
 and 
Select by default
 are selected, the assignment checkbox is selected by default, but users can deselect it to override the assignment rule.
Email notification checkbox
 — Displays the 
Send notification email to contact
 checkbox on case edit pages.
Case Close
Solution information section
 — Displays the solution information section on the case close edit pages.
Notify Contact
 — Displays the 
Notify Contact
 checkbox on case close edit pages.
Lead
Lead assignment checkbox
 — Displays the 
Assign using active assignment rule
 checkbox appears on the lead edit page.
Lead assignment rules run automatically when the 
Select by default
 checkbox is selected.
If both 
Show on edit page
 and 
Select by default
 are selected, the assignment checkbox is selected by default, but users can deselect it to override the assignment rule.
Person Account
Evaluate this account against territory rules on save
 checkbox — Displays the 
Evaluate this account against territory rules on save
 checkbox on person account edit pages.
Territory assignment rules run automatically when the 
Select by default
 checkbox is selected.
If both 
Show on edit page
 and 
Select by default
 are selected, users can uncheck the 
Evaluate this account against territory rules on save
 checkbox on the account edit page, and territory assignment rules will not be run.
Task
Email notification checkbox
 — Displays the 
Send Notification Email
 checkbox appears on the task edit page.
Note
A user’s personal preference for defaulting the state of the checkbox takes precedence over the organization-wide setting.
See Also:
Enhanced Page Layout Editor User Interface Elements
Customize Page Layouts with the Enhanced Page Layout Editor
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_layoutcustomize_pd_notes.htm&language=en_US
Release
202.14
