### Topic: Why did my installation or upgrade fail? | Salesforce
Why did my installation or upgrade fail? | Salesforce
Why did my installation or upgrade fail?
An installation can fail for several reasons:
The package includes custom objects that will cause your organization to exceed its limit of custom objects.
The package includes custom tabs that will cause your organization to exceed its limit of custom tabs.
The developer of the package has uploaded a more recent version of the package and has deprecated the version associated with this installation URL. Contact the publisher of the package to get the most recent installation URL.
You’re trying to install an extension to a package, and you don't have the base package installed.
The package requires that certain components are enabled in your organization, or that required features are enabled in your edition.
The package contains Apex code and you are not authorized to run Apex in your organization. 
The package you’re installing has a failing Apex test.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_distribution_installing_why_did_my_install.htm&language=en_US
Release
202.14
