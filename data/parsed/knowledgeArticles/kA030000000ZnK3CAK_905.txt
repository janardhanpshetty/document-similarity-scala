### Topic: Find AppExchange Listing Information for ISV Partner, Patron Manager.
AppExchange Listing Information:
Partner:
  
Patron Manager
Application Name: 
PatronManager
AppExchange Listing Name / URL:
PatronManager: https://appexchange.salesforce.com/listingDetail?listingId=a0N30000003K6wSEAS
Partner Success Team: 
Joseph Ulep (SAM) / Randy Case (TE) / Lauren Grumet (AE)
PPS Entitlement:  https://org62.my.salesforce.com/a0a30000000tYkk 
 
 
Resolution
Application Overview:
Combined Product Description:
 PatronManager is a full feature CRM that combines sophisticated box office ticketing and subscriptions, fundraising tools, email marketing, and specialized reporting and tracking for all types of ticket-selling organizations.
No More Multiple Databases: One system for ticketing, subscriptions, fundraising, email marketing, and operations.
Personalized & Reliable Support: Our staff provides all set-up implementation, data migration, training and ongoing support.
Affordable: We have several pricing models, the most popular of which is a per-ticket fee model in which your use of our system is paid for by nominal per-ticket fees paid by customers on each transaction.
PatronManager by Patron Technology combines sophisticated box office ticketing and subscriptions, fundraising tools, email and direct marketing, and specialized reporting and tracking for all types of ticket-selling organizations, from arts & non-profits to universities and higher education. We currently empower 500 arts and other non-profits with cutting-edge technology.
Run your box office, manage your subscribers and members, use highly customized fundraising tools, market to your patrons with our integrated PatronMail email system, and use our specialized “CRM Snapshots” to report on ticket sales, donations, and contacts with your needs in mind.
PatronManager is built on the Force.com platform, which means the days of separate systems or needing to sync your data are over. Everyone works from the same system with data captured in real-time! Enable your staff to build the kind of reports that used to take days or weeks to generate instantly. 
Ultimately, PatronManager helps you sell more tickets, raise more money, and operate a lot more efficiently.
 
Distribution Details:
 
Target Market Segment: 
SMB (Small/Medium Business); MM (Mid-market); GB (Large business); ENTR (Enterprise -- 3,500+ employees)
Sold to: Net New Customers
Distribution Mechanism: AppExchange; Trialforce
Target User Demographic:
 
Box Office Manager - Manages all configuration and data around ticket sales.
Onsite Admin - Manages the technical aspects of the application such as configuring ticket printer, etc.
Box office rep - Sells tickets
Marketing Manager - Manages and maintains marketing data in the app
Customer's clients - Allows customer's clients to purchase, donate and manage their season tickets (portal)
 
Application Technology Overview:
Marketecture Diagram:
 
 
Solution Components:
Standard Objects Referenced: Accounts, Contacts, Campaigns, Cases, Opportunities
Number of Custom Objects: 51-75
Number of Reports: 11-20
Number of Classes: 100+
Number of Triggers: 31-40
Number of Visualforce Pages: 100+
ISVforce technology leveraged: Managed Package; Push Upgrades; LMA; Trialforce; Branded Login
Is Force.com primary system of record: Yes 
Other platforms used: 
Has managed package: Yes
Has composite Web App: No 
Has native mobile app?: No 
100% native on Force.com Platform: Yes 
Type of Application: Native application with callouts to Email, Credit card processing.
Supported Editions: Enterprise
Approved for install into existing orgs? Yes
Has Extension Packages? No 
Has Composite Web Services?: No 
Has Desktop App: No
 
UI Architecture:
Supported UI Types: Desktop Browser-Based; Mobile Browser-Based
Mobile Devices Supported - iOS; Android
Developer tools used for browser UI: Visualforce/Apex; JS frameworks (JQuery, ExtJS, etc.)
% of app not using SFDC standard look / feel: 60%
 
Integration Methods:
Integration at UI layer: No
UI Integration Methods: N/A
Realtime integration required: Yes (checkbox)
Realtime integration methods: Visualforce UI + Apex Callouts
Avg number of API calls in 24 hour period: Thousands
Batch Integration Required: No 
Batch Integration Frequency: N/A
Avg number of records per batch: N/A
Batch ETL Methods: N/A
 
Large Data Concerns:
More than 20MB of data per user: Yes
Largest data set stored on single object: Thousands
Describe queries run on large data sets: Multi-level Joins
Reporting on large data sets with SFDC tools? No 
Data Volume Concerns? None at this time. Could be more of an issue as the move into commercial market
 
Sample UI Screenshots:
