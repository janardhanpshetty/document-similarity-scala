### Topic: No.
Can I create a public goal with private metrics?
Resolution
Work.com doesn't support public goals with private elements.  Everyone with access to the goal will have access to the associated metrics.
