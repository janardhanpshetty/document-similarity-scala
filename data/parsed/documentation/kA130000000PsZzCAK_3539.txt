### Topic: Considerations for Changing the Account for Contacts | Salesforce
Considerations for Changing the Account for Contacts | Salesforce
Considerations for Changing the Account for Contacts
Consider key points before changing the account that’s associated with a contact.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
Note
If the contact has a parent account, and the organization-wide default is Controlled by Parent, “Edit” access on the account is required to edit the contact.
If any of the following are true, you can change the account that’s associated with a contact.
You have access to the contact, and you’re the owner of the target account or the manager of the owner in the role hierarchy.
You’re the owner of the contact, or you have access to the contact, and you have access to the target account.
You’re an admin.
If you don’t meet any of these conditions, contact your admin to change the associated account.
If you change the 
Account Name
 field:
Cases and opportunities that are associated with the contact remain associated with the previous account and don’t roll up to the new account.
New cases and opportunities that are created for the contact are associated with the new account.
For contacts that are enabled for portals, 
review these considerations
.
See Also:
Contacts
Share Contacts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=contacts_details.htm&language=en_US
Release
202.14
