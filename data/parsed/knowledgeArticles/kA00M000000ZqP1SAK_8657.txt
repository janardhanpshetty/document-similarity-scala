### Topic: Workflow rules are executed when the ownership of a Case record is changed from a Salesforce license User to a Portal/Community User.
Workflow rules are being evaluated when the ownership of a Case is changed from a Salesforce license User to a Community or Portal User.
Resolution
Both Contact and Account Name are 
Foreign Key
 fields on the Case object. When the 
Foreign Key 
value is changed, all the Workflow rules on that object are evaluated.
In this scenario, when the ownership of a Case changes from a Salesforce license User to a Community or Portal User, the Contact on that Case record is updated with the Contact of the Community or the Portal User and hence Workflow Rules will be evaluated.
