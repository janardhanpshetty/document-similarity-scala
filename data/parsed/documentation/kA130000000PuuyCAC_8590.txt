### Topic: Record Icons in Data.com Search Results | Salesforce
Record Icons in Data.com Search Results | Salesforce
Record Icons in Data.com Search Results
When you search 
Data.com
 for accounts and contacts, you might see one or more icons next to the records in your search results.
Available in: 
Salesforce Classic
Available with a 
Data.com Prospector
 license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Icon
Definition
Considerations
Already in Salesforce
The record is already in 
Salesforce
. Hover over the icon, then click the link to go to the 
Salesforce
 record. Either of these conditions represents a record that’s already in 
Salesforce
.
The record was added to 
Salesforce
 from the 
Data.com
 tab. In the process, it was purchased from 
Data.com
 and affects the addition limit.
The record was manually created in 
Salesforce
 and later matched to a 
Data.com
 record. The record was not purchased from 
Data.com
.
If the record was already added, you can export it without it affecting your addition limit.
If a record was already added, you can’t add it again unless your organization allows duplicates. Adding a duplicate record doesn’t affect your addition limit.
If the record was added by another user and you don’t have permission to view the record, you won’t be able to link to it from the 
Data.com
 search results. If, however, your organization allows duplicates, you can add the record again so you’ll have access to it. Adding the record again doesn’t affect your addition limit.
Already Purchased
The record was already purchased from 
Data.com
. Either of these conditions represents a record that has been purchased from 
Data.com
.
The record was added to 
Salesforce
 from the 
Data.com
 tab. In the process, it was purchased from 
Data.com
 and affects the addition limit.
The record was exported from 
Data.com
. In the process, it was purchased from 
Data.com
 and affects the addition limit.
If the record was already added, you can export it without it affecting your addition limit.
If the record was already exported, you can add it without it affecting your addition limit.
If a record was already added, you can’t add it again unless your organization allows duplicates. Adding the record again doesn’t affect your addition limit.
Inactive
The 
Data.com
 record corresponding to the 
Salesforce
 record has been reported as inactive by either 
Data.com
 or a user in your organization. A contact record may be inactive if it contains out-of-date or inaccurate information. An account record is inactive if D&B has marked it as out of business.
 Inactive records are hidden by default, but you can use filters to show them.
See Also:
Prospect for Companies Fast Right In Salesforce
Prospect for Leads Fast Right In Salesforce
Prospect for Key Contacts at Your Accounts
Prospect for Contacts Fast Right In Salesforce
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=jigsaw_int_understanding_record_icons.htm&language=en_US
Release
200.20
