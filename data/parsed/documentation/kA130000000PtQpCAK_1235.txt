### Topic: Validate a Change Set | Salesforce
Validate a Change Set | Salesforce
Validate a Change Set
You can validate a change set without deploying changes. Validating a change set allows you to view the success or failure messages you would receive with an actual deploy.
From Setup, enter 
Inbound Change Sets
 in the 
Quick Find
 box, then select 
Inbound Change Sets
.
Click the name of a change set.
Click 
Validate
.
Note
We recommend starting a validation during off-peak usage time and limiting changes to your org while the validation is in progress. The validation process locks the resources that are being deployed. Changes you make to locked resources or items related to those resources while the validation is in progress can result in errors.
After the validation completes, click 
View Results
.
Change sets that have been successfully validated might qualify for a quick deployment. For more information, see 
Quick Deployments
.
See Also:
Viewing Inbound Change Sets
Viewing Change Set Details
Deploy a Change Set
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=changesets_inbound_test_deploy.htm&language=en_US
Release
202.14
