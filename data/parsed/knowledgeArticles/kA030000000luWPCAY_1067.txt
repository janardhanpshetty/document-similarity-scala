### Topic: Salesforce AppExchange partner applications may not work after the Summer '15 release if they have not been updated to address changes we have made regarding Home Page Components.
Support agents, there is a change being made in the Summer '15 release that may break certain ISV/AppExchange partner applications that have not been updated.
With the Summer '15 release, Salesforce will no longer support markup that fails the whitelist filter. Any unsupported content will be removed prior to rendering the page.
This change will break partner applications; they will not work if the partner hasn't updated their application. Two partners so far have applications that are affected by this change, InsideSales and CVM Solutions.
Resolution
If you encounter a customer or partner case related to this issue, please follow these steps: 
 
1) Let the partner know that their application must be updated to fix the problem. 
2) Refer the partner to the alert that is posted on the partner community for more information: 
https://partners.salesforce.com/partnerAlert?id=a033000000C7QYyAAN
 (Partner Community login required) 
3a) If the Partner has premier support (Partner Premier + Account Manager), please look up the Partner Success Account Manager(SAM) on the account record (CST field) and @mention let the SAM and Greg Costanzo from the case. 
Include the name of the partner in the @mention
3b) If the Partner does not have premier support (Partner Basic, Basic), perform these steps
- Look up the account team on the partner account record (Account Owner, ISV AE, Senior ISV AE fields) 
- @mention the account team and Greg Costanzo from the case. Include the name of the partner in the @mention
NOTE: 
ISV Partners with AppExchange applications CANNOT apply for an extension for their applications. If a customer of an ISV partner encounters this problem and their business is at a standstill because of it, please escalate to a regional director or developer support manager. 
For customers (NOT PARTNERS), 
INTERNAL: Summer '15 - Extension Process for HTML Home Page Component Security Fix
