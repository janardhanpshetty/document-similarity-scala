### Topic: Steps in Eclipse to remove an unwanted trigger from production
Please follow these steps in Eclipse to remove an unwanted trigger from production:
Resolution
Using the IDE:
1) Assuming you have a sandbox environment synchronized with your Prod org, you will first want to inactivate the trigger in sandbox. You can refresh your IDE project to consume these changes
2) In the Sandbox org, you will then want to run all tests: Navigate to Setup->Develop->Apex Classes and click "Run All Tests". This will run all of the tests in the sandbox org. This is an important step to validate whether or not the inactive trigger has impacted any asserts within your test methods. Assuming this behavioral change has impacted your tests, you will need to update your asserts to reflect this.
3) Once all tests are passing, you can now deploy these changes to your production environment. You can use the Deploy to Server wizard in the IDE. Simply select the tests that you have modified, as well as the inactive trigger, and deploy these changes to your production environment.
4) Validate the trigger is now inactive in your production environment.
Deleting the trigger (if necessary - an inactive trigger will not fire and therefore can be considered as "soft deleted")
5) Create a new IDE project against your Active production org.
6) Right click on the specified trigger and select delete. Accept the option to delete from server. Since the trigger is inactive - it can now be deleted from the production environment.
