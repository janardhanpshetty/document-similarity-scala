### Topic: Specifying a Post Install Script | Salesforce
Specifying a Post Install Script | Salesforce
Specifying a Post Install Script
Once you have created and tested the post install script, you can specify it in the 
Post Install Script
 lookup field on the Package Detail page. In subsequent patch releases, you can change the contents of the script but not the Apex class.
The class selection is also available via the Metadata API as 
Package.postInstallClass
. This is represented in package.xml as a 
<postInstallClass>foo</postInstallClass>
 element.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=apex_post_install_script_specify.htm&language=en_US
Release
202.14
