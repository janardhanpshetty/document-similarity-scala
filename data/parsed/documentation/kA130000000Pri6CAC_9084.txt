### Topic: Are the territory hierarchy and the role hierarchy different? | Salesforce
Are the territory hierarchy and the role hierarchy different? | Salesforce
Are the territory hierarchy and the role hierarchy different?
Note
This information applies to the original Territory Management feature only, and not to Enterprise Territory Management.
Yes. The territory hierarchy and the role hierarchy are independent. See the following comparison:
Territory Hierarchy
Role Hierarchy
Is available by contacting Salesforce
Is available by default
Determines forecasts
Has no impact on forecasting when territory management is enabled
Supports assigning a user to multiple territories
Supports assigning a user to only one role
Affects account and opportunity reports
Affects all other reports
Grants access to records regardless of ownership. Users receive whatever access is most permissive across both hierarchies.
Grants access to records based on ownership. Users receive whatever access is most permissive across both hierarchies.
See Also:
Territory Management FAQ
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_territories_are_the_territory.htm&language=en_US
Release
202.14
