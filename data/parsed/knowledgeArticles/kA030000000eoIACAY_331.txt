### Topic: Validation Status
Trial Feature Request runs the Salesforce provisioning tool used to convert trials to production orgs. It is allowed to use this provisioning method for partner customer trial orgs. 
Tenants must be attached to accounts before TFR will work, tenants act as the link between the org and the account. Tool is ran from the account, then changes are saved into the org via the tenant.
Chatter group with more information on the tool: 
https://org62.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F930000000k9oW
FAQS: 
https://org62.my.salesforce.com/06930000005UNpk
 
Resolution
1. Create an account (record type: trial). Instructions found here (
https://org62.my.salesforce.com/kA030000000eoHb
). Please make sure to create a trial account, any other account record type will fire off automation not intended for this TFR process.
2. Once you have a tenant linked, click the "New Trial Feature Request" button on the Trial Feature Request related list on the account.
3. On the TFR edit screen, the "Provisioning View" section lists all the items which have already been provisioned to the org and do not need to be selected. In the "Product Selection" section, add in any additional licenses/features/provisionable products by searching for the name and clicking the name. Check the Demystification spreadsheet if you have questions about the product name, as it may be different from the Blacktab or marketing name (
https://docs.google.com/a/salesforce.com/spreadsheets/d/1kVQe4oxIU1SjUdsizz9QnrtsotPu0KapOjqi8q-PFWY/edit?usp=sharing
). Once you add the product, it will show up in the "Provisioning View" section, please edit the "Additional Quantity" field to provision the amount you want.
NOTE: some products come in bundles, read product name carefully.
NOTE: there is no preview page, please double check your work before you submit.
4. Once you are ready, hit the "Save & Submit" button to submit the request for approval. Take the link to the TFR record you created and chatter your manager on the case with a link provided to the TFR.
 
