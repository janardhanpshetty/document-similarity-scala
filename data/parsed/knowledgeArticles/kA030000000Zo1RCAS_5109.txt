### Topic: Learn the limitations for using Attributes in a Data Filter in Marketing Cloud.
​The limit of how many Attributes you can use in a 
Data Filter
 depends on the number of Profile Attributes available in your Salesforce Marketing Cloud account. 
Resolution
Data Filter Limit
There's a limit of 
256
 Rules that can be added to a Data Filter. The more Rules you have in your Data Filter, the more time it takes to create the segment and there's an increased potential for a time out error occurring when running the filter. 
