### Topic: Create List Views for Salesforce Classic Mobile | Salesforce
Create List Views for Salesforce Classic Mobile | Salesforce
Create List Views for Salesforce Classic Mobile
Salesforce Classic Mobile setup available in: both Salesforce Classic and Lightning Experience
Mobile app available in: 
Performance
, 
Unlimited
, and 
Developer
 Editions, and for an extra cost in: 
Professional
 and 
Enterprise
 Editions
User Permissions Needed
To view Salesforce Classic Mobile devices and users:
“View Setup and Configuration”
To manage Salesforce Classic Mobile custom views:
“Manage Mobile Configurations”
You can create custom list views for Salesforce Classic Mobile users. Custom list views for Salesforce Classic Mobile, also called mobile views, are different from Salesforce custom views in these ways:
Administrators set up mobile views for each mobile configuration. The views are available to all users assigned to the configuration, and administrators can’t restrict visibility to certain groups of users within the configuration. Each mobilized object in a mobile configuration can have up to 10 custom views.
Users can’t filter mobile views by All Records or My Records. The views apply to all records stored locally on the device regardless of ownership; however, ownership filters can be applied using the additional fields in the search criteria.
Mobile views don't support filter logic.
Mobile views are limited to a two-column display.
Users can sort mobile views in ascending or descending order by up to two fields.
For each mobile configuration, you can define up to 10 custom views per object. These views are then pushed to the devices of users assigned to the affected configurations. To create a custom view for Salesforce Classic Mobile:
From Setup, enter 
Salesforce Classic Configurations
 in the 
Quick Find
 box, then select 
Salesforce Classic Configurations
. Then click the name of a mobile configuration. You might need to 
create a mobile configuration
 if you haven't already.
Scroll down to the Mobile Views related list.
Choose an object type from the Select an object drop-down list, and then click 
New Mobile View
. Only objects included in the mobile configuration's data set appear in the drop-down list. You can’t create mobile views for the user object.
Enter the view name.
Because display space on mobile devices is limited, the maximum length of a mobile view name is 30 characters.
In the Specify Filter Criteria section, enter conditions that the selected items must match; for example, 
Amount is greater than $100,000
.
Choose a field from the first drop-down list.
Note
You can’t create views based on fields you 
excluded from mobile page layouts
 or fields that are 
hidden for all profiles and permission sets
.
Choose a filter operator.
In the third field, enter the value to match.
Warning
Note the following about filter criteria values for mobile views:
You can use the 
$User.ID
 merge field
 as a value in your filter criteria to reference the current user. You can't enter user names in your filter criteria.
You can only enter special date values in your filter criteria, not actual dates.
You can't use FISCAL special date values in the filter criteria.
Select 
Match All
 if items in the mobile view should match all the criteria you entered. Select 
Match Any
 if items in the mobile view should match any of the criteria you entered. Mobile custom views do not support advanced filtering options.
In the Select Fields to Display section, select the fields to use as display columns.
The default fields are automatically selected. You can choose up to two different columns of data fields to display in your mobile custom view.
In the Define Sort Order section, optionally set a primary and secondary sort order for the view.
Select a field in the Order By drop-down list. You can sort by fields that have been excluded from the object's mobile page layout.
Set the sort order to Ascending or Descending.
Click 
Save
.
See Also:
Manage Salesforce Classic Mobile Views
Manage Salesforce Classic Mobile Configurations
Manage Salesforce Classic Mobile Devices
Setting Up Salesforce Classic Mobile
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=creating_mobile_views.htm&language=en_US
Release
202.14
