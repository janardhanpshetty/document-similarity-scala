### Topic: Learn how you can display a logo on the Homepage layout, located on the left-hand side of the page.
Learn how System Administrators can add a logo to the Homepage Page Layout.
Resolution
Upload your Image
As a System Administrator you can display a logo on your sidebar, located on the left-hand side of most pages. To add a logo to your sidebar, be sure to have the image saved to the Documents tab (read how to 
upload and replace documents
), making sure to check the box to make the image "externally available" (for portal users) and that the image is in a folder that is accessible to internal users.
 
Create a New Image Custom Component
 
1. Go to 
Setup
 | 
Build
 | 
Customize
 | 
Home
 | 
Home Page Components
.
2. Click 
New.
3. If a screen with text shows, click 
Next.
4. Name the component.  For the purposes of this article, we'll call it "Company Logo."  
5. Select "Image" from the Radio boxes, and then click 
Next.
6. Click "Insert an Image," and be sure to select the folder selected in the previous section ("Upload your Image").
7. Select the image you uploaded.  It must be under 100 kilobytes (KB). 
8. 
If necessary, click on the image and resize it using the selection box so that it's no wider than the indicator below the image box, and then click 
Save.
Create a new Home Page Layout and add the component
 
1. Go to 
Setup
 | 
Build
 | 
Customize
 | 
Home
 | 
Home Page Layouts 
(It's directly underneath Home Page Components).
2. Click 
New. 
3.
 
Name your new Homepage.  Optionally, if you have an existing Homepage layout from which you'd like to base this one, you can select that now.
4. Click 
Save.
5. Select the individual components you'd like to add to the layout.  Be sure to include your new Component (we called it "Company Logo") created in the previous section.  You'll find it in the "Select Narrow Components to Show" section.
6. Click 
Next.
7. Select the order of the Left and Right columns as desired, and then click 
Save & Assign.
8. For each profile which you'd like to see the new Homepage with the new "Company Logo", change the picklist next to that profile to the name of your new Homepage.  
9. Click 
Save.
