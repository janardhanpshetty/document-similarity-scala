### Topic: Share a Report | Salesforce
Share a Report | Salesforce
Share a Report
Share your reports with colleagues so everyone’s working with the same info.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
Note
As you work with folders, keep these Lightning Experience limitations in mind.
Features Not Available in Lightning Experience
Feature
Notes about Lightning Experience Availability
Standard Folders, containing default reports and dashboards that come packaged with Salesforce
Move Reports and Dashboards between Folders
Create and Delete Report and Dashboard Folders
Folders created in Salesforce Classic and their contents are available in Lightning Experience. Users can save inside these folders when creating or cloning a report or dashboard.
Share Report and Dashboard Folders
Sharing permissions set on folders in Salesforce Classic are obeyed in Lightning Experience, but sharing permissions can’t be set nor changed.
Search for Report and Dashboard Folders
Users can’t search for report and dashboard folders in Lightning Experience. Global search results include reports and dashboards, but not report and dashboard folders.
Share Reports and Dashboards
Reports and dashboards are shared through folders. You share the folder, not the report or dashboard itself. To let others work with your report or dashboard, give them Viewer, Editor or Manager access to the folder where the report or dashboard is stored.
Deliver Your Report
To get the information in your report to the people who need it, you can share the report’s URL, make the report available for Chatter feeds, or export the data to another tool, such as Excel. You can also set the report to run on a schedule so that viewers always have the latest information.
Overview of Embedded Report Charts
Typically, users have had to navigate to the Reports tab to find data. But you can give them valuable information directly on the pages they visit often. To do that, just embed report charts in detail pages for standard or custom objects. When users see charts on pages, they are empowered to make decisions based on data they see in the context of the page without going elsewhere to look for it. For example, an opportunity record shows important data directly on its detail page.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=rd_reports_share.htm&language=en_US
Release
202.14
