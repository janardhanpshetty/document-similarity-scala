### Topic: When you send an email from Salesforce it creates a new record for the Task and the email object both and is taking extra storage. Is there a way to turn off an auto activity creation
When you send an email from Salesforce it creates a new record for the Task and the email object both and is taking extra storage. Is there a way to turn off an auto activity creation?
Resolution
This is currently working as designed, for the orgs that have 
Email-to-Case or 
On-Demand Email-to-Case enabled the "send an email" option creates both activity and the email record
. Please see the related idea below where the product manager has confirmed the same as well.
https://success.salesforce.com/ideaView?id=08730000000Bq4cAAC
