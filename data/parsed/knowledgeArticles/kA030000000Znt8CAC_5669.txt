### Topic: Learn how a sandbox refresh can affect rules in Social Hub and your social posts and cases.
 
When you refresh your Salesforce sandbox organization
, the sandbox organization gets a new organization ID. If your rules in Social Hub still reference the previous organization ID, your cases and social posts won't be sent to Salesforce until the rules are updated with the new organization ID.
Resolution
 
Fix your Social Hub rules and send posts in your sandbox organization again
 
 1. 
If you are using a Social Customer Service connection, re-authenticate your Radian6 account in your Salesforce sandbox organization.
 
 1. Click 
Setup
.
 2. Select 
Build
 | 
Customize
 | 
Social Apps Integration
 | 
Social Customer Management
.
 3. Click the 
Settings
 tab.
 4. Under "Radian6 credentials", enter your Radian6 username and password, then log in.
 
 2. 
Update your Social Hub rules to send posts to the new organization ID.
 
 1. 
Log in to Radian6 Social Hub
.
 2. Click 
Connections
.
 3. Click 
Salesforce.com
. Under "Social Customer Service Orgs", your new Sandbox organization and its ID appear.
 4. Click the 
Rules
 tab.
 5. Click the 
pencil icon
 next to any rules with a "Send to Salesforce" action that send posts to your previous organization ID.
 6. Under the "Send to Salesforce" action in your rule configuration, select the new sandbox organization ID.
 7. Click 
Update Rule
 to save your new rule configuration.
 
 3. 
If necessary, 
re-run your rules to process the posts
 that were missed after you refreshed your Sandbox organization.
