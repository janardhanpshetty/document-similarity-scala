### Topic: Trying to change a custom Lookup field to a Master-detail relationship results in an "Unable to Access Page" error and does not complete.
When trying to change a custom Lookup field to a Master-detail relationship, Users are getting an error: 
Unable to Access Page 
The value of a parameter contains a character that is not allowed or the value exceeds the maximum allowed length. Remove the character from the parameter value or reduce the value length and resubmit. If the error still persists, report it to our Customer Support team. Provide the URL of the page you were requesting as well as any other related information. 
The end URL will look something like this: 
ex/ex.jsp?handledEx=common.config.field.BlockedSchemaChangeException 
Trying to create a brand new Master-detail relationship also fails. 
 
Resolution
Escalate the case to Tier 3 Config following CM101. 
Tier 3 steps: 
A Scrutiny should fix this. 
Go to Org Instance and click Scrutiny. 
Enter Org ID. 
Enter the first 3 characters of the object the field resides on in the "Entity (For CF and CI)" field.
Run Scrutiny 
ScrutinyMasterDetailHardDeleteCleanup 
(under CustomFK branch). 
If it finds any results, commit the Scrutiny and once completed, have the customer confirm they can now change the Lookup relationship to a Master-detail. 
 
