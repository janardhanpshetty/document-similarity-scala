### Topic: Actions, Buttons, and Links | Salesforce
Actions, Buttons, and Links | Salesforce
Actions, Buttons, and Links
Buttons and links let users interact with Salesforce data and with external websites and services, such as search engines and online maps. Salesforce includes several standard buttons and links. You can also create custom ones. 
Actions let users do tasks, such as create records in the Chatter publisher and in Salesforce1.
Available in: both Salesforce Classic and Lightning Experience
Available in: All Editions
User Permissions Needed
To create or edit buttons, links, and actions:
“Customize Application”
Actions
Actions add functionality to Salesforce. Choose from standard actions, such as create and update actions, or create actions based on your company’s needs.
Custom Buttons and Links
Custom buttons and links help you integrate Salesforce data with external URLs, applications, your company’s intranet, or other back-end office systems.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=working_with_buttons_links_actions.htm&language=en_US
Release
202.14
