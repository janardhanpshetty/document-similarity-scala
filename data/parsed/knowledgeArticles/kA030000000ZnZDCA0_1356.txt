### Topic: What should be done with the emails received from Adobe (no-reply@omniture.com) with a subject similar to "Omniture Partner Integration - ... issue."?
What should be done with the emails received from Adobe (
no-reply@omniture.com
) with a subject similar to "Omniture Partner Integration - ... issue."?
Resolution
These emails indicate that an ExactTarget customer, who is using the Omniture Genesis integration, is encountering an issue where their Tracking data is failing to be exported to Omniture. The troubleshooting steps for these cases are as follows:
1) Examine the email received from Adobe for the following:
A) The name of the file which could not be processed
B) The folder name where the file is located (Adobe will begin providing this Q1 2012)
2) Attempt to locate the MID in the provided filename, see "File Name Solution" below.
3) If the filename does not have an MID, assign the case to Tier 2. Please create a case comment with the following notes, and select solution # 4617 for the case.
Tier 2,
A customer's Omniture integration is failing to generate a correct file name. Please use solution # 4617 to locate the account creating problematic files.
File Naming Pattern: [paste problematic file name from Adobe's email here]
Folder Name / Suite Name: [put folder name here if available, otherwise "no folder name available"]
---File Name Solution---
The number at the end of the filename, see screenshot available in the application help link below, may or may not be an ExactTarget MID. Search for the MID on the proper stack (7 digits = S4). If no account is found, move to step #4 above. If an account is found, impersonate into the account.
In that account, check File Locations under the Admin tab. There will be a file location, usually something like "Omniture Metrics" or "Omniture Campaign", pointing to an External FTP Site with an URL similar to either of the following. If this is not found, move to step #4 above.
ftp://ftp.omniture.com/
[some folder name]/metrics
ftp://ftp.omniture.com/
[some folder name]/campaign
Record the value of the [some folder name] section. This is the Omniture Suite Name. Contact the customer to resolve this issue. The customer should be able to provide the Integration Number. The Suite Name and Integration Number are required for the File Naming Pattern. Assist the customer with ensuring that their Campaign and Metrics Data Extract and File Transfer Activities are using a File Naming Pattern value similar to the following (see Guide/Reference Link for further detail):
%%Year%%-%%Month%%-%%Day%%_<SuiteName>_Metrics_<INTEGRATION NUMBER>.tab
%%Year%%-%%Month%%-%%Day%%_<SuiteName>_Campaign_<INTEGRATION NUMBER>.tab
Help Link
http://image.exct.net/lib/fe7115707566057e7117/m/1/Omniture+Partner+Integration+Error+Email+Sample.jpg
