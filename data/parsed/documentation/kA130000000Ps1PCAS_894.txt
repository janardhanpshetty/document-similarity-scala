### Topic: Reporting on Force.com Sites | Salesforce
Reporting on Force.com Sites | Salesforce
Reporting on Force.com Sites
Available in: Salesforce Classic
Available in: 
Developer
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To install packages:
“Download AppExchange Packages”
To run reports:
“Run Reports”
AND
“Read” on the records included in reports
To create, edit, save, and delete reports:
“Run Reports” and “Read” on the records included in the reports
AND
“Create and Customize Reports”
To create, edit, and delete dashboards:
“Run Reports”
AND
“Manage Dashboards”
To keep track of your site activity and usage, take advantage of the Sites Usage Reporting managed package to analyze your monthly page views, daily bandwidth, and daily service request time so you can avoid reaching 
monthly and daily limits
 for individual sites, as well as for your organization.
To get started using Salesforce Reports and Dashboards for sites:
Install the Sites Usage Reporting managed package.
Use packaged reports to analyze site usage.
Optionally, 
create custom reports to analyze site usage.
Use the Site Usage Dashboard to monitor sites.
Installing the Sites Usage Reporting Managed Package
The Sites Usage Reporting managed package, available on AppExchange, contains out-of-the-box reports and a dashboard for monitoring sites usage.
To find the Sites Usage Reporting managed package, go to AppExchange and search on “sites reporting,” or go to 
http://sites.force.com/appexchange/listingDetail?listingId=a0N30000001SUEwEAO
.
Using Packaged Reports to Analyze Site Usage
The Sites Usage Reporting managed package contains the following reports for the sites in your organization. You can find these reports in the Site Usage Reports folder under All Reports in the Reports tab. You can also select 
Site Usage Reports
 in the 
Folder
 drop-down list, then click 
Go
.
Note
Site usage data is aggregated at midnight, GMT, so the current day's page view counts may not be accurately reflected in reports, depending on your time zone. Cache server page views may take a day or more to be reflected in reports.
Report
Description
Current Period Page Views
Shows the total page views for the current period (calendar month), measured against page views allowed. Page views are broken down by site and by day. The current period limit applies to all sites within the organization.
Daily Total Bandwidth Usage
Shows the total bandwidth usage over the last 30 days, broken down by site, by day, and by origin and cache servers.
Daily Total Page Views
Shows the total page views over the last 30 days, broken down site, by day, and by origin and cache servers.
Site Daily Origin Bandwidth Usage
Shows the total origin bandwidth usage over the last 30 days, broken down by site and by day.
Site Daily Request Time Usage
Shows the total origin service request time over the last 30 days, broken down by site and by day.
Top Bandwidth Consuming Sites
Shows the sites that consumed the most bandwidth during the current period.
Top Resource Consuming Sites
Shows the sites that consumed the most service request time during the current period.
Top Sites by Page Views
Shows the sites that generated the most page views during the current period.
Creating Custom Reports to Analyze Site Usage
You can also create custom reports on sites:
From the Reports tab, click 
New Report
.
For the report type, select 
Administrative Reports
, then 
Site Usage Reports
. You must enable sites for your organization and install the Sites Usage Reporting managed package to see the Site Usage Reports custom report type.
Click 
Create
 to create a custom report. Fields related to your sites, such as 
Site Name
, 
Site Status
, 
Daily Bandwidth Limit
, and 
Daily Request Time Limit
 can all be used in your custom report.
Note
When you create your own custom reports using the Site Usage Reports custom report type, be aware that the 
Origin Bandwidth
 column is measured in bytes, and the 
Request Time
 column is measured in milliseconds. Make sure you consider the difference in units when comparing these columns to the 
Daily Bandwidth Limit
 and 
Daily Request Time Limit
 columns, which are measured in megabytes and minutes, respectively.
For the out-of-the-box reports included with the managed package, bandwidth is measured in megabytes and request time is measured in minutes.
Using the Site Usage Dashboard to Monitor Sites
The Sites Usage Reporting managed package contains the Site Usage Dashboard to help you monitor the sites in your organization at a glance. The dashboard contains a component for each of the reports provided in the managed package.
To access the dashboard, from the Dashboards tab:
Use the 
View Dashboard
 field.
Or, click 
Go to Dashboard List
 and select 
Site Usage Dashboard
 from the dashboard list.
To modify the dashboard, click 
Edit
. You can also create your own custom dashboard using any custom reports you may have created. Consider adding the Site Usage Dashboard as the dashboard snapshot on your home page.
See Also:
Viewing 24-Hour Force.com Sites Usage History
Tracking Your Force.com Site with Google Analytics
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sites_analytics.htm&language=en_US
Release
202.14
