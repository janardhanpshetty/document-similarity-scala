### Topic: Show and Hide the Record Count for a Block | Salesforce
Show and Hide the Record Count for a Block | Salesforce
Show and Hide the Record Count for a Block
You can choose to show or hide the number of records, or 
record count
, for each block in a joined report. By default, record count is displayed for each block in the report builder and on the run reports page.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, and delete reports:
“Create and Customize Reports”
AND
“Report Builder”
You can hide or show record count two ways.
Click the arrow in the block header ( 
 ) to display the block menu. The check mark beside the 
Record Count
 menu item shows that record count is enabled for the block. Click 
Record Count
 to toggle between showing and hiding the count.
When you’ve hidden report details, position the cursor over the Record Count column to display an arrow ( 
 ). Click the arrow, and select 
Remove Column
. To display record count again, click the arrow in the block header and select 
Record Count
.
Note
If you haven’t summarized any rows in your report blocks and have also hidden both details for the report and row counts for all blocks, your blocks will be hidden on the run reports page. To display the blocks, choose 
Show Details
 from the run reports page or the report builder.
Parent topic:
 
Work with Blocks
Previous topic:
 
Rename a Block
Next topic:
 
Delete a Block
See Also:
Show and Hide Report Details
Use a Summary Function in a Custom Summary Formula
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_blocks_row_count.htm&language=en_US
Release
202.14
