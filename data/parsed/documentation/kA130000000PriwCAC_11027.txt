### Topic: Who Can See My Attached Files and Links? | Salesforce
Who Can See My Attached Files and Links? | Salesforce
Who Can See My Attached Files and Links?
Share files and links with people by attaching them to posts or comments.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Attach links and files to posts and comments from your device, Salesforce Files, Files Connect external sources, and 
Salesforce CRM Content libraries.
. All file types are supported, including everything from Microsoft® PowerPoint presentations and Excel spreadsheets, to Adobe® PDFs, image files, audio files, and video files.
This table describes who can see a file you attached and where it appears:
If You Attach a File:
Who Can See It?
Where Does it Appear?
On a Chatter feed
All Chatter users in your company
In your and your followers' Chatter feeds, and profile
On the Files list 
and Salesforce CRM Content 
of all Chatter users in your company
On your Chatter profile or on someone else's Chatter profile
All Chatter users in your company
In your feed, their feed, your followers' feeds, and their followers' feeds and profiles
In the Files Owned list on your profile
On the Files list 
and Salesforce CRM Content 
of all Chatter users in your company
On a public group
All Chatter users in your company
In your feed, your followers' feeds, and the group members' feeds
In your feed on your profile
In the feed on the group
In the Group Files list on the group
On the Files page 
and Salesforce CRM Content 
of all Chatter users in your company.
On a private group that you're a member of
Members of the group
 and users with “Modify All Data” and “View All Data” permissions
In your feed and the group members' feeds
In your feed on your profile
In the feed on the group
In the Group Files list on the group
On your Files lists, group members' Files lists,
 and Salesforce CRM Content
On an unlisted group that you're a member of
Members of the group and users with the “Manage Unlisted Groups” permission
In your feed and the group members' feeds
In your feed on your profile
In the feed on the group
In the Group Files list on the group
On your Files lists, group members' Files lists,
 and Salesforce CRM Content
On a record
Anyone with sharing rights to the record
Record detail page
Your profile page
Feed search results
All Company feed
See Also:
Post Visibility
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_attachments.htm&language=en_US
Release
202.14
