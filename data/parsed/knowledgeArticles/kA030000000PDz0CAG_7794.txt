### Topic: To list on the AppExchange as a paid application, there is a fee associated with the security review.
 
What fees are involved with publishing Applications on the AppExchange?
Resolution
There is a Security Review fee involved for publishing Commercial Apps on AppExchange. Commercial Apps are primarily the applications that require fee per user. Non-commercial Apps are the ones that are provided for free. There is a one-time upfront fee, and a small subsequent annual fee for this process. The initial Security Review fee is $2,700 USD for each paid app submitted (no fee for free apps). This includes the annual listing fee of $150 USD for the first year. 
For more details, please visit: 
http://wiki.developerforce.com/index.php/Security_Review
 and 
p
.force.com/security
 
