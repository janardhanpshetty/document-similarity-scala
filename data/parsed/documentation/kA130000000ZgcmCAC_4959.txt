### Topic: Configure the Dataflow | Salesforce
Configure the Dataflow | Salesforce
Configure the Dataflow
Configure the dataflow based on your dataflow design. You can configure the dataflow to extract data, transform datasets based on your business requirements, and register datasets that you want to make available for queries. To configure the dataflow, add transformations to the dataflow definition file.
Available in Salesforce Classic and Lightning Experience.
Available for an extra cost in 
Enterprise
, 
Performance
, and 
Unlimited
 Editions. Also available in 
Developer
 Edition
.
User Permissions Needed
To edit the dataflow definition file:
“Edit Wave Analytics Dataflows”
A 
dataflow definition file
 is a JSON file that contains transformations that represent the dataflow logic. The dataflow definition file must be saved with UTF-8 encoding.
Before you can configure a dataflow to process external data, you must 
upload the external data to Wave Analytics
.
In Wave Analytics, click the gear icon (
) and then click 
Data Monitor
 to open the data monitor. 
The Jobs view of the data monitor appears by default.
Select 
Dataflow View
.
To download the dataflow definition file, click 
Download
 in the actions list (1).
Make a backup copy of the dataflow definition file before you modify it. 
Wave Analytics doesn’t retain previous versions of the file. If you make a mistake, you can upload the previous version to roll back your changes.
Add transformations to the dataflow definition file.
For example, based on the design in the 
previous step
, you can add the following transformations:
{
   "
Extract_Opportunities
": {
      "action": "sfdcDigest",
      "parameters": {
         "object": "Opportunity",
         "fields": [
            { "name": "Id" },
            { "name": "Name" },
            { "name": "Amount" },
            { "name": "StageName" },
            { "name": "CloseDate" },
            { "name": "AccountId" },
            { "name": "OwnerId" }
         ]
      }
   },
   "
Extract_AccountDetails
": {
      "action": "sfdcDigest",
      "parameters": {
         "object": "Account",
         "fields": [
            { "name": "Id" },
            { "name": "Name" }
         ]
      }
   },
   "
Transform_Augment_OpportunitiesWithAccountDetails
": {
      "action": "augment",
      "parameters": {
         "left": "Extract_Opportunities",
         "left_key": [ "AccountId" ],
         "relationship": "OpptyAcct",
         "right": "Extract_AccountDetails",
         "right_key": [ "Id" ],
         "right_select": [
            "Name"
         ]
      }
   },
   "
Transform_Filter_Opportunities
": {
      "action": "filter",
      "parameters": {
         "filter": "StageName:EQ:Closed Won",
         "source": "Transform_Augment_OpportunitiesWithAccountDetails"
      }
   },
   "
Register_Dataset_WonOpportunities
": {
      "action": "sfdcRegister",
      "parameters": {
         "alias": "WonOpportunities",
         "name": "WonOpportunities",
         "source": "Transform_Filter_Opportunities" 
      }
   }
}
Note
The JSON keys and values are case-sensitive. Each bolded key in the previous example JSON contains one action, which identifies the transformation type. The order in which you add the transformations to the dataflow definition file doesn’t matter. Wave Analytics determines the order in which to process the transformations by traversing the dataflow to determine the dependencies among them.
Before you save the dataflow definition file, use a JSON validation tool to verify that the JSON is valid.
An error occurs if you try to upload the dataflow definition file with invalid JSON. You can find JSON validation tool on the internet.
Save the dataflow definition file with UTF-8 encoding, and then close the file.
In the Dataflow view of the data monitor, click 
Upload
 from the action list (1) to upload the updated dataflow definition file. 
Note
Uploading the dataflow definition file does not affect any running dataflow jobs and does not automatically start the dataflow job.
You can now start the dataflow on demand or wait for it to run on the schedule. Users cannot query the registered datasets until the dataflow runs.
Parent topic:
 
Create Datasets with a Dataflow
Previous topic:
 
Design the Dataflow
Next topic:
 
Start and Stop a Dataflow
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_integrate_workflow_configure.htm&language=en_US
Release
202.14
