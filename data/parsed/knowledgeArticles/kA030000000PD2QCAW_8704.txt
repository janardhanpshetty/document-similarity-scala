### Topic: This article would help you get the Organization Name via APEX.
You want to get the name of your organization via apex.
Resolution
You can get the organization name either directly from the DB:
Organization org = [SELECT name FROM Organization limit 1];
System.debug(' org name: ' + org.name);
Or through the UserInfo class:
String orgName = UserInfo.getOrganizationName();
