### Topic: Find out the difference between the Estimate your organization's code coverage link vs Run All Tests.
You may find that the "Estimate your organization's code coverage" link under Setup > Develop > Apex Classes gives a different result than Run All Tests. This article explains why.
 
Resolution
The "Estimate your organization's code coverage" link doesn't run actual tests. If you hover over the link to view the contextual help, it specifies the result can differ from Run All Tests.
There are several reasons this calculation wouldn’t be the same as the actual result, including managed packages, dynamic code, and custom settings. In any event, this number isn't always a precise reflection of the running of all tests, though it will usually be fairly close. The number you should be looking at is the resulting percentage when you 
select Run All Tests
.
