### Topic: Refer to this article to get Mobile SDK Support Email Templates
Below mentioned are the Support Email Templates you can use for Mobile SDK:
Resolution
All cases will need to be closed out immediately:
Hi Customer, 
Since the issue is related to Mobile SDK we ask you that you please, post on the below forum here:
https://plus.google.com/communities/114225252149514546445
   
Here is an article explaining our Mobile SDK Support:
https://help.salesforce.com/apex/HTViewSolution?urlname=Salesforce-Mobile-SDK-Support&language=en_US
The reason we ask is because our Mobile SDK is an open source product and the community listed above is the best place to engage SDK resources. This community is made up of customers as well as our SDK engineering and product management team.  We will close this case as it is outside of our scope of support.
Regards,  
Your Name
