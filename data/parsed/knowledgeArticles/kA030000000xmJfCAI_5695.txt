### Topic: You can access Twitter and Facebook social accounts in Radian6 Mobile managed account Stacks.
Both Twitter managed accounts and Facebook managed accounts can be used in Radian6 Mobile. Your managed accounts 
must be registered 
through 
Social Account Manager 
 or in 
Social Studio
 to use them in Radian6 Mobile. 
Resolution
 
Build a Managed Account Stack
 
1. Tap 
Add a Stack 
from the main menu.
2. Tap either 
Twitter Managed Account 
or 
Facebook Managed Account. 
3. Under "Associated Managed Accounts," tap 
+Add Managed Account 
 to add any registered social accounts you can access to your Stack.
-You can add social accounts that match the type of Stack you create. If you create a Twitter Managed Account Stack, you can add your Twitter social accounts to it, or you can add your registered Facebook social accounts to a Facebook Managed Account Stack.
4. Tap 
Save
 to view the Stack.
For information on Engage Columns in Social Studio Mobile, please see 
Social Studio Mobile
.
