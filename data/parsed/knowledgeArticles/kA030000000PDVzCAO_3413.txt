### Topic: When an account is transferred from one user to another, the following rules will apply...
What happens to sharing when I transfer an Account?
Resolution
When an account is transferred from one user to another, the following rules will apply:
-All manual sharing of the account is deleted, and default sharing is applied.
-Open opportunities owned by the existing owner will be transferred to the new owner.
-You will have the option to transfer closed opportunities owned by the existing account owner. Closed opportunities owned by other users will not be transferred.
-You will have the option to transfer open opportunities owned by users other than the existing account owner.
-Private opportunities will not be transferred. If the owner of the private opportunity can still see the account after transfer, it will still be linked to the account. If the owner of the private opportunity cannot see the account after the transfer, the private opportunity will be disassociated from the account.
-You have the option to transfer closed and open cases as well.
