### Topic: When do account assignment rules evaluate accounts? | Salesforce
When do account assignment rules evaluate accounts? | Salesforce
When do account assignment rules evaluate accounts?
Note
This information applies to the original Territory Management feature only, and not to Enterprise Territory Management.
Active account assignment rules automatically evaluate accounts and assign them to territories when:
An account is created using the Salesforce user interface, the Force.com API, or a client such as Connect Offline.
An account is imported using an import wizard.
An account is created by the conversion of a lead.
An account is edited and saved if the 
Select by default
 checkbox is selected for the 
“Evaluate this account against territory rules on save” checkbox
 option under 
Layout Properties
. If the account assignment rule initiates an opportunity ownership change, you must have transfer access on the opportunity.
An account is edited and saved via the Force.com API.
Run Rules
 is clicked on a territory detail page, provided the 
Exclude from territory assignment rules
 checkbox on the account is deselected.
Save & Run Rules
 is clicked on the manage account assignment rules page for a territory, provided the 
Exclude from territory assignment rules
 checkbox on the account is deselected.
Duplicate accounts are merged.
See Also:
Territory Management FAQ
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_territories_when_do_account.htm&language=en_US
Release
202.14
