### Topic: Contains a summary of Platinum Support teams, clients, and assigned resources.
This is an overview of Platinum Support, the various clients, and the resources named for those clients.
Resolution
If a Mission Critical authorized contact does contact standard global support, they should be treated as any other account and follow the standard global support process. Those cases do not fall within Mission Critical Support response level commitments. 
Many times, MCS clients will be larger enterprise accounts. Given this dynamic, 
there will be resources who are not authorized to use MCS resources. When this occurs, cases should follow the same global support process as a standard client. 
When a call comes in for a MCS account on an open case during US business hours where a MCS resource is the owner, the process below can be used: 
 
DO NOT GIVE CELL PHONE NUMBERS TO CLIENTS
1.    Add all relevant comments to the case regarding the current call including new information and contact details (or just that the contact details were confirmed) 
2.    Attempt to connect with the case owner, should use the same process as escalating to advanced support where the call is presented first before transferring. 
3.    If the MCS resource is not available. offer to send to that person's voicemail. Then email the 
mc-mcsleaders@salesforce.com
 and 
mc-mcsinternal@salesforce.com
 aliases.
a.    Subject Line: {Insert Account Name} calling on {Insert Case#} and would like to speak to [MCS Engineer Name]
b.    Body of email: Include a brief summary of the call 
4.    A Platinum Leader or backup resource will review and can pick up the case. 
5.    Record the communication in the case as well (or just send from the case) 
If after-hours for US (8 PM US ET), call the toll-free number for the MCS Engineer's Pod to reach the on-call Engineer.  Please use the contact inform on the 
MC-MCS Google site
.
When a call comes in for a UK Platinum Support account on an open case during US business hours, the process below can be used: 
1.    Add all relevant comments to the case regarding the current call including new information and contact details (or just that the contact details were confirmed) 
2.    If the MCS resource is not available. offer to send to that person's voicemail. Then email the 
mc-mcsleaders@salesforce.com
 and 
mc-mcsinternal@salesforce.com
 aliases.
a.    Subject Line: {Insert Account Name} calling on {Insert Case#} and would like to speak to MCS Rep 
b.    Body of email: Include a brief summary of the call 
3.    An MCS Leader will review and can assign the case to a US-based platinum resource to carry the case. If the MCS Engineer, or case owner, needs to be contacted for context, we can reach out within the team. 
4.    Record the communication in the case as well (or just send from the case) 
If after-hours for US (8 PM US ET), call the toll-free number for the MCS Engineer's Pod to reach the on-call Engineer.  Please use the contact inform on the 
MC-MCS Google site
.
 
