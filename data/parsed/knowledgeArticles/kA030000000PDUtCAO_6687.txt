### Topic: In some cases, Custom Report Types on Opportunity Object may not show the same number of records as Standard Report Types with the same criteria.
A Custom Report Type on Opportunity Object may return a different number of records as a Standard Report Type with the same criteria. The results you get when you run your report is based on a number of Organization-Wide Settings and individual User Permissions. You can resolve the discrepancy by checking the below information. 
Resolution
You'll first need to verify if your Organization has 
Territory Management enabled
, which can be done by checking your Set-up Menu for "Territory Management." Once you've checked, please review the appropriate section below. 
 
Territory Management NOT enabled
Depending on the Report Type, you may need to select a Role: 
 
Standard Report Types
 on Opportunities allow "undrilled" reports which means it's not necessary to select a Role to get the desired Opportunities.
Custom Report Type
 you need to "drilled-down" to the right 
Role
, otherwise it defaults to the Running User's Role.
If the Organization has the Opportunity Organization-wide Defaults set to "Public Read/Only:"
 
A 
Standard Report Type
 will show all the Opportunities the Running User can see, and that meet the criteria.
A 
Custom Report Type
 will only show Opportunities owned by a User with the same Role as or a Role below them in the Hierarchy. In this case if the missing Opportunities are owned by a User with a Role higher in the Hierarchy, the Running User will need to click on that Role in the Hierarchy selector (this can be found in the report detail page under its name).
In this Sharing Model, if the owner of the missing Opportunity has a Role at the same level as the Running User, they will need to click on a higher Role. 
Territory Management
 
enabled
 
In an organization with Territory Management enabled a Standard Opportunity report shows different results than an Opportunity Custom Report Type (CRT). This is because the Opportunity CRT Report checks the consistency between Opportunity Owner and Opportunity Territory while the Standard Report does not.
 
If the Opportunity does not have a Territory set,
 
then the Opportunity won't show up in a CRT report at all.
​
If the Opportunity Owner does not belong to the Opportunity Territory, then the Opportunity won't show up in a CRT report. 
There are 2 ways the connection between Opportunity owner and Territory can be maintained:
1. Manually set the Territory of the Opportunity to match the owner.
2. If the Opportunity owner:
 
- has "allow forecasting" checked.
- the Forecast Category is "pipeline."
- the Opportunity is assigned to a Territory.
 
then the Opportunity Owner will be automatically added to the Opportunity Territory. (You can test and see this on the User's Territory related list).
Tip: 
If you change it back to "closed/lost" then you won't see it in the UI, but we still keep the relationship while this User still has "allow forecasting" checked.
