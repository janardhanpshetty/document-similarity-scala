### Topic: Salesforce email URL hacking is not recommended and not supported.
Salesforce email URL hacking is 
not recommended
 and 
not supported
. URL hacking can have some strange side effects especially with the 
From
 field (p26 parameter).
One of the most common observed issues is that if the page param doesn't match the option value exactly, then the customer will end up with two seemingly identical entries in the dropdown. Another side effect spotted is that the parameter being passed is completely ignored and a random value gets selected instead.
Resolution
The way to address these issues is to replicate the option value exactly by looking at the source of the page.
For instance, if the dropdown entry is like this:
<option value="
support@mycompany.net:US Support
">"US Support" &lt;support@mycompany.net&gt;</option>
The p26 parameter value will have to be specified as follows:
/_ui/core/email/author/EmailAuthor?
p26=support@mycompany.net:US Support
NOTE: It is important to keep in mind that this is still a URL hack, which is 
not supported by Salesforce.com
, so it is highly recommended to follow another approach such as using the emailPublisher component.
Additional Links:
Email Publisher Developer Guide
