### Topic: Explains the refresh rate of Chatter Trending Topic and what affects what makes the list.
We don't have anything formally released explaining how Trending Topics work within Chatter.  The following provides a generalize overview of how this feature determines what makes the list.
What is measured?
Trending topics measure the rate of change of topic usage. This means the total number of posts/comments with a topic is less important than the change to that number.
What's the refresh rate?
Trend stats are updated approximately every 4 hours.
What about historical posts/comments?
Trends are recalculated every 4 hours, which means posts older than 8 hours don't play a factor in the current trend.
What tags qualify for calculation?
A tag must be used at least 3 times in the 4 hour period to qualify.
Example
During a trend calculation:
- #commonTopic - has 40 posts - had 35 posts last time - Trend value = +5
- #hotTopic - has 40 posts - had 10 posts last time - Trend value = +30
- #oldTopic - has 10 posts - had 15 posts last time - Trend value = -5
- #newTopic - has 5 posts - had 0 posts last time - Trend value = +5
Those with the highest Trend Value would make the list.
Resolution
