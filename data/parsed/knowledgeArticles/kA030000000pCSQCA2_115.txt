### Topic: Steps to troubleshoot REST or SOAP API issues with errors like Session is Expired or Invalid for Partners with Basic entitlement
If a 
Basic Partner
 logs a case after receiving this error, please follow these steps to troubleshoot before closing the case Out of Scope. 
To troubleshoot Session Expired or Invalid errors, follow the steps below. 
 
Resolution
1. Check if the Client ID is whitelisted --applies only
 if the package is installed in a Professional Edition org or lower
Login to BT and go to 
https://bt1.my.salesforce.com/secur/oauth/allconsumers.apexp
You need at least one item to search (Org Id, Client Id etc)
Search records for that org/client id
It should show a list of "Org Id", "Consumer Id" etc
Now go to https://bt1.my.salesforce.com/?ec=302&startURL=%2F0C4
Look for the name for the app you are checking; for example Groove (in "G"); https://bt1.my.salesforce.com/0C4?thePage%3Aj_id1%3Alsi=2&thePage%3Aj_id1%3Alsc=6
Check for "Oauth Link"
"Oauth Link" consists of "Org ID" (without 00D) and "Consumer Id" (without 888); from the search you did at 
https://bt1.my.salesforce.com/secur/oauth/allconsumers.apexp
Concatenate those 2 and look for that entry in 
https://bt1.my.salesforce.com/0C4?thePage%3Aj_id1%3Alsi=2&thePage%3Aj_id1%3Alsc=6
If a record exists then app has been whitelisted
If a record doesn't exist then app has NOT been whitelisted (properly or missing)
If the app is not whitelisted, let the partner know, then transfer the case to the Alliances Technical Support queue for whitelisting 
2. Look at the session timeout value  
Setup | Session Settings | Session Timeout
If this value is low (under 2 hours), ask the partner if this can be increased, 
and let them know that low Session Timeout values can cause sessions to become expired. 
3. Find a successful API callout, and an API callout failure in Splunk, within same session (refer to the session timeout value above) 
If possible, try to find successful and failing callouts within 1/2 of the time specified by the session timeout. 
Compare the Session ID 
- these should be the same for both callouts
Compare the Endpoint 
- these should be the same for both callouts
If the Session IDs or Endpoints do not match, provide the partner with the two Session IDs or Endpoints,
ask the partner to correct this issue, and let you know if the problem is resolved. 
If all of the above are verified, and you have not found any issues,
escalate to Tier 3/R&D with this information for further troubleshooting.
