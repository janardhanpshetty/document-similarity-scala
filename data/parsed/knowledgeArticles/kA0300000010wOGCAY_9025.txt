### Topic: If the Lightning For Outlook is Stuck in loading page after entering token on the Desktop Client 2013 / 2016. You can try using this tool to debug and capture additional logs.
If the Lightning for Outlook is Stuck in loading page after entering token on the Desktop Client 2013 / 2016. You can try using this tool to debug and capture additional logs.
You will find in some instances the customer will be able to login into Lightning for Outlook via the OWA or Office365 but are unable to login via Desktop client version of Outlook 2013. 
Resolution
Please see the instructions below on how to do this:
 
1)
 Download the 
debug manifest
 "Salesforce-Debug.xml" included in this article
2)
 Log in to your Exchange 2013 web interface.
3)
 Click on the Cog in the top right hand corner and select Manage Apps
 
 
4)
 Click on the "+" Symbol and select "Add from File"
 
 
5)
 Browse to the location where the "Salesforce-Debug.xml" was downloaded to
6)
 Click on "Next" followed by "Install" and "OK"
7)
 Setup a proxy to record all network activity
8)
 Launch the debug mailapp in Outlook
9)
 Wait 45 seconds
10)
 If you are still stuck on the loading screen...
a)Click inside of the app to make sure the app has keyboard focus
b)Press the ‘p’ key 5 times quickly
c)You may be prompted to allow clipboard access. Click “Allow access”
 
 
d)Open notepad, and paste the contents of your clipboard. Save this document as parentlogs.txt
e)Clear the logs from your clipboard (copy something random, like the subject of the email)
f)Click the Salesforce cloud 5 times. The cloud should disappear and you should see something new on the screen.
g)Take a screenshot of the app
h)Click inside of the app to make sure the app has keyboard focus.
i)Press the ‘c’ key 5 times quickly
j)You may be prompted to allow clipboard access. Click “Allow access”
k)Open notepad, and paste the contents of your clipboard. Save this document as childlogs.txt
l)Make sure the child logs are different from the parent logs. Sometimes the clipboard does not get cleared and the logs from step (d) get pasted again.
m)Save the network logs from the proxy
n)Send the network logs, parentlogs, childlogs, and the screenshot to T3 for analysis
 
Sample logs on next page:
 
To give you a rough idea, the parent/child logs should look something like this format when you paste into notepad:
 [{"timestamp":1445528125793,"function":"groupCollapsed","arguments":{"0":"INFO: showLoading"}},{"timestamp":1445528125795,"function":"groupCollapsed","arguments":
{"0":"INFO: loading progress: 1% currently 0"}},{"timestamp":1445528125796,"function":"groupCollapsed","arguments":{"0":"INFO: showLoading"}},
{"timestamp":1445528125796,"function":"groupCollapsed","arguments":{"0":"INFO: loading progress: 1% currently 1"}},
{"timestamp":1445528125935,"function":"groupCollapsed","arguments":{"0":"INFO: Found window.Office"}},{"timestamp":1445528125935,"function
":"groupCollapsed","arguments":{"0":"INFO: Setting Office.initialize"}},{"timestamp":1445528126034,"function":"groupCollapsed","arguments"
:{"0":"INFO: Office.initialize has been called."}},{"timestamp":1445528126034,"function":"groupCollapsed","arguments"
:{"0":"INFO: GETTING ROAMING SETTINGS: orgUrl, orgId"}},{"timestamp":1445528126034,"function":"groupCollapsed",
"arguments":{"0":"INFO: GOT ROAMING SETTINGS: ","1":[{"orgUrl":"https://eu4.salesforce.com","orgId":"00DD0000000ri1e"}]}},{"timestamp":1445528126035,"function":"groupCollapsed","arguments":{"0":"INFO: loading progress: 40% currently 1"}},{"timestamp":1445528126035,"function":"groupCollapsed","arguments":{"0":"INFO: OfficeConnectorController.init"}},{"timestamp":1445528126375,"function":"groupCollapsed","arguments":{"0":"INFO: Setting up channel for origin:
https://eu4.salesforce.com"}},{"timestamp":1445528126389,"function":"groupCollapsed","arguments":"INFO:
Subscribing for messages on source window,[object Window]"},{"timestamp":1445528126390,"function":"groupCollapsed",
"arguments":{"0":"INFO: Generated token, providing it now"}},{"timestamp":1445528126390,"function":"groupCollapsed",
"arguments":{"0":"INFO: POSTING MESSAGE: {\"type\":\"provideToken\",\"token\":\"5760478372685611\"} for origin *"}},{"timestamp":1445528126409,"function":"groupCollapsed","arguments":{"0":"INFO: Loading inner IFRAME with url
https://eu4.salesforce.com/clients/mailapp/authenticate?orgId=00DD0000000ri1e&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkkzTDVfM3pTRVZPT3RmQmZFTGpXRmMwaFNwWSJ9.ey
Jpc3MiOiIwMDAwMDAwMi0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDBAZWZkNDIwZWUtYTkyOC00NGFhLThlMzYtNWQ2NDhmNzAy
MTZmIiwiYXVkIjoiaHR0cHM6Ly9tYWlsYXBwLmZvcmNlLmNvbS9jbGllbnRzL21haWxhcHAvYWN0aXZhdGlvbj9kZWJ1Z3V0aWw9dHJ1ZSIsIm5iZiI6M
TQ0NTUyNzg1NCwiZXhwIjoxNDQ1NTU2NjU0LCJhcHBjdHhzZW5kZXIiOiIwMDAwMDAwMi0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDB
AZWZkNDIwZWUtYTkyOC00NGFhLThlMzYtNWQ2NDhmNzAyMTZmIiwiaXNicm93c2VyaG9zdGVkYXBwIjoiVHJ1ZSIsImFwcGN0eCI6IntcIm1zZXhja
HVpZFwiOlwiMjYyOTFhMDctZjlkOC00YmQ1LThjZTctMDdhN2I0ZWNhMzU1XCIsXCJ2ZXJzaW9uXCI6XCJFeElkVG9rLlYxXCIsXCJhbXVybFwiOlwia
HR0cHM6Ly9vdXRsb29rLm9mZmljZTM2NS5jb206NDQzL2F1dG9kaXNjb3Zlci9tZXRhZGF0YS9qc29uLzFcIn0ifQ.DAy-4kk-DhIIttVGVgvAg5wHJ8q5MDaLlAE-sDpg3eObt0rzffd4JPZu9CHNKEUDA0MrIIjHX_yRYqKHIHj09E4G-vFly5lU_fCHhGpCqWkbrgqKWE-LfNKt78Efz0k49SJBXBaozPslcWS9NUELtQW4FHyEVcuDpABFworlXYHjDKWGSqrQkh3ELt1b_duOnaIyTAv8bxsc6fvNx4fPKBuE4JQB0I3T8N-99GZDCl-aR1BZ5zbv2A_fYNw8ceiHsNyR75mNCTDA6yXPyDbbt7tWUIcC6sxxP0W4dfSOkqENgyTV9l13jxcYow60KXEULg0b1lqY5g4uEiEB8sgZUg&email=
daniel%40sfdcmsft.com&redirectUrlParams=P21lc3NhZ2VUb2tlbj01NzYwNDc4MzcyNjg1NjExJmJ5cGFzc0RvbWFpblZhbGlkYXRpb249dW5kZWZpbm
Vk&messageToken=5760478372685611"}},{"timestamp":1445528126410,"function":"groupCollapsed","arguments":{"0":"INFO: Checking origin
https://outlook.office365.com against /^(http(s)?):\\/\\/[a-z0-9.-]*(\\.salesforce\\.com|\\.lightning\\.force\\.com)(:\\d+)?$/ false"}},{"timestamp":1445528126410,"function":"groupCollapsed","arguments":{"0":"INFO: ignoring message from origin: https://outlook.office365.com"}},{"timestamp":1445528128039,"function":"groupCollapsed","arguments":{"0":"INFO: Checking origin https://eu4.lightning.force.com against /^(http(s)?):\\/\\/[a-z0-9.-]*(\\.salesforce\\.com|\\.lightning\\.force\\.com)(:\\d+)?$/ true"}},{"timestamp":1445528128039,"function":"groupCollapsed","arguments":{"0":"INFO: Checking origin https://eu4.lightning.force.com against /^(http(s)?):\\/\\/[a-z0-9.-]*(\\.salesforce\\.com|\\.lightning\\.force\\.com)(:\\d+)?$/ true"}},{"timestamp":1445528128040,"function":"groupCollapsed","arguments":{"0":"INFO: Updating origin to: https://eu4.lightning.force.com, was
https://eu4.salesforce.com"}},{"timestamp":1445528128040,"function":"groupCollapsed","arguments":{"0":"INFO: RECEIVED MESSAGE:
{\"token\":\"5760478372685611\",\"data\":{\"callId\":1,\"type\":\"methodCall\",\"method\":\"timestampService.recordMultiple\",\"parameters\":[{\"framed_page_load\":1445528127934}]}}"}},{"timestamp":1445528128040,"function":"groupCollapsed","arguments":{"0":"INFO: calling
timestampService.recordMultiple"}},{"timestamp":1445528128040,"function":"groupCollapsed","arguments":{"0":"INFO: recording multiple timestamps","1":[{"framed_page_load":1445528127934}]}},{"timestamp":1445528128040,"function":"groupCollapsed","arguments":{"0":"INFO:
[WindowChannel] postMessage requested for methodReturn","1":[{"type":"methodReturn","callId":1}]}},{"timestamp":1445528128040,
"function":"groupCollapsed","arguments":{"0":"INFO: POSTING MESSAGE: {\"type\":\"methodReturn\",\"callId\":1} for origin https://eu4.lightning.force.com"}},{"timestamp":1445528128089,"function":"groupCollapsed","arguments":{"0":"INFO: Child iframe loaded"}},{"timestamp":1445528128089,
"function":"groupCollapsed","arguments":{"0":"INFO: loading progress: 85% currently 40"}},{"timestamp":1445528128348,"function":"
groupCollapsed","arguments":{"0":"INFO: loading progress: 86% currently 85"}},{"timestamp":1445528128544,"function":"groupCollapsed",
"arguments":{"0":"INFO: loading progress: 87% currently 86"}}]
