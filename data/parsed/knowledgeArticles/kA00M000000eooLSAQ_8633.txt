### Topic: This article explains the difference between Role Name and Owner Role Name on report
The 
Role Name
 and 
Owner Role
 name shows different on the User report type.
Resolution
The 
Role
 
Name
 field displays the
 Role
 of the user record.
The 
Owner Role
 
Name
 is referencing the role of the user who is the 
Owner
 of the record. The Owner of the record will be the user who created the 
External User. 
Since 
Internal User
 records do not have owners, the 
Owner Role Name
 will only populate for 
External
 
Users
 (
Community or Partner
). 
