### Topic: External IDs and OData Entity Keys | Salesforce
External IDs and OData Entity Keys | Salesforce
External IDs and OData Entity Keys
When you access external data with the OData 2.0 adapter for Salesforce Connect, the values of the External ID standard field on an external object are derived according to the entity key that’s defined in the OData service metadata document.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Developer
 Edition
Available for an extra cost in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Each external object has an 
External ID
 standard field. Its values uniquely identify each external object record in your org. When the external object is the parent in an external lookup relationship, the External ID standard field is used to identify the child records.
Important
Don’t use sensitive data as the values of the External ID standard field, because Salesforce sometimes stores those values.
External lookup relationship fields on child records store and display the External ID values of the parent records.
For internal use only, Salesforce stores the External ID value of each row that’s retrieved from the external system. This behavior doesn’t apply to external objects that are associated with high-data-volume external data sources.
This list view for the Order_Detail external object displays External ID values.
Each External ID value is derived according to the entity key that’s defined in the OData service metadata document of the remote data service (OData producer). The entity key is formed from a subset of the entity type’s properties.
This excerpt from an OData service metadata document shows that the External ID values for the Order_Detail external object are derived from the OrderID and ProductID properties.
<EntityType Name="Order_Detail">
  
<Key>

    
<PropertyRef Name="OrderID"/>

    
<PropertyRef Name="ProductID"/>

  
</Key>

  <Property Name="OrderID" Type="Edm.Int32" Nullable="false"/>
  <Property Name="ProductID" Type="Edm.Int32" Nullable="false"/>
  <Property Name="UnitPrice" Type="Edm.Decimal" Nullable="false" Precision="19" Scale="4"/>
  <Property Name="Quantity" Type="Edm.Int16" Nullable="false"/>
  <Property Name="Discount" Type="Edm.Single" Nullable="false"/>
...
This record detail page displays the OrderID and ProductID fields. Their values are combined to create the value of the External ID standard field.
See Also:
OData 2.0 or 4.0 Adapter for Salesforce Connect
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=odata_external_id_entity_key.htm&language=en_US
Release
202.14
