package similarity.ml

import org.apache.spark.ml.UnaryTransformer
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.types.{ArrayType, DataType, StringType}

/**
 * Created by jshetty on 8/9/16.
 */
class IdentityTransformer(override val uid: String) extends UnaryTransformer[Seq[String], Seq[String], IdentityTransformer] {
  def this() = this(Identifiable.randomUID("identityTransformer"))

  override protected def createTransformFunc: (Seq[String]) => Seq[String] = {_.toSeq}

  override protected def outputDataType: DataType = new ArrayType(StringType, true)

}
