### Topic: How to contact Marketing Cloud Premier Support as a Social Studio only customer.
* Please note this applies to Social-only customers. If you have a Salesforce or Marketing Cloud log-in, you can contact support through the help and training portal*
As a Social Studio customer, there are two ways to contact Marketing Cloud Premier Support. 
You can submit a case via email or call the Customer Support team. 
Option 1 - Email Support
If you'd like to log a ticket with our customer support team, please email them directly via marketingcloudsupport@salesforce.com *
*Premier response times outlined below
Option 2 - Phone Support
Phone support is available around the clock in English.  
If you have a Severity 1 critical production issue that affects all users, please call Support to speak to a representative directly at 
866-767-0701.
Global Numbers:
UK
: 00800.404.551.52 
AUS:
 1.800.203.823
Please note that Premier Support response times are determined based on the Severity of your Case. Please see the table below to understand how we determine response times. 
Support cases are assigned a severity level from 1-4 by the support team. A Severity level 1 case should always be submitted by phone - and will always be prioritized and worked first. Every effort is made to resolve high priority issues as quickly as possible.
Further details can be found in your Premier Terms and conditions. 
Case Escalation
If you are looking to escalate your case, please reach out directly to your Account Executive.
 
Resolution
