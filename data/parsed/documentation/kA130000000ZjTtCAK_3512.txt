### Topic: Customize the Community Self-Registration Process with Apex | Salesforce
Customize the Community Self-Registration Process with Apex | Salesforce
Customize the Community Self-Registration Process with Apex
Update the 
CommunitiesSelfRegController
 to customize the default self-registration process for your community. You can use the same controller for the default self-registration page (
CommunitiesSelfReg
) or a custom Visualforce or Community Builder self-registration page.
You can configure self-registration entirely in Community Management. This customization is recommended only if you want to modify the self-registration behavior beyond the defaults, if you have more then one community in your organization, or if you are using a custom self-registration page.
Note
You can add, edit, or delete Apex using the Salesforce user interface only in a Developer Edition organization, a Salesforce Enterprise Edition trial organization, or sandbox organization. In a Salesforce production organization, you can only make changes to Apex by using the Metadata API 
deploy
 call, the Force.com IDE, or the Force.com Migration Tool. The Force.com IDE and Force.com Migration Tool are free resources provided by Salesforce to support its users and partners, but are not considered part of our Services for purposes of the Salesforce Master Subscription Agreement.
From Setup, enter 
Apex Classes
 in the 
Quick Find
 box, then select 
Apex Classes
.
Click 
Edit
 next to 
CommunitiesSelfRegController
.
Optionally, enter a value for 
ProfileId
 to define the type of profile the user should be assigned.
If you selected a default profile while setting up self-registration in Community Management, the value in the Apex code will override that default.
Note
Note that regardless of which role you enter for the 
roleEnum
, the role for new users will default to 
None
. Once a user self-registers, you can update their role on the user detail page.
Enter the account ID for the partner or customer account that users who self register should be associated with.
If you selected a default account while setting up self-registration in Community Management, the value in the Apex code will override that default.
Ensure that the account you use is enabled as a partner. To do so, go to the account, click 
Manage External Account
, then click 
Enable as Partner
.
If you’re enabling self-registration for multiple communities, add code to create appropriate types of users for each community, that is, assigning the correct profile, role, and account ID per community.
Click 
Save
.
Enable access to accounts and contacts for the guest profile. The guest profile is automatically associated with your community’s Force.com site.
From Setup, enter 
All Communities
 in the Quick Find box, then select 
All Communities
 and click the 
Manage
 link next to a community. To access this page, you need the Create and Set Up Communities” permission.
From Community Management click 
Administration
 | 
Pages
 | 
Go to Force.com.
.
Click 
Public Access Settings
.
Click 
Edit
.
In the Standard Object Permissions section, select Read and Create next to Accounts and Contacts.
Click 
Save
.
In the Enabled Apex Class Access related list, click 
Edit
.
Add the 
CommunitiesSelfRegController
 and click 
Save
.
In the Enabled Visualforce Page Access related list, click 
Edit
.
Add the 
CommunitiesSelfReg
 and click 
Save
.
Optionally, if you want to customize the contents of the default self-registration page, edit the 
CommunitiesSelfReg
 page.
From Setup, enter 
Visualforce Pages
 in the 
Quick Find
 box, then select 
Visualforce Pages
.
Click 
Edit
 next to 
CommunitiesSelfReg
.
Add code to customize the fields required for self-registration or the page’s look and feel.
In the default form, all fields except 
Password
 are required.
Click 
Save
.
Once setup is complete, external users who submit the completed self-registration form (including a password) are logged in to the community.
Note
If a user self-registers for a community with Chatter Answers enabled, the Chatter Answers User permission is not automatically set for the user.
If you’re using a custom Visualforce self-registration page instead of the default 
CommunitiesSelfReg
 page, add the following code to 
CommunitiesSelfRegController
. Replace 
CommunitiesCustomSelfRegPage
 with the name of the custom self-registration page.Then add this to the first line of code in the 
CommunitiesSelfReg
 page.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=communities_self_reg.htm&language=en_US
Release
202.14
