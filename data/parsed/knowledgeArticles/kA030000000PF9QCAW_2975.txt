### Topic: This article describes the product features listed on the Help & Training
To help with routing your cases quickly, please review the following definitions to select the correct product topic:
CRM Applications:
  
CRM Application
Customer Service and Support
Questions or issues with Cases Management, Solutions, Knowledge, Live Agent, Ideas, Customer Support Portal
Sales and Marketing
Questions or issues with Opportunities, Leads, Forecasting, Territory Management, Data.com/Jigsaw
Reports & Dashboards
Questions or issues with creating Reports, Dashboards, Analytic Snapshots, or new Custom Report Types
Email
Questions or issues with mail delivery, email to case, or automated system emails
Mobile
Questions of issues with mobile devices: Salesforce1, iPad, iPhone, Touch, Blackberry, Android, Mobile Chatter
Network & Browser Performance
Questions or issues related to response times, browser or network performance
Desktop Integration
Questions or issues related to Chatter Desktop, Connect for the various email systems,  Microsoft Outlook, Microsoft Office Integration, along with Salesforce for Outlook 
Security
Questions or issues with Computer Activation, Trusted IP Restrictions, Phishing, Security Tokens, Data Residency Option for encryption or login issues
Packaging, Change Sets & Installing Apps
Questions or issues related to Packaging, Uploading and Installing apps for Appexchange
Limits & Feature Activation
Requests to activate add-on features like Multi-currency, Territory Management, Person Accounts; increase limits on workflows, portal roles, email etc AND for a change of your system administrator
Configuration
Questions or issues with Roles and profiles, Workflows, Custom Objects, Custom Fields, Page Layouts, Sharing, Translation, Validation rules and any other configuration question
Data Management & Search
Questions or issues with Data Import, Data Loader, Data Export, Storage Usage, Sandbox, Content, Documents, Files and Search
Collaboration 
Chatter 
Developer Support
 
  
Developer Support
Apex and Visualforce
Questions or issues related to Apex and Visualforce Development, 
Force.com
 sites and 
Site.com
API integration and SSO
Questions and issues related to all our APIs, API Performance, Single Sign On, OAuth, SAML, SSL Certificates and Delegated Authentication
 
Sales and Billing
  - Questions related to invoices and license activations
Help and Training
 - Questions or issues related to the Help & Training Site including difficulties completing Training module.
Additional Products
 - Buddy Media, Radian6, Work.com, Data.com Clean & Data.com Prospect
 
 
Resolution
