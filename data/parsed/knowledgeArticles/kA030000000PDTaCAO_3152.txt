### Topic: Learn how to delete components from a Managed Package.
In order to get a Managed Package to function, you'll need to add components. We'll fill you in on what needs to be done when you need to remove one of those components.
Resolution
In order to remove components from a Managed Released Package, there are two options:
1. Partners can request to 
enable "managed component deletion"
 feature in the packaging org.
NOTE
: 
Although a component is deleted, its 
Name
 remains within 
Salesforce
. You can never create another component with the same name. The Deleted Package Components page lists which names can no longer be used.
Also, 
some components aren't developer-deletable
.
 
If a component can't be deleted using this method, you'll need to follow option 2. 
2. Support can change the release version from Managed-Released, to Managed-Beta 
if certain conditions are met
. Be sure to provide support with the following details:
i. Package Version details -  Name, version, etc.
ii. Package version in question and later versions should not be currently installed in any subscriber org (
if installed, they need to be uninstalled
)
iii. If the package version in question and later versions have patch org created. If you do have any patch org created, please mention that in your case and confirm if it is ok to purge the patch org. (
Patch org needs to be purged to revert the package
)
NOTE
: We can't change a Managed Package to an Unmanaged Package.
A few things to keep in mind
The package will need to go through security review again, once changes are made and new version is released.
Review fee will also apply, please work with 
your PAM / TE for more details on the fee.
All subsequent released versions will also be reverted to beta, eg, if v1.2 is requested to be reverted to beta and v1.3 & 1.4 were released, all released versions after v1.2 will also be reverted.
