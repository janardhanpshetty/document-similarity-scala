### Topic: Viewing and Adding Partners | Salesforce
Viewing and Adding Partners | Salesforce
Viewing and Adding Partners
Available in: Salesforce Classic
Available in: 
All
 Editions for organizations activated before Summer ’09
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions for organizations activated after Summer ’09
User Permissions Needed
To add partners to an account:
“Edit” on accounts
AND
“Read” on opportunities
To add partners to an opportunity:
“Read” on accounts
AND
“Edit” on opportunities
The opportunity and account detail pages include a Partners related list for viewing and adding partner relationships.
To view account information for a specific partner, click the partner’s name in the Partners related list.
If you do not have access to view and edit a partner account, the account name appears in the related list but you have no access to other information about that account.
Click 
Del
 next to a partner to the partner relationship.
Note
If you change the account for an opportunity that has partners, all partners are deleted from the Partners related list.
To add a partner to an opportunity or account:
Click 
New
 in the Partners related list.
For opportunities only, select the 
Primary
 option to indicate the primary partner for the opportunity, if applicable.
An opportunity can have only one primary partner. Setting a new primary partner automatically removes the “Primary” status from any existing primary partner.
Marking a partner as “Primary” allows you to report on the partner in all of the opportunity reports.
In the 
Partner
 column, enter the name of an existing account by typing into the box or clicking the lookup icon to perform an account search. Alternatively, click 
New
 from the lookup dialog to create a new account.
In the 
