### Topic: How to configure a workspace and items to think about when getting started.
How and why to use Workspaces to quickly organize teams by region, brand or business function to promote collaboration for campaigns, content creation, publishing and reporting.
 
What is a Workspace?
Workspaces are the core component of Social Studio. In a workspace, you will be able to:
View a calendar of posts from that workspace’s social accounts
Compose, schedule, and publish content for that workspace’s social accounts
Compose drafts for later use
View performance for the workspace’s social accounts
Add notes to communicate with other members who have access to the workspace
Use approval rules to preview and approve content before it goes live
Engage with posts made to that workspace’s social accounts or brought in by a social media search
Workspace Design - What to think about when getting started
Workspaces are a core component in Social Studio and enable customers to establish their own groups or "working spaces" and can be utilized by the following ways:
By discrete business units
By geographical markets
By external agencies
By social campaigns
Workspaces can be a combination of the above (i.e product lines within geographical markets). It is important to define who in your organization should be a member of what workspace(s).
Workspaces allow for a logical and central place to plan, publish and engage content. A workspace is also an area for users to collaborate with a set of shared resources, social accounts and calendars.
Using workspaces the administrator (Super User) configures an environment that allow other workspace members to interact with social posts that are specific to that business tenant.
It’s important to remember that:
The same social account can be added to more than one workspace
Users can be members of more than one workspace
Workspace members can have different permissions in different workspaces
*** For a workspace to be effective it must have members who can collaborate and interact and social accounts in which to post, approval and schedule content.
Creating your Workspace - Best Practices
After Roles and Users have been set up and defined, a workspace should be created to begin the process of interacting and creating content to share and track among teams.
From the Main window, click the 
+ Create New
 button.
Enter the name of the workspace in the 
Workspace Name
 field.
Enter an optional description of the workspace in as little or much detail as necessary.
Assign a picture for the workspace by clicking the 
Upload a Workspace Image
 icon.
Click the 
Next 
button to add Social Accounts to the workspace
 
Finalizing the Workspace
With the workspace named, described, picture assigned, and members and social accounts added, a summary window appears showing the following for the newly created Workspace:
Workspace block - This block shows the name of Workspace, description of the Workspace and a workspace picture (if available)
User Accounts block - This block shows the total number of user accounts and small pictures of the users (if available) Hovering over the icon will show the name of the member.
Social Accounts block - This block shows the total number of social accounts with a small icon of the social account. Hovering over the icon will show the name of the social account
Click 
Finish
 to start publishing and engaging within your Workspaces. 
Making Changes to your Workspace
The settings for Workspaces you create or you Administer can be changed at any time include the following:
Basic Workspace Information
Change Workspace Name
Change Workspace Description
Change Workspace Image
Remove Workspace
Workspace Members
Social Accounts
Topic Profiles
Publish Labels
Bitly Tracking
Approval Rules
Engagement Macros
Publish Macros
Engage Tab Templates
Resolution
