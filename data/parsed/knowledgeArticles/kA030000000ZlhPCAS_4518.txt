### Topic: With our Spring ’15 release*, we will be ending support for the Legacy Answers feature. While you will still be able to use the Legacy Answers feature, support for technical or product issues will no longer be provided by Salesforce, and there will not be active development on the product. *Currently targeted for February 2015; date subject to change
Legacy Answers End of Support in Spring ‘15
As a system administrator of a Salesforce organization, we want to notify you of an important change to the availability and support of our Legacy Answers feature 
(referred to in the past as Answers)
, which your organization has used within the last six months.
What is changing?
With our Spring ’15 release*, we will be ending support for the Legacy Answers feature. While you will still be able to use the Legacy Answers feature, support for technical or product issues will no longer be provided by Salesforce, and there will not be active development on the product.  
*
Currently targeted for 
February 
2015; date subject to change
Why are we doing this?
 
We launched the Chatter Answers feature in Spring '12 to provide a new product with enhanced functionality, based on feedback from users. Chatter Answers provides more features, greater flexibility, and a more intuitive User interface.
What do we recommend?
 
We encourage you to upgrade to Chatter Answers before the Spring ‘15 release.  It has enhanced features and more functionality than the Legacy Answers product - all at no additional cost. We understand that this may cause some disruption for those using Legacy Answers, and we are here to help. Upgrading requires only minor changes on your part. To make it as smooth as possible, we created a Knowledge Article that you can find 
here
.
How can I get more information?
If you have further questions, please 
reach out to Customer Support by logging a ticket on your Help & Training page.
To understand Salesforce’s philosophy on retiring functionality, please see our feature retirement philosophy 
here
.
Any unreleased services or features referenced in this or other press releases or public statements are not currently available and may not be delivered on time or at all. Customers who purchase Salesforce applications should make their purchase decisions based upon features that are currently available.
 
Frequently Asked Questions
 
1.  When will the End of Support for Legacy Answers take effect?
The End of Support for Legacy Answers is currently targeted for the Spring ’15 release in February 2015, although dates are subject to change.
2.  What exactly will happen after the End of Support date passes?
 
After the Spring ’15 release, you may continue using Legacy Answers, yet support for technical or product issues will no longer be provided by Salesforce. You will be asked to upgrade to Chatter Answers.
3.  Why is Salesforce making this change? 
Based on feedback from users, we had developed a new product with enhanced functionality.  The Chatter Answers feature that we launched in Spring ‘12 provides more features, greater flexibility and a more intuitive User Interface.
4.  What do you recommend?
 
We encourage you to upgrade to Chatter Answers before the Spring ‘15 release. The upgrade has enhanced features and more functionality than the Legacy Answers product, and at no additional cost!  To make the upgrade to Chatter Answers as smooth as possible, we created a step-by-step description which you can see below.
5.  
How can I find additional information? 
If you have any further questions, please 
reach out to Customer Support by logging a ticket on your Help & Training page.
______________________________________________________________________
Customer Step-by-Step: Migrating from Legacy Answers to Chatter Answers
 
When we built Chatter Answers, we allowed for a smooth transition from the Legacy Answers product. The Data is the same:
 - The data Category Group is the same.
 - The parent relationship with Community is the same.
 - The API is the same.
Chatter Answers has many enhanced features, including merging communication with Agents into a single, unified experience, adding reputation management, flagging and moderation, email notifications, and more.
To migrate (find more detail in the 
Chatter Answers Implementation Guide
)
Enable Chatter Answers: Edit your current community object and enable it for Chatter Answers. 
Generate a new VF Page (this is automatic if you leave the field empty). It will have the name of your community appended. 
Enable the force.com site you use for Chatter Answers 
The only trick to go from Legacy Answers to Chatter Answers is the fact that Chatter Answers supports many communities. The way this is done is through a tree structure of data categories. Legacy Answers may only take the top level when you are posting, so you may only see Community 1 and Community 2 instead of seeing what is underneath. You can learn more about multiple communities by 
clicking here
. 
For Chatter Answers, you will select the top level of your community (i.e. Community 1) which would then display all the sub-topics.
To transition from Legacy Answers to Chatter Answers:
Create a top level Category in your Data Category - a Group named "All" (or whatever name you choose).
Create a community top level, for example called "FME." 
MOVE all your existing categories under “FME.”
On the Community object, select “FME” as your top level.
You are now finished and can create branding on your page.
 
Resolution
