### Topic: About the Service Wave App | Salesforce
About the Service Wave App | Salesforce
About the Service Wave App
Salesforce created the Service Wave Analytics app to make it easy for service managers and agents to use data to drive the success of your service business. Learn the app’s benefits before you create and use the app to explore your Service Cloud data.
Available in Salesforce Classic and Lightning Experience.
Available for an extra cost in 
Enterprise
, 
Performance
, and 
Unlimited
 Editions. Also available in 
Developer
 Edition
.
User Permissions Needed
To use Wave templated apps:
“Use Wave Analytics Templated Apps”
To use Service Wave:
“Access Service Cloud Analytics Templates and Apps”
To create and manage Wave apps:
“Manage Wave Analytics Templated Apps”
“Edit Wave Analytics Dataflows”
Service Wave gives you best-practice key performance indicators (KPIs) about your Salesforce service data in a single place. We’ve based the app’s dashboards on learnings from many years of helping businesses manage customer relationships. The goal is to provide the right amount of information at the right time to help both managers and agents make the right decision.
Service managers get a complete view of service customer data that includes trending as well as historical and peer benchmarks. Agents can quickly view a snapshot of each case and customer to help them make quick decisions about their next customer interactions.
We’ve done a lot of the hard work: Our team has built complex queries, formulas, and ratios that draw from your service and sales data and assembled them into easy-to-read visualizations. You just create the app: using a built-in configurator, answer a few questions about the data and fields you’d like to see, and Wave takes care of the rest. Once you’ve created the app, use its prebuilt datasets and dashboards to explore Service Cloud data from any device that supports Wave.
You get actionable insights fast from your sales data using the intuitive Wave interface. And you can drill deeper into key aspects of your business by customizing Service Wave around your needs.
Note
Your organization can use Service Wave without Salesforce Wave Analytics platform by purchasing a Service Wave license. A Service Wave license is included with the Wave platform license.
Parent topic:
 
The Service Wave Analytics App
Next topic:
 
Service Wave Prebuilt Dashboards and Datasets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_app_service_wave_learn.htm&language=en_US
Release
202.14
