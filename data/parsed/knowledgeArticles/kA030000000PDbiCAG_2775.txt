### Topic: Steps to mass delete attachments via the Data Loader
How do I mass delete attachments via the Data Loader?
Resolution
Note: Best practices dictate that you should perform a complete Data Export preceding any Mass Delete functions in your production salesforce.com org.
 
1.
         
Perform a Data Export for the object Attachments that you wish to delete:
            Setup | Administration Setup | Data Export
            Note: Make certain you select the Include attachments/documents (including attachments/documents may significantly slow down and enlarge your export) checkbox.
2.         Take the original Data Export file called Attachment.csv"
3.         Remove all rows / attachments that you do not want to delete
4.         Remove all columns except Id
5.         Save the modified CSV as delete_attachment.csv
6.         Login to Apex Data Loader
7.         Select the Delete option
8.         In Step 2, select the Show all Salesforce Objects checkbox; Select Attachments; then click on the Browse button and select the delete_attachment.csv, then click the Next button.
9.         In Step 3, click on the Create or Edit a Map button and then click on the Auto-Match Fields to Columns button. This will match up the column header (Id) to your database. Click OK to proceed. When back on the Step 3 Mapping window, click the Next button.
10.       Specify the directory in Step 4 Finish that will house your Apex Data Loader Success and Error files. (Note: This can be any folder on your computer). After you've specified the directory, click the Finish button.
11.          You will then be prompted Yes/No pop-up to proceed with the Delete action. Click Yes. The Apex Data Loader will then attempt to delete your Attachments on each of the row records. This process can take quite a while depending on the size and number of attachments being inserted. You will be prompted with an Operation Finished pop-up when the processes has completed.
 
