### Topic: Due to excel's cell format, leading zeroes are omitted from the file. To get the exact data, we can utilize excel's External Data converter.
During an export, leading zeroes from number/text fields are being omitted. This usually happens with Zip Codes (01234 turns to 1234). This can be fixed by formatting the affected cell. For more details and resolution please click 
here
.
However, if the affected data/column is not a zip code and the number of digits varies, the above recommendation won't be much helpful. In this case, please follow these steps on how to retain leading zeroes from your extracted file.
Resolution
1.) Open Microsoft Excel
2.) Go to Data | Click "From Text"
3.) Browse and Select your extracted csv file | Import
4.) Select Delimited | Uncheck Tab and Check Comma | Next
5.) Choose Text under Column Data Format | Select the affected Number Column from the Data Preview | Finish | OK
