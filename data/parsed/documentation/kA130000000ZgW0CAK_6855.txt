### Topic: Setting Values in the Process Builder | Salesforce
Setting Values in the Process Builder | Salesforce
Setting Values in the Process Builder
Throughout the Process Builder, you need to set values, for example, to set conditions in a criteria node, to set the fields on a new case in a Create a Record action, or to specify an Apex method to reference.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Field Picker
Use the field picker to reference fields on the record that started the process or fields on related records.
Process Builder Value Types
When setting a value for a given field—whether on the record that started the process or a related record— the available value types are filtered based on the field that you’ve selected.
Multi-Select Picklists in the Process Builder
The Process Builder lets you select multiple values for a multi-select picklist field.
Setting Advanced Options in the Process Builder
The Process Builder allows you to choose some advanced options for executing actions in your processes.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=process_userinterface_values.htm&language=en_US
Release
202.14
