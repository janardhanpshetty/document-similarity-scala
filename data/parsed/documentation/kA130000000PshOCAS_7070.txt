### Topic: What is the difference between the Account Site and Location Type fields? | Salesforce
What is the difference between the Account Site and Location Type fields? | Salesforce
What is the difference between the 
Account Site
 and 
Location Type
 fields?
Account Site
 is the field found on account records and the Data.com account card. 
Location Type
 is the field found on D&B Company records. (D&B Company records are available to organizations that have purchased Data.com Premium Prospector.) Both fields contain the same values: 
Single location
, 
Headquarters/Parent
, and 
Branch
.
See Also:
Using Data.com FAQ
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_datadotcom_what_is_the_difference_bt_AccountSite_and_LocationType_fields.htm&language=en_US
Release
202.14
