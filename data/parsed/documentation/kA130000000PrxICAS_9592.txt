### Topic: Can I change or delete the drop-down list of entries that appears when I edit a text field? | Salesforce
Can I change or delete the drop-down list of entries that appears when I edit a text field? | Salesforce
Can I change or delete the drop-down list of entries that appears when I edit a text field?
No. These auto-complete entries that appear when you are editing certain text fields are a feature of Internet Explorer. The browser remembers text you have entered previously and provides a list of those entries for you to automatically complete the field. If you would like to turn this feature off, click 
Tools
 on your browser’s menu bar, select 
Internet Options
, click the 
Content
 tab, and then choose the 
AutoComplete
 button to change your browser’s settings.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_general_can_i_change.htm&language=en_US
Release
202.14
