package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{NGram, RegexTokenizer}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, udf}

import scala.collection.immutable.HashSet

/**
 * Created by Janardhan Shetty on 12/19/16
 * spark-submit --class similarity.ml.CaseSimilarity --master local[12] --driver-memory 8G /Users/jshetty/spark-applications/documentsimilarity/target/scala-2.11/documentsimilarity_2.11-1.0.jar
 */

 object CaseSimilarity {

  val getConcatenated = udf((first: String, second: String) => { first + " " + second })

  //spark.sql.crossJoin.enabled=true

  // Udf
  val jaccard = udf { (set1: Seq[Long], set2: Seq[Long]) =>
    val lSet1 = HashSet() ++ set1
    val lSet2 = HashSet() ++ set2
    val percent = (1.0F * lSet1.intersect(lSet2).size / lSet1.union(lSet2).size) * 100
    percent
  }

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder.master("local[12]").appName("Case Similarity").getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext
    sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val numGrams = 4
    val seed = 32
    val minThreshold = 87.0
    val maxThreshold = 100.0

    // Read as RDD
    //val casePath = "/Users/jshetty/case-similarity/data/3months/support/support.csv"
    val casePath = "/Users/jshetty/case-similarity/data/3months/support/support_201609.csv"
    val resultPath = "/Users/jshetty/case-similarity/results/res1"
    val caseInput = spark.read.option("header", "True").option("delimiter", "^").csv(casePath)

    // Concat subject + description
    val cases = caseInput.withColumn("caseText", getConcatenated(col("SUBJECT"), col("DESCRIPTION")))

    // Tokenize
    val caseTokenizer = new RegexTokenizer().setInputCol("caseText").setOutputCol("caseWords").setPattern("\\W")

    // Approach-1 : 4-gram + MurmurHashing + Jaccard Similarity
    val caseTokGram = new NGram().setInputCol("caseWords").setOutputCol("caseGrams").setN(numGrams)

    // Hashing
    val caseHashing = new Hasher().setInputCol("caseGrams").setOutputCol("caseTokGramHashed").setSeed(seed)

    // Pipeline
    val casePipeline = new Pipeline().setStages(Array(caseTokenizer, caseTokGram, caseHashing))

    // Fit
    val caseFit = casePipeline.fit(cases).transform(cases)

    val caseResult = caseFit.select("ID", "caseTokGramHashed")

    // Cross product
    val caseJoined = caseResult.crossJoin(caseResult)
    // columns renamed:
    val columnsRenamed = Seq("CaseID1", "caseNumber1Hashed", "CaseID2", "caseNumber2Hashed")
    val resultDF = caseJoined.toDF(columnsRenamed: _*)

    // Do from here not complete
    val jaccardDF = resultDF.withColumn("score",jaccard(col("caseNumber1Hashed"), col("caseNumber2Hashed")))

    //jaccardDF.show(5)
    //val result = jaccardDF.select("CaseNumber1", "CaseNumber2", "score").filter(s"score > $threshhold").sort($"score".desc).coalesce(1)
    val result1 = jaccardDF.select("CaseID1", "CaseID2", "score").filter(s"score > $minThreshold")
    val result2 = result1.select("CaseID1", "CaseID2", "score").filter(s"score < $maxThreshold")
    result2.limit(100).write.csv(resultPath)
    sc.stop()

  }
}
