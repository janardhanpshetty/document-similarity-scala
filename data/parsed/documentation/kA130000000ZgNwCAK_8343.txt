### Topic: Search for External Files with Files Connect | Salesforce
Search for External Files with Files Connect | Salesforce
Search for External Files with Files Connect
Search an external data source like Google Drive, Box, or SharePoint right within Salesforce.
Available in: both Salesforce Classic and Lightning Experience
Files Connect for cloud-based external data sources is available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Files Connect for on-premises external data sources is available for an extra cost in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To access cloud-based data sources like SharePoint Online:
“Files Connect Cloud”
To access on-premises data sources like SharePoint 2010:
“Files Connect On-premises”
Search in a Specific External Data Source
In Salesforce Classic
From the left column of Files home or your Chatter feed, click the data source name.
In the search box, enter terms such as 
document title
 or 
author
. (The specific information you can search for depends on the configuration of the external data source.)
Note
You can also search external data sources from the file selector when attaching a file to a Chatter post. In Salesforce Classic, click 
File
 above the feed, and then select a file from Salesforce. In Lightning Experience, click the paperclip icon below the post to open the Select File window.
Search Globally for Salesforce and External Data
If your administrator enables global search for an external data source, you can conveniently search its contents along with your Salesforce data.
In Salesforce Classic
In the global search box at the top of the Salesforce window, enter your search terms.
To filter the results down to a specific external data source, click its name in the left column (for example, “SharePoint Online”).
Tip
If you often want to see content from a specific external data source, pin it to the top of global search results: In the left column, hover over the data source name, and click the pin icon. (If you don’t see the data source listed, click Search All.)
In Lightning Experience
From Files home, enter a search term into the global search box in the header.
You can filter your search results to return only Files by selecting “in Files” after entering your search in the global search box, or by doing a global search and then selecting Files from the global search results.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_files_connect_search.htm&language=en_US
Release
202.14
