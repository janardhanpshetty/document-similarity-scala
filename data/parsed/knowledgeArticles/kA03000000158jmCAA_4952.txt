### Topic: Learn more about the requirements for SalesforceA.
SalesforceA for Android v3.0.0 has been released and does require Android OS 4.4 as a minimum to run. Users trying to access SalesforceA on Spring '16 or higher will need to be running SalesforceA Android v3.0+ at a minimum.
 
Resolution
Considerations with the SalesforceA for Android v3.0.0+
Requires Android OS 4.4 at minimum
If a user is on Spring '16 sandbox or Production and hasn't upgraded to minimum v3.0.0 for SalesforceA for Android, a splash page will provide options to download new version (users will have to upgrade).
 
Heads up
 - A splash screen will instruct the user to AppStore or Google Play store regardless of Android OS version. If a user isn't able to upgrade to v3.0.0 or higher, they'll need to ensure that the Android OS min requirement of 4.4 is met. 
