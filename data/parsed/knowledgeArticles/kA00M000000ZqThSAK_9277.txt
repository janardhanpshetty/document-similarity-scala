### Topic: This article helps how to Change record owner in Lightning Experience.
You can give ownership of a record to another user as long as that user has atleast 
Read 
permission for the type of record being transferred.
Resolution
Switch to
 Lightning Experience
:
User Permission: Transfer Record
1) Go To Object | Open any record | Click on Details page | Go to Record Owner field |
      a) Click on the 
Change Owner Icon 
 besides 
Owner Name:
Tips:
If you don’t see 
Change Owner Icon
, you don’t have 
Profile Permissions
 to 
Transfer Record
.
2. Once you click on Icon you can enter or select a 
New Owner
 name from drop-down list.
3. Select the 
Send Notification Email
 checkbox to notify the 
New Owner
. 
Second method to Change the Ownership on record in Lightning Experience is through Change Owner Button, Please find the below steps:
1) Go To Setup | Setup Home | Object and Fields | Object Manager | Select the Object | Go To Page layout
     a) Select 
Buttons
 on the top left corner.
     b) Check for 
Change Owner Button
 and drag & drop in layout in button section or if the button is 
Grayed
 out, which means               button is already added to your detail page layout.
Change Owner button on object record:
     c) Click on Save.
 
