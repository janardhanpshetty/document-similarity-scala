### Topic: For the latest version please check the Google Doc: https://docs.google.com/presentation/d/188nhu5hTHlOrOzrmDvdE7ewS2Fc74u80Vyitw81I61k/edit#slide=id.g71708ab07_2_20
This Article provides a set of neutral, generic navigation terms that:
Work with existing Salesforce concepts and terms
Work across all form factors -- desktop, console, and mobile
Future-proof the navigation model
 
Please note that the screenshots are from UX designs and don't reflect the final 204 interface. They may be updated in a future version of the Article.
​Visit the Google Doc 
Terminology: Lightning Navigation Model
 to find the latest version/comments of this Document.
Resolution
Core Navigation Elements
Header
:  Region at top of the page that provides global functionality across all services.
Footer
:  Region at bottom of the page. Houses the utility bar for LEX/console and global actions for S1.
Utility bar
:  Displays in footer and provides continual access to tools and utilities for the current app.
 
No formal name for the icons in the LEX/S1 header or the S1 footer. 
We will not use “app tab bar” for S1 to avoid confusion with Salesforce tabs. 
In content, we’ll use the icon images for calls to action (ex: Tap [notifications icon] to…)
Navigation Bar
Navigation Bar / List
:  Container that gives the context for the currently selected app. Renders as a horizontal bar below the header in LEX/console and as a full page list in S1.
 
Items
:  The navigation bar displays the “items” that are included in the current Salesforce app. Users navigate in an app by selecting items.
Keeps the terminology general so it comfortably describes all ‘things’ that may display in the navigation bar, for all services (including Marketing Cloud), across all form factors.
Definition of “item”: an individual unit, especially one that is part of a list, collection, or set.
Items can include: objects, the home page, the feed, tasks, events, all custom tab types (VF page, Lightning page, Web page), mobile features like Today and Forecasts, Marketing Cloud menus, etc.
Narrative for Tabs as Items
Users navigate the current app by selecting items in the navigation bar
Available items depend on the app and can include:  all tab types (objects, home page, Chatter, Visualforce pages, Lightning pages, Web pages), Tasks + Events, mobile features like Today and Forecasts, Marketing Cloud menus
When creating Lightning apps, admins pick the items to include
App customization flow UI text will explain that admins can include tabs as items in an app
We won’t call the entries in the navigation bar “tabs” because that term isn’t relevant for some supported items and some services. Examples:
No tabs for Tasks, Events, Today, mobile Forecasts app.
Marketing Cloud doesn’t have the concept of tabs but will be using the navigation bar to display its menus.
“Item” as an umbrella term keeps the navigation model terminology neutral so it makes sense throughout the Salesforce ecosystem, whether talking about tabs, menus, or something else yet to be integrated.
App Launcher
App Launcher:
 Place to access apps, the AppExchange site, communities, or to open a specific item (such as objects, VF pages, etc).
​After much discussion, we agreed to keep “app” in the name. Majority use case is to launch apps.
Customers are used to the “App Launcher” term. Changing the name doesn’t provide enough value to cause everyone to learn a new term.
We’ll revisit later if we have customer data that customers are confused about accessing individual items from a feature with “app” in the name.
 
Switcher
Switcher
:  UI that allows the user to quickly switch between different usernames or orgs.
This term won’t appear in the LEX/Console UI, but will in the title on the S1 page.
Use this term in content only when absolutely necessary to explain the context. 
Eliminated “account switcher” because of the account object and “org switcher” because that isn’t always the case.
This general name also accommodates the LEX>Classic links.
