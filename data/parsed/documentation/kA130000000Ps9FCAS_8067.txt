### Topic: Flow Screen Element | Salesforce
Flow Screen Element | Salesforce
Flow Screen Element
Displays a screen to the user who is running the flow, which lets you display information to the user or collect information from the user.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Flow Screen Element: General Info
Identifies which navigation buttons are available for a given screen, as well as whether help text is available to the flow user.
Flow Screen Element: User Input Fields
Lets users manually enter information. A 
flow user input field
 is a text box, long text area, number, currency, date, date/time, password, or checkbox in a Screen element.
Flow Screen Element: Choice Fields
Lets users select from a set of choices. A 
flow choice field
 is a radio button, drop-down list, multi-select checkbox, or multi-select picklist in a Screen element.
Flow Screen Element: Display Text Fields
Displays information to users while they’re running the flow.
See Also:
Flow Elements
Define the Path That a Flow Takes
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_designer_elements_screen.htm&language=en_US
Release
202.14
