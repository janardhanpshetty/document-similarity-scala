### Topic: Some customers may observe that they get no results when they search for Account records via the account name itself even though they have access to them
***INTERNAL ONLY***
Some customers may observe that they get no results when they search for Account records via the account name despite them having access to these records.
A typical scenario would look something like this:
User John Doe, attempts to search for Account Record "
Discovery Communications Inc
.
" via global search, he expects to see the record as part of his results but doesn't, he then click on Accounts from the left to narrow his search assuming that he may not be seeing the record due to search crowding, once again, John is unable to see the record.
John (System Admin) then confirms he has access to the record by plugging the ID in the URL, https://nax.salesforce.com/001D000001NSVcI
On the support side (T2/T3), you immediately replicate the issue in the Search Query Tool to confirm the following:
1. Is the customer on SAN/SOLR?
2. Are you able to see the record as part of the results in SQT?
 
You use the following parameters in SQT:
Index ID
 - 00D200000007Yp5 
Query Type
 – UNIFIED_STRUCTURED 
Query
 - Discovery Communications Inc 
Entity Types (Key Prefixes)
 - 001 
Index Location 
- SAN 
Rank    Result 
1    001D000001NSTRZ
- Based on the above,you confirmed that the desired record i.e., 
Discovery Communications Inc
. 
is showing up in the SQT results.
Resolution
The issue we're seeing above happens due to the fact that the 
Parent Account record
 for the associated Account record in question has been either deleted or is in a state where despite being deleted, has a dangling reference, you should see something like this:
The above can be resolved by running the 
tools.scrutiny.generic.ScrutinyForeignKeys
 scrutiny with 
FK Fields
 = 
Account.Parent
[
IMPORTANT
 - Please do not forget to run the above in read-only mode and share the results with the customer, only run in COMMIT when you have the customers approval to do so]
The results of the scrutiny would look something like this:
Organization Id
: 00D200000007Yp5
Options:
 FK: Account.Parent
Maximum quick rows
: 5000 [
The default is 100, so make sure you increase this value else you will not see the complete list of affected records
]
Tasks:
    tools.scrutiny.generic.ScrutinyForeignKeys: Run-only
tools.scrutiny.generic.ScrutinyForeignKeys.Account.Parent.Account: found 1058, did not fix in 0 sec
We can fix this issue by suggesting the following to the customer [
Do not forget to attach the list of id's to the case
]: 
We've identified the list of account records on your end that are still tied to deleted parent account records (the list is attached to this case) and hence will not show up in search unless these records are fixed. 
1. We can run a quick script that would delete the contents of the Parent Account field ( total records that would be corrected are 1058, in this way, you can search for them right away and update the parent account field with an appropriate value. 
2. You could update these 1508 account records with an active value in the Parent Account field i.e., re-parent the account record to an account record that is not deleted. 
3. You could simply remove the Parent Account field from the search layout by going to Setup>App Setup>Customize>Accounts>Search Layouts 
Once you have acquired the approval from the customer, reach out to Tier 3 and have them run the scrutiny in COMMIT mode which would resolve the issue.
