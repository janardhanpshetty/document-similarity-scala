### Topic: Outbound Message Actions | Salesforce
Outbound Message Actions | Salesforce
Outbound Message Actions
An outbound message sends information to a designated endpoint, like an external service. Outbound messages are configured from Setup. You must configure the external endpoint and create a listener for the messages using the SOAP API. You can associate outbound messages with workflow rules, approval processes, or entitlement processes.
Available in: Lightning Experience and Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
For example, automatically initiate the reimbursement process for an approved expense report by triggering an outbound API message to an external HR system.
From Setup, enter 
Outbound Messages
 in the 
Quick Find
 box, and select 
Outbound Messages
. Then use these settings to configure your outbound message.
Field
Description
Object
Choose the object that has the information you want included in the outbound message
Name
Enter a name for this outbound message.
Unique Name
Enter a unique name to refer to this component in the API. The requirement for uniqueness is only within the selected object type. You can have outbound messages with the same unique name, provided they are defined for different objects.
The 
Unique Name
 field can contain only underscores and alphanumeric characters. It must be unique within the selected object type, begin with a letter, not include spaces, not end with an underscore, and not contain two consecutive underscores.
Description
Enter a description that makes it easy for other users to tell what the outbound message does.
Endpoint URL
Enter an endpoint URL for the recipient of the message. Salesforce sends a SOAP message to this endpoint.
User to send as
Select the Salesforce user to use when sending the message. The chosen user controls data visibility for the message that is sent to the endpoint
Send Session ID
Select 
Send Session ID
 if you want the Salesforce session ID included in the outbound message. Include the session ID in your message if you intend to make API calls and you don't want to include a username and password. Never send a username and password in an unencrypted message, especially in a production environment. It isn’t secure.
Priority
Choose a priority for the outbound message.
Account fields to send
Select the fields to include in the outbound message and click 
Add
.
If your endpoint URL uses a client certificate, see 
Import a Client Certificate for Your Endpoint URL
.
See Also:
Track the Delivery Status of an Outbound Message
Considerations for Outbound Messages
SOAP API Developer Guide
Associate Actions with Workflow Rules or Approval Processes
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workflow_managing_outbound_messages.htm&language=en_US
Release
202.14
