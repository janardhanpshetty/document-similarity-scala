### Topic: This article talks about Apex Trigger Support for Chatter Group Records.
Administrators or developers can now use Apex triggers to control record operations in groups in sensitive contexts such as in public 
communities, where you want close control over who adds records to groups. This feature is available in both Lightning Experience and 
Salesforce Classic.
Resolution
