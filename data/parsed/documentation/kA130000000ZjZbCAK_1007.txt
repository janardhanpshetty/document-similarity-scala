### Topic: Enable Feed Tracking for Coaching | Salesforce
Enable Feed Tracking for Coaching | Salesforce
Enable Feed Tracking for Coaching
Track updates for coaching spaces, so users can receive notifications about important changes.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To customize fields tracked in feeds:
“Customize Application”
Determine which coaching updates should notify people and select the associated fields for tracking.
From Setup, enter 
Feed Tracking
 in the 
Quick Find
 box, then select 
Feed Tracking
.
Click on the Coaching object.
Click the 
Enable Feed Tracking
 checkbox.
Select the fields you would like to track.
Tip
For coaching, it’s helpful to track Inactive and All Related Objects.
Click 
Save
.
See Also:
Goals and Coaching Features
Enable Feed Tracking for Goals
Enable History Tracking for Goal Fields
Enable History Tracking for Coaching Fields
Configure Key Company Goals
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workcom_impl_enable_feed_tracking_coaching.htm&language=en_US
Release
202.14
