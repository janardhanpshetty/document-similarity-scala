### Topic: Creating and Using CSS Style Sheets | Salesforce
Creating and Using CSS Style Sheets | Salesforce
Creating and Using CSS Style Sheets
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To build, edit, and manage Site.com sites:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
A default style sheet called “Site Style Sheet” is included with every site you create. However, if you're familiar with CSS and need multiple style sheets, you can create new ones to use in your site.
To create a style sheet:
Click 
Style Sheets
 | 
New
 on the Overview tab. Alternatively, click 
New Style Sheet
 in the Style Sheets view.
Enter a name for the style sheet.
Click 
Apply
. The style sheet opens.
Add style items and groups
 to the style sheet.
Note
Style sheet names can only contain alphanumeric characters, hyphens, colons, and underscores.
You can also 
import a CSS file
 to use in your site.
After you create a new style sheet, you must attach it to a page to apply its styles to the page.
To attach a style sheet to a page:
Select the page in the Page Structure pane (
).
In the Style Sheets section of the Properties pane (
), click 
.
Select the style sheet in the list that appears.
Attach the style sheet to the page by clicking 
 beside the drop-down list.
Tip
If you used a 
page template
 to create your site pages, the quickest way to include the new style sheet on every pages is to attach it to the template. This automatically includes a reference to the style sheet in every page that's based on the template.
See Also:
Understanding the Style Sheet View in Site.com
Cascading Style Sheets Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_style_css_create_sheet.htm&language=en_US
Release
202.14
