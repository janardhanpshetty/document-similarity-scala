### Topic: Cloud-Based Lightning for Outlook | Salesforce
Cloud-Based Lightning for Outlook | Salesforce
Cloud-Based Lightning for Outlook
Stay on top of important sales opportunities when you work in Microsoft® Outlook®. When using Outlook Web App (OWA), Outlook 2016, or Outlook 2013 along with Microsoft Office 365™, manage your sales more efficiently. Relate email and events along with their attachments to Salesforce records—all without installing and maintaining software. And sync contacts and events between your email applications and Salesforce using Lightning Sync.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Also, create Salesforce contacts, events, leads, opportunities, tasks, and cases—directly in Outlook.
Lightning for Outlook
Stay on top of important sales opportunities when you work in Outlook. When using Outlook® Web App (OWA), Outlook 2016, or Outlook 2013 along with Microsoft Office 365™, you can manage your sales more efficiently. Relate email and attachments to Salesforce records. And, create Salesforce records—directly in Outlook.
Lightning Sync Keeps Your Microsoft® Items in Sync with Salesforce
Keep your contacts and events in sync between your email system and Salesforce without installing and maintaining software.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=app_for_outlook_user_product_area.htm&language=en_US
Release
202.14
