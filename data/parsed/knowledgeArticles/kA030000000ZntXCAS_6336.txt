### Topic: This article explains that blob datatype is not supported for remote objects.
You may face errors given below on using the BLOB datatype for remote Objects.
- 
"Body: value not of required type" : On inserting attachment/document or any object where would you need to provide BLOB datatype. 
- "
Error parsing json response: 'missing ) after argument list'. Logged in?" : On querying the field of BLOB type using Remote Objects.
Sample code : Create a VF page having code given below
 
<apex:page > 
	<apex:remoteObjects jsNamespace="myattach">
		<apex:remoteObjectModel name="Attachment"  fields="id, name, body ">
		</apex:remoteObjectModel>
	</apex:remoteObjects>
	<script> 
		// query attachment
		// Id "00P2000000bNDV4" is hardcoded you need to replace this attachment id with a valid attachment id for your org. 
		var ct = new myattach.Attachment();
		  ct.retrieve( 
		    { "where": { "id": {"eq": "00P2000000bNDV4"}} },
		 function(err, records) { 
		        if (err) { 
		        alert(err);
		        } 
		        else {
		             alert("success");
		        } 
		    } 
		);
	</script> 
</apex:page>
 
- Access this page over browser, you should see a JS alert saying "Error parsing json response: 'missing ) after argument list'. Logged in?"
Resolution
In above code, we are querying Body (fields="id, name, body") which is blob type and that is why we are getting error. Currently, 
Blob datatype is not supported for Remote Object.
 
