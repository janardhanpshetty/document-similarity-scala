### Topic: A user may receive an 'insufficient privileges' message when attempting to edit an asset although they have 'Edit' permission on assets. This occurs when the product related to the asset has been made inactive.
Why am I receiving an insufficient privileges message when editing an asset?
Resolution
A user may receive an 'insufficient privileges' message when attempting to edit an asset although they have 'Edit' permission on assets. This occurs when the product related to the asset has been made inactive. To remedy this, you can:
1. Get the product activated.
2. Change profile to have Edit permissions on products.
