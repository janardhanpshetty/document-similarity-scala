### Topic: The new Lightning Experience GUI must be customized to include the ability to send emails from a case.
The new Lightning Experience GUI must be customized to include the ability to send emails from a case. This article describes the steps to follow in order to add a Send Email button to the Case page layout
 
Resolution
1.- From the management settings for the Case object, go to "Buttons, Links, and Actions" 
2.- Click on the "New Action" button
3.- In the "Action Type" dropdown list, select "Send Email"
4.- In the "Standard Label Type" dropdown list, select "Send Email"
5.- In the "Name" text box, enter the name of your button. Do not use blank spaces in the In the "Target Object" dropdown list, select "Case"
6.- Optionally, enter a description for your button
7.- Click on "Save" to save all your changes
You will be automatically taken to the Action Layout Editor. Use it to add the fields that your users will have access to when composing an email (eg, CC, BCC). Once the button is created and the layout is set up, you need to add the button to your page layout(s)
8.- From the management settings for the Case object, go to "Case Page Layouts"
9.- Click on the page layout you want to add the button to
10.- In the palette at the top of the page layout editor, click on "Salesforce 1 Actions" category
11.- Drag the button you previously created and drop it in the "Salesforce1 and Lightning Experience Actions" section of your page layout
12.- Save your changes
Repeat steps 8 through 12 for all the page layouts in your org. 
