### Topic: Upload and Publish Content | Salesforce
Upload and Publish Content | Salesforce
Upload and Publish Content
Add files to libraries to take advantage of the permission setting and content delivery capabilities of Salesforce CRM Content.
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To upload and publish files and Web links:
Manage Libraries
 checked in your library permission definition
OR
Add Content
 checked in your library permission definition
To publish Google docs:
Google Apps account
The Libraries tab has several publishing-related options at the top of the page that let you upload, classify, and publish files, content packs, Web links, and Google docs in Salesforce CRM Content.
Note
The 
Add Google Doc
 button on the 
Libraries
 tab is available only if your Salesforce admin has enabled the Add Google Doc to Salesforce service.
To publish files, Web links, and Google docs in Salesforce CRM Content, or to create content packs, refer to the following topics:
Publish Files to Libraries
Contribute Web Links to Salesforce CRM Content
Create and Modify Content Packs in Salesforce CRM Content
See Also:
Update Content Versions
Creating Content Deliveries
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=content_upload.htm&language=en_US
Release
202.14
