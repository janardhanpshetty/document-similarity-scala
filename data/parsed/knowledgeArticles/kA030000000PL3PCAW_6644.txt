### Topic: On the Contacts Tab, we would like to display custom reports that we have created in the "Reports" section. How can we configure which reports are displayed in the "Reports" section of the Contacts tab?
On the Contacts Tab, we would like to display custom reports that we have created in the "Reports" section. How can we configure which reports are displayed in the "Reports" section of the Contacts tab?
Resolution
The Reports Section for an Object's Tab may not be customized without the use of custom code such as Visualforce.
 
There is an idea on our IdeaExchange around this:
Customize Standard Object Tab Reports (and add for custom objects)
https://success.salesforce.com/ideaView?id=08730000000BpjwAAC
 
A couple other options would be to link to the Reports in the "Custom Links" section on the Page Layout for Records, or add them to the Sidebar.
