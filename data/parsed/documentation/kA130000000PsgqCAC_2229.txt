### Topic: Define Data Sets for Salesforce for Outlook | Salesforce
Define Data Sets for Salesforce for Outlook | Salesforce
Define Data Sets for Salesforce for Outlook
This feature available to manage from: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To view configurations:
“View Setup and Configuration”
To create, edit, or delete configurations:
“Manage Email Client Configurations”
To create, edit, or delete data sets:
Sync Direction can’t be 
Don’t sync
Data sets are subsets of the Salesforce records that Salesforce for Outlook users can sync. They consist of filters that limit which records sync.
 Each configuration must have a data set in order for users to sync with Outlook.
The Outlook Configuration detail page shows a summary of the configuration’s current filters.
Note
If your company uses Platform Encryption, you can’t set filters on fields you’ve encrypted. Otherwise, Salesforce for Outlook can’t sync contacts, events, or tasks for users assigned to that configuration.
From Setup, enter 
Outlook Configurations
 in the 
Quick Find
 box, then select 
Outlook Configurations
.
Click the name of an Outlook configuration.
In the Data Sets related list, click 
Edit
.
Specify filters.
To see how many items sync with the filters you’ve specified, you can check the data set size.
Click 
Save
.
See Also:
Sample Lightning Sync and Salesforce for Outlook Data Sets
Salesforce for Outlook Contact Filters
Outlook Configurations and Outlook Publisher Layouts
Check the Size of Salesforce for Outlook Data Sets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=outlookcrm_config_def_data_sets.htm&language=en_US
Release
202.14
