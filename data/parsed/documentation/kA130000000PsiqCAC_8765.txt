### Topic: Monitor Pending Workflow Actions | Salesforce
Monitor Pending Workflow Actions | Salesforce
Monitor Pending Workflow Actions
When a workflow rule that has time-dependent actions is triggered, use the workflow queue to view pending actions and cancel them if necessary.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Workflow tasks and email alerts are not available in 
Database.com
User Permissions Needed
To manage the workflow queue:
“Modify All Data”
From Setup, enter 
Time-Based Workflow
 in the 
Quick Find
 box, then select 
Time-Based Workflow
.
Click 
Search
 to view all pending actions for any active workflow rules, or set the filter criteria and click 
Search
 to view only the pending actions that match the criteria. The filter options are:
Workflow Rule Name
: The name of the workflow rule.
Object
: The object that triggered the workflow rule. Enter the object name in the singular form.
Scheduled Date
: The date the pending actions are scheduled to occur.
Create Date
: The date the record that triggered the workflow was created.
Created By
: The user who created the record that triggered the workflow rule.
Record Name
: The name of the record that triggered the workflow rule.
The filter is not case-sensitive.
To cancel pending actions:
Select the box next to the pending actions you want to cancel.
Click 
Delete
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workflow_queue.htm&language=en_US
Release
202.14
