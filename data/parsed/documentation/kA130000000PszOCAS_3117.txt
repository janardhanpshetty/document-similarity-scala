### Topic: Override Translations in Managed Packages | Salesforce
Override Translations in Managed Packages | Salesforce
Override Translations in Managed Packages
Although you can't edit labels or translations in a managed package, they are controlled by the developer, you can override them with the Translation Workbench. For example, override a custom field in a package.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To override terms:
“View Setup and Configuration”
AND
“Customize Application”
From Setup, enter 
Override
 in the 
Quick Find
 box, then select 
Override
.
Select the 
Package
 that you’re overriding.
Select the 
Language
 that you're entering your overrides in.
Select a 
Setup Component
. Click the pull-down menu to select from the list of translatable customizations. See 
Translatable Customizations
 for a complete list of possible customizations.
If necessary select an object and aspect. For example, workflow tasks have an object (Account, Contact, etc.) and aspect (Subject or Comment).
Double click in the override column to enter new values. You can press TAB to advance to the next editable field or SHIFT-TAB to go to the previous editable field.
Note
The 
Out of Date
 column indicates that the item has been updated and the term may need to be changed. When editing a button or link label, you see the 
Button or Link Name
 column, which is used to refer to the component when using the SOAP API.
Double click in the translation column to enter new values. You can press TAB to advance to the next editable field or SHIFT-TAB to go to the previous editable field.
Note
The 
Out of Date
 column indicates that the item has been updated and the term may need translating. When editing a button or link label, you see the 
Button or Link Name
 column, which is used to refer to the component when using the SOAP API.
Click 
Save
.
See Also:
Enable and Disable the Translation Workbench
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=entering_translated_terms_in_packages.htm&language=en_US
Release
202.14
