### Topic: Creating Style Sheet Items and Groups | Salesforce
Creating Style Sheet Items and Groups | Salesforce
Creating Style Sheet Items and Groups
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To build, edit, and manage Site.com sites:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
When adding style items to style sheets, you can define CSS classes and IDs, or you can redefine the formatting of HTML tags such as 
body
 or 
h1
. When you change the CSS style of an HTML tag, anything formatted with that tag is immediately updated.
Creating Style Items
To open a style sheet, double-click it in the Style Sheets view of the Overview tab, or hover over it and click 
| 
Edit
.
If you're very familiar with CSS and prefer coding by hand, click 
Edit Style Sheet Code
 to edit the style sheet directly using the CSS editor. Additionally, to add at-rules (for example, 
@media
), you must edit the style sheet directly.
Alternatively:
Select the style sheet and click 
| 
Insert Style Item
.
Enter the name of the style item:
To redefine the default formatting of a specific HTML tag, enter the HTML tag name—for example, 
body
 or 
h1
.
To create a CSS class, enter the class name and ensure that you include a period before it—for example, 
.classname
.
To create a CSS ID, enter the ID name preceded by 
#
—for example, 
#contentID
.
Click 
Apply
.
Add style definitions by either:
Setting 
style properties
 in the visual style editor on the right
Typing CSS styles in the text box in the Style Preview section and clicking 
Save
As you modify the definition of a selected style item, you can see how your changes appear in the Style Preview section.
Tip
A class name must begin with a period or it will not be recognized as a CSS class.
An ID name must begin with 
#
 or it will not be recognized as a CSS ID.
Use IDs when there is only one occurrence per page. Once you've used the ID, you can't use it again on that page. Use classes when there are one or more occurrences per page.
Class and ID names can contain alphanumeric characters, hyphens, and underscores only, and can't begin with a number or include spaces.
Creating Style Groups
Use groups to organize your CSS logically. This makes it easier to locate and maintain styles.
When the style sheet is open:
Select the style sheet and click 
| 
Insert Style Group
.
Enter a name for the group and click 
Apply
.
To add a new style to the group, select the group and click 
| 
Insert Style Item
. To add an existing style to the group, drag it onto the folder icon.
Assigning Style Items
After you've created styles, you can assign them to the pages and pages elements of your site.
To assign a class to a page or page element, select it and either:
Type the class name in the 
Class
 field of the Properties pane (
).
Select 
Class
 in the Style pane (
), start typing the name, and select it in the list that appears.
To assign an ID to a page or page element, either:
Type the ID name in the 
ID
 field in the Properties pane (
).
Select 
ID
 in the Style pane (
) and select it in the drop-down list.
See Also:
Creating and Using CSS Style Sheets
Understanding the Style Sheet View in Site.com
Cascading Style Sheets Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_style_css_create_rule.htm&language=en_US
Release
202.14
