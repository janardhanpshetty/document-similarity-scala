### Topic: Prerequisites | Salesforce
Prerequisites | Salesforce
Prerequisites
Plan out your implementation before enabling Work.com in your organization.
Available in: Salesforce Classic
Work.com
 is available as an add-on license for 
Professional Edition
, 
Enterprise Edition
, 
Unlimited Edition
, or 
Developer Edition
, and is included in 
Performance Edition
.
User Permissions Needed
To enable Work.com features:
“Customize Application”
To assign permission sets:
“Assign Permission Sets”
To assign profiles:
“Manage Users”
To set field-level security:
“Manage Profiles and Permission Sets”
AND
“Customize Application”
To view the Calibration tab:
“Enable Work.com Calibration”
Administrators should make the decisions outlined in the 
Business Decision Checklist
 before enabling Work.com.
Before beginning the implementation process, Salesforce must enable Work.com permissions and provision Work.com licenses for your organization. Your Salesforce contact will coordinate this with you, but you can check if your organization has available Work.com licenses. In Setup, enter 
Company Information
 in the 
Quick Find
 box, then select 
Company Information
, and check for Work.com User under 
Feature Licenses
.
Note
It is recommended that you enable Chatter regardless of the edition your organization is using. Many Work.com features use the Chatter feed to notify and interact with users. To confirm Chatter is enabled, from Setup, enter 
Chatter Settings
 in the 
Quick Find
 box, then select 
Chatter Settings
, and verify that Chatter Settings is enabled.
See Also:
Assign a Work.com Administrator
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workcom_impl_prereqs.htm&language=en_US
Release
202.14
