### Topic: This article discussion what actions must be taken when a customer claims they have vulnerabilities in their organization:
If a customer claims to have discovered vulnerabilities in their organization, these types of concerns must be handled first by the 
Salesforce Internal Security Skill group
. If you are not in the security skill group and encounter a case where a customer is claiming that Salesforce has vulnerabilities in the platform, please do the following:
                 
Click on the Edit Button at the top of the page for the case object.
Scroll down to Case Reason, double click on the field.
In the General Application Area select Security.
In the Functional Area select the Phishing/Vulnerability selection. Like Below:
Scroll down until you locate the optional category.
Check the Assign using active assignment rules.
Click on the Save button.
If you're not a Security team please @mention Brian Estebez and Gabriel Nitu on the case so it can be reviewed to determine the best approach in processing the case.
Resolution
