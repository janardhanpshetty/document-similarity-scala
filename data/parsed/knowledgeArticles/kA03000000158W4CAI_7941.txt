### Topic: End users are trying to get Facebook conversations either manually (via “Get Conversations”) or Scheduled Searches, but request fails with the following error: “Facebook returned the error: (#4) Application request limit reached, Code Line:437”
This internal article only pertains to AppExchange App 
Salesforce for Twitter and Facebook
. (Also called “Salesforce for Social Media”)
End users are trying to get Facebook conversations either manually (via “Get Conversations”) or Scheduled Searches, but request fails with the following error:
“Facebook returned the error: (#4) Application request limit reached, Code Line:437”
Resolution
T1/T2/T3 support can share the following workaround:
1.  Go to “Salesforce for Social Media” App
2.  Click on “Social Setup” tab
3.  Left menu, click on “Advanced”
4.  Edit and paste App ID and App Secret in correspondent fields (Custom Facebook App section)
App ID
515697881794015
App Secret
5cb761ef2830446e53060688e54168c1
5.  Save
6.  Left menu, click on “Facebook”.  Click on one Facebook page, click on “Revoke Facebook Access”.  Repeat step 5 to revoke all Facebook pages with problem.  
7.  Click on “Social Setup” tab, left menu, click on “Facebook”. Click on one Facebook user.  Click on “Grant Facebook Access” button for Facebook Page user and validate the user and the pages with CRM2.  Repeat step 6 for all Facebook users.
 
