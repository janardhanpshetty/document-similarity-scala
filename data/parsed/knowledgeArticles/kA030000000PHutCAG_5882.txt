### Topic: I will like to know how to create a mail merge template (a step by step process) with Connect for Office.
How to create a mail merge template (a step by step process) with Connect for Office.
Resolution
To create mail merge templates:
 
1.        Open a new blank document in Microsoft Word 2002, 2003, or 2007.
2.        Use Word to compose the document so it contains the text, data, and formatting you require.
3.        Locate the text or data variables in your Word document that will change each time the template is used. For example, in the salutation “Dear Bob,” “Bob” is a variable because it will change when the letter is sent to a different person.
4.        Replace each variable with the appropriate Salesforce merge field.
Important: Each mail merge field label you use must be unique.
o    If you have Connect for Office, you can 
use Word integration
 
to insert merge fields quickly.
o    If you do not have Connect for Office:
a.        Place your cursor where you want to insert a merge field.
b.        In Word 2003 and earlier, select Insert and then Field from the Word menu bar. In Word 2007, select the Insert tab on the Ribbon, click Quick Parts in the Text group, and then click Field.
c.        Select Mail Merge in the Categories drop-down list.
d.        Select MergeField in the Field names box.
e.        In the Field Properties area of the Field name box, manually enter the 
valid Salesforce merge field
, such as Opportunity_LineItem_ProductName.
f.         Click OK.
Note:
To list information about all products associated with an opportunity, insert the Opportunity_LineItem_Start merge field where you want to begin listing product information. Then, insert all the merge fields you want to include for each product on an opportunity. Finally, insert the Opportunity_LineItem_End to end the list.
5.        Save your Word document.
6.        Proceed to 
Uploading Mail Merge Templates to Salesforce
.
The syntax for these merge fields is 
OBJECT
_
FIELD_NAME
 or 
FIELD_NAME
. Merge fields for mail merge templates must:
Be unique
Contain only letters, numbers, and the underscore (_) character
Not exceed 40 characters
For example: AccountNumber. To ensure you’re using the correct syntax, use Connect for Office to insert merge fields into your mail merge template.
To Upload the document:
1.        In Salesforce, click 
Your Name
 | Setup | Communication Templates | Mail Merge Templates.
2.        Click New Template.
3.        Enter a name and description for the template. This will help users correctly choose a template when generating mail merge documents.
If your organization uses Extended Mail Merge, choose the appropriate document type: Document, Label, or Envelope. Extended Mail Merge is available by request only. 
Contact salesforce.com Customer Support
 if you are interested in this feature.
4.        Click Browse to select your Word mail merge template.
5.        Click Save to finish.
Then go to the contact/account/lead and under Activity History click on mail merge then Generate.
