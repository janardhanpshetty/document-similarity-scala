### Topic: Tips for Effective Support Reporting | Salesforce
Tips for Effective Support Reporting | Salesforce
Tips for Effective Support Reporting
You can get a lot of useful information out of your cases and solutions data if you keep a few tips and best practices in mind.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions except 
Database.com
 (The edition determines which reports you see.)
When reporting on cases, add the 
Parent Case Number
 field to your report. This field indicates if a case is associated with a parent case.
When reporting on first-call resolution of cases, add the 
Closed When Created
 field to your report. This field indicates cases that were closed by support reps via the 
Save & Close
 button during the creation of the case.
You can create a case report containing contact email addresses, export that data to Excel, and then do a mass mail merge using Microsoft Word.
Standard Report Types
Choose the Translated Solutions report to summarize the translated solutions associated with each master solution.
Choose the Contact Role report to show all cases with their associated contact roles.
Choose the Cases with Articles report to see the articles attached to cases. This report is only available if Salesforce Knowledge is enabled.
The report displays articles even if they're not marked as available for the internal app channel.
Custom Report Types
You can create a custom report to view a list of cases with milestones by choosing the Cases with Milestones report type. This report type is only available if entitlements is enabled.
Choose the Case History and Solution History report types to track the history of standard and custom fields on cases and solutions where field histories are set up for tracking. Use these reports to see tracked fields' old and new values. You can't use filter conditions to search the results of the 
Old Value
 and 
New Value
 fields.
You can create a custom report to view a list of both inbound and outbound emails by case by choosing the Cases and Emails report type. This type of report is only available to organizations with Email-to-Case or On-Demand Email-to-Case enabled.
You can run case lifecycle reports to view the results of the 
Range
 field, which indicates the length of time since the case last changed status or owner. Each time the status or owner changes, the counter begins again at zero.
Cases in Portals
If you have enabled the Self-Service portal, you can run reports to track usage of your Self-Service portal.
When reporting on case comments, use the 
Public Case Commented
 field to indicate if the comment is private or public. Public comments are indicated with a check mark. To limit report results to public comments, customize the report and add a field filter where 
Public Case Commented equals True
. Likewise, the filter 
Public Case Commented equals 0
 yields only private case comments.
Choose the 
Closed by Self-Service User
 field to report on how many cases have been closed by users via suggested solutions on the Self-Service portal.
Solution Categories
Create a custom report that sorts solutions by category. Select the 
Category Name
 field to display the solution’s category and the 
Parent Category Name
 field to display the category directly above the solution’s category.
If you restrict your report to solutions in a particular category, the report includes only solutions that are directly associated with that category. It does not include solutions in subcategories of the specified category.
To report on uncategorized solutions, use the advanced report filters. Choose the 
Category Name
 field and the “equals” operator, and leave the third field blank.
Team Members
You can report on case teams in which you are a member. After you run a case report, select 
My case team's cases
 from the 
Show
 drop-down.
Owner Role
 for case reports is defined differently than for other objects. For most objects, 
Owner Role
 is defined in the 
Role Name as displayed on reports
 field on the user’s role. Cases uses the 
Label
 field instead.
You can limit any case report to cases owned by users or cases in queues. Choose User Owned Cases or Queue Owned Cases from the View drop-down at the top of a case report.
See Also:
Using Custom Report Types to Report on Support Activity
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_support_tips.htm&language=en_US
Release
202.14
