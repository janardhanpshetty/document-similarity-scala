### Topic: What is required to turn on Personalized URLS?
What is required to turn on Personalized URLS?
Resolution
Global Support may turn this business rule on, and it's free as long as the client has already purchased Microsites/Landing Pages and a Private Domain for Landing Pages/Microsites. They can use a Private Domain purchased specifically for Landing Pages, or, if they have SAP, they can use the private domain already purchased through SAP. This must be confirmed on their current contract/quote. 
The client must have a Private Domain to activate and use PURLs. 
Once a Personalized URL is used, it may not be reused again. Each PURL must be unique. 
PURLs may take a friendly URL and target the URL to a specific subscriber based on the unique subscriber key. 
