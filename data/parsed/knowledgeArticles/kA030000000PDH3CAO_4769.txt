### Topic: Some related list are not visible to portal user unless they contain data, eg: Emails and Activity History to the Case Page Layout in Customer Portal; opportunity related list on account page layout in partner portal
I have added the Emails and Activity History to the Case Page Layout for my Customer Portal, however these related lists are not visible in the Portal.
Or
I have added the opportunity related list to account page layout for my partner portal, however it is not visible in the portal.
Resolution
This is an expected behavior.
1. When these related lists do not include any data that is visible to a Customer Portal User (for example, if there are no Emails related to a Case, or no completed Activities which have been marked as Public), the related lists will not be displayed to the Customer Portal User.
2. When there is no opportunity that is visible to a Partner Portal User related to the account, the related list will not be displayed to the partner portal user.
 
