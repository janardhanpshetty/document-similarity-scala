### Topic: Can I Stop a User From Creating Chatter Groups? | Salesforce
Can I Stop a User From Creating Chatter Groups? | Salesforce
Can I Stop a User From Creating Chatter Groups?
If there are users that shouldn’t be able to create groups, you can prevent them from doing so.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
The ability to create groups (public, private, or unlisted) is controlled by the permission “Create and Own New Chatter Groups.” The permission is enabled by default and administrators can disable it on the appropriate users’ profiles.
After an administrator disables this permission, a user can no longer create or be assigned as owner of new Chatter groups. If the user was already the owner of any groups, that ownership is not revoked.
See Also:
Edit Profiles in the Original Profile Interface
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_faq_groups_can_i_stop_users_creating.htm&language=en_US
Release
202.14
