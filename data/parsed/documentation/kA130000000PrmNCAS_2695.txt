### Topic: Partner Portals Overview | Salesforce
Partner Portals Overview | Salesforce
Partner Portals Overview
You can create partner portals to help you better manage your partners.
The following table briefly describes the partner portal:
Partner Portal
Purpose
Provides your business partners access to Salesforce data that you have made available to them
User Interface
Includes a highly customizable user interface using a point-and-click editor, and functionality similar to Salesforce, such as: permissions, custom objects, sharing rules, and Web tabs
Supported Record Types
Accounts, contacts, documents, ideas, leads, opportunities, solutions, and custom objects
Quantity
Contact Salesforce for more information
Administrator Controls
Administrators can:
Generate partner portal usernames and passwords
Manage the information of partner portal users
Manage partner portal users using permissions, roles, and sharing rules
User Controls
Partner users can update their own user information
See Also:
Partner Portal Overview
Enable the Partner Portal
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_portals_what_partner.htm&language=en_US
Release
202.14
