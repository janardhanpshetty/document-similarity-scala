### Topic: Set Up Apex Triggers for Flagging Items | Salesforce
Set Up Apex Triggers for Flagging Items | Salesforce
Set Up Apex Triggers for Flagging Items
Use triggers to create custom advanced moderation logic that automatically flags items in your community.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create triggers:
“Modify All Data”
Tip
Did you know you can do this in the UI? Most communities don’t need custom moderation triggers. You can create moderation rules and criteria directly in Community Management. For more information, see 
Community Moderation Rules
.
Using triggers to automatically flag items allows you to moderate your community behind the scenes. These flags are 
only
 visible to moderators. You can view flags in Community Management, query for them in the API, or use custom report types to create reports on flagged items, people whose items are flagged most, and more.
Consider the following when creating triggers:
Create Apex after insert triggers on either FeedItem, FeedComment, ChatterMessage, or ContentDocument.
Define criteria that when met creates a NetworkModeration (flag) record, with the FeedComment, FeedItem, ChatterMessage, or ContentDocument as the parent.
Example
This trigger automatically flags posts in your community that contain 
BadWord
.
trigger autoflagBadWord on FeedItem (after insert) {
    for (FeedItem rec : trigger.new) {
        if (!
<CommunityId>
.equals(rec.networkScope))
            continue;

        if (rec.body.indexOf('BadWord') >= 0) {
            NetworkModeration nm = new NetworkModeration(entityId=rec.id, visibility='ModeratorsOnly');
            insert(nm);
        }
    }
}
A similar trigger on comments would look like this.
trigger autoflagBadWord on FeedComment (after insert) {
    for (FeedComment rec : trigger.new) {
        if (!
<CommunityId>
.equals(rec.networkScope))
            continue;

        if (rec.commentBody.indexOf('BadWord') >= 0) {
            NetworkModeration nm = new NetworkModeration(entityId=rec.id, visibility='ModeratorsOnly');
            insert(nm);
        }
    }
}
See Also:
SOAP API Developer Guide
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_moderation_flag_triggers.htm&language=en_US
Release
202.14
