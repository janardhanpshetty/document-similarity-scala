### Topic: General Chatter Limits | Salesforce
General Chatter Limits | Salesforce
General Chatter Limits
Limits for Chatter features by edition, browser limits, and mention limits.
Chatter Limits by Feature
The limits are subject to change. Contact Salesforce for more information.
Feature
Limit
Groups
Groups a user can join
300
1
Groups in your org
30,000
1
Members in a group
Unlimited
Characters in the Information field in a group
4,000
Max size of email when posting to a group by email (including text and attachments)
25 MB
Attachments you can include in a post to a group by email
25
Feeds
Mentions in a single post or comment
25
Characters in a mention search string
500
 for a person’s first and last name (combined), or for a group name
Characters in single post or comment
10,000
Email notifications sent after you like or comment on a post
10
Bookmarks
Unlimited
Favorites
50
People, topics, and records you can follow
500
2
Maximum posts you can see in your feed at any given time
The 
500
 most recent posts. This limit doesn’t apply to profile, group, and record feeds.
Files
File attachment size
2 GB
Times a file can be shared
100
People and Profiles
Profile photo size
8 MB
People you can add to a chat
10
People you can add to Chat My Favorites
100
Characters in a Chatter message
10,000
Skills
Skills assigned to a user
100
Characters in a skill name
99
Topics
Topics on a single post
10
Topics on a single record
100
Characters in a topic name
99
1
All groups count toward this limit, except archived groups. For example, if you're a member of 300 groups, of which 10 are archived, you can join 10 more groups.
2
You can follow a maximum combined total of 500 people, topics, and records. To see how many items you’re following, view the Following list on your profile.
Posts and comments that users make in Chatter are retained during the entire time that a customer's applicable org remains provisioned. We reserve the right to enforce limits on:
The number of system-generated tracked feed updates. Currently tracked feed updates that are older than 45 days and have no likes or comments are deleted automatically. The tracked feed updates no longer appear in the feed. However, if auditing is enabled for a tracked field, the audit history for that field is still available.
The number of email notifications that can be sent per org per hour.
Browser Limits for Chatter
Some third-party Web browser plug-ins and extensions can interfere with the functionality of Chatter. If you experience malfunctions or inconsistent behavior with Chatter, disable the Web browser's plug-ins and extensions and try again.
Chatter Mentions Limits
You can’t mention archived groups, unlisted groups, customer groups, and private groups you’re not a member of. If a person who isn’t a member of a private group is mentioned in a post or comment on that private group, the mention link displays as gray, unless this mentioned person has “Modify All Data” permission. In this case the link displays in blue. However, the Salesforce security and sharing rules still apply. People who aren’t a member of a private group don’t have access to the private group and won’t see or get notified about any updates.
See Also:
Approvals Limits in Chatter
Chatter Plus Limits
File Limits in Chatter
Search Limits for Files in Chatter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_collaboration.htm&language=en_US
Release
202.14
