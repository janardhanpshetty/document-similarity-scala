### Topic: This article provides a way to keep the salesforce session alive without clicking/making any action on salesforce manually.
INTERNAL DOCUMENT WITH CODE.
In case we have a canvas/iframe being used in salesforce where there is no action being made with salesforce. Instead, the actions are made in canvas/iframe.
In this scenario, the VF page would remain intact without any action and the salesforce session would timeout.
Is there a way to keep the salesforce session alive?
Resolution
We tested several things and applied AJAX action calls. We eventually found that any action made manually (by clicking on page) is considered an action, however, if a java script calls the method, it is not counted against the session maintenance.
Finally, I was able to come up with VF remote method implementation which helped with the issue. We were able to maintain the session without refreshing the page (session setting in Org is set to 15 minutes):
VF Page:
<apex:page controller="SessMgtCont" showHeader="false"> 
<script> 
window.onload = timeshow(); 
function rmfcall(){ 
Visualforce.remoting.Manager.i
nvokeAction( 
'{!$RemoteAction.SessMgtCont.m
yvfrmfn}', 
function(result, event){ 
if (event.status) { 
document.getElementById('{!$Co
mponent.frm.otf}').innerHTML=r
esult; 
window.document.getElementById
('mmtimer').innerHTML=0; 
} 
}, 
{escape : true} 
); //end of remote call 
} 
function timeshow(){ 
if(window.document.getElementB
yId('mmtimer') != null){ 
window.document.getElementById
('mmtimer').innerHTML=Number.p
arseInt(window.document.getEle
mentById('mmtimer').innerHTML)
+1; 
if(window.document.getElementB
yId('mmtimer').innerHTML == "600") 
rmfcall(); 
} 
} 
window.onload= beginloadfn(); 
function beginloadfn(){ 
window.setInterval(timeshow, 1000); 
} 
</script> 
<div id="mmtimer">0</div> 
<apex:form id="frm"> 
<apex:outputText id="otf"> </apex:outputText> 
<apex:commandButton onclick="rmfcall()" rerender="otf" value="Click to make a remote call."/> 
</apex:form> 
</apex:page> 
Controller: 
global class SessMgtCont { 
@RemoteAction 
global static string myvfrmfn(){ 
return 'done'; 
} 
} 
This code makes a VF remote call every 10 minutes. Thus, the session is maintained.
As per salesforce session documentation, this call needs to me made after the half time of session setting. This means, if the session timeout is set to 15 minutes, the call is to be made after 8 minutes. any call made before 8 minutes will not be considered to be counted against the salesforce session refresh.
Following chatter thread can be referred for further discussion:
https://org62.my.salesforce.com/0D53000001Wgjwp
NOTE: This code is just for sample purpose. This is not a code which is absolute and will not be supported.
 
