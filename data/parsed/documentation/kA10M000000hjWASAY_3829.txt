### Topic: Use the Metadata API for Wave Assets | Salesforce
Use the Metadata API for Wave Assets | Salesforce
Use the Metadata API for Wave Assets
Wave provides full support for the Metadata API, which can be used to retrieve, deploy, create, update, or delete customizations for your organization.
Every time your Salesforce organization is customized, its metadata is modified. Editing page layouts, creating a custom field, or adding a Wave dataflow are all metadata updates. Metadata is the information that describes the configuration of your organization and of your Wave assets. It’s data about your data. For example, the metadata type 
WaveLens
 includes properties that describe the lens—what dataset it uses, the label of the dashboard, what visualization widget type it uses (pivottable, stackvbar, heatmap, and so on).
You can create your own custom metadata types, which enable you to create your own setup objects whose records are metadata rather than data. Rather than building apps from data records in custom objects or custom settings, you can create custom metadata types and add metadata records, with all the manageability that comes with metadata: package, deploy, and upgrade.
While the 
Metadata API
 is the foundation on which change sets and packaging is built, developers can use the Metadata API to manipulate their Wave assets too. For example, your application could retrieve metadata about your dashboards and back it up to (and restore from) some repository, effectively providing version control.
The following Wave metadata types are supported in the Metadata API:
WaveApplication
WaveDashboard
WaveDataflow
WaveDataset
WaveLens
WaveTemplateBundle
See Also:
Understanding Metadata API
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_packaging_wave_metadata.htm&language=en_US
Release
202.14
