### Topic: Organize Dashboards | Salesforce
Organize Dashboards | Salesforce
Organize Dashboards
Keep your dashboards at your fingertips by sorting them into folders and deleting unused reports. If you have a lot of reports, you can use the search field to find the one you need.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
Delete a Dashboard
It’s a good idea to delete dashboards that you no longer need.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=rd_dashboards_organize.htm&language=en_US
Release
202.14
