### Topic: Searching a Debug Log | Salesforce
Searching a Debug Log | Salesforce
Searching a Debug Log
To search for text in a debug log, use the Command Line Window in the Developer Console.
Before you can search, you must execute Apex statements to generate the log from the Command Line Window.
To open the Command Line Window, click CTRL+L.
Execute Apex code to generate a log: 
To enter Apex statements at the command-line, type 
exec 
<Apex statements>
.
For example:
exec List<Account> accts = new List<Account>();
for (Integer i=0; i<20; i++){
	Account a = new Account(name='Account Name ' + i);
	accts.add(a);
}
To execute code you already entered in the Enter Apex Code window, type 
exec-r
.
After the log has been generated, type 
find 
<string>
 to search for the specified text. 
For example: 
find Account Name
.
Search results are displayed in the Command Line Window.
To close the Command Line Window, click 
CTRL+L
.
See Also:
Developer Console Command Line Reference
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=code_dev_console_search_debug_log.htm&language=en_US
Release
202.14
