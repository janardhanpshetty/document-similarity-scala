### Topic: This aritlces tells you that why SAML SSO bypasses device activation
We have setup Trusted IP ranges at the org level. So when a user tries to login from a new location (new IP), Salesforce login prompts the user to enter 5 digit verification code (for device activation). This is working as expected when using Salesforce login URL. But if the user uses MyDomain login URL to login through SAML SSO, verification code prompt is not working.
Resolution
SAML bypasses device activation. There's no workaround except setting up some sort of conditional Two-Factor Authentication using login flows. For example challenge users that login from an IP that is outside the Network Access range. The following doc includes an example though (search for Conditional Two-Factor):
https://developer.salesforce.com/page/Login-Flows
Keep in mind that with this type of solution, user will be challenged every time they login from an untrusted IP (even they already logged in from that IP).
