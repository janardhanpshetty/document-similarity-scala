### Topic: INTERNAL: logo180.png returning 404 error
Customer would notice URL "https://login.salesforce.com/17181/logo180.png." while they run browser debugger (F12 on chrome etc), PFA the screen shot of the same
We have seen customer queries on this like
> Does this cause performance issue?
> Does this cause blockage to any existing functionality? etc
Resolution
All communication regarding this query is to be handled by 
Security team ONLY
1. This .png is used as part of Malware check designed by our Security team
2. The 404 error would not cause any major performance issue or affect any existing functionality 
3. http://login.salesforce.com/17181/logo180.png If this resource returns a 404, the detection was likely a false positive. If you receive a Salesforce logo, the system is infected and should be isolated from your network and cleaned.
4. We can indicate to the customer that our detection is based off known activity the malware performs.
We will not go into details as this is Salesforce specific 
The more people that become aware of it the greater the risk of it turning up on some blog article somewhere.
Points 1-4 can be communicated to customer, however for any further details please loop in the CSIRT Team(Customer Incident Response Support chatter group) for assistance
