### Topic: Inviting People to Join Chatter | Salesforce
Inviting People to Join Chatter | Salesforce
Inviting People to Join Chatter
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
User Permissions Needed
To invite people to Chatter:
“Allow Invitations”
You can invite people from your company that don't have Salesforce licenses to use Chatter. 
Invited users can view profiles, post on their feed, and join groups
, but can't see your Salesforce data or records
.
To invite people to Chatter:
Click 
Invite People to Chatter
 on the People page or 
Invite Coworkers!
 on the Chatter page.
Note
You can only invite people with email addresses in your company's domains.
Portal users can't send invitations.
To send invitations from Chatter, enter email addresses and click 
Send
.
To send invitations using your own email account, click 
send your own email invitation
. 
If you have a default email client set, an email containing an invitation link opens in your mail program. Otherwise, you see an invitation link that you can copy and paste into an email.
You can also invite coworkers to join a public group even if they don't use Chatter yet. 
Additionally, owners and managers of private groups can send invitations. If customer invitations are enabled, owners and managers can also invite 
customers
. When someone accepts an invitation to join the group, they join Chatter as well.
Click 
Groups
 and click on a group name.
In the Members section on the group detail page, click 
Invite People
.
If you are the group owner or manager, you can also click 
Add/Remove Members
, then click 
Invite them to Salesforce Chatter!
.
Enter email addresses and click 
Send
.
Note
If a user doesn't accept the invitation within the first day, Chatter sends an email reminder the second day. If the user doesn't accept that invitation, Chatter sends another reminder the following day.
A user has up to 60 days to accept an invitation to join Chatter. If during that time, an administrator deselects 
Allow Invitations
 or removes the user's domain from the list, the user can't accept the invitation.
When delegated authentication single sign-on is enabled in your organization, invited users bypass the password registration page. If their username already exists, they won’t be able to accept the invitation.
See Also:
About Chatter Customers in Private Groups
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_invite.htm&language=en_US
Release
202.14
