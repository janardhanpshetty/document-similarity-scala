### Topic: How do I uninstall Connect for Outlook? | Salesforce
How do I uninstall Connect for Outlook? | Salesforce
How do I uninstall Connect for Outlook?
Important
Effective Winter ’16, we retired Connect for Outlook. This means the product no longer saves your emails or syncs your contacts, events, and tasks between Microsoft® Outlook® and Salesforce.
But don’t worry! Become even more productive when you migrate to either Lightning for Outlook or Salesforce for Outlook. Team up with your Salesforce administrator to determine which product is best for you and your colleagues.
Close Microsoft® Outlook®.
In Windows®, open the Control Panel.
Select the option to uninstall programs.
Scroll to Salesforce Outlook Edition, and then follow the prompts to uninstall it.
See Also:
Connect for Outlook End of Life Frequently Asked Questions
Which Microsoft® Email Integration Product is Right for My Company?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_outlook_how_do_i_uninstall.htm&language=en_US
Release
202.14
