### Topic: This is a short summary of the resources available for customers who may not be able to schedule a 1:1 call at this time. It includes information on the Circles of Success program, our Success Webinars, Help and Training information and the customer community.
Improving  Adoption
 
We've put together a summary of the resources we feel will be beneficial to you in increasing your Salesforce adoption, and by extension, the value you get from Salesforce.
Remember, you can always connect with us through the 
Success Community
.
 
We host a series of web events called 
Circles of Success
, which are 
90-minute live, interactive virtual discussions (not webinars) with other customers focused on key adoption-related topics.
​​Topics include: Change Management, ​Using Metrics to Boost Sales Performance, Boosting Efficiency with Service Metrics, Lead and Opportunity Management and Increasing your Productivity with Automation.
Our 
Help and Training page
 contains a wealth of information on every Salesforce topic, including documentation, questions and answers from other customers and on-demand training courses for Standard and Premier customers.
Trailhead
 is our interactive learning path through the basic building blocks of the Salesforce1 Platform. Test your knowledge of the platform while earning points and badges to celebrate your achievements.
You can browse our "
How to....In Salesforce
" to access almost 100 short videos which will show you how to do the most common Salesforce tasks.
We run regular Success Webinars and you can view a schedule of upcoming events, as well as browsing a list of recorded webinars 
here
And finally, you can connect with other Salesforce customers to share your experiences, learn from others and contact the Customers for Life team in our  
Success Community
 - The group for customers from the UK and Ireland is 
here
 and you can see a short video on how to join 
here
 
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
