### Topic: SoftPhone Overview | Salesforce
SoftPhone Overview | Salesforce
SoftPhone Overview
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
A SoftPhone is a customizable call-control tool that appears to users assigned to a call center with machines on which CTI adapters have been installed.
 A SoftPhone's functionality, user interface, and location are determined by the version of the CTI Toolkit with which it was built. See:
About CTI 1.0 and 2.0 SoftPhones
CTI 3.0 and 4.0 SoftPhones
See Also:
Call Center Overview
Salesforce CTI Toolkit Overview
Salesforce Console
Using the SoftPhone
Use a SoftPhone with a Salesforce Console
Checking the Version of Your CTI Adapter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cti_softphones_intro.htm&language=en_US
Release
202.14
