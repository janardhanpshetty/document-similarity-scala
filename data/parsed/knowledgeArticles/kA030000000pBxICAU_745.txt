### Topic: What is the cause of this error?
Emails sent via Journey Builder support personalization strings referencing fields in the Event Source Data Extension. Because the Send Email Activity is built upon Triggered Send architecture, there exists a requirement that for any Interaction using a Send Email Activity, there must exist a field named EmailAddress set with a data type of Email Address, and a field named SubscriberKey set with a data type of Text. 
Resolution
If an Interaction has at least one Send Email activity, and the Event Source Data Extension associated with the Interaction's Trigger does not have one or both these fields as configured above, the application will attempt to create the missing field(s). This is problematic in the following scenarios: 
- A field named SubscriberKey already exists, with a data type other than Text 
- A field named EmailAddress already exists, with a data type other than Email Address 
In these two scenarios, the Interaction will fail to activate and return an error in the User Interface. A corresponding error will be returned in Applog.Error indicating that the field name already exists. 
In order to resolve this issue, the offending field should be renamed or removed from the data extension.
