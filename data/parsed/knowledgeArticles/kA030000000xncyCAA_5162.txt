### Topic: No longer will users have to use an external app to view where their Contact / Accounts are located geographically. With the new Salesforce1 Google Map images, records with standard Address fields will now display an image of the address via Google Maps
Resolution
No longer will users have to use an external app to view where their Contact / Accounts are located geographically.  With the new Salesforce1 Google Map images, records with standard Address fields will now display an image of the address via Google Maps, as shown below:
 
Note
:  Maps on standard Address fields are enabled by default.
To check if you have the feature enabled / disabled please navigate to Setup | Build | Customize | Maps & Location | Settings in your Organization.
 
The image of the Google Map cannot be zoomed in or zoomed out, as it is 'static'.  In order to see more details, you must tap the image, which should open the address in a Map app on your Mobile device.  
Depending on your Mobile device, the following Map app would be launched:
iOS Mobile devices - Apple Maps
Android, Blackberry, and Windows 8.1 Mobile devices - Google Maps
Circumstances when a Map wouldn't display:
For Accounts or Contacts that DON'T have an address, or any of the required information for the Address field is missing
For Organizations that have Salesforce1 Offline access enabled, and users device is offline
