### Topic: Learn how to mass update existing Account record owners using the Data Import Wizard.
レコードを一括更新できるのはSalesforceを利用するメ
リットの一つです。
取引先所有者を一括更新することでよりいっそう時間を節約するこ
とができます。方法は以下の通りです。
Resolution
1. 取引先のレポートを作成します。※飛び先が英語・・・たぶんこち
らが正しい
https://help.salesforce.com/HT
ViewHelpDoc?id=reports_builder
_create.htm&language=ja
2. 「カンマ区切り形式」（CSV） ファイルとして、そのレポートを エクスポートします。
※飛び先英語・・・おそらくこれ
https://help.salesforce.com/HT
ViewHelpDoc?id=reports_export.
htm&language=ja
3. Excel で 先にエクスポートした「カンマ区切り形式」ファイルを開き、取引
先所有者の列をキーにしてデータを並べ替えます。
4. 新しい所有者のユーザ ID を探すため、[設定] へ移動します。
5. [ユーザの管理] | [ユーザ] をクリックします。
6. 取引先の所有者に設定したいユーザの名前をクリックします。
7. ブラウザのURLアドレスが表示されているバー（アドレスバー）
にそのユーザのSalesforce ID（'005' から始まる 15 桁の ID です）を含むアドレスが表示されます。
そのユーザの Salesforce ID をコピーします。
- URL アドレスの例: 
https://na1.salesforce.com/005
30000003xqMx?noredirect=1
,
この場合、’005から始まる15桁、00530000003x
qMx が そのユーザのSalesforce ID です。
8. 既存の取引先所有者の値を新しい所有者の ID で置き換えるため、ユーザ ID を Excel で開いているエクスポートファイルへコピーし、ペーストしてくだ
さい。
9. エクスポートファイルを保存します。これでインポートの準備がで
きました。
10. [設定] へ移動します。
11. [データの管理] | [データインポートウィザード] | [ウィザードを起動する] | [取引先と取引先責任者] | [既存のレコードを更新] を順にクリックします。
12. 「取引先の一致条件:」で "Salesforce ID" を選択し、「既存の取引先情報を更新」のチェックボックスを ON にします。
13. 列の対応付けを行います。
「（あなたの項目）:取引先 ID」 を「取引先: 
Salesforce.com
 ID」に対応付けます。
「（あなたの項目）: 取引先所有者」を「取引先: 所有者」に対応付けます。
14. 「インポートを開始」
注意 - 管理者はレコードの所有権、および、ユーザインターフェイスを介
して関連したレコードを一括して移動するため、データの所有権の
一括移行オプションを活用することもできます。
@Mitsuaki Sato
Show More
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Add Topics
August 9, 2015 at 10:40 PM
  
Kazuyuki Igarashi
@Satomi Zushi
 san,
ありがとうございます。修正しました。
Show More
Like
Unlike
 
  ·  
 
August 9, 2015 at 11:19 PM 
Attach File
 
Click to comment
 
 
Satomi Zushi
 to salesforce.com Only
@Kazuyuki Igarashi
さん
金曜日のステータスで、６個レビュー終わったと言ってましたが、
こちら含めて昨日今日Publishされている３件が急ぎ対象で
すよね？？
Show More
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Add Topics
August 4, 2015 at 11:05 PM
  
Kazuyuki Igarashi
はい。そうです。よろしくお願いいたします。
Show More
Like
Unlike
 
  ·  
 
August 4, 2015 at 11:08 PM 
Attach File
 
Click to comment
 
 
Kazuyuki Igarashi
 to salesforce.com Only
#KBFeedback
 ,
Hi 
@Charlotte Koocheki
,
The click path in the step 11 is incorrect.
It says:
"Click Data Management | Data Import Wizard | Launch Wizard | Select Accounts | Update existing records."
However it should be:
"Click Data Management | Data Import Wizard | Launch Wizard | Accounts and Contacts | Update existing records."
Could you please check it?
Thanks,
Kaz
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
July 30, 2015 at 8:31 PM
  
Ananth Sivakumar
Igarashi - san. Article has been updated. Thanks for your feedback.
Show More
Like
Unlike
 
  ·  
1 person
 
  ·  
 
August 3, 2015 at 2:10 PM 
Kazuyuki Igarashi
Thank you, 
@Ananth Sivakumar
Show More
Like
Unlike
 
  ·  
 
August 3, 2015 at 5:05 PM 
Attach File
 
Click to comment
 
 
Deepkant Verma
 
published this new Knowledge Base.
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Bookmark
Add Topics
December 22, 2014 at 3:49 PM
  
Show All 
6
 Comments
Deepkant Verma
Hi 
@John Falge
. If you please get some time to review this article again. Seems like Ashish followed all you suggested him to work on this one.
Thanks
Show More
Like
Unlike
 
  ·  
 
December 23, 2014 at 12:30 PM 
John Falge
@Ashish Malhotra
 thanks for re-submitting and contributing this article!! I have made additional changes to the article and wanted to emphasize a few things:
You should never have to put anything into the Help Site URL field and be sure to select all of the relevant Product Features and set a next review date out for 6 months.
Other then this great job and keep the articles coming!
Show More
Like
Unlike
 
  ·  
 
December 29, 2014 at 10:25 AM 
Ashish Malhotra
Thank you so much! 
@John Falge
I greatly appreciate your efforts & time you spent to make the changes on this article. And I honestly believe that there is big difference between the article I drafted and later modified by you. I accept this as a feedback & will try to work more effectively while working on my future articles.
@Deepkant Verma
Show More
Like
Unlike
 
  ·  
 
December 29, 2014 at 11:22 AM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Usage
The ability to update records in bulk is one of benefits of using Salesforce. We'll show you how to save even more time by making bulk updates to Account record owners.
Resolution
1. 
Create a report on Accounts
 include the fields Account Owner, Account ID and Account Name. Optionally, add additional fields that may help you to identify the account's to be transferred.
2. 
Export the report
 as a .CSV (comma-separated values) file.
3. Sort the resulting report export's .CSV file in Excel by Account Owner.
4. Next you will need to replace the existing Account Owner's name with the new Account Owner's Salesforce User Id. Locate the new owner's User Id in Salesforce by navigating to 
Setup
, 
Manage Users
 | 
Users
.
5. In the list of users click on the user's name that you'd like to transfer Account ownership to.
6. Copy the Salesforce Id for that user from your browser's URL (15 digit ID beginning with '005').
- The URL may read: https://<yourinstance>.salesforce.com/00530000003xqMx?noredirect=1 where "00530000003xqMx" is the 15 digit User Id.
7. Paste the new owner's user Id over the existing Account Owner name values in the Excel file.
8. Once the existing Account Owner's names have been replaced by your new owner's user Ids save your file and it is now ready for import.
9. Navigate to 
Setup
, 
Data Management
 | 
Data Import Wizard
 | 
Launch Wizard
 | 
Select Accounts
 
and Contacts
 | 
Update existing records
.
10. Select to match by Salesforce ID and select 
Update existing Account information
.
11. Map the columns to fields:
- Map your field: Account ID to Account: Salesforce.com ID.
- Map your field: Account Owner to Account: Record Owner.
 
12. Start Import.
 
Note:
 Administrators can also leverage the 
Mass Transfer Records
 options within Salesforce to mass transfer record ownership and their related records via the User Interface.
