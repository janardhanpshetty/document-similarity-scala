### Topic: When you convert a Lead you either created a New account or attach it to an Existing Account. When you select new Account, by default a New contact is created. However, Converting Lead to an existing Contact is possible, if it’s attached to existing account and have existing contact associated to that Account.
This scenario concerns the Lead Conversion in Salesforce Classic, not Lightning Experience
Why don't I see the 'Attach to Existing Contact' on the lead conversion screen?
 
 
Resolution
The ability to select an existing contact record during the lead conversion process depends on two things.
You must attach the lead record to an existing account.
There must be an existing contact to de-dupe against.
If the user's level of access/visibility does not allow them to fulfill both of the above criteria, you won't see the "Attach to Existing Contact" screen.
 
Once the above criteria are met, you can follow the below mentioned steps to convert the lead: 
Open the lead which needs to be converted and click on ‘Convert’ button
In the Account Name field, select Attach to Existing Account (Eg: Big cars Account)
Complete the other details on the page as per the requirement and click on ‘Convert’ button
Since a Contact with same name already exists in the selected Account, it will allow you to either create a new contact or attach to an existing contact.
Select the Contact in which you want to merge the lead while conversion.
Click on Convert button. 
