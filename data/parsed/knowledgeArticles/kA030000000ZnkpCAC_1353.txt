### Topic: Slow running reports, dashboards, and list views should be optimized. If performance is still unacceptable, the issue should be escalated with login access and an escalation template that has been filled out completely.
NOTE: Any cases regarding performance of list views, reports, or dashboards should be handled by CRM Reporting & Dashboards, even if more than one list view, report, or dashboard are affected.
For List View, Report, or Dashboard issues, follow the troubleshooting steps below:
Resolution
1. Optimize the affected list views, reports or dashboard’s source reports. See the resources below for more information:    
Improve List View Performance
Improve Report Performance
Improve Dashboard Performance
2. Once confirmed that all list views, reports, and dashboard source reports have been optimized, escalate to Tier 3 to explore advanced report, dashboard, and list view performance troubleshooting options. See the resources below for more information:
How to escalate a Report/Dashboard/List View Performance Case to Tier 3
Advanced Report Troubleshooting Options
