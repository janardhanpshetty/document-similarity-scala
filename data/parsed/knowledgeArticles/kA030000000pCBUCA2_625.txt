### Topic: FAQs for the recently announced SalesforceIQ for Sales Cloud - INTERNAL
Q.
 What are the main benefits of using SalesforceIQ? 
A. 
SalesforceIQ automatically captures your email communication and calendar events so salespeople can focus on selling and not entering data. The CRM also provides Intelligence Fields that automatically update and provide insight into your day-to-day activity. Along with streamlining data entry and intelligence fields SalesforceIQ suggests specific Follow-Ups so nothing slips through the cracks.
Q
: Is SalesforceIQ for Sales Cloud available to Sales Cloud customers?
A
: SalesforceIQ for Sales Cloud customers is available for download in the iOS/Android/Chrome stores today.
     It is free for Sales Cloud customers during this beta period.
Q
: Can I get access to SalesforceIQ for Sales Cloud?
A
: Please refer this chatter post for further information:
     https://org62.my.salesforce.com/0D53000002CuGTw
Q
: How much does SalesforceIQ for Sales Cloud cost?
A
: SalesforceIQ for Sales Cloud is currently in beta. We are in the early stages of evaluating a pricing model, and the pricing will be determined when the product is GA.
Q
: Which countries can access SalesforceIQ for Sales Cloud?
A
: SalesforceIQ for Sales Cloud is available globally with a just a few restrictions:
     iOS: All app stores except France. Android: All app stores except Cuba, Iran, and Sudan. Chrome: All countries
Q.
 Is there a trial available?  If so, how can my customer run a trial?
A.
 Yes, a 14 day trial is available. There are two places a trial can be initiated
1.     Salesforce.com via the SalesforceIQ product pages
2.     http://www.relateiq.com/start
The customer will need to start the trial on their own as it requires authenticating their Gmail or MS Exchange email account so the AE should send the link above directly to the customer instead of populating it for them.
Resources:
Cloud: Selling SalesforceIQ
 chatter group
 
Resolution
