### Topic: Here's how to batch add batch definitions using Workbench.
Some customers may need to batch add badge definitions either because they are a legacy Work.com customer or they are migrating from another recognition app.
Resolution
1. Go to the 
Documents
 tab and upload all the badge images as Documents.
 
2. Log into 
Workbench
 using your Salesforce credentials.
 
3. Set 
Environment
 = Production, 
API Version
 = 35.0, agree to the terms of service and click the 
Login with Salesforce
 button.
 
4. Set 
Jump to
 = SOQL Query, 
Object
 = Document and click the 
Select
 button.
 
5. Enter the following in the 
Enter or modify a SOQL query below:
 text-area
 
SELECT Id,Name FROM Document WHERE CreatedDate = Today
6. Hit the 
Query
 button and copy the generated results to an Excel file.  You'll need this for the next step.
 
7. Open the WorkBadgeDefinition CSV and complete all the fields for each badge. Set ImageUrl field to the corresponding Document's Salesforce ID from step 6.
 
8. Go back into Workbench and Select 
data | Insert
 from the top menu.
 
9. Set 
Object Type
 = WorkBadgeDefinition, 
From File
 and select your 
WorkBadgeDefinition
 CSV.
 
10. Click 
Next
, click 
Map Fields
 then click 
Confirm Insert
.
 
11. Click the Download Full Results button.
 
12. Open the WorkAccess CSV. Copy the Salesforce Id and OwnerId values from the downloaded results in step 11 and paste them into the ParentId and OwnerId fields of WorkAccess CSV and set AccessType = Give.
 
13. Go back into Workbench and Select 
data | Insert
 from the top menu.
 
14. Set 
Object Type
 = WorkAccess, 
From File
 and select your 
WorkAccess
 CSV.
15. Click 
Next
, click 
Map Fields
 then click 
Confirm Insert
.
 
16. Click the Download Full Results button.
 
After step 16 the badges will be created, but only the Owner will be able to give them out. If others should be able to give the badge you need to complete the following steps:
17. Open the WorkAccessShare CSV. Copy the Salesforce Id values from the downloaded results in step 16 and paste them into the ParentId field of WorkAccessShare CSV and set AccessLevel = Read and UserOrGroupId = Salesforce Id of the user who can give the badge. You need one WorkAccessShare record for each person that should be able to give a certain badge (excluding the owner).
 
18. Go back into Workbench and Select 
data | Insert
 from the top menu.
 
19. Set 
Object Type
 = WorkAccessShare, 
From File
 and select your 
WorkAccessShare
 CSV.
20. Click 
Next
, click 
Map Fields
 then click 
Confirm Insert
.
