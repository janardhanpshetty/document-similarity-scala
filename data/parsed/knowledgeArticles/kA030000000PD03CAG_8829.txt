### Topic: This article has information regarding the typical size of the relay state message from Salesforce and its authentication steps.
The Typical size of the relay state message from salesforce.com is typically around 375 bytes.
Resolution
Additional detail around this:
Authentication Steps ...
1) A user requests a resource in their org, for example: https://customer.my.salesforce.com/001/o
2) Salesforce notices that the user does not have a session for that org, and sends a SAML Authentication Request to the org’s SAML Identity Provider. This includes a parameter called “RelayState” that is the URL the user was requesting ( in this example “/001/o” )
3) The SAML Identity Provider logs the user in, and sends them back to Salesforce with a SAML Response and the RelayState **This is where customers MAY need to make adjustments to their specific IDP provider setup **
