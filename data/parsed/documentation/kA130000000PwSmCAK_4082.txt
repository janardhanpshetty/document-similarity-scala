### Topic: Signup Request Home | Salesforce
Signup Request Home | Salesforce
Signup Request Home
User Permissions Needed
To create or view signup requests:
“Signup Request API”
Note
You are limited to 20 sign-ups per day. If you need to make additional sign-ups, log a case in the Partner Community.
The Signup Requests tab displays the signup requests home page. From this page, you can perform the following actions.
Create a new signup. If you using a Trialforce template to create the signup, make sure the template has been approved.
View the details of a previous signup, including its history and approval status.
Create new views to display signups matching criteria that you specify.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=signup_request_home.htm&language=en_US
Release
202.14
