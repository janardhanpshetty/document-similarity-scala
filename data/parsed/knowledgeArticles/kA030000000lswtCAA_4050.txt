### Topic: This article explains how to build a formula field that will copy the contact owner name. The contact owner name is made up for 2 different value - Owner first name and Owner last name. Doing a formula to combine these 2 will get the required result
How to create a formula that will copy the record owner name?
Resolution
Create a text formula field : 
Owner.FirstName & " " & Owner.LastName
This would concatenate the first name and last name of the contact owner with a Space between the first and last name thus giving the required output. This can be used on Account, contact, opportunity and any other custom object.
Lead and Case work a little different
For Lead/Case it would be 
Owner:User.FirstName & " " & Owner:User.LastName
