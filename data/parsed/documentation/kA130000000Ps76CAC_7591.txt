### Topic: Using the Case Milestones Related List | Salesforce
Using the Case Milestones Related List | Salesforce
Using the Case Milestones Related List
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions with the Service Cloud
User Permissions Needed
To view case milestones:
“Read” on cases
To edit case milestones:
“Edit” on cases
The Case Milestones related list on a case detail page displays a list of milestones that automatically apply to the case due to an entitlement process. Milestones are required steps in your support process. They're metrics that represent service levels to provide to each of your customers. Examples of milestones include First Response and Resolution Times on cases.
Note
No records to display
 appears in the related list if no milestones apply to the case.
The related list contains fields that your company has chosen to display, based on its business processes. Depending on your company's requirements, you may see some or all of the following fields.
Action
Lists the actions you can perform on the milestone. For example, if you have the “Edit” permission on cases, you can click 
Edit
 to select the milestone completion date.
Note
Customer Portal users can't edit case milestones.
Milestone
The name of a set of steps in an entitlement process that applies to the case. Users with the “Manage Entitlements” permission can click a milestone's name to view the entitlement process, case criteria, time triggers, and actions associated with it.
The following table lists the types of actions associated with milestones:
Action Type
Description
Success Actions
The actions to take when a milestone successfully completes. Success actions still fire on milestones that are closed late.
Warning Actions
The actions to take when a milestone is near violation.
Violation Actions
The actions to take when a milestone is violated.
Administrators can set up milestones to automate the following for each action type:
Workflow Action
What It Does
Example
New Task
Create a workflow task
Create a task for a support agent to call a customer when a First Response milestone is violated.
New Email
Create an email alert
Notify case owners when a First Response milestone on their case is near violation.
New Field Update
Define a field update
Update the case Priority field to 
High
 when a First Response milestone is near violation.
New Outbound Message
Define an outbound message
Send data about parts or services to an external system after a First Response milestone is completed.
Select Existing Action
Select an existing action
Use an existing email alert to notify a case owner when their case is near violation of a first response.
Start Date
The date and time that the milestone tracking started.
Target Date
The date and time to complete the milestone.
Completion Date
The date and time the milestone was completed.
Target Response
Shows the time to complete the milestone. Automatically calculated to include any business hours on the case.
 Depending on your company's business requirements, the time can appear in minutes, hours, or days.
Time Remaining
Shows the time that remains before a milestone violation. Automatically calculated to include any business hours on the case.
 
Depending on your company's business requirements, the time can appear in minutes, hours, or days.
Elapsed Time
Shows the time it took to complete a milestone. Automatically calculated to include any business hours on the case. Elapsed Time is calculated only after the Completion Date field is populated.
 Depending on your company's business requirements, the time can appear in minutes, hours, or days.
Violation
Icon (
) that indicates a milestone violation.
Time Since Target
Shows the time that has elapsed since a milestone violation. Automatically calculated to include any business hours on the case.
 
You can choose to display the time in days, hours and minutes, or minutes and seconds.
Completed
Icon (
) that indicates a milestone completion.
Because they're part of a case's history, completed milestones remain on a case even if they're no longer applicable.
See Also:
Case Fields
What Is Entitlement Management?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cases_milestones.htm&language=en_US
Release
202.14
