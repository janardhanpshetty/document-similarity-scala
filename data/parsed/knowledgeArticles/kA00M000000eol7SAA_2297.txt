### Topic: If a customer request to change or transfer their Force.com subdomain to a different org, additional steps should be performed before the customer attempts to create additional sites or communities in the target org.
Usecase:
  Customer have 2 org .
"Org 1" (i.e. Source Org) have  Domain Name(site/community) => trackmydomain.force.com
Customer wants to use this domain in "Org 2" 
Customer raised a support case to rename the domain in "Org 1" so that domain can be used in "Org 2"
Steps to avoid future errors:
If the subdomain is being used by 
Force.com
 Sites in the source org (the org domain is to be transferred FROM):
1. Open the source org detail page via Blacktab
2. Change the subdomain (Unique Site Subdomain) to something unique. For example, for a subdomain "mysites", try changing it to "mysites1" or similar.
3. Now login as an administrator to the source org. This is necessary in order to change the guest user names to match the new subdomain, which ultimately will avoid any conflicts when the domain is transferred to the target org.
4. Go to "Setup | Develop | Sites".
5. For ALL 
force.com
 sites in the source org:
Click on the the Site Label to go to the Site detail page.
Click on the "Public Access Settings" button.
Click on the "View Users" button.
Click on the "Edit" Link in the Action colume, beside the Guest user.
Change the guest user name to match the new subdomain. For example, for a site named "My Site" in an org with the subdomain "mysites", the guest user name would be be "
My_Site@mysites.force.com
". So if the subdomain was changed from "mysites" to "mysites1", then the guest username should changed similarly to "
My_Site@mysites1.force.com
". 
6. Now go to the Blacktab org detail page of the target org (the org the domain is being transferred to), and apply the new domain, or alternatively ask the customer to register the domain in the new org, if they haven't done so already.
Similar steps need to be taken for communities, if the subdomain being transferred is for communities (Unique Chatter Community Subdomain), the difference being that the community guest users will need to be renamed. The steps above should prevent additional conflicts, should the customer happen to create a new site/community with the same name as one in their old org.
Resolution
