### Topic: Notes on Using Mass Delete | Salesforce
Notes on Using Mass Delete | Salesforce
Notes on Using Mass Delete
Consider the following when using mass delete:
Available in: Salesforce Classic
Available in: 
All
 Editions
This feature is only available in 
Database.com
 via the API. You can only mass delete records of custom objects in 
Database.com
.
User Permissions Needed
To mass delete data:
“Modify All Data”
General Notes About Mass-Deleting
You can delete up to 250 items at one time.
When you delete a record, any associated records that display on that record’s related lists are also deleted.
Only reports in public report folders can be mass-deleted.
You can’t mass-delete reports that are attached to dashboards, scheduled, or used in reporting snapshots.
Notes About Mass Delete for Sales Teams
You can’t delete partner accounts that have partner users.
Products on opportunities cannot be deleted, but they can be archived.
When you mass-delete products, all related price book entries are deleted with the deleted products.
When you delete activities, any archived activities that meet the conditions are also deleted.
When you delete activities, requested meetings aren’t included in the mass-delete until they are confirmed and automatically converted to events.
When you delete recurring events, their child events are not displayed in the list of possible items to delete, but they are deleted.
Notes About Mass Delete for Service Teams
Accounts and contacts associated with cases cannot be deleted.
Contacts enabled for Self-Service, and their associated accounts, cannot be deleted.
Deleting a master solution does not delete the translated solutions associated with it. Instead, each translated solution becomes a master solution.
Deleting a translated solution removes the association with its master solution.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_massdelete_notes.htm&language=en_US
Release
202.14
