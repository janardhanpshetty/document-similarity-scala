### Topic: Edit Custom Fields | Salesforce
Edit Custom Fields | Salesforce
Edit Custom Fields
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
Standard Objects are not available in 
Database.com
User Permissions Needed
To create or change fields:
“Customize Application”
From the management settings for the field’s object, go to Fields.
Click 
Edit
 next to the field’s name.
Modify the 
field attributes
. The attributes differ depending on the field type. 
If you’re editing a picklist, you can change its definition and its values. For picklist settings, see 
Add or Edit Picklist Values
.
To change the type of this custom field, see 
Change the Custom Field Type
.
Optionally, define custom help text for the field.
For lookup and master-detail relationship fields, optionally define a lookup filter.
For formula fields, click 
Next
 to modify the formula.
In Enterprise, Unlimited, Performance, and Developer Editions, click 
Next
 to set the field-level security for the field.
Note
Editing fields may require changing a large number of records at once. To process these changes efficiently, your request may be queued and you may receive an email notification when the process has completed.
To customize the way a custom object’s related list appears on a parent record’s detail page, edit the parent record’s page layout. For example, if you want to edit which fields appear on a custom object’s related list on accounts, you would edit the account page layout.
You cannot change the 
Field Name
 if a custom field is referenced in Apex.
When editing fields for accounts, opportunities, cases, contacts, or custom objects, check for any criteria-based sharing rules that use the field in the rules. A field change may affect which records are shared.
See Also:
Define Default Field Values
Find Object Management Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=editing_fields.htm&language=en_US
Release
202.14
