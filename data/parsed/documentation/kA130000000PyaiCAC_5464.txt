### Topic: Tips for Working with Date and Date/Time Formula Fields | Salesforce
Tips for Working with Date and Date/Time Formula Fields | Salesforce
Tips for Working with Date and Date/Time Formula Fields
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
Dates and times are always calculated using the user’s time zone.
Date and date/time fields can’t be used interchangeably. The name alone may not indicate if a field is a date or date/time. For example, 
Created Date
 and 
Last Modified Date
 are date/time fields whereas 
Last Activity Date
 is a date field. Use the DATEVALUE function to convert a date/time field into a date field.
Note
The 
Created Date
 and 
Last Modified Date
 fields display only the date, not the date and time.
Use addition and subtraction operators with date or date/time fields to calculate duration. For example, subtract a date from another date to calculate the number of days between the two. Likewise, you can subtract the date/time from another date/time to get the number of days between the two as a number. See NOW or TODAY for suggested use.
Use addition and subtraction operators with numbers to return another date or date/time. For example, 
{!CreatedDate} + 5
 calculates the date and time five days after a record’s created date. Note that the expression returns the same data type as the one given; a date field plus or minus a number returns a date, and a date/time field plus or minus a number returns a date/time.
When calculating dates using fractions, Salesforce ignores any numbers beyond the decimal. For example:
TODAY() + 0.7
 is the same as 
TODAY() + 0
, which is today’s date.
TODAY() + 1.7
 is the same as
TODAY() + 1
, which is tomorrow’s date.
TODAY() + (-1.8)
 is the same as 
TODAY() + (-1)
, which is yesterday’s date.
To calculate the value of two fractions first, group them within parentheses. For example:
TODAY() + 0.5 + 0.5
 is the same as 
TODAY() + 0 + 0
, which is today’s date.
TODAY() + (0.5+0.5)
 is the same as 
TODAY() + 1
, which is tomorrow’s date.
Years can’t be zero and must be between -4713 and 9999.
See Also:
Tips for Building Formulas
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tips_for_using_date_datetime_formula_fields.htm&language=en_US
Release
202.14
