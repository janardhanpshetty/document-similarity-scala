### Topic: Do zone files from deliverability include entries for DKIM or domain keys?
Do zone files from deliverability include entries for DKIM or domain keys?
Resolution
Yes, typically if we give a client a zone file it includes DKIM entries. We only send zone files to clients who are hosting things themselves, if you have SAP we will not send a zone file because we are hosting everything on the client's behalf.
