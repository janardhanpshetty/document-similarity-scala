### Topic: You can follow or unfollow Twitter Accounts within Service Cloud but cannot see current followers or accounts you are following.
Can I see Twitter accounts that follow me or following in Service Cloud?
Resolution
You cannot view your current followers or the accounts that you follow within Service Cloud when using Radian6 for Salesforce.  You can choose to follow or unfollow Twitter accounts in Service Cloud under your selected Twitter managed account within the reply to the last post in your case.
 
