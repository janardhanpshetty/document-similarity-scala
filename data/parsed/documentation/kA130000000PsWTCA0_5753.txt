### Topic: Set Up Interaction Logs for a Salesforce Console | Salesforce
Set Up Interaction Logs for a Salesforce Console | Salesforce
Set Up Interaction Logs for a Salesforce Console
An interaction log lets Salesforce console users write notes on records that appear on primary tabs.
Salesforce console available in Salesforce Classic and App Launcher in Lightning Experience. Setup for Salesforce console available in Salesforce Classic.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set up interaction logs:
“Customize Application”
For example, when cases appear on primary tabs, an interaction log can appear so that users can take notes on cases. You can create multiple interaction logs and customize them to display specific task fields for different users to update.
From Setup, enter 
Interaction Log Layouts
 in the 
Quick Find
 box, then select 
Interaction Log Layouts
.
Click 
New
.
Name the interaction log.
Select task fields to add to the interaction log and click 
Add
.
*
 indicates required fields.
You can only add editable task fields to interaction logs.
The 
Enter your notes here...
 field is automatically added to all interaction logs; you can't remove it.
If you want this interaction log to be the default for all users, select 
Set as default layout
.
You can't delete a default interaction log; you must first mark another interaction log as the default for your organization.
Click 
Save
.
After you set up or customize interaction logs, you can assign them to different user profiles and turn them on to display.
Tip
You can create custom fields for tasks and add them to interaction logs. For example, you can create a 
Caller Disposition
 picklist with values of Angry, Neutral, and Satisfied.
See Also:
Turn On Interaction Logs
Assign Interaction Logs
Salesforce Console Configurable Features
Salesforce Console
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=console2_setup_log.htm&language=en_US
Release
202.14
