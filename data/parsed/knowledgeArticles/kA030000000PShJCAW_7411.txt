### Topic: This is by design. When we generate the Case Description based on the EmailMessage body we initially save everything to that field up to the 32K character limit (on the database side); however in some scenarios the Description field shows a few characters over the limit because the database and the UI code counts characters using different criteria.
When we generate the Case Description based on the EmailMessage body we initially save everything to that field up to the 32K character limit (on the database side); however in some scenarios the Description field shows a few characters over the limit because the database and the UI code counts characters using different criteria.
Resolution
This is by design; however, there are three workarounds:
1) Manually reduce the characters and/or remove line breaks from the Description so that count is below 32K (EmailMessage body still stay intact). This will let you edit/save the record without issue.
2) Create customization such as Apex Trigger to get remove extra characters on the Description 
3) Create inbound email Apex class (for Email to Case) and also create a custom long text Description field that will let you save characters over 32K
