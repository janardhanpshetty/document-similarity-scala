### Topic: It may be necessary to summarize a field twice. For example, the Amount field needs to be summarized to aggregate the amounts in two places on the report
How can I summarize a report by the same field twice?
Resolution
It may be necessary to summarize a field twice.  For example, the Amount field needs to be summarized to aggregate the amounts in two places on the report.  Currently, when you select to summarize the field twice, it will only recognize the first choice and the second selection will be ignored.
The work around is to create a custom field with a formula datatype that will populate the value of the original field and provide a secondary field for summarization in reports.  Here are the steps below:
Setup | Customize | Object | Fields | "New"
1. Select the "Formula" option and click "Next."
2. For "Field Label," insert the name you would like the field to be displayed as on the page layout.
3. Hit Tab. This will populate the "Field Name."
4. Select the same format as the original field you are duplicating and click "Next."
5. Within the "Select Field" picklist, select the name of the original field and it will be inserted into the formula.
6. Click on "Validate Syntax" and once the No errors found value is returned, click "Next."
**Enterprise Edition Customers-
You will need to select those profiles you would like to have access to this new field, and click on "Next."
7. Select which page layouts you would like to add the field to, click "Save."
** Note: In order to include this field in your report, the user's profile must have visibility to it.
8. Click into the reports tab and begin to create your report.
The report will show this new field and can now be summarized along with the original field.
