### Topic: Prospecting Insights Data Details | Salesforce
Prospecting Insights Data Details | Salesforce
Prospecting Insights Data Details
Data.com Prospecting Insights combines D&B company details and industry intelligence in a quick-view snapshot, to help you start smart conversations with your prospects and customers.
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
 Editions
Prospecting Insights is available with Data.com Corporate Prospector and Data.com Premium Prospector licenses. Insights data is view-only, and is available for accounts that were imported from, or cleaned by, Data.com.
Insights Category
Details
Business Details
Description of the company supplied by the Hoover’s, Inc. subsidiary of Dun and Bradstreet
Company address, phone, and website
Location type–headquarters or a branch office
Location size in square feet, identified as the actual measurement or estimated
Location ownership–owned or rented
Tradestyle, if the company uses one
Year the company was founded
Financial Details
Fiscal year end, identified by month
Stock market exchange and symbol
Standard & Poor’s 500 index indicator
Fortune 1000 rank
Delinquency risk–the likelihood of this company paying 90 or more days late over the next 12 months
*
Important
Use this information for marketing pre-screening purposes only.
Annual revenue for the most recent and prior year, and growth percentage
Net income for the most recent year
*
Employee number for the most recent and prior year, and growth percentage
Contacts from Data.com
Graphical view of contact distribution at the company
Links to contacts at the company, by department–clicking a link takes you to the Data.com Find Contacts page
Top Competitors
Links to websites for the company’s top competitors
Primary competitors listed by revenue
Industry Details
Primary NAICS and SIC classifications
Industry selector for industries related to the primary NAICS and SIC classifications
Sample call-prep questions
Competitive landscape analysis
*
–an overview of the industry’s competitive environment
Industry trends
*
 and industry opportunities
*
 –how events and issues are affecting industry businesses
Links to selected industry websites
*
Available only with a Premium license
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=jigsaw_int_prospecting_insights_details.htm&language=en_US
Release
202.14
