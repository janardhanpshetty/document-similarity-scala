### Topic: This article contains information on Multi-Currency Reporting and Dashboards including future functionality discussions.
Preface:
Currently an org with multi-currency enabled and dashboards/reports needs to clone reports and dashboards in order for them to display in different currencies.
In an org with 5 dashboards and 100 reports, one would need to build 50 dashboards and 1000 reports to support 10 currencies.
This is a nightmare from a build effort and maintenance perspective.
The attached deck and DF13 session recording (with slides) demonstrate 4 workarounds to this (3 of which are code free).
The Analytics Product Management Team is aware of the solution as are a number of people around the business, but the solution has not been institutionalised such that all customer facing teams (inc support) and our self service knowledge base are aware of the solution.
A platform fix for this issue is not targeted to be within the next 3 releases post Winter '14 (as of time of writing).
Note: Use of dynamic dashboards makes the solution highly efficient (1 dashboard with 1 set of underling reports for all users viewing the dashboard who are content with the aggregation levels within the dashboard).
Even static dashboards will benefit as the underlying reports driving the dashboard do not need to be cloned, modified and then linked back to the cloned dashboard components (ie Cloning the dashboard and changing running user is all that is required vs the current labour intensive technique).
 
Resolution
See attached Deck
See Dreamforce 13/Youtube Videos
Dreamforce Site:
http://www.salesforce.com/dreamforce/DF13/sessions.jsp#page=0&search=analytics+ninjas&role%5Btitle%5D=Role&role%5Bvalue%5D=All&industry%5Btitle%5D=Industries&industry%5Bvalue%5D=All&product%5Btitle%5D=Products&product%5Bvalue%5D=All
Analytics Ninjas
 - see 24 mins 50 seconds in
Analytics Ninjas 2
 [Repeat] - see 30 mins 20 seconds in
NB: (to be removed from any published article)
Though a number of companies have been shown the solution (Kone, Abbot Labs) only Thompson Reuters to my knowledge has deployed it. I have no knowledge of how it scales but given we have plenty of orgs with formulas in dashboards and reports I see no reason why it should degrade to any greater extent than other reports/dashboards using formulas.
 
