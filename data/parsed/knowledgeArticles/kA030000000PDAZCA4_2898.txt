### Topic: This article provides a summary of available resources that might help you in your CTI integrations.
The CTI integration is actually not handled by Salesforce. We actually provide a toolkit to a wide variety of partners and telephony system providers, such as Cisco and third party integration specialists.
Resolution
 
Here is a link for Cisco:
The Cisco Unified CallConnector for Salesforce is available for download for Cisco customers at 
www.cisco.com/go/salesforce
You will probably want to call them to ensure you are downloading the correct version.
Overview:
1) You need to find a partner product that will work for you. You should contact Cisco directly to ask them for the specific CTI adapter which works with your specific Cisco model.
2) There are third party vendors such as AMC and others who have adapters for Cisco systems. You can find most of those on our AppExchange website.
Cisco related:
http://appexchange.salesforce.com/results?type=undefined&keywords=Cisco
CTI general:
http://appexchange.salesforce.com/results?type=Apps&keywords=CTI
CTI forum link that you might be interested in... mentions a few other vendors:
https://sites.secure.force.com/success/ideaview?id=08730000000I2BDAA0
3) Install the adapter & Import the Call Definition File into your Salesforce org. Associate the users and you are up and running.
All of the details around this are located in the Help & Training. Search under CTI setup.
