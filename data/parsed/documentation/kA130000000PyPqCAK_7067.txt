### Topic: Managing Subtab Apps | Salesforce
Managing Subtab Apps | Salesforce
Managing Subtab Apps
You can view and customize the subtab apps on users’ profile pages.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view apps:
“View Setup and Configuration”
To manage apps:
“Customize Application”
From Setup, enter 
Apps
 in the 
Quick Find
 box, then select 
Apps
 to display your organization’s subtab apps.
You can do the following:
To view details for a subtab app, click the name in the 
Subtab Apps
 section. This displays its properties, such as which tabs are part of the app, including any tabs that are not yet deployed. Click custom tabs in the Included Tabs list to view details.
To change the properties of a subtab app, click 
Edit
 to choose the tabs to include in the subtab app, change their display order, and set the 
Default Landing Tab
.
Note
Administrators can change permission sets or profile settings to limit users’ access to each tab. This allows administrators to make specific tabs available to some users, but not to others.
See Also:
What is a Subtab App?
Controlling Subtab App Visibility
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dev_subtab_manage.htm&language=en_US
Release
202.14
