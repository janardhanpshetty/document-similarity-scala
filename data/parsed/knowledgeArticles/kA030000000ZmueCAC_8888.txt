### Topic: Creating a self lookup on the object and mentioning the same Child Relationship name creats problem while generating the WSDL.
Creating a self lookup on the object and mentioning the same Child Relationship name creating problem while generating the WSDL.
E.g On account object create a self lookup field with below details and generate the WSDL.
Field Name: Account_Lookup 
Child Relationship Name: Account_Lookup 
The parent relationship is missing if the name is same.
 
Resolution
Change the name of the Child relationship to represent plural reference.
It's bad practice to name these two fields the same: one is a singular reference to a parent-object relationship, while the other should be a plural reference to multiple children objects. 
Users should give the names something more meaningful (so, if creating a look-up field on account to an account, the field might be aptly named "Parent Account", and the child relationship may be aptly named "Associated Accounts" or "Child Accounts" or "Affiliated Accounts"). 
 
