### Topic: I cannot add a custom formula I created on a specific report block on my joined report. I cannot perform the drag and drop functionality of adding columns on the report builder.
After I created a joined report, I created a custom formula on one of the report block, but I cannot seem to add it as a column on the respective report block. I don't have a place where I can drop it on the report block, just like the way I can drag and drop a normal field.
 
Resolution
After creating a custom formula for the report block on a joined report, you still need to have a common group field across report blocks. You can choose any of the fields under the 'Common Fields' section, then drag it to the portion where it says 'Drop a field here to group across report blocks'. Once there is a grouping then you can now perform a drag and drop action for the custom summary formula on the respective block. As indicated on the documentation: 
Custom Summary Formulas with Joined Reports
"The formulas aren’t automatically added to the report when you create them. To add a formula, drag it to a block with the same report type"
Steps to Reproduce:
1. Access Report tab
2. Click on New Report
3. Select Accounts standard report type click on Create button
4. Set the report to Joined Report
5. Click on Add Report type button, select Opportunities standard report type click Ok.
6. Create a new custom summary formula under the Account report block, name it Test and select Record Count and click Ok.
7. Try to drag and drop the 'Test' custom formula on the Account report block and you will not be able to place it on the end section of the report block.
8. Add now the Account Name field as the common group field by drag and drop on the '
'Drop a field here to group across report blocks" section of the report builder.
9. You should be able to drag and drop now the 'Test' custom formula on the Account report block.
