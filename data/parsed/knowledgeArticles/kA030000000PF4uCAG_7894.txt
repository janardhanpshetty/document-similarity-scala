### Topic: Users get an error when trying to edit a chatter free user profile as "default app error"
We have tried to edit the Chatter Free User profile and remove the "Invite Customers To Chatter" permission, but we get an error message saying "You must choose a default App" and the change is not saved. But there is nowhere on the screen to choose a default App.
Resolution
1.Escalate to Tier 3 with the following template:
Tier 3 - 
Issue: 
"
Name of the Account".is unable to edit the chatter free user profile, every time they try to do so they get the following error; 
Error: Invalid Data. 
Review all error messages below to correct your data. 
You must choose a default App 
Steps to Reproduce: 
1. Login to Org 
2. Login as Admin user
3. Click on “edit” and then “save” on the chatter free user profile
4. You will get the above stated error 
Requested Response: Can Tier 3 assist in determining why this is occurring? According to the KB article https://na1.salesforce.com/apex/KnowledgeViewArticle?id=a5H30000000VZJj&sfdc.override=1 the following scrutiny has to be run in order to fix this issue DefaultTabsetMissingForCSNProfiles scrutiny. 
============================================================ 
- Describe urgency from the customer's perspective: High. They are unable to edit this profile. 
- Critical CRM feature or Org Inaccessible to Users: No 
- Red Account: No 
- Number of users impacted: multiple 
- Renewal at risk: No 
- New deal at risk: No 
- Estimated $ impact: N/A 
- Has this been Escalated to Senior Management, if so, who: No
Tier 3 will run the following DefaultTabsetMissingForCSNProfiles scrutiny in order to fix the issue.
