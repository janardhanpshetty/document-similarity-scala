### Topic: When logging into Lightning For Outlook you may see this error: " We encountered a problem. Contact your administrator and mention that Salesforce is not setup to work with your Exchange server.
When logging into 
Lightning for Outlook 
you may see this error: 
 We encountered a problem. Contact your administrator and mention that Salesforce is not setup to work with your Exchange server
.
 
Resolution
This error is caused by having the wrong Exchange Metadata URL in your Lightning For Outlook settings in your Salesforce org.
If you are using a on Premise Exchange 2013 you will need to obtain the "Exchange Metadata URL" from your Exchange server.
Note*
In order gather this URL we are providing an app that installs in the Exchange 2013 add-in applications. 
To get the "Exchange Metadata URL" perform the following steps:
 
1. Download the "T
okenDecoder.xml
" included in this article
2. Log in to your Exchange 2013 web interface and navigate to the 
Exchange Admin Center
3. Click on the 
Organization | Apps 
or 
Organization | Add-ins
 
4. Click on the 
+
 Symbol and select 
Add from File
5. Browse to the location where the 
TokenDecoder.xml
 was downloaded to and click on "Next" followed by "Install" and "OK"
6. Make sure the Token Decoder user default value is set to Enabled, by default it may be set to Disabled and as a result the Token Decoder tab not loading. If it is disabled, depending on the version of your Exchange you either need to click on the add-in and select 
Enable 
from the right pane or select the add-in and then click on 
Edit 
and change the 
Specify the user defaults:
  value to
 Optional. enabled by default
 and click on 
Save
7. Now go back to Outlook and select any email in your Inbox you will now see a new tab called
 Token Decoder
8. Select any email and then click on the "Token Decoder" tab and you will be able to see the "Exchange Metadata URL"
9. Take the "Exchange Metadata URL" and add it to the 
Exchange Metadata URL 
field in Salesforce and click the 
Save
 button to apply the changes.  
NOTE 
you can add multiple URLs here separated by commas as there could be a couple of different URLs for different Exchange users.
10. Also add the domains that pertain to your Exchange by clicking on the 
New
 button under the 
Microsoft® Outlook® Web App Whitelist
 options and click  on 
Save
 
Note** 
If the Data for the fields does not populate try using a different browser.
11. Now log back into your Outlook and select an email to load the Salesforce button
12. Click on the Salesforce button 
13. The App will prompt you to login, click on the 
Log in to Salesforce 
blue button
14. The Salesforce login page will open up in the browser, enter your credential and login
* If clicking on the blue 
Log in to Salesforce
 button does not open the page you can apply the 
IE optimal settings article
 and try again
You can download the Token Decoder 
here 
!!
See Also:
Lightning For Outlook
 
