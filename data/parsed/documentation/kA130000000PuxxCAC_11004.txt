### Topic: Password Policy Limits | Salesforce
Password Policy Limits | Salesforce
Password Policy Limits
Limits for the number and use of user passwords.
You can set various password and login policies to secure your organization.
Note
User passwords cannot exceed 16,000 bytes.
Logins are limited to 3,600 per hour per user. This limit applies to organizations created after Summer ’08.
See Also:
Password Policy Fields in Profiles
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_passwords.htm&language=en_US
Release
202.14
