### Topic: Support can only help customers creating HTML with Letterhead templates.
Salesforce support can help customers with the default HTML with Letterhead and Plain Text templates.  Custom HTML templates are out of scope and should be composed by users familiar with HTML.  
Resolution
Email Template Editors:
We fully support the standard functionality of the email template editors 
available in Salesforce (formatting controls, merge fields, etc).
HTML (using Letterhead) templates
:
These templates are fully supported by Salesforce since we provide an easy to use HTML editor (where you can add images, links, bullet points, etc) and no HTML code is required.
For HTML (using Letterhead) templates, users should not directly copy/paste the template body from a formatted document.  Instead, the body should be written in the Salesforce template page, and formatted using the formatting controls available in the template editor.
If, however, the text is coming from an prepared document, it should be copied from the source (such as Microsoft Word) into a plain text editor (like Notepad), and then copied from the plain text editor to the template body in Salesforce.  This will remove any hidden formatting controls copied from the source which may not displayed correctly in the email recipient's email client.  User 
will need to use the formatting controls available in the email template editor to re-create the formatting that was done in Microsoft Word.
Custom (without using Letterhead) templates
:
Custom (without using Letterhead) templates are templates created from HTML code written outside Salesforce that is copied to the HTML body in Salesforce.
Custom HTML templates are not supported by Salesforce. While we can help with the merge field functionality here, any other issues related to their content (such as incorrect formatting, invalid links, proper use of HTML, and the like) need to be addressed by the customer.  This will require some knowledge of web development.
Template Design:
Template design decisions should be in the hands of an organizations marketing department.
Here are some "Best practices"  to note:
-Test the email template before using it.
-View the email template in different email clients such as Gmail and Outlook and screens sizes 
(monitor, tablet, phone) 
before sending it to make sure it will appear as expected.
