### Topic: Translation Files | Salesforce
Translation Files | Salesforce
Translation Files
Salesforce provides different files types to translate customization labels and review translations.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Source: Use to translate labels for the first time.
Untranslated: Use to translate labels after the first translation pass.
Bilingual: Use to review and edit translations.
Translation files are identified by the extension 
.stf
, to represent the Salesforce translation format. A translation filename includes the name of the export option used to create it, the language code for the file's content, and a date stamp.
Multiple 
.stf
 files created with the Untranslated and Bilingual options are compressed into zip files up to 5 MB in size. If multiple zip files are needed, the zip filenames are each date stamped and incremented. For example, 
Untranslated 2010–09–20 05:13 1of2.zip
Warning
Consider the following when editing your 
.stf
 files:
Don't change the 
.stf
 file extension.
If you use tabs, new lines, or carriage returns in your text for translation, notice that they are represented with special characters in the 
.stf
 file format. Tabs are 
\t
, new lines are 
\n
 and carriage returns are 
\r
. To ensure consistency between your language versions, ensure these characters are maintained in your translations.
Working with the Source File
Use the Source file to translate an organization's labels for the first time. The Source file contains labels for all of an organization's translatable customizations in the organization's default language.
If you aren't using a standard translation tool such as Trados, work with the file using an application that supports tabs and word wrap, such as WordPad or MS Excel.
Note
If you use MS Excel to enter translations in your 
.stf
 file, your file format may be corrupted. MS Excel automatically adds quotation marks around entries that have commas. We advise you open your files in a text editor before import and remove these quotation marks if they have been added. The import will fail if these quotation marks are not removed.
To prepare the Source file for your translators:
Create one copy of the Source file for each language you are translating into.
In the header of each Source file, change the language code from the organization's default language (such as 
en_US
) to the translation language (such as 
fr
).
Tell your translators to replace the untranslated values in the LABEL column with translated values.
Note
Don't add columns to or remove columns from the translation file.
Column
Description
Edit Options
KEY
Unique identifiers for labels
Do not edit
LABEL
Labels that are visible to end users
Replace untranslated values with translated values
Working with the Untranslated File
Use the Untranslated file to translate labels that haven't been translated. One Untranslated file is generated for each language. When multiple files are generated, they're exported to a 
.zip
 file containing 
.stf
 files for each translation language.
If you aren't using a standard translation tool such as Trados, work with the file using an application that supports tabs and word wrap, such as WordPad or MS Excel.
Note
If you use MS Excel to enter translations in your 
.stf
 file, your file format may be corrupted. MS Excel automatically adds quotation marks around entries that have commas. We advise you open your files in a text editor before import and remove these quotation marks if they have been added. The import will fail if these quotation marks are not removed.
Tell your translators to replace the untranslated values in the LABEL column with translated values.
Note
Don't add columns to or remove columns from the translation file.
Column
Description
Edit Options
KEY
Unique identifiers for labels
Do not edit
LABEL
Labels that are visible to end users
Replace untranslated values with translated values
Working with the Bilingual File
Use the Bilingual file to review translations, edit labels that have already been translated, and add translations for labels that haven't been translated. One Bilingual file is generated for each translation language.
The TRANSLATED section of the file contains the text that has been translated and needs to be reviewed. The UNTRANSLATED section of the file contains text that hasn't been translated.
Edit the file using an editing application that supports tabs and word wrap, such as WordPad or MS Excel.
Note
If you use MS Excel to enter translations in your 
.stf
 file, your file format may be corrupted. MS Excel automatically adds quotation marks around entries that have commas. We advise you open your files in a text editor before import and remove these quotation marks if they have been added. The import will fail if these quotation marks are not removed.
Identify labels that are out of date by scrolling through the OUT OF DATE column to locate values that have an asterisk (
*
). Update out of date labels as needed.
Edit translated labels in the TRANSLATION column of the TRANSLATED section.
Replace untranslated labels with translated values in the LABEL column of the UNTRANSLATED section.
Delete a translation by replacing the desired value in the TRANSLATION column in either section with a left and right angle bracket pair (
< >
). When the Bilingual file is imported, the label reverts to its original value.
Attention
Don't attempt to delete a translation by deleting a translated label from the file. Deleting a translation in the file doesn't remove the translation from the application after the file is imported.
Note
Don't add columns to or remove columns from the translation file.
Column
Description
Edit Options
KEY
Unique identifiers for labels
Do not edit
LABEL
Labels that are visible to end users
Do not edit labels in the TRANSLATED section of the file
In the UNTRANSLATED section of the file, replace untranslated labels with translated values
TRANSLATION
Current translation
In the TRANSLATED section of the file, edit current translations
In the UNTRANSLATED section of the file, add translations
OUT OF DATE
Indicates whether the source text has changed since the previous translation.
The out of date indicators are:
An asterisk (
*
): The label is out of date. A change was made to the default language label and the translation hasn't been updated.
A dash (
-
): The translation is current.
Do not edit
See Also:
Exporting Translation Files
Import Translated Files
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=translation_file_description.htm&language=en_US
Release
202.14
