### Topic: Can we change the format for the auto numeric field "Article Number" on the Knowledge article object
Can we change the format for the auto numeric field "Article Number" on the Knowledge article object
Resolution
Please be advised that currently the knowledge article number field cannot be customized and its value cannot be modified as well. Please see the documentation below for further details
https://www.salesforce.com/developer/docs/api/Content/sforce_api_objects_knowledgearticle.htm
specifically the field named "Article Number"
Properties
Autonumber, Defaulted on create, Filter, Sort
The unique number automatically assigned to the article when it's created. You can't change the format or value for this field
