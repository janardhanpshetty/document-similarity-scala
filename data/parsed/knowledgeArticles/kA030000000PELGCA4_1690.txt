### Topic: Want to go beyond simply finding documents? Would you like to collaborate with other business areas? And wouldn't it be great to make the most of your institutional knowledge by sharing content? Follow these recommendations to set you up for success as you roll out the content library.
5 steps to using the content library
Make sure everyone in your organization always has the latest sales presentations, collateral, and other documents right at their fingertips.
The content library license is included in all editions at no additional cost. As the administrator, you can control how users can take advantage of this feature in your organization.
To get up and running with the content library, make sure it is enabled for your organization, go to: 
Setup 
➤ 
Customize 
➤ 
Salesforce
 Files
 
➤ 
Settings 
➤ 
Salesforce CRM Content
. 
Step 1: Create Libraries and Library permissions
Libraries determine who can access documents, and Library permissions determine who can do what to those documents. As you get started with the content library for your organization, define the following information for Libraries:
Set library permissions 
– You can define any permission you want each library member to have in addition to simply viewing content. When you define user permissions, it’s key to balance content access with security considerations.
Start with a “Sales Content” 
library – Use this library to store official sales materials and give marketing and sales operations users “author” permissions. Sales representatives should have “view” (and potentially “comment/tag”) permission. If your organization uses Google Docs, encourage library members to add their Google docs to a library.
Add a second “Marketing” or “Sales Collaboration” library
 – Give marketing a “draft room” to collaborate on materials before sharing them with the sales team. And let that team share its own informal materials, such as RFP responses.
Enable groups
 – To simplify user administration, use existing public groups or create new groups using your company’s role hierarchy. Add individual users as needed and decide which users or groups can see what content.
 
Step 2: Organize materials
Streamline the user experience of searching and navigating by clearly defining tagging rules, consistently classifying content, and creating custom fields and content types that are relevant to your organization. 
Determine what types of tags make sense for your organization 
– You can use open, guided, or restricted tagging:
Open tagging
 – Lets users enter any tags they want
Guiding tagging
 – You suggest tags to choose from or let users add their own
Restricted tagging
 – Lets users choose only from the suggested (guided) tags
In general, avoid using too many tags. Because the search engine will help users find specific content and because tags help users browse and subscribe, excessive tagging will create confusion.
Ensure content is classified consistently
 – Work with your contributors to define consistent tag names. By establishing tag names from the outset, you can avoid tags such as “channel” and “channels” in the same Library.
Create custom fields and content types – 
You can use content types to manage multiple custom fields to represent different types of content. Think of content types as “record types for documents.”
You can define which content types can be used in each Library. Use the standard fields for basic organization: Author, File Type, Tag, Library, and any custom fields relevant to your business or business process. If you use the Related Content lists on Salesforce objects, align your custom content fields with the standard and custom fields on the objects to increase the accuracy of a “find content” search.
Step 3: Integrate the content library with other Salesforce applications
When you integrate the content library with other applications, users won’t have to change applications to get their work done.
Provide access to the content library from other applications – 
Expose selected content tabs in other applications to make it easy for users to find content from any application.
Recommend content based on record data – 
You can suggest relevant content based on specific fields on commonly used Salesforce objects. As a result, you’ll eliminate the need to search for content when reps have already given you key information and also improve the chances of their using the right information.
Associate content with specific records – 
You can link files as you publish them to records in Salesforce as long as those files consist of generic content that’s not private to a record. Be sure to establish logical connections between data and documents, such as campaigns and any underlying assets. In addition, be sure to align your categorization model in the content library with your data model in CRM and other areas.
Step 4: Migrate existing content
When you move materials into the content library, take the opportunity to “clean house” and determine what materials to keep, who should own the materials moving forward, and how the materials fit into the new classification structure.
       
Follow these steps for your migration plan: 
Create an inventory of all existing content
Remove outdated materials and identify new content owners
Develop a “straw man” approach to organizing content
Upload a sample piece of content for end users to test  
Complete the final migration
Step 5: Launch content to your organization
A well-designed rollout is your key to success, so be sure to cover all your bases in advance: 
Confirm sponsorship, training, and tactics
 – It’s crucial to get the support of your executives so they can put their weight behind the rollout. Once you have executive sponsorship, train your users to help them see the value of the application and become productive quickly. Also, use the right combination of “carrots” and “sticks” to drive usage.  
Highlight valuable content 
- 
To highlight valuable content and make it easier to find, mark it as “featured” on the content details page. Featured content is assigned a higher priority in search results than similar content and it appears on the Library tab for easy access. To use this feature, be sure to check the Feature Content option when you setup users Library permissions.
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
 
