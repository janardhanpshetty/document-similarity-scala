### Topic: Run a Bulk Macro on Multiple Records | Salesforce
Run a Bulk Macro on Multiple Records | Salesforce
Run a Bulk Macro on Multiple Records
You can run a bulk macro on only one record at a time, or you can run it on multiple records at the same time. Use bulk macros to quickly address similar customer cases or records.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view macros:
“Read” on Macros
To create and edit macros:
“Create” and “Edit” on Macros
To create and run irreversible macros:
“Manage Macros That Users Can’t Undo”
To run bulk macros:
“Run Macros on Multiple Records”
Note
You need the Manage Macros That Users Can’t Undo permission only if you want to run macros that contain a Submit Action instruction. All bulk macros contain a Submit Action instruction. The lightning bolt icon (
) indicates that the macro performs an action—such as sending an email—that cannot be undone.
You can run bulk macros on records from the Accounts, Cases, Contacts, and Leads objects. However, you can run a bulk macro only on records in one object list view at a time. For example, you can run a bulk macro on multiple cases in the Cases list view, but not on cases and accounts at the same time.
An icon showing a green lightning bolt with two underlines (
) indicates whether a macro is a bulk macro.
Bulk macros are processed in increments of 10 macros at a time. You can run a bulk macro on more than 10 cases, but the system processes the macro in groups of 10 at a time.
In the Case list view, select the cases that you want to run the macro on.
You can filter the cases to identify which cases you want to run the macro on.
Open the Macros widget.
In the Macro widget, select a macro with the green lightning bolt icon (
) and click 
Run
.
In the confirmation window, click 
OK
 to continue.
The macro runs on the selected cases. In the list view, the cases on which the macro ran successfully are highlighted in green and denoted by a green check mark icon. Cases that the macro didn’t run successfully on are highlighted in red and denoted by a red X icon. To see an explanation about why the macro didn’t work on a case, hover over the red X icon.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=macros_run_bulk.htm&language=en_US
Release
202.14
