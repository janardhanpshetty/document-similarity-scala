### Topic: Customize Search Layouts | Salesforce
Customize Search Layouts | Salesforce
Customize Search Layouts
Customize which fields display for users in search results, search filter fields, lookup dialogs, the recent records lists on tab home pages in Salesforce Classic, and in lookup phone dialogs for Salesforce CRM Call Center.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To change search layouts:
“Customize Application”
You can specify a different set of fields to show in each search layout. The settings apply to all users in your organization and Salesforce Customer Portal.
You can also customize which buttons display in custom list views and search results. You can hide a standard list view button or display a custom button. Standard buttons aren’t available on search result layouts. To display a custom button, create the custom button, giving it the 
List Button
 
Display Type
.
From the management settings for the appropriate object, go to Search Layouts.
Click 
Edit
 next to the layout you want to customize. Specify a different set of items to display. You can edit search results, lookup dialogs, recent records lists on tab home pages in Salesforce Classic, lookup phone dialogs, list views, and search filter fields.
For list view and search results layouts, select the standard or custom buttons you want to display. To hide a standard button on the list view, deselect it. Standard buttons aren't available on search result layouts.
Move fields between Available Fields and Selected Fields.
To customize which fields display in the layout, select one or more fields and click 
Add
 or 
Remove
.
To sort fields in the layout, select one or more fields in Selected Fields and click 
Up
 or 
Down
.
To select multiple fields individually, use CTRL+click.
To select multiple fields as a group, use SHIFT+click.
To customize which items display in the layout, select the item and click 
Add
 or 
Remove
.
To sort items in the layout, select the item and click 
Up
 or 
Down
.
Note
When editing a search results layout for an object, you can select the 
Override the search result column customizations for all users
 checkbox. If selected, all user column customizations within your organization are overwritten and restored to the organization-wide default settings.
Click 
Save
.
See Also:
Page Layouts
Notes on Search Layouts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_searchlayout.htm&language=en_US
Release
202.14
