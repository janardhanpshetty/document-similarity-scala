### Topic: Salesforce Knowledge administrators must update any article report types that reference knowledge article version fields by using lookup fields for article version fields in the Custom Report Type (CRT).
Due to security issues in using lookup fields in reporting, knowledge article version fields are no longer supported through lookup on the report type layout. Therefore, older article custom report types that reference knowledge article versions fields through a lookup must be updated.
Resolution
To avoid missing fields in reports:
Edit your CRTs to add knowledge article version fields via related objects or create new CRTs
Add knowledge article version fields to your CRT layout
Add knowledge article version fields to your reports
The attached pdf has step-by-step instructions.
