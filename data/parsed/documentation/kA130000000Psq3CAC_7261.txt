### Topic: Find Out How Clean Your Accounts Are | Salesforce
Find Out How Clean Your Accounts Are | Salesforce
Find Out How Clean Your Accounts Are
If you use Data.com Clean, you can review the 
Clean Status
 on all your accounts by running a simple Accounts by Clean Status report.
Available in: Salesforce Classic
Available with a Data.com Prospector license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available with a Data.com Clean license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To implement Data.com Clean:
“Customize Application”
Important
If your records appear to be matched incorrectly with Data.com records, think carefully before selecting 
Flag differences and auto-fill blank fields 
as a clean preference. If mismatches occur with a large number of your records, contact Salesforce Support.
Click the Dashboards tab, then select Data.com Analytics. Run the Accounts by Clean Status report.
If your clean jobs have run successfully, you’ll find your accounts grouped by their clean status values.
On the report, find any Salesforce records with a clean status of 
Different
. Open several records and click 
Clean
 to look at the differences.
Repeat the comparison process with records that have a clean status of 
Not Found
.
See Also:
How Are Salesforce and Data.com Records Matched?
Data.com Clean Statuses
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_reviewing_clean_match_results.htm&language=en_US
Release
202.14
