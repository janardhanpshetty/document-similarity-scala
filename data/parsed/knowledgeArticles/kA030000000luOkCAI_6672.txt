### Topic: this is the process to get the External Objects and External Data sources feature enabled.
Available in:  Enterprise, Performance, Unlimited and Developer Editions
The 
External Objects and External Data sources 
features are part of
 the Lightning Connect which is a license provisioned
 feature.
Resolution
If you've reviewed all relevant documentation and would like to get those feature activated please be informed that activating this functionality must be handled by your 
Account Executive.
Support will not be able to assist you with this increase.
If the organization that need the increase is owned by a Partner:
Have a System Administrator log a Case with Salesforce Partner Support
Please include all important details including the organization ID and a business case for the request
Partner Support will review the Case and action it as needed.
