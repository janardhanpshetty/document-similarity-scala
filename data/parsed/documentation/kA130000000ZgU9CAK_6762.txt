### Topic: Live Agent Configuration Settings | Salesforce
Live Agent Configuration Settings | Salesforce
Live Agent Configuration Settings
Live Agent configuration settings control the functionality that’s available to agents and their supervisors while agents chat with customers.
Available in: Salesforce Classic
Live Agent is available in: 
Performance
 Editions and in 
Developer
 Edition orgs that were created after June 14, 2012
Live Agent is available in: 
Unlimited
 Edition with the Service Cloud
Live Agent is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions
Apply settings when you create or edit a Live Agent configuration.
Basic Information
Configure the basic functionality that’s available to agents when they chat with customers.
Setting
What It Does
Live Agent Configuration Name
Names the configuration.
This configuration name, or a version of it, automatically becomes the 
Developer Name
.
Developer Name
Sets the API name for the Live Agent configuration.
Chat Capacity
Indicates how many chats an agent who is assigned to this configuration can be engaged in at the same time.
Sneak Peek Enabled
Indicates whether agents can see what a chat customer is typing before the customer sends a chat message.
Request Sound Enabled
Indicates whether to play an audio alert when the agent receives a new chat request.
Disconnect Sound Enabled
Indicates whether to play an audio alert when a chat is disconnected.
Notifications Enabled
Indicates whether to display a desktop alert when an agent receives a new chat request.
Custom Agent Name
Sets the agent’s name as it appears to customers in the chat window.
Auto Greeting
Sets a customized greeting message that the customer receives automatically when an agent accepts the customer’s chat request.
Optionally, use merge fields to customize the information in your greeting by using the Available Merge Fields tool. For example, you can personalize the chat experience by using merge fields to include the customer’s name in the greeting.
Note
If you specify an automatic greeting message in both your Live Agent configuration and in an individual chat button, the message that’s associated with your chat button overrides the message that’s associated with your configuration.
Auto Away on Decline
Sets the agent’s Live Agent status to “Away” automatically when the agent declines a chat request.
This option applies only when agents are assigned to chat buttons that use push routing.
Auto Away on Push Time-Out
Sets an agent’s Live Agent status to “Away” automatically when a chat request that's been pushed to the agent times out.
This option applies only when agents are assigned to chat buttons that use push routing.
Critical Wait Alert Time
Determines the number of seconds that the agent has to answer a customer’s chat before the chat tab alerts the agent to answer it.
Agent File Transfer Enabled
Indicates whether an agent can enable customers to transfer files through a chat.
Visitor Blocking Enabled
Indicates whether an agent can block visitors from an active chat within the Salesforce console. See 
Let Your Agents Block Visitors by IP Address
.
Assistance Flag Enabled
Indicates whether an agent can send a request for help (“raise a flag”) to a supervisor.
Chatlets
Chatlets are tools that are available only to organizations that use Live Agent in the Live Agent console. The Live Agent console is no longer supported, so we don’t recommend setting up chatlets. But don’t worry—if you use Live Agent in the Salesforce console, you don’t need chatlets.
Assign Users
Assign eligible users to the configuration to give them access to Live Agent functionality. Later, you’ll see that you can also assign profiles to a configuration. If a user is assigned a configuration at the profile and user levels, the user-level configuration overrides the configuration that’s assigned to the profile.
Warning
Users can be assigned to only one Live Agent configuration at a time. If you assign the same user to a second Live Agent configuration, the system removes that user from the first Live Agent configuration without warning you. So make sure that you know exactly which Live Agent configuration each user should be assigned to!
For example, let’s say that User A is assigned to Live Agent Configuration A. Then, you create Live Agent Configuration B and accidentally assign User A to it. Salesforce automatically removes User A from Live Agent Configuration A and reassigns the user to Live Agent Configuration B without notifying you.
Setting
What It Does
Available Users
Indicates the users who are eligible to be assigned to the configuration.
Selected Users
Indicates the users who are assigned to the configuration.
Assign Profiles
Assign eligible profiles to the configuration to give users who are associated with the profiles access to Live Agent functionality. If a user is assigned a configuration at the profile and user levels, the user-level configuration overrides the configuration that’s assigned to the profile.
Setting
What It Does
Available Profiles
Indicates the user profiles that are eligible to be assigned to the configuration.
Selected Profiles
Indicates the user profiles that are assigned to the configuration.
Supervisor Settings
Supervisor settings determine the Live Agent functionality that’s available to support supervisors. In addition, these settings determine the default filters that apply to the Agent Status list in the supervisor panel.
Setting
What It Does
Chat Monitoring Enabled
Indicates whether supervisors can monitor their agents’ chats in real time while their agents interact with customers.
Whisper Messages Enabled
Indicates whether supervisors can send private messages to agents while agents chat with customers.
Agent Sneak Peek Enabled
Indicates whether supervisors can preview an agent’s chat messages before the agent sends them to the customer.
Default Agent Status Filter
Determines the default agent status, such as Online, Offline, or Away, by which to filter agents in the supervisor panel.
When supervisors view the Agent Status list in the supervisor panel, they see a list of agents who have that status.
Default Skill Filter
Determines the default skill by which to filter agents in the supervisor panel.
When supervisors view the Agent Status list in the supervisor panel, they see a list of agents who are assigned to that skill.
Default Button Filter
Determines the default button by which to filter agents in the supervisor panel.
When supervisors view the Agent Status list in the supervisor panel, they see a list of agents who are assigned to that button.
Assigned Skills
Determines the skills that are visible to supervisors in the supervisor panel.
When supervisors view the Agent status list in the supervisor panel, they see a list of agents who are assigned to these skills. If you don’t select any skills, the Agent Status list displays agents who are assigned to any skill.
Chat Conference Settings
Determine whether agents can invite other agents to join them in a customer chat. Chat conferencing lets your agents include multiple agents in a single chat. That way, your agents can help your customers get the solutions that they need without making your customers wait for their chats to be transferred.
Note
Chat conferencing does not support the Related Entities panel. If you attempt to use it with chat conferencing, important details might not be saved on your record.
Setting
What It Does
Chat Conferencing Enabled
Indicates whether agents can invite other agents to join them in customer chats.
Chat Transfer Settings
Determine how agents can transfer chats to other agents.
Setting
What It Does
Chat Transfer to Agents Enabled
Indicates whether agents can transfer chats to another agent directly.
Chat Transfer to Skills Enabled
Indicates whether agents can transfer chats to agents assigned to a particular skill.
Chat Transfer to Skills
Determines the skill groups to which agents can transfer chats.
Agents can transfer chats to available agents who are assigned to those skills.
Chat Transfer to Live Chat Buttons Enabled
Indicates whether agents can transfer chats to a button or queue.
Chat Transfer to Live Chat Buttons
Determines the buttons to which agents can transfer chats.
Agents can transfer chats to available agents who are assigned to those buttons.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=live_agent_configuration_settings.htm&language=en_US
Release
202.14
