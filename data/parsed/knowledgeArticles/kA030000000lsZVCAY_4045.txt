### Topic: This article explains the permission needed to create or run a report based on leads with converted lead information. There are few user permission and object permission which is discussed in this article
What are the permission needed to create or run a report based on Leads with converted lead information report type?
Resolution
The user's profile must have the following permission:
A. User Permission:
Create and Customize Reports
Report Builder
B. Object Permission :
Lead **
Opportunity **
Account
Contact
** In order to show the "
Leads with converted lead information" report type in the selection of  Report Type when creating a report, the user's profile must have a read permission at the minimum on Leads and Opportunity objects.
 
