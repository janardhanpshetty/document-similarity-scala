### Topic: Learn how to prevent cases from being generated from bounced or OOO emails by using Apex and Email2Case.
It's possible to control Email2Case creation, but it'll require some development knowledge. To help get you started we've provided some sample code on how to prevent case creation from bounced or out of office emails.
Resolution
First, make sure you assign a Case Origin specific to e-mail to case.
With that in place, you can create a trigger on case, for a 
before insert
 action, similar to this, to detect email2case cases about to get inserted, and check/block their progress:
Example code
trigger manageEmailCases on Case (before insert) {

    // go through our new cases and see if any email2case, "process" them
    for(Case thisCase : trigger.new) 
    {
        if(thisCase.Origin == 'Support Email') {
            /* Here's where to do your work
            if(thisCase.Subject.contains('OOO')) {
                thisCase.name.addError('Not saving e2c');
            }
            */
        }
    }

}
 
What you put in the logic depends entirely on organizational requirements. 
Hard coding known origins or subjects is an option, but you'll also want to be defensive of "swallowing up" cases with a bit of bad logic. It would be more apt to manage their fields, such as auto-close them, or add a "for review" flag instead.
If this is a feature you'd like to see added to Salesforce, 
vote for the idea on IdeaExchange
.
