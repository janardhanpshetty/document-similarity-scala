### Topic: This outlines the steps to be taken and the information that needs obtained when troubleshooting and escalating Predictive Intelligence cases.
For all questions related to Predictive Intelligence you will want to first understand whether or not the client is working in the Predictive Web or Predictive Email portion of Predictive Intelligence because this will change the information that you will need to gather.
You will want to gather the below information prior to escalation to Tier 3 or Product Operations.
Resolution
Predictive Web
If the client is having issues with Predictive Web locate the page on their site that is producing recommendations. 
Launch URL to test > Launch Developer Tools > Refresh the page
Search for “record” or “track” to find the collect call. 
If the Status Code: 200 OK move along to gathering the recommendation call
If the Status Code: 500 escalate this to Product Operations noting that the Predictive Web system is down 
Search for “recommend” or “igo” for the recommendation calls 
Use the header tab to find out more information about what is being collected or recommended. Be sure to make note of this for any escalation to Tier 3.
Predictive Email
For Predictive Email you will want to understand the email in particular they are dealing with, the subscriber they are seeing the issue with, and a diagnosis of the behavior they see versus what they are expecting to see.
