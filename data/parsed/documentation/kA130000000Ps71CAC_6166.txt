### Topic: Displaying and Editing a Campaign Member | Salesforce
Displaying and Editing a Campaign Member | Salesforce
Displaying and Editing a Campaign Member
Available in: Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view, edit, or remove campaign members:
“Read” on campaigns
AND
“Read” on leads
AND
“Read” on contacts
To add campaign members:
“Read” on campaigns
AND
“Edit” on leads
AND
“Edit” on contacts
From the campaign member detail page, you can edit, delete, or clone a campaign member record. Additionally, if the campaign member is based on a lead, you can convert the lead; if the campaign member is based on a contact, you can create an opportunity.
To view the campaign member detail page, click the campaign member's name in the Name, First Name, or Last Name columns on the Campaign Members related list on a campaign detail page or on the Existing Menbers tab.
Note
To view a campaign member, you must have permissions on the campaign and the lead or contact. For example, to view a campaign member created from a lead, you must be able to view both the campaign and the lead.
To edit the campaign member detail page layout, from the object management settings for campaign members, go to Page Layouts, and then click 
Edit
 next to the page layout name.
Editing a Campaign Member
To edit a campaign member, click 
Edit
 on the campaign member detail page, change the fields you want to update, then click 
Save
. Fields derived from the contact or lead can only be edited from the lead or contact detail pages.
Note
If your administrator has enabled inline editing for your organization, you cannot use inline editing on the 
Status
 drop-down list on the campaign member detail page. Instead, click 
Edit
 on the campaign detail page to edit the 
Status
 drop-down list.
Cloning a Campaign Member
To clone a campaign member, click 
Clone
 on the campaign member detail page, then select a campaign and a contact or lead. Change the fields you want to clone for the new campaign member, then click 
Save
.
Deleting a Campaign Member
To delete a campaign member, click 
Delete
 on the campaign member detail page.
Note
Deleting a campaign member record is permanent; the record is not recoverable from the recycle bin. However, the original lead or contact record is not deleted.
Converting a Lead
If the campaign member was created from a lead, you can click 
Convert Lead
 on the campaign member detail page to convert the lead. When you convert a lead, the campaign member is still a part of the campaign, but its type changes from lead to contact.
Creating an Opportunity
If the campaign member was created from a contact, you can click 
Create Opportunity
 on the campaign member detail page to create an opportunity. When you create an opportunity, the campaign member is still a part of the campaign.
See Also:
Find Object Management Settings
Campaign Member Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=campaigns_members_detail.htm&language=en_US
Release
202.14
