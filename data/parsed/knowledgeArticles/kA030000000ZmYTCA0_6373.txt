### Topic: This article will go over how MX records
Salesforce or any other mail application will try to deliver mail to the hosts specified in the MX records for the domain in the users email address.  For example, considering the email address test@salesforce.com the sending MTA (Mail Transfer Agent) will use DNS to lookup the MX record associated with the Salesforce domain.  You can check this from most machines from the command line however this is the view of DNS you are getting from your domain which could be different to the DNS information available to users on the internet. To try to validate a more global view of DNS try a tool such as MXtoolbox to check MX records.
 
 
Resolution
MXtoolbox example:
http://mxtoolbox.com/SuperTool.aspx?action=mx%3asalesforce.com&run=toolpage
 
MTAs will always try to deliver to the MX record with the lowest preference in the MX list. If they cannot deliver to this MX they will try deliver to the one with the next highest preference.  If MTAs cannot deliver to any legitimate MX record for the domain they will generally try attempting delivery at increasing intervals for periods from about 4 hours up 72 hours depending on how the MTA has been configured.  Salesforce MTAs do this for up to approximately 24 hours.
 
If organizations are using email relaying in Salesforce our MTAs do not use MX records as with normal delivery. They only send mail to the hostname or IP address in the email relay settings. A best practice for email relaying is to not use an IP and always use a hostname and try to have the hostname have an associated MX record that can facilitate redundancy on the organizations side.
