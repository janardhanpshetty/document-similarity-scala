### Topic: How do I check the code coverage of my Org?
I want to know the actual code coverage of my org. What is the impact of managed package on code coverage in my org?
Resolution
>> "Run All Test" button gives the code coverage of the whole org i.e. custom code that you written + code coverage of the managed package.
>> "Calculate your organization's code coverage" gives the code coverage of the custom code only. In its results the managed package code coverage is not counted
