### Topic: Configuring Office 365 implementations for email relay
The out of the box Office 365 connectors are not configurable to accept and pass to the Internet email from a third party system on Port 25 where the senders address contains your domain name.  Salesforce sends and relays mail using your corporate email as the senders email address meaning a connector cannot be directly configured in Office 365 to accommodate this. Office 365 will accept mail on Port 587 in this scenario but it needs SMTP authentication which is not available with the Salesforce server to server relay functionality. 
Office 365 needs to accept mail from a device in your own domain space before you can set a connector to relay the mail on to the Internet.  To achieve this consider setting up an IIS or other relay server in your domain space to accept mail from Salesforce before passing this to your 365 implementation for either internal delivery and also relay to the internet.
Consult with Office 365 support for more information about this scenario.
See also: 
https://technet.microsoft.com/en-us/library/dn554323%28v=exchg.150%29.aspx
 
Resolution
