### Topic: There is a limit to the number of email addresses that can be entered in My Acceptable Email Addresses field.
I would like to add more than one email to my "My Acceptable Email Address". Is there any limitation on characters to include?
Resolution
'My Acceptable Email Addresses' field gives you the option to add email addresses from which you want to add emails to salesforce. This field is available to all users in salesforce with a valid license. The limit for "My Accepted Email Addresses" field is 3500 characters. 
 
