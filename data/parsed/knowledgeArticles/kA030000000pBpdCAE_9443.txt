### Topic: SSO Time zone issue using JIT Provisioning
When creating new standard and portal user records in Salesforce via an IDP (Identity Provider) using JIT Provisioning, Customer gets the following errors based on timezone issues.
Getting errors: Example
===========
ErrorCode=5& 
ErrorDescription=Unable+to+cre
ate+user& 
ErrorDetails=Required+fields+a
re+missing%3A+[Time+Zone] 
Example Ti
mezonesidkeys that are causing issues:
=======================
>> Mexico/BajaSur 
>> 
Asia/Saigon 
>> America/Atka 
>> Asia/Colombo 
>> Asia/Katmandu 
As per the Salesforce Documentation, the above mentioned timezones are supported.
 
https://help.salesforce.com/HTViewHelpDoc?id=admin_supported_timezone.htm&language=en_US
) 
Resolution
1. The help doc ( https://help.salesforce.com/HTViewHelpDoc?id=admin_supported_timezone.htm&language=en_US ) is outdated 
2. TimeZoneSidkeys were updated in 186, but document still reflecting outdated valued: 
"America/Indianapolis" replacedBy="America/Indiana/Indianapolis" 
"Asia/Calcutta" replacedBy="Asia/Kolkata" 
"Asia/Saigon" replacedBy="Asia/Ho_Chi_Minh" 
"America/Atka" replacedBy="America/Adak" 
"Mexico/BajaSur" replacedBy="America/Mazatlan" 
"Asia/Katmandu " replacedBy="Asia/Kathmandu" 
"America/Buenos_Aires" replacedBy="America/Argentina/Buenos_Aires" 
"Asia/Dacca" replacedBy="Asia/Dhaka" 
Refer the below Chatter Thread where Chuck Mortimore and T-3 responded:
========
https://org62.my.salesforce.com/0D53000001fXHEW
A Gus investigation is created for the same to update the outdated document:
https://gus.my.salesforce.com/apex/adm_investigationdetail?id=a07B00000012pkpIAA&sfdc.override=1
 
