### Topic: Announcing the End of Life for the Inbound Link Count functionality in the Radian6 Analysis Dashboard application
As of July 31, 2016 the Inbound Link Count in the Radian6 Analysis Dashboard application will no longer be supported.
Resolution
What’s changing?
The Inbound Link Count (ILC) metric measures the number of users who have linked to the post from another post (full or shortened URL), and is used in calculating influence for blogs, videos, and forums. The ILC metric is captured in the Analysis Dashboard in the following locations:
 
1. River of News metric (non Twitter).
2. Influencer hover metrics (non Twitter).
3. Influencer calculation, equalizer.
Effective July 31st, the ILC will no longer show any data values. 
Why are we making this change?
ILC is captured on a very small amount of data, and we have made the decision to focus on development of influencer identification and analysis in our latest social product, Social Studio. 
If you have further questions, please contact marketingcloudsupport@salesforce.com
 
