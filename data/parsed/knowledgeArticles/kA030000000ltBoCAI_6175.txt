### Topic: Salesforce for Outlook is a powerful tool. It can sync Contacts, Events, and Tasks. You can also use it to add emails from Outlook to Salesforce. Please note that there is a difference between "adding" and "syncing" emails."Syncing Emails" is a misnomer, as it's not an automated process, like syncing Contacts or Events.
When using 
Salesforce for Outlook
, you can add emails using the
 Side Panel
, but they don't automatically sync over to Salesforce when you click on an email in Outlook. 
Good to know:
 "Adding" and "syncing" emails are different functions. "Syncing Emails" is a misnomer, as it's not an automated process, like syncing Contacts or Events.
Resolution
Flow for adding Emails to Salesforce from Outlook
1. When you select an email in your Outlook, the 
Side Panel
 will perform an automatic search to match the email addresses in the 
From
, 
TO 
and 
CC 
fields with record(s) in your Salesforce Organization. This may take a few seconds depending on your computer and connection speed. This will search all the email fields in Salesforce, including Standard and Custom email fields.
2. Next, the Side Panel will display all the matching records (
maximum number of records that can be displayed in the Side Panel is 10
).
3. 
Now you can associate the email by clicking on the envelope icon next to the name, company, or any other contact details displayed in the Side Panel.
 
 
 
4. Once the email is added, the envelope will turn green and a green banner on top of the Side panel notifying you that the
 Email Added to: <record name>.
 
 
Note:
 If the email you're adding has attachments after you click on the envelope icon, then it turns green, you will see the option to add one or more of those attachments on top of the Side Panel, above your record. You can click on the paper clip icon next to each attachments to select and include them. Once the attachment is selected, the paper clip icon will turn green as well.
5. Now if the Side panel doesn't display the specific record you want to add the email to, you can perform a manual search by clicking on the magnifying glass icon 
 and typing in the name of the record (
Search phrase must be 2 or more characters
) and click on the magnifying glass icon again to display the results.
 
Example:
  When performing a manual search for 'al" (example search), you can click on the magnifying glass and the Side Panel displays the Contact. You can type in the full name as well.
 
 
Add emails to Salesforce from Outlook when composing and sending emails
Alternatively, you can perform this task when composing and sending a new email. When you click on the "New" email button, you can type in the email addresses or select anyone from your address book into the 
TO
, 
CC
, or/and
 BCC
 fields. The Side Panel will perform an automatic search for these names and if it finds any matching record it will display them. 
 
Tip: 
You can fo
llow the steps in the above section to associate the emails and the envelope will turn green but the email won't be associated until you hit the 
Send
 button in Outlook.
 
