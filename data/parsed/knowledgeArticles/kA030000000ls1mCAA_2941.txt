### Topic: Third party applications might encounter this error message when performing an inbound call (target is Salesforce) if they do not support strong ciphers. This article provides some suggestions third party middleware administrators can follow to address this problem.
Third party applications might encounter this error message when performing an inbound call (target is Salesforce) if they do not support higher strength ciphers. To resolve this issue, the affected application needs to be updated as per the provider instructions. This article provides some information that has been used successfully by affected customers to solve this issue.
Resolution
TIBCO
A TIBCO installation with this issue may require updating to a newer version. Installing the Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy (where available) into your TIBCO JRE will prevent this issue and will allow for the highest strength cipher suites to be used. The files can be found at the download page for the J2SE used under the heading Other Downloads:
For Java Version 6: 
http:// www.oracle.com/technetwork/java/javase/downloads/jce-6-download-429243.html
For Java Version 7: 
http:// www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html
'Cipher not initialized' on Weblogic or Fusion Middle Ware (FMW) boxes
Weblogic or Fusion Middle Ware integrations will have to be enabled with "Strong Ciphers" by following the steps listed here:
WebLogic: 
http://docs.oracle.com/cd/E13205_01/wcp/wng10/userguide/installation.html#1053703
Oracle Application servers: 
http://docs.oracle.com/cd/E19644-01/817-5447/sgencryp.html#wp15647
This will require you download and install Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files. 
 
