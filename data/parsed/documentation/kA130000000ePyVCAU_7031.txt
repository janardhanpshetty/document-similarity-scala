### Topic: Drive Learning with Protocols and Articles | Salesforce
Drive Learning with Protocols and Articles | Salesforce
Drive Learning with Protocols and Articles
Salesforce Knowledge lets you easily create and manage content and make it available to other healthcare professionals and to the patient and care team members. An article can contain the protocols you use to manage conditions or can hold educational materials you send to patients. You can write, edit, publish, and archive articles using the Articles Management tab or find and view published articles using the Articles tab.
Health Cloud is available in Salesforce Classic
Available in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To create articles:
“Manage Articles”
AND
“Create” and “Read” on the article type
To edit draft articles:
“Manage Articles”
AND
“Read” and “Edit” on the article type
To edit published or archived articles:
“Manage Articles”
AND
“Create,” “Read,” and “Edit” on the article type
Authors create articles by selecting an article type, writing content, and selecting where it should be published. You create both articles and protocols from the Article Management tab, but you select a different article type depending on the content you want to create.
Note
It’s possible that not everyone in your organization will have the license type or permissions to create articles and protocols for your patients and care coordinators. Contact your Salesforce administrator for access to the Article Management tab.
On the Article Management tab, click 
New
.
If your organization supports multiple languages, choose the language for the article. 
Choose an article type, enter the article title, and click 
OK
.
Edit the article's fields, and select a validation status. If your article contains a rich text area field, you can add some formatting such as bulleted lists, links, and images. 
Optionally, if your organization uses data categories, select the categories to associate with your article: 
Click 
Edit
 next to a category group to open the category selection dialog box.
In the 
Available Categories
 list, expand the category hierarchy to select a category.
Click 
Add
 to move a selected category to the 
Selected Categories
 list.
Note
You can’t add both a category and its child categories to the 
Selected Categories
 list. When you add a category to an article:
Child categories in the 
Available Categories
 list are unavailable unless you remove the parent from the 
Selected Categories
 list.
Child categories in the 
Selected Categories
 list disappear from that list.
Users searching for articles can find them by selecting an exact category or by selecting a parent or child category.
Click 
OK
.
Select the audience you want to publish the article to:
Internal App: Salesforce communities users can access articles in the Articles tab depending on their role visibility.
Customer: Customers can access articles if the Articles tab is available in a community.. Customer users inherit the role visibility of the manager on the account. In a community, the article is available only to users with Customer Community licenses or Customer Community Plus licenses.
Partner: Partners can access articles if the Articles tab is available in a community. Partner users inherit the role visibility of the manager on the account. In a community, the article is available only to users with Partner Community licenses.
Public Knowledge Base: Articles can be made available to anonymous users by creating a public knowledge base using the 
Sample Public Knowledge Base for Salesforce Knowledge
 app from the AppExchange.
Your own website. Articles can be made available to users through your company website.
Click 
Quick Save
 to save your changes and remain on this page. Alternatively, click 
Save
 to save your changes, close the article, and go to the Article Management tab. 
Click 
Publish...
 when the content is ready to be published. 
Select 
Publish article(s) now
 or 
Schedule publication on
 to choose the date to publish the article.
If the article has previously been published, select the 
Flag as new version
 checkbox to make the new article icon (
) display next to your article in the selected channels. Users from these channels can see that this article has been modified since the last time they’ve read it. This checkbox is not available when you publish an article for the first time, as the icon displays by default for new articles. 
Click 
OK
.
Articles you scheduled for publication at a later date continue to appear in the Draft Articles filter, now with the pending icon (
) next to the article title. Hover over the icon to see the publication date.
See Also:
Create and Edit Articles
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_article_create.htm&language=en_US
Release
202.14
