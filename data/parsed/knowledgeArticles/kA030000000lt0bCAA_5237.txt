### Topic: Detailed steps on how to invite peers when the Invite button does not appear.
The "Invite Peers" button might not appear for the subject of the performance summary, if the manager summary has already been submitted. 
Resolution
The Administrator will need to reopen the submitted manager summary first in order to resolve this issue.
 
1. Login as Administrator
2. Navigate to the Performance tab | Deployment subtab
3. Right next to the cycle in question, click the arrow and select People
4. Search for the subject of the performance summary and you will see self and performance summary
5. Right next to the Performance summary, click on the arrow and select "Re-open"
6. Once you reopen the manager summary, you can ask the subject of the performance summary to verify that the "Invite Peers" button is visible.
