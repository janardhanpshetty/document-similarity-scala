### Topic: Schedule Reports | Salesforce
Schedule Reports | Salesforce
Schedule Reports
You can set up a report to run itself daily, weekly, or monthly and send the results automatically to the people who need to see them, so that you don’t have to remember to log in and do it yourself.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To schedule reports:
“Schedule Reports”
Tips for Scheduling Reports
Schedule a Report for Refresh
Schedule a report to run daily, weekly, or monthly. An HTML version of the report can be sent by email to users in your organization.
View a Report’s Schedule
View a report’s schedule on the Schedule Report page or from the Reports tab. View all report schedules for the organization under Setup.
Manage a Report’s Schedule
Create, change, view or delete a scheduled report from the Schedule Report page.
Change a Report’s Schedule
You can make changes to an already scheduled report on the Schedule Report page.
Delete a Report’s Schedule
Select a scheduled report and unschedule it to delete its scheduled run.
Tips on Scheduling Reports
Some tips to keep in mind about timings, limits, and email notifications when scheduling a report.
Parent topic:
 
Deliver Your Report
Previous topic:
 
Run Reports in the Background
Next topic:
 
Subscribe to Get Report Notifications
See Also:
Schedule a Report for Refresh
Change a Report’s Schedule
Delete a Report’s Schedule
Manage a Report’s Schedule
Tips on Scheduling Reports
View a Report’s Schedule
Subscribe to Get Report Notifications
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_schedule_overview.htm&language=en_US
Release
202.14
