### Topic: Where do I install the Environment Hub? | Salesforce
Where do I install the Environment Hub? | Salesforce
Where do I install the Environment Hub?
If you’re an ISV partner, the Environment Hub is already installed in your Partner Business Org.
Otherwise, install the Environment Hub in an org that all your users can access, such as your CRM org. Do not install the Environment Hub in a Developer Edition org that contains your managed package. Doing so can cause problems when you upload a new package version or push an upgrade to customers.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=environment_hub_faq_where_install.htm&language=en_US
Release
202.14
