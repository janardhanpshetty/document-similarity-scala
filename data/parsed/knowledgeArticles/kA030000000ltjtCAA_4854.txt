### Topic: Navigate your way through the Coaching object.
The Coaching tab lets you:
View the feed related to a coaching space.
 Click 
Show Feed
 to see the feed related to a coaching space. 
Note: Enable Chatter Feed Tracking to display the feed.
Access the goals, metrics, activities, notes, and attachments related to a particular coaching space.
 This page shows the metrics owned by the person being coached. However, metrics related to committed goals will no longer be displayed out of the box.
Set coaching spaces to inactive when they’re no longer useful.
 This retains the record history, and the coaching space can also be reactivated anytime.
Resolution
Access Coaching Records
 
1. On your homepage, click 
Coaching
. 
2. Click on any record to access the detail page.
The detail page is where you can view the Chatter Feed, access related lists, and few Coaching Details and System Information. 
