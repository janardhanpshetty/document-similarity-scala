### Topic: This article will assist you resolving issues with Publisher Action for Side Panel.
The Publisher Action for Side Panel sometime does not work properly or the 
 
 does not appear at all.
Resolution
Please verify the below mentioned to make sure you can get the desired actions under the Publisher Action for Salesforce for Outlook Side Panel:
Step 1:  
Make sure that the Publisher Actions is Enabled for Chatter. Please refer to 
Enabling Publisher Actions in Chatter
             
If you haven’t created global Create a Record actions for the kinds of records you want available to your users, you’ll have to do so now.
Step 2:
 
Verify that you’ve 
set up Create a Record global actions
 for each publisher action you want to be available in your users’ side panel publisher menus.
Add the publisher actions to the appropriate publisher layout.
For Group, Contact Manager, or Professional Editions, you'll 
add publisher actions to your users’ side panel publisher menus with the global publisher layout editor
. If you’re already using the global publisher layout for other Salesforce features, you’ll notice that any publisher actions you add for the side panel are displayed for all features. The side panel publisher, however, displays only actions with the action type Create a Record.
For Enterprise, Performance, Unlimited, or Developer Editions, you’ll 
add publisher actions to your users’ side panel publisher menus with the side panel publisher layout editor
. You can also create multiple side panel publisher layouts to give different users access to different side panel publisher menus. 
Step 3:
  Make sure that a Record Type is selected other than Master.
 
                     1. From the Salesforce application, click Setup | Create | Global Actions.
                     2. Click Edit in front of the Action. Example New Case.
                     3. If the Case object has Record Types, a Record Type field will show up: select a record type other than Master.
                     4. Click "Save"
                     5. In the layout editor, drag and drop fields that you want to show or hide on the action
                     6. Save the layout.
Step 4:  
Even after following the above steps if you are still not able to see the Publisher Action in Salesforce for Outlook Side Panel, please create a new action and include the same in the global publisher layout or side panel publisher layout.
                    1. From Setup, click Create | Global Actions.
                    2. Click New Action.
                    3. Select Action Type Create a Record.
                    4. Only Create a Record actions appear in the side panel publisher menu.
                    5. Select a Target Object for the type of record you want available to users with this action.
(If the Target Object have more than one Record Type, make sure that the users you’ll assign to this action also have access to that  record type and select a record type other than Master.)
                    6. Enter a Label for the new action that tells your users what it does.
                    7. Click Save.
                    8. From the Action Layout Editor, drag the fields you want to appear on the side panel publisher record detail page.
If any of these fields are marked as required for this type of record elsewhere in your organization, make sure to include the fields in this  action, too. This is because Salesforce won’t let your users save records from the side panel publisher if the required fields aren’t included in the record detail. The action layout editor displays red asterisks next to required fields.
                       
                        You can also refer to the article: 
Create Publisher Actions for the Salesforce Side Panel Publisher
Now, after successfully getting the Publisher Action to Appear properly sometimes Side Panel constantly refreshes and doesn't allow global actions to work. Under those circumstances please check for the internet explorer version and also if it is configured properly.
Version: 
You need Microsoft® Internet Explorer® 9 or later.
Configuration Settings 
is available in the KB Article: 
Configuring Internet Explorer
Or 
Reset Internet Explorer:
Options -> Internet Options -> Advanced Tab -> Reset ... -> Check the box to delete personal settings and continue.  Restart the browser.
(NOTE: 
Resetting Internet Explorer isn't reversible, and all previous settings are lost after the reset. 
)
Trusted Website: 
Make sure that Salesforce is added to the trusted websites. Please refer to the article: 
Add Salesforce to trusted sites in IE
All of the above provided troubleshooting steps should be able to assist you in various scenarios you might face with Publisher Action in Side Panel.
