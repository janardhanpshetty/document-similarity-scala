### Topic: Why can’t I add emails to Salesforce? | Salesforce
Why can’t I add emails to Salesforce? | Salesforce
Why can’t I add emails to Salesforce?
If you receive the error message 
User is not authorized to send emails to this service
 when you use Salesforce for Outlook to add emails, your email address isn’t authorized in Salesforce.
To authorize email addresses in Salesforce:
From your personal settings, enter 
My Email to Salesforce
 in the 
Quick Find
 box, then select 
My Email to Salesforce
.
In My Acceptable Email Addresses, enter the email addresses you want to authorize. Salesforce allows you to add emails through Salesforce for Outlook from only the addresses listed in this field. You cannot leave this field empty.
See Also:
Edit Your Salesforce for Outlook Email Settings
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_sfo_why_not_authorized_add_emails.htm&language=en_US
Release
202.14
