### Topic: The steps outlined discusses how to assign a territory to an account using Data Loader. This applies to organizations with Enterprise Territory Management (Territory Management 2.0).
There are accounts that do not fit with the Territory Assignment rules and need to be manually assigned to its proper territory. What is the best approach to assign the territories to the account using Data Loader when using Enterprise Territory Management (Territory Management 2.0)?
Resolution
The ObjectTerritory2Association represents an association between a territory and an object record, such as an account. To assign an account to a territory:
1. Prepare a CSV file with the following column headers: 
• AssociationCause: the value should be "Territory2Manual"
• AccountId 
• Territory2Id 
2. Open Data Loader and click Insert. 
3. Login using your Salesforce credentials. 
4. On the "Select Salesforce object" box, select "Show all Salesforce objects" box then highlight the ObjectTerritory2Association.
5. Choose the CSV file that was created on step 1. 
6. Click Create or Edit a Map then select Auto-Match fields to columns 
7. Click OK. 
8. Select the directory where your success and error files will be saved then click on Finish. 
See also:
ObjectTerritory2Association
Territory Management 2.0 Objects​
