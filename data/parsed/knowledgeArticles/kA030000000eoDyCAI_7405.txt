### Topic: Cannot see the flow via UI. However, I could access the flow in workflow designer by replacing the Flow ID in the URL. But I do not see an option to deactivate/delete this flow. Trying to save the flow as Save As, results in an internal server error.
This happens when the user first creates a Process Builder Flow, which in the background is really just a special type of flow and should never be accessed or modified via Visual Workflow Designer. In short, accessing Process Builder flow via Visual workflow designer will cause such issue.
Resolution
Here are the steps to determine this issue,
- Login in WB (WorkBench) 
- Go to Info-->metadata components 
- Select flow. 
- Expand components section, search for your flow via flow Name.
- You will be able to find different versions of the flow, 
For example,
==========================
A_Update_Member (id: 301j0000000HK7JAAW) --> Active flow
A_Update_Member-5 (id: 301j0000000H1N5AAK) 
A_Update_Member-6 (id: 301j0000000M8EXAA0) 
A_Update_Member-7 (id: 301j0000000M8EcAAK) 
============================
- Now, open rest explorer in a different tab, perform "GET" on 
active version of the flow like, 
/services/data/v35.0/tooling/sobjects/flow/301j0000000HK7JAAW. Here you can see the "ProcessType: Workflow" which implies that this is a PB flow. 
- Opening PB flow via workflow designer caused the issue here and now it is no longer showing up in both Process Builder flows list and visual workflows list.
- Perform GET for other inactive flow id's to check the process type. 
Here are the steps to workaround this issue,
- Firstly, "DELETE" , inactive versions of the flow i.e, 
==============
A_Update_Member-5 (id: 301j0000000H1N5AAK) 
A_Update_Member-6 (id: 301j0000000M8EXAA0) 
A_Update_Member-7 (id: 301j0000000M8EcAAK) 
===============
- You can delete them via same tooling REST API of WB. Select "DELETE" and execute on, 
/services/data/v35.0/tooling/sobjects/flow/<Flow-Id> replace your flow id like, 
/services/data/v35.0/tooling/sobjects/flow/301j0000000H1N5AAK 
- Once you delete the inactive versions, you will be able to see the active version of the flow in Process builder list via UI. You can deactivate or delete this Process Builder flow via UI. 
