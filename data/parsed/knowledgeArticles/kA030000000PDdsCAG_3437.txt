### Topic: Steps to create mailing labels in Microsoft Word.
I would like to know how to create Mailing Labels in Microsoft Word using my contacts in Salesforce.
Resolution
To create mailing labels in Microsoft Word:
1. Run a Contact "Mailing List" report (Which is a standard report type that is in the Accounts and Contacts report folder), Export to Excel and save the file to your local hard drive. Now Delete the columns you don't plan on using for your Label and Delete the information at the bottom of your report that came from Salesforce otherwise it will interpret it as another contact later.
2. Open Microsoft Word.
3. Go to "Mailings" in Microsoft Word,select Start Mail Merge, and select "Step by Step Mail Merge Wizard".
4. Select Labels as your document type and click Next:
     
5. Set up your Label options so that you have the correct Printer Tray selected, Label Vendor, and Product Number is correct click OK when you are satisfied with your label selections. Then click "Next: Select recipients"
6. On the "Select recipients" page select the "Use an existing list" radial button.  Click on Browse and select the Excel that you created in Step 1 and hit Open.  Now click on "Next: Arrange your labels"
7. Click on "More items..." and first Match your fields. Click OK and now insert the Fields in the order you wish them to appear in your Label.  Hit "Close" when you are done.  Now go into the Label and add spaces, commas, and line breaks.  Once you are satisfied hit "Update all labels" and "Next: Preview your labels"
8. You can "Find recipients..." if you need to add additional people outside of Salesforce.  Also you can click on "Edit recipient list.." if you have duplicates or if you would like to omit some contacts from the labels. Click "Next: Complete the Merge"
(Please Note: If you have duplicates in your labels this means there are duplicates in Salesforce and this will need to be resolved within Salesforce so these duplicates are not encountered again.)
9. You now have the opportunity to Print, Save, or "Edit Individual labels..." 
This should complete your Labels in Microsoft using Contacts from Salesforce.
**Please note that this solution is not valid with windows XP. Please search our online knowledge base for 'Mailing Labels on XP' to obtain directions on how to create labels.
Also check the information for extended mail merge available in Help & Training:
 
https://na1.salesforce.com/help/doc/user_ed.jsp?section=help&loc=help&target=mailmerge_mass.htm%23topic-title.
 Extended Mail Merge is valid for Windows XP.
 
