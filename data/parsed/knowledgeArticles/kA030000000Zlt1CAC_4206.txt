### Topic: This feature guide will help you get started with Work.com Performance Summaries
Work.com: Getting Started with Work.com Performance Summaries
 
Overview
With Work.com, performance summaries are fast and easy. Administrators create, deploy, and manage performance cycles from the Performance tab. End users access all feedback and performance summary features from the Feedback tab.
You can see recent work (such as thanks, coaching tasks, and feedback) in individual performance summaries to make completion of reviews quick and thorough. To modify the recent work that appears, click 
Customize Related Lists
 on a feedback request's detail page. You can't quote recent work directly in your responses, but you can manually add this information.
 
Managers can easily evaluate the progress of employees whenever they choose — at the end of a quarter, project, or goal. 
Getting Started
 
Perform administrator
 
The first step is to determine who in your organization will be responsible for deploying performance summaries. This role could be assigned to an existing Salesforce administrator or it might be assigned to someone else.  Note that any user granted these permissions will have read/write access to all performance summary data and caution should be exercised when delegating these permissions. Refer to the 
Recommended Work.com Permission Sets
 or 
Recommended Work.com Profiles
 documentation for details on which permissions a perform administrator needs.
 
In order to add people to a cycle, deploy a cycle, and reassign a performance summary in a cycle, the administrator must have View All and Modify All access to the feedback objects (Feedback, Feedback Questions, Feedback Question Sets, Feedback Requests) and the Performance Cycles object.
 
Note:  
This set of permissions gives the Perform administrator “Modify All” access to the feedback object. This means that users with this permission have access to view and change any answer to any performance summary question, including their own. 
Changes to a performance summary 
cycle can
 be done through the Work.com U
I in Salesforce
 or through the SOAP API. Refer to the 
SOAP API Developer's Guide
 for more information about managing performance summary cycles with the SOAP API.
 
Key Features for Perform Administrators
 
Create a Performance Summary Cycle
 
Users with the perform administrator permission set/profile can access the 
Performance Cycles
 tab, where they can 
create
, 
deploy
, 
finish
, and 
manage
 any performance summary cycle. End users may also be able to see performance cycle questions depending on your org's permissions.
 
From the Performance Cycles tab, click
 New
.
The 
Activity From
 and 
Activity To
 fields define the period of time that is being evaluated. 
Only feedback and activity from this period, such as thanks badges and goals, will be available to view.
​
​
Create Question Sets and Questions
After you create a performance cycle, create new feedback question sets. Question sets group questions according to their intended audience. For example, a performance summary about Bill Jones could have up to four different audiences:
 
Manager Summary—Questions for Bill’s manager. By default, every cycle must include a
manager summary.
Self Summary—Questions for Bill.
Peer Summary—Questions for Bill’s peers.
Skip Level Summary—Questions for Bill’s direct reports.
After you create question sets, create different questions for each question set. You can specify rules for each question:
 
Optional—The person writing the summary isn’t required to answer this question.
Confidential—The subject of the summary won’t see the submitted answer. This rule isn’t available for self summaries.
You can also add instructions at the beginning of each question.
Note: You can no longer add #first_name in feedback question text to display summary subject's first names in performance summaries. Please use a generic term, such as user, to refer to summary subjects instead.
 
Add People to a Performance Cycle
Once you're done adding questions, add people to the performance summary cycle manually or through a CSV file.
Note: Users must have the proper object permissions and the “View Setup and Configuration” user permission enabled to add people.
Add people from a performance cycle detail page's Feedback Requests section. Users can be added to a cycle if they exist in the system and have a manager assigned. Managers can be assigned by populating the “Manager” field on the user record.
:
 
To manually enter subjects, click 
Add Subject
. Enter the user’s name and click 
Save
.
To upload subjects through a CSV file, click 
Upload Subject CSV
, then select 
Choose File
. Select a CSV file, click Upload, and save your changes. The CSV file should contain a list of either user IDs (example: 005R0000000DjZW) or usernames (example: john@acme.com) in the first column. The data should begin on the second row, because the first row is seen as the header row and is not uploaded.
 
The fastest way to get a list of usernames is to create a report. 
 
The list of People will be verified before it can be successfully added to the cycle. 
Verification will check to ensure that each person has a manager identified, each user is active, and that each user is not already added to the cycle.
The Job Tracker at the bottom of the performance cycle detail page displays the status of the CSV upload. Additionally, you can confirm the users included in the performance summary cycle in the Feedback Requests section.
People can be added before or after a deployment:
People added 
before a deployment 
will get an email when the summary is deployed.
People added 
after a deployment
 will automatically be assigned the associated summaries and will receive an assignment email notification.
Delete People
If you added someone you don't need to evaluate, delete them from the performance summary cycle. You can only delete people from performance cycles with a state value of Setup. For more information on deleting people, visit 
Delete People from a Performance Summary Cycle
.
To delete people, click 
Del
 in the Feedback Requests section of a performance cycle detail page.
Cloning a Cycle
You can create performance cycles by clicking 
Clone
 on another performance cycle's detail page, but cloning a cycle copies only the cycle name, activity start date, and activity end date. Question sets, due dates, and other details are not copied.
 
Manager Group Visibility
 
Performance summaries are not automatically shared up the management hierarchy in Work.com on Salesforce. Sharing is determined by manager groups
. Before deploying any performance summary cycle, you must enable the “Manager Groups” permission to share up the management hierarchy tree.
To enable Manager Groups:
1. From Setup, click 
Security Controls > Sharing Settings
.
2. Click 
Edit
.
3. Under Other Settings, select 
Manager Groups
.
4. Click 
Save
.
To learn more about Manager Groups, please refer to this 
documentation
.
 
Deploy a Performance Summary Cycle
Deploy a performance cycle once you've finished adding questions and want to make the summaries available for people to complete. You can't edit questions or cycle details after a performance summary cycle is deployed, but you can add more people.
​
To deploy a performance cycle, click 
Deploy
 on a performance cycle's detail page.
Inactive users are automatically removed when a cycle is deployed. When deployment is complete, we'll send you a detailed email that describes the cycle deployment and any users that were removed. Performance cycle managers can add them back again later, if necessary.
Always review the confirmation pop-up before deploying the cycle. You can track a cycle's deployment status from the Job Tracker on the performance cycle detail page.
 
Once deployed, a manager summary is assigned to the manager of each person added to the cycle. If the cycle contains self summaries, the self summary is assigned to the associated person. Each person assigned a summary receives an email notification and can start working on their summaries.
 
Reassign Performance Summaries
Administrators can reassign performance summaries if, for example, a user's manager changes by clicking 
Reassign
 on a feedback request's detail page.
 
Share Performance Summary Feedback
Administrators can select 
Share All
 on the performance cycle detail page to share submitted performance summaries with the summary subjects.
Only cycles with the status of In Progress can be shared.
Shared responses don’t include questions marked Confidential.
 
Key Features for Perform Standard Users
Answering a performance summary
 
When a performance summary is started, the New Feedback Requests filter on the Feedback tab will show all the summaries that have been assigned and require attention.  
If there is a peer summary as part of the performance summary, simply 
add peers
 using the 
Invite Peer
 button on the feedback request's detail page. 
Any peer, self or skip level summary assigned can be 
declined.
Save or submit 
a summary by clicking the buttons on the feedback request's detail page.
 
 
Recent Work
Recent Work items (such as thanks, goals, metrics, and feedback) are controlled by the start and end dates of the cycle. Users can toggle what appears by clicking on the 
Customize Related Lists
 button. You can't quote recent work directly in your responses, but you can manually add this information.
 
If given visibility, users have the ability to print feedback questions without the confidential questions.
 
Reports and Dashboards
Salesforce Reports and Dashboards are available for all Work.com users.  We offer unmanaged packages of standard report types, including reports and dashboards for Performance summaries. Please refer to the 
Admin Guide
 for further information and the link to download the package. 
 
 
Resolution
