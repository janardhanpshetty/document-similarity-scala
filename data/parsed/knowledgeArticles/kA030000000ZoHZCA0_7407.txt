### Topic: This is the process for having Salesforce Support increase the Content Delivery Bandwidth Limit (MB).
Available in: 
Group, Professional, Enterprise, Performance, Unlimited
 and 
Developer 
Editions
Organizations sometimes find that the default 
Content Delivery Bandwidth Limit (MB) 
of 1024 MB is insufficient for their needs. In these scenarios Salesforce Support can assist in increasing this limit. The maximum limit is on a per Case basis. 
Prior to requesting this limit increase it's best to familiarize yourself with the following documentation:
How does the Content Delivery Bandwidth Limit (MB) work?
 
Resolution
If you would like to move forward with increasing this limit, please take the following steps: 
Have a System Administrator log a Case with Salesforce Support 
Please include all essential details. This includes a business reason for the request as well as the actual limit being requested.
Support will review the Case and action it as needed. 
