### Topic: Guidelines for Finding Similar Opportunities | Salesforce
Guidelines for Finding Similar Opportunities | Salesforce
Guidelines for Finding Similar Opportunities
Close your deals faster by taking advantage of the rich information contained in successfully closed deals that are similar to yours. Here are some guidelines to help you find the opportunities that will help you close your deals.
Available in: Salesforce Classic
Available in: 
All
 Editions for organizations activated before Summer ’09
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions for organizations activated after Summer ’09
Use the Similar Opportunities related list on the opportunity’s detail page to find Closed/Won opportunities with common information.
The criteria used to find similar opportunities is determined by your administrator. The search finds a maximum of 10,000 opportunities with close dates in a three-month period and displays up to 300 of the records that best match the search criteria. The results are ranked by the number of matching fields.
Results can be filtered by Close date or by matching fields.
To see how a record in the search results is similar to your current deal, hover over the opportunity name. The matching fields are highlighted in the Match Criteria sidebar.
See Also:
Enable and Configure Similar Opportunities
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=opp_similaropp.htm&language=en_US
Release
202.14
