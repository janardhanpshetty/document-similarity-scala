### Topic: an user is displaying zero records while opening a report, but the data should be displayed and s/he has access to the data.
If a user is getting zero records on a Dashboard when they are the running user and also/if when running a report, and the report 
has a filter of user's name and surname 
and while they have full access to the records and another user with the same Profile and Role is able to see the data then this is due to the affected user's Locale.
Resolution
The affected user has set a locale that has the name format as "
LName FName"
 and this means that all the names in the filter criteria are being reversed.
E.g. : "Owner Name equals X(First Name) Y(Last Name)" (When run by the 
LName FName
 Locale user) the filter becomes reversed, showing as Y(Last Name) X(First Name) and of course the names for other locale users are not displayed in this way and therefore will not show in the report
In order to verify if a locale has the name format as "
LName FName", please check the article 
Supported Locales
, some examples are:
-  Chinese
- Hungarian
- Japanese
- Korean
- Korean
- Vietnamese
There are 5 Workarounds:
1) Add a second filter with the names in reverse and then add "OR" as filter logic
2) The "
LName FName" locale 
user re-creates the report
3) The "
LName FName" locale
 user changes their locale settings to another locale other than the "
LName FName"
 setting
4) Manually change each Salesforce username to appear in reverse
5) U
se the owner alias name for the filters when the report is being used globally by users in different locales.
As a reference below are instructions on how to change the locale, 
Understanding Language, Locale, and Currency
Go to Your Name | Setup | My Personal Information | Personal Information | Edit and select a locale from the supported locale drop-down list
 
