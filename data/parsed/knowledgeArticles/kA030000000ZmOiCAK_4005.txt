### Topic: Instructions on how to change your subscription settings to Salesforce.org emails
How can subscription be changed for Salesforce.org email?
Resolution
You can change your subscription settings from Salesforce.org emails. Here are the steps to changing your subscription preferences:
Go to: 
http://info.salesforce.org/preferences​
Enter your Email Address under the 
Email subscription
 section.
To unsubscribe from specific types of emails from Salesforce.org you will need to uncheck the checkbox for each option. To opt-out of all emails, keep the four options unchecked.
Click on Update Subscriptions 
Note:You can also unsubscribe from Salesforce.org emails by clicking on the Unsubscribe link at the bottom of an email from Salesforce.org. 
 
Other than updating the email settings, you can also update your Profile Information on the same page by entering your Email address and choosing the options under the 
Profile Information
 section.
