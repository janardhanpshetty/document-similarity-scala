### Topic: All data stored in the system is stored as UTF-8 encoded unicode. This includes double byte languages (like Japanese, Kanji characters).
Suggested Description: Data available through external source is stored and handled by UTF-8 encoded unicode which is the appropriate way to handle the language system.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
September 8, 2015 at 8:29 AM
  
Harihara Suthan
Added the description
Show More
Like
Unlike
 
  ·  
 
September 8, 2015 at 10:26 AM 
Attach File
 
Click to comment
 
 
Abhik Biswas
 to salesforce.com Only
#KBFeedback
 The same resolution works for web to lead as well hence it should be added for both web-to-lead and web-to-case under the resolution this should be added.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
September 8, 2015 at 8:24 AM
  
Harihara Suthan
Please see the feedback before this one.
@Ranjita Sobhani
 - Please advise reps to read other feedback too. If one already exist there is no need for same feedback
Show More
Like
Unlike
 
  ·  
 
September 8, 2015 at 10:26 AM 
Attach File
 
Click to comment
 
 
Eric Van Horssen
 to salesforce.com Only
#KBFeedback
 Can we have the article chanegd a bit so that it mentions both W2C and W2L? Thank you!
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
September 6, 2015 at 12:20 AM
  
Harihara Suthan
Done 
@Eric Van Horssen
 - Added a note
Show More
Like
Unlike
 
  ·  
 
September 8, 2015 at 10:26 AM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Configuration
What are the character sets supported with web-to-case?
Resolution
All data stored in the system is stored as UTF-8 encoded unicode. This includes double byte languages (like Japanese, Kanji characters).
When you create the standard template in Web-To-Case: 
Setup - App Setup - Customize - Self Service - Web-To-Case - Generate the HTML - 
One of the first lines you'll find there is the following:
"<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">"
This value can be manipulated to be adapted to any character set you may require but UTF-8 should be enough for any needs as it was created to be a superset of encodings for all languages.
As a reference you can review the Article:
http://en.wikipedia.org/wiki/UTF-8
There it explains that UTF-8 will store Cyrillic characters using double size and Thai and Hindi's Devanagari characters using triple due to the nature of the characters but they will be handled appropriately.
Note: The same applies to Web-to-lead as well
