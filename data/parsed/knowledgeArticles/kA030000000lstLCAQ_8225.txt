### Topic: Usage support can utilize Splunk and Black Tab to monitor the progress of a sandbox refresh.
Customers will at times submit tickets requesting that we monitor the progress of their sandbox refresh. The Usage team can utilize Splunk and Black Tab to monitor the progress.
Resolution
Black Tab
From Black Tab, identify the Pending Org ID for the new sandbox.
1. Navigate to the Sandbox page in Black Tab.
2. Click on the refreshed Sandbox.
3. Make note of the Pending Sandbox Org ID.
4. In the Suspend Information section, make note of the destination Sandbox instance. This will be the CS# in the Queue Name. 
NA7.SandboxQueueDef_Large.
CS9
5. Check the Suspend Information to determine the current status of the refresh.
Sandbox Copy Progress
Once you have identified which sandbox instance a refresh is copying to you can navigate to its BT and view the current progress of the copy.
1. Select the BT link: Sandbox Copy Progress
2. In the drop down there will be several entries for each production instance, select the one that does not contain underscores and corresponds to the sandbox's production instance:
3. On the corresponding page locate the sandbox in the available list by searching the page 
(Ctrl + F)
 for the Organization or Sandbox Name.
4. Once you have located the sandbox click the available View link in the action column to see overall progress.
Note: 
Do not click any links!
 Copy progress can be helpful in gauging progress of a particular table or the overall status of a copy but should be considered a loose reference. The numbers provided may not be exact and tables may be restarted.
Splunk
In Splunk, run the following query. Monitor for any errors and to confirm that the progress continues to move forward:
 
index=[PENDING SANDBOX INSTANCE]  [PENDING ORG ID] (`logRecordType(sx*)` OR scrutiny OR `logRecordType(bk*)` OR `logRecordType(g*)`) earliest=-2d (TI)
index=[PENDING SANDBOX INSTANCE]  [PENDING ORG ID] (`logRecordType(ss*)` OR scrutiny OR `logRecordType(bk*)` OR `logRecordType(g*)`) earliest=-2d
 (sandstorm)
Radio
As you're monitoring the sandbox refresh in Splunk, it may look as though it is hung on the ScrutinyCustomForeignKeyValue. This scrutiny takes time to process. You can check Radio to confirm that it is still moving forward. 
1. Navigate to https://radio-sjl.dmz.salesforce.com/ActiveRequests.html?instance=[instance]
2. Search for the Pending Sandbox Org ID (upper right)
3. Make note of the numbers in the Runtime column.
4. Refresh the page.
5. Check the numbers in the Runtime column to ensure they are increasing.
6. Click into the SID link for the active request and ensure that the 
process/p1 value is changing
. 
 
