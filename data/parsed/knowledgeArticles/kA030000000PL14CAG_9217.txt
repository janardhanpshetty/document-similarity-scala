### Topic: This article explains the cause of "URL No Longer Exists" error when an user comments on a case and how to fix it
A User creates a new Case Comment and clicks Save. The Comment saves, however, the User is brought to a page that says "URL No Longer Exists". 
 
Resolution
RESOLUTION: 
This issue occurs when the User has Qwiklinx as an add-on in Internet Explorer. 
Qwiklinx changes our hidden input value: 
original: <input name="retURL" id="retURL" type="hidden" value="/CASEENTITYID"> 
After modification: <input name="retURL" id="retURL" type="hidden" value="%2FCASSEENTITYID">(%2F equals / in URL world) 
This is a kind of hack for our webapp, since Hidden field type is not secure. Qwiklinx changes our hidden fields in this circumstance and is also harmful for the customer's company. Recommend user disable or remove it in his/her browser or they could develop a VBScript to disable it when they open a salesforce webapp in a new tab. 
