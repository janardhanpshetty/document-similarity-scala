### Topic: Determining what packages are managed and what/how many of their components may be counting toward org limits can be done by gathering information about each installed package using Blacktab.
Determining which packages are managed and determining which of their components and how many may be counting toward org limits can be done by first identifying all the managed packages in an org, then by viewing details about each of those packages using Blacktab.
Resolution
Identify Managed Packages
First, find the managed packages listed in Blacktab.
Navigate to the Org in Blacktab, login access is not required.
Click on Installed Packages under Setup.
Packages will be listed, managed packages are indicated by an icon to the left of the package name.
Determine if the Package Components Count Towards Org Limits
See 
INTERNAL: How to identify which package or app's and their components (objects, apps, record types, etc.) are counting against org limits
.
Note:
 All managed packages published on the AppExchange should automatically have the field "Don't count towards Apps, Tabs, and Objects Org Limits" checked. If somehow it is not, the app's vendor or partner will need to log a case with Support and direct it to the "Alliances Technical Tier 1" queue to enable it.
Determine how many components of each type are counted towards the org limits
If the 
"Don't count towards Apps, Tabs, and Objects Org Limits" 
checkbox was not checked upon following the steps in the article linked above you can then find out how many components in each package are counted towards the org limits.
For any managed package that was identified as not having the checkbox selected, Click on the Package Name (from the org's Blacktab navigate to Setup and click Installed Packages) which will bring you to the package's detail page (0A3 record).
Scroll down on the page to Find the Package Member Types related list, note the listed components and the count.
 
