### Topic: Learn how to improve security with session domain locking.
This feature associates a current UI session for a User, such as a Community User, with a specific domain to help prevent unauthorized use of the session ID in another domain.
Resolution
Enabling Lock Sessions to the Domain in which they were first used
Classic Salesforce UI: 
Go to
 
Setup
 | 
Security Controls
 | 
Session Settings
 | Lock sessions to the domain in which they were first used.
New Lighting UI: 
Click on the top-right 
Gear
 | 
Setup Home
 | 
Security
 | 
Session Settings
 | Lock sessions to the domain in which they were first used.
 
Example: 
Domains 
test1.my.salesforce.com 
and 
test2.my.salesforce.com.
A User can log in to "test1.my.salesforce.com," take the SID (session ID), and then call another domain "test2.my.salesforce.com." The User could fetch data from 
"test1.my.salesforce.com" by calling "
test2.my.salesforce.com."
"Lock sessions to the domain in which they were first used"
 
prevents attackers from using the UI session (SID) for 
"
test1.my.salesforce.com
" 
domain to call the 
"
test2.my.salesforce.com." 
domain.
Want to learn more? 
Check out our documentation on 
Modifying Session Security Settings
.
