### Topic: This is meant to address how converted lead status values are set, and how the values displayed on lead convert are derived?
Before a Lead is converted into an Account and a Contact, users must select a "converted status" value for the record to be converted..
Resolution
How to set converted leads status values?
To set the values available for the "converted status" field on the convert page layout do the following:
1. Click on:
Setup >> Customize >> Leads >> Fields >> Lead Status
2. Click "Edit" next to a value
3. Select the "converted" checkbox if you would like it to be an available value during lead conversion (NOTE: you can have more than one converted status value)
 
How to remove converted status off lead status value?
To make a value no longer have a converted status you should click edit beside the value and uncheck the converted checkbox
 
How are converted status values derived when a lead is converted?
If record types are
 NOT
 being used for Leads, all of the designated converted status values will show up on the lead convert page.  However, if record types 
ARE
 being used, the values displayed on the lead convert page are taken from the values available on the default record type of the profile assigned to the user doing the lead conversion, not from the converted status values that are setup on a lead record type / lead process that is assigned to the lead being converted.
