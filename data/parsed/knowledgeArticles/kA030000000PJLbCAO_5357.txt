### Topic: New UPDATE Clauses to Report on Article Searches and Views There are new UPDATE clauses in SOQL and SOSL to report on Salesforce Knowledge article searches and views
Access to the pilot UPDATE VIEWSTAT clause of SOQL from the REST API. And, actually, the UPDATE VOTESTAT clause might very well be useful as well.
Resolution
New UPDATE Clauses to Report on Article Searches and Views There are new UPDATE clauses in SOQL and SOSL to report on Salesforce Knowledge article searches and views. 
In SOQL, the UPDATE VIEWSTAT clause allows developers to use the API to update an article’s view statistics. You can use this syntax to increase the view count for every article you have access to online:
 
SELECT Title FROM FAQ__kav
WHERE PublishStatus='online' and
Language = 'en_US' and
KnowledgeArticleVersion = 'ka230000000PCiy'
UPDATE VIEWSTAT
 
In SOSL, both UPDATE TRACKING and UPDATE VIEWSTAT were added.
• The UPDATE TRACKING clause allows developers to use the API to track the keywords used in the Salesforce Knowledge article search.
You can use this syntax to track a keyword used in Salesforce Knowledge article search:
FIND {Keyword}
RETURNING KnowledgeArticleVersion (Title WHERE PublishStatus="Online" and language="en_US") UPDATE TRACKING
 
• The UPDATE VIEWSTAT clause allows developers to use the API to update an article’s view statistics.
You can use this syntax to increase the view count for every article you have access to online in US English:
FIND {Title}
RETURNING FAQ__kav (Title WHERE PublishStatus="Online" and language="en_US" and KnowledgeArticleVersion = 'ka230000000PCiy') UPDATE VIEWSTAT
Note : It will available in all org after API V28.0 release
