### Topic: How do I update the text that a potential subscriber sees when receiving a Forward to a Friend email?
How do I update the text that a potential subscriber sees when receiving a Forward to a Friend email?
Resolution
The default tag states:
"If you would like to receive this email please click here."
If a client wants this link removed or for it to be directed to their Webpage, you can adjust it following these steps:
1. Log into Manage
2. 
Navigate to Data Maintenanc
e | 
Brand Tag Management
3. Make a tag set
4. Include these tags
ftaf_subscribe_html
ftaf_subscribe_text
4. Update the tag with the customer's specifications.
Currently, the translated versions of the forwarded Forward to a Friend email only occur when the account has the UNICODE_SUPPORT business rule enabled. It is reasonable to consider a change to allow the FTAF_SUBSCRIBE_HTML brand tag override the ftaf_subscribe_htem key in the resource file if the UNICODE_SUPPORT business rule is off.
This would create the following options: 
UNICODE_SUPPORT = off; can override just the URL via brand tag FTAF_ACCOUNTINFO_URL or can override the entire sentence including url and text via brand tag FTAF_SUBSCRIBE_HTML 
UNICODE_SUPPORT = on; can override just the URL via brand tag FTAF_ACCOUNT_URL. CANNOT override the sentence text.
