### Topic: This article would help you with the steps to follow when you get an error "An error occurred while extracting" while installing SFO (Salesforce for Outlook)
User is unable to install Salesforce for Outlook, installation wizard disappears or user gets the error message:
 
Resolution
This is not a Salesforce issue and is more than likely related to a corrupted Windows profile. You would need to refer to your internal IT team to recreate your Windows profile.
If you do not have an IT department, you probably have a local Windows user profile which means your computer is in a work group and not part of a domain.
For information on how to create a new username in Windows 7 and up please refer to the 
Microsoft link 
and select your Windows version on top right side using the drop down.
After you create the new profile you can log off and log in with the new username you have just created and try to install the Salesforce for Outlook.
 
 
