### Topic: Learn the best practices for establishing a specific Role Hierarchy in relation to Activity and Opportunity reports.
The Role Hierarchy controls data access in standard reports and some Wave apps. Some roles may not be able to pull comprehensive 
Activity Reports
 or 
Opportunity Reports
 and some Wave apps may not function properly without a specific Role Hierarchy configuration.
Here are the 
Role Hierarchy best practices for reporting
 to avoid potential data issues:
Resolution
 
Reporting for your entire organization
Create a role that exists alone at the top level of your organization and use it to run your reports if you need organization wide reports (generally named "CEO").
If your organization has multiple roles at the same level at the top of the Role Hierarchy, your organization will be sectioned into "departments" or "branches." Without a single, top-level role, Users in different "branches" of the Role Hierarchy may not be able to report on Activities across the entire company in reports, even if they have the "View all data" and "Modify all data" permissions enabled on their profiles.
For more help, see our 
"User Role Hierarchy" help documentation
.
 
Be careful with Portal Roles
Avoid Role Hierarchies with multiple 
Portal Roles
 above your report's selected Role. Since you must select a role to run an Activity or Opportunity report, your report will include all activities for Portal Role Users since they live under the Contact Owner role in the Role Hierarchy.
 
Save and Run to apply changes
If you're still having trouble with your data, make sure to save and run your report (your changes won't be applied if you just "run" the report without saving it).
