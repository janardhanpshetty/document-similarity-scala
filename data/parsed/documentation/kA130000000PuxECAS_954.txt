### Topic: Salesforce Mobile Product Comparison | Salesforce
Salesforce Mobile Product Comparison | Salesforce
Salesforce Mobile Product Comparison
Salesforce1, SalesforceA, Salesforce Classic Mobile, and Salesforce Authenticator are available on different mobile devices and in different editions, with different support options.
Product
Description
Supported Salesforce Editions
Supported Mobile Devices
Offline Support?
Salesforce1
Access and update Salesforce data from an interface that’s optimized for navigating and working on your touchscreen mobile device.
You can view, edit, and create records, manage your activities, view your dashboards and reports, and use Chatter. Salesforce1 supports many standard objects and list views, all your organization’s custom objects, the integration of other mobile apps, and many of your organization’s Salesforce customizations (including Visualforce tabs and pages and Lightning Pages).
Free for customers using:
Personal Edition
Group Edition
Professional Edition
Enterprise Edition
Unlimited Edition
Performance Edition
Developer Edition
Contact Manager Edition
Android phones
Android tablets
iPad models
iPhone models
Windows 8.1 and Windows 10 phones (mobile browser app only)
Yes, for viewing data.
Beta: Create and edit data (Salesforce1 for Android, v9.0 or later and Salesforce1 for iOS, v10.0 or later)
SalesforceA
Manage users and view information for Salesforce organizations from your smartphone.
Deactivate or freeze users, reset passwords, unlock users, edit user details, and assign permission sets.
This app is restricted to users with the “Manage Users” permission.
Free for customers using:
Group Edition
Professional Edition
Enterprise Edition
Unlimited Edition
Performance Edition
Developer Edition
Contact Manager Edition
Android phones
Android tablets
iPad models
iPhone models
iPod Touch
No
Salesforce Classic Mobile
You can view your dashboards, run simple reports, log calls and emails, track your activities, and create, edit, and delete records. Most standard Sales objects and some Service objects are available. Custom objects and configurations are also supported.
App requires mobile licenses and is available for:
Professional Edition
Enterprise Edition
Unlimited Edition
Performance Edition
Developer Edition
Android phones
Android tablets
iPhone models
Yes
Salesforce Authenticator
Secure your account by using your mobile device for two-factor authentication. Verify your identity or block unrecognized activity with a one-tap response to a push notification. If you enable location services for the app, you can set trusted locations, such as your home or office. If automation is available, the app can verify your identity automatically when you’re in a trusted location. Get a code to use as a backup identity verification method.
Free for all customers using:
Group Edition
Professional Edition
Enterprise Edition
Performance Edition
Unlimited Edition
Developer Edition
Contact Manager Edition
Android phones
iPhone 5 or later models
Yes, for generating verification codes
See Also:
Requirements for the Salesforce1 Mobile App
SalesforceA
Salesforce Classic Mobile
Salesforce Editions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=mobile_product_overview.htm&language=en_US
Release
202.14
