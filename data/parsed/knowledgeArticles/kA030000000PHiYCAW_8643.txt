### Topic: When viewing sentiment for a Topic Profile in a Topic Analysis widget in the Radian6 Analysis Dashboard, you may want to hide or show neutral sentiment. These steps outline how to hide or show the neutral sentiment segment.
Segmenting by sentiment is available in the Topic Analysis widget  for Topic Profiles.  When viewing sentiment in a Topic Analysis widget  in the Radian6 Analysis Dashboard, you may notice a percentage is scored as 'Neutral' meaning they are neither positive nor negative. The steps below can be followed to hide those posts. Note that when segmenting by sentiment, the Topic Analysis Widget will exclude Neutral posts by default.
Resolution
1. Open a Topic Analysis widget by clicking on the Topic Analysis widget icon on the left tray of the Radian6 Analysis Dashboard.
2. Select a Topic Profile and configure the Topic Analysis widget as desired. 
3. Click 
Done
.
4. Select either 
Bar
 or 
Pie
 for the desired graph type.
5. Segment the graph by either  
Sentiment > Widget Keywords
 or 
Sentiment > Topic Profile Sentiment Subjects
. These options are found at the top left-hand corner of the widget.
 
Note: You will notice that a percentage of your results may be scored as 
Neutral
. This is normal and should be expected.
6. To hide the neutral posts, click on the ascending bars in the lower right corner of the Topic Analysis widget.
7. Select 
Hide Neutral
.
 
The neutral sentiment is now hidden. To show neutral sentiment, follow the steps above. Select 
Show Neutral
 to display the neutral sentiment.
