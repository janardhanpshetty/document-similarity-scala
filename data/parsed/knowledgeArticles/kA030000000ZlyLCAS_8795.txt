### Topic: This will show steps on how to update the Last Name in Outlook to resolve the error “Sfdc.Server.SfdcServerCreateUpdateException: Required fields are missing: [LastName] for Outlook record”
ISSUE
I am trying to sync using Salesforce for Outlook however I am receiving an error message that says 
“ERROR [Contact] Create to SFDC gave error(s) Sfdc.Server.SfdcServerCreateUpdateException: Required fields are missing: [LastName] for Outlook record”. 
This happens when Contacts are missing the 
Last Name
 value
The SFO sync icon in the System tray at the bottom right corner of the screen will also change to  
Any type of sync issue will change the SFO icon to this icon and if you double click on it you should see the details about the error
 
 
Resolution
First, double click on the SFO system tray icon (as explained above) to open the actual error message with the details. If the error message says 
"Required fields are missing: [LastName]
" then follow the steps below to resolve the issue.
1- You can click on the contact name on the SFO error screen (it is in a form of hyperlink that you can click on to open the contact in Outlook) to open the contact's detail page
2- Click on Full name
3- The “
Check Full Name"
 windows will open
4- Update the
 Last name
 as it is a mandatory field in Salesforce. If your contact does not have a Last Name or you are syncing a company as a contact, or for any reasons you do not have a Last name you can use a dot "." or comma "," or such to populate the Last Name field
5- Click 
OK 
and then click on 
Save & Close 
You may have to repeat steps 1-5 if you have more than contact with missing Last Name
The screen shot below is from Outlook 2007 but the steps are the same for other versions of Outlook
 
6- Click on Retry on the SFO error screen or if you have already closed it, right click on SFO system tray icon and click on Sync | Sync Now
7- Sync should complete successfully and you should not see that error message again
 
