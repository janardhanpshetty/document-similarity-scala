### Topic: Refer to this article if you need to get an Entity–relationship model diagram (ERD) of all the Work.com objects
Simply load the objects in Salesforce's 
Schema Builder
 to see the relationships.  The Work.com objects are:
Coaching
Goal
Goal Collaborator
Goal Links
Feedback
Feedback Question
Feedback Question Set
Feedback Request
Performance Cycle
Reward
Reward Fund
Reward Fund Type
Badge
Thanks
Skill
Skill User
To get more details on each of these objects take a look at our 
Standard Object Reference
.  The objects will shown with their API name, so if you want more details on Goals look for the WorkGoal object.
 
Resolution
