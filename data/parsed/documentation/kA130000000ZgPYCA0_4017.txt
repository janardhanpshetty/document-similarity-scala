### Topic: Select a Matching Data.com Record for a Salesforce Account | Salesforce
Select a Matching Data.com Record for a Salesforce Account | Salesforce
Select a Matching 
Data.com
 Record for a 
Salesforce
 Account
If your organization uses Dun & Bradstreet’s DUNSRight™ matching service to match and clean your account records, we automatically match records according to a preset Dun & Bradstreet confidence code, which your administrator selects. If we don’t have a record qualified for an 
automatic
 match, but we have identified one or more 
potential
 matches, we’ll set your record’s clean status to 
Select Match
. This clean status lets you know you need to 
manually
 review match candidates and select one. After you select a match, you manually clean your 
Salesforce
 record in the usual way.
Available in: 
Salesforce Classic
Available with a 
Data.com Prospector
 license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available with a 
Data.com Clean
 license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To clean account records:
“Edit” on accounts
To clean contact records:
“Edit” on contacts
To clean lead records:
“Edit” on leads
Select a Data.com Match for your Salesforce Account Record
If you need to select an initial match, from the record, click the 
Select Match
 clean status value, then click 
Select Account from Data.com
. 
On the select match page, you’ll see up to five match candidates, with the most similar record at the top.
Review the candidates. Hover over any candidate’s gray match grade bar to view the record’s Dun & Bradstreet DUNSRight match information, including its confidence code and match grade details (if available). The blue match grade legend shows Dun & Bradstreet code letters that correspond to these fields in your account record, in this order left to right. 
Account Name
 or 
Tradestyle
 
| 
Street Number
 | 
Street Name
 | 
City
 | 
State / Province
 | 
Mailing Address / PO Box
 | 
Phone
Click any code letter to see the field name and an explanation.
Letter
Definition
A
Values either match exactly or are similar enough that they should be considered to match.
B
Values are similar.
In some cases, when Data.com and Dun & Bradstreet data are updated at different times, if a field on the D&B Company record is blank, the match grade letter will indicate that the field values are similar (B) instead of blank (Z).
F
Values do not match.
Z
Value is blank in one or both of the records.
When the 
State
 value is blank within a D&B Company record, the 
Street
 field value is normalized to be blank. In these cases, both fields are colored gray in the match grade legend and the match grade for each is 
Z
.
Highlight the match you want, then click 
Select and Proceed to Clean
. If you don’t find a match you want, click 
Cancel
; your record’s status will remain 
Select Match
.
Clean the record. 
The record’s 
Clean Status
 field is updated.
Example
For example: your organization’s Dun & Bradstreet Confidence Code setting is 
9
. You open your United Technologies account record to find that its 
Clean Status
 field value is 
Select Match
. You click the status value to open the select match page. There you see four match candidates from Data.com. You hover over the match grade bar for the top candidate: United Technologies Corporation. In the popup, you see that the United Technology record’s confidence code is 
8
. The match grade legend shows these letters: 
B A A B A A Z
. You know that 
A
 means the values should be considered to match, so you check out the values that do not.
You click the first 
B
 and see that the 
Account Name
 field values are similar, but not a clear-cut match: your record’s 
Account Name
 value does not include 
Corporation 
while the 
Data.com
 record’s 
Account Name
 value does. You click the second 
B
 and find that your record’s 
City
 value is 
Francisco
 while the 
Data.com
 record’s 
City
 value is 
San Francisco
. So... similar, but not an exact match. You click 
Z
 and see that your record’s 
Phone
 value is blank. The 
Data.com
 record’s 
Phone
 value is 
+1.415.901.7000
.
The 
Data.com
 United Technologies Corporation record looks like the one that matches your United Technologies record, so you select it, then click 
Select and Proceed to Clean
. On the comparison page, you click 
Select All
 to accept all of the 
Data.com
 record’s values, including the 
Account Name
, 
City
, and 
Phone
 values that were different or blank in your record. You click 
Save
 and your record’s clean status changes to 
In Sync
. Awesome.
See Also:
Select a Different Data.com Match for a Salesforce Account Record
Clean Individual Salesforce Records
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_selecting_matching_record.htm&language=en_US
Release
200.20
