### Topic: This article discusses how to prepare existing sandboxes for a production organization's migration to the Salesforce Government Cloud.
Salesforce does not migrate sandbox organizations between instances when the production instance is migrated. 
 
For Government Cloud customers, any existing sandboxes connected with your production organization will continue to exist on the sandbox instance where it resides before the migration. These non-migrated sandboxes will remain active, and you will continue to be able to access and use them.
 
Note: 
After migration, whenever you create a new 
sandbox or refresh an existing one, the new sandbox copies will
 automatically be directed to a Govt Cloud sandbox server.
Resolution
If your Salesforce production organization will be migrated to the Government Cloud, you will have two choices:
 
1. If you no longer need any existing sandboxes, you can 
delete
 them yourself 
prior to migration.
 Once your org's migration is complete, you will be able to create a new sandbox org on the Government Cloud. 
Note:
 Because of the nature of the 
Govt Cloud, i
t is advisable to refresh these
 existing sandboxes onto the Govt Cloud, or delete them and create new ones on the Govt Cloud, as soon as possible after migration.
2. If you need to, you can continue to work with the non-migrated sandboxes after your production migration, with a few caveats:
 
- You will need to login to the non-migrated sandbox(es) via 
test.salesforce.com
.
- You will need to login to Government Cloud sandboxes via the sandbox version of your organization's My Domain.
 
- Change Set deployment via the Salesforce user interface may function where a deployment connection already exists between production and sandbox.
- Deployments with the Force.com IDE Tool, etc. should continue to work as expected.
 
 
