### Topic: Viewing Signup Request Details | Salesforce
Viewing Signup Request Details | Salesforce
Viewing Signup Request Details
User Permissions Needed
To create or view signup requests:
“Signup Request API”
From the Signup Request detail page:
Click 
Delete
 to delete the signup request
Click 
Clone
 to create a new signup request with the same attributes as this one
The detail page has the following sections.
Signup Request Detail
Signup Request History
Signup Request Detail
This section displays the following attributes (in alphabetical order).
Attribute
Description
Company
The name of the company requesting the trial signup.
Country
The two-character, upper-case ISO-3166 country code. You can find a full list of these codes at a number of sites, such as: 
www.iso.ch/iso/en/prods-services/iso3166ma/02iso-3166-code-lists/list-en1.html
Created Org
The 15–character Organization ID of the trial organization created. This is a read-only field provided by the system once the signup request has been processed.
Email
The email address of the admin user for the trial signup.
Error Code
The error code if the signup request isn’t successful. This is a read-only field provided by the system to be used for support purposes.
First Name
The first name of the admin user for the trial signup.
Last Name
The last name of the admin user for the trial signup.
Edition
The Salesforce template that is used to create the trial organization. Possible values are 
Partner Group
, 
Professional
, 
Partner Professional
, 
Sales Professional
, 
Professional TSO
, 
Enterprise
, 
Partner Enterprise
, 
Service Enterprise
, 
Enterprise TSO
, 
Developer
, and 
Partner Developer
.
Preferred Language
The language of the trial organization being created. Specify the language using a language code listed under Fully Supported Languages in “Which Languages Does Salesforce Support?” in the Salesforce Help. For example, use 
zh_CN
 for simplified Chinese. The value you select overrides the language set by locale. If you specify an invalid language, the organization defaults to English.
Populated during the sign-up request and for internal use by Salesforce.
ShouldConnectToEnvHub
When set to 
true
, the trial organization is connected to the Environment Hub. The sign-up must take place in the hub master organization or a spoke organization.
Source Org
The 15–character Organization ID of the Trialforce Source Organization from which the Trialforce template was created.
Status
The status of the request. Possible values are 
New
, 
In Progress
, 
Error
, or 
Success
. The default value is 
New
.
Template
The 15–character ID of the approved Trialforce template that is the basis for the trial signup. The template is required and must be approved by Salesforce.
Template Description
The description of the approved Trialforce template that is the basis for the trial signup.
Trial Days
The duration of the trial signup in days. Must be equal to or less than the trial days for the approved Trialforce template. If not provided, it defaults to the trial duration specified for the Trialforce template.
Username
The username of the admin user for the trial signup. It must follow the address convention specified in RFC822: 
www.w3.org/Protocols/rfc822/#z10
Signup Request History
This section shows the date the signup request was created, the user who created it, and the actions that have been performed on it.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=signup_request_details.htm&language=en_US
Release
202.14
