### Topic: How to resolve an error stating 'Unhandled Error in Silverlight Application [Arg_NullReferenceException]'.
When going to the field mapping configuration in Microsoft Dynamics CRM 2011 on-premise or online, the screen presents an Error on Page warning in the bottom left corner of the screen. When you click the error, it provides the following details: 
Webpage error details:
User Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E) 
Timestamp: Wed, 28 Sep 2011 12:00:38 UTC 
Message: Unhandled Error in Silverlight Application [Arg_NullReferenceException]
Resolution
Follow these steps to resolve this error:
1. In Microsoft Dynamics CRM, select the Advanced Find option.
2. In Look For, select 
ExactTarget Configurations
.
3. Click the exclamation mark (!)  for the results. 
4. Double click the record that is returned.
5. Verify that the 
OAuth Application ID
 is blank. 
6. For this value, input: 6C7F3A46-9BFD-40E9-87EE-456577D4FFE7 
7. Click 
Save & Close
.
