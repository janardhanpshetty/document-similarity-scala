### Topic: Detailed information on what information a person can see when working on a performance summary under View Recent Work.
What information can users see under 
View Recent Work
 when completing a summary?
Resolution
All the information about the subject of the performance summary that has happened between a cycles "Activity from" and "Activity to" date will be captured in recent work. For more detailed information, take a look at the following table:
When the subject of the performance summary completes his/her 
Self Summary
 under View recent Work, he/she can see:
  
Sub Tab
Visibility
Current Summaries
All current peer and skip level summaries that have been shared with the subject, related to the summary cycle that this summary is part of.
Past Summaries
All past self and manager summaries about the subject that have been completed and shared with him/her.
Goals
All public and private goals in which the subject is a contributor.
Thanks
All Thanks given to the subject of the performance summary.
Feedback
All Feedback about the subject that has been explicitly shared with him/her.
Coaching
All coaching tasks that have been assigned to the subject.
When the manager completes a
 Performance Summary
 on his/her  direct report under View recent Work, he/she can see:
  
Sub Tab
Visibility
Current Summaries
All peer, skip level and self summaries about the subject that have been shared with the writer, related to the summary cycle that this summary is part of.
Past Summaries
All self and manager summaries about the subject that have been completed and shared with the writer.
Goals
All public and private goals in which the subject is a contributor and the writer has access to.
Thanks
All Thanks given to the subject of the performance summary.
Feedback
All Feedback about the subject that has been explicitly shared with the writer.
Coaching
All coaching tasks that have been assigned to the subject that the writer has access to.
When a person completes 
Peer or Skip Level Summary
 under View recent Work, they can see:
  
Sub Tabs
Visibility
Goals
All public and private goals in which the subject is a contributor and the writer has access to.
Thanks
All Thanks given to the subject of the performance summary.
Feedback
All Feedback about the subject that has been explicitly shared with the writer.
Coaching
All coaching tasks that have been assigned to the subject that the writer has access to.
