### Topic: Adding Video to Content Blocks in Design Mode | Salesforce
Adding Video to Content Blocks in Design Mode | Salesforce
Adding Video to Content Blocks in Design Mode
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To build, edit, and manage Site.com sites:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
To edit only content in Site.com sites:
Site.com Contributor User
AND
Contributor role assigned at the site level
Designers and site administrators can add YouTube®, Google®, Adobe® Flash®, Windows Media®, and Apple QuickTime® videos to content blocks when viewing a page in Design Mode.
When the page is open:
Double-click the content block on the page.
Position your cursor where you want to insert the video and click 
.
In the Video Properties dialog box, select the video type and either:
Enter the URL to the video in the 
Video URL
 text box—for example, 
http://www.youtube.com/watch?v=123abc
.
Select a video from your website by clicking 
From Website
 and selecting the video in the list that appears.
Upload a video from your computer by opening the Upload tab, browsing to the image, and clicking 
Upload
.
Note
You can only select or upload Flash, Windows Media, and QuickTime videos.
To specify how the video is displayed on the page, you can set:
The width and height of the video
How much space surrounds the video (which is controlled by the 
HSpace
 and 
VSpace
 properties)
How it aligns with the text on the page
You can also preview the video.
Click 
Apply
. The video appears as an icon in the content block.
Click 
Save
.
Note
You can view all video types, other than Windows Media videos, when you 
preview a page
.
See Also:
Editing Content Blocks in Design Mode
Understanding the Content Editing Toolbar
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_contentblock_video.htm&language=en_US
Release
202.14
