### Topic: When using the Lightning Process Builder or Flow and a Contains Operator with multiple values separated by commas the Process/Flow will not evaluate correctly.
When using the Lightning Process Builder or Flow and a Contains Operator with multiple values separated by comma deliminators the Process/Flow will not evaluate as intended. 
Example Process:
Set Filter Conditions: 
[Case].Contact.Email   CONTAINS   someone@company.com,someoneelse@company.com
The above example process will not evaluate as intended because it uses multiple values separated by a comma. 
Resolution
The Lightning Process Builder and Flow do not support comma-separated values in rule criteria. In order to get this functionality to work with the Process/Flow criteria can be modified to only use one value per Filter Condition and add additional Filter Conditions as needed ensuring that each Filter Condition is grouped together with an OR Filter Condition.
Work Around Example:
Using the same condition as above:
Set Filter Conditions: 
1) [Case].Contact.Email   CONTAINS   someone@company.com
2) [Case].Contact.Email   CONTAINS   someoneelse@company.com
Filter Conditions (
Customize the filter logic):
(1 OR 2) 
