### Topic: Supported Objects for Omni-Channel | Salesforce
Supported Objects for Omni-Channel | Salesforce
Supported Objects for Omni-Channel
Omni-Channel turbocharges your agents’ productivity by assigning records to them in real time. But which objects and records does Omni-Channel support?
Available in: Salesforce Classic
Omni-Channel is available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Omni-Channel currently supports routing for the following objects and records.
Cases
Chats
SOS video calls
Social posts
Orders
Leads
Custom objects
See Also:
Service Channel Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=service_presence_supported_objects.htm&language=en_US
Release
202.14
