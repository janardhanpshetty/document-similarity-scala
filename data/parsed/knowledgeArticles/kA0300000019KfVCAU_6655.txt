### Topic: This is working as designed. "id" param is used in many places and special uses ("Standard Controllers"). But other params are not kept in "action" attribute for <form>. However, those params are still accessible using properties. And that is the approach that should be used to access those params. Other method of accessing those params (say javascript to access URL param) are not supported.
Let's assume that visualforce page is accessed using following URL: /apex/VFPage?id=1&test=1&test2=2 using following code:
 
Apex Class
*****************************
public class testPageControler{
    public testPageControler(){
    }
    public PageReference doAction(){
        return null;
    }
}
*****************************
Visualforce
*****************************
<apex:page controller="testPageControler" showHeader="false" >
  <apex:form >
      <apex:commandLink value="Click Me" action="{!doAction}" / > 
  </apex:form>
</apex:page>
Upon clicking "Click Me", page is submitted to "/apex/VFPage?id=1". Other params are stripped. 
Resolution
This is working as designed. "id" param is used in many places and special uses ("Standard Controllers"). But other params are not kept in "action" attribute for <form>. However, those params are still accessible using properties. And that is the approach that should be used to access those params. Other method of accessing those params (say javascript to access URL param) are not supported.
Following code is an example of how to use above params in VF page. 
Note: This is a sample code. Please test and edit it accordingly. 
public class QueryParamController {
    public QueryParamController() {
        counter = 909;
    }
    private Integer counter;
    public String getCounterVal() {
        return String.valueOf(counter);
    }
    public PageReference incrementCounter() {
        ++counter;
        return null;
    }
    public List<String> getAllParamKeys() {
        Set<String> keys = System.currentPageReference().getParameters().keyset();
        List<String> copy = new List<String>();
        for (String kk : keys) {
            copy.add(kk);
        }
        return copy;
    }
    public List<String> getAllParamValues() {
        List<String> vals = System.currentPageReference().getParameters().values();
        List<String> copy = new List<String>();
        for (String vv : vals) {
            copy.add(vv);
        }
        return copy;
    }
}
 
<!--  QueryParamPage  -->
<apex:page controller="QueryParamController">
  <apex:form >
    Counter: {!counterVal} <p/>
    Query Param Keys: {!AllParamKeys} <p/>
    Query Param Vals: {!AllParamValues} <p/>
    <apex:commandButton value="Increment" action="{!incrementCounter}"/>
  </apex:form>
  <p/> QueryParamPageMarker
</apex:page>
 
