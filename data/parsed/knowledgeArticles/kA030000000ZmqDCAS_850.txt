### Topic: Process to follow for Partner request to convert, increase or swap licenses.
Partners who have reseller accounts will often make requests to modify the license count in a customers org. To determine who in support will handle the request, please follow these guidelines.
Resolution
First, gather the Account Id for the Customers org. You can do this by pulling up the org Id provided by the partner via BT. With the Account loaded, scroll down to the Contracts related list. Look for a Contact with a "
Re-seller" status.
If there is a contract with the Re-seller status, meet SLA and move the case to the "
Alliances Shared Services
" queue.
If there is NOT a contract with the Re-seller status, inform the Partner that this customer will have to work with their Account Executive for this request. Also, reach out to the AE and let them know the Account name and the details of the request that has been made.
Also, if the case has an attachment similar to 
"Exhibit A - Form of Service Order", this request should also be moved to the 
"
Alliances Shared Services
" queue.
If the ask is to swap a license in a sandbox
: If the ask is indeed for a 
swap
 and not a 
match
, the swap will have to be executed in the prod org 
first
 by alliances. Then tech support will have to finalize the ask by swapping the license in the desired sandbox.
