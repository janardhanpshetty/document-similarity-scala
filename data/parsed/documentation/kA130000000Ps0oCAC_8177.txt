### Topic: Editing Multiple Campaign Members | Salesforce
Editing Multiple Campaign Members | Salesforce
Editing Multiple Campaign Members
Available in: Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view the Existing Members tab:
“Edit” on campaigns
AND
“Read” on leads or contacts
AND
Marketing User
 checked in your user information
To update and remove campaign members:
“Edit” on campaigns
AND
“Edit” on leads and contacts
AND
Marketing User
 checked in your user information
Editing Campaign Members from the Manage Members Page
Note
Before using the Manage Members page, verify that you are using a supported browser.
To update the campaign member status, edit campaign member details, or remove campaign members from the Manage Members page:
Click 
Manage Members
 and choose 
Edit Members - Search
 from the drop-down button on a campaign detail page or the Campaign Members related list on a campaign detail page.
On the Existing Members subtab, optionally enter filter criteria to find existing members and click 
Go!
.
Note
For campaigns with campaign members created from both leads and contacts, you must have “Read” permission on leads and contacts to see all members. If you only have “Read” on leads, you will only see campaign members created from leads; if you only have “Read” on contacts, you will only see campaign members created from contacts.
Select the checkboxes next to the records you want to edit. To select all records on the current page, select the checkbox in the header row.
Optionally perform the following actions:
To change the campaign member status, select a status from the 
Update Status
 drop-down list.
To remove a member from a campaign, click 
Remove
.
To edit the details of a campaign member, click 
Edit
.
To 
view, edit, delete, clone, a campaign member
, click the campaign member's name in the Name column.
The latest Manage Members page is not available in accessibility mode. In accessibility mode, you can still 
add contacts or leads to a campaign
, 
update campaign members
, 
and 
remove campaign members
 
using the campaign member wizards.
See Also:
Manage Campaign Members
Adding Multiple Campaign Members
Displaying and Editing a Campaign Member
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=campaigns_members_editing.htm&language=en_US
Release
202.14
