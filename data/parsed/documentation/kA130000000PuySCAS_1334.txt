### Topic: Control Access to Chatter Desktop | Salesforce
Control Access to Chatter Desktop | Salesforce
Control Access to Chatter Desktop
Chatter Desktop is enabled for most organizations, but administrators can control access to Chatter Desktop.
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, 
Developer
, and 
Database.com
 Editions
Salesforce CRM, Approvals, Chatter email notifications, Chatter Invitations, and customer invitations are not available in 
Database.com
.
From Setup, enter 
Chatter Desktop Settings
 in the 
Quick Find
 box, then select 
Chatter Desktop Settings
.
Select 
Enable Chatter Desktop
 to allow users to access Chatter data from Chatter Desktop.
Deselect this option to block access to the Chatter Desktop self-installation page and prevent all instances of Chatter Desktop from accessing Chatter data.
Select 
Allow Chatter Desktop Managed Installations Only
 to prevent non-administrators from installing Chatter Desktop.
For example, select this option if your IT department plans to deploy Chatter Desktop to your organization.
Save your changes.
See Also:
Chatter
Install Chatter Desktop Managed Version
Configure Chatter Desktop Managed Version
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_desktop_admin.htm&language=en_US
Release
202.14
