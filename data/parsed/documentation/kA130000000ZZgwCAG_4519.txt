### Topic: Sample Financial Calculations Formulas | Salesforce
Sample Financial Calculations Formulas | Salesforce
Sample Financial Calculations Formulas
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
For details about using the functions included in these samples, see 
Formula Operators and Functions
.
Compound Interest
This formula calculates the interest, you will have after T years, compounded M times per year.
Principal__c * ( 1 + Rate__c / M ) ^ ( T * M) )
Compound Interest Continuous
This formula calculates the interest that will have accumulated after T years, if continuously compounded.
Principal__c * EXP(Rate__c * T)
Consultant Cost
This formula calculates the number of consulting days times 1200 given that this formula field is a currency data type and consulting charges a rate of $1200 per day. Note that 
Consulting Days
 is a custom field.
Consulting_Days__c *
                    1200
Gross Margin
This formula provides a simple calculation of gross margin. In this formula example, 
Total Sales
 and 
Cost of Goods Sold
 are custom currency fields.
Total_Sales__c - Cost_of_Goods_Sold__c
Gross Margin Percent
This formula calculates the gross margin based on a margin percent.
Margin_percent__c * Items_Sold__c * Price_item__c
Payment Due Indicator
This formula returns the date five days after the contract start date whenever 
Payment Due Date
 is blank. 
Payment Due Date
 is a custom date field.
(BLANKVALUE(Payment_Due_Date__c, StartDate +5)
Payment Status
This formula determines if the payment due date is past and the payment status is “UNPAID.” If so, it returns the text “PAYMENT OVERDUE” and if not, it leaves the field blank. This example uses a custom date field called 
Payment Due Date
 and a text custom field called 
Payment Status
 on contracts.
IF( 
AND(Payment_Due_Date__c < TODAY(),
ISPICKVAL(Payment_Status__c, "UNPAID")), 
"PAYMENT OVERDUE", 
null )
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=useful_advanced_formulas_financial.htm&language=en_US
Release
202.14
