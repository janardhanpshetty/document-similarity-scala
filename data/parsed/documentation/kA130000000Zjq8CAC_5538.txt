### Topic: Tips for Creating Macros | Salesforce
Tips for Creating Macros | Salesforce
Tips for Creating Macros
How you name and design your macro can impact its usefulness to support agents. Keep these tips in mind when creating macros.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
The 
Macro Name
 helps support agents decide which macro to use. The name is used when agents search for macros, so it’s useful to use a short name (so that agents can see it in the macros list) that succinctly identifies the macro’s purpose.
Although 
Description
 is optional, it is useful for helping support agents to understand what the macro does.
The macro instructions are like a little computer program, so you must tell the macro each step, or instruction, to perform. Each instruction is equivalent to a click that the support agent makes when manually performing the task. The first macro instruction selects the object that the macro acts upon, such as the Active Case Tab.
The second macro instruction specifies the context, or component of the Salesforce Console for Service, in which the macro works. For example, the Email Action context allows the macro to set fields and perform actions within the Email Publisher.
Note
If a Salesforce Console for Service component isn’t enabled and configured, then you can’t create a macro for it.
The third macro instruction specifies the action that the macro performs. For example, the macro in the screenshot changes the value for the CC Address field in an email action in the active case tab.
You can add an additional set of instructions in the same context or in a different context. A simple macro just performs one task. Create more complex macros by adding instructions.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=macros_tips.htm&language=en_US
Release
202.14
