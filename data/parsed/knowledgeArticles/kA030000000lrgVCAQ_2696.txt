### Topic: This article discusses why the latestDateCovered value returned by the getUpdated() method may not update for a long period of time.
Background:
Salesforce exposes two API calls to allow for Data Replication
You may see this referenced unofficially as the "Replication API"
There are two API calls used in this method, 
getUpdated() and getDeleted()
This is an old concept and is essentially replaced by the more modern 
Streaming API
Developers can still use getUpdated() to retrieve the list of record IDs that have been updated in a specified time window.
getUpdated() returns the GetUpdatedResult object, which contains the latestDateCoveredValue
From our documentation
:  "The timestamp of the last date covered in the getUpdated() call".
latestDateCovered is to be used as the start time of the time window when the method is called again
The additional note reads:  "If 
Salesforce
 executes a long-running transaction on your instance, the value in this field is the start time of that long-running transaction until it completes. This is because a long-running transaction might affect your user data (for example, batch processing)."
latestDateCovered is the earliest time before which we are absolutely confident the IDs returned have been updated
Issue:
latestDateCovered is not updated regularly.
It may take hours to update, or in rare cases days.
Resolution
This is a very conservative figure to ensure that all in-flight transactions are not missed.
As per the note in the documentation - "If 
Salesforce
 executes a long-running transaction on your instance..." - any long transaction elsewhere on the service (but only on your instance) may delay latestDateCovered from being updated.
These long-running transactions could be executed by Salesforce to maintain database accuracy and reliability.
Your organisation does not have to be executing any long-running operations to impact latestDateCovered.
Salesforce has identified several intermittent issues in the past that has prevented latestDateCovered from being updated for periods of days.
If you notice latestDateCovered is not updated for 48 hours or longer, please log a support case and quote Knowledge Article 000199031.
Support will attempt to identify any issues on the service that's impacting this value from being updated;  be aware that Support may confirm there are no issues with the service and it is working as designed.
Otherwise, please be patient in waiting for latestDateCovered to be updated and take the steps mentioned below to handle this scenario accordingly.
Best practices and Workarounds:
The following points are 
taken from our documentation
:
Please be aware that it is up to your integration logic to handle and discard any redundant record IDs that are returned as a result of the same latestDateCovered value being passed back to getUpdated().
Client applications should save the timespan used in previous Data Replication API calls so that the application knows the last time period for which data replication was successfully completed.
To ensure data integrity on the local copy of the data, a client application needs to capture all of the relevant changes during polling—even if it requires processing data redundantly to ensure that there are no gaps. Your client application can contain business logic to skip processing objects that have already been integrated into your local data.
Gaps can also occur if the client application somehow fails to poll the data as expected (for example, due to a hardware crash or network connection failure). Your client application can contain business logic that determines the last successful replication and polls for the next consecutive timespan.
If for any reason the local data is compromised, your client application might also provide business logic for rebuilding the local data from scratch.
Alternatively, consider using the more advanced and modern 
Streaming API
 to poll for updates.
