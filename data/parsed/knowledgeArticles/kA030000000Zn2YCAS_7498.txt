### Topic: When I try to add a Product in a New Opportunity it shows an empty list even if I have Active Products in my Organization
Organizations that have Multi-currency enabled have the option of setting the default currency for each Opportunity. 
 
The Opportunity currency is tied to the currency of the associated Price Book entry.  If the Opportunity currency is set to EURO and when you add Products then it will only show Products with EURO List Price.
Resolution
To resolve this you can either change the Opportunity Currency to the available Product Currency by going to Opportunity Tab> Select the Opportunity record> click edit> Change the Opportunity Currency and Save.  
You can also set a List Price in a different currency by following the steps below:
Select the product.
From the product detail page, click 
Edit
 from the Price Books related list next to the custom price book that should contain the list price.
Click 
Add to Price Book
 from the Price Books related list if you have not already added the product to a custom price book.
Enter the list price. Alternatively, select 
Use Standard Price
 if both prices are the same.
Select 
Active
 to make this price available to products on opportunities or quotes.
Click 
Save
.
For reference you can also review the links below:
Set Product Prices:- 
Set Product Prices
Add Product to Opportunities:- 
Add Products to Opportunities
 
