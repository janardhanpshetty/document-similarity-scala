### Topic: On certain Opportunity records, you will notice that you are unable to edit the Amount field; when hovering over it, you will see a lock icon.
On certain Opportunity records, you will notice that you are unable to edit the Amount field; when hovering over it, you will see a lock icon.
Resolution
This happens when the Opportunity has related products. For opportunities with products, the amount is the sum of the related products. You cannot directly edit the amount unless the opportunity has no products. To change the amount for an opportunity that contains products, edit the sales price or quantity of the related products. You can open the following link to see how to 
Edit Products on Opportunities
.
More information about Opportunity fields and how they behave can be found here 
Opportunity Fields
 
