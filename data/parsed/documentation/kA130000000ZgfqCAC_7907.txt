### Topic: Comparison Operators | Salesforce
Comparison Operators | Salesforce
Comparison Operators
Comparison operators return true or false.
Wave Analytics supports the following comparison operators.
Operator
Name
Description
==
Equals
True if the operands are equal. String comparisons that use the equals operator are case-sensitive.
Example predicate expressions:
'Stage Name' == "Closed Won"
!=
Not equals
True if the operands are not equal. String comparisons that use the not equals operator are case-sensitive.
Example predicate expression:
'isDeleted' != "False"
<
Less than
True if the left operand is less than the right operand.
Example predicate expression:
'Revenue' < 100
<=
Less or equal
True if the left operand is less than or equal to the right operand.
>
Greater than
True if the left operand is greater than the right operand.
>=
Greater or equal
True if the left operand is greater than or equal to the right operand.
in
Multi-value list filter
True if the left operand exists in the list of strings substituted for a multi-value picklist (field value).
Example predicate expression:
'Demog' in ["$User.Demographic__c"]
In this example, 
Demographic__c
 is of type 
multiPicklistField
. During evaluation, the multi-value picklist field is substituted by a list of strings, with 1 string per user-selected item.
Note
Comma-separated lists are not supported within the square-bracket construct.
You can use the <, <=, >, and >= operators with measure columns only.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_datasets_predicate_operatorscomparison.htm&language=en_US
Release
202.14
