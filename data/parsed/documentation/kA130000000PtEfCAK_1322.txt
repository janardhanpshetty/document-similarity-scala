### Topic: Change Sets | Salesforce
Change Sets | Salesforce
Change Sets
Use change sets to send customizations from one Salesforce org to another. For example, you can create and test a new object in a sandbox org, then send it to your production org using a change set. Change sets can only contain modifications you can make through the Setup menu. For example, you can’t use a change set to upload a list of contact records. Change sets contain information about the org. They don’t contain data, such as records.
Available in: both Salesforce Classic and Lightning Experience
Available in 
Enterprise
, 
Performance
, 
Unlimited
, and 
Database.com
 Editions
User Permissions Needed
To edit deployment connections:
“Deploy Change Sets” AND “Modify All Data”
To use outbound change sets:
“Create and Upload Change Sets,”
“Create AppExchange Packages,”
AND
“Upload AppExchange Packages”
To use inbound change sets:
“Deploy Change Sets” AND “Modify All Data”
When you want to send customizations from your current org to another org, create an 
outbound change set
. Once you send the change set, the receiving org sees it as an 
inbound change set
.
Sending a change set between two orgs requires a deployment connection. Change sets can only be sent between orgs that are affiliated with a production org. For example, a production org and a sandbox, or two sandboxes created from the same org can send or receive change sets.
See Also:
Deployment Connections for Change Sets
Outbound Change Sets
Inbound Change Sets
Components Available in Change Sets
Special Behavior in Deployments
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=changesets.htm&language=en_US
Release
202.14
