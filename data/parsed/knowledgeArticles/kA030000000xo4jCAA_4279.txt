### Topic: User with “Edit Self-Service Users” and CRED for Accounts and Contacts cannot set the Profile when enabling a Customer Portal User
User with “Edit Self-Service Users” and CRED for Accounts and Contacts cannot set the Profile when enabling a Customer Portal User
Resolution
The most likely reason for this is that the User is a Delegated Admin.
Delegated administrators cannot edit Users in the Roles specified in the Delegate Group, because High Volume Portal Profiles don't have Roles.
You can't create a High Volume Portal user for the first user on an Account because the Role hasn't been created. 
Solution: Grant the User the 'Manage Users' permission or remove them from the Delegated Admin group
