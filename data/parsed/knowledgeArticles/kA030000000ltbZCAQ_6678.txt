### Topic: The Salesforce for Twitter and Facebook AppExchange application is designed to connect Salesforce with social channels and allow agents to retrieve social posts, interact with them from within Salesforce and post replies to social media sites from within Salesforce. Although it is not designed to be an application to only sync one way it is possible to configure the app to only allow Salesforce users to retrieve social media and prevent users from posting externally to their site.
There may be circumstances where Administrators would like to leverage the 
Salesforce for Twitter and Facebook
 application to only pull social media comments and posts into Salesforce and prevent users from posting externally to social media sites from within Salesforce.
The application's design intent is to retrieve and bring social media posts into Salesforce so users may action them appropriately and reply externally to social media from within Salesforce and is not intended to be a one way sync from social media to Salesforce. With this information, there is no explicit setting within the app's setup, configuration, or functionality that would effectively block users from posting to social media entirely. However, Administrators can manipulate the availability of the apps various features to help mitigate the possibility of users posting externally to Facebook and Twitter.
  
Warning
This article may not be an all inclusive list of required changes to prevent users from posting to social media and is intended to be a general guide. It's essential to perform thorough testing to ensure users do not have the ability to post to social media through your organization's unique implementation of the application or other means that may not be explicitly covered here. Salesforce will not be held responsible for external posts or comments made by users from your organization as the result of the application's use.
Resolution
When configuring the Facebook portion of the App to pull posts from your organization's Facebook page you're presented with a prompt upon authentication.  It reads, "CRM would like to post to Facebook for you." If you select Not Now, the authentication process is unable to properly associate the integration to your Facebook page.  So in order to complete the configuration you must allow the application to post to Facebook for you hence, preventing you from blocking the application from posting to Facebook from an integration stand point.
Similarly, when authorizing the app to access a Twitter account you're presented with a prompt stating, "This application will be able to:
- Read Tweets from your timeline.
- See who you follow, and follow new people.
- Update your profile.
- Post Tweets for you.
- Access your direct messages."
There is no way to alter these selections on authorization and not possible to deselect the option to "Post Tweets for you." If you choose the Cancel option instead of Authorize app you will not be able to complete the configuration process to allow Salesforce to be approved for use effectively preventing the app's integration with Twitter.
However, Administrators may be able to prevent users from posting replies to social media by removing the package's reply buttons from list views, page layouts and related lists via 
Setup, Create | Objects | Locate the "Conversation" object and click its label.
1. Navigate to the Buttons, Links, and Actions section and open the setup for each button with the label "Reply" and click the "Where is this used?" button to locate where each reply button is currently employed to remove it from appropriate setup sections.
2. Confirm that the Reply button has been removed from the following areas within the Conversation object's setup page:
Conversations List View by clicking its Edit link in the Search Layout section 
Conversation Page Layout(s) by opening each via their Edit links in the Page Layouts section
- Remove the Reply detail page button from each Conversation page layout
- Remove the Reply button from the following related list's on each Conversation page layout: Open Activities, Activity History, and Child Conversations.  See 
Customizing Related Lists
 for more details
- Be sure to save each page layout after committing the changes
3. Remove the Task's Reply list button via 
Setup, Customize | Activities | Task Buttons, Links, and Actions
.  Open the button's setup by clicking its label "Reply" and selecting the "Where is this used?" button to locate each page layout where it's currently in use.  Open each layout and ensure the Reply button is removed from the Activity History and Open Activities related lists.  Again, be sure to save each page layout after making the changes.
4. The Conversations object shares as relationship with the following standard Salesforce objects. Check to confirm the reply button is removed from the conversation's related list for all page layouts on the following object's:
- Account
- Case
- Contact
- Lead
- Solutions
5. The Social Publisher and Social Agents Tabs will need to be marked as Tab Hidden for user's assigned profiles as well since these include a Share button that will allow users to insert messages externally on social media.
See Also:
Managing Page Layouts
Customizing Page Layouts with the Enhanced Page Layout Editor
 
