### Topic: An external user may receive an "Insufficient Privileges" error message when viewing reports with certain Standard Record Types, such as Accounts with Assets, or Products, even though they have access to the folder and can view other reports in that folder.
An external user may receive an "Insufficient Privileges" error message when viewing reports with certain Standard Record Types, such as Accounts with Assets, or Products, even though they have access to the folder and can view other reports in that folder.
In order for External users to have access to a report, the Org-Wide Defaults (OWD) of each object included in the report type must be set to Private.
However, Products is a standard object included in many standard record types, most notably "Accounts with Assets" and "Products" report types. 
Since Products do not have OWD settings, it is impossible to set these to Private.
 
Resolution
To work around this, you can create a Custom Report Type utilizing, for example, Accounts with Assets, and then include all of the relevant fields used in the Standard Report Type. You will still be able to add the products fields, but they will not show up when the external user accesses the report.
More details on how to create a Custom Report Type 
here
