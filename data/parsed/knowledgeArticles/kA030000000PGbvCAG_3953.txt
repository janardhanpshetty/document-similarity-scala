### Topic: Users may encounter an error when trying to download a Data.com Reporting Package.
While attempting to install a Data.com reporting package but receive the error "Package Install Error There are problems that prevent this package from being installed."
Example Problem Descriptions:
"The requested package does not exist or has been deleted" 
"Missing Organization Feature: DataDotComClean"
"Missing Organization Feature: DandBEnterprise null" 
Resolution
In order to install a Data.com Reporting package, the coinciding product is required. Specifically;
The Data.com Prospector Reporting Package requires a Prospecting license to have been purchased
The Data.com Clean Reporting Package requires Clean to have been purchased
The Data.com Premium Reporting Package requires a Premium license 
For more information, please review the System Requirements found in the details of each package on the AppExchange:
Data.com Clean Reports
Data.com Prospector Reports
Data.com Premium Reports
