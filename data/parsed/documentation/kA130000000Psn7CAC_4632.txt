### Topic: Search for Content | Salesforce
Search for Content | Salesforce
Search for Content
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To search Salesforce CRM Content:
Salesforce CRM Content User
 checked in your personal information
AND
Member of the library
The Content tab displays files, content packs, Web links, and Google docs published in your Salesforce CRM Content libraries. 
If Chatter is enabled, and your administrator has enabled the setting to show Salesforce Files in Salesforce CRM Content
, files posted to Chatter groups, and other files that aren't private or privately shared, are also displayed on the Content tab. The top twenty most recently modified items are listed by default. You can filter the list using the Filter Your Results sidebar.
To search for specific content:
From the Search drop-down list, restrict your search to a specific library or choose to search in all libraries. If available, restrict your search to just Salesforce Files or just files in Chatter groups you're a member of.
Tip
To display all the locations where the content appears, click 
Display Options
 and select 
Display Locations
. The locations are listed under each title.
Enter your search term(s) and click 
Go!
. 
Salesforce CRM Content performs a full-text search of the following document types: rich text format (RTF), UTF-8 encoded TXT, HTML, XML, Adobe® PDF, and Microsoft® Office 97 through Microsoft Office 2007 Word, Excel, and PowerPoint files.
If your administrator has enabled multi-language support, you can restrict your search to a specific language. By default, Salesforce CRM Content searches all the content in your libraries that is published in your default user language. Searching in all languages also searches the titles, author names, tags, file extensions, and custom fields of content in all languages.
Note
Searching in all languages does not search the text or description of documents that were published in languages other than your default user language.
Optionally, in the 
Filter Your Results sidebar, filter your search results by file format, featured content, author, tag, libraries, language, custom field, or Chatter group (if available). The number in parentheses next to each filter type shows you how many matching files, content packs, Web links, and Google docs are in the search results.
The Content tab provides several options:
Click 
Display Options
 to customize your view by adding sortable columns for various content criteria (such as Size and Publication Date), or choosing to display descriptions, tags and locations. 
Display Locations
 shows the libraries and Chatter groups where the content appears. 
My Libraries: none
 or 
My Chatter Groups: none
 means that the file is a Salesforce file and is not part of any libraries or Chatter groups. The “smart bar” graphic for downloads, comments, ratings, and subscribers allows you to compare files, content packs, Google docs, and links within a search result set.
Note
Chatter groups are only included in 
Display Locations
 if Chatter is enabled, and your administrator has enabled the setting to show files in Salesforce CRM Content.
Select one or more files and click 
Download
 to create a zip file with your selected content. Web links and Google docs cannot be included in zip files.
Hover over a file icon to see a snapshot of information about the specific file, content pack, Google doc, or Web link and options such as subscribing, voting, and downloading.
Click the subscription icon next to the file name to toggle a subscription on or off. For more information, see 
View and Edit Content Subscriptions
.
Click the file name to view the content details page. The content details page provides all the available information about a file, content pack, Google doc, or link. For more information, see 
View and Edit Content Details
.
Search supports several file types and has file size limits. If a file exceeds the maximum size, the text within the file isn't searched, but the file's author name, tags, file extension, and custom fields are.
File Type
File Extensions
Maximum File Size for Search
HTML
.htm
, 
.html
, 
.xhtml
5 MB
PDF
.pdf
25 MB
PPT
.ppt
, 
.pptx
, 
.pptm
25 MB
RTF
.rtf
5 MB
Text
.c
, 
.cpp
, 
.css
, 
.csv
, 
.ini
, 
.java
, 
.log
, 
.sql
, 
.txt
5 MB
Word
.doc
, 
.docx
, 
.docm
25 MB
XLS
.xls
, 
.xlsx
, 
.xlsm
5 MB
XML
.xml
5 MB
See Also:
Search for Files
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=content_search.htm&language=en_US
Release
202.14
