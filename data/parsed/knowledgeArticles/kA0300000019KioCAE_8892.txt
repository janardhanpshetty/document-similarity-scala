### Topic: Some best practices to keep in mind when implementing Subscriber Key
Subscriber Key Considerations
A Subscriber Key should be implemented for each and every subscriber within your database.
It should NOT change.  Reconfiguration of the Subscriber Key takes effort and planning.  Therefore, give thought to the value being used and make sure it's going to work for your organization now and long-term.
Keep in mind: the purpose of Subscriber Key is to allow an individual email address to receive more than 1 of the same message. If this is a concern, there are ways to suppress based on email address. 
Subscriber Key allows users to change their email address and still retain tracking data in Salesforce Marketing Cloud.  Otherwise a new record would need to be created and tracking data would be lost.
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
