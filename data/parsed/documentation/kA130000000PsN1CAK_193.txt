### Topic: How do I use the import wizard to mass update existing person accounts? | Salesforce
How do I use the import wizard to mass update existing person accounts? | Salesforce
How do I use the import wizard to mass update existing person accounts?
To mass update existing 
person account
 records via the import wizards:
Run a 
person account
 report and export the results to Excel. To enable record matching, make sure to include the name, email address, 
Salesforce
 ID, or external ID field in the report.
Use Excel or a similar application to update field values in the import file as desired.
Launch the appropriate import wizard.
If you own the records in your import file, launch the Import My 
Person Accounts
 wizard from your personal settings by selecting 
Import
 | 
Import 
Person Accounts
.
If the records in your import file are owned by multiple different users, launch the Import My Organization's 
Person Accounts
 wizard from Setup by entering “Import 
Person Accounts
” in the 
Quick Find
 box, then selecting 
Import 
Person Accounts
.
At the Prevent Duplicates step:
If you are using the Import My Organization's 
Person Accounts
 wizard, choose the 
Yes
 radio button. This option is not included in the other wizard.
In the “Which field...” section, choose the field in your file to use for record matching: name, 
Salesforce
 ID, email address, or external ID.
In the “If existing records...” section, choose the second option, 
Update existing records and do not insert any new records
.
Proceed with the remaining steps in the wizard.
After finishing the wizard and receiving the email notification that the process has completed, verify in 
Salesforce
 that the records have been updated as you intended.
Important
Salesforce
 has replaced the individual import wizards for accounts, contacts, and other objects with the unified Data Import Wizard. (Individual import wizards open in small pop-up windows, while the unified wizard opens in a full browser with dataimporter.app at the end of the URL.) To start using the unified wizard, from Setup, enter 
Data Import Wizard
 in the 
Quick Find
 box, then select 
Data Import Wizard
. (The options you see depend on your permissions.)
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_import_person_how_do_i_use.htm&language=en_US
Release
198.17
