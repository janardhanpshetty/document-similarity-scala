### Topic: Lightning for Outlook | Salesforce
Lightning for Outlook | Salesforce
Lightning for Outlook
Stay on top of important sales opportunities when you work in Outlook. When using Outlook® Web App (OWA), Outlook 2016, or Outlook 2013 along with Microsoft Office 365™, you can manage your sales more efficiently. Relate email and attachments to Salesforce records. And, create Salesforce records—directly in Outlook.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
After your administrator enables Lightning for Outlook, you complete one-time procedures to get your Salesforce content to appear in your email application.
Then, when you select an email (1), then select 
Salesforce
 (2), relevant sales-related records appear (3). For the records that appear, you can relate the email to multiple contacts—provided your administrator enabled Shared Activities. Also add the email to one other record that accepts tasks, like an opportunity, a lead, or a case. You choose whether to include the attachments. 
Create Salesforce records directly from Outlook (4). Select the record to see more details about it in your email application. Or get complete details about the record or the email related to it directly in Salesforce (5).
Similar to the way you relate emails, Lightning for Outlook lets you relate these calendar events to Salesforce records.
Any event you attend
Any event you organize, if you’re working in Outlook 2016 for Windows 
or
 Outlook Web App on Office 365
To relate an event, select it in your Microsoft email application. Then, select Salesforce (1).
Relevant Salesforce records appear (2). And like when you work with emails, you can select a record to see more details in your email application. Or, you can get complete details about the record directly in Salesforce (3).
Lightning for Outlook System Requirements
Make sure that your system meets these requirements before you set up Lightning for Outlook.
Set Up Microsoft® Outlook® to Experience Salesforce
Get your system ready to relate email, its attachments, and events to Salesforce records from Outlook Web App (OWA), Outlook 2016, or Outlook 2013. Create Salesforce records—all while you’re working in Outlook.
Experience Salesforce in Microsoft® Outlook®
Keep your opportunities on track when you access sales-related records in Microsoft Outlook Web App (OWA), Outlook 2016, and Outlook 2013, along with Office 365™.
See Also:
Lightning for Outlook System Requirements
Experience Salesforce in Microsoft® Outlook®
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=app_for_outlook_user_overview.htm&language=en_US
Release
202.14
