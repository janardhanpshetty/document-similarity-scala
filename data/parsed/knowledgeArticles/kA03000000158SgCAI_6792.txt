### Topic: As documented in Winter '16 release notes, Link Custom Lookup Fields From Activities to Other Records
New in Winter '16, we’ve enabled the Lookup Relationship field for activities. You no longer need to choose which type of record is most important to activities. Now you can link custom lookup fields from activities to other records.
Resolution
Keep these limitations in mind when using Lookup Relationship for activities.
• Custom lookups don’t control activity sharing. Linking to another record using an activity custom lookup doesn’t affect users’ access to the activity—the lookup is simply a reference to another record.
• Each custom lookup field can reference one type of record, one record at a time; no two fields can link to the same type of record. To reference records of different types, create multiple custom lookup fields.
• Custom lookup fields combine open and closed activities in the same related list, with each custom lookup corresponding to a new related list. The lists don’t show activities related to other records using the Name and Related To fields, as with Open Activities and Activity History lists.
• The list of related activities from a custom lookup sorts in descending order by 
ActivityDate
 and
 LastModifiedDate
. You can’t modify the sort.
• You must enable new custom fields before the fields appear on reports. To enable a new custom field, modify the report type and add the new custom field to the list of available fields for the report.
• Custom Report Types that use activity custom lookup field relationships are available if you enabled Shared Activities.
