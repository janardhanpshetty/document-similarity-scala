### Topic: Mailer-deamon emails are not added to Cases via Email to Case in order to prevent email loops from occurring.
I am sending an email from my email2case service address. The address I am  emailing is invalid and a delivery failure bounce back is sent back to the inbox. That delivery failure email is not added to the case even though it contains email2case ref ID. Why is the bounce back is not logged on the case?  Email contains the Ref ID so it should be added to the Case's Email Related list. 
Resolution
This is working as designed.  Salesforce blocks the Mailer-Daemon address in order to prevent email loops.
NOTE: Emails that are returned with the address: Postmaster@<domain>.com should process and attach to the case normally.  This only occurs with Mailer-Daemon Email messages. 
