### Topic: The user is not able to reset the password by following the basic 'forgot password email' method. The email of the newly reset password is not received by the user may be due to email firewall/spam issues.
If a user is not able to receive the password reset email and they urgently need a log into Salesforce; the system administrator has the ability to modify the Salesforce User email address to a secondary email.
 
Resolution
The user will need to contact the System administrator of the organization to change their email address:
1.     From Setup, click 
Manage Users
 | 
Users
.
2.     Select the affected user.
3.     Click Edit.
4.     
Change the Email Address and Save.
5.     Check the Email and verify Email address.
6.     Click 
Reset Password
 to have a new password emailed to the user.
7.     Click on the link and change the password
 
 
Note: If the user is the only administrator in the organization and cannot access their Org, in order to modify their email address, the following KB needs to be followed:
Internal: Evaluation Criteria for Admin Email Change Requests
 
