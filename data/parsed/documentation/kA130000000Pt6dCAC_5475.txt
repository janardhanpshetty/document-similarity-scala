### Topic: Salesforce Knowledge Terminology | Salesforce
Salesforce Knowledge Terminology | Salesforce
Salesforce Knowledge Terminology
Available in: 
Salesforce Classic
Salesforce Knowledge
 is available in 
Performance
 and 
Developer
 editions.
Salesforce Knowledge
 is available for an additional cost in 
Enterprise
 and 
Unlimited
 editions.
The following terms are used when describing 
Salesforce Knowledge
 features and functionality.
Archived Article
Archived articles were published but later removed from public visibility. Article managers can view and manage archived articles on the Article Management tab by clicking the 
Articles
 tab in the View area and choosing 
Archived Articles
. Archived articles are not visible in the Articles tab (in any channel) or the public knowledge base. Articles can be archived manually or automatically via an expiration date.
Article
Articles
 capture information about your company's products and services that you want to make available in your knowledge base.
Article Manager
Salesforce
 uses the term 
article manager
 to represent a specific type of user. Article managers can access the Article Management tab to create, edit, assign, publish, archive, and delete articles. Article managers are sometimes referred to as knowledge managers. Article managers require the “Manage Articles” user permission. The Article Management tab is not visible to users without “Manage Articles.”
Article Type
All articles in 
Salesforce Knowledge
 are assigned to an 
article type
. An article's type determines the type of content it contains, its appearance, and which users can access it. For example, a simple FAQ article type might have two custom fields, 
Question
 and 
Answer
, where article managers enter data when creating or updating FAQ articles. A more complex article type may require dozens of fields organized into several sections. Using layouts and templates, administrators can structure the article type in the most effective way for its particular content. User access to article types is controlled by permissions. For each article type, an administrator can grant “Create,” “Read,” “Edit,” or “Delete” permissions to users. For example, the article manager might want to allow internal users to read, create, and edit FAQ article types, but let partner users only read FAQs. 
Article-Type Layout
An 
article-type layout
 enables administrators to create sections that organize the fields on an article, as well as choose which fields users can view and edit. One layout is available per article type. Administrators modify the layout from the article-type detail page.
Article-Type Template
An 
article-type template
 specifies how the sections in the article-type layout are rendered. An article type can have a different template for each of its four channels. For example, if the Customer Portal channel on the FAQ article-type is assigned to the Tab template, the sections in the FAQ's layout appear as tabs when customers view an FAQ article. For the Table of Contents template, the sections defined in the layout appear on a single page (with hyperlinks) when the article is viewed. 
Salesforce
 provides two standard article-type templates, Tab and Table of Contents. Custom templates can be created with 
Visualforce
.
Category Group for Articles
In 
Salesforce Knowledge
, a 
category group
 organizes data categories into a logical hierarchy. For example, to classify articles by sales regions and business units, create two category groups, Sales Regions and Business Units. The Sales Regions category group could consist of a geographical hierarchy, such as All Sales Regions as the top level, North America, Europe, and Asia at the second level, and so on up to five levels. When creating articles, authors assign the relevant categories to the article. End users searching for articles can search and filter by category.
Channel
A channel refers to the medium by which an article is available. 
Salesforce Knowledge
 offers four channels where you can make articles available.
Internal App: 
Salesforce
 users can access articles in the Articles tab depending on their role visibility.
Customer: Customers can access articles if the Articles tab is available in a 
community
 or 
Customer Portal
. Customer users inherit the role visibility of the manager on the account. In a 
community
, the article is only available to users with 
Customer Community
 or 
Customer Community Plus
 licenses.
Partner: Partners can access articles if the Articles tab is available in a 
community
 or 
partner portal
. Partner users inherit the role visibility of the manager on the account. In a 
community
, the article is only available to users with 
Partner Community
 licenses.
Public Knowledge Base: Articles can be made available to anonymous users by creating a public knowledge base using the 
Sample Public Knowledge Base for 
Salesforce Knowledge
 app from the 
AppExchange
. Creating a public knowledge base requires 
Sites
 and 
Visualforce
.
Your own website. Articles can be made available to users through your company website.
Data Category for Articles
In 
Salesforce Knowledge
, 
data categories
 are a set of criteria organized hierarchically into category groups. Articles in the knowledge base can be classified according to multiple categories that make it easy for users to find the articles they need. For example, to classify articles by sales regions and business units, create two category groups, Sales Regions and Business Units. The Sales Regions category group could consist of a geographical hierarchy, such as All Sales Regions as the top level, North America, Europe, and Asia at the second level, and so on up to five levels. Authors assign categories to articles. Administrators can use data categories to control access to articles.
Draft Article
Draft articles are in-progress articles that have not been published, which means they are not visible on the Articles tab (in any channel) or in a public knowledge base. Article managers can access draft articles on the Article Management tab by clicking the 
Articles
 tab in the View area and choosing 
Draft Articles
. 
You can filter draft articles by those assigned to you or those assign to anyone (all draft articles for your organization).
 Draft articles can be assigned to any user involved in the editorial work.
Draft Translation
Draft translations are in-progress translations of articles into multiple languages. They have not been published, which means they are not visible on the Articles tab (in any channel) or in a public knowledge base. Article managers can access draft translations on the Articles Management tab by clicking the 
Translations
 tab in the View area and choosing 
Draft Translations
. 
You can filter draft translations by those assigned to you, those assigned to a translation queue, or those assigned to anyone (all draft translations in your organization).
 Translations can be assigned to any user who can publish 
Salesforce Knowledge
 articles.
Knowledge Agent
Salesforce
 uses the term 
knowledge agent
 to represent a specific type of user. Knowledge agents are article consumers in the internal 
Salesforce Knowledge
 app. These users can access the Articles tab to search for and view articles, but they cannot create, edit, or manage articles.
Published Article
Published articles are available on the Articles tab in the internal app and, if applicable, in the 
Customer Portal
, 
partner portal
, and public knowledge base. To remove a published article, you can archive it or change its status to “draft” on the Article Management tab. To access published articles on the Articles Management tab, click the 
Articles
 tab in the View area and choose 
Published Articles
.
Published Translation
Published translations are articles translated into multiple languages that are available on the Articles tab in the internal app and, if applicable, in the 
Customer Portal
, 
partner portal
, and public knowledge base. To remove a published translation, you can archive it or change its status to “draft” on the Article Management tab. To access published translations on the Articles Management tab, click the 
Translations
 tab in the View area and choose 
Published Translations
.
See Also:
Salesforce Knowledge Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=knowledge_terms.htm&language=en_US
Release
200.8
