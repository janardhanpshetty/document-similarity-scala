### Topic: With the Spring '16 it will be possible to send out Email Alerts for Task, a long requested feature, this article will show you how to set this up.
Reminders for Activities, will only pop-up when you are logged into your browser. Not every user might be logged in every day, in which case you might want to set up email reminders to be send to a user for Tasks or Events
Lots of Salesforce users have voted for this Idea, and this will be available from Spring '16:
Enable email alert as workflow action for events and tasks
Prerequisites:
Create an Email Template to use:
Manage Email Templates
Resolution
Creating a Workflow to send out an Email Reminder
Go to Setup; Type Worklfow in the Quickfind box and select Workflow Rules
Click New Rule; Select Object; Task, click Next
Edit Rule: Set a Rule Name and Description
Evaluation Criteria: created, and any time it's edited to subsequently meet criteria
​
Rule Criteria: criteria are met:
Field: Task: Reminder; Operator: equals; Value: True
Click Save & Next
On the Next page, click Add Time Trigger
Now choose when to send the reminder:
1; Days; Before; Task: Reminder Date/Time
Click Save
​​​Now you can click on Add Workflow Action, within the Time Trigger we just created
Choose New Email Alert, supply a description
Choose the Email Template
As Recipient Type, search for Owner, move Assigned To to the Selected Recipients column
For From Email Address, choose either Current User or an Org-Wide address
Click Save, you resulting screen should be:
​
​Click Done
Verify your settings and Activate the Workflow
