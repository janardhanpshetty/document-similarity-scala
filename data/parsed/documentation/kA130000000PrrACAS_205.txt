### Topic: Guidelines for Keeping Your Organization’s Records Complete and Up-to-Date with Data.com Clean | Salesforce
Guidelines for Keeping Your Organization’s Records Complete and Up-to-Date with Data.com Clean | Salesforce
Guidelines for Keeping Your Organization’s Records Complete and Up-to-Date with 
Data.com Clean
Here are some tips and best practices for implementing, monitoring, and working with 
Data.com Clean
.
Available in: 
Salesforce Classic
Available with a 
Data.com Prospector
 license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available with a 
Data.com Clean
 license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To implement Data.com Clean:
“Customize Application”
To back up data:
“Data Export”
To set field history tracking:
“Customize Application”
To set field-level security:
“Customize Application”
Understand how matching works
.
Use your sandbox environment to try out the automated clean jobs before running them in your production org with live data:
After 
Data.com Clean
 is provisioned in your production org, refresh your sandbox so it has the same permissions and data. Then follow the steps in 
Implementing Data.com Clean
 to set up Clean in your sandbox. If everything runs smoothly in your sandbox environment, follow the same implementation steps in your production org.
Check your match and update rates on the Clean Metrics & Analytics page
. There, you’ll find information about the total number of records cleaned, how many have been matched, and how many have been updated. Figures represent all processed records (whether cleaned manually, from a list, or from automated Clean jobs) and reflect your rates since your organization implemented 
Data.com Clean
.
Schedule regular backups of your account, contact, and lead data. It’s always a good practice, and if your 
Salesforce
 records are ever matched inappropriately, you can revert to previous versions. Backups are most important if you clean records from lists or with automated Clean jobs, because those Clean methods allow you to auto-fill blank fields.
Set up field history tracking for accounts, contacts, and leads. Field history tracking helps you identify changes to field values, and tracks who made changes and when. If you use field history tracking, make sure you add the Account History, Contact History, and Lead History related lists to those objects’ respective page layouts.
Install and use Data.com Reports
.
Parent topic:
 
Set Up Data.com Clean
Previous topic:
 
Find Out How Many of Your Organization’s Records Need Better Data
See Also:
Data.com Clean
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_increasing_match_rates.htm&language=en_US
Release
200.5
