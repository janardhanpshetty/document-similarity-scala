### Topic: Add Values to a Collection Variable | Salesforce
Add Values to a Collection Variable | Salesforce
Add Values to a Collection Variable
After you create a collection variable, populate it with values to reference throughout your flow. You can’t use a Record Lookup or Fast Lookup element to populate a collection variable, but there are some workarounds.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
To use values from outside the flow, set the collection variable’s 
Input/Output Type
 to “Input” and then use URL parameters, Visualforce controllers, or subflow inputs. When the values are coming from outside the flow, the values can be set only at the start of the flow interview.
To add values that are stored in...
Do this...
For more information
A screen field
Add the field’s entered or stored value to a collection variable by using an Assignment element
Choice fields
Input fields
Output fields
Assignments
A variable
Add the variable’s stored value to a collection variable by using an Assignment element
Variables
Assignments
An sObject variable
Add one of the sObject variable’s stored field values to a collection variable by using an Assignment element
sObject variables
Assignments
An sObject collection variable
Loop through the sObject collection variable. Within the loop, add one of the loop variable’s stored field values to a collection variable by using an Assignment element
sObject collection variables
Loops
Assignments
See Also:
Flow Collection Variable Resource
Sample Flow That Populates a Collection Variable
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_designer_resources_coll_var_pop.htm&language=en_US
Release
202.14
