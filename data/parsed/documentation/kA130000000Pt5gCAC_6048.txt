### Topic: Flow Elements | Salesforce
Flow Elements | Salesforce
Flow Elements
Each 
element
 represents an action that the flow can execute. Examples of such actions include reading or writing Salesforce data, displaying information and collecting data from flow users, executing business logic, or manipulating data.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
In the Cloud Flow Designer, the canvas and Explorer tab display the elements that exist in the flow. The Palette tab displays the available element types that you can add to the flow by dragging them onto the canvas.
General Settings for Flow Elements
Every flow element has three settings in common: name, unique name, and description.
Flow Apex Plug-In Element
Calls an Apex class that implements the 
Process.Plugin
 interface. If you used the Tag property in the 
PluginDescribeResult
 class, the Apex class appears under a customized section. Otherwise, it appears under the Apex Plug-ins section.
Flow Assignment Element
Sets or changes values in variables, collection variables, sObject variables, and sObject collection variables.
Flow Call Apex Element
Calls an Apex class’s invocable method.
Flow Decision Element
Evaluates a set of conditions and routes users through the flow based on the outcomes of those conditions. This element performs the equivalent of an if-then statement.
Flow Email Alert Element
Sends an email by using a workflow email alert to specify the email template and recipients. The flow provides only the record ID.
Flow Fast Create Element
Creates Salesforce records using the field values from an sObject collection variable. Or creates one Salesforce record using the field values from an sObject variable.
Flow Fast Delete Element
Deletes Salesforce records using the ID values that are stored in an sObject collection variable. Or deletes one Salesforce record using the ID value that’s stored in an sObject variable.
Flow Fast Lookup Element
Finds Salesforce records to assign their field values to an sObject collection variable. Or finds one Salesforce record to assign its field values to an sObject variable.
Flow Fast Update Element
Updates Salesforce records using the field values from an sObject collection variable. Or updates one Salesforce record using the field values from an sObject variable. If a record’s ID is included in the variable, its field values are updated to match the other values that are stored in the variable.
Flow Loop Element
Iterates through a collection one item at a time, and executes actions on each item’s field values—using other elements within the loop.
Flow Record Create Element
Creates one Salesforce record by using individual field values that you specify.
Flow Record Delete Element
Deletes all Salesforce records that meet specified criteria.
Flow Record Lookup Element
Finds the first Salesforce record that meets specified criteria. Then assigns the record’s field values to individual flow variables or individual fields on sObject variables.
Flow Record Update Element
Finds all Salesforce records that meet specified criteria and updates them with individual field values that you specify.
Flow Quick Action Element
Calls an object-specific or global quick action that’s already been configured in your organization. Only “Create,” “Update,” and “Log a Call” actions are supported.
Flow Post to Chatter Element
Posts a message to a specified feed, such as to a Chatter group or a case record. The message can contain mentions and topics, but only text posts are supported.
Flow Screen Element
Displays a screen to the user who is running the flow, which lets you display information to the user or collect information from the user.
Flow Send Email Element
Sends an email by manually specifying the subject, body, and recipients in the flow.
Flow Step Element
Acts as a placeholder when you’re not sure which element you need.
Flow Submit for Approval Element
Submits one Salesforce record for approval.
Flow Subflow Element
Calls another flow in your organization. Use this element to reference modular flows and simplify the overall architecture of your flow.
Flow Wait Element
Waits for one or more defined events to occur, which lets you automate processes that require a waiting period.
See Also:
Flow Resources
Cloud Flow Designer
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_designer_elements.htm&language=en_US
Release
202.14
