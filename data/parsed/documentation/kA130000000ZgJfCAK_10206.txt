### Topic: Brand a Salesforce Console | Salesforce
Brand a Salesforce Console | Salesforce
Brand a Salesforce Console
You can brand a console by adding a custom logo to its header and specifying colors for its header, footer, and primary tabs.
Salesforce console available in Salesforce Classic and App Launcher in Lightning Experience. Setup for Salesforce console available in Salesforce Classic.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To define colors in the Salesforce console:
“Customize Application”
From Setup, enter 
Apps
 in the 
Quick Find
 box, then select 
Apps
.
Click 
Edit
 next to a console.
To add a custom logo, click 
Insert an image
, and choose an image from the document library.
Consider these requirements when choosing a custom app logo from the document library:
The image must be in GIF or JPEG format and less than 20 KB.
If the image is larger than 300 pixels wide by 55 pixels high, then it is scaled to fit.
For the best on-screen display, we recommend that you use an image with a transparent background.
The 
Externally Available
 checkbox must be selected on the document’s properties so that users can view the image.
To specify a color, type a hex code in 
Header Color
, 
Footer Color
, or 
Primary Tab Color
. For example, type #0000FF to define the header as blue.
Click 
Save
.
To verify that the correct colors display, view your console.
Note
Defining header or footer colors is not yet available for the Metadata API.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=console2_components_color.htm&language=en_US
Release
202.14
