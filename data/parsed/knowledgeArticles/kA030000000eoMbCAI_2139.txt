### Topic: This Accelerator is designed for customers looking to implement computer telephony integration (CTI) who are not familiar with the Salesforce CTI strategy and often don’t know where to start. The specialist will review Open CTI strategy, Implementation Best Practices as well as implementation partner selection as part of this accelerator. This is a 5 point engagement
Accelerator
Salesforce CTI Assessment
What problem are we solving for you? 
This accelerator targets those customers who want to understand CTI functionality better and would like to start planning their Salesforce CTI Strategy. The aim is to help them navigate their first steps for CTI Implementation and find a partner.
The specialist will familiarize you with the benefits of CTI integration and typically include:
     Most Popular Use Cases:
  In Context Screen Pop for any Standard or Custom object based on ANI/DNIS
  Click to Dial to Connect to Customers Immediately
  Agent SoftPhone Controls Provide Enhanced Rep Experience
  Enhanced Reporting through Call Logging in Salesforce
    Typical Benefits
  Improved Agent Productivity
  Improved Customer Experience
  Reduce Handling Time and Overall Costs
  Report Effectively on Agent Activity Driving Better Performance / Staffing etc.
 Accelerator Guiding Principles
    Salesforce Commitment
We will facilitate a discovery session to help you define your CTI strategy
Work with Contact Center management to detail use cases
Include additional LOB leaders in extended use case discussions
Gather pertinent details regarding telephony environment
Deliver Discovery Report including the following:
      Review of telephony architecture and key use cases
      List of up to 3 Partner options
      Open CTI Deployment Best Practices
    Your Commitment
      Accelerator Pre-requisites:
  Customer is looking to implement CTI, is not familiar with the Salesforce CTI strategy and needs appropriate guidance.
      Product Features / Enabled:
  Service Cloud licenses enabled in Org
      People / Resources Needed:
  A LOB stakeholder who is committed to implementing CTI
  An IT/Telephony stakeholder who is willing to participate in the discovery
  A Service management stakeholder to represent current use cases
  
Engagement Process and Timing
Key Milestones & Approximate Duration 
Milestone
Duration (Min)
Estimated Timeframe
1.) Accelerator Review
30
Start Date
2.) Discovery Session Preparation
60
Start + 5
3.) Discovery Session with Contact Center Exec & Telephony Team
Discuss and review current telephony architecture
Determine use cases where CTI will improve productivity
Discuss deployment best practices
120
Start +7
4.) Review Options and Present CTI Partner Options
60
Start +12
5.) Review Partner Selection (as needed)
30
Start +15
6.) Final CheckPoint (Survey / Email)
15
Start +15
 
Resolution
