### Topic: This article walks through the steps to follow to identify the Users who are not assigned to a Permission Set.
There are two types of Permission Sets:
1)
 Permission Set that is Owned by User (
User created
).
2) 
Permission Set that is owned by Profile (
System Generated
). 
Permission Set Owned by the User:
 This Permission Set is created by the User. Read more: 
Permission Set Overview Page
.
Permission Set owned by the Profile
:
 Every profile is associated with a permission set that stores the profile’s user, object, and field permissions, as well as setup entity access settings. Permission sets that are owned by profiles can be queried but not modified. 
By default, every user will be assigned to a Permission Set that is owned by the Profile and this Permission Set will not be visible in the User Interface. If you query the 
Permission Set Assignment 
object through Dataloader/Workbench, you will retrieve the list of Users who are not assigned to any Permission Set (
User created
).
Resolution
To identify the Users who are not assigned with Permission Set, please follow the below steps:
1) Login in to Dataloader and select the "
Export
" option.
2) Check the checkbox "
Show all Salesforce objects
" and select the "
Permission Set Assignment
" object.
3) Run the below query to get the list of Users assigned to a Permission set
:
Select AssigneeId,PermissionSet.isOwnedByProfile FROM PermissionSetAssignment Where PermissionSet.isOwnedByProfile=False
4) Remove the duplicate records from the file generated in the above step in excel by selecting "
Remove Duplicates
" option under "
Data
".
5) Create a Report in Salesforce based on the 
User 
object to display the User Id and export it.
6) Perform a Vlookup between the files generated in Step 5 with Step 4 to get the list of Users who are not assigned to a Permission Set .
See Also : 
Permission Set Assignment
