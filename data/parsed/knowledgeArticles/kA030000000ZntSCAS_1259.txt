### Topic: Unable to view Outbound Message Notifications in Setup or Unable to find the section "Failed Outbound Message" in Monitor Outbound Messages
Unable to view Outbound Message Notifications in Setup
Resolution
If you are unable to view "Outbound Message Notifications" in Setup, then the BT perm "Dead Letter Queue for Outbound Messaging" needs to be enabled.
Once this is enabled you will be able to view "Outbound Message Notification in Setup"
It is also seen that once this perm is enabled you will be able to view the "Failed outbound messages" section under Setup -> Monitor -> Outbound Messages
