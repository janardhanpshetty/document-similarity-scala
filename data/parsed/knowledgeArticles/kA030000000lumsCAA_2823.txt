### Topic: Learn what day are spam complaints registered in the Spam Complaints Over Time report in Marketing Cloud.
Resolution
Spam complaints are registered on the day of a Send. Complaints are bucketed and displayed in the report on the day of the Send to ensure that the ratios are accurate. 
 
For example, if you have 1000 Sends on Monday and no complaints registered, then on Tuesday you receive 10 spam complaints but no sends, the 10 spam complaints are registered on Monday, the day of the send for a 0.1% complaint rate. 
 
This provides the most accurate ratio. If the spam complaints were registered on the day they are received, you would have a perfect day on Monday with no complaints received, and a 100% complaint rate on Tuesday with no complaints, which is not an accurate ratio.
