### Topic: This article will go through the step by step instructions to restore Post on to your Page Layout that is assigned to the Page Layout where the issue exist.
the name of the record type and it will post
Other record 
type do
 not have this problem.
 
Options are:
1. type does
2. types do
 
Thank You.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
Edited November 3, 2015 at 11:03 AM
  
Attach File
 
Click to comment
 
 
Samil Gutierrez
 to salesforce.com Only
#KBFeedback
 
@Daniel Decook
 The Reference link is not working in this article click on the Help Site URL| click on the Customize Chatter Group Layouts and Publisher Actions | you will get an error "Oops, the page you're trying to view isn't here"
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
July 24, 2015 at 8:59 AM
  
Daniel Decook
Hello 
@Samil Gutierrez
,
Thanks for the feedback. I updated the URL to point to the article. The link works for me let me know if you have any issues. Thanks.
Show More
Like
Unlike
 
  ·  
 
July 24, 2015 at 12:05 PM 
Attach File
 
Click to comment
 
 
Himanshu Jauhari
 to salesforce.com Only
#KBFeedback
 
@Deepkant Verma
 In description we can add that the same procedure is required for creating new event and new task for particular record type.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
October 21, 2014 at 4:32 PM
  
Ananth Sivakumar
I think it might confuse our reader. For now let the article remain as it is. Thanks for the feedback.
Show More
Like
Unlike
 
  ·  
1 person
 
  ·  
 
October 26, 2014 at 7:20 PM 
Himanshu Jauhari
ok 
@Ananth Sivakumar
 Thanks for the suggestion.
Show More
Like
Unlike
 
  ·  
 
October 27, 2014 at 7:47 AM 
Attach File
 
Click to comment
 
 
Himanshu Jauhari
 to salesforce.com Only
#KBFeedback
 I beleive we can update the skill group from CRM Usage to CRM Configuration as "Page Layouts / Record Types" issue is being handled by CRM Configuration
@Deepkant Verma
 
@Ankur Dullar
FYI 
@Renu Bahaduri
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
October 8, 2014 at 8:48 AM
  
Ananth Sivakumar
Publisher action being chatter related is a grey area. This article can stay with Usage for the time being.
Show More
Like
Unlike
 
  ·  
 
October 12, 2014 at 7:39 PM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Usage
With a particular record type, I do not have the option to 
Post 
a comment. I can change the name of the record type and it will post.
Other record type does not have this problem.
Resolution
This is because your Organization may not be using global publisher layout.
Reference:
Customize Chatter Group Layouts and Publisher Actions
Got the page layout of the affected record type in this example it will be an Opportunity
Setup | Customize | 
Ex:
 Opportunities | Page Layouts
   (Standard Objects)
Setup | Create | Objects | Custom Object in question | Page Layouts Related List    
(Custom Objects)
This example below is for a record type named B2C Opportunity Layout
From the left pane you will find need to highlight Actions, then select 
Post
, and drag it into the Publisher Actions pane and save.
Now verify that you able to post using your record type.
