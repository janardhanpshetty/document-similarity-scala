### Topic: Macros | Salesforce
Macros | Salesforce
Macros
Support agents who use Case Feed now can run macros to automatically complete repetitive tasks—such as selecting an email template, sending an email to a customer, and updating the case status—all in a single click. Macros save time and add consistency to support agents’ work.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
You can create macros to perform multiple actions on the Case Feed. For example, a macro can enter the subject line of an email and update the case status. A single macro can perform multiple actions on different parts of the Case Feed at the same time.
Note
Macros are supported on feed-based standard and custom objects.
Getting Started with Macros
Before you can create and run macros, set up the Salesforce Console for Service environment.
Tips for Creating Macros
How you name and design your macro can impact its usefulness to support agents. Keep these tips in mind when creating macros.
Create Macros
You can create a macro by specifying the instructions for actions that the macro performs. A macro is like a little computer program. You need to tell the macro each step that it performs. This example shows how to create a simple macro.
Run a Macro in the Salesforce Console for Service
Macros automate a series of repetitive keystrokes that support agents make in the Salesforce Console for Service. You can quickly complete repetitive tasks, such as updating the case status, by running a macro.
Shortcuts for Macros
You can use the keyboard shortcuts to work even more efficiently with macros.
See Also:
Feed-Based Layouts Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=macros_def.htm&language=en_US
Release
202.14
