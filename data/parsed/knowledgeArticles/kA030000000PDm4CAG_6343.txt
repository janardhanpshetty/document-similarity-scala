### Topic: In order to add a product to a pricebook in Salesforce through the API, there is a specific sequence that must be followed.
How do I define the standard or unit price for a new product via the API?
Resolution
In order to add a product to a pricebook in Salesforce through the API, there is a specific sequence that must be followed.
In this process, you will need to assign a standard price and unit price to the PricebookEntry.
The steps below outline the sequence:
1. Create your Product2 item and save the id.
2. Obtain the id for your standard pricebook. Select Id from Pricebook2 where IsStandard = true.
3. Create a new PricebookEntry object using the Product2Id from step 1 and the Pricebook2Id from step 2.
*IMPORTANT - you MUST set the UseStandardPrice field = false. Otherwise, this will cause a Field Integrity Error. Be sure to set the unit price here. This will be your "standard price".
4. Obtain the id for the custom pricebook that you actually want to include the new product in. Select Id from Pricebook2 where name = 'my pricebook'.
5. Create the new PricebookEntry object using the Product2Id from step 1 and the Pricebook2Id from step 4. You can set the unit price here to something other than the price you specified in the standard pricebook, or you can set the UseStandardPrice field to true to use the one from the standard pricebook.
The only way to set the standard price is to create a pricebookentry in the standard price book. There is no way to explicitly create a "standard price" as the standard price is the unit price of the pricebookentry in the standard price book for a particular product.
