### Topic: App Builder Tools Overview | Salesforce
App Builder Tools Overview | Salesforce
App Builder Tools Overview
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
The platform includes innovative point-and-click app builder tools that give you the power to customize Salesforce to meet the needs of your business. You can also build your own applications to share and store information that is important to you. These tools do not require any programming knowledge. From Setup, enter 
Create
 in the 
Quick Find
 box, then select 
Create
 to get started.
The platform also includes app builder tools that require some programming knowledge. Tools that require advanced programming knowledge are accessed in Setup by entering 
Develop
 in the 
Quick Find
 box, then selecting 
Develop
.
Help Site URL
Release
202.14
