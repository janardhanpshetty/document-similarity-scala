### Topic: The URLReWriter class for Force.com sites can also be leveraged on the Knowledge base.
The URLReWriter class detailed here - 
http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_site_urlRewriter.htm
 - for Force.com sites can also be leveraged on the Knowledge base. 
Does UrlReWriter work for non-public knowledge base channel enabled articles? 
No, any knowledge articles wishing to make use of the UrlReWriter class logic will require the "Public Knowledge Base" channel to be enabled for the article otherwise the Id will be used instead of the URL-Name to override the article URL.
Resolution
