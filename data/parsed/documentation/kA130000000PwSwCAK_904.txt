### Topic: Create Opportunity Splits | Salesforce
Create Opportunity Splits | Salesforce
Create Opportunity Splits
Add and adjust splits to share revenue from opportunities with your team members.
Available in: Salesforce Classic
Available in: 
Performance
 and 
Developer
 Editions
Available in: 
Enterprise
 and 
Unlimited
 Editions with the Sales Cloud
User Permissions Needed
To add splits for opportunity team members:
“Edit” on opportunities
AND
Owner of opportunity record or above owner in organization hierarchy
In the Opportunity Splits related list of an opportunity, click 
Edit Opportunity Splits
.
Complete the fields as needed.
To remove a team member from the split, click 
 or 
Del
 next to the team member’s name.
Save your changes.
See Also:
Considerations for Creating Opportunity Splits
Opportunity Splits
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=teamselling_opp_splits_edit.htm&language=en_US
Release
202.14
