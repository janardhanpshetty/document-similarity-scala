### Topic: Only see tab in SFDC mobile app once activation completed
Once Salesforce Classic is successfully activated, I log in and I only see the Home tab. All the other tabs are missing.
Resolution
Please check with your salesforce system administrator to make sure that they have actually built out a dataset within the mobile configuration. Typically, this issue is caused when an admin has not added ANY object data to the mobile configuration.
