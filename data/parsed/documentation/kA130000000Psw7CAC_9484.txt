### Topic: Create, Clone, or Refresh a Sandbox | Salesforce
Create, Clone, or Refresh a Sandbox | Salesforce
Create, Clone, or Refresh a Sandbox
Create a sandbox to use for development, testing, and training. Clone a sandbox to copy its data and metadata into another sandbox. Refresh an existing sandbox to update its contents.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Database.com
 Editions
User Permissions Needed
To view a sandbox:
“View Setup and Configuration”
To create, refresh, activate, and delete a sandbox:
“Modify All Data”
You have a few ways to copy metadata and data to a sandbox.
Create a sandbox.
When you create a sandbox, Salesforce copies the metadata from your production org to a sandbox org. While creating a Partial Copy or Full sandbox, you can apply a sandbox template, if you have created one. Customers create sandbox templates to define specific object data to copy into the Partial Copy or Full sandbox.
Clone an existing sandbox to copy all its data and metadata to a new sandbox.
Refresh and activate an existing sandbox.
Refreshing a sandbox updates the sandbox’s metadata from its source org. If the sandbox is a clone or if it uses a sandbox template, the refresh process updates the org’s data in addition to its metadata.
Create a Sandbox
From Setup, enter 
Sandboxes
 in the 
Quick Find
 box, then select 
Sandboxes
.
Click 
New Sandbox
.
Enter a name (10 characters or fewer) and description for the sandbox.
Tip
We recommend that you choose a name that:
Reflects the purpose of this sandbox, such as QA.
Has only a few characters, because Salesforce appends the sandbox name to usernames on user records in the sandbox environment. Names with fewer characters make sandbox logins easier to type.
Select the type of sandbox you want.
Note
If you don’t see a sandbox option or need licenses for more, contact Salesforce to order sandboxes for your org.
If you reduce the number of sandboxes you purchase, you are required to match the number of your sandboxes to the number you purchased. For example, if you have two Full sandboxes but purchased only one, you can’t create a Full sandbox. Instead, convert a Full sandbox to a smaller one, such as a Developer Pro or Developer sandbox, depending on which types you have available.
Select the data to include in your Partial Copy or Full sandbox.
For a Partial Copy sandbox, click 
Next
, and then select the template you created to specify the data for your sandbox. If you have not created a template for this Partial Copy sandbox, see 
Create or Edit Sandbox Templates
.
For a Full sandbox click 
Next
, and then decide how much data to include.
To include template-based data for a Full sandbox, select an existing sandbox template. For more information, see 
Create or Edit Sandbox Templates
To include all data in a Full sandbox, choose whether and how much field tracking history data to include, and whether to copy Chatter data. You can copy from 0 to 180 days of history, in 30-day increments. The default value is 0 days. Chatter data includes feeds, messages, and discovery topics. Decreasing the amount of data you copy can significantly speed sandbox copy time.
To run scripts after each create and refresh for this sandbox, specify the Apex class you previously created from the SandboxPostCopy interface.
Click 
Create
.
Tip
Try to limit changes in your production org while the sandbox copy proceeds.
The process takes from several minutes to several days, depending on the size and type of your org.
When your sandbox is ready for use, you receive a notification email that your sandbox has completed copying.
To access your sandbox, click the link in the notification email.
Users can log in to the sandbox at 
https://test.salesforce.com
 by appending 
.
sandbox_name
 to their Salesforce usernames. For example, if a username for a production org is 
user1@acme.com
, and the sandbox is named “test,” the modified username to log in to the sandbox is 
user1@acme.com.test
.
Note
Salesforce automatically changes sandbox usernames, but not passwords.
New sandboxes have the default email deliverability setting 
System email only
. The 
System email only
 setting is especially useful for controlling email sent from sandboxes so that testing and development work doesn’t send test emails to your users.
Clone a Sandbox (Pilot)
Note
We provide Sandbox-to-Sandbox Cloning to selected customers through a pilot program that requires agreement to specific terms and conditions. To be nominated to participate in the program, contact Salesforce. Pilot programs are subject to change, and we can’t guarantee acceptance. Sandbox-to-Sandbox Cloning isn’t generally available unless or until Salesforce announces its general availability in documentation or in press releases or public statements. We can’t guarantee general availability within any particular time frame or at all. Make your purchase decisions only on the basis of generally available products and features. You can provide feedback and suggestions for Sandbox-to-Sandbox Cloning 
in the IdeaExchange
.
When you clone a sandbox, all its data and metadata are copied into the new sandbox.
From Setup, enter 
Sandboxes
 in the 
Quick Find
 box, then select 
Sandboxes
.
Click 
New Sandbox
.
Enter a name (10 characters or fewer) and description for the sandbox.
Tip
We recommend that you choose a name that:
Reflects the purpose of this sandbox, such as QA.
Has only a few characters, because Salesforce appends the sandbox name to usernames on user records in the sandbox environment. Names with fewer characters make sandbox logins easier to type.
From the Create From drop-down menu, select the name of the sandbox that you want to clone.
To run scripts after each create and refresh for this sandbox, specify the Apex class you previously created from the SandboxPostCopy interface.
Click 
Create
.
Tip
Try to limit changes in your source org while the sandbox copy proceeds.
When your new sandbox is ready, you can manage it from your production org like you would with any other sandbox.
Refresh a Sandbox
From Setup, enter 
Sandboxes
 in the 
Quick Find
 box, then select 
Sandboxes
.
A list of your sandboxes displays. Sandboxes that you can refresh have a Refresh link next to their name.
Next to the name, click 
Refresh
.
Review the Name, Description, and Create From values, and edit these values if needed.
Select the type of sandbox environment you want.
Note
A table shows the number and type of sandbox licenses available in your org. You can select a different sandbox type to refresh.
If the sandbox you’re refreshing is a clone, this option isn’t available. A cloned sandbox refreshes from its source org and retains the source org’s sandbox license type. If a sandbox’s source org has been deleted, the clone refreshes from your production org.
You can’t refresh or delete a sandbox while it’s being cloned.
Select the data you want to copy.
For a Partial Copy sandbox, click 
Next
, and then select a template to specify the data for your sandbox. If you have not created a template for this Partial Copy sandbox, see 
Create or Edit Sandbox Templates
.
For a Full sandbox, click 
Next
, and then decide how much object data to include.
To include template-based data in a Full sandbox, select an existing sandbox template. For more information, see 
Create or Edit Sandbox Templates
.
To include all object data in a Full sandbox, choose whether and how much field tracking history to include, and whether to copy Chatter data. You can copy from 0 to 180 days of history, in 30-day increments. The default value is 0 days. Chatter data includes feeds, messages, and discovery topics. Decreasing the amount of data you copy can speed sandbox copy time.
Optionally, select 
Auto Activate
. If you select 
Auto Activate
, your sandbox is activated immediately after you refresh it, and you do not receive an activation email.
Click 
Create
.
Salesforce starts copying data to the sandbox.
When the copy is complete, activate the sandbox to use the refreshed data. If you didn’t select 
Auto Activate
 while refreshing your sandbox, Salesforce sends you an email when your sandbox is ready to activate.
If you didn’t select 
Auto Activate
 while refreshing your sandbox, activate your refreshed sandbox:
From Setup, enter 
Sandboxes
 in the 
Quick Find
 box, then select 
Sandboxes
.
A list of your sandboxes displays. Refreshed sandboxes to activate have an 
Activate
 link next to their name.
Click the link next to the sandbox you want to activate.
Warning
Activating a replacement sandbox that was created using the Refresh link deletes the sandbox it is refreshing. The current configuration and data are erased, including application or data changes that you’ve made. Click the 
Activate
 link only if you don’t need the current contents of the sandbox. Your production org and its data aren’t affected. New sandboxes that aren’t activated within 30 days are deleted. We send at least two email notifications before we schedule the sandbox for deletion. Users who have created or most recently refreshed any sandbox for your org receive the notifications.
Monitor Your Sandbox’s Progress
From Setup, enter 
Sandboxes
 in the 
Quick Find
 box, then select 
Sandboxes
. The list of your sandboxes displays a progress bar for items in the queue, in progress, or recently completed.
To show the percentage completed of a copy in progress, hover over the progress bar.
To see information about the sandbox, including copy progress or how much time before the next available refresh, click the name.
If your sandbox status is suspended or stopped for more than 1 hour, contact Salesforce customer support.
See Also:
Sandboxes
Sandbox Licenses and Storage by Type
Create or Edit Sandbox Templates
Sandbox Setup Considerations
Sandbox License Expiration
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_sandbox_create.htm&language=en_US
Release
202.14
