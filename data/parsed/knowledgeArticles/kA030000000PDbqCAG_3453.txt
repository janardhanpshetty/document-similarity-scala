### Topic: A user will have a list of unread leads that they own and can be viewed under the list view of My Unread Leads.
What is meant by the Read / Unread status of leads?
Resolution
A user will have a list of unread leads that they own and can be viewed under the list view of My Unread Leads.
A lead will only be considered "Read" when the owner of that lead views / edits that lead in the UI. It will remain "Unread" even if someone other than the owner views or edits the lead record or some mass action is taken outside of the UI (i.e. a mass email to many leads sent via the API). Once it has been "read", it will come off the list view My Unread Leads. The only type of mass action that can affect it would be to update the specific 'Unread by Owner' field using the API (e.g. using the Excel Connector or the Dataloader). Specifically if the owner of a Lead edits it through a list view using the "Change Status" button or "Inline Editing" the Unread checkbox will NOT be updated.
When you transfer ownership of a lead, the lead will appear in the new owner's 'My Unread Leads' view, until they click on the lead.
Note:
 For leads owned by a Queue, the status will remain 'Unread', even if a member of the Queue reviews it.
