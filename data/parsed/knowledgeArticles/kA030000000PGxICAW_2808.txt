### Topic: Setting up a forwarding filter from Gmail to Salesforce using the Email-to-Case feature.
Customer wants to set up a forwarding filter from their 
Gmail 
account to Salesforce for the
 Email to Case
 feature. 
Resolution
Ensure that the internal Routing Address (ex: Returns@acme.com) is valid and accepting inbound emails.
Follow the steps in 
Setting a forwarding filter from Gmail
Gmail 
allows you to automatically forward incoming email to another address. So you can use the long On-Demand Email to Case Routing address as your forwarding address in Gmail
Here's how to forward messages automatically:
1. Before you setup forwarding you need to configure your On-Demand Email to Case and create routing addresses. Please see the 
Configuring Routing Addresses for Email-to-Case and On-Demand Email-to-Case
 article to do this. Once you create your routing address and save to generate the Email Services Address (Auto generated long email address), copy it and then follow the steps below
2. Open up a browser and login to your Gmail
3. Click the gear icon 
 in the upper right, and then select 
Settings
.
4. Click the 
Forwarding and POP/IMAP
 tab.
5. On that page, in the 
Forwarding
 section, select 
Add a forwarding address
You can also set the filters from this section :            
Tip: You can also forward only some of your mail by
 
creating a filter!
6. On the 
Add a forwarding address
 pop up box paste the long Email Services Address that you copied above and click on 
Next
7. Next click on 
Proceed 
on the 
Confirm forwarding address
 pop up box
8. Next you will see a message on the pop up box saying that 
A confirmation code has been sent to verify permission
9. Click Ok on that and then leave the Gmail screen and open up another tab and login to your Salesforce org and click on the 
Case 
tab.
10. You should see a new case created with a 
Subject 
similar to 
(#667745483) Gmail Forwarding Confirmation - Receive Mail from <youremail>@gmail.com
(it may take a few minutes for the case to get created depending on your connection)
11. Open the case and you should see the 
Confirmation code 
in the Description field. Copy the code and go back to your Gmail
12. In Gmail, you should be on your 
Forwarding and POP/IMAP
 tab. Paste the confirmation code in the box above the 
Verify 
button and click on 
Verify
13. Select the "
Forward a copy of incoming mail to  <forwarding email > and Keep Gmail's copy in the inbox
 Radio button
14. Under 
IMAP Access
, make sure 
Enable IMAP 
is selected and click on 
Save Changes
15. You have finished setting up your forwarding, now test it by sending an email to your Gmail and a case should be created
For the more up to date information on how to forward emails from Gmail please refer to the Gmail documentation:
https://support.google.com/mail/answer/10957?hl=en
