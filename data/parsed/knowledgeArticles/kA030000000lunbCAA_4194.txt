### Topic: This article provides a workaround to mark the actual completion date of a task. Due on the task remains as is irrespective of earlier completion or later completion of the task
Currently the system doesn't log when the task is completed and there is no change to the due date of the Task when the status is changed to 
completed
? How to mark a task with the actual day it is Completed?
 
Thank You.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
November 3, 2015 at 11:31 AM
  
Attach File
 
Click to comment
 
 
Barry Greenlees
 to salesforce.com Only
#KBFeedback
 
@Daniel Decook
@Derek Wiers
 The spelling should be Naming not
Nameing the field (Completed Date), click Next
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
July 3, 2015 at 6:06 AM
  
Daniel Decook
Thanks for the feedback. Article is updated.
Show More
Like
Unlike
 
  ·  
 
July 6, 2015 at 12:12 PM 
Attach File
 
Click to comment
 
 
Harihara Suthan
 to salesforce.com Only
#kbkudos
 
@Barry Greenlees
 - This is now published. Well drafted article with clear instruction. Thanks for taking time and sharing your knowledge
FYI 
@Derek Wiers
 
@Daniel Decook
Show More
Topics:
   
Kbkudos
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
July 1, 2015 at 12:28 PM
  
 
Barry Greenlees
 likes this.
Attach File
 
Click to comment
 
 
Harihara Suthan
 to salesforce.com Only
@Barry Greenlees
 - This is a good use case article but needs little bit of rework.
@Derek Wiers
 - Please help Barrry with the same so that instead of long sentences we can make it as bullet points (step by step instruction)
Show More
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Add Topics
June 25, 2015 at 9:15 AM
  
 
Derek Wiers
 likes this.
Barry Greenlees
@Derek Wiers
@Harihara Suthan
How does this format work for the Article?
Show More
Like
Unlike
 
  ·  
 
June 25, 2015 at 10:11 AM 
Harihara Suthan
Awesome 
@Barry Greenlees
 - Just one change, instead of bullet points it can be numbered sequence. I will do it myself and send it up for approval. Thanks
Show More
Like
Unlike
 
  ·  
1 person
 
  ·  
 
June 25, 2015 at 11:00 AM 
Harihara Suthan
Sent for approval 
@Barry Greenlees
Show More
Like
Unlike
 
  ·  
1 person
 
  ·  
 
June 25, 2015 at 4:00 PM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Configuration
Currently the system doesn't log when the task is complete and there is no change to the due date of the Task when the status is changed to completed? How to mark a task with the actual day it is Completed?
Resolution
The work around for this would be to create a custom field date field on tasks and then create a workflow with a field update to time stamp the date the task status is changed, this would be done by going to:
Setup> Customize> Activities> Activity Custom Fields
Click New
Select the Date field
Clicked next
Naming the field (Completed Date), click Next
Selected the field as read only and save.   
To create the workflow you would go to:
Setup> Create> Workflow & Approval
Clicked Workflow Rules
Click the New Rule button
Select the Task Object, click next
Name the workflow rule
Set to evaluate when the record is created and every time it is edited to subsequently met the criteria
Set the criteria to:                       
                                      Field: Status 
                                      Operator: Equals 
​                                      Value: Completed 
Then click Save and Next
In immediate actions
Click the drop down Add Workflow Action and select Add Field update
Name the field update
Click the field drop down and select the custom field (Completed Date)
Select to update with a formula and then added the formula TODAY(),
Click Save, and click Done.
Once the Workflow is Activated the Tasks will be time stamped when there status is Completed.
