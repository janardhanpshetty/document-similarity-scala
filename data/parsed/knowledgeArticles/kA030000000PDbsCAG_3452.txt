### Topic: This error will appear as there is a time-based workflow action pending that is related to the lead. This can be tracked down by going to Setup | Monitoring | Time-Based Workflow. Then filter on criteria related to the lead record that caused the error to see which workflow is implicated.
Why do I receive the following error when converting a Lead: "Unable to convert lead that is in use by workflow"?
Resolution
This error will appear if either of the following conditions are met:
1. There is a time-based workflow action pending that is related to the Lead. 
This can be tracked down by going to Setup | Monitoring | Time-Based Workflow. Then filter on criteria related to the Lead record that caused the error to see which workflow is implicated.
2. The Lead that you are trying to convert is currently in an Approval Process
This can be tracked down by adding the "Approval History" related list to the page layout of the Lead record that you are trying to convert, and confirming if there is a "pending" entry in the "Approval History" related list.  
In case of "pending" entry, recall the approval leads from the approval process, else do the conversion post the approval process is completed.
3. T
his error can also be seen post conversion, if a workflow gets triggered and one of its field update action changes a field that is referenced in a validation rule.
