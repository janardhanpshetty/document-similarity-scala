### Topic: Searching for Tables | Salesforce
Searching for Tables | Salesforce
Searching for Tables
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To search tables:
“View” on tables
Note
Tables are currently available through a pilot program. For information on enabling tables for your organization, contact Salesforce.
You can search for tables by table name or description.
Click the Tables tab.
If the Tables tab isn’t visible, you can customize your display to show it.
To restrict your search to a certain group of tables, click a filter on the sidebar.
Note
If Recently Viewed is selected, the search returns results from all tables.
Enter a word from a table name or description, or the first few characters of a word followed by an asterisk (*). 
For example, searching for 
reg*
 returns tables named 
Travel Regulations
 and 
Registered Guests
, and a table with the description 
Applicants who have registered
.
Click 
Search
 to see all relevant tables based on your search terms. Or click 
 to clear your search terms.
Help Site URL
Release
202.14
