### Topic: Create Approval Processes for Email Drafts | Salesforce
Create Approval Processes for Email Drafts | Salesforce
Create Approval Processes for Email Drafts
Approval processes determine how your organization handles draft email messages—specifying, for example, which messages require approval and whether approvers are automatically assigned. Create customized approval processes based on your company’s needs.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create approval processes:
“Customize Application”
Enable draft emails
.
Though you can create approval processes for email messages without this step, those processes won’t be triggered until your organization has email drafts available.
Create a send action
.
Send actions ensure that email messages are sent once they’ve been approved.
Create approval processes.
Be sure to choose Email Message from the Manage Approval Processes For: drop-down list.
To give certain users, such as senior support agents, the ability to choose whether to submit an email message for approval or simply send the message, assign them to a profile that has the 
Bypass Email Approval
 permission selected.
See Also:
Create an Approval Process with the Standard Wizard
Enable Default Email Templates in Case Feed
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=case_interaction_approval_processes.htm&language=en_US
Release
202.14
