### Topic: With the Winter ‘16 release*, users will be warned when a website, application, or other source uses a link or script to force a user to login to Salesforce. When Salesforce detects this activity, we will prompt the user to verify the account and login attempt.
What is 
Enable Security Forced Login Interception
? 
With this feature, we are adding measures to prevent scripts, sites and other sources from automatically logging in users without the user’s explicit authorization. 
Sometimes an application sends a username and password in a URL directly to the Salesforce login page without prompting the user to type anything for authentication. For example, users bookmark the Salesforce login page with their username and password and then click the bookmark link. When Salesforce detects this activity, we prompt the user to verify the account and login attempt.
Users can select or deselect 
Don't ask again on this device
. When this option is selected and the user clicks Continue, Salesforce remembers the preference for the account and browser combination. Salesforce doesn’t prompt users when logging in through a web form and using a standard login page, such as at https://login.salesforce.com, or a login page for custom Salesforce domains, portals, or communities. 
Single sign-on
 users also don’t receive this message.
What kind of integrations will be affected by this feature?
This feature 
only
 affects web crawlers or web integrations that are using 
'screen scraping'
 instead of the normal REST/SOAP APIs
Why are we making this change?
We are committed to providing customers with the best experience and control over their implementations. This security improvement warns users of being forcefully logged in to Salesforce orgs without explicit authorization. It provides a more trusted login experience.
What action do I need to take? 
If your organization requires the ability to use forced logins without this additional security, please contact 
Customer Support
 via the Help & Training portal to have this permission disabled.
Where can I find more information? 
For more information on the improved security for unwanted login attempts, please see the 
Winter ‘16 Release Notes
. For additional questions, you can open a case with Customer Support via the Help & Training portal.  
Resolution
