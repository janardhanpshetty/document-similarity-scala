### Topic: Knowledge Articles and Work Orders | Salesforce
Knowledge Articles and Work Orders | Salesforce
Knowledge Articles and Work Orders
You can attach Knowledge articles to work orders and work order line items to help field technicians access important procedural info, guidelines, specs, and more. Learn how to make the most of this feature.
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions with the Service Cloud
User Permissions Needed
To attach or detach an article on a work order or work order line item:
“Read” on work orders AND “Read” on the article type AND Knowledge enabled
To edit page layouts:
“Customize Application”
To edit console layouts:
“Customize Application” AND “Service Cloud User”
Attaching an article
You can attach an article to a work order or work order line item in two ways:
Navigate to the record to which you want to attach the article. In the Knowledge One widget in the console, search for an article. In the article’s action menu, click 
Attach to Work Order
 or 
Attach to Work Order Line Item
.
Navigate to the record to which you want to attach the article. In the Articles related list, click 
Find Articles
. Use the search to locate your article, then click 
Attach to Work Order
 or 
Attach to Work Order Line Item
 in the article’s action menu.
Viewing an attached article
Articles attached to a work order or work order line item appear in the Knowledge One widget and the Articles related list on the record. View an article by clicking its title. You can also navigate to attached articles from the feed of a work order or work order line item if feed tracking for related lists is enabled.
On article detail pages, the Linked Work Orders and Linked Work Order Line Items related lists show which records an article is attached to.
Detaching an article
You can detach an article from a work order or work order line item in two ways:
Navigate to the record that the article is attached to. In the Knowledge One widget in the console, click 
Detach from Work Order
 or 
Detach from Work Order Line Item
 in the article’s action menu.
Navigate to the record that the article is attached to. In the Articles related list, click 
Detach
 next to the article.
Updating an attached article
If an article is out of date, you can publish a new version by navigating to the article and clicking 
Edit
.
When you attach an article to a record, that version of the article stays associated with the record even if later versions are published. If needed, you can detach and reattach an article to a record to ensure that the record is linked to the latest version of the article. The 
Linked Article Version
 field on the linked article detail page leads to the attached version.
Managing linked articles
Customize linked articles’ page layouts, fields, validation rules, and more from the Linked Articles node in Setup under Knowledge.
To learn how to configure your console and page layouts so articles can be attached to work orders and work order line items, see 
Set Up Work Orders
.
(1) 
View and change the articles attached to a record from the Articles related list.
(2) 
View an article’s properties by clicking 
View
, or view the article itself by clicking its title. Click 
Detach
 to remove the article from the record.
(3) 
The Knowledge One widget in the console sidebar lets you manage attached articles and search the Knowledge base.
(4) 
Each article’s action menu contains the option to attach or detach it.
Warning
The Article widget and Feed Articles Tool aren’t available in the feed view.
Quick actions and global actions aren’t supported for linked articles.
The Knowledge One widget on work orders and work order line items lets you search for articles, but doesn’t offer article suggestions.
In the Article Toolbar on the Knowledge home page, you can’t attach an article to a work order or work order line item.
The Linked Work Orders and Linked Work Order Line Items related lists on articles aren’t available in Lightning Experience or Salesforce1.
Linked articles are view-only in Lightning Experience and Salesforce1.
In Lightning Experience, clicking an article link in a feed item redirects you to the article page in Salesforce Classic. In Salesforce1, linked articles can’t be accessed from feed items.
See Also:
Set Up Work Orders
Linked Article Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=wo_knowledge.htm&language=en_US
Release
202.14
