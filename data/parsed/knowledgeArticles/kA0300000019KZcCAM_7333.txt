### Topic: Process for increasing the maximum number of entities that a user can subscribe to that rule on number of items you can follow in chatter
Available in:  Group, Professional, Enterprise, Performance, Unlimited and Developer Editions
This limit controls the maximum amount of records and users that can be followed by an individual user through Chatter.
The default limit is 500 and hard coded maximum is 2,000.
Resolution
To request Salesforce.com Support to increase the number of entities that a single user can follow in chatter, please take the following steps:
1- 
Verify that the Organization where you want the feature to be enabled is on an edition where the feature is available.
2- 
Have a System Administrator to log a Case with Salesforce Support
3- 
Please mark as "feature activation" on the General Application Area
On the case description, please specify: 
Feature requested: 
Maximum number of entities that a user can subscribe to
Organization ID where you want the limit to be increased (Navigate to Setup > Company Profile > Company Information):
I am the system admin in charge of this feature.
What is the current limit?
What is the requested limit?
4- 
Our Support Team will review the Case and action the request as needed.
Note: As a best practice we kindly suggest to implement an un-follow process to prevent hitting the limit in the future.
 
