### Topic: What are push notifications? How do I enable or disable them for Chatter Mobile for BlackBerry? | Salesforce
What are push notifications? How do I enable or disable them for Chatter Mobile for BlackBerry? | Salesforce
What are push notifications? How do I enable or disable them for Chatter Mobile for BlackBerry?
Push notifications are alerts that apps send to your mobile device's home screen when you're not using the app. These alerts can consist of text, icons, and sounds, depending on your device type. 
The Chatter Mobile for BlackBerry mobile app uses push notifications to keep you aware of important Chatter activity without requiring you to return to the app.
 
A setting controls whether or not you receive Chatter push notifications
; however, you can't enable push notifications if they were disabled by your administrator
.
To learn about enabling or disabling push notifications, visit the Chatter mobile app help site from your device.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_faq_mobile_what_are_push_notifications.htm&language=en_US
Release
202.14
