### Topic: Chatter | Salesforce
Chatter | Salesforce
Chatter
Chatter is a Salesforce collaboration application that connects, engages, and motivates users to work efficiently across the organization regardless of role or location. Chatter lets users collaborate on sales opportunities, service cases, campaigns, and projects with embedded apps and custom actions.
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
User Permissions Needed
To enable Chatter:
“Customize Application”
Salesforce organizations created after June 22, 2010 have Chatter already enabled for all users by default. However, if you only want certain parts of your organization to use Chatter, you can do a profile-based rollout of Chatter instead. With a profile-based rollout, only the users that have been assigned the required user profile or permission sets have access to Chatter. Profile-based rollout of Chatter is useful for larger companies and companies or government agencies with regulatory constraints. It allows them to plan a controlled rollout and deploy Chatter on a department-by-department basis.
We recommend rolling out Chatter for all users in your organization, unless your company doesn’t want all users to have full access to Chatter. Chatter is secure and works in accordance with all the security and permission settings in your Salesforce organization.
See Also:
Profile-Based Chatter Rollout Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_setting_up.htm&language=en_US
Release
202.14
