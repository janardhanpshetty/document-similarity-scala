### Topic: Polls | Salesforce
Polls | Salesforce
Polls
Use polls to conduct a survey. A poll contains a list of choices in a feed post that lets people cast a vote by selecting one of the choices.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Polls are a great way to get people’s opinions. For example, you are in charge of planning an annual sales meeting and need to find out when most people are available to attend the meeting. Instead of emailing everybody in the sales organization, you could create a poll in the Sales Chatter group that lists possible dates and ask people to vote for the date that works best for them.
When you create a poll, anybody with access to the feed or the poll can vote on the poll. So when you post a poll, consider who you want to participate. For example, if you post a poll in a private group, only the members of that group can see the poll and vote. Keep the following in mind about polls:
When you create a poll, you can add up to ten choices.
When voting on a poll, you can only select one choice.
Polls are anonymous. You can see how many people voted, but you can’t see who voted.
Click 
Refresh
 to see the latest poll result and the number of votes that were cast.
You can’t repost a poll.
People can only cast one vote, but they can change their vote.
You can mention someone and include hashtag topics in the poll question.
You and others can add topics to your poll question after posting.
The email notification options you enabled in your Chatter Email Settings apply to polls just as they do to posts. For example, if you get a notification whenever someone comments on a post you made, you also get an email notification when someone comments on a poll you posted.
Note
The poll result doesn’t always add up to 100 percent due to rounding. For example, if a poll has three choices and each gets one vote, the result adds up to 99 percent.
See Also:
Liking Posts and Comments
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_feed_polls.htm&language=en_US
Release
202.14
