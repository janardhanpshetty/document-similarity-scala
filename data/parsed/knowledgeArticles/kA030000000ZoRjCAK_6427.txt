### Topic: The Create Case button is available when SfO is logged into a production org, but will not display if SfO is pointed to a newly refreshed sandbox org
The Create Case button loads, is available and functional when Salesforce for Outlook is logged into a production org, but will not display if SfO is pointed to a new sandbox org.  Reinstalling Salesforce for Outlook doesn't solve the problem, and neither do the usual troubleshooting steps (clearing registry keys, repairing Outlook, etc). 
Resolution
This odd behavior is likely due to the fact that email service addresses are missing in the sandbox org. These email services are used by the Create Case button to create cases; the button cannot work without them so Salesforce for Outlook won't display it.
To fix this problem, please do the following: 
Use a browser to log into your sandbox
In the Setup menu, go to Customize | Cases | Email-to-case
Locate the routing address you use to create cases from Outlook (the source is marked as "Outlook")
Click on the Edit button next to the name
Notice there is no email service address 
Click on Save without making any changes
A new email service address should be automatically generated. Repeat the same steps for all the routing addresses whose source is "Outlook".
Keep in mind that the sandbox refresh process does not copy email service addresses and they must be recreated manually after every refresh.
