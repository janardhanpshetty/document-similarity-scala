### Topic: Steps to resolve when Salesforce1 app menu do not display the standard or custom objects.
Why does the Salesforce1 app menu not display the Standard / Custom objects like:
Cases
Contacts
Accounts
Leads
Opportunities
 
Resolution
To resolve this issue, follow the below steps:
* Login to salesforce through the browser.
* Click on 'Setup'
* Under Administer
Mobile Administration | Salesforce1 Navigation
Make sure 'Smart Search Items' appears under the 'Selected' items. If not move 'Smart Search Items' from Available to Selected.
Save the settings.
Login to the Salesforce1 app through a mobile device and you will find all the objects appearing in menu items
If a specific object is still not showing up, make sure that the user's profile has the object tab settings set to "Default On" or "Default Off". The tab setting "Tab Hidden" will not make the object show up in Salesforce1.
NOTE: Additionally, users who wish to ensure certain objects are always shown within the Recent section per Salesforce1 can PIN object-specific search results via the desktop UI. This will also refine what is presented via the Global Search so it's IMPORTANT to pin ALL relevant objects that you wish to have displayed within Global Search.
 
