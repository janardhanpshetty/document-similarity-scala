### Topic: Emails About Flow Errors | Salesforce
Emails About Flow Errors | Salesforce
Emails About Flow Errors
Every time a flow interview fails, the admin who created the associated flow gets an email. The email includes the error message from the failure and details about every flow element that the interview executed.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Note
This release contains a beta version of the flow error email that is production quality but has 
known limitations
. You can provide feedback and suggestions for the flow error email on the 
IdeaExchange
.
If the interview failed at multiple elements, the admin receives multiple emails, and the final email includes an error message for each failure. If a flow uses fault connectors, its interviews can fail at multiple elements.
Example
An error occurred at element Apex_Plug_in_1.
List index out of bounds: 0.

An error occurred at element Fast_Delete_1.
DELETE --- There is nothing in Salesforce matching your delete criteria.

An error occurred at element Email_Alert_1.
Missing required input parameter: SObjectRowId.
See Also:
Limitations of Emails About Flow Errors (Beta)
Customize What Happens When a Flow Fails
Why Did My Flow Interview Fail?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_troubleshoot_email.htm&language=en_US
Release
202.14
