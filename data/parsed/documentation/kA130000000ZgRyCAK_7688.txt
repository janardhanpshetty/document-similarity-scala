### Topic: Create a Process | Salesforce
Create a Process | Salesforce
Create a Process
To create a process, define its properties and which records it should evaluate, and then add criteria nodes and actions.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, or view processes:
“Manage Force.com Flow”
AND
“View All Data”
Define the Process Properties
Define the process properties to uniquely identify and help you manage your processes.
Identify Which Records to Evaluate
Associate the process with an object, and specify when to start the process.
Add Process Criteria
Define the criteria that must be true before the process can execute the associated actions.
Add Actions to Your Process
After you’ve defined a criteria node, define the actions that are executed when the criteria are met.
Execute Actions on More Than One Criteria
Choose whether to stop or continue your process after specific criteria are met and associated actions execute.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=process_create.htm&language=en_US
Release
202.14
