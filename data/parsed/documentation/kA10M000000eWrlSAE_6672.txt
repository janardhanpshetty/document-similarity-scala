### Topic: Lightning Experience Home | Salesforce
Lightning Experience Home | Salesforce
Lightning Experience Home
Give your users everything they need to manage their day from the Home page in Lightning Experience. Your sales reps can see their quarterly performance summary and get important updates on critical tasks and opportunities. You can also customize the page for different types of users and assign custom pages for different profiles.
Available in: Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Create and edit Home pages from the Lightning App Builder. From Setup, enter 
Lightning App Builder
 in the 
Quick Find
 box, then select 
Lightning App Builder
. Click 
New
 to create a Lightning Home page, or edit an existing page.
You can also access the Lightning App Builder directly from the Home page. Click 
 and select 
Edit Page
 to create a copy of the current Home page to edit.
Set a New Default Home Page
Set a new default Home page to surface the information that’s most relevant for your users. All users see the default Home page unless they have profiles that are assigned to another Home page.
Assign Custom Home Pages to Specific Profiles
Assign pages to different profiles to give your users access to a Home page perfect for their role.
Lightning Experience Home Permissions and Settings
Give your users access to opportunity details and other permissions so they can get the most out of the Home page.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_home_lex_intro.htm&language=en_US
Release
202.14
