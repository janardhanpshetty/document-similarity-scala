### Topic: Soql query with a custom indexed filter that is set as unique, may return incorrect results in sandbox. Rebuilding the custom index/external Id on the field should resolve this issue.
Issue:
An SOQL Query that uses a filter which is unique and indexed, may return incorrect results in sandbox. With the refresh, the index on the custom field may get corrupted.
 
Resolution
Remove and Recreate the External Id and unique properties of the custom field should resolve the issue.
If the field has a custom index  reach out to support for rebuilding the index on the field.
