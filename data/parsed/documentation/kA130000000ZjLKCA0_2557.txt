### Topic: Specify the Asset Owner for Assets | Salesforce
Specify the Asset Owner for Assets | Salesforce
Specify the Asset Owner for Assets
Asset records have an 
Asset Owner
 field, which is used to set hierarchy and sharing-based access controls. By default, the asset owner is the user who creates the asset record.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
 Editions
User Permissions Needed
To enable sharing:
“Manage Sharing”
To create, edit, and delete page layouts:
“Customize Application”
Beginning Spring ’15, the 
Asset Owner
 field is used for access control when the Assets object’s sharing setting is 
Private
, 
Public Read-Only
, or 
Public Read/Write
. If the Asset object’s sharing setting is 
Controlled by Parent
, the asset owner field has no effect on access control. That’s because the parent account’s settings control access to the asset.
If you activated Salesforce before Spring ’15, you can automatically update the asset owner field on asset records when you enable the Asset object sharing preference. The sharing preference sets the value of the asset owner field to equal either:
The user who created the asset
The user who is the owner of the parent account
By default, the 
Asset Owner
 field doesn’t appear on the page layout. To add the 
Asset Owner
 field to the page layout, from the object management settings for assets, go to Page Layouts.
From Setup, enter 
Asset Settings
 in the 
Quick Find
 box, then select 
Asset Settings
.
Select the option 
Enable Asset Sharing
.
The options for setting the default 
Asset Owner
 field appear for newly created asset records.
Set the 
Asset Owner
 to be either:
Creator of the asset
Owner of the parent account
Save your changes.
To change the asset owner of an asset record, click the Assets tab and select an asset record. Or, open the asset record from the Assets Related List on the Accounts, Contacts, or Products tabs. In the Asset Detail section, click 
Change
 next to the 
Asset Owner
 field and select a different user.
See Also:
Find Object Management Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=assets_owner_field.htm&language=en_US
Release
202.14
