### Topic: Create Global Quick Actions | Salesforce
Create Global Quick Actions | Salesforce
Create Global Quick Actions
You can add global actions to any page that supports actions, like the Home page, the Chatter tab, and object pages. For example, add a Log a Call action to global layouts to let users record call details right from a Chatter thread.
User Permissions Needed
To create actions:
“Customize Application”
Walk Through It: Create a Global Quick Action
From Setup, enter 
Actions
 in the 
Quick Find
 box, then select 
Global Actions
.
Click 
New Action
.
Select the type of action to create.
Customize the action.
For a Create a Record action, select the type of object to create. If the object has more than one record type, select the one you want to use for records created through this action.
For a Custom Action, select a Visualforce page or canvas app, and then specify the height of the action window. The width is fixed.
Enter a label for the action. Users see this label as the name of the action.
Tip
If you’re creating a Create a Record or Log a Call action, choose an option from the 
Standard Label Type
 list to have Salesforce generate the label. For the labels in this list that include “Record” and “Record Type”, Salesforce fills in the type of object or the record type the action creates. For example, if you choose the Create New “Record” standard label on a create contact action, the generated label is Create New Contact.
If necessary, change the name of the action.
This name is used in the API and managed packages. It must begin with a letter and use only alphanumeric characters and underscores, and it can’t end with an underscore or have two consecutive underscores. Unless you’re familiar with working with the API, we suggest not editing this field.
Type a description for the action.
The description appears on the detail page for the action and in the list on the Buttons, Links, and Actions page. The description isn’t visible to your users.
 If you’re creating several actions on the same object, we recommend using a detailed description, such as “Create Contact on Account using New Client record type.”
For a Create a Record or Log a Call action, select whether you want a feed item to be created when the action is completed.
For a Create a Record, Update a Record, or Log a Call action, you can add a custom success message that displays after the action executes successfully.
Optionally, click 
Change Icon
 to select a different icon for the action.
Custom images used for action icons must be less than 1 MB in size.
Click 
Save
.
After you create a quick action, customize its layout, 
add predefined values
, and then 
add the action to page layouts
.
Tip
If you delete an action, the action is removed from all layouts that it’s assigned to.
See Also:
Set Predefined Field Values for Quick Action Fields
Customize Actions with the Enhanced Page Layout Editor
Custom Success Messages for Quick Actions
Visualforce Pages as Object-Specific Custom Actions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=creating_global_actions.htm&language=en_US
Release
202.14
