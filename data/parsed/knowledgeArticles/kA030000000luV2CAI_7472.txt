### Topic: Beginning in Summer '15 you can utilize Chatter to collaborate with your team on Territory Management.
If your organization uses Chatter, your team can now use it to collaborate on territory model development.
After you enable and configure Chatter Feed Tracking, anyone with access to a territory model record can use Chatter to collaborate on that model. Those who follow the record will get notifications in their own Chatter feeds when model states change and tracked fields are updated. Make sure to tell others who maintain territory models that they can now use Chatter to follow and collaborate on territory models directly from model records.
For details on enabling the Chatter feed for the Territory Model object, see the 
Enterprise Territory Management Implementation Guide
. 
Resolution
