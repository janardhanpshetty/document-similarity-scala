### Topic: Flow choices are not updating salesforce records in update or create Flow elements
When I map my Flow Screen Choice Fields to salesforce object fields, either in an update, or record create, the choice values are not passed to the record in salesforce.
Resolution
When mapping a Screen Choice Field to any field in salesforce, your choices require "Stored Values" or they will pass null.
Edit your "Choices" and enter a value in the Stored Value section. Simply type the text of the value you'd like the choice to store and pass to salesforce. Type the literal string with no quotes brackets, etc. (in many situations the stored value will be the same as the label).  Disregard the "No Matches Found" message and save the stored value.
Your Screen Choice fields mapped to salesforce will now pass the choice value selected in the flow screen.   
