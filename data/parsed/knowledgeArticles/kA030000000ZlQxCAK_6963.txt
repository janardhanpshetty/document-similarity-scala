### Topic: Users are not seeing the Chat Button field on a Transcript record
Some Users are unable to see the Chat Button field on the Live Agent Transcript record. The field is visible via Field Level Security and is on their associated Page Layout. 
Resolution
Beyond Field Level Security and the field needing to be on the Page Layout, the User also needs to have "View Setup and Configuration" perm via their Profile or Permission Set. 
 
This is because the Chat Button field is a lookup to the Chat Button, which is housed in Setup. User's without "View Setup and Configuration" do not have access to these branches of Setup. 
