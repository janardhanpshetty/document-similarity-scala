### Topic: Edit Object Permissions in Profiles | Salesforce
Edit Object Permissions in Profiles | Salesforce
Edit Object Permissions in Profiles
Object permissions specify the type of access that users have to objects.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Professional
, 
Group
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view object permissions:
“View Setup and Configuration”
To edit object permissions:
“Manage Profiles and Permission Sets”
AND
“Customize Application”
From Setup, either:
Enter 
Permission Sets
 in the 
Quick Find
 box, then select 
Permission Sets
, or
Enter 
Profiles
 in the 
Quick Find
 box, then select 
Profiles
Select a permission set or profile.
Depending on which interface you're using, do one of the following:
Permission sets or enhanced profile user interface—In the 
Find Settings...
 box, enter the name of the object and select it from the list. Click 
Edit
, then scroll to the Object Permissions section.
Original profile user interface—Click 
Edit
, then scroll to the Standard Object Permissions, Custom Object Permissions, or External Object Permissions section.
Specify the object permissions.
Click 
Save
.
See Also:
Object Permissions
Profiles
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=perm_sets_object_perms_edit.htm&language=en_US
Release
202.14
