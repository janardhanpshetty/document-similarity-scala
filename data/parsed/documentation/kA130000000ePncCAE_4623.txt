### Topic: Enable Login Forensics | Salesforce
Enable Login Forensics | Salesforce
Enable Login Forensics
Perform this quick, one time setup to start collecting data about your org’s login events.
User Permissions Needed
To enable login forensics
“Modify All Data”
You can enable login forensics from the Event Monitoring Setup page in the Setup area.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=monitor_login_forensics_enable.htm&language=en_US
Release
202.14
