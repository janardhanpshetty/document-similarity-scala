### Topic: Viewing 24-Hour Force.com Sites Usage History | Salesforce
Viewing 24-Hour Force.com Sites Usage History | Salesforce
Viewing 24-Hour Force.com Sites Usage History
Available in: Salesforce Classic
Available in: 
Developer
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To create and edit Force.com sites:
“Customize Application”
Monitor the bandwidth and request time usage for each of your sites by viewing the usage data tracked on this related list. By closely monitoring your sites, you can avoid 
exceeding your limits
.
To view the 24-hour usage history for your site:
From Setup, enter 
Sites
 in the 
Quick Find
 box, then select 
Sites
.
Click the name of the site you want to view.
View the 24-Hour Usage History related list. Usage information may be delayed by up to several minutes due to processing time.
The 24-Hour Usage History related list tracks and displays the following usage metrics for your site:
Metric
How It's Calculated
Origin Bandwidth
Bandwidth is calculated as the number of megabytes served and received from the site's origin server. The 
Daily Limit
 applies to a rolling 24-hour period.
Request Time
“Service request time” is calculated as the total server time in minutes required to generate pages for the site. The 
Daily Limit
 applies to a rolling 24-hour period.
“Origin server” refers to the Web server that hosts your site. “Rolling 24-hour period” refers to the 24 hours immediately preceding the current time.
For each metric, the related list displays 
Current Usage
, 
Daily Limit
, and the 
Percent Used
.
See Also:
Tracking Your Force.com Site with Google Analytics
Reporting on Force.com Sites
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sites_usage_history.htm&language=en_US
Release
202.14
