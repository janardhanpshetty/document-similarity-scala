### Topic: Community Users may face insufficient privileges on their cases when viewing through the community. When logging in as a Portal user, however, they can see the cases without issue.
ISSUE:
----------
Community users may notice an Insufficient Privileges error when viewing their cases from within the community, even though the cases have been shared to them via a Sharing Set, and the user has the necessary profile permissions on cases in order to view such records.
Symptoms of this include being able to hover over the cases and see information listed in the case (as opposed to a miniature insufficient privileges error in the hover box), as well as being able to see the case listed in Related Lists (such as on the parent Account record) and List Views. Normally, if a user does not have access to a record, it will not appear in list views or related lists.
STEPS TO DETERMINE IF THIS IS HAPPENING TO YOU:
----------------------------------------------------------------------------------------
-Navigate to affected user's Contact record
-Click Manage External User > Log Into Community As User
-Navigate to a case where you receive Insufficient Privileges
-Log out of the community
-On the same contact, click Manage External User > Log Into Portal As User
-Navigate to the same case where you received Insufficient Privileges in the community
-If you can view the record, this is indeed the issue.
Resolution
The difference between the Community View and the Portal View, is that the Portal does not have the Sidebar/Header nor Chatter Feed, whereas the Community Does.  In order to create and view the Feed element, Read access on the Email field on the Contact Object is required.
To resolve the issue, ensure that the standard Email field on the Contact object has at least Read access granted via Field-Level Security on the profile (or via Permission Set) of the Community User.
