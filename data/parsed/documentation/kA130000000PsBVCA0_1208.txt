### Topic: Edit Your Salesforce for Outlook Sync Settings | Salesforce
Edit Your Salesforce for Outlook Sync Settings | Salesforce
Edit Your Salesforce for Outlook Sync Settings
Depending on whether your administrator gave you permissions, customize which items sync between Microsoft Outlook and Salesforce, and the directions they sync.
This feature available to manage from: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To access your Salesforce for Outlook configuration
Assigned to an active configuration
To disable sync:
“Whether object is synced”
From your personal settings in Salesforce, search for Salesforce for Outlook. Then click 
View My Configuration
.
To see which kinds of records sync, hover over filter icons (
).
To stop an item from syncing, clear its checkbox. All fields related to that item are disabled on the page.
Save your changes. All changes take effect the next time your data syncs. Return to your original settings any time by clicking 
Revert to default
 at the top of your modified configuration. Your administrator can change your settings as needed.
See Also:
Manage Your Salesforce for Outlook Configuration
Syncing Between Microsoft® Outlook® and Salesforce Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=outlookcrm_personal_config_sync.htm&language=en_US
Release
202.14
