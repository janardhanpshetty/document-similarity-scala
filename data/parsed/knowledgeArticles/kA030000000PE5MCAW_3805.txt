### Topic: This article describes the workaround to provide this functionality.
How to understand the Report Type used in a report?
The easiest way to see it would be to click on customize and review the information above the name of the report on the top left hand side of the page.
If you want to run a report including all the report types of your report, please follow the steps below:
Resolution
A. Build a custom report type with the primary object as Reports
Navigate to Setup | Create | Report Types | New Custom Report Type
Select "Reports" as your primary object
Add your Report Type Label and Name, a description, the category to store the report in, and decide if the Report Type should be deployed or not. Click "Next."
Add a secondary object if desired. Reports can be linked to Dashboard Components to track what report is populating which component.
Click "Save."
B. On the Report Type detail page, scroll down and click the "Edit Layout" Button.
Add in your desired fields, such as "Report Type" from the box on the right.
Click "Save."
C. Build a custom Report using your newly created custom Report Type, filtering by report name or report ID.
Your Report Type will be the one located where you designated in step A3.
The report type field will be either: the name of the Report Type chosen in the report wizard, or, if the Report Type is a custom one, the name you previously designated.
If your standard report type is not available anymore, you will find a number instead. Please find below a list of the most common numbers and related standard report types:
1 Accounts
2 Contacts w/ Accounts
3 Training
4 Campaigns
5 Campaign Calldown
6 Campaigns w/ Contacts
7 Campaigns w/ Leads
8 Campaign Members
9 Campaigns w/ Opportunities
10 Cases
11 HTML Email Status
12 Forecasts
13 Forecast: Quota vs Actual
14 Forecast History
15 Activities w/ Accounts
16 Activities w/ Cases
17 Activities w/ Contacts
18 Activities w/ Leads
19 Activities w/ Opportunities
20 Lead Status
21 Leads
22 Opportunities
24 Tasks & Events
25 Opportunities w/ Products and Schedules
26 Opportunities w/ Contact Roles
27 Opportunity History
28 Lead Lifetime
29 Opportunity w/ Partner
30 Products w/ Custom Objects
31 Opportunity Sales Teams
32 User
33 Solution
34 Self Service Portal Usage
35 Self Service Portal Users
36 Activities w/ Contracts
37 Activities w/ Campaign
38 Payments
39 Invoices
40 Invoices w/ Line Items
41 Contracts w/ Orders
42 Contract w/ Orders w/ Line Items
43 Billable Product
44 Leads
45 Accounts w/ Partner
46 Contracts
47 Accounts
48 Documents
49 Accounts w/ Account Teams
50 Opportunities w/ Competitor
52 Accounts w/ Custom Objects
53 Contacts w/ Custom Objects
54 Solutions w/ Custom Objects
55 Opportunities w/ Custom Objects
56 Campaigns w/ Custom Objects
57 Cases w/ Custom Objects
58 Contracts w/ Custom Objects
59 Custom Objects w/ Custom Objects
60 Products
61 Pricebooks w/ Product
62 Contracts w/ Approvals
63 Cases w/ Case History
64 Products w/ Opportunity
65 Events w/ Attendee
66 Customizable Forecasting: Summary
67 Customizable Forecasting: History
68 Customizable Forecasting: Opportunity Forecasts
69 Accounts w/ Contact Roles
70 Contracts w/ Contact Roles
71 Activities w/ Solutions
72 List
73 Opportunity Trend
74 Activities w/ Custom Object
75 Solution Categories
76 Assets
77 Assets w/ Products
78 Assets w/ Cases
79 Case Emails
81 Accounts w/ Assets
82 Contacts w/ Assets
83 Cases w/ Assets
84 Products w/ Assets
85 Assets w/ Custom Objects
86 Orders w/ Custom Objects
87 Orders
88 Orders with Line Items
89 Activities w/ Products
90 Solutions w/ Solution History
91 Cases w/ Case History
92 Territories
93 Opportunities w/ Contact Roles & Products
94 Opportunities with Sales Team & Product
95 Custom Object w/ Audit History
96 Leads w/ Lead History
97 All Pending Workflow items
98 Pending Delegated Workflow items
99 Solution Master Translations
100 Cases w/ Solutions
101 Cases w/ Contact Roles
102 Contracts w/ Contract History
103 Orders w/ Order History
104 Api Usage
105 Leads w/ Connections
106 Accounts w/ Connections
107 Opportunities w/ Connections
108 Custom Objects w/ Connections
109 Campaigns w/ Lead Lifetime
110 Contacts w/ Contact History
111 Accounts w/ Account History
112 Campaigns w/ Campaign Members
113 Logins
114 Opportunities with Stage History
115 Content
116 Workspace
117 Campaigns w/ Influences
118 Opportunities w/ Opportunity Splits
119 Opportunities w/ Products & Splits
120 Opportunities w/ Schedules & Splits
