### Topic: Considerations for Editing Users | Salesforce
Considerations for Editing Users | Salesforce
Considerations for Editing Users
Be aware of the following behaviors when editing users.
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Usernames
A username must be unique across all Salesforce organizations. It must use the format of an email address (such as xyz@abc.org), but doesn’t need to be a real email address. While users can have the same email address across organizations, usernames must be unique.
If you change a username, a confirmation email with a login link is sent to the email address associated with that user account. If an organization has multiple login servers, sometimes users can’t log in immediately after you’ve changed their usernames. The change can take up to 24 hours to replicate to all servers.
Changing email addresses
If 
Generate new password and notify user immediately
 is disabled when you change a user’s email address, Salesforce sends a confirmation message to the email address that you entered. Users must click the link provided in that message for the new email address to take effect. This process ensures system security.
Personal information
Users can change their personal information after they log in.
User sharing
If the organization-wide default for the user object is Private, users must have Read or Write access to the target user to access that user’s information.
Domain names
You can restrict the domain names of users’ email addresses to a list of specific domains. Any attempt to set an email address with another domain results in an error message. To enable this functionality for your organization, contact Salesforce.
See Also:
Edit Users
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=users_edit_considerations.htm&language=en_US
Release
202.14
