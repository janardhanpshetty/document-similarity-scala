### Topic: A summary of image-oriented sapplets.
A summary of image-oriented sapplets.
Resolution
The 
Photo Gallery
 and 
Images
 sapplets were designed expressly to host and display images. In both of these sapplets, you can upload  images for display on your tab layout, with additional options for showing thumbnails, titles, descriptions, links, and other customizations. 
Many other sapplets also have image support built-in. The 
Product Gallery
 sapplet, for example, displays a series of tabbed item descriptions and images. The 
About Us
 sapplet is a container for basic business information such as hours and location, but also can show a logo or picture. 
Many 'description'-style sapplet fields support HTML. This flexibility provides for deeper customization, by allowing images to be displayed in-line almost anywhere in a tab layout. To take advantage of this, images can be hosted in the suite's 
Asset Library
, and referenced with the appropriate HTML tags as necessary.
For step-by-step instructions on configuring ProfileBuddy sapplets, please see the ProfileBuddy User Guide on the 
ProfileBuddy Quick Start
.
