### Topic: At times a Salesforce user will receive an Insufficient Privileges error message when trying to delete an Event record from a Public Calendar, even if the user created the Event record.
At times when the organization wide defaults sharing settings is set to "
Controlled by the Parent
" for the 
Activity 
Object, a standard user with a  "Full Access" permission on the public calendar, will not be able to delete an Event record which was created on the Public Calendar, even when the user is the creator of the Event and has all the necessary profile permissions "Edit Task and Events".
The Full Access permission on the Public Calendar will only allow other users to
 see detailed information about events in the calendar, insert events in the calendar, and edit existing events in the calendar.
 If the Event is related/associated to an Object record or to a Contact/Lead record, the related object or associated Contact/Lead record presumably dictates the delete permission for the Event record as it is controlled by the Parent Object.(Related/Associated Object).
 
Resolution
NOTE: Unless the user is the 
"Owner
" of the associated/related record, or a user who has 
"Modify All data"
 permission set on the profile, or "
Modify All
" on the Associated/ Related Object, the Event record can not be deleted while in the Organization Wide Sharing Settings the Activity Object is 
set
 to 
Controlled by Parent
.
Workarounds:
1. Set the Activity object to "Private" in the Organization Wide Sharing Settings . A user will be able to delete any Event from the Public Calendar as long as the user has the "Full Access" permission assigned for the specific Public Calendar and has the Edit Tasks and Events on the user profile enabled. (The Event record is not controlled by the associated object.)
2. Edit and remove the Related/Associated Object name from the Event record field, and than delete the Event record.
 
