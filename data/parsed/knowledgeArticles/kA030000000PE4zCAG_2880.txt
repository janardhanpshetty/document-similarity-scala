### Topic: When a mail program that can open HTML e-mails (such as Outlook) opens the file, it scans the HTML file for references to images in an attempt to load them. When that URL is opened, Salesforce registers that the email has been viewed and the html status is updated accordingly.
How does Salesforce Track HTML Emails?
Resolution
HTML emails are sent as an HTML file. When a mail program that can open HTML e-mails (such as Outlook) opens the file, it scans the HTML file for references to images in an attempt to load them.
One of the images included in an HTML e-mail is a 1x1 pixel image which is so small it is normally transparent. This image is stored on the Salesforce server. When that URL is opened, Salesforce registers that the email has been viewed and the html status is updated accordingly.
However, if people have configured their browser to not download embedded images, this doesn't work and the html tracking will not register. Conversely some spam filters and mail gateways scan the contents of emails and will open the tracking URL as they pass the mail.  This will record that the mail has been opened but it would not have been opened by the actual recipient.
You can activate the ability to track these emails by following either of the click paths below:
Click path for the 
ORIGINAL 
Setup interface: 
Setup | App Setup | Customize | Activities | Activity Settings | Email Tracking (checkbox) 
Click path for the 
NEW 
Setup interface: 
Go to Setup | Build | Customize | Activities | Activity Settings 
| Email Tracking (checkbox) 
