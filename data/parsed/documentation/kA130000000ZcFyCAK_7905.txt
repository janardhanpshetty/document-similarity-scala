### Topic: Notes on Using Lookup Filters with Person Accounts | Salesforce
Notes on Using Lookup Filters with Person Accounts | Salesforce
Notes on Using Lookup Filters with Person Accounts
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions except for 
Database.com
.
If your organization uses person accounts, note the following:
Person Accounts don't support Contact filters; however, Person Accounts support Account filters. For example, if the 
Account
 field has a dependent lookup filter that's added to a Person Account, dependent lookups are supported. If the 
Contact
 field has a dependent lookup filter that's added to a Person Account, dependent lookups isn't supported.
Lookup filter criteria on 
Account Name
 only apply to business accounts, not person accounts. For example, if your lookup filter criteria is 
Account Name
 does not contain book
, business accounts with “book” in the name, such as John’s Bookstore, are not valid, but person accounts with “book” in the name, such as John Booker, are valid and appear in the lookup dialog for the 
Account
 field. If you need to filter on the name for a person account, use the 
First Name
 or 
Last Name
 fields instead.
Use the 
Is Person Account
 field in your lookup filter criteria to restrict the valid values of a lookup field to one type of account (either person accounts or business accounts). For example, to restrict a lookup to only person accounts, include the following in your lookup filter criteria: 
Is Person Account equals True
.
You can't package lookup filters that reference standard fields specific to person accounts, such as the 
Email
 and 
Title
 fields.
See Also:
Lookup Filters
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_lookup_filters_notes_person_accounts.htm&language=en_US
Release
202.14
