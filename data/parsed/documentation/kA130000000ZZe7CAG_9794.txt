### Topic: Sample Discounting Formulas | Salesforce
Sample Discounting Formulas | Salesforce
Sample Discounting Formulas
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
For details about using the functions included in these samples, see 
Formula Operators and Functions
.
Maintenance and Services Discount
This formula field uses two custom currency fields: 
Maintenance Amount
 and 
Services Amount
. It displays “Discounted” on an opportunity if its maintenance amount and services amount do not equal the opportunity 
Amount
 standard field value. Otherwise, it displays "Full Price."
IF(Maintenance_Amount__c + Services_Amount__c <> Amount, 
"Discounted", 
"Full Price")
Opportunity Discount Amount
This formula calculates the difference of the product 
Amount
 less the 
Discount Amount
. Note that 
Discount Amount
 is a custom currency field.
Amount -
                    Discount_Amount__c
Opportunity Discount Rounded
Use this formula to calculate the discounted amount of an opportunity rounded off to two digits. This example is a number formula field on opportunities that uses a custom percent field called 
Discount Percent
.
ROUND(Amount-Amount* Discount_Percent__c,2)
Opportunity Discount with Approval
This formula adds a “Discount Approved” checkbox to an opportunity. It uses conditional logic to check the value of the approval flag before calculating the commission.
IF(Discount_Approved__c, ROUND(Amount – Amount * DiscountPercent__c, 2), Amount)
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=useful_advanced_formulas_discounting.htm&language=en_US
Release
202.14
