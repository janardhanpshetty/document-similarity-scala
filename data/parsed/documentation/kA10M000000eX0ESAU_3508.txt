### Topic: Prospecting Insights in Lightning | Salesforce
Prospecting Insights in Lightning | Salesforce
Prospecting Insights in Lightning 
Data.com Prospecting Insights combines Dun & Bradstreet company details and industry intelligence in a snapshot. Tabs with company information, account hierarchies, and links to related industries help you start smart conversations with your prospects and customers.
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
 Editions
Watch a demo: 
Prospecting Insights in Lightning
Prospecting Insights is available with a Data.com Prospector license. Insights data is view-only and available for accounts that were imported from, or cleaned by, Data.com.
Important
If you use Professional Edition, add the Data.com Key field to the Accounts page layout.
Company
View a company overview, financial details, a list of top competitors, and a breakdown of account contacts. View and add contacts by level or department. Read details about key competitors. Understand company linkages.
Hierarchy
Get a complete hierarchical view of a company and its linkages. Click the plus (+) sign next to a company to add it as an account.
Industry
Critical industry insights help you intelligently engage with an account.
Industry Details
View details about the accounts industry as identified by Standard Industrial Classification (SIC) and North American Industry Classification System (NAICS) codes and descriptions.
Competitive Landscape
Understand the potential of the account in its related industry.
Trends
Understand the account’s financial situation and identify potential growth areas.
Opportunities
Review various account opportunities.
Call Prep
Prepare for a call with detailed Q&A.
Industry Websites
See the websites of the account’s top competitors.
See Also:
Manage Access to Data.com Search Results and Related Features
Make Sure You Can See Data.com Prospecting Insights
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=jigsaw_int_see_more_prospecting_insights_details.htm&language=en_US
Release
202.14
