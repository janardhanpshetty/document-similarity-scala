### Topic: Who in my organization can use the import wizards? | Salesforce
Who in my organization can use the import wizards? | Salesforce
Who in my organization can use the import wizards?
All users in your organization can use the Import My Contacts wizard. In Enterprise, Unlimited, Performance, or Developer Edition organizations with person accounts enabled, all users can use the Import My Person Accounts wizard.
Only an administrator can use the organization-wide Data Import Wizard to import accounts, contacts, leads, solutions, or custom objects for multiple users at one time. In Personal Edition, the Data Import Wizard isn't available. In Contact Manager Edition, leads and solutions in the Data Import Wizard aren't available. In Group Edition, solutions in the Data Import Wizard isn't available.
Important
Salesforce has replaced the individual import wizards for accounts, contacts, and other objects with the Data Import Wizard. Individual import wizards open in small popup windows, while the Data Import Wizard opens in a full browser with dataimporter.app at the end of the URL.
 From Setup, enter 
Data Import Wizard
 in the 
Quick Find
 box, then select 
Data Import Wizard
. The options you see depend on your permissions.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_import_general_who_in_my_org.htm&language=en_US
Release
202.14
