### Topic: If you I discard a new or refreshing Sandbox before it is activated, you cannot create a new copy immediately. Customers will need to wait the regular interval from the date the discarded copy was created, before they are able to create a new copy.
If I choose the Discard option for my new Sandbox copy before it is activated, can I create a new copy immediately?
Resolution
After a sandbox is discarded, you will need to wait the regular interval for the sandbox license type, before a refresh will be available again.
When you create or refresh a sandbox, the "copied on" date for that sandbox is updated. This date is retained, even if the new copy is discarded before activation. 
NOTE: For more information on sandbox refresh interval times, please see 
Understanding Sandbox Environment Types
.
