### Topic: Validation Status
Platform Cache is a new product available to ISV partners for FREE.
If you get a request to enable this feature, 
meet SLA
, change the record type of your case to 
Partner Program Support
, and assign with active assignment rules(if the case does not go to 
Alliances Technical Tier 1
 queue, manually assign it). 
ONLY ALLIANCES SHOULD ENABLE THIS FEATURE!
- What kind of org can get Platform cache? 
[Platform Cache is available to all orgs, but for free only to EE, UE and PxE (customer) orgs.  ISV partners can get 1 10MB partition in up to two of their DE orgs.  All general DE orgs will have to apply for the trial cache, which will be approved by the PM (Raj Advani), and will have an expiration date]
 
- Limited to 2 DE orgs per partner.
 
- Limited to 10MB per org, anything additional requires PM approval.
 
- Available only for ISV partners, any other type of partner or customer will require PM approval.
- Blacktab Name: 
Platform Cache available to the Org (MB). Edits associated with purchase.
 
- Limited amount of cache available. What's the long term plan here? 
[From PM:  We are taking a wait and see approach right now to see what the demand is in the general DE category.  Right now we feel as though we have enough space to handle future requests, as long as we manage this properly]
 
Resolution
1. Open up this 
spreadsheet
 to determine if partner already has org(s) with platform cache enabled. If partner has 2 orgs with platform cache already, then please provide the two org IDs to the partner and let them know they are only allowed to have platform cache in two DE orgs.
2. If partner has less than 2 orgs on the spreadsheet, ask the partner the following question:
~~~
Hi,
 
This feature is currently available to 2 orgs per partner, can you please confirm you would like this feature enabled in the org you provided? 
 
Regards, 
Salesforce Partner Support
~~~
3. After confirming the org ID, run it in BT and open up the org details. Increase the limit "
Platform Cache available to the Org (MB). Edits associated with purchase.
" to 10 and hit "Save".
4. Fill out all the details required in the 
spreadsheet
 after provisioning platform cache to the org.
5. Let the partner know platform cache has been provisioned to their org and close the case.
APPROVAL PROCESS: Fill out the details required in the spreadsheet and @mention the Product Manager (Raj Advani) on the line item.
 
