### Topic: View a Particular Feed | Salesforce
View a Particular Feed | Salesforce
View a Particular Feed
Change your Chatter feed to display a particular subset of posts.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
By default, you see your What I Follow feed when you go to your Chatter page. Use the feed selections on the left sidebar of the Chatter page to view a subset of posts in your feed. For example, view only posts that mention you, or posts you bookmarked, or all posts in your company.
Click 
 
Feed
 to see the available feeds.
Click a feed to display the associated subset of posts in your feed.
What I Follow
Shows updates for everything you follow, including posts from people you follow, groups you are a member of, and files
 and records
 you’re following. Use the drop-down list at the top of your feed to further narrow the subset of posts.
To Me
Shows posts that are made on your profile page, including
Posts others make on your profile page
Posts and comments where you have been mentioned
Posts you made that have comments
Bookmarked
Shows your bookmarked posts in your feed.
All Company
Shows posts and comments from your entire company, including posts and comments from:
People in your company
Public groups
Private groups you are a member of
Feed tracked changes for records and fields and system-generated posts, if someone liked or commented on the post. You must have access to the record to see the post.
See Also:
Filter Your Files List
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_feed_filters.htm&language=en_US
Release
202.14
