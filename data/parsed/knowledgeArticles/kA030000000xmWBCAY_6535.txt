### Topic: Speed up resolution time by providing some added details to your Radian6 for Salesforce case.
Before 
submitting a case to Salesforce Marketing Cloud Customer Support
, refer to the below templates to make sure you're including all the details required for them to assist you. If excluded, Support will reach out to ask for this information, however, providing it with your case can save time for you to help them resolve your issue faster.
Resolution
Add the following and 
grant access to a Salesforce Marketing Cloud Support Analyst
:
 
1. Detailed explanation of the expected result.
2. Detailed explanation of the actual result.
3. Organization ID#.
4. What package version are you using?
5. Is the issue in a Sandbox or Production Organization?
6. Radian6 Username and Integration User information.
7. Screen shot of error or error message.
8. Are you using the Engagement Console or Social Hub with Radian6 for Salesforce?
9. Example of Social Posts or Cases with the issue.
10. Example of Daily Digest email if errors appear or any other error email.
11. Please also include any troubleshooting steps you have taken to try to resolve the issue.
