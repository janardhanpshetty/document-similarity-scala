### Topic: Guidelines for Reducing Search Crowding | Salesforce
Guidelines for Reducing Search Crowding | Salesforce
Guidelines for Reducing Search Crowding
Are users reporting that records aren’t appearing in their search results? Use these guidelines to help your users find the record they need.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions 
except Database.com
The search engine applies limits to the number of records analyzed at each stage of the search process. Limits are important because they help maintain performance and don’t overwhelm the user with irrelevant records. However, users don’t always find all possible matching results because the record that they’re looking for falls outside the result limit. This behavior is called search crowding or truncation. Search crowding typically happens when:
Users have limited permissions or access to records. Therefore, the records they do have access to might not be part of the results set that is filtered by access permissions.
Users search using a term that matches a huge number of records. Because the search matches so many records, the search engine can’t determine what specific record the user is searching for.
The search engine relevancy algorithms and sharing permissions decide the records returned in search results and the order of the results. To avoid search crowding and truncation:
Encourage users to use more specific search terms
Searches work best when users enter a unique search term. 
Acme Company San Francisco
 returns more relevant results than 
Acme
.
Encourage users to narrow the search scope
When users are on the search results page, limit the search scope to the object type for the record desired. The search is rerun. Potentially, users could see more results, because the full result set limit is applied against a single object.
Create list views
Create a list view for a specific set of contacts, documents, or other object records that you search for repeatedly. List views have no limits to the number of records and have a set order. Sharing rules are also applied.
See Also:
SOSL Limits
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_crowding_considerations.htm&language=en_US
Release
202.14
