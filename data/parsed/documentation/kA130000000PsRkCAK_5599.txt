### Topic: Translate Articles Within Salesforce Knowledge | Salesforce
Translate Articles Within Salesforce Knowledge | Salesforce
Translate Articles Within Salesforce Knowledge
If your organization translates Salesforce Knowledge articles internally, you can enter the translation from the translation detail page.
Available in: Salesforce Classic
Salesforce Knowledge is available in 
Performance
 and 
Developer
 Editions and in 
Unlimited
 Edition with the Service Cloud.
Salesforce Knowledge is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions.
User Permissions Needed
To work with translated articles:
“Manage Articles”
AND
“Create,” “Read,” “Edit,” or “Delete” on the article type (depending on the action)
Depending on the status of your translation and 
the article actions assigned to you
, you can do the following from the translation detail page.
Action
Description
Article Status
Archive
Archiving
 removes published translations that are obsolete so they no longer display to agents and customers on your organization's Salesforce Knowledge channels.
To archive a translation, archive its master article.
Assign...
Assigning changes the owner of the translation.
Draft translations
Delete
Deleting a translation permanently removes it from the knowledge base.
Note
You can't undelete a draft translation.
Draft translations
Edit
Editing modifies the translation's content or properties.
Draft and published translations
Preview
Previewing shows how the translation appears to end users.
Note
Voting and Chatter information is not available when previewing a Knowledge article.
Draft and published translations
Publish...
Publishing translations makes them visible in all channels selected.
Draft translations
Click the 
Article Management
 tab and select 
Translations
 in the View area.
Select 
Draft Translations
.
Note
You can also edit a published translation. It reverts to draft status until you republish it, although you can choose to keep the existing version published while you update it.
Optionally, change the 
Assigned To
 filter to view articles that are not assigned to you for translation.
For example, you might want to view articles assigned to a translation queue.
Click 
Edit
 next to the article and language you want to translate.
Enter your translation.
Click 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=knowledge_article_translation.htm&language=en_US
Release
202.14
