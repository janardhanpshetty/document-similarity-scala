### Topic: This article explains on a possible way to resolve the XMLHttpRequest cannot load, No 'Access-Control-Allow-Origin' is present error.
Users get error  XMLHttpRequest cannot load, No 'Access-Control-Allow-Origin' is present in visualforce page
You may have a problem with an embedded visualforce page for custom objects that include custom components. The page might be embedded on the standard page layout for a custom object. When that custom object record is viewed, the VF page is rendered. The page then starts polling to see when a process is complete. This polling request is failing because the request is originating from a different domain. 
For e.g. you can see that the Origin header is set to “
https://na14.salesforce.com
”. The request is going to “c.na14.visual.force.com” so the request fails.
Resolution
The issue can be addressed using the built-in HTTP(S) Proxy. You can enable the proxy in the standard setup menu.
Setup -> Security Control -> Remote Sites / Remote Proxy Settings
There you can add your own salesforce instance. e.g. eu1.salesforce.com (don't forget to enable https).
In a second step you have to modify your ajax request to set url as "https://c.na1.visual.force.com/services/proxy"
The best solution is to use the fully documented HTTP Proxy service from Salesforce.
