### Topic: Create Steps in the Flex Designer | Salesforce
Create Steps in the Flex Designer | Salesforce
Create Steps in the Flex Designer
With the flex dashboard designer (beta), you can build queries (also known as steps) within the designer. Convert the calculated columns of compare tables into visualizations, or customize your queries in the SAQL editor. Both approaches provide an easy way to set up your charts by specifying which measures to show and in what order.
Note
A dashboard widget consists of a step plus a visualization. The step is often a query that dynamically runs to show you different views of your data. The step can also be static values pulled from your data. Either way, it’s different from a lens. A lens stands alone, while a step is only available in the dashboard designer. When you clip a lens for use in a dashboard, you’re sending the step part to the designer.
There are two ways to create steps in the flex designer:
Clone an existing step and then modify the clone
Build a new step
To clone an existing step, do the following.
Click Show Steps (
) to open the steps panel.
Click the step that you want to clone. It opens in exploration mode, where you can easily modify it. If the step was created using SAQL, the clone opens in the SAQL editor instead.
Complete your changes to the clone.
Click 
Save
. The clone is saved with “Clone of” prepended to the original step’s name. The modified clone appears in the steps panel and is available for use in the dashboard.
Note
Two types of steps can’t be cloned: a static step and a Compare Table that has a SAQL query for one or more of its columns.
To build a new step, do the following.
Click Show Steps (
) to open the steps panel.
Click 
Create Step
.
Select a dataset. Datasets appear in most recently used order. Type in the search box if your dataset isn’t easy to find.
Explore and create visualizations. 
From here, you have three choices.
If your step is not a Compare Table, you can simply click 
Save
 and the step is added to the designer’s available steps.
To use formula fields, select the Compare Table (
) from the charts gallery and create calculated columns. 
When you save, you’re prompted to confirm or change how you’d like the columns with measures to appear in the visualization.
Slide the measures to change their order. You can also click a measure to hide or show it. The order affects how a measure is used in a visualization. For example, in a Scatter Plot, the first measure is used as the x-axis, the second as the y-axis, and the third as the size of the bubble.
From there, 
Save
 adds the step to the designer’s available steps.
To refine your query, select 
Show SAQL
 in the Options menu (
), and click the Edit icon (
).
The SAQL editor provides contextually smart auto-completion suggestions. When you make changes in the SAQL editor, the 
Next
 button is disabled until you select 
Run Query
 and the editor verifies that the query is valid.
Next
 takes you to the drag-and-drop dialog box to confirm or change how you’d like the groups and measures to appear in the visualization. From there, 
Save
 adds the step to the designer’s available steps.
Note
In some cases, if you clone a step and edit its query's SAQL, the resulting JSON doesn’t reflect the actual function applied to measures. Once you run and save the SAQL, the function is changed to 
count
. For example, if the original step contained this 
measures
 array:
"query": {
        "measures": [
        ["max","DailyActiveUsers"]
        ]
      }
      
and the SAQL was edited, run, and saved, then the updated JSON contains this 
measures
 array instead:
"query": {
        "measures": [
        ["count","*","max_DailyActiveUsers"]
        ]
      }
    
This discrepancy has no effect on the data viewed in the dashboard.
See Also:
Quickly Build Responsive Grid-Based Dashboards with Flex Dashboard Designer (Beta)
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_dashboard_create_query.htm&language=en_US
Release
202.14
