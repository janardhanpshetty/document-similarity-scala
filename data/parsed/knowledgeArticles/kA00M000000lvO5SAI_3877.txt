### Topic: Agents are seeing that the Transfer button becomes greyed out and cannot be clicked on.
Agents may notice that when they are in an active chat and want to transfer it to another Skill, the Transfer icon becomes greyed out and inaccessible. This is happening even when other Agents from those Skills are available for chats.
Resolution
This will happen if you have Live Agent Configurations that have a Skill assigned that has no Users or Profiles associated. If you create a Skill that has 0 Agents assigned, and assign it to a Configuration that has "Chat Transfer To Skills Enabled" checked, it causes a conflict in the system. 
Since no agents or profiles are assigned this Skill, we assume that no user is online when performing our checks to update the transfer options on peer login, logout and status change.
You should not assign empty Skills as transferable options to Configurations. If you want the Configuration to not be able to transfer by Skill, then disable "Chat Transfer To Skills Enabled" option. 
 
