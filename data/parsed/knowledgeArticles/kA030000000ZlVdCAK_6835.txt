### Topic: Approval Assignment email is sent twice when there is rejection action to send an email to the previous approver.
Issue:
If any of your approval step has a rejection action to send an email to the previous approver,  two emails are sent to the approver.
Cause:
when an approver rejects, the following happens:
- an email alert is sent to the previous approver to inform that the record has been rejected= OK
- an email alert is sent to the previous approver to inform that a new record has to be approved (via the approval assignment email template) = NOT OK!
 
 NOTE:The approver request email is automatically sent anytime someone is pegged as an approver - that's not really configurable. 
Resolution
 
 In the Rejection Behavior Section, change the selection to "Perform all rejection actions for this step AND all final rejection actions" and f
ix the rejection email which goes to the previous approver and instead adjust the content of your approval request email so it covers both scenarios: a new approval assignment as well as a re-assignment due to rejection.
