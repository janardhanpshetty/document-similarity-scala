### Topic: The Admin receives an error message when trying to re-assign a performance summary: "Cannot reassign the Manager Summary Request to someone responding to a request on this subject."
The Admin receives an error message when trying to re-assign a performance summary: "Cannot reassign the Manager Summary Request to someone responding to a request on this subject."
Resolution
The error suggests that the manager the Admin is trying to reassign the performance summary to has already been assigned a peer or skip level summary on the same individual. To confirm this, follow the steps below:
1. Go to the Performance | Deployment tab
2. Select the People Action for the cycle in question
3. Enter the name of the employee the summary is about and click on their name in the auto-complete drop-down.
4. A table should load showing you all the summaries about the employee.
If the new manager appears there the Admin will first need to delete their existing peer or skip level summary. Make sure to copy any information that has been entered by the manager as part of the peer or skip level summary. Once the peer or skip level summary is deleted, the Admin should be able to reassign the performance summary to the new manager.
To delete a summary the Admin needs to use a data management tool such as Workbench.
1. Log in as a Salesforce Admin in to 
Workbench
2. From the top menu choose 
Queries
 | 
SOQL Query
 and paste the query below in the 
Enter or modify a SOQL query below
 box:
SELECT Id, Name from WorkPerformanceCycle WHERE Name = 'Coaching Cycle'
Note: Be sure to replace the 'Coaching Cycle' text with the name of your performance cycle
3. Hit the Query button and copy the value in the Id column
4. Paste the query below in the 'Enter or modify a SOQL query below' box:
 
SELECT FeedbackRequestState,FeedbackType,Id,PerformanceCycleId,QuestionSetId,RecipientId FROM WorkFeedbackRequest WHERE FeedbackRequestState = 'Draft' AND FeedbackType = 'CyclePeer' AND PerformanceCycleId = 'ID COPIED IN STEP 3' AND RecipientId = 'ID OF USER WHO NEEDS TO BE ASSIGNED A PERFORMANCE REVIEW' AND SubjectId = 'ID OF THE USER THE PERFORMANCE REVIEW IS ABOUT'
Note: Be sure to replace 'CyclePeer' with 'CycleSkip' if you want to delete a skip level summary.
To get Id's of users you can visit their detail page and copy the Id from the web browser's URL:
5. Hit the Query button
Note: Only one record should be returned here.
6. Click on the value in the Id column to be redirected to the request then hit the Delete button. After doing that the peer summary should be deleted for the user.
7. Go back to Salesforce | Performance tab | Deployment subtab and re-assign the performance summary to the new manager.
