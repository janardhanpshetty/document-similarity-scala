### Topic: Why are there two import options under Manage Members for campaigns? | Salesforce
Why are there two import options under Manage Members for campaigns? | Salesforce
Why are there two import options under Manage Members for campaigns?
You can access the Data Import Wizard from any campaign detail page. Click 
Manage Members
 and select one of these options.
Add Members - Import File
—Add contacts, leads, or person accounts to the campaign.
Update Members - Import File
—Update the statuses of campaign members.
See Also:
Importing Campaign Members
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_import_campaign_why_are_there.htm&language=en_US
Release
202.14
