### Topic: Delete Articles and Translations | Salesforce
Delete Articles and Translations | Salesforce
Delete Articles and Translations
You can delete articles and translations on the Article Management tab or the detail page of the article or translation. Deleting permanently removes articles from the knowledge base. You can delete draft articles, draft translations of articles, or archived articles, but not published articles or translations.
Available in: Salesforce Classic
Salesforce Knowledge is available in 
Performance
 and 
Developer
 Editions and in 
Unlimited
 Edition with the Service Cloud.
Salesforce Knowledge is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions.
Note
To delete published article and translations, first remove them from publication by choosing edit or archive.
When a user without delete access cancels the editing on published article, the newly created article draft is not deleted automatically.
Deleting articles moves them to the Recycle Bin, where you can undelete them if you change your mind. If you delete an article with translations, the translations are also moved to the Recycle Bin. However, if you delete a single translation, you can't undelete it. Deleting an article or translation may fail if another user or the system simultaneously modifies it while the deletion is being processed. You receive an error message when this occurs.
Note
Conflicts might occur when different agents perform actions on the same articles simultaneously. Depending on who performs the action first, the articles will not be available for subsequent users though the articles still display momentarily in the articles list. Performing an action on these articles results in a conflict error.
If you delete a draft article that is a working copy of a currently published article, the original published version is not affected but the draft version is permanently removed. It does not go to the Recycle Bin. You can edit the published version to work again on a draft copy.
When a user without delete access cancels editing a published article, the newly created article draft is not deleted automatically.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=knowledge_article_delete.htm&language=en_US
Release
202.14
