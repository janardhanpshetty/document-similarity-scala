### Topic: Reporting With Divisions | Salesforce
Reporting With Divisions | Salesforce
Reporting With Divisions
If your organization uses divisions to segment data, you can customize your reports to show records within specific divisions.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To limit reports by division:
“Affected by Divisions”
Use the Division drop-down list on the report to select one of the following.
A specific division
Your current working division.
All records across all divisions.
Note
Reports that use standard filters (such as My Cases or My Team’s Accounts) show records in all divisions. These reports can’t be further limited to a specific division.
See Also:
Divisions Overview
Changing Your Working Division
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_division.htm&language=en_US
Release
202.14
