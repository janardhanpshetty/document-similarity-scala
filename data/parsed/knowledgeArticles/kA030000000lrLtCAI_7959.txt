### Topic: Troubleshooting steps and methodology to effectively investigate an Internal Server Error (ISE) received from a package or App installation failure.
Customers may encounter an internal server error (ISE) when an App from the AppExchange or package installation into their Salesforce environment fails.  Given every App and customer's implementation is unique the install process may encounter exceptions for which a user friendly error message does not exist, hence they receive an ISE.  In this scenario the only course of action is for them is to engage Support to investigate further.
The 
INTERNAL: Internal Server Error Release Escalation Process
 article details appropriate steps to take when you receive an ISE in any application area and should be followed.  The troubleshooting steps below are intended to be performed in addition to the process above to help expedite case handling when an ISE is encountered specifically on App installation failures.
 
Resolution
With any internal server error, it's best to begin troubleshooting by searching the 62 Org via Global Search for the error's Stacktrace Id.  If that particular error has occurred previously past cases may provide it's resolution.  To get the Stacktrace Id you'll have to locate the Error ID which is typically provided at the bottom of an Internal Server Error message and will look like this:
Error ID: 1523154416-86221 (1677602414)
An ISE is composed of two distinct parts. The Gack Id (first set of numbers usually containing a dash): 1523154416-86221 and the Stacktrace Id: 1677602414 which may include the minus sign (-) if one is prefixed before the second sequence of numbers in the Error ID.
If your Global Search for the Stacktrace Id does return results or provide a proposed resolution or Knowledge Article you'll need access to Splunk and GUS to continue troubleshooting.  If access to these resources is unavailable please follow the standard 
Support - Severity 2 - 4 T1/T2 rep process
 and complete the template found here: 
INTERNAL: Support Escalation Template - Usability Brand
 to then direct the case to the "SFDC - T2 - CRM Usage" queue.
SFDC T2 Usage steps:
1. Use the following Splunk query to locate the ISE by substituting the appropriate values in for the [bracketed] items and setting the date range:
index=
[InstanceHere]
* 
[OrgIdHere]
 (sourcetype=CASE(applog*:gslog) OR sourcetype=CASE(applog*:gglog)) 
[StacktraceIdHere]
 
2. Expand the result for the error if it's returned in Splunk to then locate the Thrown: field which will contain the underlying error message that's resulting in the ISE:
An example using the Error ID above is: "Thrown: lib.gack.GackContext: moduleapi.isv.packaging.exception.PackageInstallException: 00D20000000DB01: Multiple install problems encountered [PackageNameHere: duplicate value found: unknown duplicates value on record with id: 0DS200000009afk: awDuplicateCheckforSalesforce: duplicate value found: unknown duplicates value on record with id: 0DS200000009afk]"
  
Note
Typically when you replicate the ISE you'll see variable Gack Ids but a common Stacktrace Id.  Although the Stacktrace Id may be the same for the occurrences of an error in one particular environment, the same underlying error may produce a different Stacktrace Id in a separate environment.
For example, the underlying error message "duplicate value found: unknown duplicates value on record with id: 0DS200000009afk" in the example ISE above has also produced the following errors in different environments:
Error ID: 227136751-32803 (584268425)
Error ID: 476852425-16318 (929979564)
 
3. If the error message in Splunk doesn't provide a clear resolution search for the StacktraceId and/or the underlying error message via the Global Search in GUS:
There's a chance that, although the Stacktrace Id may be variable across different environments, the underlying error message could be the same and also encountered in a previous investigation which may also provide the issue's resolution.
 
For example, searching "duplicate value found: unknown duplicates value on record with id" returns related work records (Investigations and Bugs) in GUS.  If you sort the work record results by date with the most recent first, you'll find a previous occurrence of this particular error message in the top result.  Upon reviewing the investigation's comments you'll see that it was resolved via the "ScrutinyFlexIndex" scrutiny under CF with Entity/Field: AppMenuItem.
