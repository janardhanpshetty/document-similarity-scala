### Topic: Missed some Infrastructure Dreamforce Sessions from 2015? Catch up on them here!
Missed some Infrastructure Dreamforce Sessions from 2015? Catch up on them here!
Resolution
The Future of Scaling at Salesforce
: Salesforce's customer transaction growth continues to break all expectations, growing by 80% in the most recent quarter. This means you're being successful! But it also requires serious engineering to scale the service and keep up with this unprecedented demand. In this talk, Software Architect Ian Varley will expand on his DreamForce talks from 2013 and 2014 to cover the Future of Scale at Salesforce. The talk will tie together the design patterns and approaches that will drive Salesforce to 10x (and 100x!) our current scale, while maintaining a no-compromise approach to safety and availability. We'll cover fungible infrastructure capacity, containerization, automation, continuous integration, event streaming, distributed data systems, and more!
Video & More
: 
https://success.salesforce.com/Ev_Sessions?eventId=a1Q30000000DHQlEAO#/session/a2q30000001AlVOAA0​
 
Network Optimization and Directions at Salesforce
: Network access is key to your Salesforce applications. Come listen to the Salesforce Network Engineering experts on how you can optimize your network connections to Salesforce through a combination of Network best practices, testing and monitoring of the connections between your sites and Salesforce (with ThousandEyes or Appneta), and through Salesforce Connect partners (including AT&T, Verizon, BT, Tata Communications, Vodafone and more). Also get a sneak peak at some of the future thinking on our network directions.
Video & More
: 
https://success.salesforce.com/Ev_Sessions?eventId=a1Q30000000DHQlEAO#/session/a2q30000001HiocAAC
 
 
Site Switching and Disaster Recovery
: At Salesforce, Trust is our #1 Value and a key component of that Trust is Availability. Come learn how Salesforce uses Site Switching to ensure availability for our Infrastructure. You will learn what the team does and how you can get involved to participate.
Video & More
: 
https://success.salesforce.com/Ev_Sessions?eventId=a1Q30000000DHQlEAO#/session/a2q30000001AlVcAAK
 
Data Center Information Management at Salesforce
: At Salesforce, we manage our entire hosting infrastructure using our own platform. 100,000 pieces of equipment, tens of data center environmental metrics and thousands of Change Requests, incidents and alarms ever day flow though Force.com leveraging the very best of our platform capacity, from mobile to analytics. Come learn how you can use Salesforce to also manage your large IT automated environments from a single location, all in the cloud.
Video & More
: 
https://success.salesforce.com/Ev_Sessions?eventId=a1Q30000000DHQlEAO#/session/a2q30000001AlVNAA0
 
Site Reliability Engineering at Salesforce
: In this talk you will learn about how the Global Site Reliability team coordinates Incident Management for critical incidents within the Salesforce core applications. You will also learn what we do to ensure service availability, as well as the opportunity to see the lengths to which we've gone to research and develop the best methodology to consistently and efficiently respond when things do go wrong with the service and ​further to continually ​test & improve the resiliency of the service on an ongoing basis. You will also get an insiders view into the workings of our response plans, the roles, the strategy and the measurable gains we've incurred as a result.
Video & More
: 
https://success.salesforce.com/Ev_Sessions?eventId=a1Q30000000DHQlEAO#/session/a2q30000001HiohAAC
 
