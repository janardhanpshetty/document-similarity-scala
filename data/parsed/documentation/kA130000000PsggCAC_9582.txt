### Topic: Category Group Article Visibility Settings Examples | Salesforce
Category Group Article Visibility Settings Examples | Salesforce
Category Group Article Visibility Settings Examples
Review examples of category group settings for article visibility permissions.
Available in: Salesforce Classic
Data categories and answers are available in 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions.
Salesforce Knowledge is available in 
Performance
 and 
Developer
 Editions and in 
Unlimited
 Edition with the Service Cloud.
Salesforce Knowledge is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions.
There are three types of visibility:
All Categories: All categories are visible
None: No categories are visible
Custom: Selected categories are visible
With custom data category visibility, you can only see the data categories permitted by their role, permission sets, or profile.
These examples are based on two sample category groups, Products and Geography:
Note
Although category group visibility settings are available with answers communities (questions) and Salesforce Knowledge (articles), the examples below apply to articles only. Answers communities support one category group and one data category per question.
Example 1: A Role Hierarchy
In this example, the Acme Electronics organization manufactures hardware and provides customer support for both consumers and enterprises. The Engineering department is organized by products. The Support department is organized geographically. Europe and the Americas are managed by corporate teams, but Asia is outsourced. Within the corporate and outsourced teams, there are subteams dedicated either to consumer or enterprise support.
The table below shows the categories visible to each role in the Acme Electronics organization, and states whether the visibility settings are inherited from the parent role or if they are 
custom visibility settings
.
Acme Electronics Role Hierarchy
Visible Geographic Categories
Visible Product Categories
CEO
All Countries
All Products
VP of Engineering
All Countries
Inherit from CEO
All Products
Inherit from CEO
Consumer Engineering Team
All Countries
Inherit from VP of Engineering
Consumer Electronics
Custom
Enterprise Engineering Team
All Countries
Inherit from VP of Engineering
Enterprise Electronics
Custom
Computers Engineering Team
All Countries
Inherit from VP of Engineering
Computers
Custom
VP of Support
All Countries
Inherit from CEO
All Products
Inherit from CEO
VP of Corporate Support
Europe, America
Custom
All Products
Inherit from VP of Support
Director of Corporate Consumer Support
Europe, America
Inherit from VP of Corporate Support
Consumer Electronics, Computers
Custom
Director of Corporate Enterprise Support
Europe, America
Inherit from VP of Corporate Support
Enterprise Electronics, Computers
Custom
Outsourced Support
Asia
Custom
All Products
Inherit from VP of Support
Consumer Support Team
Asia
Inherit from Outsourced Support
Consumer Electronics, Computers
Custom
Enterprise Support Team
Asia
Inherit from Outsourced Support
Enterprise Electronics, Computers
Custom
Example 2: Article Visibility
The table below is an in-depth example of how 
category visibility settings
 restrict what users see. This example has three sample users whose category settings are noted in parentheses.
Table 1. Example: How Category Visibility Settings Restrict What Users See
Categories
When User 1's visibility is All countries/Computers, the category is:
When User 2's visibility is America/All products, the category is:
When User 3's visibility is France/None, the category is:
All countries/Laptop
VISIBLE
VISIBLE
NOT VISIBLE
Canada/Computers
VISIBLE
VISIBLE
NOT VISIBLE
USA/All products
VISIBLE
VISIBLE
NOT VISIBLE
Europe/Switches
NOT VISIBLE
NOT VISIBLE
NOT VISIBLE
Europe/No Categories
VISIBLE
NOT VISIBLE
VISIBLE
User 1: The user must be granted visibility in each category that classifies the article, or each category that classifies the article must be visible by default. 
In this example, User 1
 can see Europe, because Europe is the child of All Countries, but he cannot see Switches, because Switches does not belong to Computers. That's why 
User 1
 cannot see articles classified with Europe/Switches.
User 2: When a category is made visible to a user through custom settings or is made visible by default, its child and parent categories are implicitly included; therefore, 
User 2
 can see articles categorized with All Countries because it is the parent category of America. He can also see Articles classified with USA because it is the child of America.
User 3: If a user has no access to the whole category group, he can only see articles that are not categorized in that group. 
User 3
 cannot see the articles categorized with All countries/Laptop because he has no visibility in the category group that includes Laptop, but he can see articles categorized with Europe/No categories.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=category_visibility_whatis_example.htm&language=en_US
Release
202.14
