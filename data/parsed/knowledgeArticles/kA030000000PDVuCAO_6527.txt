### Topic: Salesforce support cannot change System Administrator access to your Account without written consent from a C-Level executive in your organization. This tightly controlled process is often referred to as an Admin Change Request.
If you can’t access the System Administrator account in your organization, support may be able to change the email address associated with the 
existing, active System Administrator 
User Record. This applies ONLY if no active System Administrators can log into your account. 
 
Resolution
First, try these options: 
 
If your company does have an active System Administrator then please have this person change the email address or other User details by logging in and going to Setup > Manage Users > Users, and then clicking “Edit” next to the name.
If you have access to a former administrator’s email, you may also consider utilizing the "forgot password?" feature.
If you own the corporate email domain, you can use "password reset" and have the password reset email intercepted by your internal email administrator.
 
If these approaches do not work, and NO authorized System Administrators can access your account, Salesforce Support can help change the System Administrator email address. To preserve the security and integrity of the data within your account, we require a letter from a C-Level officer (CEO, CIO, CFO, Business Owner, etc). Once the letter is received and attached to the support case, the Salesforce Support representative will escalate your case for further review. Please find the required format for this letter below.
 
Admin Change Request Letter
On a company letterhead, please provide the following information:
 
1. What is the current full USERNAME on the user record that you are requesting to change?  
2. What is the new EMAIL address?  (This should be a corporate email address as opposed to a Gmail, Yahoo, AOL, or similar email address)
3. What is the brief reason for the change?  
4. What is the Case Number you opened with Salesforce support?
5. A C-level executive signature (CEO, CIO, CFO, Business Owner, etc), the signature must be in ink and include the executive title and their EMAIL address.
Thank you,
Executive Name
Executive Title
Executive Phone number
Executive Email address
[ - executive signature -
]
Please send the letter to Salesforce support by email for faster processing or fax:
a) Scan/email the letter to the Salesforce support representative handling your case
b) Or, Fax to +1 415-592-3583 with the attention of Technical Support and inform the Salesforce Support representative that this has been faxed. 
Salesforce Support and legal teams will review and validate the request and act accordingly. If approved we will make the requested change and notify, via email from the case, the person requesting and the C-level executive authorizing the change. We will then change the email address as requested allowing the user to reset the password (using forgot password) and log into the account.
