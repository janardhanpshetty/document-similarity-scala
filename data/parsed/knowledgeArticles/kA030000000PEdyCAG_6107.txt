### Topic: The organization has the "Enable Collapsible Sidebar" checkbox checked under App-Setup -> Customize -> User Interface, but the user still cannot see a collapsible sidebar option.
Why don't I see the collapsible sidebar even though it's enabled in my org?
Resolution
The organization has the "Enable Collapsible Sidebar" checkbox checked under App-Setup -> Customize -> User Interface, but the user still cannot see a collapsible sidebar option.
To trouble-shoot this scenario, first check the user's profile to see if there is a Call Center attached to their profile.  User's with the a Call Center attached will not have access to the collapsible sidebar.  To check this, search for the user record and click on it.  Find the "Call Center" field and check if there is one listed.
If there is no Call Center attached to the user's profile, contact Salesforce support to troubleshoot the situation further.
Please Note
 - Accessibility Mode will also prevent a User from seeing the Collapsible Sidebar option. 
