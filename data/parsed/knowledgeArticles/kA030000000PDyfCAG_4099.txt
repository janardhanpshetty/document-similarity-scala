### Topic: To find out which Report Type is being used in your report, you can create a new 'Report Type' with 'Report' as the Primary object
Once a custom report type has been created and you are working with various reports/dashboards, it can be difficult to determine which report type is utilized by the report itself.
Resolution
In order to pull a separate report providing this info, complete the following steps:
***For the old report builder:
Build a custom report type - Setup | Create | Report Types | Click on "New Custom Report Type"
Select "Reports" as the primary object 
Select the "Report Type" field on the layout page
Build a report (typical setup, choose criteria etc.)
"Report Type" column will reflect which Custom Report Type is being used.*
*If the report was built from a standard report type, the field will display the category originally selected in the report wizard when the report was being created.
***For the New report builder:
Click on Customize in the report, and look at the upper left corner.
