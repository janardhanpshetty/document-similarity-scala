### Topic: This article is applicable to the services branded as Pardot (“Pardot Services”) and provides links to documentation describing a number of trust and compliance-related topics.
This article is applicable 
to the services branded as Pardot (the “
Pardot Services
”) and provides links to documentation describing
:
the architecture of, the security and privacy-related audits and certifications received for, and the administrative, technical and physical controls applicable to the Pardot Services;
features, restrictions and notices associated with any:
                       *  information sourced from third parties or public sources and provided to customers via the Pardot Services;
                       *  Pardot Services functionality that allows customers to interact with social media and other websites; and
                       *  desktop and mobile device software applications provided in connection with the Pardot Services.
Resolution
The following articles and their attachments are available on 
help.salesforce.com
. 
Pardot Security, Privacy, and Architecture Documentation (attached to this article)
Pardot Notices and License Information (attached to this article)
 
