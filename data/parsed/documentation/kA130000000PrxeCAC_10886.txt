### Topic: Validation Rules Fields | Salesforce
Validation Rules Fields | Salesforce
Validation Rules Fields
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Field
Description
Rule Name
Unique identifier of up to 40 characters with no spaces or special characters such as extended characters.
Active
Checkbox that indicates if the rule is enabled.
Description
A 255-character or less description that distinguishes the validation rule from others. For internal purposes only.
Error Condition Formula
The expression used to validate the field.
 See 
Build a Formula Field
 and 
Formula Operators and Functions
.
Error Message
The message that displays to the user when a field fails the validation rule.
If your organization uses the Translation Workbench, you can translate the error message into the languages Salesforce supports. See 
Enable and Disable the Translation Workbench
.
Error Location
Determines where on the page to display the error.
 To display the error next to a field, choose 
Field
 and select the field. If the error location is a field, the validation rule is also listed on the detail page of that field. If the error location is set to a field that is later deleted, to a field that is read only, or to a field that isn’t visible on the page layout, Salesforce automatically changes the location to 
Top of Page
.
Note
Error messages can only be displayed at the top of the page in validation rules for case milestones and Ideas.
See Also:
Define Validation Rules
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_validation_rules_fields.htm&language=en_US
Release
202.14
