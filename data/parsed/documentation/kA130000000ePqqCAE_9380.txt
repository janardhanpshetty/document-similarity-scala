### Topic: Case Teams and Queues | Salesforce
Case Teams and Queues | Salesforce
Case Teams and Queues
Create teams of people who work together to resolve cases faster, and create queues to share workloads among teams.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Case Teams
Case teams help groups of people work together to solve a case, such as a support agent, support manager, and a product manager.
Queues
Queues help you prioritize, distribute, and assign records to teams who share workloads. You can access queues from list views, and queue members can jump in to take ownership of any record in a queue. Queues are available for cases, leads, orders, custom objects, service contracts, and knowledge article versions.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=service_teams_overview.htm&language=en_US
Release
202.14
