### Topic: Set Up Create Case for Salesforce for Outlook Users | Salesforce
Set Up Create Case for Salesforce for Outlook Users | Salesforce
Set Up Create Case for Salesforce for Outlook Users
This feature available to manage from: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
The Create Case feature in Salesforce for Outlook lets users create cases in Salesforce from emails in Microsoft® Outlook®. As an administrator, you can create Email-to-Case destinations that appear in the drop-down list button 
Create Cases
 in Outlook. For each destination, you choose the assignee, which can be either individual users or queues. You can add up to 10 destinations for each Outlook configuration. When users create cases, they can add up to 10 emails simultaneously for each destination.
Before Salesforce for Outlook users can create cases from Outlook emails, you’ll need to perform the following procedures.
If you haven’t already done so, enable and configure On-Demand Email-to-Case.
Define Email-to-Case destinations (also known as email routing addresses).
Enable the Create Case feature in your configurations, which adds the 
Create Cases
 drop-down list button in Outlook.
Salesforce for Outlook assigns a category to the emails that Salesforce for Outlook users add as cases to Salesforce. This category, 
Added to Salesforce as a case
, makes it easy for users to search for emails they added as cases to Salesforce.
See Also:
Set Up Reps to Create Records Directly from the Salesforce Side Panel
Create Salesforce for Outlook Configurations
Set Up Email Options for Salesforce for Outlook
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=outlookcrm_create_case_setup.htm&language=en_US
Release
202.14
