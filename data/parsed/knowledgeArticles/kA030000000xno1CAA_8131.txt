### Topic: Join us along with a number of other like-sized Salesforce customers in a moderated discussion on how to best manage your business by the numbers. Ask questions, share ideas, discuss, and engage – that's the blueprint for a successful Circles of Success best practice discussion!
 
Increase Sales using Reports & Dashboards
Salesforce includes a comprehensive analytics package that can help you track and assess the performance of your business. Are you finding it difficult to make sense of the different features? Ever wondered how our other successful customers are tracking their metrics? 
Some of the key topics we will cover include:
Leading KPIs of top customers
Built-in functionality to easily forecast and manage pipeline
Existing best practice Dashboards/Reports for individuals and executives
Strategies for using business metrics to improve operational efficiency
Join us along with a number of other like-sized Salesforce customers in a moderated discussion on how to best manage your business by the numbers. Ask questions, share ideas, discuss, and engage – that's the blueprint for a successful Circles of Success best practice discussion! 
Target Audience
Business users with intermediate level knowledge of using Salesforce Analytics.
Learning Objectives
You will walk out of this Circles of Success best practice discussion with:
An understanding of how key business metrics can be tracked in Salesforce
Resources you can leverage to take advantage of Salesforce built in Analytics
Recommended apps from the Salesforce AppExchange
Pre-Requisites
Access to a PC or Mac with ability to install 
GoToWebinar
Success Community Topics:
Any discussions/collaboration on the Salesforce Success Community relevant to this Roundtable will be tagged with the following Topics.
#CoS
#CirclesOfSuccess
#BusinessMetrics
 
For more resources check out the 
Achieve More Hub: Analyze Data Using Reports and Dashboards
 
Resolution
