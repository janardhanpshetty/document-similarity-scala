### Topic: Background on why closing chrome doesn't achieve the desired effect and how to manually close down individual processes or all chrome processes.
Background:
 Many security and application updates as well as troubleshooting steps require you to close all browsers. Often, 
restarting the browser doesn't achieve the expected fix or 
the installer will continue to complain that the browser needs to be closed.
The Issue: 
When you close your chrome browser window, you might think you have truly "closed" Chrome, but because a lot of plugins, apps and extensions run in separate processes (and can be duplicated if you run multiple chrome profiles),  you will likely still have a number of "chrome.exe" processes showing up in the Windows Task Manager "Processes" tab.  Power users can easily reach 30 or more of these open at any given time, and many of these launch automatically whether you have opened any Chrome browser windows or not.
Support agents should be aware of this, as recommendations to users close and restart chrome may not be accomplishing what you expect it to do.
Resolution
What to Do: 
The task manager 
for 
Both Windows and Chrome allow you to manually end these processes, but it is a very arduous task if you have to individually highlight each of  them, select "End Process" and confirm  upon closing. 
To kill one misbehaving plugin or window, Chrome's built-in task manager (accessed with Shift + Escape) is superior to Windows' task manager because it labels each app, extension, and browser window instead of showing a bunch of identically named "chrome.exe" processes.
Even Better: 
Below is a shortcut to definitively and quickly close all running Chrome processes so you can get a clean full restart.
Caveat:  Be aware that you could lose unsaved data within your browser or plugins when doing the following procedure.
Dropping to the Windows command  prompt and typing the following:
tskill chrome
will instantly kill all of these and allow you to relaunch chrome.
