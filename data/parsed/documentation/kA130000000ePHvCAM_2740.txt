### Topic: Work on Mobile Devices | Salesforce
Work on Mobile Devices | Salesforce
Work on Mobile Devices
Salesforce provides several mobile apps to keep you connected and productive, no matter where you are.
Salesforce Mobile Product Comparison
Salesforce1, SalesforceA, Salesforce Classic Mobile, and Salesforce Authenticator are available on different mobile devices and in different editions, with different support options.
Salesforce1 Mobile App
The Salesforce1 mobile app is Salesforce on the go! This enterprise-class mobile experience gives you real-time access to the same information you see in the office, but organized for getting work done between customer meetings, while waiting for a flight, even when you’re in line for coffee. The intuitive interface makes it easy to navigate and interact with data on a touchscreen, so you can review and update information with just a few taps. And Salesforce1 includes many of your org’s customizations, so the app is tailored to your business needs.
Salesforce Classic Mobile
The Salesforce Classic Mobile app provides mobile access to your Salesforce data from Android™ and iPhone® devices. You can view, create, edit, and delete records, keep track of your activities, view your dashboards, run simple reports, and log calls and emails. Your Salesforce admin must assign you a mobile license before you can use Salesforce Classic Mobile.
Salesforce Authenticator
Welcome to the all-new Salesforce Authenticator mobile app! Version 2 is a powerful and innovative two-factor authentication app for your mobile device. The app adds an extra layer of security to protect your Salesforce account and data. And we’ve designed it to be super easy to use. You get increased security and increased convenience—all in one beautiful app.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=mobile_products.htm&language=en_US
Release
202.14
