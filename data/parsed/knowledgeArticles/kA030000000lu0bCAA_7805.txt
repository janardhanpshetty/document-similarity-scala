### Topic: In very rare occasions the "total downloads" field and the total number of download records on the "file and content" report do not match due to the fact that an increment of the total downloads will never be undone if an error occurs later in the request.
I created a new "File and Content Downloads"/"File and Content Engagement" report and they are showing a value on the 
Total Downloads 
field.
The number of records that 
the "File and Content Downloads" report is showing is less than the 
Total Downloads field
. Why is this happening?
Resolution
If the report is showing all the records for that specific file (please review the report filters carefully), there might be, in some
 rare 
occasions a mismatch between the record count and the Total download field and this is due to the way that the download counting is designed. Salesforce cannot guarantee that the two numbers will match as an increment of the total downloads will never be undone if an error occurs later in the request, whereas the insert of a download audit event will be rolled back.
If that mismatch is occurring often, please create a case with support to get it investigated appropriately.
