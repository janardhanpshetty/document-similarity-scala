### Topic: This article helps how to search object name while updating the records on a Process Builder
While adding an 
action
 in a process builder to 
update a record
 related to the object you will see two options:
           1) Select the record that started your process.
           2) Select a record related to the Object.
When you select the second option and try searching for the related object by its object name
or API name, you won’t be able to find the object name if you have a rename 
Child Relationship Name 
field
 
 
Resolution
If you have changed the 
Child Relationship Name
 option on a 
Lookup field
 or a 
Master-Detail Relationship field
. Then on process builder you will be able to search with 
child relationship name
 and not with the object name or API name.
 
Below are the details to check the 
Child Relationship Name
 field option:
 
Click on Setup | Customize | Object | check for 
Lookup field or Master-Detail Relationship field
 name | Click on Label | you will see: 
Child Relationship Name field
.
 
Below is the example for Field Label as 
Account
 & Child Relationship Name as 
Example_Test
 :
The Child Relationship Name will show up on a 
Process Builder
 and not the original name for object or API name.
 
NOTE
: 
This will only apply if your Field Label is different from Child Relationship name field.
 
 
