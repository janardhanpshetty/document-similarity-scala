### Topic: Standard Lookups in Salesforce Classic | Salesforce
Standard Lookups in Salesforce Classic | Salesforce
Standard Lookups in Salesforce Classic
Standard lookups search through a limited set of searchable fields per object and users can’t refine search results.
Available in: Salesforce Classic
Available in: 
All 
Editions 
except Database.com
Availability
Standard lookups are available for most standard objects. All lookup fields use standard lookups by default.
Search Behavior
When you perform a blank lookup, a list of recently viewed records displays in the lookup search dialog.
Standard lookups query selected 
Name
 fields for objects.
A wildcard is automatically appended to each of your search terms. For example, a search for 
bob jo
 is really a search for 
bob*
 
jo*
 and returns items with 
bob jones
, 
bobby jones
, or 
bob johnson
.
Standard lookup queries match your search term against the org’s database (not the search index) without breaking up the term into separate tokens.
Search Results
Most standard lookups return up to 200 of the most relevant records and allow you to page through 50 records at a time. Lookup searches for campaigns only return the top 100 results. If you’ve gone through all the results and still don’t see the desired record, enter a more specific search term.
Results are ordered in alphabetical order by the primary record name field.
You can’t sort, filter, or customize the columns in the search results.
Salesforce only returns matches for a custom object if your admin has associated a custom tab with it. You don’t have to add the tab for display.
Note
Consider converting standard lookups into enhanced lookups for better search results. Admins enable enhanced lookups.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_lookupdialog_standard.htm&language=en_US
Release
202.14
