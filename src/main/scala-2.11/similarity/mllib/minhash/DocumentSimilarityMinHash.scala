package similarity.mllib.minhash

import org.apache.spark.sql.SparkSession

import scala.collection.immutable.HashSet
import scala.util.Random
import scala.util.hashing.MurmurHash3.stringHash

/**
 * spark-shell --master local[8] --driver-memory 6G
 * Created by jshetty on 7/28/16.
 *  Document similarity score using minHash technique on 1000 documents
 *  1. Create documents shingles
 *  2. Create minHash signature of the document
 *  3. Create jaccard similarity of each document v/s others
 *  4. Rank these according to jaccard similarity score
 *  5. Post-processing: Confirmation Step:
 *    Take candidate pairs shingle set and calculate jaccard distance for similarity score
 */
object DocumentSimilarityMinHash {

  def createShingles(words: Array[String]): Array[Array[String]] = {
    val shingleLength = 10
    val numShingles = words.length - shingleLength + 1
    val gram = Array.ofDim[String](numShingles, shingleLength)
    for (i <- 0 to numShingles - 1) {
      for (j <- 0 to shingleLength - 1) {
        gram(i)(j) = words(i + j)
      }
    }
    gram
  }

  def getMatchPercentage(set1: Array[Int], set2: Array[Int]): Double = {
    val set = HashSet() ++ set2
    var counter = 0;
    for (i <- 0 to set1.length - 1) {
      if (set.contains(set1(i))) {
        counter += 1
      }
    }
    val percent = (counter.toDouble / set1.length) * 100
    percent
  }

  // Jaccard distance between Documents using minHash
  def jaccard(set1: Array[Int], set2: Array[Int]): Float = {
    val lSet1 = HashSet() ++ set1
    val lSet2 = HashSet() ++ set2
    val percent = (1.0F * lSet1.intersect(lSet2).size / lSet1.union(lSet2).size) * 100
    percent
  }

  def main(args:Array[String]): Unit = {
    val spark = SparkSession.builder.master("local[8]").appName("Document Similarity using MinHash").getOrCreate()
    val sc = spark.sparkContext

    val numHashFun = 100
    val seed = 42
    var slope = Random.nextInt(1000)

    //val inputPairRDD = sc.wholeTextFiles("hdfs://localhost:9000/user/documentsimilarity/dataset/1000docs/*")
    //val inputPairRDD = sc.wholeTextFiles("hdfs://localhost:9000/user/documentsimilarity/dataset/testdocs/*")
    val inputPairRDD = sc.wholeTextFiles(args(0))

    // get only filename instead of whole path
    val inputPair = inputPairRDD.map((docPair :(String, String)) => (docPair._1.split("/").last, docPair._2))

    val shinglesRDD = inputPair.flatMapValues((docContent: String) => createShingles(docContent.split("\\W+"))).mapValues((wShingles: Array[String]) => wShingles.mkString(""))

    var resultRDD = shinglesRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).reduceByKey((hashValue1 :Int, hashValue2: Int) => math.min(hashValue1, hashValue2))

    // Get 100 Hash functions.
    for (i <- 1 to numHashFun) {
      slope = Random.nextInt(1000)
      resultRDD = resultRDD.union(shinglesRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).reduceByKey((hashValue1 :Int, hashValue2: Int) => math.min(hashValue1, hashValue2)))
    }
    // Don't do distinct
    val minHashesThousand = resultRDD.groupByKey().mapValues((hashValues: Iterable[Int]) => hashValues.toArray)

    // Calculate the getMatchPercentage foreach pair
    val similarityMatchPercentage = minHashesThousand.cartesian(minHashesThousand).map(docPair => (docPair._1._1, docPair._2._1, getMatchPercentage(docPair._1._2, docPair._2._2))).distinct
    // Jaccard distance Simply to verify getMatchPercentage both should be same
    val jaccardScore = minHashesThousand.cartesian(minHashesThousand).map(docPair => (docPair._1._1, docPair._2._1, jaccard(docPair._1._2, docPair._2._2))).distinct
    // Save only those pairs which are greater than 2%
    val jaccardPairs = jaccardScore.filter((tuple: (String,String,Float)) => (tuple._3 > 2.0 && tuple._3 < 100.0) )
    // MinHash Scores
    jaccardPairs.saveAsTextFile(args(1))

    //Post processing using all shingles hash function on shingles
    val result = shinglesRDD.mapValues((shingle:String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).groupByKey().mapValues((hashValues: Iterable[Int]) => hashValues.toArray)
    val jaccardConfirmationScore = result.cartesian(result).map(docPair => (docPair._1._1, docPair._2._1, jaccard(docPair._1._2, docPair._2._2))).distinct

    // Confirmation scores
    jaccardConfirmationScore.saveAsTextFile(args(2))

  }

}
