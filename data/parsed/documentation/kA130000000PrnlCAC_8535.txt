### Topic: Create Communities | Salesforce
Create Communities | Salesforce
Create Communities
Create communities using a wizard that helps you choose a community template that meets your business needs.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, customize, or activate a community:
“Create and Set Up Communities”
AND
Is a member of the community whose Community Management page they’re trying to access.
The number of communities you can create for your organization is listed on the All Communities page in Setup.
To start creating 
communities
, from Setup, enter 
Communities
 in the 
Quick Find
 box, select 
All Communities
, then click 
New Community
.
The Community Creation wizard appears, with different template options for you to choose from.
Hover over a template to see more information about it.
Kokua
A visually rich, responsive self-service template that lets users search for and view articles or contact support if they can’t find what they’re looking for. Supports Knowledge and Cases.
Koa
A text-based, responsive self-service template that lets users search for and view articles or contact support if they can’t find what they’re looking for. Supports Knowledge and Cases.
Napili
A powerful, responsive self-service template that lets users post questions to the community, search for and view articles, and contact support agents by creating cases. Supports Knowledge, Cases, and Questions & Answers.
Aloha
A configurable App Launcher template that lets users quickly find applications and access them using single sign-on authentication.
Salesforce Tabs + Visualforce
Standard Salesforce structure and tabs that you can customize using Visualforce. Supports most standard objects, custom objects and Salesforce1.
Hover over the template you want to use, and then click 
Choose
.
If you selected Koa or Kokua, specify the categories and Company Name for your template, then click 
Next
.
Category Group Name
This is the unique name of the data category group that contains the data categories for your site. The name reflects the hierarchy of categories that you’ve set up for your community and is used throughout the site to organize articles.
Top Level Category
This is the highest-level category that you want to display. Only the children of this category appear in the community. You can have several nested layers of categories above this category, but the page will show this category as the parent and show its subcategories as children. 
Company Name
This is the name of your company as you want it to appear in the community header.
Enter a community name.
Note
If you’re creating multiple communities, keep in mind that community names may be truncated in the global header drop-down menu. Users can see up to 32 characters of the name, and the 
Preview
 and 
Inactive
 status indicators count toward that number. Make sure that the visible part of the name is distinctive enough for users to distinguish between multiple communities.
Enter a unique value at the end of the URL field.
This value is appended to the domain you entered when 
enabling communities
 to create a unique URL for this community. For example, if your domain is 
UniversalTelco.force.com
 and you’re creating a customer community, you can designate the URL as 
UniversalTelco.force.com/customers
.
Note
You can create one community in your organization that doesn’t have a custom URL.
You can change your community name and URL after the community is activated, but users won’t be redirected to the new URL. If these changes are necessary, be sure to inform your community members before making the change.
Click 
Create Community
.
The community is created in 
Preview
 status.
On the confirmation page, click 
Go to Community Management
 to customize your community. If you chose a template other than Salesforce Tabs + Visualforce, you can customize your community in Community Builder.
When you create a community, default pages for login, self-registration, change password, forgot password, and your home page are set based on your community template. You can customize or change these default pages at any time in Community Management.
Important
When you create a community, your profile is automatically added to the list of profiles that have access. As a result, all users in your organization with this profile can log in to the community once it’s 
Active
. If you don’t want all users with your profile to have access, you can remove the profile and give yourself access through a different profile or permission set.
See Also:
Salesforce Communities Overview
Customize Communities
How many communities can my organization have?
Compare Features Available in the Community Templates
Implementation Guide: Using Templates to Build Communities
Implementation Guide: Getting Started with the Aloha Community Template for Salesforce Identity
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_creating.htm&language=en_US
Release
202.14
