### Topic: Where can I find the Microsoft CRM 4.0 connector log file?
Where can I find the Microsoft CRM 4.0 connector log file?
Resolution
The log file is located on the server running Microsoft CRM 4.0. You will need to have access to this server to retrieve these files. Most often, the log file is stored at: 
C:\Program
 Files\ExactTarget\Connector\Logs\ 
You can also find this when logged into Microsoft CRM 4.0. In the Menu Bar, go to 
ExactTarget | About
. In Settings, go to
 Log File Path. 
This
 
is where the connector log file is stored for this instance of Microsoft CRM 4.0. 
 
 
