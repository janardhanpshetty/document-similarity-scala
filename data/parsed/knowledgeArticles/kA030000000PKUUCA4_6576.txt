### Topic: This article explains that for an Activity to be Related to a Campaign the Campaign first has to be Active. It also explains how to make Campaigns Active from the Detail.
When creating Activities, if a user is unable to relate it to a campaign, please ensure the following that the Campaign is marked as 
active
.
Resolution
To activate a Campaign, you can follow this click path:
Campaigns tab
 | 
Select a Campaign
 | 
Edit
 | Check the "
Active
" checkbox | 
Save
Once the Active box has been checked, the Campaign should be available to relate to an Activity and searchable via Lookup Dialogs.
