### Topic: Control Access to Salesforce Objects and Fields | Salesforce
Control Access to Salesforce Objects and Fields | Salesforce
Control Access to Salesforce Objects and Fields
Wave Analytics requires access to Salesforce data when extracting the data and also when the data is used as part of row-level security. Configure the permissions of the Integration User on Salesforce objects and fields to control the dataflow’s access to Salesforce data. Configure the permissions of the Security User to enable row-level security based on custom fields of the User object.
User Permissions Needed
To clone a user profile:
“Manage Profiles and Permission Sets”
To edit object permissions:
“Manage Profiles and Permission Sets”
AND
“Customize Application”
When configuring permissions for the Integration User or Security User, make changes to a cloned version of the user profile.
From Setup, enter 
Profiles
 in the 
Quick Find
 box, then select 
Profiles
, and then select the user profile.
For the Integration User, select the Analytics Cloud Integration User profile. For the Security User, select the Analytics Cloud Security User profile.
Click 
Clone
 to clone the user profile.
Name and save the cloned user profile.
Click 
Object Settings
.
Click the name of the Salesforce object.
Click 
Edit
.
To enable permission on the object, select 
Read
 in the Object Permissions section.
To enable permission on a field of the object, select 
Read
 for the field in the Field Permissions section.
Note
You can’t change the permissions on standard fields of the User object.
Save the object settings.
Assign the cloned user profile to the Integration User or Security User.
From Setup, enter 
Users
 in the 
Quick Find
 box, then select 
Users
.
Select the user to which you want to assign the user profile.
Click 
Edit
.
In the Profile field, select the user profile.
Click 
Save
.
Verify that the Integration User or Security User has the right permissions on fields of the objects.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_salesforce_object_field_levels_control_access.htm&language=en_US
Release
202.14
