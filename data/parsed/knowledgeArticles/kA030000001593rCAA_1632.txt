### Topic: What this Error ID means and what to do when you get a case about it: Error ID:1103093356-659911 (-102518988)
Customer has On-Demand E2C setup and the Automated Case User receives an error email with following content and StackTraceId:
Subject:
Email-to-Case: Error(s) encountered while processing
Body: 
An unexpected error o
ccurred while processing an incoming email. Please contact salesforce.com customer support with the error ID below:
Error ID : 1103093356-659911 (-102518988). 
 
Resolution
This gack or StackTraceId is most likely related to a known connection pool timeout issue at the database level on our end. This timeout error could prevent E2C email from being processed. If Splunk shows below log lines around the time of the issue, then this is very much related:
ORACLE.ConnectionPoolTimeOutException -> Unexpected exception during E2C processing at common.util.database.ConnectionPool2.getConnection(ConnectionPool2.java:563)`common.apex.methods.SupportStaticMethods`sendExceptionNotification`RATEOF-BLOC
K 
We have a User Story logged to address this issue in future (Summer'16 - safe harbor). Please review following GUS User Story and attach it to any related cases:
W-2760523 - https://gus.my.salesforce.com/apex/adm_userstorydetail?id=a07B0000001abDZ&sfdc.override=1
