### Topic: Flow Properties | Salesforce
Flow Properties | Salesforce
Flow Properties
A flow’s properties consist of its name, description, interview label, and type. These properties drive the field values that appear on a flow or flow version’s detail page. The properties of a flow and its flow versions are separate.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Tip
The properties for a given flow’s versions automatically match the active version’s properties by default. In other words, if you have three versions and you activate version 2, Salesforce updates the properties for versions 1 and 3 to match version 2. However, if you edit the properties for an inactive version, that version’s properties are no longer automatically updated to match the active version.
From the Cloud Flow Designer, click 
 to update the properties for a flow or a flow version.
Property
Description
Name
The name for the flow or flow version. The name appears in the flow management page and flow detail page. It also appears in the run time user interface.
You can edit the name for inactive flows and flow versions.
Unique Name
The unique name for the flow. The unique name is used to refer to this flow from other parts of Salesforce, such as in a URL or Visualforce page. A unique name is limited to underscores and alphanumeric characters. It must begin with a letter, not include spaces, not end with an underscore, and not contain two consecutive underscores. The unique name appears on the flow detail page.
You can’t edit the unique name after the flow has been saved.
Description
The description for the flow or flow version. The description appears in the flow management page and flow detail page.
You can edit the description for inactive flows and flow versions.
Type
The type for the flow or flow version. The type appears in the flow management page and flow detail page. It determines which elements and resources are supported in the flow or flow version, as well as the ways that the flow can be implemented. For details, see 
Flow Types
.
If the type is Login Flow, you can’t update the type after the flow has been saved.
Interview Label
The label for the flow’s interviews. An 
interview
 is a running instance of a flow. This label appears in:
The Paused and Waiting Interviews list on the flow management page
The Paused Interviews component on the Home tab
The Paused Interviews item in Salesforce1
You can edit the interview label for inactive flows and flow versions. By default, the interview label contains the flow name and the 
{!$Flow.CurrentDateTime}
 system variable.
Use a text template to reference multiple resources in the label. For example, 
Flow Name
 - {!Account.Name} - {!$Flow.CurrentDateTime}.
See Also:
Save a Flow
Flow and Flow Version Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_admin_flow_properties.htm&language=en_US
Release
202.14
