### Topic: Encourage Idea Creation and Sharing in Salesforce Communities | Salesforce
Encourage Idea Creation and Sharing in Salesforce Communities | Salesforce
Encourage Idea Creation and Sharing in Salesforce Communities
Add your Ideas users to Salesforce Communities to take advantage of new ways to collaborate.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To customize Ideas settings for Salesforce Communities:
“Customize Application”
Create more engagement and collaboration around Ideas as you enable your customers to post and comment on Ideas right from their Salesforce Communities home page. 
Adding Ideas to Salesforce Communities lets your users reap the benefits of a vibrant, creative partnership between community members. 
Communities are customizable, public or private spaces for employees, customers, and partners to collaborate on best practices and business processes. 
When you enable Ideas in Salesforce Communities, you give your community members the ability to create ideas and idea themes and have a dialog around them. You can create public communities that let your customers or partners exchange ideas, as well as private internal communities that are specific to your employees.
Moderating and managing ideation communities can be assigned to internal community members, depending on their privileges. Internal users can moderate both internal and external communities because they have access to internal communities as well as any public communities that they have permission to access.
To organize your community into smaller groups, you can create zones within a community that reflect special interests, product groupings, or types of customers. Zones are shared by the Ideas, Answers, and Chatter Answers applications, allowing you to view and create zones from those locations. For example, if you're a computer manufacturer you can create a community named Laptop Products and another named Desktop Products. Within each of those communities, you can create zones that are specific to different aspects of the products.
Community members have visibility into different zones based on their user profiles:
Community users see the zones associated with the community they’re signed in to.
Internal users with permission to see Ideas can see all internal-only zones in the organization. If internal users sign in to a community, they see only those zones associated with that community.
Internal users with permission to see Chatter Answers can see all internal-only zones for the organization in the Q&A tab. If internal users sign in to a community, they see only those zones associated with that community.
Portal users can see the zones associated with their portal.
Portal users with access to both a portal and a community can see the zones associated with the portal or community that they are currently signed in to.
Users who are accessing the portal or community through an API can access all zones that they have access to in all contexts.
Global searches in the internal application performed by internal users return results from all ideas that are available within the organization. Searches performed by all other users in Salesforce Communities return results from the ideas that are available within the community.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=ideas_salesforce_communities.htm&language=en_US
Release
202.14
