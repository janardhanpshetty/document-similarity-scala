### Topic: Create Agent Console Layouts | Salesforce
Create Agent Console Layouts | Salesforce
Create Agent Console Layouts
Agent console available in Salesforce Classic. Setup for Agent console available in Salesforce Classic and Lightning Experience.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create console layouts:
“Customize Application”
Note
As of the Spring ’15 release, Agent console is not available to new organizations.
The newer Salesforce console improves the Agent console by providing you with more options and more advanced technologies. See 
Salesforce Console
.
To create a layout for the Agent console:
From Setup, enter 
Console Layouts
 in the 
Quick Find
 box, then select 
Console Layouts
.
Click 
New
 and optionally choose an existing layout to clone.
Enter a name for the new layout.
Click 
Save
.
Click 
Edit
 in the Selected List Views section.
To add or remove objects to the layout, select an object, and click the 
Add
 or 
Remove
 arrow.
To change the order of the objects as they appear in the console's list view frame, select an object in the Selected List box, and click the 
Up
 or 
Down
 arrow.
A user can only view objects in the console's list view frame if those objects are added to the console layout to which their profile is assigned.
Click 
Save
.
Next, 
choose the related objects
 to show in the mini view of the console.
See Also:
Managing Console Layouts for the Agent Console
Help Site URL
Release
202.14
