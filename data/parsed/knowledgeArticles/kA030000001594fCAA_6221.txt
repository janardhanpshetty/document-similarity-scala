### Topic: SFO login wizard fails to complete after user clicks on the Allow button
The following error is displayed during the Salesforce for Outlook wizard, after user enters their credential and clicks on the 
Allow
 button
Error:
You don't have permission to view Salesforce records. Contact your administrator
 
 
Root Cause
Salesforce for Outlook starts checking and verifying access for the user to the Contact, Events and the Task objects when the Allow button is clicked and if there are any of the required fields hidden for the user the error above may show up on the screen.
 
Resolution
You can try the following steps to address this issue
 
- Login to the affected org as a Salesforce Administrator an go to
 Setup | Customize | Contacts | Fields
- Click on the following field one at a time and then click on the 
Set Field-Level Security
 button on top and make sure the box for the affected user profile is checked, if it is not checked (meaning its hidden for that profile) make sure to check it and click on Save
Account Name
Email
Last Name
**There may be other fields that would cause this issue so you may need to check the rest of the standard required fields.
- Next Exit SFO and restart it
- Try to reproduce the issue, if the issue is still reproducible try the steps above for the Events and Tasks fields as well
  
Setup | Customize | Activities 
and try the same for Events and Tasks
 
