### Topic: Field Update Actions | Salesforce
Field Update Actions | Salesforce
Field Update Actions
Field update actions let you automatically update a field value. You can associate field updates with workflow rules, approval processes, or entitlement processes.
Available in: Lightning Experience and Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
From Setup, enter 
Field Updates
 in the 
Update
 box, and select 
Field Updates
. Then use these settings to configure your field update.
Before you begin, check the type of the field you want to update. Read-only fields like formula or auto-number fields are not available for field updates.
Field
Description
Name
Enter a name for this field update.
Unique Name
Enter a unique name to refer to this component in the API. The requirement for uniqueness is only within the selected object type. You can have field updates of the same type with the same unique name, provided they are defined in different objects.The 
Unique Name
 field can contain only underscores and alphanumeric characters. It must be unique within the selected object type, begin with a letter, not include spaces, not end with an underscore, and not contain two consecutive underscores.
Description
Enter a description for the field update.
Object
Select the object whose field you want to update.
Field to Update
Select the field to update. Fields are shown only for the object that you selected. You can select a field on a related object in a master-detail relationship.
You can use field updates on encrypted custom fields, but the encrypted field isn't available in the formula editor.
Tip
Avoid associating more than one field update with a rule or approval process that applies different values to the same field.
Re-evaluate Workflow Rules After Field Change
Select if you want workflow rules on this object to be re-evaluated after the field value is updated. If you select this option, Salesforce re-evaluates all workflow rules on the object if the field update results in a change to the value of the field, triggering any workflow rules whose criteria are met. For more information, see 
Field Updates That Re-evaluate Workflow Rules
.
Specify New Field Value
The value that the field should be updated with. The available options depend on the type of field you are updating. For more information, see 
Value Options for Field Update Actions
 .
See Also:
Associate Actions with Workflow Rules or Approval Processes
Value Options for Field Update Actions
Cross-Object Field Updates
Field Updates That Re-evaluate Workflow Rules
Considerations for Field Update Actions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workflow_managing_field_updates.htm&language=en_US
Release
202.14
