### Topic: Create a Report on Your Notes | Salesforce
Create a Report on Your Notes | Salesforce
Create a Report on Your Notes
View and analyze details about all your notes using reports. You can only report on notes taken with Notes, our enhanced note-taking tool.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, edit, and delete reports:
“Create and Customize Reports”
AND
“Report Builder”
Make sure you’re familiar with standard reports and how to customize them.
Create a new report with the following criteria.
Report type
File and Content Report
Fields
File Name
Published Date
Created By
Last Revised Date
Last Revised By
File Type
Filters
File Type
 equals 
SNOTE
Be sure to save your report so you can run it again later.
If you’d like to share the report with others, create a report folder and save the report to it. Then, use the sharing settings for the folder to add individuals or groups of users.
See Also:
Notes
Build a Report
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=notes_create_report.htm&language=en_US
Release
202.14
