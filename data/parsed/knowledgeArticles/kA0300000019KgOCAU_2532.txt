### Topic: Details on why #first_name no longer works.
Why is #first_name no longer working?
Resolution
Salesforce's Winter '16 release includes the latest version of feedback and performance summaries.  With the release you gained a handful of new features, but due to technical reasons #first_name is no longer supported.  If you are in the middle of a performance cycle and need to update questions to remove references of #first_name please follow these steps.
1. Go to the 
Performance Cycles
 tab and select your performance cycle.
2. Copy the id of the performance cycle from your web browser's location bar.  As an example, your web browser's location bar may display something like this: https://na1.salesforce.com/0W7B00000001fQ8
The Id of the performance cycle would be 
0W7B00000001fQ8
1. Login to 
Workbench
 as an Admin using your Salesforce credentials
2. Select 
Production
 from the 
Environment
 picklist, 34.0 from the 
API Version
 picklist, agree to the terms and click the 
Login with Salesforce 
button.
3. Select 
data | Update
 from the top menu.
4. For the 
Object Type
 picklist select 
WorkPerformanceCycle
5. Paste the performance cycle Id in the text box
6. Click 
Next
7. Change the State field from 
InProgress
 to 
Setup
8. Click 
Confirm Update
9. Go back into Salesforce and edit all the questions that contain #first_name
10. Repeat steps 1-6
11. 
Change the State field from 
Setup
 to 
InProgress
