### Topic: Roles Per Partner Portal Account | Salesforce
Roles Per Partner Portal Account | Salesforce
Roles Per Partner Portal Account
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set the number of roles per portal account:
“Customize Application”
Note
Starting in Summer ’13, the partner portal is no longer available for organizations that aren’t currently using it. Existing organizations continue to have full access. If you don’t have a partner portal, but want to easily share records and information with your partners, try Communities.
Existing organizations using partner portals may continue to use their partner portals or transition to Communities. Contact your Salesforce Account Executive for more information.
You can set the default number of roles for partner portal accounts. This benefits your partner portal by reducing the number of unused roles.
 You can set up to three roles; the system default is three.
For example, if you currently have three roles created when an account is enabled for your partner portal, but only need one role for new accounts, you can reduce the number of roles to one.
To set the number of roles per partner portal account:
From Setup, enter 
Partners
 in the 
Quick Find
 box, then select 
Settings
.
Click 
Set Portal Role and User Defaults
.
In the Portal Role and User Defaults page, click 
Edit
.
In the 
Number of Roles
 drop-down list, set your default number of roles per partner portal account.
Click 
Save
.
The number of roles for existing portal accounts doesn't change with this setting.
See Also:
Partner Portal Overview
Partner Portal Limits
Partner Portal Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=partner_portal_account_role.htm&language=en_US
Release
202.14
