### Topic: This article describes the behavior of the delete action within Workbench when attempting to delete records from the PartnerNetworkRecordConnection object.
When deleting records from the PartnerNetworkRecordConnection object the Operation Is Not Allowed INVALID_OPERATION error is presented. 
Resolution
Only records in the following status' can be deleted from the 
PartnerNetworkRecordConnection object:
Active (received)
Pending (sent)
Records in the following status' cannot be deleted:
Inactive
Inactive (converted)
Inactive (deleted)
Note: The 
PartnerNetworkRecordConnection object is only accessible through Workbench (
https://workbench.developerforce.com/) and is not accessible through Data Loader. 
More information on the 
PartnerNetworkRecordConnection object
: 
https://www.salesforce.com/developer/docs/api/Content/sforce_api_objects_partnernetworkrecordconnection.htm
