### Topic: Resolve Process-Level Access Errors | Salesforce
Resolve Process-Level Access Errors | Salesforce
Resolve Process-Level Access Errors
Insufficient Privileges errors might be caused by a validation rule.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All Editions
User Permissions Needed
To view and change validation rules:
”View Setup and Configuration”
AND
”Customize Application”
To view and define Apex triggers:
“Author Apex”
To resolve Insufficient Privileges errors, you would typically determine if they are caused by misconfigured permission sets, profiles , or sharing settings. Otherwise, you might want to review your organization’s validation rules.
Review your validation rules. 
A validation rule might be preventing the user from completing a task, such as transferring a case record after it’s closed.
From your object management settings, find the object that you want to check, and then scroll down to Validation Rules.
Verify that none of the validation rules are causing the error. Or fix the validation rule if the user must gain access through it.
See Also:
Resolving Insufficient Privileges Errors
Define Validation Rules
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_insufficient_privileges_process.htm&language=en_US
Release
202.14
