### Topic: Learn how to create a "New Promoted Only Tweet" in Social.com.
You'll need to be granted this level of access within Twitter. If you don't have the proper access, an Account Administrator will need to provide you with the User permission, as you cannot grant permissions to yourself. Once completed, simply sync the Ad Account in Social.com. Learn how below. 
 
Resolution
Assign the permission
Note
: If you don't already, add a second Twitter User with Account Admin permissions. If you already have another User with Account Admin permissions, skip to step 2. 
 
1. Create a second user with Account Admin permissions within Twitter. 
2. Log into Twitter Ads as the second Account Admin, and then go to 
Edit Access to Account.
3. Give the original user (yourself) "Promoted Only Tweet"
 
Permission.  
4. Click 
Save.
Sync the Twitter Ad Account 
 
1. Click 
Administration.
2. Click 
Twitter.
3. Click on the 
Account ID
 Hyperlink.
4. Click the 
Synchronize
 button.
5. Click 
Synchronize Now
.
