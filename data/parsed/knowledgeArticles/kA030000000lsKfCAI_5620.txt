### Topic: This article will go over the limitation and the Idea on Idea Exchange for this issue.
We recently have enabled the Create Audit Field Feature and are attempting to Import Opportunities and Opportunity History from our old Salesforce org and from another CRM.  As we are trying to consolidate our information in a higher SFDC Edition than our old org.
Resolution
At this time, we do not have an effective way to import opportunity stage history using the Apex Data Loader.
There is an Idea on the Idea Exchange that discusses this issue further please click on the link below if you wish to vote.
https://success.salesforce.com/ideaView?id=08730000000BqqSAAS
You may also visit the AppExchange for packages that could work as alternatives.
