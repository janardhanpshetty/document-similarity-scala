### Topic: A user that has the same profile as other users, but certain fields and components of the account page layout are not displaying for him. Specifically, for example, the "System Information" section All other users with the same profile see this information.
A user that has the same profile as other users, but certain fields and components of the account page layout are not displaying for him. Specifically, for example, the "System Information" section All other users with the same profile see this information.
Resolution
Since both users are using the same profile Field Level Security is the same. Layout section might have been closed.
Ask user to do the following:
1. Open the record.
2. On the Section Header click the drop down icon to open hidden section details.
Fields under that section should now be visible.
You may also try solution found in 
Knowledge Article Number: 000004565 : 
New Page Layout is missing fields
