### Topic: This article explains how the count of number of times a picklist value is used in a record. This field can be used for Reporting purposes.
How to Count the number of times a Picklist Value has been used on a particular record
Example Picklist Value - Opportunity Stage, Case Status values
Resolution
To achieve this requirement a field has be created on the object and a workflow rule has to be created to updated the count in this field.
Create a Number field on the Object (Example Label - "Num")
Create a workflow on Object
Set the Evaluation Criteria as "Evaluate the rule when a record is "
created, and any time it’s edited to subsequently meet criteria
"
Rule Criteria : "Picklist Field" equals "Picklist Value"
In Workflow Action Choose Field Update
Select the field "Num" field created in Step 1
Update the field using the formula: IF(ISBLANK(PRIORVALUE(Num_c)), 1, PRIORVALUE(Num_c) + 1)
Activate the Workflow
Once done, any time the picklist value is selected and the record is saved the Count gets incremented by 1 every time
Example use cases
A support organization can be interested in finding out how many times a particular case has been reopened. This can be helpful to determine the quality of resolution offered to clients. In that case we can use the above solution to meet the requirement.
A sales Organization might be interested in finding out how many times "Preparing Quote" is used in a sales process. For example a particular opportunity can possibly move from "Preparing Quote" to "Negotiation" and back to "Preparing Quote" and so on before winning the deal. In such instances the information on how many deals had more number of "Preparing quote" can be a potential improvement for the Sales organization.
Note: Both of the above examples are hypothetical and only to give a guidance on how the above solution can be put into action
