### Topic: Learn how Facebook Lookalike Audiences are created through Active Audiences in the Marketing Cloud.
When a Facebook custom audience is created via 
Active Audiences in the Marketing Cloud
, it's possible to have Facebook also create a "Lookalike Audience." Learn all about 
Facebook Lookalike Audiences
 below. 
Resolution
 
How Facebook creates Lookalike Audiences
 
In order for the Lookalike Audience to be created, the source audience must have a matched count of at least 100 or the Lookalike Audience will fail.  
Facebook evaluates profiles for matched audience members with their own proprietary algorithm, and based on the evaluation, Facebook populates the Lookalike Audience with other Facebook users that are considered similar to the original audience members. 
Lookalike Audiences can be optimized for reach or similarity. Optimizing for reach will return the top 5% of users with similar interests whereas optimizing for similarity will return the top 1% of users, those most similar to the audience members. 
Note: 
Facebook Lookalike Audiences are not created until after the source audience is created and it will not show as "available" when the initial audience activation completes. There will be a delay to allow Facebook to process the activation. 
