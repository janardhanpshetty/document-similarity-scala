### Topic: Just-in-Time Provisioning Requirements | Salesforce
Just-in-Time Provisioning Requirements | Salesforce
Just-in-Time Provisioning Requirements
Just-in-Time provisioning requires the creation of a SAML assertion. Consider the following when creating your SAML assertion.
Provision Version
 is supported as an optional attribute. If it isn't specified, the default is 1.0. For example:
<saml:Attribute Name="ProvisionVersion" NameFormat=
   "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
      <saml:AttributeValue xsi:type="xs:anyType">1.0</saml:AttributeValue>
</saml:Attribute>
ProfileIDs change per organization, even for standard profiles. To make it easier to find the profile name, Salesforce allows you to do a profile name lookup by passing the 
ProfileName
 into the 
ProfileId
 field.
Field Requirements for the SAML Assertion
To correctly identify which object to create in Salesforce, you must use the 
User.
 prefix for all fields passed in the SAML assertion. In this example, the 
User.
 prefix has been added to the 
Username
 field name.
<saml:Attribute 
   Name="User.Username" 
   NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
      <saml:AttributeValue xsi:type="xs:anyType">testuser@123.org</saml:AttributeValue>
</saml:Attribute>
The following standard fields are supported.
Fields
Required
Comments
AboutMe
Alias
If not present, a default is derived from FirstName and LastName.
CallCenter
City
CommunityNickname
If not present, a default is derived from the UserName.
CompanyName
Country
DefaultCurrencyIsoCode
Derived from organization settings.
DelegatedApproverId
Department
Division
Email
Y
For example, 
User.Email=test2@salesforce.com
EmailEncodingKey
If not present, a default is derived from the organization settings.
EmployeeNumber
Extension
Fax
FederationIdentifier
 (insert only)
If present, it must match the SAML subject, or the SAML subject is taken instead. Can't be updated with SAML.
FirstName
ForecastEnabled
IsActive
LastName
Y
LanguageLocaleKey
LocaleSidKey
If not present, a default is derived from the organization settings.
Manager
MobilePhone
Phone
ProfileId
Y
For example, 
User.ProfileId=Standard User
ReceivesAdminInfoEmails
ReceivesInfoEmails
State
Street
TimeZoneSidKey
If not present, a default is derived from the organization settings.
Title
Username
 (insert only)
Y
For example, 
User.Username=test2@test.com
. Can't update using SAML.
UserRoleId
Defaults to “no role” if blank.
Zip
Other field requirements:
Only text type custom fields are supported.
Only the 
insert
 and 
update
 functions are supported for custom fields.
When using the API for user creation, you can pass the new username into the 
User.Username
 field. You can also specify the 
User.FederationIdentifier
 if it is present. However, the 
Username
 and 
FederationIdentifier
 fields can't be updated with API.
See Also:
About Just-in-Time Provisioning for SAML
Just-in-Time Provisioning for Portals
Just-in-Time Provisioning for Communities
salesforce_communities_implementation.pdf
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sso_jit_requirements.htm&language=en_US
Release
202.14
