### Topic: Extracting Incremental Changes to Data (Pilot) | Salesforce
Extracting Incremental Changes to Data (Pilot) | Salesforce
Extracting Incremental Changes to Data (Pilot)
You can configure the sfdcDigest transformation to extract only records that changed in a Salesforce object since the last dataflow run. Use incremental extraction to decrease the time required to extract the data.
Note
Incremental extraction with the sfdcDigest transformation is currently available through a pilot program. Any unreleased services or features referenced in this or other press releases or public statements are not currently available and may not be delivered on time or at all. Customers who purchase our services should make their purchase decisions based upon features that are currently available.
There are two types of extractions: full and incremental. With a full extraction, the sfdcDigest transformation extracts all records from the Salesforce object. With incremental extraction, the transformation extracts only records that have changed since the last dataflow run. Unless configured otherwise, the sfdcDigest transformation runs full extractions.
When you enable incremental extraction, the sfdcDigest transformation performs different actions on the dataset based the types of record changes in the Salesforce object since the last dataflow run:
Record Change in Salesforce Object
Change to Dataset
Deleted record
Deletes corresponding record in the dataset.
New record
Inserts the new record into the dataset.
Updated record
Updates the corresponding record in the dataset.
Consider the following guidelines when enabling incremental extraction for an sfdcDigest transformation:
Use incremental extraction for as many instances of the sfdcDigest transformation in the dataflow as possible.
If necessary, when the sfdcDigest transformation is configured to run an incremental extraction, you can use the 
fullRefreshToken
 parameter to run a one-time, full extraction. You might use this option if the data in the Salesforce object and dataset become out of sync.
The sfdcDigest transformation extracts all records from the object if you enable incremental extraction and it’s the first time running the dataflow.
You must use a full extraction in the following cases because incremental extraction is not supported:
The Salesforce object contains a formula field that references an external data object.
The Salesforce object fields or field types changed since the last dataflow run.
The filter in the sfdcDigest transformation changed since the last dataflow run.
The Salesforce object is one of the following objects:
CallCenter
CaseTeamMember
CategoryNode
CollaborationGroup
CollaborationGroupMember
CollaborationGroupMemberRequest
Division
Domain
DomainSite
Group
GroupMember
Profile
Site
Territory
Topic
User
UserRole
UserTerritory
Example
Let’s consider an example. You’d like to incrementally extract data from the Opportunity object. As a result, you configure the following dataflow definition:
{    
   "Extract_Opportunities": {        
      "action": "sfdcDigest",        
      "parameters": {            
         "object": "Opportunity",
         
"incremental": true
,
         "fields": [                
            { "name": "Id" },                
            { "name": "Name" },
            { "name": "StageName" },                
            { "name": "CloseDate" },
            { "name": "AccountId" },
            { "name": "OwnerId" }         
         ]  
      }    
   },    
   "Register_Opportunities_Dataset": {        
      "action": "sfdcRegister",        
      "parameters": {            
         "alias": "Opportunities",            
         "name": "Opportunities",            
         "source": "Extract_Opportunities"        
      }    
   }
}
Later, you realize that one of the opportunity records is missing. You decide to run a one-time full extraction by adding the 
fullRefreshToken
 parameter as shown in the following dataflow definition.
{    
   "Extract_Opportunities": {        
      "action": "sfdcDigest",        
      "parameters": {            
         "object": "Opportunity",
         "incremental": true,
         
"fullRefreshToken": "bb"
,
         "fields": [                
            { "name": "Id" },                
            { "name": "Name" },
            { "name": "StageName" },                
            { "name": "CloseDate" },
            { "name": "AccountId" },
            { "name": "OwnerId" }         
         ]  
      }    
   },    
   "Register_Opportunities_Dataset": {        
      "action": "sfdcRegister",        
      "parameters": {            
         "alias": "Opportunities",            
         "name": "Opportunities",            
         "source": "Extract_Opportunities"        
      }    
   }
}
After the full extraction, the dataflow performs an incremental extraction each time thereafter even though the 
fullRefreshToken
 parameter is included in the dataflow definition. To run a full extraction again, change the value of the 
fullRefreshToken
 parameter to a different value.
See Also:
sfdcDigest Transformation
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_integrate_salesforce_extract_transformation_incremental_extraction.htm&language=en_US
Release
202.14
