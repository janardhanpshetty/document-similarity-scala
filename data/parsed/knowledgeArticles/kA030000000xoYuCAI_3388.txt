### Topic: After users log in and authorize an application to access their Salesforce user data, they aren’t prompted to log in again when they authorize other applications. This behavior applies to single sign-on using OpenID Connect (or OAuth, if a user logs in to another Salesforce app).
After users log in and authorize an application to access their Salesforce user data, they aren’t prompted to log in again when they authorize other applications. This behavior applies to single sign-on using OpenID Connect (or OAuth, if a user logs in to another Salesforce app).
See 
Log In Once for Concurrent Apps That Use OpenID Connect
 for more information. 
Resolution
