### Topic: Custom Permissions Detail Page | Salesforce
Custom Permissions Detail Page | Salesforce
Custom Permissions Detail Page
Custom permissions provide access to custom processes or apps.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
In Group and Professional Edition organizations, you can’t create or edit custom permissions, but you can install them as part of a managed package.
User Permissions Needed
To view custom permissions:
“View Setup and Configuration”
On the Custom Permission detail page you can:
Change the custom permission details by clicking 
Edit
.
Remove the custom permission by clicking 
Delete
.
Create a custom permission based on the current one by clicking 
Clone
.
Add or remove required custom permissions by clicking 
Edit
 in the Required Custom Permissions related list.
See Also:
Custom Permissions
Create Custom Permissions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=custom_perms_detail.htm&language=en_US
Release
202.14
