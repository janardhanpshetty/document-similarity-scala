### Topic: Logging In to the SoftPhone | Salesforce
Logging In to the SoftPhone | Salesforce
Logging In to the SoftPhone
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To log in to Salesforce CRM Call Center:
Member of a call center
Salesforce CRM Call Center
 requires you to first log in to Salesforce and then to log in separately to your organization's phone system. The phone system login is located in the sidebar and only appears if you are:
Assigned to a call center in Salesforce
Using a machine on which an appropriate CTI adapter is installed. (You can quickly verify that an adapter is installed by looking for the
 icon in the system tray of your computer. The system tray is located next to the clock in the lower-right corner of your monitor.)
If a phone system login does not appear, contact your administrator.
Depending on the phone system that your organization uses, the login prompts you to enter your user ID, password, and other credentials. Once you have entered these values, click 
Log In
 to complete the connection to your phone system.
To automatically log in to your phone system without having to click the 
Log In
 button:
From your personal settings, enter 
SoftPhone
 in the 
Quick Find
 box, then select 
My SoftPhone Settings
.
Select 
Automatically log in to your call center when logging into Salesforce
. Once you have logged in to your phone system, Salesforce remembers your login information and automatically makes a connection to your phone system whenever you log in to Salesforce.
Note
If you explicitly log out of the phone system at any time while you are logged in to Salesforce, automatic log in is turned off for the remainder of your Salesforce session. To reenable automatic log in, log out of Salesforce and then log back in.
After logging in to a phone system, your call center state is automatically set to Not Ready for Calls. If you want to receive calls, you must 
change your call center state
 to Ready for Calls.
Tip
If you're using a CTI adapter built with version 4.0 of the 
CTI Toolkit
, you'll be automatically logged out of the SoftPhone after 8 hours if you have one browser tab open and go to an external Web page, or to a Salesforce page without a sidebar (such as a dashboard). Automatic-logout times can vary because they're based on how your administrator has configured your SoftPhone. To stay logged in, make sure you have at least one browser tab open to a Salesforce page with a sidebar, such as Home or Cases. Automatic logout doesn't occur in the Salesforce console.
See Also:
Using the SoftPhone
Checking the Version of Your CTI Adapter
Salesforce CRM Call Center System Requirements
Tip sheet: Getting Started with your SoftPhone
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cti_login.htm&language=en_US
Release
202.14
