### Topic: Sample trigger code that gives edit access of every badge created to a specific group
Sample trigger to grant badge edit permissions to a specific group.
Resolution
DO NOT SHARE WITH CUSTOMER WITHOUT TESTING THE CODE.  LET THE CUSTOMER KNOW THIS IS SAMPLE CODE, SO IT SHOULD ONLY BE USED TO GUIDE THEM IN THEIR DEVELOPMENT.
 
trigger AddEditBadgePerm on WorkBadgeDefinition (after insert) {

    List<WorkBadgeDefinitionShare> shareList = new List<WorkBadgeDefinitionShare>();

    for (WorkBadgeDefinition badge : Trigger.New) {
        // UserOrGroupId can be the Salesforce Id of a single user or the a Public Group
        WorkBadgeDefinitionShare badgeShare = new WorkBadgeDefinitionShare(
        	AccessLevel = 'Edit',
        	ParentId = badge.Id,
        	UserOrGroupId = '00Gi0000003auY9'
		);
        
        shareList.add(badgeShare);
    }

    insert shareList;

}
