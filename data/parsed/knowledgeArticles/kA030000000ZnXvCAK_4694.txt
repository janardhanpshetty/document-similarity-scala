### Topic: When workflow send out email alert, the Product field (Picklist field) does not populate. Check customer org to see if they have a package install, with field API name same as the merge field name. To resolve the issue, either rename the field API name to something different, or create a formula field to contain affected field in it then use this new formula field in email template.
When workflow send out email alert, the Product field (Picklist field) does not populate. When clicking “Send Test and Verify Merge Fields” in email template the fields populate correctly. Send via “Emails” related list in case also work fine. It’s not populating when send out via workflow. 
Resolution
Check customer org to see if they have a package install, with field API name same as the merge field name. See below for example.
 
The issue is due to two fields in case object as follows:
 
1. Product__c   
2. Apttus_Config2__Product__c
 
The second field is part of a managed package with namespace Apttus_Config2, and essentially has API name Product__c too when the namespace is removed. Because of this the merge engine is confused, and is actually returning the value for Apttus_Config2__Product__c rather than for Product__c.
 
So to resolve the issue, you may either rename the Product__c field API name to something different (e.g. Product1__c), or create a formula field (e.g. ProductCopy__c in sandbox) to contain Product__c in it then use this new formula field in email template. Or if you remove the Apttus_Config2__Product__c field and the package then the issue should not re-occur.
