### Topic: Here you will find announcements related to the Legacy Work.com 1.0 app and Work.com on Salesforce
 
Upcoming Maintenance Windows
Scheduled Maintenance 
(
10:00pm-10:30pm
 ET)
 - 
May 17, 2015
Scheduled Maintenance 
(
10:00pm-10:30pm
 ET)
 - 
May 18, 2015
Scheduled Maintenance 
(
10:00pm-10:30pm
 ET)
 - 
May 19, 2015
We will be performing a scheduled maintenance during the above dates and times. During these scheduled maintenance windows, users should not be affected.
Please, make sure that your firewalls are configured properly. Firewalls need to allow access to all of the IP addresses listed below:
136.146.68.14
136.146.212.8
136.146.66.9
If you have any questions or concerns, please don't hesitate to contact Work.com support by creating a case at http://help.salesforce.com or calling 1 800 NO SOFTWARE.
Work.com 1.0 update: 
Please update Work.com 1.0 password 
***This applies to the legacy Work.com application 
only
, if you login to Work.com from: https://app.work.com/#login please update your password.
At 
Work.com
, trust is our #1 value and we take the protection of our customers’ data very seriously. This is an important update related to your 
Work.com
 account.
On April 7, the OpenSSL Project released an update to address a vulnerability nicknamed “Heartbleed". 
The vulnerability affects a substantial number of applications and services running on the Internet, including the 
Work.com
 product accessed at 
http://www.work.com
.
The 
Work.com
 environment was immediately updated and is no longer vulnerable.
We encourage all users to reset their 
work.com
 account passwords. We do not have any evidence that passwords have been compromised, but any time a large scale vulnerability is discovered, the safest thing to do for your account is to update your password. 
If you have any further questions about this issue please don’t hesitate to contact us at 
support@work.com
.
Past Maintenance Windows
 
Scheduled Maintenance 
(
11:30pm-12:30am
 ET)
 - 
February 4, 2015
Scheduled Maintenance 
(
8:00pm-8:30pm
 ET)
 - 
September 11, 2014
Scheduled Maintenance 
(
11:00pm-11:30pm
 ET)
 - 
September 10, 2014
Scheduled Maintenance 
(
10:30pm-11:30pm
 ET)
 - 
June 11, 2014
Scheduled Maintenance 
(
10:00pm-10:30pm
 ET)
 - 
May 28, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - 
May 7, 2014
Scheduled Maintenance 
(10:00pm-10:30pm ET)
 - 
May 1, 2014
Scheduled Maintenance 
(10:00pm-10:30pm ET)
 - 
April 29, 2014
Scheduled Maintenance 
(10:00pm-10:30pm ET)
 - 
April 21, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - 
April 16, 2014
Scheduled Maintenance
 
(10:00pm-11:00pm ET)
 - 
April 10, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - 
March 26, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - 
March 19, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - 
March 5, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - February 24, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - February 20, 2014
Scheduled Maintenance 
(2pm-3pm ET)
 - February 14, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - February 12, 2014
Scheduled Maintenance 
(5:00pm-5:30pm ET)
 - January 31, 2014
Scheduled Maintenance 
(10:30pm-11:30pm ET)
 - January 22, 2014
Scheduled Maintenance 
(11:00pm-12am ET)
 - January 13, 2014
Scheduled Maintenance (9:00pm-10:00pm ET) - December 24, 2013
Scheduled Maintenance (10:00pm-11:00pm ET) - December 23, 2013
Scheduled Maintenance (10:00pm-11:00pm ET) - December 21, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - December 18, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - December 10, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - December 4, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - November 7, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - October 31, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - October 25, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - October 24, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - October 17, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - October 10, 2013
Scheduled Maintenance (10:30pm-11:30am ET) - September 30, 2013
Scheduled Maintenance (10:30pm-11:30am ET) - September 26, 2013
Scheduled Maintenance (10:30pm-11:30am ET) - September 19, 2013
Scheduled Maintenance (10:30pm-11:30am ET) - September 9, 2013
Scheduled Maintenance (10:30pm-11:30am ET) - August 29, 2013
Scheduled Maintenance (10:30pm-11:30am ET) - August 22, 2013
Scheduled Maintenance (10:00pm-12:00am ET) - August 15, 2013
Scheduled Maintenance (10:30pm-12:30pm ET) - August 8, 2013
Scheduled Maintenance (10:30pm-12:30pm ET) - August 1, 2013
Scheduled Maintenance (10:30pm-12:30pm ET) - June 27, 2013
Scheduled Maintenance (10:30pm-12:30pm ET) - June 20, 2013
Scheduled Maintenance (11:30pm-12:30pm ET) - June 13, 2013
Scheduled Maintenance (11:30pm-12:30pm ET) - June 6, 2013
Scheduled Maintenance (11:30pm-12:30pm ET) - May 30, 2013
Scheduled Maintenance (11:30pm-12:30pm ET) - May 20, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - May 23, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - May 9, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - May 8, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - May 2, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - April 29, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - April 25, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - April 18, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - April 11, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - April 5, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - April 4, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - March 28, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - March 21, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - March 14, 2013
Scheduled Maintenance (10:30pm-11:30pm ET) - March 11, 2013
(Resolved) 
Work.com website is down
September 12, 2014 - We are experiencing an issue with the http://work.com website. In order to access your account please copy and paste this link into your browser and proceed to log in:  https://app.work.com/#login. We are working on this issue and will provide an update shortly.
If you have any questions or concerns, please don't hesitate to contact Work.com support by sending us an email at support@work.com or by creating a case at http://help.salesforce.com.
(Resolved) 
Email Issues related to Google's Postini Service
February 26, 2013 - We have received multiple reports that Google's Postini service has been experience some technical issues.  Users of Work.com and Postini may notice that emails are not being correctly delivered to their destination address or are being incorrectly flagged as "Junk" or "Spam" 
(Resolved) 
Certificate or Login Issues (Related to DNS)
November 8, 2012 - Some Work.com users have experienced issues related to certificate errors or blank pages when connecting to Work.com. After investigating the issue, our findings show that the issue is related to slow propagation of DNS entries on the client's side.   Last week on the 2nd of November, we released Work.com GA, in the migration process we decreased the DNS records TTL value, in order to speed up DNS propagation to clients and ensure smooth transition and no downtime. However, from our investigation we found that some DNS caching servers override the enforced cache/renew interval and retain a stale record for the Work.com service (https://app.work.com). These stale records point to a non-existent service and are the cause of the aforementioned issues.  In order for the issues to be resolved, the caching server responsible for serving the stale records needs to be refreshed. Our advice to our clients is if they experience the issue described herein to contact their IT department.    
As always we are happy to help, please reach out to us at support@work.com for assistance.
(Resolved) 
Known Issue - Login button on Work.com
November 1, 2012 - When clicking on the "Login" button in the top right corner of the Work.com website, users are directed to an incorrect login page.  Work.com is functioning correctly besides this one link.  In order to access your account please copy and paste this link into your browser and proceed to log in: 
https://rypple.com/feedback/#login
This issue has now been RESOLVED and we apologize for any inconvenience it may have caused.
 
Resolution
