### Topic: Creating Custom Object Sharing Rules | Salesforce
Creating Custom Object Sharing Rules | Salesforce
Creating Custom Object Sharing Rules
Available in: Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To create sharing rules:
“Manage Sharing”
Custom object sharing rules can be based on the record owner or on other criteria, including record type and certain field values. You can define up to 300 custom object sharing rules, including up to 50 criteria-based sharing rules.
If you plan to include public groups in your sharing rule, confirm that the appropriate groups have been created.
From Setup, enter 
Sharing Settings
 in the 
Quick Find
 box, then select 
Sharing Settings
.
In the Sharing Rules related list for the custom object, click 
New
.
Enter the Label and Rule Name. The Label is the sharing rule label as it appears on the user interface. The Rule Name is a unique name used by the API and managed packages.
Enter the 
Description
. This field describes the sharing rule. It is optional and can contain up to 1000 characters.
Select a rule type.
Depending on the rule type you selected, do the following:
Based on record owner
—
In the 
owned by members of
 line, specify the users whose records will be shared: select a category from the first drop-down list and a set of users from the second drop-down list (or lookup field, if your organization has over 200 queues, groups, roles, or territories).
Based on criteria
—
Specify the Field, Operator, and Value criteria that records must match to be included in the sharing rule. The fields available depend on the object selected, and the value is always a literal number or string. Click 
Add Filter Logic...
 to change the default AND relationship between each filter.
Note
To use a field that’s not supported by criteria-based sharing rules, you can create a workflow rule or Apex trigger to copy the value of the field into a text or numeric field, and use that field as the criterion.
In the 
Share with
 line, specify the users who get access to the data: select a category from the first drop-down list and a set of users from the second drop-down list or lookup field.
Select the sharing access setting for users.
Access Setting
Description
Read Only
Users can view, but not update, records.
Read/Write
Users can view and update records.
Click 
Save
.
See Also:
Sharing Rules
Sharing Rule Considerations
Sharing Rule Categories
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_sharing_rules_create_cust_obj.htm&language=en_US
Release
202.14
