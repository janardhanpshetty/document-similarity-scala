### Topic: This article explains steps to connect OData in SAP NetWeaver Gateway as External Data Source for Lightning Connect(OData 2.0 Adapter). That means you can view OData on SAP as a Salesforce External Object.
Just like below screen shot, this KB explains steps to connect OData in SAP NetWeaver Gateway as External Data Source for Lightning Connect(OData 2.0 Adapter). That means you can view OData on SAP as a Salesforce External Object. 
Note: In this example we use free SAP environment for developers, so anybody can setup their own SAP-Salesforce test env. No SAP license is required.
There are 3 steps.
1. Steps to create SCN(SAP Community Network) account
2. Steps to login to test SAP environment, then check sample OData records
3. Steps to setup External Resource on Salesforce, then view OData on SAP as Salesforce records
Detail for those 3 steps are the following.
1. Steps to create SCN(SAP Community Network) account
1-0. Note. Please use Firefox or Internet Exploler. Do NOT use Chrome.(refer 
http://scn.sap.com/docs/DOC-40986
)
1-1. Create SCN(SAP Community Network) account via 
https://supsignformssapicl.hana.ondemand.com/SUPSignForms/
 
1-2. Receive email from SCN then activate your SCN account as described in the email
1-3. Visit 
https://supsignformssapicl.hana.ondemand.com/SUPSignForms/
 then your SAP User Name (in this example)P1941608672 will be displayed
1-4. Again visit 
https://supsignformssapicl.hana.ondemand.com/SUPSignForms/
 then click button [Reset User] to set your password for the P1941608672
1-5. Enter your password then click button [Update]
Step1 Completed. Now you can use your SAP test user.
2. Steps to login to test SAP environment, then check sample OData records
2-0. Note. Please use Firefox or Internet Exploler. Do NOT use Chrome.(refer 
http://scn.sap.com/docs/DOC-40986
)
2-1. Visit 
https://sapes1.sapdevcenter.com/webgui
 then enter your username, password, click button [Logon]
2-2. SAP user is asked to change password for its first login. Enter current Password and New passwords, then click [Change]
2-3. Click [Continue] to login to SAP
2-4. Type /IWFND/GW_CLIENT in the "Menu" then hit Return key
2-5. Type /sap/opu/odata/IWFND/RMTSAMPLEFLIGHT in "Request URI", choose [GET], click [Execute]
2-6. You will see "TravelAgencies", "SubscriptionCollection", "FlishgCollection", and more. They are the OData in this SAP test environment. We will view those data via Salesforce External Object at Step3.
2-7. As an example, type /sap/opu/odata/IWFND/RMTSAMPLEFLIGHT/FlightCollection in "Request URI", choose [GET], click [Execute]
2-8. OData records will be displayed such as FlightCollection(carrid='AA',connid='0017',fldate=datetime'2014-12-17T00%3A00%3A00')
2-9. Now include the record in the URI as /sap/opu/odata/IWFND/RMTSAMPLEFLIGHT/FlightCollection(carrid='AA',connid='0017',fldate=datetime'2014-12-17T00%3A00%3A00') then click [Execute]
2-10. Detail of the OData record will be displayed. This will be viewed as a Salesforce record at Step3.
Step2 Completed. Now you found some OData records in the SAP environment.
3. Steps to setup External Resource on Salesforce, then view OData on SAP as Salesforce records
3-1. Loginto your test Salesforce org(Developer Edition), go to Setup->Develop->External Data Sources then hit [New External Data Source]
3-2. Choose "Lightning Connect: OData 2.0", URL is 
https://sapes1.sapdevcenter.com/sap/opu/odata/IWFND/RMTSAMPLEFLIGHT
 enter SAP user name (in this example)P1941608672 and its password, then click [Save]
3-3. Click [Validate and Sync]
3-4. Then the OData on SAP will be listed. Check all(or some) of them then click [Sync]
3-5. Go to Setup->Create->Custom Tabs then click [New]
3-6. All of the SAP OData will be listed. Choose "FlightCollection" in this example then save the Tab(just default Tab settings are fine)
3-7. Go to MySettings->Authentication Settings for External Systems, then choose the test Salesforce user and enter SAP username and password, then hit [Save]
3-8. Go to Setup->Security Controls->Field Accessibility then click "FlightCollection"
3-9. By default, most of the columns are hidden by Field Level Security as below.
3-10. So, change some(or all) of them as visible.
3-11. Go to Tab "FlightCollection". 
I have added columns for the view "all". 
Here we can view OData records on SAP. As example, click the top record "connid=0017", "fldate=2014/12/17 9:00"
3-12. This is the same information we saw at step 2-10.
Step3 Completed. Now you have got this view on your environment.
Resolution
