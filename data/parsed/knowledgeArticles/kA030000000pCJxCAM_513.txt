### Topic: Number fields are displaying in Arabic format when a user uses an Arabic Locale - we now have a workaround for this
Issue:
Initially customers that had an Arabic Locale such as "Arabic (Egypt)" saw numbers in date fields, number fields and currency fields appear in English script. Now, due to an un-avoidable Java update the numbers appear in Arabic Script, as shown below. Note: The types of fields where the numbers appear in Arabic script will depend on whether the user locale, or the company currency locale is set to Arabic
Resolution
Solution:
R&D have responded to this issue by introducing a new perm "
ShowHinduArabicNumbers
".  When this perm is enabled, the following Arabic locales will show numbers as English" script numerals for the following locales:
("ar", "AE"), ("ar", "BH"), ("ar", "EG"), ("ar", "IQ"), ("ar", "KW"), ("ar", "OM"), ("ar", "QA"), ("ar", "SA"), ("ar", "SD"), ("ar", "YE"), ("ar", "JO"), ("ar", "LB"), ("ar", "SY")
For currency fields the updated numbers will now look like this:
The Arabic script at the end shows the currency used
Tier 1/2 Action to take:
This perm will *not* be automatically enabled, and must be requested. To request that this perm will be activated please create a case for the customer requesting this (Function Area: Custom Fields), and escalate to the Tier 3 team who will take it from there.
Tier 3 Action to take:
Please activate this feature by ticking the blacktab perm: "Show Hindu-Arabic Numbers". If you need any background information on the cause of the Arabic script issue check W-2744764 
