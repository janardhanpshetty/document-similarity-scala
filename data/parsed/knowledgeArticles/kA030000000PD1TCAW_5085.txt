### Topic: This article explains about the steps which should be taken to insert/update RichText Fields using Data Loader. Rich Text Field is a format content holding field and thus, needs special considerations.
I would like to add a series of images to my Records in Salesforce, how can I do this through the Data Loader?
Resolution
1.) First create a new rich text field(RichText__C) on the object you want to add the images to. Next we need to prepare an import file to upload the images into documents. 
Click here
 (Article #3865)  for how to import documents.  NOTE: You will need the Salesforce IDs(Or External IDs) of each record on the object you are importing the images into eventually. To make this process easier when preparing your document import file include this column into your import file ensuring that the IDs of the records match to the documents that will eventually be imported onto them (We'll call this column RecordID).
2.) Next we need to take the success file from the document import and use it to construct the HTML code for the rich text field.
3.) Add a new column to the beginning of the success file with a column header called link.
You can use the following Link as a template.
For externally available images paste and copy this link all the way down the file: <img src ="https://c.naX.content.force.com/servlet/servlet.ImageServer?id={ImageID}&oid={OrgID}">
For internally available images paste and copy this link all the way down the file: <img src ="https://c.naX.content.force.com/servlet/servlet.FileDownload?file={ImageID}">
4.) Find and Replace naX with the instance of the server the images are on and find and replace {OrgID} with the ID of the Organization (Available in Setup | Administration Setup - Company Profile | Company Information).
5.) Insert a new column into excel in front of the link column with the header matching the name of the Rich Text field and ensure the column labeled 'ID' is column C.
6.) Use the following formula to populate the ImageID: =SUBSTITUTE(B2,"{ImageID}",C2)
7.) Right click on column a and copy. Right click again and then paste special - Values. Save the file.
8.) Open the Data loader (Login to Data Loader).
9.) Select the function as Upsert.
10.) Select the Object by checking "Show all objects".
11.) Select the object the rich text field is on| Map the ID of the records and the rich text field column| Select the directory where your success and error files will be saved and then click Finish and OK.
12.) The rich text fields will be populated with your images in salesforce.
 
