### Topic: Understanding User Sharing | Salesforce
Understanding User Sharing | Salesforce
Understanding User Sharing
Set organization-wide defaults for internal and external user records. Then, extend access using sharing rules based on membership to public groups, roles, or territories, or use manual sharing to share individual user records with other users or groups.
Available in: Salesforce Classic and Lightning Experience
Manual sharing available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
When you enable user sharing, users can see other users in search, list views, and so on only if they have Read access on those users.
Review these considerations before you implement user sharing.
“View All Users” permission
This permission can be assigned to users who need Read access to all users, regardless of the sharing settings. If you already have the “Manage Users” permission, you are automatically granted the “View All Users” permission.
Organization-wide defaults for user records
This setting defaults to Private for external users and Public Read Only for internal users. When the default access is set to Private, users can only read and edit their own user record. Users with subordinates in the role hierarchy maintain read access to the user records of those subordinates.
User sharing rules
General 
sharing rule considerations
 apply to user sharing rules. User sharing rules are based on membership to a public group, role, or territory. Each sharing rule shares members of a source group with those of the target group. You must create the appropriate public groups, roles, or territories before creating your sharing rules. Users inherit the same access as users below them in the role hierarchy.
Manual sharing for user records
Manual sharing can grant read or edit access on an individual user, but only if the access is greater than the default access for the target user. Users inherit the same access as users below them in the role hierarchy. Apex managed sharing is not supported.
User sharing for external users
Users with the “Manage External Users” permission have access to external user records for Partner Relationship Management, Customer Service, and Customer Self-Service portal users, regardless of sharing rules or organization-wide default settings for User records. The “Manage External Users” permission does not grant access to guest or Chatter External users.
User Sharing Compatibility
When the organization-wide default for the user object is set to Private, User Sharing does not fully support these features.
Chatter Messenger is not available for external users. It is available for internal users only when the organization-wide default for the user object is set to Public Read Only.
Customizable Forecasts—Users with the "View All Forecast" permission can see users to whom they don't have access.
Salesforce CRM Content—A user who can create libraries can see users they don't have access to when adding library members.
Standard Report Types—Some reports based on standard report types expose data of users to whom a user doesn’t have access. For more information, see 
Control Standard Report Visibility
.
See Also:
User Sharing
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_sharing_users_concept.htm&language=en_US
Release
202.14
