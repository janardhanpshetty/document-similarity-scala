### Topic: When an opportunity is logged with a couple of contact roles against it - with 1 contact allocated to two or more roles, the opportunity appears twice or more against the contact, making it appear like a duplicate opportunity.
We created a custom report type and we are encountering dupes (<1% of entries). Normally this wouldn't be a major concern (bc of the small # of occurences) but this is a report for Finance. The report combines Contacts, Opportunities, ans two custom objects.
Resolution
When an opportunity is logged with a couple of contact roles against it - with 1 contact allocated to two or more roles, the opportunity appears twice or more against the contact, making it appear like a duplicate opportunity.
Custom report types with Contact + Opportunity relationship  are taken in Salesforce as joins of Contacts + OpportunityContactRoles + Opportunities. So, since some of the opportunities have associated contacts with multiple roles, this can make it appear that there are duplicates in the report when really, these are separate rows (one for each contact role per contact).
If they're not aware that there are duplicate contact roles for the contact records, or if they don't need the contacts to be associated with multiple contact roles, they can simply delete them and then the report would appear as they expect.
There is already an Idea proposed about this as well:
"Duplicate Opportunity against Contact record":
http://success.salesforce.com/ideaView?id=087300000006n4EAAQ
