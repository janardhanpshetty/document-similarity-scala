### Topic: If on-demand email-to-case finds a foreign refId in an outbound email, it will add a combination of the external and the local ref Ids
If on-demand email-to-case finds a foreign refId in an outbound email, it will add a combination of the foreign and the local ref Ids
Resolution
We have made improvements to allow Salesforce customers to collaborate via email-to-case without creating duplicate cases. In order to allow such a process, email-to-case now generates a refId that combines multiple refIds into a single, longer refId and appends it to the Subject line of all outbound email messages. 
Email-to-case in the receiving Salesforce org recognizes the combined refId and adds incoming emails to the correct case. 
The new functionality is triggered when email-to-case detects an external refId in an outbound email, even if the settings "Insert Thread ID in the Email Subject" and "Insert Thread ID in the Email Body" have been disabled. 
For example, if your email-to-case generates a refId that look like this: 
[ ref:_00D907YFi._50060vXsWt:ref ]
and you add the following external/foreign refId to any outbound email message:  
[ ref:_00D0063._50030bWhni:ref ]
then email-to-case will combine both and append the following merged refId to the subject line: 
[ ref:_00D907YFi._50060vXsWt,_00D0063._50030bWhni:ref ]
