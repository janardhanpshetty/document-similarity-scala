### Topic: Background information around the use of custom thread ids
Out of the box Email to Case functionality supports the use of a system generated thread id when emails are sent out from Cases or Auto response rules.  The thread id allows inbound mails to be associated to existing cases in Salesforce. 
 
Resolution
Organization Splits, migrations and scheduled releases can mean that the format of the thread ids can change from time to time.  This is always factored in the a
pplication code that processes the thread id in the Salesforce application.
A side effect of any refactoring in the Salesforce application that could affect thread ids is that organizations using custom thread ids that are composed from formulas or other could experience problems around this when any application side changes are made. Unfortunately application side changes cannot factor in the variety of formulas that could be used by organizations to construct thread ids.
Salesforce doesn't support the use of compound custom thread ids as these cannot be accommodated in any application side changes around the processing and composition of thread ids.   To avoid problems in this area please create processes around email to case that are based on the regular system generated thread ids.
A custom thread id example would be the creation of a custom field on the Case object that creates a thread id from a formula based on the Organization and Case id values. This can be used as a merge field in templates, API emails workflow and other. The formula would not take into account any future changes to Case or Organization ids or logic changes to the thread id processing on the application side.
