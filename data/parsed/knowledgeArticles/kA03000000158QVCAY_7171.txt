### Topic: In Winter '16, you are now able to Distribute Lightning Components on the AppExchange
The Lightning component framework makes it easy for customers to build responsive apps for any screen. As a partner, you can now distribute Lightning components to millions of Salesforce users on our app marketplace.
You can list a Lightning component on the AppExchange by following these high-level steps:
Package your functionality in a managed package.
Upload your package to the Partner Community.
Log in to the Partner Community and navigate to the Publishing page.
On the Organizations tab, connect your packaging organization.
On the Packages tab, submit your Lightning component for security review.
On the Listings tab, create and publish your listing.
For more details about publishing on the AppExchange, see the ISVforce Guide.
Resolution
