### Topic: In case of multi language settings, depending on single or multi language and also on whether PKB 2 is checked or not and error "File not Found" will be thrown.
Once the Public Knowledge base is setup with sites.com, the articles are shown on the web URL of the sites. When the Articles are clicked, the file not found error is  displayed as shown below. 
 
Resolution
If this is case then check the Multi Lingual setting on the Knowledge (Setup->Customize->Knowledge->Settings->Language Settings) and the Multi-Language Enabled checkbox on PKB 2 Settings (PKB 2 Settings -> select the site from drop down -> Common set up). Both the settings should match.
 
Scenario 1: 
Multiple languages are selected on the Language settings of the Knowledge, but in the PKB 2 Setting Multi Language checkbox is not selected.
Result: This will throw “File not found” error.
 
Scenario 2:
 Single language is selected on the Language settings of the Knowledge, but in PKB 2 Setting Multi Language checkbox is selected.
Result: This will throw “File not found” error.
 
Scenario 3:
 Multiple languages are selected on the Language settings of the Knowledge and in the PKB 2 Setting Multi Language checkbox is selected.
Result: When the articles are clicked then they are opened on URL.
 
