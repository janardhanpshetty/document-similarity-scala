### Topic: This article outlines considerations for Change Set deployment when the production organization has been migrated to Government Cloud
When both the source and target organizations are both hosted on instances in the Government Cloud (or both hosted on a standard Salesforce instance), the declarative Change Set functionality works as 
documented
.
When a production organization has been migrated into the Salesforce Government Cloud, newly created or newly refreshed sandboxes will also be in the Government Cloud. And any sandboxes that existed outside of the Government Cloud prior to the production org's migration, will remain on their original instance. 
 
Resolution
When sandboxes exist outside of Government Cloud, there are additional considerations when deploying change sets between related organizations.
1. Creating Deployment Connections
From the Deployment Connections list found at Setup > Deploy > Deployment Settings, each related org should be listed.
Deployment Connections can be created between related organizations (sandbox-to-sandbox, production-to-sandbox, or sandbox-to-production) regardless of whether the two orgs are inside or outside of Government Cloud at the time that the connection is created.
    
2. Deploying Declarative Change Sets
With a Deployment Connection correctly configured, any organization regardless of its location will be able to successfully setup and send an outbound change set, regardless of the target organization's location with respect to Government Cloud. However, once uploaded, if the source org is inside of the Government Cloud and the target organization is outside of the Government Cloud, the change set will not appear in the "Inbound Change Sets" list of the target organization.
  
 
 
Source Location
 
 
In Government Cloud
Outside Government Cloud
Target Location
In Government Cloud
Supported
Supported
Outside Government Cloud
Not Supported
Supported
3. Deploying Change Sets using the Force.com IDE Tool or Workbench
Regardless of the location of the target and source instances, third-party tools (such as Workbench) and developer tools such as the Force.com IDE Tool or Migration Tool will continue to work for programmatic deployments between organizations.
