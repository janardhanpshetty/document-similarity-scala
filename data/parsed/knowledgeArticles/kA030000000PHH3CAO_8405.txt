### Topic: How do I remove or change the focus from a certain input field on a VF page
I want to remove the focus from the first input field on a VF page. I want it to be removed permanently or shift to some other field
Resolution
The following sample code demonstrates how to achieve this requirement:
<apex:page standardController="Opportunity" > 
<apex:form > 
<script> 
function setFocusOnLoad() { 
document.getElementById('{!$Component.pg.pg1.pg2.val1}').focus(); 
} 
onload=setFocusOnLoad; 
</script> 
<apex:pageblock id="pg"> 
<apex:pageBlockSection title="Opportunity Information Input" columns="1" id="pg1"> 
<apex:pageBlockSectionItem id="pg2" > 
<apex:inputfield id="val1" value="{!Opportunity.StageName}"/> 
<apex:inputfield value="{!Opportunity.StageName}"/> 
</apex:pageBlockSectionitem> 
<apex:pageBlockSectionItem > 
<apex:inputfield value="{!Opportunity.Name}"/> 
<apex:inputfield value="{!Opportunity.Name}"/> 
</apex:pageBlockSectionitem> 
<apex:pageBlockSectionItem > 
<apex:inputfield value="{!Opportunity.Name}"/> 
<apex:inputfield value="{!Opportunity.Name}"/> 
</apex:pageBlockSectionitem> 
</apex:pageBlockSection> 
</apex:pageblock> 
</apex:form> 
 
</apex:page>
