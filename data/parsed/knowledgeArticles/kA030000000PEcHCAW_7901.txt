### Topic: Since ONLY active system administrator can request and download the data export, verify the person requesting the data export is currently an Active System Administrator on THE org.
INTERNAL: How to qualify a case for Data Export
Resolution
How to qualify a case for Data Export:
 
STEP 1:
1.
    
Since ONLY active system administrator can request and download the data export, verify the person requesting the data export is currently
 an Active System Administrator on THE org. 
 
            o
  
NO INACTIVE users 
            o
  
NO Standard users or any other profiles
 
2.
    
In order to do this, go to the organization's Black Tab
 
3.
    
Click on "View Users" and verify users information 
 
4.
    
Notify user that the data export link will be sent ONLY to the email account that is on the system admin's profile (Verify user's email address as well)
 
5.
    
IF the Organization requesting the "Data Export", does NOT have an active system admin or if the system admin is no longer with the company
         please move to 
step 2
 
STEP 2 :
 
  
   1.  We have a solution that explains the "change of system admin" process. 
 
 
    2. Please have the customer confirm via email once the fax has been sent
 
    3. Update the case comment with this confirmation and add the Black Tab link for the organization 
 
    4. Escalate the case to Tier 2 with General Application and Functional Area as "Feature Activation".
