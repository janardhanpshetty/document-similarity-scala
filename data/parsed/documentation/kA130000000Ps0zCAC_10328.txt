### Topic: Defining Roll-Up Summaries | Salesforce
Defining Roll-Up Summaries | Salesforce
Defining Roll-Up Summaries
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To view roll-up summary field definitions:
“View Setup and Configuration”
To edit roll-up summary field definitions:
“Customize Application”
Define roll-up summary fields on the object that is on the master side of a master-detail relationship. If a relationship does not already exist, first create a master-detail relationship between the master object that displays the value and the detail object containing the records you are summarizing.
To define a roll-up summary field:
Create a custom field on the object where you want the field displayed. Summary fields summarize the values from records on a related object, so the object on which you create the field should be on the master side of a master-detail relationship. For instructions on creating a custom field, see 
Create Custom Fields
.
Choose the 
Roll-Up Summary
 field type, and click 
Next
.
Enter a field label and any other attributes. Click 
Next
.
Select the object on the detail side of a master-detail relationship. This object contains the records you want to summarize.
Select the type of summary:
Type
Description
COUNT
Totals the number of related records.
SUM
Totals the values in the field you select in the 
Field to Aggregate
 option. Only number, currency, and percent fields are available.
MIN
Displays the lowest value of the field you select in the 
Field to Aggregate
 option for all directly related records. Only number, currency, percent, date, and date/time fields are available.
MAX
Displays the highest value of the field you select in the 
Field to Aggregate
 option for all directly related records. Only number, currency, percent, date, and date/time fields are available.
Enter your filter criteria if you want a selected group of records in your summary calculation. If your organization uses multiple languages, enter filter values in your organization's default language.
When you use picklists to specify filter criteria, the selected values are stored in the organization's default language. If you edit or clone existing filter criteria, first set the 
Default Language
 on the Company Information page to the same language that was used to set the original filter criteria. Otherwise, the filter criteria may not be evaluated as expected.
Click 
Next
.
Set the field-level security to determine whether the field should be visible for specific profiles, and click 
Next
.
Choose the page layouts that should display the field. The field is added as the last field in the first two-column section on the page layout. For user custom fields, the field is automatically added to the bottom of the user detail page.
Click 
Save
 to finish or 
Save & New
 to create more custom fields.
See Also:
Roll-Up Summary Field
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_defining_summary_fields.htm&language=en_US
Release
202.14
