### Topic: Inviting Business Partners to Connect using Salesforce to Salesforce | Salesforce
Inviting Business Partners to Connect using Salesforce to Salesforce | Salesforce
Inviting Business Partners to Connect using Salesforce to Salesforce
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To invite to a business partner to connect using Salesforce to Salesforce:
“Manage Connections”
Before you can start sharing data with your business partners, you need to set up a connection to them using Salesforce to Salesforce. To create that connection, you need to invite your business partner.
Note
Before beginning the invitation process, you need to create an account and an associated contact for your business partner.
To invite a business partner to connect using Salesforce to Salesforce:
Click the Connections tab.
Click 
New
.
Enter a contact name or use the lookup icon to select a contact.
Important
Make sure that the email address is valid; otherwise, your invitation may be sent to the wrong person.
Optionally, select a related account.
Note
The account associated with the connection can be changed after the invitation is sent.
Choose a user to manage the connection.
Optionally, choose a template to apply to the connection.
Click 
Save & Send Invite
.
To invite multiple business partners to connect using Salesforce to Salesforce:
Click the Contacts tab.
Select a standard or custom list view, then click 
Go!
.
In the list view, select the checkboxes next to the contacts you want to invite.
Important
Make sure that the email addresses are valid; otherwise, your invitations may be sent to the wrong people.
Click 
Invite to Connect
.
Choose a user to manage the connection.
Optionally, choose a template to apply to the connection.
Click 
Save & Send Invite
.
The people you invite will receive an email invitation to connect with you using Salesforce to Salesforce. They can choose to accept or decline your invitation. You can check whether they have accepted or declined the invitation by reviewing your connections on the Connections tab. From the Connections tab, you can also resend or cancel the invitation. To learn more about connections, see 
Connections Home Page
.
Note
For information about sending an Organization Sync replication connection invitation in Salesforce to Salesforce, see 
Connect the Organizations
.
See Also:
Finding Out if Your Partners Use Salesforce
Connection Templates in Salesforce to Salesforce
Creating and Applying Connection Templates
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=business_network_send_invitation.htm&language=en_US
Release
202.14
