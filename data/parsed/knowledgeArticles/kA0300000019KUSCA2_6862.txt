### Topic: There are two situations in which the Match Production Licenses tool may report success without making any change to the org.
I used the 
Match Production Licenses
 tool in my sandbox and I received an email confirming that the tool had run successfully. However, I can't see any changes in the licenses and other provisioned amounts in my sandbox.
Resolution
The Match Production Licenses tool will report success so long as it completes without error, even if it found nothing to match. 
This may be the result if the Sandbox and Production org are an exact match with regard to licenses. In that situation, there will be no changes.
For a small number of sandboxes, particularly those that were last refreshed a very long time ago (more than one year ago), old settings on the sandbox might cause the tool to report a successful run without matching the expected licenses.
An example of this situation would be that 10 Salesforce licenses were added to the production org, but when the tool is run, the Salesforce license count on the sandbox is not increased to match production. If you experience this behavior, please log a case with Salesforce support for assistance.
