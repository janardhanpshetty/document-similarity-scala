### Topic: Best practice for new to Salesforce, signing up for a Salesforce.org trial, and the NPSP, already using Salesforce and want to install NPSP, or if you have NPSP and want to upgrade.
Show More
Like
Unlike
 
  ·  
 
August 3, 2015 at 9:22 AM 
Attach File
 
Click to comment
 
 
Show All (1)
Followers
« 
Go Back
Information
 
Skill Group
CRM Usage
This article discusses how to:
-Sign up if you are new to Salesforce and the Nonprofit Starter Pack (NPSP)
-If you already have the Nonprofit Starter Pack (NPSP) and want to upgrade 
-If you have 
Salesforce System Administrator profile, using the Enterprise Edition (not other editions nor Force.com),
 but do not have the Nonprofit Starter Pack (NPSP) and want to install
 
Resolution
Current Version of the Nonprofit Starter Pack: 3.0
If you are new to Salesforce and The Nonprofit Starter Pack (NPSP)
Visit the Salesforce.org 
trials page
.
Click on "Select Trial Edition" and choose 
Pre-Configured for Donor Management (Enterprise Edition + NPSP)
 as your trial edition.
Fill out the rest of the form and click Submit.
If you already have the Nonprofit Starter Pack (NPSP), and want to upgrade:
IMPORTANT BEFORE INSTALLATION: 
Befores you upgrade you should take a look at the 
Upgrade Guide
. The guide explains the process and walks you through the process. 
Please remember that  Salesforce does not recommend installing upgrades directly in production organizations. Instead, install your NPSP upgrade in one of your free sandbox environments, or a developer organization first. See 
Creating or Refreshing a Sandbox
 in Salesforce Help for more information
.
Additionally, if you worked with an implementation partner to develop your Salesforce solution, we strongly recommend that you review any implications for your custom code that this upgrade might affect.
 
Visit the 
NPSP 3.0 Installer page
.
Log in to your Salesforce organization by clicking Log In, choosing 
Production or Developer Edition
 or 
Sandbox
, and entering your login credentials.
Once you're logged into Salesforce, the Installer tells you which packages or bundles your organization needs for the update.
Review the installation list and click Install.
IMPORTANT AFTER INSTALLATION: 
When upgrading the Nonprofit Starter Pack in your Production or Developer Edition of Salesforce you will need to perform a few additional steps, one of which includes running a 
Health Check
. Running Health Check will tell you if there are any configuration steps that you need to correct. You should also create a test contact, account/household and opportunity to confirm that everything appears as it should. For more about this please make sure you consult the 
post-install instructions
 once you've completed your upgrade by clicking 
here
.
If you already have Salesforce, but don't have the Nonprofit Starter Pack (NPSP) installed:
You can install the Nonprofit Starter Pack (NPSP) on any "empty" version of Salesforce Enterprise Edition (Production or Sandbox)
IMPORTANT BEFORE INSTALLATION
 :
 Salesforce does not recommend installing upgrades directly in production organizations. Instead, install your NPSP upgrade in one of your free sandbox environments, or a developer organization first. See 
Creating or Refreshing a Sandbox
 in Salesforce Help for more information
.
 
Visit the 
NPSP 3.0 Installer page
.
Log in to your Salesforce organization by clicking Log In, choosing 
Production or Developer Edition
 or 
Sandbox
, and entering your login credentials.
Review the installation list and click Install.
IMPORTANT AFTER INSTALLATION
 : 
After installing the Nonprofit Starter Pack in your Production or Developer Edition of Salesforce you will need to perform a few additional steps, one of which includes running a 
Health Check
. Running Health Check will tell you if there are any configuration steps that you need to correct. You should also create a test contact, account/household and opportunity to confirm that everything appears as it should. For more about this please make sure you consult the 
post-install instructions
 once you've completed your upgrade by clicking 
here
.
