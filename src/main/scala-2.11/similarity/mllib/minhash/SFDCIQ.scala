package sfdciq.rdd.campaign

/**
 * Created by jshetty on 3/24/17.
 */


import org.apache.spark.sql.SparkSession

import scala.collection.mutable.HashSet


object SFDCIQ {

  def putBlacklist(blacklist: Array[String]): HashSet[String] = {
    val blacklistSet = HashSet() ++ blacklist
    blacklistSet
  }

  def remove_dollar(amount: String): Float = {
    amount.substring(1).toFloat
  }

  val topN = 10000

  def main(args:Array[String]): Unit = {
    val spark = SparkSession.builder.master("local[8]").appName("SFDC Mass Email Campaign").getOrCreate()
    val sc = spark.sparkContext

    val customersRDD = sc.textFile("/Users/jshetty/sfdc_interv/customers.txt")
    val transactionsRDD = sc.textFile("/Users/jshetty/sfdc_interv/transactions.txt")
    val blacklistRDD = sc.textFile("/Users/jshetty/sfdc_interv/blacklist.txt")

    // Remove the headers in all 3 files
    val customerHeader = customersRDD.first()
    val transactionsHeader = transactionsRDD.first()
    val blacklistedHeader = blacklistRDD.first()

    val filteredCustomers = customersRDD.filter(line => line != customerHeader)
    val filteredTransactions = transactionsRDD.filter(line => line != transactionsHeader)
    val filteredBlacklisted = blacklistRDD.filter(line => line != blacklistedHeader)

    // Split using
    val customers = filteredCustomers.map(line => line.split(";")).filter(line => line.length == 3)
    val transactions = filteredTransactions.map(line => line.split(";").map(elem => elem.trim))
    val blacklist = filteredBlacklisted.map(elem => elem.trim)

    // Preprocess transactions
    val prepTransactions = transactions.map(arr => (arr(0), arr(1).substring(1), arr(2).substring(0,4))).filter(tuple => (tuple._3 == "2016")).map(tuple => (tuple._1, tuple._2.toFloat, tuple._3))
    //val sortedTrans2016 = prepTransactions.map(tuple => (tuple._1, tuple._2)).reduceByKey((a, b) => a + b).sortBy(_._2, false)

    // Take ordered first 10,000 and also sort
    val sortedTrans2016 = prepTransactions.map(tuple => (tuple._1, tuple._2)).reduceByKey((a, b) => a + b).takeOrdered(topN)(Ordering[Float].reverse.on(x=>x._2))

    // remove blacklist emails from customers rdd
    val blacklistSet = blacklistRDD.collect.toSet


    // Email
    val white = customers.map(x => (x(0), x(1), x(2).split(",").filter(y => !blacklistSet.contains(y))))
    //val whitelistCustomers = customers.filter(arr => !blacklistSet.contains(arr(2)))
    val customersPair = white.filter(x => x._3.length>0).map(item  => (item._1,item._2,item._3.mkString("")))

    // Join sortedTrans with customers through Id
    //val customersPair = whiteCustomers.map(item => (item(0), item(1), item(2)))
    val transactionsPair = sc.parallelize(sortedTrans2016)

    val pairRDD1 = customersPair.map { case (customerID, fullName, email) => ((customerID), (fullName, email)) }
    // Then, we create the second PairRDD, with (name, address) as key and landmark as value

    val joined = pairRDD1.join(transactionsPair).map {
      case ((customerID), ((fullName, email), amount)) => (customerID, fullName, email, amount)
    }

  }

}
