### Topic: In Summer 2015, link clicks will begin to be counted using Bitly instead of Social Studio's custom link click analytics for customers using the default link shortener (bddy,me) or a custom Bitly link shortener. All link click data using the default link shortener or a custom Bitly link shortener will be backfilled as far back as the organization was using Bitly link shortening in Social Studio.
In Summer 2015, link clicks will begin to be counted using Bitly instead of Social Studio's custom link click analytics for customers using the default link shortener (bddy,me) or a custom Bitly link shortener. All link click data using the default link shortener or a custom Bitly link shortener will be backfilled as far back as the organization was using Bitly link shortening in Social Studio.
Resolution
Link click count backfill overview
Because there are several different methods for tracking and counting link clicks and  every partner has a slightly different way of implementing it, 
Marketing Cloud has decided to display link click data  from a single, trusted analytics provider - Bitly. 
Having been in the link shortening and tracking space for a long time, Bitly is a trusted and respected link analytics provider who aggressively filters out bots and other spam clicks from their link click measurements. 
 
Important notes about the Bitly link click data update
The link click count updates impact Social Studio numbers only. This change does not impact ConversationBuddy/Buddy Media link click data.
Accounts that do not opt to shorten URLS will not see any Bitly link click data. You must use the default shortener (bddy.me) or your own custom Bitly shortener to see Bitly link click data.
If you use a custom shortener that is not through Bitly, you will not see any link click data. You must use the default shortener (bddy.me) or your own custom Bitly shortener to see Bitly link click data.
Accessing historical Social Studio link click data
After the link click data in Social Studio is updated with Bitly's stats, the historical Social Studio link click data will no longer be available in the application. Customers can request an export of their historical Social Studio link click data from Marketing Cloud Technical Support. They will provide their organization ID (learn how to find it in Social Studio 
here
) and the date range that they would like the historical data. 
Submit a database request 
with the organization/tenant ID and the date range needed to retrieve the export for the customer.
 
Backfill Milestones
Marketing Cloud will begin updating customer link data with the 196 release. Customers will not be impacted by the data processing during this time because the new link click count data will not be visible to them in Social Studio. Once the link click data processing is complete across all customer accounts, customers will receive a communication notifying them that the changes are complete. At that time, customers will see the new Bitly link click stats in their Social Studio account. 
Because every company tracks and measures link clicks differently, the original Social Studio numbers will likely be different than the numbers reported by Bitly or from other networks and providers. 
 
Related Topics
Marketing Cloud - Redirecting Product Roadmap Questions
 
