### Topic: This article contains the information you will need as a Premier + Admin customer to submit a proper Approval Matrix for your account entitlement. There is an included attachment at the bottom with instructions that should be filled out and provided with your Admin Services case.
What are the Approval Matrix instructions for Premier + Admin entitlement customers?
Resolution
For Premier+ customers, the Approval Matrix indicates who at your organization is authorized to make setup changes to your production or sandbox orgs.
This matrix and the Premier+ Support Administrator licenses are required before the admin team begins any work from cases submitted as Administration Requests. 
Please fill out the attached information (See Attachments below) and provide with your "Admin Services" case.
