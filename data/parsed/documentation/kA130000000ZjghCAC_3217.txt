### Topic: Considerations for Setting Up Geocode Clean Rules | Salesforce
Considerations for Setting Up Geocode Clean Rules | Salesforce
Considerations for Setting Up Geocode Clean Rules
Review these considerations before adding geocode information to Salesforce accounts, contacts, and leads using Data.com geocode clean rules.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Important
Geocodes are added to records using Data.com technology. However, a Data.com license is 
not
 required to use this feature.
When geocode information is added to records:
Existing values in geocode information fields are overwritten.
The 
SystemModStamp
 field is always updated. Review any integrations that use this field to make sure that they aren’t affected.
The 
LastModifiedDate
 and 
LastModifiedById
 fields are 
not updated
. This is the default option. You can change it when you set up your geocode clean rules.
Geocode information is not visible on records. However, it can be viewed using the Salesforce API. Also, you can create custom fields using formulas to show geocode information fields on records.
Status information associated with clean rules can’t be copied to or from a sandbox. For example, in your production organization, you activate your 
Geocodes for Lead Address
 clean rule, and geocode information is successfully added to all of your existing leads. As a result, the status of the 
Geocodes for Lead Address
 clean rule for each lead is 
In Sync
. You can’t copy this status information for records to your sandbox. To get status information in your sandbox, you need to activate your clean rules and process your records in your sandbox.
Person accounts are not currently supported.
See Also:
Set Up Geocode Clean Rules
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_admin_considerations_for_setting_up_clean_rules_so_that_geocode_info_is_added_to_cleaned_records.htm&language=en_US
Release
202.14
