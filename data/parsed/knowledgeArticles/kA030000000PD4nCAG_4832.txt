### Topic: Case visibility in the Customer Portal is mainly controlled off the Contact Name field on Case. If the Customer Portal user is the Contact on the Case, then they have the ability to see it.
I have a list view on Cases called 'My Cases' with a filter by owner that filters by my cases. This list view is available to customer portal users. What will customer portal users see when they select this list view from the customer portal?
Resolution
Case visibility in the Customer Portal is mainly controlled off the Contact Name field on Case.  If the Customer Portal user is the Contact on the Case, then they have the ability to see it. 
The reason we configure access like this is because, in most scenarios, a customer who logs a case would never be the Case Owner, but almost always the Case Contact who an internal Case Owner would reach out to.  That being said, when you have a list view filtered on "My Cases", for Portal users, it returns Cases that are deemed theirs, which is Cases where they are on the Case Contact. 
NOTE: To ensure that this list view is working correctly for the community users, the Case OWD should be Private and there should be a sharing set created for the Community profiles.
Also 
 "My Cases" list view should have the "Visible to all users (Includes partner and customer portal users)" option selected
IMPORTANT:
 For self-service community templates such as Napili, Koa and Kokua, it still calls the listView API and get the filter logic whereby it is still only filtering by caseownerid = currentUserId. 
The suggested workaround is to create a custom formula checkbox field i.e., ContactId = $User.ContactId and use this as a criteria in the list view.
