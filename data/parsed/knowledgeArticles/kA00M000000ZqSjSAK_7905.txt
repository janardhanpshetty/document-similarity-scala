### Topic: This article helps how to avoid using extra picklist values in salesforce while updating/inserting through Data Loader.
When loading values in picklist using 
Data Loader
, user can load picklist values which do not exist in the
 Salesforce
 org, this can create data integrity issue.
Resolution
To avoid this we can use 
Strictly enforce picklist values
 option while creating the new picklist field.
When we select this option it validates picklist values against the list of defined values and gives error if non-existence value is used in the CSV file while updating the picklist.
See Also:
Eliminate Picklist Clutter with Restricted Picklists (Pilot)
Manage Inactive Values in a Restricted Picklist
