### Topic: Approvals Limits in Chatter | Salesforce
Approvals Limits in Chatter | Salesforce
Approvals Limits in Chatter
Approval limits for delegated approvers, approvals posts, and Sites or portal users.
Approvals in Chatter doesn't support delegated approvers or queues.
You can’t recall or reassign an approval request from a post. Instead, perform these actions from the approval record.
Approval requests from Sites or portal users aren’t supported.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_approvals_chatter.htm&language=en_US
Release
202.14
