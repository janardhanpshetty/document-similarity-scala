### Topic: This article shows a formula on how to add hour to a datefield.
What is the proper way to add hour to a datatime field using formula?
Resolution
The solution is:
(DateValue(<DateTime field> +Addhour /24))
