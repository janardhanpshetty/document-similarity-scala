### Topic: Salesforce Support may ask you to grant login access in order to assist you with a question, issue or request. Learn how to grant access and what permissions are needed.
Salesforce Support may ask you to 
grant login access
 in order to assist you with a question, issue, or request. This is so that Support can log into the application using your login to troubleshoot and fix issues stemming from your inquiry. You must grant access to them and specify a duration for that access, but please be assured that no one can log in to your account to resolve support issues unless you grant access in this way.
Caution:
 Salesforce Support will 
never
 ask for your password, either via phone or email. If you receive any email requests for your password from a source pretending to be from Salesforce, please report it here: 
http://content.trust.salesforce.com/trust/en/securityalerts
Resolution
To 
grant login access
 there are two methods, depending on whether you are a System Administrator or any other type of user. Learn how below. 
 
System Administrator
Depending on your Organization, the "Setup"
 button will be beside your name at the top right of the page or listed as a drop-down option when you click on your name.
 
1. Choose one of the following:
-Click on 
Setup | My Personal Information | Grant Login Access
.
- Click on
 Your Name | My Settings | Personal | Grant Account Login Access
2. Set the access expiration date for "Salesforce.com Support" (Minimum of 1 month for technical escalations).
3. Click 
Save
.
All Other Users
 
1. Choose 
Your Name | My Settings | Personal | Grant Account Login Access
.
2. Set the access expiration date for "salesforce.com support" (Minimum of 1 month for technical escalations).
3. Click 
Save
.
Note:
 If you're not getting an option to give login access to Salesforce Support, please check your Organization's Login Access Policies (from Setup, click 
Security Controls
 | 
Login Access Policies
), and ensure that "Available to Administrators Only" is not selected for Salesforce.com Support. If it's selected, please change it to "Available to Users."
 
Additional Important Information
 
To expedite your case, please send confirmation to the Salesforce Support rep who is working with you, along with any requested information.  Also provide the Expiration Date of your login access to the agent working your case.
You cannot grant login access if you're 
logged in as another individual
 through their login access. For example, a System Administrator cannot login as their end user and Grant Login Access to Salesforce Support. Similarly, if the Admin has the feature "
Organization Admins can Login as Any User
" privilege, Salesforce Support cannot login as a System Admin and then login as any User in their Organization. If Support needs access to both the end user and the Admin's account, both the Admin and end User will need to grant access separately. 
You have the option to either go back in multiple times and grant login access or you can grant a month as an option.  It is whatever you are most comfortable with providing.  Also once your case has been closed you are encouraged to go back in and set the access back to "No Access".
You can’t grant access to certain support organizations if your administrator has set up restrictions or a packaged application’s license prevents it.
