### Topic: Apex Test History | Salesforce
Apex Test History | Salesforce
Apex Test History
The Apex Test History page shows all the test results associated with a particular test run. The page shows results only for tests that have been run asynchronously.
Available in: Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
From Setup, enter 
Apex
 in the 
Quick Find
 box, and select 
Apex Test History
 to view all test run results for your org. Test results are retained for 30 days after they finish running, unless cleared. 
The Apex Test History page lists the test runs by ID. Click a test run ID to display all the test methods for that test run. You can filter the test methods to show passed, failed, or all test methods for a particular test run.
Click the test class name to view more details about a specific test run.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=code_test_history.htm&language=en_US
Release
202.14
