### Topic: Searchable Fields: Reward Fund Type | Salesforce
Searchable Fields: Reward Fund Type | Salesforce
Searchable Fields: Reward Fund Type
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 editions
Searchable Fields
Sidebar Search
Advanced Search
Global Search
Name
All custom auto-number fields and custom fields that are set as an external ID
(You don't need to enter leading zeros.)
All custom fields of type text, text area, long text area, rich text area, email, and phone
See Also:
Searchable Fields by Object in Salesforce Classic
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=search_fields_reward_fund_type.htm&language=en_US
Release
202.14
