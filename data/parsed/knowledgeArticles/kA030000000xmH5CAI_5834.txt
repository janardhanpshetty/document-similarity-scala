### Topic: There are character limits when replying to last post from Service Cloud
There are character limits when replying to a Twitter or Facebook post from the Service Cloud.
Resolution
The character limits are:
Facebook comments and replies = 30,000 characters or less
Twitter replies  = 140 or less
