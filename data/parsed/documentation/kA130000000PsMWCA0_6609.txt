### Topic: Refreshing Forecasts | Salesforce
Refreshing Forecasts | Salesforce
Refreshing Forecasts
Available in: Salesforce Classic
Available in: 
Professional
 (no Custom Field forecasts), 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Opportunity Splits available in: 
Performance
 and 
Developer
 Editions and in 
Enterprise
 and 
Unlimited
 Editions with the Sales Cloud
Note
This information applies to Collaborative Forecasts and not to 
Customizable Forecasts
.
While you're viewing a forecasts page, your subordinates may be editing related opportunities or adjusting the forecasts of their own subordinates. Or you might be adjusting one of your subordinates' forecasts from your own page. All of these events can change forecast amounts.
When any opportunity update or forecast adjustment occurs for a forecast you've selected in a rollup table, we let you know.
If the selected forecast has adjustments in process (your own or your subordinate's), you'll see a processing indicator (
) next to the forecast.
If any of the selected forecast's related opportunities have been updated, you'll also see the processing indicator in the header of the opportunities list.
A message, 
Processing Changes
, appears in the opportunities list header.
To see the changes, you need to click 
Refresh
.
For example, while Gordon Johnson is viewing his team's opportunity revenue forecast for December, his subordinate Renee Reynolds edits an opportunity she expects to close in December. The opportunity is currently in the Commit forecast category. Renee changes the opportunity amount from $2500 to $2600. This change affects her December forecast, increasing the amount by $100.
When Gordon clicks the forecasts table cell for Renee's December commit forecast, the processing indicator appears within that cell. The indicator tells him that Renee made a change that affects the forecast. He sees the same processing indicator on the opportunities list, so he knows Renee edited an opportunity. He clicks 
Refresh
. The opportunity amount changes from $2500 to $2600, and Renee's Commit forecast for December reflects the $100 increase.
See Also:
Forecasts Adjustments Overview
Adjustments Purges
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=forecasts3_refresh_overview.htm&language=en_US
Release
202.14
