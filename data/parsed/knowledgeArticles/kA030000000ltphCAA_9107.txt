### Topic: Simple steps that an effective Executive Sponsor should take to drive adoption of Salesforce within their company.
Executive Sponsorship is one of the strongest drivers of adoption. Here are a four small actions that you and other leaders within your company can take to help drive adoption:
1. Lead by example and use Salesforce
2. Run your meetings from Reports and Dashboards
3.  Use Chatter to ask for status updates on Opportunities and Accounts
4. Reiterate the benefit that using a tool can have on the users and the business
 
For additional resources on how to drive adoption check out the 
Achieve More with Salesforce Hub
Resolution
