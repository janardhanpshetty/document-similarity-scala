### Topic: What information to provide to support when sending questions related to Analysis Dashboard.
Here are some of the items you should include when submitting a case to Salesforce Marketing Cloud Customer Support when you have questions with your Analysis Dashboard to ensure Support has all the information they need.
 
Resolution
Before submitting a case to Salesforce Marketing Cloud Customer Support for help, refer to the below templates to make sure you are including all the details required for them to assist you. If excluded, Support will reach out to ask for this information so this can save some time for you to help them resolve your issue faster!
You can reach Support by emailing marketingcloudsupport@salesforce.com or if a critical issue, contact our toll free number (1-888-672-3426).
With all cases submitted to Support, please make sure you include the following:
 
Username and Account name.
Screenshot of error message, widget, report, alert, configuration, etc. 
The name of the topic profile you are working with.
Other information including by issues listed below:
Widget Inquiries
Widget Exports
Topic Profile Keyword Inquiries
Alerts
Reports
Source Filters
Widget Inquires
The type of widget are you inquiring about.
Step by step instructions of your configuration so Support can re-create the same widget/issue. Be as detailed as you can. If possible, include a screen shot of the back of the widget to show the configuration.
If there is an error with one of your widgets, please include the widget ID - or screenshot. This can be found by flipping over the widget to see the configuration side. See image below:
How to Find Widget ID
Widget Exports
Widget ID(s) for the Widget(s) you are trying to export data from. See the 
instructions above
 on how to find a Widget ID located on the back of the Widget
Which Browser you are using (i.e. Chrome, Firefox , Internet Explorer, etc.)
Export format you are choosing.
Complete description of what the you are trying to export.
Complete description of what is actually happening:
Are you able to download it if  you email it? Download it?
Topic Profile Keyword Inquiries
Topic Profile Name.
Topic Profile ID number that can be found within the Topic Profile Configuration.
Complete description of what results you are looking for within your Topic Profile.
Complete description of what you are actually experiencing
Screenshots of the Widget(s) you are using to view your results in.
Alerts
When an Alert is created but not received there are two recommended troubleshooting steps a Support Representative will ask you about. 
Have you tried re-creating the Alert?
Have you tested using a hotmail or gmail address in the To: field to see if the Alert is sent? 
Sometimes corporate addresses can not receive Alerts due to firewalls. This knowledge will help Support troubleshoot the issue better. 
Please also include the information below if the Alert is not sent or you are experiencing any other issue with Alerts:
Alert Name
Email Recipients(s)
Date/Time(s) it should have triggered
Complete description of what the you are trying to do with the Alert. Please include step by step so we can try to reproduce the issue as well as including the configuration of the Alert.
Complete description of what is actually happening. 
Reports
When a Report is created but not received there are two recommended troubleshooting steps a Support Representative will ask you about. 
Have you tried re-creating the Report?
Have you tested using a hotmail or gmail address in the To: field to see if the Report is sent? 
Sometimes corporate addresses can not receive Alerts due to firewalls. This knowledge will help Support troubleshoot the issue better.
Have you tried sending it using different formats (HTML/PDF)?
Please also include the information below if the Report is not sent or you are experiencing any other issue with Reports:
Name of Report:
Email address of recipients(s):
Report Format (HTML or PDF?)
Date/Time/Timezone report (should / is scheduled to) have sent:
Widget ID(s) that the Report includes. See 
these instructions
 on how to find the Widget ID.
Complete description of what you are setting the report up to do.
Complete description of what is actually happening.
Screenshot(s) of the Report configuration
Source Filters
Source Filter name.
Topic Profile name you are applying the Source Filter
Source Filter Box you are applying the Filter to (Include All, Include Keyword-Matched Content, Exclude All)
Complete description of what you are trying to do with the Source Filter. Please include step by step so we can try to reproduce the issue.
Complete description of what is actually happening. 
Which widget are you using where the Source Filter results are not working correctly?
Now you should have all you need to submit a case to Salesforce Marketing Cloud Support with your Analysis Dashboard inquiries. 
 
