### Topic: This behavior is related to the 'Edit My Reports' Profile Permission, which gives a User the ability to Save a Report to a Public folder they may only have View access to.
I have a User with solely View access to a Report folder, yet the User is still able to Save Reports to the folder and Edit those Reports within the Report Folder. 
Resolution
This behavior is related to the
 'Edit My Reports' Profile Permission.
This Profile Permission, when enabled, gives a User the ability to Save a Report to a Public folder they may only have View access to. Additionally, this allows them to Edit Reports they've Saved to the folder. Removing this Profile permission removes the ability to save Reports to the Public Folder. However, it preserves their ability to Create, Edit or Clone Reports to their Personal Custom Reports folder. 
NOTE: This Profile Permission will only be available for Organizations with 'Enhanced Folder Sharing' enabled 
 
