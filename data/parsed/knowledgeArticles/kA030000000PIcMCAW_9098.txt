### Topic: Refer to this article if you want to understand why you may be getting an Internal Server Error while managing a Custom Setting.
Issue -
User is getting internal server error when managing custom settings.
Reproducing the issue -
Go to Setup | App setup | Develop | Custom Settings
Click the Manage link to manage an existing custom setting.
Result -
 
Internal server error message is thrown
Resolution
Make the following checks to understand the reason of this error:
Check the fields included in custom setting.
Make sure that they do not have any errors on their default values.
​
For instance, a custom field in custom settings is a date field having the following formula as it's default value:-
DATE(YEAR(Today()) , MONTH(Today()),31)
This formula will fail on records created in February, April, June, September and November, which will throw an Internal Server Error when you manage that Custom Setting
