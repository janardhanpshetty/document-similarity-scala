### Topic: Set Up Enhanced Email | Salesforce
Set Up Enhanced Email | Salesforce
Set Up Enhanced Email
Enhanced Email gives you and your reps a ton of great email functionality, including the ability to relate emails to other records, add custom fields to emails, use triggers with emails, modify the email message layout, and manage emails using the Salesforce API. Enhanced Email is automatically enabled for most organizations except those that use Email-to-Case.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To send email:
“Send Email” permission and access to the record the email is sent from.
To attach files to email or templates:
Access to the file you’re attaching.
From Setup, enter 
Enhanced Email
 in the Quick Find box, then select 
Enhanced Email
.
Click 
Enable
.
Update the Email Message page layout to: 
Add the 
Related To
 field.
Then, your users can see which records are related to an email.
Remove the 
Parent Case
 field from the Email Message page layout. This field is generally blank unless you use Email-to-Case and an email is associated with a case.
See Also:
Improve Email in Salesforce with Custom Fields, Customized Layouts, and a Better Email Detail Page
Considerations for Setting Up Enhanced Email
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=enable_enhanced_email.htm&language=en_US
Release
202.14
