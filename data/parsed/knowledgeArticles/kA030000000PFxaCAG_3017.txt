### Topic: This article let the admin verifies with the user if the routing address is verified after creating a new email to case on demand routing address.
BEHAVIOR:
After creating a new Email to Case On-Demand Routing Address, the administrator may notice that inbound emails redirected to the Email Services Address are creating cases as expected, however some of the configured default values which include:
Case Priority
Case Origin
Case Record Type
Are not taking the preset values.
 
Resolution
CAUSE
:
The behavior generally occurs when the Routing Address has not been verified.  When a new Routing Address is created, a confirmation email will be delivered to the address, where a link must be selected for validation.
The status of Routing Addresses can be viewed under Setup | Customize | Cases | Email-to-Case.  Under the Routing Addresses section, the admin should either see 'Verified' or 'Pending [Resend]'
To address the issue, verify the Routing Address via the confirmation email.
 
