### Topic: Set Up Entitlements | Salesforce
Set Up Entitlements | Salesforce
Set Up Entitlements
Entitlements are units of customer support in Salesforce, such as “phone support” or “web support”. Set up entitlements in your Salesforce org to help support agents determine whether a customer is eligible for support.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions with the Service Cloud
Enable Entitlements
Enable entitlements in your Salesforce org to help support agents deliver the correct service level to your customers.
Customize Entitlements
Customize entitlement fields and page layouts based on your business needs and how your agents work.
Set Up Entitlement and Asset Lookup Filters on Cases
Set up lookup filters on entitlement-related case fields to restrict the entitlements that users can select on a case.
Give Users Access to Entitlement Management
After you set up entitlement management, make sure that users have the appropriate user permissions, field access, and tab access.
Set Up an Entitlement Template
Entitlement templates let you predefine terms of support that users can add to products.
Automatically Add Entitlements to Cases from Web, Email, and Communities
Entitlements don’t automatically apply to cases created using Web-to-Case, Email-to-Case, or communities. However, you can add entitlements to these features using Apex code.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=entitlements_setting_up.htm&language=en_US
Release
202.14
