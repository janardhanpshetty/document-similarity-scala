### Topic: This solution outlines a configuration setup and process leveraging E2C for customers who wish to use their org for internal case management as well as SFDC Cases.
INTERNAL: How to leverage Email2Case for Premier Customers
Resolution
This solution outlines a configuration setup and process leveraging E2C for customers who wish to use their org for internal case management as well as SFDC Cases.
Use Case:
Customer has internal Help Desk
End users are creating cases in their internal org that sometimes need to be logged with Premier Support
Customer wants to eliminate manually logging cases in both places
There is a specific criteria that identifies these types of cases, like "Password Resets" or Record Type, etc.
Create Special Handling Instructions:
If these types of cases will require special handling by our Premier Tier 1 group create a new "Special Handling Solution" for the customer, 
example 
Link the new solution to the account Service Entitlement, use the "Special Handling Instructions" look up field to locate the new solution.
Customer App Configuration:
Created a Workflow Rule with criteria to trigger email to case.
Add an Email Alert that will send an email to 
premiersupport@salesforce.com
 (Prem_Email2Case) which will create a Case in our system and route to the "Global Premier Support" queue.
Use a Text email template that provides relevant case information, this will appear in our 62 org case Description field (see attachment for example).
Alternative Note: To auto populate specific fields in the Premier Support cases the customer can add a Java Script code to one of the Email2Case agent files.
Create a Field Update that will automatically close the internal case.
Internal Case Assignment Rules Configuration:
You may or may not need to filter the incoming email2case in the Assignment Rules.
This can be done by adding "Case: Created By Alias 
equal or not equal
 to eprem"
Internal Outlook Email2Case Inbox Folder:
Once this is set up and active you may want to test and monitor.
To do this add the "Prem_Email2Case" Inbox to see if the email has come in
After the email has come in and a case is created the email is moved to the "Processed" folder
Review this 
INTERNAL - How to access the Premium Support (Premier Support) Email2Case mailbox?
 for Outlook setup
Customer Benefits:
Customer has efficient case management and the benefit of 24 x 7 Premier Support coverage
They continue to see the value of using Premier Support for processes they can't support internally
The Premier Support Designated Rep is supported by Stream
Enables an automatic back up procedure for Designated Rep and eliminates monitoring emails from customers
