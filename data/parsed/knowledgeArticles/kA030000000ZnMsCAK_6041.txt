### Topic: Limited Mass Email functionality is available in some Org Editions that do not have full access
The full functionality of 
Mass Email
 is currently available in the following Salesforce Org Editions:
Available in: 
Professional
,
 Enterprise
,
 Performance
,
 Unlimited
,
 
and 
Developer Editions
The above Org Editions allows properly provisioned user Profiles to send 
Mass Emails
 to Contacts, Person Accounts, Leads, and Users.
System Administrators in other Org Editions not noted above may see limited functionality for 
Mass Emails
.
Resolution
Other Org Editions, such as 
Contact Manager
 Edition will have the ability to utilize the 
Mass Email
 to Users feature.
 
