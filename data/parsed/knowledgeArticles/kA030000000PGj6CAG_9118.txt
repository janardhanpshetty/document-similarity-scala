### Topic: This article was written to explain how to resolve a scenario where a "Data Not Available" error is occurring when self-service portal (not Customer Portal) users enter incorrect login credentials.
This article applies to the older Self-Service Portal (SSP), not the newer Customer Portal feature.
Customers who are using the Self-Service Portal, and who may have been moved from one salesforce instance to another (i.e. from NA1 --> NA13) may report the error below when their SSP users enter incorrect login credentials (username/password) from the login page:
Data Not Available 
The data you were trying to access could not be found. It may be due to another user deleting the data or a system error. If you know the data is not deleted but cannot access it, please look at our support page.
When this problem occurs, it's because the customer was likely recently moved from one instance to another (either because they requested to do so, or because of an instance split), and they have a hardcoded reference in their web page source code that refers their old salesforce instance.  
 
Resolution
If a customer reports this issue, you can confirm if this is the problem by doing the following:
NOTE:
 The example below is given for a customer that was moved from NA1 to NA13, but this could occur for any customer, that has moved from any instance to another.
 
1. Go to the customer's web page where the SSP login is being hosted, right click on the page, and select "view source code",
2. Do a Crtl+F for "salesforce" on the source code pop-up window, and you should notice that the customer has a hardcoded reference to NA1 in their source code: 
 
================================================================
<!--BEGINNING OF HELPDESK LOGIN SECTION--> 
<div style="position:absolute;top:14px;left:6px;width:377px;height:21px;"> 
<img src="images/Login%20Tab.jpg" livesrc="images/Login%20Tab.psd" alt="" height="21" width="377" border="0" /> 
</div> 
<div style="position:absolute;top:47px;left:2px;width:200px;height:161px;-adbe-c:c"> 
<form action=
"https://na1.salesforce.com/sserv/login.jsp
"
 method="post"> 
<input type="HIDDEN" name="orgId" value="CUSTOMERORGID" /> 
================================================================
and again here: 
 
================================================================
<!-- <a style="color: #284C9A; font-weight: bold" href="
https://na1.salesforce.com/sserv/resetpassword.jsp?retURL
=%2Fsserv%2Fresetpassword.jsp%3ForgId%3DCUSTOMERORG ID%26pSr%3D1%26pLu%3D%252Fsserv%252Flogin.jsp%253ForgId%253DCUSTOMERORG ID&orgId=CUSTOMERORGID&pLu=%2Fsserv%2Flogin.jsp%3ForgId%3DCUSTOMERORGID">Forgot your help desk password?</a></font> --> 
================================================================
 
 
In this particular scenario, the customer was on the NA13 instance, not NA1 instance used in the source code above. If you login to the customer org, and go i to the SSP settings page,
 
https://naX.salesforce.com/setup/sserv/selfservicesetupdetail.jsp?setupid=CaseSelfserviceSettings&retURL=%2Fui%2Fsetup%2FSetup%3Fsetupid%3DCaseSelfservice 
 
and click on the Generate Login HTML, the proper code that they should be using is: 
 
================================================================
<FORM ACTION=
"https://na13.salesforce.com/sserv/login.jsp" 
METHOD="POST"> 
<INPUT TYPE="HIDDEN" NAME="orgId" VALUE="CUSTOMERORGID"> 
 
<label for="un">Username:</label> 
<INPUT TYPE="TEXT" id="un" NAME="un"> 
<BR> 
 
<label for="pw">Password:</label> 
<INPUT TYPE="PASSWORD" id="pw" NAME="pw"> 
<BR> 
 
<INPUT TYPE="SUBMIT" VALUE="Login"> 
</FORM> 
================================================================
If this particular problem is found, the customer has to update their source code, to remove all the hardcoded references to their old instance, and update them with the references for their current instance.
 
If a customer reports this problem, and the steps above do not work to resolve the issue, or you're not able to use the steps to 100% confirm the root cause, escalate the case to the next support tier for further investigation.
