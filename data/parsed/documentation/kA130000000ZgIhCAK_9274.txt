### Topic: Transfer Chats | Salesforce
Transfer Chats | Salesforce
Transfer Chats
You can transfer chat sessions to other agents when a customer needs extra help with an issue, or to make room for new requests.
Available in: Salesforce Classic
Live Agent is available in: 
Performance
 Editions and in 
Developer
 Edition orgs that were created after June 14, 2012
Live Agent is available in: 
Unlimited
 Edition with the Service Cloud
Live Agent is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions
User Permissions Needed
To chat with visitors in Live Agent in the Salesforce console:
Live Agent is enabled, set up, and included in a Salesforce console app
While chatting with a customer, click 
Transfer
.
Select a transfer option from one of the menus.
Note
There are options for each type of transfer that’s enabled for your Salesforce org. If you don’t see the option you need, ask your administrator to add it for you.
(Optional) Write a message for the agent receiving the chat. This message is part of the chat request to provide context for the next agent.
If the transfer is accepted, your chat and any associated records automatically close (don’t worry, you’ll be prompted to save your changes if you haven’t already). If it’s rejected, you can try again with another recipient or transfer method.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=live_agent_transfer_chat_requests.htm&language=en_US
Release
202.14
