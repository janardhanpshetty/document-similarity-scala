### Topic: Salesforce CRM Content | Salesforce
Salesforce CRM Content | Salesforce
Salesforce CRM Content
Organize, share, search, and manage all types of files within your organization.
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Salesforce CRM Content Overview
Organize, share, search, and manage content within your organization and across key areas of Salesforce with Salesforce CRM Content. Content includes all file types, from traditional business documents such as Microsoft® PowerPoint presentations to audio files, video files, Web pages, and Google® docs.
View and Edit Libraries
Manage Libraries
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=content_parent.htm&language=en_US
Release
202.14
