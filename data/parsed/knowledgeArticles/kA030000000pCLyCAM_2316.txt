### Topic: Orders object is not fully supported in Lightning Experience
When using Lightning Experience in Winter '16, the Orders object is not fully supported
.
For example, mouse overs of lookup fields on the Orders object may not work as intended. The workaround until Orders are fully supported is to switch to Aloha Classic when working with Orders in the meantime.
 
Resolution
For more information on Lightning limitations: 
https://help.salesforce.com/apex/HTViewHelpDoc?id=lex_gaps_limitations_sales.htm&language=en_US
