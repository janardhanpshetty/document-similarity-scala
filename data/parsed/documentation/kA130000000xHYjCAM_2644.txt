### Topic: Configure Authentication Providers | Salesforce
Configure Authentication Providers | Salesforce
Configure Authentication Providers
External users can log in using their credentials from Facebook©, Janrain©, or another Salesforce organization if you set up authentication providers on the Auth. Providers page in Setup and choose to display them on the community login page.
Note
The following information assumes that you are familiar with the use of authentication providers for single sign-on.
If you’re using a custom Visualforce login page instead of the default login page, use the 
Single Sign-On Initialization URL
 from an Auth. Provider detail page as the target URL of a custom login button. For example: 
https://login.salesforce.com/services/auth/sso/
orgID
/
URLsuffix
?community=https://acme.force.com/support
If you’re using Janrain as the authentication provider you can pass the following to the Janrain login widget that’s deployed on your site.
janrain.settings.tokenUrl='https://login.salesforce.com/services/authcallback/
orgID
/
URLsuffix
'​+'?flowtype=sso&community='+encodeURIComponent('https://acme.force.com/customers');
See Also:
About External Authentication Providers
Using Request Parameters with Client Configuration URLs
Customize Login, Logout, and Self-Registration Pages in Your Community
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_auth_providers_configure.htm&language=en_US
Release
202.14
