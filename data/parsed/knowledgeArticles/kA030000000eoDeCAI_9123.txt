### Topic: This articles provides information to sign up an org with a template via the API for Partners.
In order to sign up an org with a template via the API, partners must have their source org approved. 
 
Partners will see this message if they do not have their source org approved yet:
  
Pre-requisites:
 Security Review & Contract
Information Needed:
 Application name, source org ID
Resolution
1. Verify partner has passed
 security review 
and 
contract
.
2. Take the org ID and open the record up in blacktab
3. Click "Installed Packages" on the left hand navigation
4. For each non-Salesforce (or Salesforce foundation) package, 
check in org62 to make sure the package has been through security review. 
5. Once you have verified all packages in the source org has been through security review, set the following:  "Trialforce Template Max Trial Length" set to 30, and verify "Trialforce Export" checkbox equals true. If not, you need to enable this checkbox.
6. *
Important Tenant License Verification*: 
Verify field, 
"License Editor has been used in the Org," 
equals true. This ensures the org was updated to the latest provisioning architecture using tenant licenses. If checkbox is false, use the License Editor button to rebuild the org with the same base edition and licenses. For more info, check this doc: sfdc.co/tsocomms
7. Leave this case comment and then close the case:
~~~
Hi,
I've approved your source org. Templates created from this org can now be used for signup API and environment hub. 
 
Your source org has been approved to spin 30 day trials, if you need this time frame adjusted, please log a case, provide your business justification, and include if your source org is used for creating internal test/demo orgs or customer facing trials. 
 
More information on the trialforce process can be found at p.force.com/trialforce
 
Regards, 
Salesforce Partner Support
~~~
 
