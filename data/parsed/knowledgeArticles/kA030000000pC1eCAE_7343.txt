### Topic: When you attempt to reply to a Facebook post using Social Customer Service in the social post feed, none of your Facebook accounts appear in the Managed Social Account dropdown menu
When you attempt to reply to a Facebook post using Social Customer Service in the social post feed, none of your Facebook accounts appear in the Managed Social Account dropdown menu.
Resolution
If there aren't any Facebook accounts to select when you attempt to reply to a Facebook post in the social post feed, ensure that:
 
Social accounts are registered in your Salesforce organization.
If you don't have any social accounts registered yet, see 
Set up Social Customer Service
 for steps on how to add social accounts to and reply to posts from your Salesforce organization.
 
You (the agent replying) have the Assigned Social Account permission for the Facebook account you expect to see.
If you don't have have the Assigned Social Account permission, see
 Set up Social Customer Service
 for steps on how to assign social profiles to a profile or permission.
 
The Facebook post was brought in from a social account registered in your organization and not a Topic Profile.
Facebook broad listening (Topic Profile) posts are sent to Salesforce for reporting, but you cannot reply to them from a social account that you have registered in your organization. To identify whether a Facebook post is from a Topic Profile or a Social Account, go to the social post record. Under "Topic Type", identify if the type is Keyword or Managed.
  
Keyword
The post was brought in as part of a broad listening Topic Profile. You cannot reply to this post from Salesforce. Facebook posts with the Topic Type "Keyword" have a blank Managed Social Account dropdown menu when you try to reply to them.
Managed
The post was brought in from a social account that is registered in your organization. If you have the correct Assigned Social Account permission, it is possible to reply to this post from Salesforce.
 
  
