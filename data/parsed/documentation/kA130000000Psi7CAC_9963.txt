### Topic: Which Data.com user type am I? | Salesforce
Which Data.com user type am I? | Salesforce
Which Data.com user type am I?
It’s easy to find out. From your personal settings, enter 
Advanced User Details
 in the 
Quick Find
 box, then select 
Advanced User Details
. 
No results? Enter 
Personal Information
 in the 
Quick Find
 box, then select 
Personal Information
.
The 
Data.com User Type
 field identifies you as either a Data.com User or a Data.com List User. If the field is blank, you do not have a Data.com user license.
For a quick definition of your Data.com user type, just click the info icon (
) next to the field.
See Also:
How Do Data.com User Types, Licenses, and Record Addition Limits Work?
Using Data.com FAQ
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_datadotcom_how_do_i_know_which_license_i_have.htm&language=en_US
Release
202.14
