### Topic: Manage Your Salesforce Org’s Sales Territories | Salesforce
Manage Your Salesforce Org’s Sales Territories | Salesforce
Manage Your Salesforce Org’s Sales Territories
Use Enterprise Territory Management to manage and maintain your org’s sales territories. Create territory types, build a model, and then add and test your account assignment rules. When you’re satisfied with your model, activate it, then assign users and accounts. Roll it out to your org, and then run reports to assess its impact and make adjustments if necessary.
Available in: Salesforce Classic
Available in: 
Developer
 and 
Performance
 Editions and in 
Enterprise
 and 
Unlimited
 Editions with the Sales Cloud
User Permissions Needed
To manage territories:
“Manage Territories”
Note
This information applies to Enterprise Territory Management only, not to previous versions of Territory Management.
Before you begin building your territory model, plan your approach. Choose organizing characteristics such as industry, annual revenue, or location information to divide your organization’s accounts into logical segments that work for the way you do business.
Enable Enterprise Territory Management
Starting with Winter ’15, Enterprise Territory Management is ready to be enabled by administrators in new Salesforce organizations.Organizations created before Winter ’15 need to call salesforce.com to enable the feature. Enterprise Territory Management cannot be enabled in existing organizations that have Customizable Forecasting enabled.
Define Enterprise Territory Management Settings
Define and configure settings for Enterprise Territory Management.
Use Chatter to Collaborate on Territory Models
Enable Chatter Feed Tracking on the Territory Model object to collaborate on model development directly within model records. Your team can post and respond to comments, attach files, and get notifications when model states change or key fields are updated.
Build a Territory Model
A territory model organizes all the elements of your organization’s territory management plan, including a territory hierarchy, account assignments, and user assignments. Keep your model in the 
planning
 state as you build your hierarchy, define assignment rules for territories, add users to territories, and run your rules to see the resulting account assignments.
Manually Assign One or More Territories to an Account
Assign one or more territories to an account directly from the account record. Only territories that belong to models in 
Planning
 or 
Active
 state can be assigned to accounts.
Manually Assign One or More Assignment Rules to a Territory
Assign one or more object assignment rules to a territory directly from the territory record. Available rules come from the territory model the territory belongs to.
Manually Assign a Territory to an Opportunity
On an opportunity record, you can assign and track the territory whose assigned sales reps work that opportunity. Manual territory assignments are controlled by your sharing access to the opportunity’s assigned (parent) account. When you assign a territory to an opportunity, that opportunity is shared with all Salesforce users assigned to that territory's parent in the territory model's hierarchy.
Run the Opportunity Territory Assignment Filter
The opportunity territory assignment filter automatically assigns territories to opportunities based on the filter logic in the Apex class.
Clone a Territory Model
Cloning lets you make a copy of a territory model that you can use to test out different territory characteristics. The new model includes the original’s territories, assignment rules, users, and manually assigned accounts. Only models in 
Planning
, 
Active
, or 
Archived
 state can be cloned. When cloning is complete, the new model is in 
Planning
 state.
Assign Users to Territories
Assign users to the territories they will operate in to sell products and services. You can assign users to territories that belong to models in 
Active
 or 
Planning
 state, though assignments made within 
Planning
 state models are for planning purposes only: they 
do not
 affect user access to records.
Identify Territory Users by Territory Role
Keep track of user functions within territories by creating territory roles and assigning them to territory users as needed. Users can even have different roles in different territories.
Activate a Territory Model
When you’re satisfied with the structure of your territory model and its territory account assignments, you’re ready to activate it. Remember that you can also maintain models in 
Planning
 and 
Archived
 states for further modeling and reference.
Configure Enterprise Territory Management Permissions and Access for Administrators and Users
An important step in implementing Enterprise Territory Management is making sure the right users can access the right territory model elements, records, and record elements.
Run Assignment Rules for a Territory
Run account assignment rules for any territory that has rules defined and belongs to a territory model in 
Planning
 or 
Active
 state. If your territory is in 
Planning
 state, running rules lets you 
preview
 account assignments. If your territory is in 
Active
 state when you run rules, accounts are assigned to territories according to your rules.
Find Out Which Territories an Assignment Rule Applies To
If you use rules to assign accounts to territories, it can be helpful to find out which territories a single rule applies to.
See Also:
Enterprise Territory Management Concepts
Enterprise Territory Management
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tm2_manage_organization_territories.htm&language=en_US
Release
202.14
