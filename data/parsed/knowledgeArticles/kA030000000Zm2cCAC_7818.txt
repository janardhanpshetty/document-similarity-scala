### Topic: Help you to add logos, banners, and videos to your created package in App Exchange.
How to add videos, logos, banners to your created packages while creating/editing listings in App Exchange.
Resolution
Listings are your primary marketing tool for promoting your app or consulting service on the AppExchange. The more information you add to your listing, the more likely it is that users can find it. Create one listing for each app or consulting service. Editing "Uploaded Content tab" will give you the option to add logos, banners, and videos.
 
Uploaded Content Tab:
-
 
On the Uploaded Content tab you can add or remove files for your listing such as a
 logos, banners, and videos
.
  
Field
Logo
                                                             
Upload your listing's logos. Your logos show up in many places on the AppExchange, including on your listing, in search Logo results, and on the home page. Check out our guidelines for creating a great logo. As long as your listing is private, if you do not upload listing logos, AppExchange displays default logos. To make your listing public, you must upload small, tile, and high resolution logos, as described below.
 •     60x60 Logo–Upload a PNG file up to 100 KB. The log must be exactly 60 × 60 pixels.
 •     280x205 Logo–Upload a PNG file up to 300 KB. The logo must be exactly 280 × 205
        pixels.
 •     High Resolution Logo–Upload a high-resolution PNG file, with a transparent background,                     up to 10 MB. This logo is for marketing purposes only and is not used on the site.       
Effects like shadows are automatically applied, so you don’t need to include them in your logo.
 
Banner
Upload a banner for your listing. This banner is shown on your listing, summary page. You can upload a PNG file up to 1 MB. The image should be exactly 1200 × 300 pixels.
 
Video
 Provide a video about your listing. You have two options.
•  Specify a URL to view a video in a new page. This takes the customer away from your listing
    page, but enables you to generate leads.
•  Embed a YouTube video in the same page. This provides an integrated experience for the
   customer, but doesn’t enable you to generate leads. You will need to specify a caption for the video.
Images
Upload up to eight images of your listing. There is no restriction on the size of the image. Files must be PNG and up to 1 MB.
 
Resources
Upload up to six whitepapers, testimonials, case studies, data sheets, webinars, customization guides, or other resources that Resources can be hosted on the AppExchange or elsewhere. Resources can be PDF files up to 10 MB.
 
 
Logo Guidelines
 
        Logos are more than mere decorations. A great logo communicates to your customers that they can trust your app or service
        because you have taken the time to invest in your brand. It also conveys key attributes about your offering.
        
Tips for creating a great logo:
 
         1. Hire a designer or leverage a crowd-sourced logo service—you can find these online and set the price you wish to pay for your logo.
         2. Don't just use your company logo—giving your app or service its own logo and name provides a platform for expanding your offering.
         3. Use imagery people recognize—you want people to get it right away, not spend time deciphering obscure references.
         4. Keep it simple—avoid photos, wordiness, and complex imagery.
         5. Spend time looking at your competitors and at popular apps in the consumer space—leverage the market for inspiration.
         6. Test your logo in different contexts—on white backgrounds, black, and next to your company logo.
 
 
 
