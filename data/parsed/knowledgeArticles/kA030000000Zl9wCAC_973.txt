### Topic: This article explains why customers receive emails containing the error 1918307596 (stack trace) and how to resolve it.
Customers receive lot of emails in the below sample format: Each one of the emails has an Internal Server error with the same stack trace id: 1918307596
 
******************************************************************************
From: noreply@salesforce.com [mailto:noreply@salesforce.com] On Behalf Of yourchoice@abc.com
Sent: Tuesday, January 28, 2014 12:13 AM 
To: First Name, Last Name
Subject: Salesforce.com workflow could not perform a pending action 
 
Details: 
Object Type: Lead 
 
Record: XYZ
https://na5.salesforce.com/00Q7000000tx7mB 
 
Workflow Rules attempted: MODTD Close Leads When End Date Reached
https://na5.salesforce.com/01Q70000000IW7u 
 
Workflow Actions attempted: ]
MODTDLeadClosedByEndOfCampaign 
https://na5.salesforce.com/04Y70000000PLtx 
MODTD Change Status to Closed 
https://na5.salesforce.com/04Y70000000PLu1 
 
Error Number: 862189659-56245 (1918307596) 
 
Date: 28 Jan 2014 05:12:37 GMT 
******************************************************************************
Resolution
System Administrators receive this error as part of the standard Salesforce behavior. When the Time Based Workflow fails to work due to governor limits being hit for a particular trigger, an email is sent to the administrator.
Steps to stop these emails from coming in and make the workflow work:
1. Using the stack trace ID: 1918307596, the organization id, and the instance, we can find the name of Trigger which is throwing this error
2. In Splunk, run the following query 
index=instance Org-Id Stacktrace-Id
or following the example email from the Description: 
index=na5 00D7000000tx7mB 1918307596 
3. Select the appropriate date range and run the query
4. From the Splunk logs find the following line
***** 
Cause0: common.exception.ApiException: Trigger: System.LimitException: Too many SOQL queries: 101 : "Trigger ID" 
***** 
5. Use the Trigger ID to find which trigger is causing the problem.
6. Deactivate the trigger which will solve the problem OR suggest the customer to contact the Developers of the trigger to rectify the issue.
