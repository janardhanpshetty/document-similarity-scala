### Topic: This article states if the Organization Wide Address has a limit.
Is there a limit on how many Organization-Wide Addresses an Organization can create? 
Resolution
The system administrator can create as many Organization Wide Addresses as needed (unlimited).
 
