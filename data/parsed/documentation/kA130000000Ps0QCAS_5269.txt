### Topic: CTI 3.0 and 4.0 SoftPhones | Salesforce
CTI 3.0 and 4.0 SoftPhones | Salesforce
CTI 3.0 and 4.0 SoftPhones
Salesforce console available in Salesforce Classic and App Launcher in Lightning Experience. Setup for Salesforce console available in Salesforce Classic.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
A SoftPhone is a customizable call-control tool that appears to users assigned to a call center with machines on which CTI adapters have been installed. SoftPhones built with versions 3.0 and 4.0 of the CTI Toolkit display in the footer of the Salesforce console or in the sidebar of every Salesforce page. Although administrators and developers can customize CTI 3.0 and 4.0 SoftPhones, they usually include the following components:
Call center state area
Includes a drop-down list that lets you specify whether you're ready to receive calls. See 
Changing Your Call Center State
.
Phone line header
Shows the status of the phone line. A status icon changes colors and blinks (
), and provides a text description. You can click the name of the line (Line 1) to show or hide the line's dial pad and call information area.
Call information area
Shows data related to the call, such as the phone number the customer used to dial, the duration of the call, and links to any records associated with the call.
Call button area
Shows buttons that let you make call commands, such as dialing, hanging up, putting a caller on hold, transferring, conferencing, and opening a second line while on a call. See 
Using the SoftPhone
.
My Calls Today report
Opens a report of all the calls you've made or received in the last day.
SoftPhone logo
Displays a customizable logo for each CTI adapter.
Note
Some Salesforce CRM Call Center features that are described in this help system might not be available with your SoftPhone because of customizations that have been made for your organization or the 
CTI Toolkit
 with which your SoftPhone was built. See your administrator for details.
See Also:
SoftPhone Overview
Call Center Overview
Salesforce CTI Toolkit Overview
Salesforce Console
Using the SoftPhone
Tip sheet: Getting Started with your SoftPhone
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cti_3_softphones.htm&language=en_US
Release
202.14
