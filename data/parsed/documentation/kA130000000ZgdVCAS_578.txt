### Topic: Wave Platform Setup | Salesforce
Wave Platform Setup | Salesforce
Wave Platform Setup
Set up your organization to use the Salesforce Wave Analytics platform by enabling Wave, assigning permission set licenses, and then creating and assigning permission sets.
Available in Salesforce Classic and Lightning Experience.
Available for an extra cost in 
Enterprise
, 
Performance
, and 
Unlimited
 Editions. Also available in 
Developer
 Edition
.
Important
If you purchased a Wave Analytics Platform license before October 20, 2015 with Analytics Cloud Builder or Analytics Cloud Explorer permission set licenses: Read 
Set up the Wave Analytics Platform With Licenses Purchased Before October 20, 2015
. If you’re migrating users from Builder or Explorer licenses to the new Analytics Cloud - Wave Analytics Platform license: See 
Migrating From Wave Licenses Purchased Before 10/20/2015 to New Wave Platform Licenses
 before you start the setup process for those users.
Note
The information here describes how to set up your organization to use the Wave platform. Read 
Prebuilt Wave Apps
 to get started with and set up Sales Wave, Service Wave, and other apps.
Each Analytics Cloud - Wave Analytics Platform license is a single-user license that provides access to the Salesforce Wave Analytics cloud. It includes single Analytics Cloud - Sales Wave Analytics App and Analytics Cloud - Service Wave Analytics App single-user licenses. The table shows data storage limits for each license. If you require more data, you can purchase Analytics Cloud - Additional Data Rows, which entitles you to an additional 100 million rows.
License
Limit
Analytics Cloud - Wave Analytics Platform
100 million rows
Analytics Cloud - Sales Wave Analytics App
25 million rows when used without Analytics Cloud - Wave AnalyticsPlatform license. Use of Sales Wave app license does not increase data limit for platform license
Analytics Cloud - Service Wave Analytics App
25 million rows when used without Analytics Cloud - Wave AnalyticsPlatform license. Use of Service Wave app license does not increase data limit for platform license
Analytics Cloud - Additional Data Rows
100 million rows
Important
Wave Analytics license data storage limits are contractual, not technical. Licensee agrees to strictly monitor its total number of data rows.
Wave Platform Setup Process
Follow these basic steps to set up your organization to use the Wave Analytics platform.
Enable the Wave platform.
 The first step when you set up Wave is simply to enable it for your organization.
Assign permission set licenses to users.
 Next you assign an Analytics Cloud - Wave Analytics Platform permission set license to each user. Each permission set license can be assigned to only one user. Also, the user license that’s associated with the user profile must support the Analytics Cloud - Wave Analytics Platform permission set license. Not all user licenses support the Wave platform permission set license.
Define user types and create and assign permission sets.
 Next, define the main user types in your organization and create permission sets to group related user permissions according to each user type’s needs. After you create permission sets, assign them to users. Although you can assign a permission set to an individual user, for efficiency, you can also assign it to groups of users. You can assign multiple permission sets to a user.
Walk Through It: create, edit, and assign a permission set
.
You can assign a Wave permission set license along with any of the following Salesforce user licenses:
Force.com (app subscription)
Force.com (one app)
Full CRM
Salesforce Platform
Salesforce Platform One
Note
If you disable Wave Analytics, user permissions are removed from each defined permission set. If you re-enable Wave Analytics later, you must define the permission sets again.
Tip
For best results, follow the steps for setting up the Wave Analytics platform in the order shown here.
Learn About Wave Analytics Cloud Permission Set Licenses and User Permissions
Wave Analytics permission set licenses enable the permissions to explore data with and manage the Salesforce Wave Analytics platform. They also let Salesforce Community users view Wave dashboards.
Identify Wave Analytics Platform User Types
Identifying types of Wave platform users helps assure that the setup process meets your team’s analytics needs.
Enable Wave Analytics and Assign Analytics Cloud - Wave Analytics Platform Permission Set Licenses
After Salesforce provisions you with the Wave Analytics platform license, enable Wave and assign Wave Analytics permission set licenses.
Create Wave Analytics Permission Sets
To give users in your organization access to Wave features, create and assign one or more permission sets based on the level of analytics capabilities they need.
Assign Wave Analytics Permission Sets to Users
Assign permission sets to one or more users—either one at a time, or in bulk—to give them access to Wave functionality.
Complete Setting up the Wave Analytics Platform
Enhance the Wave platform user experience and fine-tune Wave access to Salesforce data with optional setup procedures.
See Also:
Set up the Wave Analytics Platform With Licenses Purchased Before October 20, 2015
Migrating From Wave Licenses Purchased Before 10/20/2015 to New Wave Platform Licenses
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_help_setup.htm&language=en_US
Release
202.14
