### Topic: Overview of each bounce reason, including category, reason, and bounce response ID number, to explain why an email bounced to a subscriber.
Resolution
Anytime a Marketing Cloud Send to one of your subscribers bounces, the recipient domain MTA sends us a bounce message with a bounce reason. This table describes each of those reasons along with a description.
Note
: Actual bounce reasons may vary because each Domain Admin has direct control over the bounce messages relayed back to the sender. For more information about bounce handling, read our 
"Bounce Mail Management" documentation
.
 
ID Number
Reason
1001
Complaints
Your email is blocked due to complaints.
1002
Spamblocked
The remote mail server has rejected this message due to characteristics of spam.
1004
Content
Message was filtered due to content.
1005
URL Block
Emails containing your URLs are blocked.
1009
High Unknown Address Pct.
Email is blocked due to the high quantity or percentage of unknown or inactive addresses on your list.
1010
Authentication
Message lacks required authentication.
1999
Other
This email has been blocked by recipient.
2001
User Unknown
Address is non-existent at the domain
2002
Domain Unknown
Recipient domain doesn't exist.
2003
Bad Address
Syntax Email address in invalid.
2999
Other
This address does not accept mail.
3001
Mailbox Full
Recipient's mailbox is full or has exceeded storage allocation.
3002
Inactive Account
Address is temporarily unavailable.
3003
Temporary Domain Failure
Receiving domain temporarily unavailable.
3999
Other
Mailbox temporarily unavailable.
4001
Server Too Busy
Receiving email server is temporarily overwhelmed with delivery attempts, from you and other senders.
4002
Data Format Error
Email is rejected due to formatting or line length errors.
4003
Network Error
Connection lost or timed out during delivery.
4999
Other
Failed due to temporary failure or indecipherable bounce message.
9999
Unknown
Unknown.
