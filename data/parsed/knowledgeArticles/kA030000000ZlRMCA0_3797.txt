### Topic: A possible workaround to consider is to give view access to users to whom the report is to be emailed. Then the picklist will give option to select these users.
“Schedule Future Runs” doesn’t give a picklist to select Public Groups/Roles, etc when “To me and/or others” is selected under “Email Report” when the folder containing report has a Sharing setting View access to “All internal Users”.
Note:
This applies for all org's, that only have internal users, when eg a portal or community is enabled, you will have more options to share and schedule for, depending on your setup.
Resolution
Workaround:
Give view access to users to whom the report is to be emailed. Then the picklist will give option to select these users. 
Below is the link for the idea:
https://success.salesforce.com/ideaView?id=08730000000kzD5AAI
    
