### Topic: Chatter Email Settings and Branding | Salesforce
Chatter Email Settings and Branding | Salesforce
Chatter Email Settings and Branding
Chatter email settings let you control whether users receive email notifications about new posts, comments, and other changes. You can also change the format of the emails, like add your company’s logo and sender information.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
If Chatter is enabled, email notifications are enabled by default. However, your users can configure their own email settings. They control whether they want to receive emails, for which changes, and how often. It is important that you teach your users how they can control the amount of email they receive. Too many email notifications are a pain point for many Chatter users and a common barrier to adoption.
Configure the general Chatter email settings and branding from Setup. Enter 
Email Settings
 in the 
Quick Find
 box, then select 
Email Settings
. This table describes the settings.
Setting
Description
General Settings
Allow Emails
Enables or disables email notifications for your entire company. If you deselect this option, all email notifications are disabled and your users don’t receive any notifications.
Allow Email Replies
If selected, users can reply to email notifications about messages and comments via email instead of navigating to the comment or message in Chatter.
Allow Posts via Email
If selected, users can post to groups using email.
Allow Attachments via Email
If selected, users can include attachments in posts to groups using email.
Show Salesforce1 mobile app download badges
Show App Store and Google Play download badges for Salesforce1 in all Chatter email notifications from your internal org. Badges don’t appear in email notifications from communities. A recipient’s language setting affects whether badges appear: if a badge isn’t translated in the set language, it doesn’t appear in the email. The option to show badges is selected by default.
Allow API-only Chatter Digests
Note
This setting appears only if API-only Chatter Digests are enabled for your company. To enable this feature, contact Salesforce.
If selected, daily and weekly scheduled digests are disabled for your entire company. You must call the API to receive digests for your users.
Sender
From Name
The name that appears as the sender’s name in the email notification. For example, add your company’s name.
Email Address
The email address of the sender. For example, add your company’s email address.
Branding
Logo
A logo that appears in the email notification. Upload a different logo to replace the default Chatter logo.
Footer Text
The text that appears in the footer of the notification email. We strongly recommend including you company’s physical address to comply with applicable anti-spam laws.
Customize Branding for Email Notifications
Customize your company’s Chatter email notifications to display sender information, footer text, and a footer logo that is personalized for your organization.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_admin_email_settings.htm&language=en_US
Release
202.14
