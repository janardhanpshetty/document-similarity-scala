### Topic: Steps to test whether the Twitter Managed Account is connected.
As a best practice, after registering your Twitter Managed Account or changing your password for your Twitter Managed Account in Twitter, you should ensure the Twitter Managed Account is connected.
For information on how to Register your Twitter Managed Account you can view the 
Twitter Managed Account Guide
 that is located on the 
Engagement Console Quick Start
.
Resolution
There are two ways to check if the Managed Account is not connected.
1. The icon does not appear on the account in Social Account Manager
2. Using a Widget in the Analysis Dashboard
 
Icon in Social Accounts Manager
1. Log into 
Social Accounts Manager.
2. Check if the Managed Account Icon appears on your Twitter Managed Account.
An orange silhouette icon will appear on your Twitter Managed Account to designate it is a Managed Account. If it is not registering as a Managed Account, no icon will appear and it will be registered as a personal Twitter Account.
 
 
Analysis Dashboard Widget
1. Log into the
 Analysis Dashboard 
using your Radian6 Credentials
2. Open up a 
Topic Analysis Widget
3. Select the 
Data Source
 as Twitter Managed Account
 
4.
 
Select the
 Twitter Handle
 listed under the Available Profiles.
5. Click the “
+” button
 to move it to the Selected Profiles. If you receive the error below, the Twitter Managed Account is not connected:
 
If the Twitter Managed Account is connected correctly, it will not show an error.
