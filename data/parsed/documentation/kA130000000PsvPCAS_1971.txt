### Topic: Batch Mode | Salesforce
Batch Mode | Salesforce
Batch Mode
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Note
The Data Loader command-line interface is supported for Windows only.
You can run Data Loader in batch mode from the command line. See the following topics:
Installed Directories and Files
Encrypt from the Command Line
Upgrade Your Batch Mode Interface
Data Loader Command-Line Interface
Configure Batch Processes
Data Loader Process Configuration Parameters
Data Loader Command-Line Operations
Configure Database Access
Map Columns
Run Individual Batch Processes
Data Access Objects
Note
If you have used the batch mode from the command line with a version earlier than 8.0, see 
Upgrade Your Batch Mode Interface
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=loader_batchmode.htm&language=en_US
Release
202.14
