### Topic: Reasons why posts will show up in Google but not in Radian6
Why am I able to see relevant results in Google that do not appear in Radian6?
Resolution
Radian6 gathers content through RSS feeds, direct API access to sites or through our forum providers.  This allows us to focus on pulling in quality content and reduce noise and spam. Additionally, site owners are able to pose crawling restrictions that will allow some crawlers to access content while disallowing others.
Many site owners want their site to be indexed by Google due to its popularity and so that it shows up in searches. However, they may not necessarily want other crawlers to access their content. Google also indexes a lot of content that is not social media content.For more information on how Google indexes content, please see
 How Google Works
.
