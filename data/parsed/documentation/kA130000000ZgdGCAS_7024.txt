### Topic: Predicate Expression Syntax for Datasets | Salesforce
Predicate Expression Syntax for Datasets | Salesforce
Predicate Expression Syntax for Datasets
You must use valid syntax when defining the predicate expression.
The predicate expression must have the following syntax:
<dataset column> <operator> <value>
For example, you can define the following predicate expression for a dataset:
'UserId' == "$User.Id"
You can create more complex predicate expressions such as:
(‘Expected_Revenue’ > 4000 || ‘Stage Name’ == "Closed Won") && ‘isDeleted’ != "False"
Consider the following requirements for the predicate expression:
The expression is case-sensitive.
The expression cannot exceed 1,000 characters.
There must be at least one space between the dataset column and the operator, between the operator and the value, and before and after logical operators. This expression is not valid: 
‘Revenue’>100
. It must have spaces like this: 
‘Revenue’ > 100
.
If you try to apply a predicate to a dataset and the predicate is not valid, an error appears when any user tries to query the dataset.
Dataset Columns in a Predicate Expression
You include at least one dataset column as part of the predicate expression.
Values in a Predicate Expression
The value in the predicate expression can be a string literal or number literal. It can also be a field value from the User object in Salesforce.
Escape Sequences
You can use the backslash character (\) to escape characters in column names and string values in a predicate expression.
Character Set Support
Wave Analytics supports UTF-8 characters in dataset column names and values in a predicate expression. Wave Analytics replaces non-UTF-8 characters with the UTF-8 symbol (
). If Wave Analytics has to replace a non-UTF-8 character in a predicate expression, users may experience unexpected query results.
Special Characters
Certain characters have a special meaning in Wave Analytics.
Operators
You can use comparison operators and logical operators in predicate expressions.
Sample Predicate Expressions for Datasets
Review the samples to see how to structure a predicate expression.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_datasets_predicate_syntax.htm&language=en_US
Release
202.14
