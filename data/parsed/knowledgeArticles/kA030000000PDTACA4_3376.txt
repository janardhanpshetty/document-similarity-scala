### Topic: Why do I receive 'Your account has been disabled' error when adding a comment to a case?
When adding a comment to a case I receive the following error:
 
Your account has been disabled
Your company administrator has disabled access to the system for you. Please contact your administrator for more information.
Resolution
This error occurs when any user adds a comment to a case owned by an inactive user. This is not expected behaviour and is something that is to be addressed in a future release. In the meantime, the workaround is to assign the case to an active user. This will allow any user to add a comment to the case without any error.
