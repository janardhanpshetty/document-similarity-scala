### Topic: When a user creates a new Account, the Account Territory doesn't get set to the user's Territory, why not?
QUESTION
: When a user creates a new Account, the Account Territory doesn't get set to the user's Territory, why not?
Resolution
ANSWER
: Account owner does NOT determine the Territory - unless part of a rule.
BACKGROUND
:
Every Account can have zero, one or more Territories at any given time:
Can be assigned manually clicking [Change] Territory link on Account record.
Can be assigned automatically using Account Territory Assignment Rules.
If both of these are done, ALL manually/automatically assigned territories are included on the Account.
SOLUTION
:
To make the Owner field automatically determine and set the Account's Territory make a rule:
Setup | Manage Territories | Hierarchy
Click into desired Territory.
Scroll down to "Account Assignment Rules Defined in This Territory" section.
Click on Manage Rules, then click on New:
Name the rule, set the criteria.
"Apply to child territories" checkbox - optional.
"Active" check box, required for rule to work.
NOTE
: New rule will only apply to New/Edited Accounts unless you click "Run Rules" to re-apply all rules to all existing Accounts.
Also see all Account page layouts (Setup | Customize | Accounts | Page Layouts) for the 2 check boxes near the top which determine whether Account Territory Assignment Rules are applied automatically to new/edited Account records:
On the Page Layout palette, click "Layout Properties"
Locate "Evaluate this account against territory rules on save" 
Enable "Show on edit page"
Enable "Select by default"
Click OK and save the changes made to the page layout
