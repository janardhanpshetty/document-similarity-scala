### Topic: This article answers whether or not scheduled jobs are affected by scheduled maintenance.
Are scheduled jobs affected by scheduled maintenance?
Resolution
When Salesforce is down due to a scheduled maintenance (see more on 
trust.salesforce.com
 
), scheduled jobs are temporarily stopped.
After the maintenance is over, the scheduled jobs will resume automatically; no manual intervention is necessary.
