### Topic: Apply a Dashboard Filter | Salesforce
Apply a Dashboard Filter | Salesforce
Apply a Dashboard Filter
Filter a dashboard to analyze the information interactively. 
When you filter a Salesforce Classic dashboard, the filtered view is preserved so that the next time you see the dashboard, data is filtered by the same view. In Lightning Experience, dashboards always open with no filters applied.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view, refresh, and filter dashboards:
“Run Reports” AND access to dashboard folder
Note
All components on the dashboard aren’t necessarily filtered on the same field. The person who created or edited the dashboard specifies which field is used.
Open a dashboard.
In Salesforce Classic, if you applied any filters last time you used this dashboard, notice that they are still selected. In Lightning Experience, the dashboard always opens with no filters applied.
Select an option from the filter drop-down.
Each filter has one or more options you can choose to narrow your selection further.
To see data unfiltered, select 
Clear Filter
 or 
All
 from the filter drop-down.
Parent topic:
 
Filter Your Dashboard
Previous topic:
 
Add a Dashboard Filter
Next topic:
 
Dashboard Filter Examples
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dashboard_filters_using.htm&language=en_US
Release
202.14
