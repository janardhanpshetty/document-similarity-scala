### Topic: There are two ways to encode Unicode characters, UTF- 8 or UTF- 16 encoding. Refer to this article on how to import non-Latin characters.
Unreadable characters and gibberish display when the file to import contains non-latin characters such as Arabic, Hebrew, and Japanese etc. Data can be corrupted when import files contain non-Latin characters that are saved as Unicode. 
 
Resolution
There are multiple ways to encode Unicode, for example ISO-8859-8(Hebrew), UTF-8 or UTF-16, etc.
Source of the file is an excel file (.xls)
1. Save the file as txt unicode. (using Excel -- Save As then assign the filename example: test; save as type:unicode text) 
2. Change the file extension from txt to CSV. It should keep the non-Latin characters. (Perform this by opening the folder in Windows Explorer where the file was saved. Right click and choose rename and just add .csv on the filename. Example: rename test to test.csv)
 
3. Then import to Salesforce with encoding UTF-16
Source of the file is .CSV
1. Save the original data (csv file) using UTF-8 encoding (not general Unicode option).
 2.Open up the saved file in a Unicode editor and specify file format as UTF-8. Make sure all data are displayed as they were expected.
 3. Do an org account/contact import and choose UTF-8 encoding.
