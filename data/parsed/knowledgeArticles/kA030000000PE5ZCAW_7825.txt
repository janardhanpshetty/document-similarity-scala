### Topic: This article would help you with a work around to upload international data characters (Japanese) which is not supported by Data Loader.
INTERNAL: Loading international data characters
Resolution
Problem: Data Loader does not migrate the data in the correct format when loaded into 
salesforce.com
. The characters get jumbled up.
This solution is designed for loading Japanese data, in the case that the file provided was not encoded in Unicode or UTF-8.
Open the file via File: Get External Data: Import from within Access.
When Importing:
- Select Delimited, then Next
- Select Comma, and First Row Contains Field Names, then Advanced Language = All; Code Page = Japanese (Shift JIS), then Next
- Create a new table, then Next & Next
- Do not add a Primary Key, and then Next and Finish
- Verify that the data mapped correctly (ie, check if you need to modify field types/lengths to accommodate the data)
- Export file by right-clicking on the Table in Access, and save as type: MS Excel 97-2003 (*.xls)
- Save the file to your desktop and open it. Do any necessary data manipulations (linking tables & records using lookups, etc)
- Save file as type: Unicode.txt
- Re-import file into Access. Follow same wizard instructions as above, except import as a Unicode (rather than Shift-JIS) file
- Take a look. If all looks ok, export out as a .csv (in UTF-8 format, using the Export Wizard, on the Advanced tab).
Without modifying the new .csv file in any way, load this in with AppExchange Data Loader
