### Topic: User gets the message : "You are not authorized to perform that operation" when browsing Google Drive
Cause:
The Drive API is not enabled in the defined Google project.for Files Connect
Resolution
Enable Drive API under 
APIs & auth
 | 
APIs
 of the defined Google project.for Files Connect
