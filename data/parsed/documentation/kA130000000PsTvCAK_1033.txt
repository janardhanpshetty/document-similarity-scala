### Topic: Built-in Sharing Behavior | Salesforce
Built-in Sharing Behavior | Salesforce
Built-in Sharing Behavior
Available in: Salesforce Classic
Sharing for accounts and contacts is available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Sharing for cases and opportunities is available in 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Salesforce provides implicit sharing between accounts and child records (opportunities, cases, and contacts), and for various groups of portal users.
Sharing between accounts and child records
Access to a parent account
—If you have access to an account’s child record, you have implicit Read Only access to that account.
Access to child records
—If you have access to a parent account, you have access to the associated child records. The account owner's role determines the level of access to child records.
Sharing behavior for portal users
Account and case access
—An account’s portal user has Read Only access to the parent account and to all of the account’s contacts.
Management access to data owned by Service Cloud portal users
—Since Service Cloud portal users don't have roles, portal account owners can't access their data via the role hierarchy. To grant them access to this data, you can add account owners to the portal’s share group where the Service Cloud portal users are working. This step provides access to all data owned by Service Cloud portal users in that portal.
Case access
—If a portal user is a contact on a case, then the user has Read Only access on the case.
Group membership operations and sharing recalculation
Simple operations such as changing a user’s role, moving a role to another branch in the hierarchy, or changing a portal account’s owner can trigger a recalculation of sharing rules. Salesforce must check access to user’s data for people who are above the user’s new or old role in the hierarchy, and either add or remove shares to any affected records.
Note
These sharing behaviors simplify administration for data access but can make mass inserts and mass updates slow. For best practices on designing record access in a large organization, see 
Designing Record Access for Enterprise Scale
.
See Also:
Securing Data Access
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sharing_across_objects.htm&language=en_US
Release
202.14
