### Topic: The Mismatched Sender ID error occurs when the wrong GCM API Key is populated in the Marketing Cloud App Center application from the wrong Android Goggle Cloud project.
When creating an Android Google Cloud project,  you setup a Public API ACCESS key in your project.  This gives the Salesforce  Marketing Cloud permission to send an API call on behalf of your application to send a MobilePush message.  The Mismatched Sender ID error occurs when the wrong GCM API Key is populated in the Marketing Cloud App Center application from the wrong Android Goggle Cloud project.
Resolution
Verify the Error
Check the error 
occurring for the failed Android device 
1. Find the MemberID of the account in order to run a query to find the error. 
Run this query against the correct stack 
to find sends for that specific account
:
SELECT * FROM cMID._PushMessageTracking
2. Find the DeviceID for one of the failed devices in the send and run this query to get specific and not have to parse through several responses: 
​SELECT * FROM cMID._PushMessageTracking WHERE DeviceID = 'ID' or WHERE PushmessageID = 1
For the erroneous sends you will see "Mismatched Sender ID" in the response column. 
 
Update the GCM API KEY
To resolve this, instruct the customer (developer) to:
1. Go to the proper Google Project.
2. Pull the correct GCM API KEY.
3. Plug that into the the Marketing Cloud APP Center application in the GCM Client section in the API Key field.
For more information on how to edit this field in the Salesforce App Center, see 
How to Provision your Android Mobile App.
 
