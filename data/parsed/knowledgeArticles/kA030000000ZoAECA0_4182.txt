### Topic: The Community Management Overview page has been streamlined to give administrators faster access to relevant information and new features.
The Community Management Overview now includes links to Best Practices, Release information, Success Community resources and supplemental videos.  Administration nodes and links have also been improved for more intuitive navigation.
Resolution
Notable changes to Community Management include:
 
- 
Administration | Miscellaneous
 has been renamed to 
Preferences
- The 
Recommendations
 
node can be used to drive community engagement (offers videos, trainings, etc...)
- Merge, Rename and Delete Topics from the 
Topics | Topic Management
 node
- Customize your community's "Change Password" and "Forgot Password" pages from 
Administration | Login & Registration
- Links to Force.com and Site.com Studio are now located under the
 Administration | Pages
 node, where administrators can also override default community pages.
