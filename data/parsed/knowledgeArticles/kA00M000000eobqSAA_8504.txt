### Topic: Salesforce IP's can be blocked or throttled by Email Service Providers. Salesforce will work directly with the 3rd party to resolve delivery issues.
BEHAVIOR:
Outbound emails from Salesforce may bounce when sending to some Email Service Providers (ESP).  The bounce message may vary, however the failure reason will typically indicate that a specific IP address has been blocked.
Example:
  
From: Mail Delivery System <
mailer-daemon@salesforce.com
> 
Date: Tue, Mar 31, 2016 at 3:19 PM 
Subject: Undeliverable: Follow up - Today's meeting 
To: wayne_t@acme.com
This message was created automatically by the mail system. 
A message that you sent could not be delivered to one or more of its 
recipients. This is a permanent error. The following address(es) failed: 
>>> jane_d@widget.com (Undelivered): 550 SC-002 Unfortunately, messages from 
96.43.147.69
 weren't sent. Please contact your Internet service provider since part of their network is on our block list. 
CAUSE:
Large ESP's such as Microsoft, Yahoo, Google Mail, AOL, can potentially impose an inbound IP Block/Throttle for specific Salesforce IP’s.  This will cause emails from some Salesforce Organizations to temporarily or permanently bounce.  
The behavior could stem from the introduction of a new IP, as the reputation has not matured; however it could also occur from a large influx of emails from a Organization to number of recipients on the same domain.  The latter could trigger red flags on the recipient server as it may appear to be suspicious or malicious.  Generally, when the issue does occur, the block is limited to a small subset or IP's.
 
Resolution
The Salesforce technology team routinely works with large ESP's to ensure the highest levels of email deliverability
When the technical team detects or is notified of the issue, we will expedite working with the ESP on removing the Block/Throttle 
Salesforce will actively pursue a resolution with the 3rd party ESP, and push for a timely resolution until restrictions have been lifted
Salesforce will determine root cause of Throttle or Block to ensure that the issue is no longer occurring, and prevent further adverse action by the ESP
To avoid any potential ESP throttling, use of 
Email Relay
 allows organizations to control their own email deliverability and source mail from their own IPs
Please log a Support Case if this issue affects your Salesforce Organization
Note:
 Please note that this article refers to Email Service Providers (ESP) and not blacklist providers.  The Salesforce Technology team can and will work with major ESPs to clear blocks they may have imposed on our IP address(es).  For our policy regarding blacklist providers, please refer to the article 
SPAM Blackhole Lists: How Salesforce Email Delivery is Affected
