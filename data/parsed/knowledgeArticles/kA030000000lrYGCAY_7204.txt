### Topic: When using Google Chrome or Safari, users will notice they are unable to resize a Quote Template image/logo. This works fine in other browsers.
When using Google Chrome or Safari, users will notice they are unable to resize a Quote Template image/logo. This works fine in other browsers.
Resolution
This is a limitation on WebKit, the platform used on some browsers, including Google Chrome and Safari. WebKit has acknowledged this but there's been little progress and this is currently out of the scope of Salesforce Support.
The following is a list of WebKit based browser, which will potentially have the same issue - Note we do not support all the browsers listed below. Check 
Supported Browsers
 for more information.
 
Safari (Win and Mac)
Chrome (Win and Mac)
Flock (Mac)
OmniWeb (Mac)
iCab (Mac)
Rockmelt (Mac; can’t download it)
Arora (Win)
Element (Win)
Midori (Linux)
Epiphany (Linux)
Konqueror (Linux)
Stainless (Mac)
Sunrise (Mac)
Shiira (Mac; doesn't run on mine)
Rekonq (Linux)
Uzbl (don't know which OS; Linux? No download link)
Iron (Win; seems a real skin over Chrome)
Maxthon (Win)
Opera(Win and Mac)
WebKit known issue: 
https://bugs.webkit.org/show_bug.cgi?id=12250
Note:
 The same issue can happen when trying to resize image in home page component. The issue might be seen only in some browsers
