### Topic: The IsPrimary field on OpportunityContactRole is not included in the Salesforce weekly or monthly export service. Use the Data Loader or a Report to export a backup file that includes the IsPrimary field.
When I include the OpportunityContactRole object in my weekly (or monthly) Salesforce export, I've noticed that the IsPrimary field is not included in my exported data. How can I backup the contents of this field?
Resolution
Since the IsPrimary field is not included with the weekly (or monthly) Salesforce export service, there are two options for exporting a backup of this field:
1. Export to CSV using a report.
You can 
create a new report
 on the OpportunityContactRole object that includes the IsPrimary field and then 
export the report
 to CSV.
2. Export to CSV using a the 
Apex Data Loader
 or another API tool.
Using the Apex Data Loader, you can use the 
Export or Export All
 command to export a CSV file of data from the OpportunityContactRole and you may select the IsPrimary field as one to include in the export.
