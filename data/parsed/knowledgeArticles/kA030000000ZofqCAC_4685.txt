### Topic: The following Article gives example syntax for a formula field that will Display the 24-hour or Military time of a Date Time field.
The following article explains how to create a formula field to display the 24-hour or Military Time of a Date/Time field.
Resolution
The base example formula is the below Syntax:
TEXT(
FLOOR (
MOD( Date_Time_Field_Goes_Here__c - ($System.OriginDateTime + 5/24),1) *24)
)
+
LEFT(
RIGHT(
TEXT( 
Date_Time_Field_Goes_Here__c 
),6
),2
)
In the above example, the 5/24 represents the time zone the user is in. In the example, the user is in EST or GMT-5 so we use 5/24. In order to display an accurate time to the user this value needs to be changed to reflect the hours away from GMT the user is in. 
Example: 6/24 is CST, 7/24 is MST, and 8/24 is PST.
