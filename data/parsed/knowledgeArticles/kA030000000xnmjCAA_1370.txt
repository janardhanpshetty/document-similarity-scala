### Topic: A customer is missing the standard Forecast Category Field values. Tier 3 can run the following fixer to resolve this: ScrutinyPicklistLastUpdateByMissing
A customer is missing the standard Forecast Category Field values. 
Resolution
Please escalate the case to Tier3.
Tier 3 can run the following fixer to resolve this: ScrutinyPicklistLastUpdateByMissing
