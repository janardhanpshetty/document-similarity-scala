### Topic: How Actions Are Ordered in the Salesforce1 Action Bar | Salesforce
How Actions Are Ordered in the Salesforce1 Action Bar | Salesforce
How Actions Are Ordered in the Salesforce1 Action Bar
The Salesforce1 and Lightning Experience Actions section of a page layout and global publisher layout drives which actions appear in the Salesforce1 action bar. It also enables you to customize the order of quick actions, productivity actions, and standard and custom buttons that are available as actions.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, 
Database.com
, and 
Developer
 Editions
If you customize the Salesforce1 and Lightning Experience Actions section of a layout, Salesforce1 reflects your customizations.
If you customize the Quick Actions in the Salesforce Classic Publisher section, but not the Salesforce1 section, the actions in the Salesforce1 action bar are a combination of those in the Quick Actions in the Salesforce Classic Publisher section plus any standard or custom buttons present on the page layout.
If you customize the Salesforce1 and Lightning Experience Actions section, the standard and custom buttons in the buttons section of the page layout aren’t automatically included in the action list. You must add the buttons as actions from the Salesforce1 Actions category in the palette.
If neither section is customized, the action bar inherits a default set of actions predefined by Salesforce. The sets of actions differ between objects, based on the most common or typical activities required for each object.
See Also:
How Predefined Actions Are Ordered in the Salesforce1 Action Bar and List Item Actions
Salesforce1 Action Bar
Considerations for Actions in Salesforce1
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=actionbar_how_actions_are_ordered_customization.htm&language=en_US
Release
202.14
