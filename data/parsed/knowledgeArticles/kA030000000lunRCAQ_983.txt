### Topic: My unique click through rates are over 100% can you fix this?
There are many circumstances where stats.click records are recorded with too many IsUnique records which results in the application calculating the Unique Click-Through Rate for a job over 100%. The most common circumstance is users of the application using "bots" to test links in their sends. When bots are used, ExactTarget will sometimes receive click events on different CVI servers simultaneously making it impossible for us to determine which one was first (unique). In this event, both records are entered as unique for a subscriber.
Resolution
Stats processing happens in 2 stages. 
1) A record is written to the fact table (stats) 
2) The records in the stats tables are counted and written to aggregate tables 
In order to clean up the data, the extraneous IsUnique records need to be change to 0 in the stats table. (stats.click) Once the IsUnique values for the job are correct in the stats table, the following stored procedures need to be executed via product Ops resource: 
Exec Stats.ResetClickAggregates <JobID> 
Exec Stats.AggregateClicks 100000
