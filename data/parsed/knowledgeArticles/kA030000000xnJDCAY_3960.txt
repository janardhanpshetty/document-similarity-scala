### Topic: When I add a product to an opportunity, I receive the following error message Error: invalid quantity change on opportunity line item
When I add a product to an opportunity, I receive the following error message
Error: invalid quantity change on opportunity line item 
Resolution
This can occur when the product has a Default Revenue Schedule and there's a workflow rule attempting to modify the Quantity field in the opportunity product. When a product has a Default Revenue Schedule, you cannot change the Quantity of the product because it would affect the existing schedules. 
A workaround is not to configure a Default Revenue Schedule and let the users manually create one, after the opportunity product has been created. 
