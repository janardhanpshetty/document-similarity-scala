### Topic: Voice Best Practices | Salesforce
Voice Best Practices | Salesforce
Voice Best Practices
For the best calling experience, use a hardwired connection to your network with adequate bandwidth, use a wired headset, and set up the proper environment on your machine.
Available in: Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Voice is available for an additional cost as an add-on license.
Use a Good Connection
A wireless connection can work, but a hardwired connection is strongly recommended.
Having a dedicated network for your Voice calls is best. We recommend having at least 10 kbps per Voice session available, but having 500 kbps+ per Voice session is optimal. Limiting the number of applications you have running (especially screen-sharing applications) and your open browser tabs also improves performance.
Note
Using a network that masks your location may cause charges to be different from those displayed for your location.
Also make sure you’re using the most up-to-date Chrome, Firefox, or Edge browser.
Make Calls with a Quality Headset
An analog headset works best. Wireless or mobile phone earbuds will work, but you may experience issues with call quality.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=voice_best_practices.htm&language=en_US
Release
202.14
