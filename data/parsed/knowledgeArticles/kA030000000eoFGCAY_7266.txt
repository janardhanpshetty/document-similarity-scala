### Topic: Learn how to change the user designated as the owner of a managed social account in Social Studio.
Learn how to change the user designated as the owner of a managed social account in Social Studio. A social account ownership transfer can be initiated by any user with "Edit" permissions on a social account.
Resolution
 
Transfer ownership 
 
1. Navigate to the account-level 
Admin
 | 
Social Accounts
 section and access the social account.
2. Click 
Edit
.
3. Click 
Transfer Ownership
.
4. Select the new owner in the "Select new Social Account Owner" list.
Enter a name in the Search bar and click the icon to search among the users, or scroll through to find the appropriate user.
5. Click 
Continue
.
The window shows the name of the social account, the number of pages associated with it (depending on social network type), the current owner (with user picture, if any, and username), and the new owner (with user picture, if any, and username).
6. Click 
Change
 next to the new owner if selected in error and select a new owner.
7. 
Click 
Confirm
 at the Confirm Social Account Transfer window.
8. 
A small banner appears saying the account ownership was successfully transferred. 
Important information to remember when transferring 
social account ownership
:
 
Ownership can be transferred to any user, except for those assigned the "Basic User" role.
The original social account login credentials are required. Changing the owner in Social Studio doesn't change the credentials for the social account.
Only one social account can have ownership transferred at a time. 
