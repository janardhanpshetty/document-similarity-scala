### Topic: Use Dashboards on the iPad | Salesforce
Use Dashboards on the iPad | Salesforce
Use Dashboards on the iPad
Salesforce Mobile Dashboards for the iPad, available from the Apple App Store, lets you access dashboards you recently viewed or are following when you’re on the go.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Important
As of Summer ’15, the Mobile Dashboards for iPad app is no longer supported. You can continue to use the app, but Salesforce no longer provides support in the form of bug fixes or enhancements for any issues you may encounter. Talk to your Salesforce administrator about migrating to the Salesforce1 app, the new Salesforce mobile experience.
The mobile dashboards app keeps you in touch with your organization’s data, performance, and trends, whether you’re in the office or on the road. Use this app to:
Browse recently viewed dashboards, or ones you’re following
Search for dashboards
View individual dashboard components, highlight their values, and drill into reports for each
Modify your report view
Email a dashboard or an individual component to others
View, post, and comment on a dashboard’s Chatter feed
When offline, access some dashboards and reports you recently viewed in the app
Note
The app doesn’t support dynamic dashboards or dashboard filters.
You can download the app for free from the 
Apple App Store
 or from the 
AppExchange
. 
You can use the mobile dashboards app on all iPad models with iOS 5 or higher.
 
The app is available in Salesforce Enterprise, Unlimited, Performance, and Developer Editions, and additionally in any organization that has enabled REST API.
If you can access your Salesforce organization from your iPad, you can use the mobile dashboards app. Open the app and log in using your Salesforce email and password. If you don’t have a Salesforce account, you can still explore the app by tapping 
Demo
.
Note
Dashboard access for your iPad is available by default for your organization. If it isn't, an administrator must enable it before you can log in to the app.
In the Salesforce Classic Mobile client application, the Dashboards tab displays if your Salesforce administrator has added the tab to your mobile configuration.
Parent topic:
 
Dashboards Help You Visualize Complex Information
Previous topic:
 
Get Started with Dashboards
Next topic:
 
View Dashboard Lists
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=mobile_dashboards_ipad_overview.htm&language=en_US
Release
202.14
