### Topic: Find Related Lists in Lightning Experience | Salesforce
Find Related Lists in Lightning Experience | Salesforce
Find Related Lists in Lightning Experience
Records in Salesforce include details and links to other related records. 
Salesforce Classic
 displays related records in lists that appear near the bottom of the page. In Lightning Experience, related information appears in related list cards. For leads and opportunities—the objects that include a workspace—access related list cards from the Related tab. For reference objects like accounts and contacts, and on groups and people, related list cards display on the right side of the page.
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
For the first eight related list cards on a record:
Related list buttons (1) are located in the upper-right corner of each related list card. If there are multiple buttons, use the pull-down menu to access them.
Each item in a related list card includes a link (2) that opens the related record.
Record-specific actions (3) are located in the pull-down menu next to each related record.
When related list cards are on the right side of the page, each card displays up to three records. On reference objects, related list cards display up to six records. In both cases, drill down (4) to see the full list of related records.
If there are more than eight related lists on a record, additional related list cards display the object’s name only. Click the name in the related list card to open the full related list, which includes the related list buttons and record-specific actions that aren’t included in the card.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=lex_find_related_lists.htm&language=en_US
Release
202.14
