### Topic: Personalize Your Salesforce Experience | Salesforce
Personalize Your Salesforce Experience | Salesforce
Personalize Your Salesforce Experience
Salesforce includes personal settings options to help you personalize your experience. Depending on which experience of Salesforce you have enabled, these settings are located in different parts of the application.
Access Your Personal Settings in Lightning Experience
If you use Lightning Experience, your personal settings are located either in Personal Setup or My Settings.
Find Your Personal Settings in Salesforce Classic
If you use Salesforce Classic, your personal settings are located either in Personal Setup or My Settings.
Parent topic:
 
Get Started as a New User
Previous topic:
 
Set Up Your Chatter Profile
Next topic:
 
Get to Know Your Salesforce Admin
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=basics_nav_personal_settings_parent.htm&language=en_US
Release
202.14
