### Topic: Log into any record, create a task, and uncheck Send Notification Email.
@Kanchan Rohlania
 FYI.
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
June 11, 2015 at 5:11 AM
  
 
Pai Li
 likes this.
Kanchan Rohlania
done
Show More
Like
Unlike
 
  ·  
 
June 11, 2015 at 10:55 AM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Usage
Why is task notification sent even though its turned off on the Task Page Layout.
Resolution
Log into any record, create a task, and uncheck Send Notification Email.
Ensure 
Send Notification Email
 Checkbox is still UNCHECKED, 
then
 CHECK ‘Make this my default setting’.  Save the task. 
This setting will ensure that task notification are no longer sent out.
