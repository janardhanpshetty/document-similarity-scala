### Topic: This Knowledge Article details the process on enabling the Case Expert feature.
The Case Experts component is a new pilot feature available with the Summer '14 release.  This is an "open pilot" which means that any customer can participate and have the feature enabled.  
That said, they must meet the following criteria:
1.  Customer org must be on Enterprise, Unlimited, Performance, or Developer Edition w/ Service Cloud.
2.  Org must have Case Feed enabled.
3.  Org must be on Summer '14.
NOTE: The Case Expert component is only available as a "sidebar" component on the Case Feed.  In Winter '15 (safe harbor), this component will be available to add in the Salesforce Console "sidebar" as well.  
Resolution
To have this feature enabled, please follow the process below:
Tier 1 / Tier 2 Agents:
1.  Confirm that the customer meets the criteria listed above.
2.  Document the OrgId(s) where this feature needs to be enabled.
3.  Escalate to Tier 3 w/ appropriate escalation template.
Tier 3 Engineer:
1.  Confirm that all the information has been correctly capture by T1/T2.
2.  Enable the following permissions in Blacktab:
 
"Enables Predictive Case Experts Feature"
 
These may already be enabled but are required if not:
"Enable Knowledgeable Users on Records"
"Enable Knowledgeable access (UI or API)"
"Topics On Records"
NOTE:  Because this is an open pilot, there is no R&D Program record and no nomination process.  The PM, Michael Ramsey, has confirmed that Tech Support may enable this feature for customers as long as the criteria (above) is met.
