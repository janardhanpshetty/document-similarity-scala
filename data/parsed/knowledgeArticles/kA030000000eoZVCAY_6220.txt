### Topic: What to do when the Salesforce for Outlook option under Connected App Access section is not available
All profiles in the org, including the System Administrator profile, do not have the 
Salesforce for Outlook
 option under 
Connected App Access
 section.
Resolution
This usually happen if no user in the org has ever logged in to Salesforce for Outlook.
To make the option appear in the profile, install Salesforce for Outlook and have one user in the org log in to the app.
