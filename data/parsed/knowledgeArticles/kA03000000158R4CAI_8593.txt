### Topic: Apps and Packaging: Lightning Experience Limitations in Winter '16
 
Apps and Packaging are available in Lightning Experience, however, there are some limitations to be aware of that are outlined below. 
Resolution
Supported Features for Packaging
You can install packages in Lightning Experience with some limitations.
 
For installed packages, you can’t assign licenses to users in Lightning Experience. If you need to assign a license, switch to Salesforce Classic.
 
Unsupported Features for Packaging
The following packaging features aren’t supported in Lightning Experience:
 
Create a package
Upload a package
Upgrade a package
 
The following 
ISVforce
 features aren’t supported in Lightning Experience:
 
Channel Order App
Environment Hub
License Management App
Trialforce
Usage Metrics Visualization App
