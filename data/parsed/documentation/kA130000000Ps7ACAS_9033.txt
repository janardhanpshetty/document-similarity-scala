### Topic: Sample Approval Process: Discounting Opportunities | Salesforce
Sample Approval Process: Discounting Opportunities | Salesforce
Sample Approval Process: Discounting Opportunities
Opportunities that are discounted more than 40% require a CEO approval. Use this example to create a one-step approval process.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Prep Your Organization
Before creating the approval process:
Create an email template to notify approvers that an approval request needs to be reviewed. Direct users to the approval page in Salesforce by including approval process merge fields.
Create the following custom fields for opportunities:
A percent field called 
Discount Percent
 so that users can enter a percentage discount.
A checkbox field called 
Discount Approved
 to indicate whether the CEO approved the discount.
Create the Approval Process
Create an approval process on the Opportunity object and specify the following:
The filter criteria for this approval process is 
Discount Percent
 greater or equal 0.4
. Records must meet this criteria before they can be submitted to this approval process.
You don't need to choose a custom field as the next automated approver because you specify later that the CEO must approve all requests.
Select the email template you created for this approval process.
Choose the record owner as the only user who can submit a discount request for approval.
Create one approval step with no filter criteria since all records submitted need to be approved or rejected.
Choose 
Automatically assign to approver(s)
 and select the name of your CEO.
If appropriate, choose 
The approver's delegate may also approve this request
 if you want to allow the user in the 
Delegated Approver
 field to approve requests.
Consider creating the following final approval actions:
Email alert to notify the user who submitted the discount request.
Field update to automatically select the opportunity 
Discount Approved
 checkbox.
Wrap Things Up
After creating the approval process, add the Approval History related list to the appropriate opportunity page layouts.
Tip
Consider adding the Items To Approve related list to your custom home page layouts. It gives users an instant view of the approval requests that are waiting for their response.
If available, use your sandbox to test the approval process, then activate it.
See Also:
Manage Email Templates
Create Custom Fields
Set Up an Approval Process
Prepare Your Org for Approvals
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_useful_approval_processes_oppty.htm&language=en_US
Release
202.14
