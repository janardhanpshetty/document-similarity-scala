### Topic: As the number of topics in a community grows, you may want to curate them to improve usability.
As the number of topics in a community grows, you may want to curate them to improve usability.
 
Resolution
In Community Management, you can merge, rename, and delete topics in one convenient location. In Summer ‘15, topic merging is fully featured, now including topic-following and endorsement data.
Note that to successfully delete a topic in Community, 
topic must be deleted in two areas, both in 'Navigational Topic' and in 'Topic Management'. Here are the steps:
STEP 1: Go to the “Topic Management”
1. In the community page, click on your name.
2. Click on “Community Management”.
3. Click on “Topics”.
4. Click on “Topic Management”.
5. Click on the pencil icon next to an idea you need to delete.
STEP 2: Go to the “Navigational Topics”
1. While still in the “Community Management” page, click on “Navigational Topics”.
2. Click on the pencil icon.
3. Click on “Remove”.
4. Click on “Save”.
For more information, please refer to: 
http://releasenotes.docs.salesforce.com/en-us/summer15/release-notes/rn_networks_topics_merge_rename_delete.htm
