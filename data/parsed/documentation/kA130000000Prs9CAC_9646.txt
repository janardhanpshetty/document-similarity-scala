### Topic: Formatting Settings for Dashboard Scatter Chart Components | Salesforce
Formatting Settings for Dashboard Scatter Chart Components | Salesforce
Formatting Settings for Dashboard Scatter Chart Components
Scatter charts are useful to show one or two groups of report data plus summaries. These settings for the scatter chart are available in the dashboard component editor under 
Formatting
.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Tweak data displayed on the dashboard scatter chart using these settings.
Setting
Description
Sort Rows By
Currently sorting is unavailable.
Maximum Values Displayed
Set the maximum number of dots to show on the chart. When set to 5 for example, the chart shows 5 dots. These are the top 5 ascending values in the grouping used by the Plot By field of the scatter chart.
Axis Range
Keep automatic or choose manual to enter minimum and maximum values for the Y-axis range. If there are values outside the manual range, Y-axis automatically extends to include them.
Legend Position
Choose a place to display the legend in relation to your chart.
Show Details on Hover
Display values or labels when hovering over charts.
Parent topic:
 
Visual Settings for Dashboard Chart Components
Previous topic:
 
Formatting Settings for Funnel Dashboard Components
Next topic:
 
Formatting Settings for Dashboard Gauge Components
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dashboards_component_scatter_chart_format_settings.htm&language=en_US
Release
202.14
