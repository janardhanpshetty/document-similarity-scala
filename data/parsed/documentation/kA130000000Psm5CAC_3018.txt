### Topic: Create a Chatter Post Template | Salesforce
Create a Chatter Post Template | Salesforce
Create a Chatter Post Template
Identify which fields to display in an approval request post.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create approval request post templates:
“Customize Application”
From Setup, enter 
Post Templates
 in the 
Quick Find
 box, then select 
Post Templates
.
Click 
New Template
.
Select the object for your template.
Click 
Next
.
Give the template a name and description.
If you want this template to be the default for the associated object, select 
Default
. 
Add up to four fields to display on the approval request post.
We recommend putting any text-heavy fields—such as Comments or Description—at the bottom.
Save your changes.
See Also:
Choose Approval Request Notification Templates
Considerations for Chatter Post Templates for Approval Requests
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_post_templates_create.htm&language=en_US
Release
202.14
