### Topic: Performing Actions on a File in a Chatter Feed | Salesforce
Performing Actions on a File in a Chatter Feed | Salesforce
Performing Actions on a File in a Chatter Feed
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
You can perform the following actions on files in feeds.
Preview—Click the file thumbnail or click 
More Actions
 | 
Preview
 next to the file.
Download—Click 
More Actions
 | 
Download
 next to the file.
Upload new version—Click 
More Actions
 | 
Upload New Version
 next to the file.
View file detail page—Click the file name or 
More Actions
 | 
View File Details
 next to the file.
Follow—Click 
Follow
 next to a file to receive updates about the file in the Chatter feed. Click 
 to stop receiving updates about the file.
Share—Click 
More Actions
 | 
File Sharing Settings
 next to the file to share the file with people, groups, or via link.
If you have the “Sync Files” permission and are the owner of the file, click 
 
Sync
 to 
sync a file
 between your Salesforce Files Sync folder on your computer and Chatter. Click 
 
Unsync
 to stop syncing the file.
To learn who can perform which actions on a file, see 
Who Can See My File?
.
See Also:
View File Details
Upload a New Version of a File
View Where a File is Shared
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_attachments_actions.htm&language=en_US
Release
202.14
