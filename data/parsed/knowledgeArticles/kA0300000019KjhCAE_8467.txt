### Topic: Remittance Information - EMEA
Remittance Information - EMEA
Resolution
For customers in Europe, the Middle East, or Africa:
For EMEA customers (GBP):
Salesforce.com EMEA Limited
Deutsche Bank AG London
PO Box 16
Sheffield
S98 1 AZ
Great Britain
Wire Transfer to:  
Bank Name:                  Deutsche Bank AG London
Account Name:             Salesforce.com EMEA Limited
Swift Code:                   DEUTGB2L
Sort Code:                    CHAPS:  40-50-81 or BACS:  23-10-48
Account Number:           26077701
IBAN:                           GB21DEUT40508126077701
 
For EMEA customers (EUR):
Salesforce.com EMEA Limited
Deutsche Bank AG London
Route de la Longeraie 9 1110 Morges
Switzerland
Wire Transfer to:  
Bank Name:                  Deutsche Bank AG London
Account Name:             Salesforce.com EMEA Limited
Swift Code:                  DEUTGB2L
Sort Code:                    23-30-55
Account Number:          26077700
IBAN:                          GB48DEUT40508126077700
 
For EMEA customers (USD):
Wire Transfer to:  
Bank Name:                 Deutsche Bank AG London
Account Name:            Salesforce.com EMEA Limited
Swift Code:                  DEUTGB2L
Account Number:          26077706
IBAN:                          GB80DEUT40508126077706
 
For EMEA customers (SEK):
Wire Transfer to:  
Bank Name:                 Deutsche Bank AG London
Account Name:            Salesforce.com EMEA Limited
Swift Code:                  DEUTGB2L
Account Number:          26077703
IBAN:                          GB64DEUT40508126077703
 
Additional Information for EMEA Customers
Registered Address:      Floor 26 Salesforce
                                    Tower 110 Bishopsgate
                                    London EC2N 4AY United Kingdom
                                   
Company number:         
05094083 (registered in England and Wales)
VAT number:                 
GB844903710
Auditors:                       
Ernst & Young
 
Directors:                      
José Luiz Moura Neto, Joachim Wettermark
 
Bank Name:                  Deutsche Bank AG
Address:                       6 Bishopsgate, London
                                    EC2P 2AT, United Kingdom
 
Investor Information
Parent company:                                        Salesforce.com, Inc.
                                                                 The Landmark @ One Market, San Francisco, CA 94105, USA
NYSE ticker symbol:                                  CRM
Dun & Bradstreet (DUNS) #:                       48-281-6449
Financial information:                                 
http://www.salesforce.com/company/investor/financials
 
 
