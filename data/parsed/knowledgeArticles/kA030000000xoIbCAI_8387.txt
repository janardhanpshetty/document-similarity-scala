### Topic: An executive sponsor's support will help increase Salesforce usage and adoption.
Why get Executive Sponsorship?
Executive Sponsorship is an individual or set of individuals with high level of responsibility for the success of a company's Salesforce implementation.  They make decisions relating to your organization's direction, strategy, and financial commitments.
All companies using Salesforce, regardless of the size or scope, need good governance and effective sponsorship.
An executive sponsor is the implementation project's executive champion, who should participate and support the initiative from the beginning, through go-live and beyond.
Your success starts and ends with the quality of your Executive Sponsorship program.  Your executives should:
Have a vested interest in the program's success.
Champion the platform across the enterprise.
Have a vision to use Salesforce innovation in order to achieve their company's business objectives.
Influence others to effect change throughout the organization.
Prioritize Salesforce projects based on strategic business objectives.
An executive sponsor's support will help increase usage and adoption.  Gaining executive sponsorship is vital to helping your users understand the value that Salesforce can bring to the organization.
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
