### Topic: How to add and delete Topic Profiles in the Analysis Dashboard.
Instructions for adding or deleting a Topic Profile in the Analysis Dashboard
Resolution
How to Add a New Topic Profile
How to Delete a Topic Profile
 
Add a New Topic Profile
1. Login to the Analysis Dashboard at 
https://login.radian6.com
 and navigate to the 
Configuration tab.
 
.
 
2. Select
 Topic Profiles
 and from the Topic Profile Manager page, click the 
Add New Topic Profile 
button. There are two areas where the 
Add New Topic Profile 
button can be found.
 
 
3. Choose a name for the Topic Profile. This is a label for identification purposes only and will not be included as a search term.
4. Configure the Topic Profile visibility, keywords, and filters as needed. See 
Topic Profile User Guide
 for more information.
5. Click 
Save
.
Note: If you have an Agency account, the Topic Profile will be created in a 
'Trial' status
. Click 
Activate Topic Profile
 to make it a billable Topic Profile. 
6.Click 
Accept 
on the Activate Topic Profile pop-up screen.
 
 
Please note- The Radian6 account user that is used to create the Topic Profile must have a user role of Full User or Super User. Requisitioner Users can create Topic Profiles but a designated approver will have to activate the Topic Profile. Further information about User Roles can be found 
here.
 
Delete a Topic Profile
1. Login to the Analysis Dashboard at 
https://login.radian6.com
 and navigate to the 
Configuration tab.
2. Select 
Topic Profiles
 and from the Topic Profile Manager page, select the Topic Profile and click the 
Delete Topic Profile 
button. You can only delete Topic Profiles that you own.
 
3. Click 
Yes 
on the Delete Topic Profile confirmation pop-up screen. 
4. The Topic Profile is deleted and removed from the Topic Profile Manager screen.
 
