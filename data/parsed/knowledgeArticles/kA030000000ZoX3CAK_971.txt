### Topic: Why are the Share and Duplicate buttons not available on some of my YouTube and Facebook video posts?
Why are the Share and Duplicate buttons not available on some of my YouTube and Facebook video posts?
Resolution
The Share and Duplicate buttons are not available to use on YouTube and Facebook video posts that were made natively on YouTube and Facebook and not through Social Studio. Share and Duplicate is only available for videos posted from Social Studio.
