package similarity.mllib.minhash


import org.apache.spark.sql.SparkSession

import scala.collection.immutable.HashSet
import scala.util.Random
import scala.util.hashing.MurmurHash3.stringHash

/**
 * Created by jshetty on 8/1/16.
 */
object DocumentSimilarityArticles {
  def createShingles(words: Array[String]): Array[Array[String]] = {
    val shingleLength = 6
    val numShingles = words.length - shingleLength + 1
    val gram = Array.ofDim[String](numShingles, shingleLength)
    for (i <- 0 to numShingles - 1) {
      for (j <- 0 to shingleLength - 1) {
        gram(i)(j) = words(i + j)
      }
    }
    gram
  }

  def jaccard(set1: Array[Int], set2: Array[Int]): Float = {
    val lSet1 = HashSet() ++ set1
    val lSet2 = HashSet() ++ set2
    val percent = (1.0F * lSet1.intersect(lSet2).size / lSet1.union(lSet2).size) * 100
    percent
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.master("local[8]").appName("Document Similarity for Knowledge articles").getOrCreate()
    val sc = spark.sparkContext

    val article = sc.wholeTextFiles(args(0))
    val kdoc = sc.wholeTextFiles(args(1))

    val numHashFun = 70
    val seed = 32
    var slope = Random.nextInt(100)

    val articlePair = article.map((docPair: (String, String)) => (docPair._1.split("/").last, docPair._2))
    val kdocPair = kdoc.map((docPair: (String, String)) => (docPair._1.split("/").last, docPair._2))

    // Convert into shingles
    val shinglesArticleRDD = articlePair.flatMapValues((docContent: String) => createShingles(docContent.split("\\W+"))).mapValues((wShingles: Array[String]) => wShingles.mkString("")).cache()
    val shinglesKdocRDD = kdocPair.flatMapValues((docContent: String) => createShingles(docContent.split("\\W+"))).mapValues((wShingles: Array[String]) => wShingles.mkString("")).cache()

    // Apply hash functions Initalization Step
    var resultArticleRDD = shinglesArticleRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).reduceByKey((hashValue1: Int, hashValue2: Int) => math.min(hashValue1, hashValue2))
    var resultKdocRDD = shinglesKdocRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).reduceByKey((hashValue1: Int, hashValue2: Int) => math.min(hashValue1, hashValue2))

    for (i <- 1 to numHashFun) {
      slope = Random.nextInt(100)
      resultArticleRDD = resultArticleRDD.union(shinglesArticleRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).reduceByKey((hashValue1: Int, hashValue2: Int) => math.min(hashValue1, hashValue2)))
      resultKdocRDD = resultKdocRDD.union(shinglesKdocRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).reduceByKey((hashValue1: Int, hashValue2: Int) => math.min(hashValue1, hashValue2)))
    }

    val minHashesArticle = resultArticleRDD.groupByKey().mapValues((hashValues: Iterable[Int]) => hashValues.toArray)
    val minHashesKdoc = resultKdocRDD.groupByKey().mapValues((hashValues: Iterable[Int]) => hashValues.toArray)

    // Jaccard for minHash values
    val jaccardConfirmationMinScore = minHashesArticle.cartesian(minHashesKdoc).map(docPair => (docPair._1._1, docPair._2._1, jaccard(docPair._1._2, docPair._2._2)))
    // Filter pairs whose Jaccard similarity is <100.00 and > 0.0. These are candidates
    val resultMin = jaccardConfirmationMinScore.filter(tup => tup._3 > 0.0)

    resultMin.saveAsTextFile(args(2))

    //Jaccard Distance : Plain using shingles
    //val articleShingles = shinglesArticleRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).groupByKey().mapValues((hashValues: Iterable[Int]) => hashValues.toArray)

    //val kdocShingles = shinglesKdocRDD.mapValues((shingle: String) => math.abs(slope * stringHash(shingle, seed = seed)) % Int.MaxValue).groupByKey().mapValues((hashValues: Iterable[Int]) => hashValues.toArray)


    //get cartesian product of these two rdd's
    //val jaccardConfirmationScore = articleShingles.cartesian(kdocShingles).map(docPair => (docPair._1._1, docPair._2._1, jaccard(docPair._1._2, docPair._2._2)))

    // Filter pairs whose Jaccard similarity is <100.00 and > 0.0. These are candidates
    //val result = jaccardConfirmationScore.filter(tup => tup._3 > 0.0)

    //result.foreach(println)

    //result.saveAsTextFile(args(3))

  }
}
