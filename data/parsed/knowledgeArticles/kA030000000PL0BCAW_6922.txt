### Topic: Customers may want to natively setup a Customer Satisfaction Survey to be sent on cases to track service rep effectiveness. There are several options to accomplish this functionality which are outlined below the AppExchange apps could be used for other types of surveys.
I would like to know how we can create some custom surveys or possibly purchase some type of Survey system that integrates with my Salesforce Org.
Resolution
AppExchange Apps
  – 
https://appexchange.salesforce.com/results?keywords=customer%20satisfaction%20survey
 
These options may come at an additional cost and are designed to meet certain specific needs and have expanded functionality that the other options may not have.  Customers are encouraged to review these options to see which App best fits with their organizations business requirements.
 
Survey Force
 – 
https://appexchange.salesforce.com/listingDetail?listingId=a0N30000003I2gDEAS
 
Survey Force is a Force.com Lab app which comes as an “as is” application which comes at no additional cost that can be configured to send custom survey from Cases.  For customers on Enterprise Edition or above as well as Professional Edition customers who have purchased Workflow this App can be used in conjunction with a workflow rule to automatically send a survey to the Case Contact based on a defined set of criteria.  
**Please note that Salesforce Support does not provide technical support for the above products nor their integration to Salesforce. Please see the support pages provided on the providers listings above. 
