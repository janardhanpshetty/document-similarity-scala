### Topic: This article has a workaround to open pdf file within browsers.
When attempting to view a pdf file from a site.com site, the pdf is always downloaded, instead of viewing within the browser.
How does one get the pdf to open in the browser.
Example of desired behaviour:
http://www.salesforce.com/us/developer/docs/apexcode/salesforce_apex_language_reference.pdf
 
1) In google, search for "apex developer guide pdf"
2) Click on the first link that says "[PDF] 
Force.com Apex Code Developer's Guide - Salesforce.com". 
3) The links opens up in the browser pdf viewer.
Resolution
This is a  feature request and R&D team is reviewing this for possible release in the future.
As a workaround, you can use either <object> or <embed>.
For example, link to another html page and have this code implemented on that page :- 
 
<a href="MyPdfViewer"> 
 
Coding sample of MyPdfViewer is :
<object data="/MyPDF.pdf" type="application/pdf" > </object>
