### Topic: Describes the different types of sandboxes and what is the best use for each type of sandbox
Sandbox Management and Best Practices 
Have you ever wondered what the different types of sandboxes are available, and what is included in each when the sandbox is created or refreshed?  This presentation includes information on each type of sandbox and the top use cases for each type of sandbox.
There are 4 types of Sandboxes, some sandboxes are included in certain Salesforce editions and may be purchased for additional cost, please contact your Salesforce Account Executive for details.  The attached presentation describes common use cases for each type of Sandbox and best practices.
Developer
 - may be refreshed every day, does not include any data automatically but allows 200 MB of data to be imported or created.
 
Developer 
Pro
 - may be refreshed every day, does not include any data automatically but allows 1GB of data to be imported or created.
 
Partial Copy
 - may be refreshed every 5 days, includes a sample of production data up to 10k parent records and related child records, or 5GB of data total.  This sandbox requires a Sandbox Template.
 
Full Copy
 - may be refreshed every 29 days, include a full copy of production data and Sandbox Templates may be used but not required.
Click image above for larger view
1-800-NO-SOFTWARE 
| 
1-800-667-6389
© Copyright 2000-2016 
salesforce.com
, inc. 
All rights reserved
. Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
 
| 
Responsible Disclosure
 
| 
Site Map
Resolution
