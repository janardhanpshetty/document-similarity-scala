### Topic: The following details should be considered when deleting portal roles from the Org
Following are the details that need to be considered when deleting portal roles.
 
Roles can be deleted from any point in the hierarchy as long as a user is not assigned. If a user is assigned then, we need to move the users to a different role and then delete the role
Remaining roles will be renamed based on the number of roles remaining
If 1 role remains, User
If 2 roles remain, Manager => User
If 3 roles remain, Executive => Manager => User
Role Id remains the same and only the name changes
For example, if we have 3 roles for a Partner account and we delete Manager Role, then we will see that there is still a Manager role, because now the Executive role is renamed as Manager.
Even though the name is changed to ensure the hierarchy is corrected as shown above, the functionality of the existing role remains the same.
Currently, there is no way to delete a role that is assigned to user
All actions (adding roles or deleting roles) are logged in the Setup Audit Trial
Resolution
