### Topic: Customizing Your Partner Portal Tabs | Salesforce
Customizing Your Partner Portal Tabs | Salesforce
Customizing Your Partner Portal Tabs
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To customize partner portal tabs:
“Customize Application”
Note
Starting in Summer ’13, the partner portal is no longer available for organizations that aren’t currently using it. Existing organizations continue to have full access. If you don’t have a partner portal, but want to easily share records and information with your partners, try Communities.
Existing organizations using partner portals may continue to use their partner portals or transition to Communities. Contact your Salesforce Account Executive for more information.
In addition to custom objects, the following tabs can be displayed in the partner portal:
Accounts
Answers
Articles (requires Salesforce Knowledge
Cases
Contacts
Documents
Ideas
Leads
Opportunities
Entitlements
Service contracts
Solutions
Salesforce CRM Content tabs
Reports
You 
can choose which tabs display to users logged in to a Salesforce partner portal, and customize the order in which tabs display to portal users
:
From Setup, enter 
Partners
 in the 
Quick Find
 box, then select 
Settings
.
Select the name of your partner portal.
Click 
Customize Portal Tabs
.
To add or remove tabs, select a tab title, and click the 
Add
 or 
Remove
 arrow to add or remove the tab to or from the Selected Tabs box. To change the order of the tabs, select a tab title in the Selected Tabs box, and click the 
Up
 or 
Down
 arrow.
Optionally, from the 
Default Landing Tab
 drop-down, you can select which tab to display to users when they log into your portal.
Click 
Save
.
You can further specify which tabs users can access by editing tab settings in users’ associated profiles and permission sets.
The page layouts of tabs on the partner portal are controlled by the page layouts configured in the profiles assigned to the portal.
To edit the tab layouts for a profile:
From Setup, enter 
Partners
 in the 
Quick Find
 box, then select 
Settings
.
Select the name of your partner portal.
Select the name of the profile you want to edit from the Assigned Profiles related list.
Click 
View Assignment
 under page layouts next to the standard or custom object you want to edit.
Click 
Edit Assignment
.
Select a new page layout next to the profile name you are editing.
Click 
Save
.
Enabling Calendar and Events on the Home Tab
You can allow partner users to view a calendar and list of scheduled events on the Home tab of the partner portal. The calendar and events in the partner portal work the same as the calendar and events on the Home tab of Salesforce, with the following exceptions: spell check, event updates, enhanced list views, drag-and-drop scheduling, calendar sharing, and hover details are not supported in the partner portal.
Note
A partner user can only see calendars shared by:
Other partner users assigned to the same account
The channel manager for their account
Portal users can view tasks and events from their calendar. Additionally, portal users with the “Edit Events” permission can create, edit, and delete events.
Enabling the Welcome Component on the Home Tab
Include the Partner Portal Welcome component on home page layouts assigned to partner portal users. When users log in to your portal they receive a welcome message with their name, their channel manager's name, and links to both their company and personal profiles. They can click the linked channel manager name to send an email to that person. When portal users change information about themselves their user record is automatically updated but their contact record is not.
Note
The Partner Portal Welcome component is also available for the Customer Portal home page, however, it only provides a welcome message with the user's name.
Allowing Portal Users to View the Reports Tab
To allow portal users to view the Reports tab:
Grant portal users access to the folders on your Salesforce Reports tab that contain the reports you want them to run.
Set the organization-wide default sharing model to Private on objects you want portal users to report on.
Grant the “Run Reports” permission to portal users.
When you add the Reports tab to your partner portal, portal users:
Can run reports but cannot customize reports or filter report results.
Can export reports to Excel if they have the “Export Reports” permission.
Do not have access to the Unfiled Public Reports and My Personal Custom Reports folders.
Receive an insufficient privileges error if they run a report that contains objects they do not have permission to view.
Note
The Reports tab is only available to partner users with Gold Partner licenses.
See Also:
Partner Portal Overview
Enable the Partner Portal
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=partner_portal_available_tabs.htm&language=en_US
Release
202.14
