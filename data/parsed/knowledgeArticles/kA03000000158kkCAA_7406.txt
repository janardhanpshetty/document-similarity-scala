### Topic: Learn why you're unable to reply to Facebook posts in a Social Customer Service and Social Hub integration.
I am unable to see any Social Accounts populate to send replies to on a Facebook case through the Social Hub and Social Customer Service integration. 
Resolution
In order to reply using your Facebook Social Accounts in Social Customer Service, the social post should be created through a Social Account instead of a Topic Profile in the Social Hub.
