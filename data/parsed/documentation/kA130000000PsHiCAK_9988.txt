### Topic: About Connection Finder | Salesforce
About Connection Finder | Salesforce
About Connection Finder
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
If your partners use Salesforce, it is beneficial to connect your Salesforce organizations so that you can share records and collaborate on relevant business processes. Before you can connect, you need to find out if your partners use Salesforce. Use 
Connection Finder
 to email your partners a link to a simple survey asking if they use Salesforce. Survey responses are recorded on the contact and account records for each partner. 
Once you know a partner’s status, you can invite them to connect using Salesforce to Salesforce.
See Also:
Finding Out if Your Partners Use Salesforce
Setting Up Connection Finder
Tips and Additional Considerations for Connection Finder
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=partner_survey_about.htm&language=en_US
Release
202.14
