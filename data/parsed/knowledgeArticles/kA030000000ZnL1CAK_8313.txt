### Topic: This article would help you to understand why a customer or partner gets an error when he tries to install a package and how to resolve it.
There are some instances where the customer or partner is trying to install a package and gets the following error below:
Error ID: 458120351-2193 (-913120686) 
Details: [Campaigns_with_Contacts_with_Activities: No such relationship Activities on object Contact: Campaigns_with_Contacts_with_Activities: No such relationship Activities on object Contact] 
 
Resolution
The package org has the shared activities permission disabled but the target org has the same perm enabled. The solution is to have the permission disabled in the target org before installing the package. Reference case #11175019. 
