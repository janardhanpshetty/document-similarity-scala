### Topic: Database.com User Licenses | Salesforce
Database.com User Licenses | Salesforce
Database.com User Licenses
Available in: Salesforce Classic
Available in: 
Database.com
 Edition
User License
Description
Default Number of Available Licenses
Database.com Admin
Designed for users who need to administer Database.com, or make changes to Database.com schemas or other metadata using the point-and-click tools in the Database.com Console.
Database.com Edition: 3
Database.com User
Designed for users who need Database.com access to data stored in Database.com.
Database.com Edition: 3
Enterprise, Unlimited, and Database.com Edition: 0
Contact Database.com to obtain Database.com User Licenses
Database.com Light User
Designed for users who need only Database.com access to data, need to belong to Database.com groups (but no other groups), and don't need to belong to roles or queues. Access to data is determined by organization-wide sharing defaults.
Database.com Edition: 0
Enterprise, Unlimited, and Database.com Edition: 0
Contact Database.com to obtain Database.com Light User Licenses
See Also:
User Licenses
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=users_license_types_dbcom.htm&language=en_US
Release
202.14
