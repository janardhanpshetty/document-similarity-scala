### Topic: Report on Sales Reps’ Meetings with Customers | Salesforce
Report on Sales Reps’ Meetings with Customers | Salesforce
Report on Sales Reps’ Meetings with Customers
Get insight into how your sales teams are prioritizing their time. Create a custom report that shows which customers sales reps are spending their time with. 
Available in Lightning Experience in: 
Contact Manager
, 
Personal
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Available in Salesforce Classic in: 
All
 Editions Except 
Database.com
User Permissions Needed
To create, edit, and delete reports:
“Create and Customize Reports”
AND
“Report Builder”
To view activities owned by you and users below you in the role hierarchy:
No permissions needed
To view all activities:
“View All Data”
Create a custom report type.
For Primary Object, choose 
Users
.
Complete the remaining required fields: label, description, and category.
Click 
Next
.
Define a relationship between activities and users.
Relate the Activity Relationships object to the Users object.
Click 
Save
.
Define the fields that appear in this report type.
In the Fields Available for Reports section, click 
Edit Layout
.
Add the following Activity Relationships fields to the layout: 
Date
, 
Subject
, 
Name
, 
Related To
, 
Relation Count
. 
Change the label for the 
Name
 field to 
Contact
 and for the 
Related To
 field to 
Opportunity
.
For each field, select the option to display the field by default.
Click 
Save
.
Create a report.
In the Reports tab, select the report type that you created in steps 1–3.
Choose 
Summary Format
, and create a grouping by Full Name.
Run and save the report.
You now have a report that you can use to help your sales teams optimize the time that they spend with customers.
Note
If Shared Activities isn’t enabled, custom reports based on activity relationships don’t include event organizers along with invitees, and they don’t show events to which no one has been invited.
Parent topic:
 
Activities Reporting
Previous topic:
 
Reports That You Can Run on Activities
Parent topic:
 
Pre-Designed Custom Report Types
Previous topic:
 
Reports That You Can Run on Activities
Next topic:
 
Create a Forecasting Custom Report Type
See Also:
Create a Custom Report Type
Design the Field Layout for Reports Created From Your Custom Report Type
Create a Report
Creating a Custom Report
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=activities_reports_activityrelationships_salesrep_customer_contacts.htm&language=en_US
Release
202.14
