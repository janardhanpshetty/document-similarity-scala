### Topic: Determining Your Development Process | Salesforce
Determining Your Development Process | Salesforce
Determining Your Development Process
Available in: Salesforce Classic
Unmanaged packages are available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Managed packages are available in: 
Developer
 Edition
All packages are unmanaged until you convert them to managed packages. This requires 
managed packages
 created in a Developer Edition organization. You may prefer developing managed packages because you can beta test them before a release and offer upgrades for them.
Before creating a package, determine the development process you aim to take so that you can choose the most appropriate type of package for your process:
Developing Unmanaged Packages
Design your app. 
See the 
Force.com Quick Reference for Developing Packages
.
Package and upload your app.
Developing Managed Packages
Design your app. 
See the 
Force.com Quick Reference for Developing Packages
.
Package and upload a beta version of your app.
Gather feedback from your beta testers and make the appropriate fixes to your app.
Package and upload your final version of the app.
Planning the Release of Managed Packages
Delete Components from Managed Packages
Viewing Unused Components in a Package
Modifying Custom Fields after a Package is Released
Configuring Default Package Versions for API Calls
About API and Dynamic Apex Access in Packages
Manage API and Dynamic Apex Access in Packages
Generating an Enterprise WSDL with Managed Packages
Understanding Dependencies
Environment Hub
The Environment Hub lets you connect, create, view, and log in to Salesforce orgs from one location. If your company has multiple environments for development, testing, and trials, the Environment Hub lets you streamline your approach to org management.
See Also:
Planning the Release of Managed Packages
Manage Packages
Create and Upload Patches
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=determine_package_type.htm&language=en_US
Release
202.14
