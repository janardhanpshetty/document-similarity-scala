### Topic: When Shared Activities is enabled, Activities with Contacts reports will show the same Account for all Contacts. This depends on whether the "Related to" field on the activity is blank.
When Shared Activities is enabled, Activities with Contacts reports will show the same Account for all Contacts. Which Account shows up on the report depends on whether the "Related to" field on the activity is blank.
 
Resolution
Consider the following scenarios.
Scenario A:
 
1.You have the following contacts and their accounts. 
Contact A - Account A
Contact B - Account B
Contact C - Account C
2. You have a Task with the following details:
- Name field: Contact A (Primary), Contact B and Contact C - All three contacts are associated to the Task. 
- Related to Field: Blank
when you run the report, you will see 1 row per each Contact - Account Name on each row shows the Account of the Primary Contact only.
Scenario B:
 
1.You have the following contacts and their accounts. 
Contact A - Account A
Contact B - Account B
Contact C - Account C
2. You have a Task with the following details:
- Name field: Contact A (Primary), Contact B and Contact C - All three contacts are associated to the Task. 
- Related to Field: Account or Opportunity
when you run the report, you will see 1 row per each Contact - Account Name on each row shows the Account of the Related to field.
Scenario C:
 
1.You have the following contacts and their accounts. 
Contact A - Account A
Contact B - Account B
Contact C - Account C
2. You have a Task with the following details:
- Name field: Contact A (Primary), Contact B and Contact C - All three contacts are associated to the Task. 
- Related to Field: Other Objects (Campaigns, Cases, Custom Object, etc.)
When you run the report, you will see 1 row per each Contact - Account Name on each row shows the Account of the Primary Contact only.
