### Topic: This article goes over how to build a test web2x form.
How to build a test Web2X form? INTERNAL ONLY
Resolution
INTERNAL ONLY - DO NOT SEND TO CUSTOMERS
In order to troubleshoot certain issues that may be experienced when working with Web to Lead or Web to Case forms it may be necessary to create another web form for your internal use. You can perform these steps regardless of whether you have login access or not.
Before submitting a record make sure to obtain permission from the customer to submit a record to their org. Make sure to coordinate your submissions to ensure that if you trigger any workflow or email notifications that they can be mitigated or managed by the customer.
If you have login access, navigate to:
Setup | App Setup | Customize | Self Service | Web to Case.
or
Setup | App Setup | Customize | Leads | Web to Lead.
If you do not have login access, bring up the organization in blacktab and follow the same steps as above, ignoring the Setup step.
1. Select the fields you would like to be made available within the web form.
2. Click on the Generate the HTML link.
3. Copy the presented code into a Notepad document.
4. Find the OID input value. It should look like: "<input type=hidden name="oid" value="00D000000000062">" Make sure the value equals the correct OID for the org you wish to submit to.
<!-- ------------------------------
------------------------------
---------- -->
<!-- NOTE: These fields are optional debugging elements. Please uncomment -->
<!-- these lines if you wish to test in debug mode. -->
<input type="hidden" name="debug" value=1>
<input type="hidden" name="debugEmail" value="
salesforce@example.com
">
5. Save the form with the .htm extension to your desktop
6. Open the file in either Firefox or Internet Explorer.
7. Fill out the form, and then submit the data.
If you intend to validate the success of your submission, you will need login access to the customers org.
