### Topic: The following article will help you to auto populate the account name in a custom lookup field in activities using a flow and the process builder.
If you have a custom lookup field to Account in the Activities object and you want this lookup to be auto populated with the account name of the record where the activity is created, the steps below will help you to so.
Resolution
In the example below, we will create the necessary items to auto populate the account name in the lookup every time an activity is created in opportunities. We are going to need a Flow and a Process.
Let's create the Flow first:
   1. Go to Setup | Create | Workflow and Approvals | Flows.
   2. Click on 
New
 Flow
   3. Click on 
Resources
 | Double click on 
Variable
.
 
We need three variables for 
EventID, OppAccountID and OpportunityID. 
 
Below are the steps to create the variables. 
 
For Event ID:
   a. Name the Variable unique name as 
Var_EventID
.
   b. Data Type: 
Text
 | Input/Output Type:
 Input and Output
   c. Click on OK.
For OppAccountID:
   a. Name the Variable unique name as 
Var_OppAccountID
.
   b. Data Type: 
Text
 | Input/Output Type: 
Input 
and 
Output
   c. Click on OK.
For OpportunityID:
 
   a. Name the Variable unique name as 
Var_OpportunityID
.
   b. Data Type: 
Text 
| Input/Output Type: 
Input 
and 
Output
   c. Click on OK.
  
Now, let's create a Record Lookup for Opportunity and a Record Update for Event. 
 
Record Lookup for Opportunities. 
     1. From the 
Palette 
pane, drag and drop the 
Record Lookup
 in canvas
 
     2. Click on pencil icon on the record lookup and enter the following:
​          
 
Name:
 Lookup For Opportunities
           
Unique Name:
 Lookup_For_Opportunities
           
Lookup: 
Opportunity
              
Field
 ID
 equals 
{!Var_OpportunityID}
              Assign the record's fields to variables to refer them in the flow.
              
Field:
 AccountId 
Variable:
 {!Var_OppAccountID}
     3. Click on OK.
 
Record Update for Events.
     1. From the 
Palette 
pane, drag and drop the 
Record Update
 in canvas
     2. Click on pencil icon on the record lookup and enter the following:
 
             
              Name:
 
Update For Events
              
Unique Name:
 Update_For_Events
              Update | Event that meets the following criteria.
                      Field
 ID 
equals
 {!Var_EventID}
              Update record fields with values
                      Field:
 Account Name 
Value:
 {!Var_OppAccountID}
     3. Click on OK.
  
Now, the two components can be connected. 
The flow will auto-populate the Account Name in the Activities lookup field.
Let's create now the Process:  
     1. Click on  Setup | Create | Workflow and Approvals | Process Builder | New.
     2. Name the Process builder and Save it.
     3. Click on 
Add Object
.
 
              a) Select 
Events.
              
b) 
Choose the criteria to start the process when a 
record is created or edited
.
              c) Click on Save
     4. Click on 
Add criteria
     5. Name the criteria | Choose the criteria: "Conditions are met".
     6. Set the conditions as Event.WhatID starts with ID and the value is first 5 characters of event ID.
            Note that Event.WhatID is the event field called "Related To ID".
     7. Make the conditions as "All of the conditions are met (AND)"
     8. Create an immediate action with the following parameters:
                  a) Action Type: Flows
                  b) Action Name: Enter a name for the action.
                  c) Choose the flow that we created.
                  d) Set the flow variables as follows:
 
                           Var_EventID : Reference : [Event].Id
                           Var_OpportunityID : Reference : [Task].WhatId
     9. Click on 
Save
 and 
Activate
 the Process Builder.
 
 
