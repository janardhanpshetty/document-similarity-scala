### Topic: Popping Out Chat Windows | Salesforce
Popping Out Chat Windows | Salesforce
Popping Out Chat Windows
Keep chatting—even when you minimize your browser or use other applications—by popping out a chat or your chat list into a separate browser window.
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Note
Chatter Messenger is not available in new orgs created after the Spring ’16 release.
Click 
 in the top-right corner of an active chat or the entire chat list to pop it out.
Click 
 in the top-right corner of a popped-out chat or chat list to pop it back in to your Salesforce screen.
See Also:
Editing Chat Options
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_chat_pop_out.htm&language=en_US
Release
202.14
