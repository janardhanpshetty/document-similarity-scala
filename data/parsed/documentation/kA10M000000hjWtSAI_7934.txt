### Topic: Personalize Service Wave with the Configuration Wizard | Salesforce
Personalize Service Wave with the Configuration Wizard | Salesforce
Personalize Service Wave with the Configuration Wizard
Use the Service Wave configuration wizard to create the Service Wave app so it reflects how your company stores and views Service Cloud data.
User Permissions Needed
To create and manage Wave apps:
“Manage Wave Analytics Templated Apps”
“Edit Wave Analytics Dataflows”
When you create the Service Wave app, the built-in configuration wizard asks you a series of questions. Start with three basic pages that contain mostly required questions. Depending on how you answer them, you then see one or two optional pages. Your answers let Service Wave know how you want users to view your Service Cloud data in the apps dashboards. Answer these questions according to the way your organization uses Service Cloud data so the dashboards are as meaningful and useful as possible.
The wizard presents answers to each question in a pick list, most of them showing a list of fields. You can select a field only once; after you select it, that field is no longer available as an answer to other questions. Other questions are yes/no (Boolean) or require you to type in object names. Questions marked with an asterisk (*) require answers, while the others are optional.
While the wizard includes explanations to help you choose appropriate answers to each question, here are a few tips for how to use the wizard. Read the following carefully to assure a successful result the first time.
Important
Set Salesforce field-level security to enable the Wave Analytics cloud integration user to see all fields you’d like to use in Service Wave. The integration user runs the dataflow, and if he or she doesn’t have proper field-level security permissions, the dataflow can fail. See 
Field-Level Security
.
Page 1: Determine How Service Wave Displays Case Metrics
Choose the metrics that are important to your organization about the duration of cases, such as how quickly they’re resolved and if they’re resolved within the bounds of any service level agreement (SLA). All are mandatory and allow you to make only a single selection, except the following:
Question 1, optional: Asks you to indicate the primary field you use in Salesforce to track case duration, if any. If you don’t track case duration, Service Wave uses its own formula to calculate case duration based on the date a case was opened and closed. If the case is still open, it uses today’s date as part of the calculation. Allows only a single selection.
Question 2, optional: Asks if you use other (secondary) fields related to case duration. You can select multiple fields, for example, 
Duration with customer
 or 
Duration with agent
.
Question 3: Asks you to select the field you use to track SLA compliance. Typically, you would track SLA compliance with a custom formula field on the Cases object or the standard Milestone Status field. (Milestone Status field values include compliant, open violation, and closed Violation). If you track SLA compliance using a Boolean custom formula field, you can’t select that field from the wizard. In that case, do the following: Select any field and then manually edit the dataflow to include the Boolean field (see 
Configure the Dataflow
). Then, find and replace the field name in Service Wave dashboard JSON (see 
Wave Analytics Dashboard JSON Guide
).
Question 4: Asks you to select the field you use to record that a case is resolved on first contact. You would typically track first contact resolution using a Boolean custom formula field on the Cases object or the standard Closed when Created (IsClosedOnCreate) field.
Page 2: Control How the App Drills Into Your Data
Tell Service Wave how you prefer to drill down into data about the status, severity, owner, reason, and type of cases. Also choose how Service Wave displays data about service cases’ channels of origin, if they’re resolved at first contact, and other aspects. All questions are mandatory and all allow you to make only a single selection, except the following:
Question 11: Asks if you use queues to assign case ownership. Answer Yes only if you have a queue in your Service Cloud org and have at least one case attached to the queue.
Important
If you answer Yes and you do not have queues and a case attached to the queue, Service Wave creation fails.
Question 12, optional: Select measure fields from the Cases object not addressed in previous questions so your dashboards drill into measures that are important to your organization. Measures are quantitative values like case resolution time, number of open and closed cases, and revenue.
Question 13, optional: Select dimension fields from the Cases object not addressed in previous questions so your dashboards drill into dimensions that are important to your organization. Dimensions are qualitative values, such as date, region, and product name.
Page 3: Select Report Areas
Choose the areas of service data you want Service Wave dashboards to report on, such as customer satisfaction (CSAT), use of knowledge resources and telephony, and whether cases are attached to sales opportunities. All questions are mandatory and allow you to make only a single selection.
Question 2: Asks if you track CSAT score. If you answer Yes, the wizard presents a page of questions about how you track CSAT. See Page Four: Set Up CSAT Metrics.
Question 3: Asks if you use knowledge resources in the support process. If you answer Yes, the wizard presents a page of questions about how you use knowledge. See Page Five: Set Up Knowledge Data.
Important
If your Service Cloud org does not use knowledge and you answer Yes, Service Wave creation fails.
Question 4: Asks about use of telephony in the support process. If you answer Yes, Service Wave creates a dashboard related to telephony.
Important
You must track telephone usage data on the Task object for Service Wave to successfully create a telephony dashboard. Your org must have telephony data in standard activity fields on the Task object, such as call duration, call object identifier, call result, and call type. Answer Yes if you store call data in these fields. Answer No if you store call data in custom objects or outside Salesforce. If your Service Cloud org does not track telephone usage data and you answer Yes, dashboard creation fails.
Page 4: Set Up CSAT Metrics (Optional)
Tell Service Wave how you track CSAT so that information is surfaced correctly in dashboards. You see this page—and must answer its questions—only if you answer Yes to Question 1 on Page 3. Follow these instructions carefully to assure success:
Question 1: Asks which object you use to track CSAT. Service Wave defaults to the Cases object. If you use a different object to track CSAT, select it from the pick list.
Question 2: Asks you to select the field from the object selected in Question 1 you use to track CSAT.
Question 3, optional: If you use something other than the Cases object to track CSAT, Service Wave has to create a join between that object and Cases. The answer to this question provides the name of the field from the Cases object to use for the join. Enter the developer name of the field.
Question 4, optional: Enter the developer name of the field from object you selected in Question 1 if you use something other than the Cases object to track CSAT. The answer to this question provides the name of the field from the second object to use for the join with the Cases object.
Important
Only answer Questions 3 and 4 if you selected something other than the Cases object in Question 1. Fill in the fields’ correct 
developer names
 when you answer these questions or app creation fails.
.
Page 5: Set Up Knowledge Data (Optional)
Tell Service Wave how to surface data about knowledge articles in dashboards. You see this page, and must answer its questions, only if you answer Yes to Question 2 on Page 3.
Question 1, optional: Asks about the article type you want to see data about in Service Wave dashboards. Choose only a single article type.
Parent topic:
 
The Service Wave Analytics App
Previous topic:
 
Create and Share the Service Wave App
Next topic:
 
Schedule the Service Wave Daily Dataflow
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_app_service_wave_configurator.htm&language=en_US
Release
202.14
