### Topic: Handling of "System.UnexpectedException" error
System.UnexpectedException is a run time exception thrown by the system. This exception cannot be caught or thrown. There is no graceful way to handle this in the apex code.
System.UnexpectedException is usually followed by a error message. Code should handle the conditions that can cause this exception and to prevent it in advance, but cannot be handled once it is generated. Error message is usually self-descriptive, but if it has a Salesforce error code, please contact salesforce.com support.
Resolution
