### Topic: Platform and Partner Community license type users that were previously able to use Dynamic dashboards are no longer being able to.
Since the 190 release, we have heard reports of users (Platform and Partner Community license type) that were previously able to use Dynamic dashboards no longer being able to do so.  This was previously possible through the use of a loophole (described in the below Successforce thread):
https://success.salesforce.com/answers?id=90630000000gx0lAAA
As of the 190 release, the loophole has been closed. This means that any environments utilizing this loophole are no longer permitted to have access to this functionality.
Due to the impact of having this loophole closed, the Product Management group is currently evaluating the best way to address it. Currently, the focus is on the Platform licensed users. Depending on the impact, this proposed change may benefit Partner Community users as well.
DO NOT SHARE WITH CUSTOMERS
The current plan is to make these Dynamic Dashboards accessible as standard functionality to these license types. This plan is still in motion and nothing has been definitively confirmed. If this does move forward, we do not have a time frame for the implementation.
Resolution
