### Topic: Install Chatter Desktop | Salesforce
Install Chatter Desktop | Salesforce
Install Chatter Desktop
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Salesforce provides two Chatter Desktop installers: a standard version for individual installations and a managed version for enterprise deployments.
The following are the minimum requirements for using Chatter Desktop:
Windows
2.33GHz or faster x86-compatible processor, or Intel Atom™ 1.6GHz or faster processor for netbook devices
Microsoft® Windows® XP, Windows Server® 2003, Windows Server 2008, Windows Vista® Home Premium, Business, Ultimate, or Enterprise (including 64 bit editions) with Service Pack 2, or Windows 7
512MB of RAM (1GB recommended)
Mac
Intel® Core™ Duo 1.83GHz or faster processor
Mac OS X v 10.5, 10.6, or v10.7
512MB of RAM (1GB recommended)
Important
Chatter Desktop uses Adobe® Integrated Runtime (AIR®), Adobe's cross-platform runtime environment for desktop applications, and only runs on operating systems that Adobe AIR supports. Chatter Desktop does not run on operating systems that Adobe AIR does not support, such as 64-bit Linux. See the Adobe website for information on Adobe AIR.
The standard version:
Requires administrator privileges on your machine
Doesn’t require administrator privileges in Salesforce
Automatically installs the required version of Adobe® Integrated Runtime (AIR®)
Requires acceptance of an end user license agreement the first time Chatter Desktop launches
Posts “Installed Chatter Desktop” to Chatter the first time Chatter Desktop launches
Periodically checks Salesforce for new versions
To install the standard version:
From your personal settings, enter 
Chatter Desktop
 in the 
Quick Find
 box, then select 
Chatter Desktop
.
Click the 
Download Now
 badge.
Follow the on-screen instructions.
Note
Administrators can disable the Chatter Desktop download page and block users from accessing Chatter Desktop.
Important
Chatter Desktop uses Adobe® Acrobat® Reader to preview PDF files. Before previewing files with Chatter Desktop, download Adobe Acrobat from 
Adobe's website
, install it, and open it at least once to complete the installation.
See Also:
Uninstalling Chatter Desktop
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_desktop_installation.htm&language=en_US
Release
202.14
