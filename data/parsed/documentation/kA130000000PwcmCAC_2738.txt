### Topic: Grant Super User Access to Partner Users in Your Community | Salesforce
Grant Super User Access to Partner Users in Your Community | Salesforce
Grant Super User Access to Partner Users in Your Community
Partner Super User Access must be enabled in your Communities Settings before you can grant access to users. Use this information to grant super user access to users with Partner Community licenses. You can also grant super user access to users in your community with Customer Community Plus licenses. To learn more, see 
Grant Super User Access to Customer Users in Your Community
.
Granting super user access to external users in your community lets them access more data and records, regardless of sharing rules and organization-wide defaults. Super users have access to data owned by other partner users belonging to the same account who have the same role or a role below them in the role hierarchy. Super user access applies to cases, leads, custom objects, and opportunities only. External users have access to these objects only if you exposed them using profiles or sharing and added the tabs to the community during setup.
View the contact record for the user.
Click 
Manage External Account
, then choose 
Enable Super User Access
.
Click 
OK
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_partner_super_user_access.htm&language=en_US
Release
202.14
