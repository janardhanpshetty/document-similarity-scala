### Topic: Explanation on how the new apex callout limits are applied
In 
this
 article (and the 
Spring '15 release notes
) we mention that the callout limits were increased for both sync and async requests.
What really happens in our code is as follows:
- the mechanism to determine how this limit is applied has changed
- we are now using a combination between this org limit (Maximum size of callout request or response) and the maximum allowed heap size (6MB sync / 12MB async).
This means that within blacktab of the Org, you will still see their callout limit set to the old 3MB in the org, but in fact the limit will now be determined by the available heap.
T3 and CCE have discussed this topic with the Apex team as part of an 
investigation
 and for now we have decided that the best course is to write an internal knowledge article to cover cases where the customers are complaining about still having 3MB set as their max callout size.
Moving forward we'd like to track how many customers open cases with this issue. If you come across any, please flag them on the investigation thread mentioned above, @mentioning Katia Hage and Mihai Bojin.
Resolution
We are now using whichever is higher between:
A. "Maximum size of callout request or response" org limit
B. and Maximum apex heap size
to determine the allowed callout size.
