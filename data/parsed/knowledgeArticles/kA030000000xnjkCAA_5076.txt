### Topic: Learn more about Goal Template Administrator permissions.
When the Goal Template Package is installed a Goal Template Administrator permission set is created.  Despite the name, this permission set doesn't grant a user access to all Goal Templates, only ones they created.
Resolution
The Goal template package introduces 3 objects to an org:
Goal Template
Metric template
Task Template
The Goal Template has a master-detail relationship with the other two objects, so sharing the Goal Template alone will give access to the associated Metric Templates and Task Templates.
Since these are regular Salesforce objects sharing can be done in any number of ways including:
Manual Sharing
Sharing Rules
Setting Modify All/View All on the Goal Template object in either a profile or permission set
