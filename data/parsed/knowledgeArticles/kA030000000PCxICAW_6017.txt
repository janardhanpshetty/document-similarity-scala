### Topic: Learn why queries aren't returning information that is available in the UI.
When Activity related objects are expired or overdue, they can be archived. In this situation, you won't able to see them in a "regular" SOQL query.
Resolution
To see those objects add "ALL ROWS" in the end of the query if it's in the system log / workbench, and "Where isArchived = TRUE" if it's in the schema explorer / API.
To see those objects:
Add "ALL ROWS" in the end of the query if its in the system log / API.
Check the "include" button in "deleted and archived records" if you're using the workbench
Use "Where isArchived = TRUE" if it is in the schema explorer.
