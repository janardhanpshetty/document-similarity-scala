### Topic: When users click on “Edit Opportunity Splits” button they get internal server error. Error such as: Aura Integration Service Error[{"message":"An internal server error has occurred. Please ensure the affected user has got Opportunity Split page layout assigned to the profile.
When users click on “Edit Opportunity Splits” button they get internal server error. Error such as below:
Aura Integration Service Error[{"message":"An internal server error has occurred 
Error ID: 1087007735-44561 (-1903901767)"}] 
Resolution
Please ensure to check if the user has got Opportunity Split page layout assigned to the profile. If there is no page layout assigned then user will get internal server error message when trying to click on “Edit Opportunity Splits” button in opportunity.
