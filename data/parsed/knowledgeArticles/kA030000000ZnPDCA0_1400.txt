### Topic: There is a hard limit on the overall amount of DML operations that can occur during a deployment. This article explains why this limit is in place, what can be communicated, and how customers can modify their tests to avoid running into it.
Given that customers can have thousands of tests, the likelihood of using large amounts of DML operations during tests is very likely. Previously, we encountered issues with the DB, and this resulted in a cap on how many DML operations of any kind can be performed during a runAllTests within a deployment. If you encounter this limit, you will get the following error:
System.LimitException: Your runAllTests request is using too many DB resources.
The limit currently is at 450K for the entire deployment transaction, for any DML operation across all tests being run. 
This limit is not public facing and 
should NOT
 be shared with the customer. 
For now, this is a hard limit to protect the DB.
Should a customer encounter this, what should you do?
Resolution
Firstly, if you cannot tell them this hard limit, what can you tell them?
Conceptually, there is something in place that protects the service when a test run has consumed too much resources, and this test run triggered that fail-safe. This was put in place to protect the database from being overwhelmed. This limit is in place to protect the service and stop excessive use given that Salesforce is a
 
multitenant 
architecture
.
So when would a customer encounter this?
Typically this will only occur during a deployment with runAllTests. This is due to a deployment running all transactions serially (synchronously) in a single transaction. Given that all of these tests are run in a single transaction, all the DML operations through all tests will be grouped into one sum. The limit is rarely encountered and would be mainly the larger customers with a large code base running all tests during deployments.
What can a customer do to avoid this limitation?
Given the issue could be through 10's of millions of lines of code (assuming this would normally affect the larger organizations if any), these are the options currently available:
1) Review code base and limit the DML statements to a minimum amount for testing. As per the 
testing best practices
, they do not need to delete data considering tests do not commit any data so any DML operations to delete every test record is not required but only to cover code and meet to criteria of a delete trigger context (
isDelete context
).
2) 
As of Spring 15
 there is now the 
@testSetup annotation for Apex tests
. Currently, for test classes developers would be re-creating test data multiple times within the same test classes which would result in large DML executions during tests. With this new annotation, you can create test records once and then access them in every test method in the test class.
3) Another possible solution could be to apply for the 
Fast Deploy pilot feature
. This could be something to look into given that this pilot feature "
only requires you to run Apex tests that provide coverage for the Apex Code being deployed.
" which will avoid this issue for deployments. As this is a Pilot it may not be completely tested and reliable, may not be released as GA, and dropped but it is another workaround along with the newly mentioned @testSetup annotation.
