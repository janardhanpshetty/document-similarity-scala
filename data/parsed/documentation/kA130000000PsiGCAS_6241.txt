### Topic: About Partner Portal User Management | Salesforce
About Partner Portal User Management | Salesforce
About Partner Portal User Management
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Note
Starting in Summer ’13, the partner portal is no longer available for organizations that aren’t currently using it. Existing organizations continue to have full access. If you don’t have a partner portal, but want to easily share records and information with your partners, try Communities.
Existing organizations using partner portals may continue to use their partner portals or transition to Communities. Contact your Salesforce Account Executive for more information.
There are two types of users for partner portals—channel managers and partner users.
Channel managers are the internal users that manage your partners. 
Before enabling a portal, you need to ensure your channel managers have the appropriate permissions so they can manage partners.
Partner users are Salesforce users with limited capabilities. They are external to your organization but sell your products or services through indirect sales channels. They are associated with a particular partner account, have limited access to your organization's data, and log in via a partner portal.
The following table shows the storage per license as well as which features the partner user can access on the portal.
Gold Partner License Type
Storage per license
5 MB of data storage per user
Documents
Yes
My Account Profile
Yes
Leads
Yes
Custom Objects
Yes
Approvals
Yes. Users can submit records for approval and view the status of approval requests in the Approval History related list on record detail pages, but can't be assigned as approvers.
Accounts
Yes
Opportunities
Yes
Salesforce CRM Content (Read Only)
Yes
Reports
Yes
Delegated External User Administration
Yes
Email
Yes
Cases
Yes
See Also:
Partner Portal Overview
Partner Portal Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=partner_portal_user_management.htm&language=en_US
Release
202.14
