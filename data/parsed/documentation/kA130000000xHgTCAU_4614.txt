### Topic: Considerations for Activity Reminder Setup in Salesforce Classic | Salesforce
Considerations for Activity Reminder Setup in Salesforce Classic | Salesforce
Considerations for Activity Reminder Setup in Salesforce Classic
Disabling individual preferences for displaying event and task reminders erases users’ activity reminder settings. If you re-enable activity reminders later, users receive reminders only for activities they create or edit after you re-enable the setting.
Available in Salesforce Classic in: 
All
 Editions Except 
Database.com
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customizeactivities_reminders.htm&language=en_US
Release
202.14
