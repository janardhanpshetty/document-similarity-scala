### Topic: Users do not have the ability to assign emails contained in their "My Unresolved Items" section after they have been added via Salesforce for Outlook and/or Email-to-Salesforce. The section displays a message stating, "You need additional permissions to assign emails to other records. Contact your administrator."
Users do not have the ability to assign emails contained in their "My Unresolved Items" section after they have been added via Salesforce for Outlook and/or Email-to-Salesforce. The section displays a message stating, "You need additional permissions to assign emails to other records. Contact your administrator."
Resolution
Ensure that the affected user(s) have appropriate permissions required to add and assign emails:
- Add Email checked in their currently assigned Outlook configuration
- Email-to-Salesforce settings are configured properly
- Edit Tasks permission on their assigned profile
- Ensure the Task object's field level security for the "Name" and "Related To" fields to visible for the affected user's assigned profile.
 
