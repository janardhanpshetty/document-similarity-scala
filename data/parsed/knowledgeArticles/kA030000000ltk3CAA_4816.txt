### Topic: Detailed information about Coaching Activities.
Coaching activities allow you to track tasks and events for coaching, just like you can with other standard objects like Accounts and Opportunities.
 
Tasks are great for tracking more granular work associated with the coaching space. You can assign tasks to your coach or the person you are coaching to help you progress and optionally send them a notification email. You can set reminders for yourself so that you keep on top of your tasks. You can also update the status of the task to reflect the status and add attachments related to the task.
 
Events are great for tracking meetings associated with this coaching space. Similar to tasks, you can assign an event to other users, set a reminder, or add attachments. You can even create recurring events for future meetings.
 
Create new tasks or events by clicking New Task or New Event on the Open Activities related list. You can view completed tasks and events from the Activity History related list. For more information about creating tasks and events, see Considerations in Using Tasks and Considerations in Using Events.
Note:
Tasks and events need to be enabled for each user via profiles or permission sets before they can use tasks and events in Coaching. For more information on activities, see Activities in the Salesforce Help. 
 
Resolution
