### Topic: When Chatter recommends records because they were viewed by users, Chatter will recommend to follow records based on how long ago they were viewed and how many different records have been viewed since then.
When navigating  to the Chatter tab, users might see that Chatter is recommending to follow records they might not recall viewing or editing recently. Chatter will indicate the reason why a record is being recommended, whether it is a file that is popular, or a user that is new to Chatter. The reasons why Chatter recommends different types of records are documented in these two articles:
1. People Recommendations: https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_people_recommendations.htm&language=en_US
2. Record Recommendations: https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_record_recommendations.htm&language=en_US
Chatter will also recommend to follow records because they have been viewed. In some cases these record recommendations do not seem to make sense, as the user does not recall viewing the record in recent past.
Resolution
When recommending records because the user has viewed the record, Chatter will recommend records based on the 100 records the user has most recently viewed. Furthermore,  
Chatter will recommend records if they have been viewed within 90 days. So, t
he reason the user is seeing recommended records for the reason that they have been viewed could be because  the user seeing these recommendations did in fact view the records in question in the past and might not have viewed many more records since then.
