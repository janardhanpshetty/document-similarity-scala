### Topic: To connect to a Salesforce sandbox instance using the Office toolkit you must use setServerUrl() so that the calls point to "https://test.salesforce.com/services/Soap/c/"
Why can I not connect to my Salesforce sandbox using the Office toolkit?
Resolution
To connect to the sandbox by using the Office toolkit, the code needs to use setServerUrl().
The following is a sample code from the doc:
Function SetServerUrl(url As String)
g_sfApi.SetServerUrl "https://test.salesforce.com/services/Soap/c/"
End Function
The following is a sample code for C#
g_sfApi = new SForceOfficeToolkitLib4.SForceSession4();
g_sfApi.SetServerUrl("https://test.salesforce.com/services/Soap/c/25.0");
g_sfApi.Login("Test@salesforce.com.test", "xxxxxxxx");
MessageBox.Show(g_sfApi.CurrentServerTime.ToString());
MessageBox.Show("Connection is successful.");
