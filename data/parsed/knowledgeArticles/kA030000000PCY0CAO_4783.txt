### Topic: An overview of the order in which to update objects when loading large amounts of data or migrating data.
Below is an overview of the order in which to update objects when loading large amounts of data or migrating data.
Resolution
Order
Object to Update
Related To
1
Accounts
 
2
Contacts
Accounts
3
Opportunities
Accounts 
Contacts
4
Products
 
5
Product Line Items
Opportunities 
Products
6
Cases
Contacts
7
Leads
 
8
Campaigns
 
9
Campaign Members
Campaigns 
Contacts 
Leads
10
Contracts
Accounts 
Contacts
11
Assets
Accounts 
Contacts 
Cases 
Products
12
Solutions
Cases
Depends
Custom Objects
Depends on implementation
Last
Activities
Can be to any standard or custom object
 
