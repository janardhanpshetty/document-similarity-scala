### Topic: Whenever I use an already used password(in past) for setting a new password(on password reset) then the <sf:exceptioncode> in the SOAP response contains unknown_exception instead of Invalid new password error
case 1)
Error if the newly entered password do not contains mixture of numbers and letters,then the error observed in the SOAP is 
<sf:exceptioncode>Invalid_new_password<sf:exceptioncode>
<sf:exceptionmessage>Your password must have a mix of letters and numbers<sf:exceptionmessage>
case 2)
But if the newly entered password is a repeated password,then the error observed in the SOAP is
<sf:exceptioncode>Unknown_Exception<sf:exceptioncode>
<sf:exceptionmessage>Invalid Repeated Password<sf:exceptionmessage>
Resolution
If a third party end/application is trying to reset a new password and then using the already used password then they have to define a custom exception in their code to show the repeated password error 
However the error is clear in case 1 defined in the description
