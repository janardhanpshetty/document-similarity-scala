### Topic: Manage Fields for a Specific Object | Salesforce
Manage Fields for a Specific Object | Salesforce
Manage Fields for a Specific Object
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Standard fields are not available in 
Database.com
Salesforce Connect external objects are available in: 
Developer
 Edition and for an extra cost in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
From the object management settings for the object whose fields you want to view, go to Fields.
Click the field label.
To modify a custom field, add custom help text, or change the data type, click 
Edit
.
If a custom field exists in a Managed - Released package, click 
Delete
 to delete the custom field component from future installations.
To set users’ access to the field, click 
Set Field-Level Security
. This option is available depending on the edition that you have.
To view who can access the field based on permissions and record type, click 
View Field Accessibility
. This option is available depending on the edition that you have.
If the custom field is a dependent picklist, click 
Change
 next to the controlling field to edit the dependency rules.
To change 
External ID
, 
Required
, or other attributes under the General Options section, see 
Custom Field Attributes
.
To restore the field and its data, click 
Undelete
. This option is available only if the field has been deleted but not permanently erased. The field’s behavior may be different after restoring it. To restore the field completely, see 
Manage Deleted Custom Fields
.
Note
If your organization uses person accounts, the Account Fields page lists both person account and business account fields.
Person accounts use a combination of account and contact fields. The following contact fields are available for person accounts, but not for business accounts.
Fields in the Account Standard Fields list that display with a person account icon.
Fields in the Contact Custom Fields & Relationships list.
See Also:
Custom Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=viewing_fields.htm&language=en_US
Release
202.14
