### Topic: Prevent Data.com Clean Jobs from Updating Records | Salesforce
Prevent Data.com Clean Jobs from Updating Records | Salesforce
Prevent Data.com Clean Jobs from Updating Records
You can prevent individual account, contact, and lead records from being updated with Data.com data when automated jobs run.
Available in: Salesforce Classic
Available with a Data.com Clean license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Use Skipped Clean Status to Prevent Data.com Clean Jobs from Updating Records
Use the 
Skipped records
 Data.com Clean job bypass preference, which makes sure records with a 
Clean Status
 of 
Skipped
 are not updated by jobs. This is the recommended method of preventing jobs from updating records.
Use Custom Fields and Validation Rules to Prevent Data.com Clean Jobs from Updating Records
Use a custom field to identify the records you don’t want auto-updated. Then, create a validation rule that tells jobs to skip these records.
See Also:
Set Up Data.com Clean Jobs
Set Up Data.com Clean
Can I prevent Data.com Clean jobs from automatically updating records?
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_preventing_autoupdates.htm&language=en_US
Release
202.14
