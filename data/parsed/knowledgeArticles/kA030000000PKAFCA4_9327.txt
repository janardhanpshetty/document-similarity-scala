### Topic: The article describes what actions to take when encountering mobile verification issues with the customer.
When a user has the mobile number verified ( correct format +1 111111111 ) and the user is logging in from an unknown IP address (outside of trusted IP ranges) or no cookie was established from the prior authentication (login that was challenged and the 5 Security code was entered) Salesforce will challenge the user with a 5-digit verification code.
 
The 5-digit verification code would be sent either in an SMS text message to mobile phone or in an email from Salesforce after entering the username and password to complete the log in.
 
Resolution
Users sometimes will report to Salesforce Support that they are receiving the text message without the 5-digit verification code. Below are steps that will need to perform to address this issue:
 
1. Did the user receive a message that was truncated or did not receive any not message at all? 
If it is truncated message, then proceed to attach the following to your case GUS investigation: 
W-1508074
 , we already have a user story filed in GUS.
 
2. Run Splunk with the organization's ID, the affected user ID and the time SMS (Text message) was sent out.
 
Splunk query for SMS/email verification
index=instance orgID userID logRecordType=scchl OR logRecordType=scmsg OR validCookie=false | convert timeformat="%D:%H:%M:%S" ctime(_time) AS DateTime | table logRecordType, DateTime, organizationId, userId, remoteAddr, stage, transport, country, details, type, identifier, severity, theRest
 
Scchl is for the security identity confirmation log
scmsg is for security message log
L is for Login log
 
If the Splunk log is returning as in the example below:
scmsg`20130708173106.406`3kvpJKm2NG0BGF-WauQ75V`1458598`0`0``````00DE0000000cOJb`005E00000021w1k`1373304666236-1261474269`SmsViaTelesign`INFO`Finished Telesign SMS callout with result of NO_ERROR(0/null)
 
It means that Salesforce does not have any problems sending out the SMS (Text Message) to Telesign (who is the provider who handles the SMS for the feature)
 
3. Check with the  affected user if the mobile number is roaming outside of the country where the number was registered in.
 
4. Check with the user if the mobile carrier of his mobile device is allowed to receive any international text messages?
 
5. Check with Telesign, the provider who sends out SMS messages to the mobile carrier. If they have an outage, we (support) will be notified. In this case, it will affect all users and in a short period of time.
 
Backline Support for Security (Tier 3) engineer can submit a ticket by sending email to support@telesign.com, within the email, please include the mobile number, the country code, the mobile carrier, and the time period that the message. A ticket will be generated and Telesign support responds quickly.
 
Known issue: 
https://success.salesforce.com/issues_view?id=a1p30000000SvxCAAS
 
Workaround:
Remove the mobile number from the  affected user's detail page
Ask the affected user to attempt to log into Salesforce.
Advise the user which option to choose to send the code E-mail or SMS, then ask the user to request another code to be sent.
 
