### Topic: When trying to pass a multi-select picklist value into the query string of a Salesforce page (such as record create/edit), no values are moved from the active to selected side.
When trying to pass a multi-select picklist value into the query string of a Salesforce page (such as record create/edit), no values appear in the url upon selection.  How can this be formatted to work as expected?
Resolution
The query string of an edit page is designed to populate selected multi-select picklist values by referencing the picklist in question in a new parameter for each new value.  In order to better understand this, let us look at a possible example usage for this functionality.
Let's say we have a picklist called 'Interests' on Contacts, a custom field with a Salesforce ID of 00N20000000xXxX, with the following values:
Basketball
Football
Rafting
Climbing
Tennis
The objective of our custom button will be to take these values from an existing contact record and populate a new one.  Let's say that the end-user had chosen Basketball, Football, and Climbing on the record from which to click the button.  Once the button is clicked, the button must be able to create a new parameter for every selected value.  The end result must like the following:
https://xx.salesforce.com/003/e?
00N20000000xXxX=Basketball&
00N20000000xXxX=Football&
00N20000000xXxX=Climbing
Now, when creating the button, our picklist {!Contact.Interests__c} is only capable of returning each selected value separated by semi-colons, for instance: Basketball;Football;Climbing.  As there is no formula function designed to easily format this, we must use Javascript to split the values of {!Contact.Interests__c} into an array, then pair it with the field ID as a parameter in the URL every time it occurs.  The following code is an example of how to do this:
var mplVals = "{!Contact.Interests__c}";
var mplSFID ="00N20000000xXxX";
var mplVals = mplVals.split(";");
var params = "";
for (var i=0;i<mplVals.length;i++){
params = params + mplSFID+ "=" + mplVals[i] + "&";
}
var URL="/003/e?"+ params;
parent.frames.location.replace(URL);




