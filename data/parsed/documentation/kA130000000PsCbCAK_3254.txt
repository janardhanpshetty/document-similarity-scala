### Topic: Managing Territories | Salesforce
Managing Territories | Salesforce
Managing Territories
Available in: Salesforce Classic
Available in: 
Developer
 and 
Performance
 Editions and in 
Enterprise
 and 
Unlimited
 Editions with the Sales Cloud
User Permissions Needed
To manage territories:
“Manage Territories”
OR
You are a forecast manager. Forecast managers can manage territories of those users working below them in territory hierarchy.
Note
This information applies to the original Territory Management feature only, and not to Enterprise Territory Management.
Managing your organization’s territories involves the following tasks:
Organize territories into hierarchies that represent your organization’s forecasting requirements and how users work in your organization.
Configure organization-wide settings for territory management.
Create new territories and edit existing territories.
Assign users to territories.
Define account assignment rules that evaluate accounts and assign them to territories.
Select accounts from lists and manually assign them to territories.
See Also:
Territory Management
Enable Territory Management
Territory Management FAQ
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=territories_manage.htm&language=en_US
Release
202.14
