### Topic: Error 1700: Error Saving Record: Could not authenticate (using OAuth) to Salesforce.com might show up when attempting to sync Salesforce Classic app on iPhone. This article provides the steps on how to restart the application to fix the issue.
Error 1700: Error Saving Record: Could not authenticate (using OAuth) to Salesforce.com 
might show up when attempting to sync Salesforce Classic app using Iphone. 
Resolution
Follow these steps to restart the application and fix the problem. 
From the Mobile Administration menu (within Admin Setup)
1. Click on Salesforce Classic | Users & Devices
2. Click on the affected user then on the related Device (link)
3. Check the device (left side box) and Click DELETE
Alternative option from user-side:
On iPhone SF Classic app
1. AppInfo | Sync | Clear Data | Clear All & Deactivate
 
