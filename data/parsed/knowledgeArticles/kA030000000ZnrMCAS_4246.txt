### Topic: Validation Rule to check correct format for United Kingdom Zip/Postal Code is used
Validation Rule to check correct format for United Kingdom Zip/Postal Code is used for Accounts Billing Zip/Potal Code or for Contact Mailing and Shipping Zip/Postal Code.
Resolution
To 
 check correct format for United Kingdom Zip/Postal Code for Accounts Billing Zip/Potal Code or for Contact Mailing and Shipping Zip/Postal Code is used using Validation Rule follow the below steps:
Click Your Name | Build | Customize | Accounts/Contacts | Validation Rules
Click on New Button
Type the Name of the Validation Rule as per your requirement
Formula to be used for validation rule:
AND( OR(BillingCountry = "UK", BillingCountry = "United Kingdom"), 
NOT(
OR(
ISNULL( Post_Code__c ),
REGEX( Post_Code__c , "[a-zA-Z]\\d\\s\\d[a-zA-Z]{2}"),
REGEX( Post_Code__c , "[a-zA-Z]\\d\\d\\s\\d[a-zA-Z]{2}"),
REGEX( Post_Code__c , "[a-zA-Z]{2}\\d\\s\\d[a-zA-Z]{2}"),
REGEX( Post_Code__c , "[a-zA-Z]{2}\\d\\d\\s\\d[a-zA-Z]{2}"),
REGEX( Post_Code__c , "[a-zA-Z]{2}\\d[a-zA-Z]\\s\\d[a-zA-Z]{2}"),
REGEX( Post_Code__c , "[a-zA-Z]\\d[a-zA-Z]\\s\\d[a-zA-Z]{2}")
)
)
)
Details of formula used:
a-zA-Z = To Define Alphabets can be used in upper or lower case.
\d = Defines Digits to be used between [0-9]
\s = To Define Space
[a-zA-Z]{2} = To Define the number of Alphabets that can used in the first & the second part of the Zip Code (i.e. 2 for example PH42 4RL).
\\d\\d = To Define the number of Digits that can used in the first part of the Zip Code.
