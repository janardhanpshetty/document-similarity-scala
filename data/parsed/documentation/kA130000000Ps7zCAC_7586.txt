### Topic: View a Single Chatter Post | Salesforce
View a Single Chatter Post | Salesforce
View a Single Chatter Post
You can view a single Chatter post in a feed for a user
, record
 or group.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
In a feed, click the timestamp displayed below the post, for example, 
Yesterday at 12:57 AM
.
In a Chatter email notification, click the link in the body of the email to view only the related post.
To view the full feed, above the post, click 
All Updates
.
See Also:
Sharing a Link to a Chatter Post
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_feed_single_update.htm&language=en_US
Release
202.14
