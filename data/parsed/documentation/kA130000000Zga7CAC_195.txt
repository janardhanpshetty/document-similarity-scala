### Topic: Granting Checkout Access | Salesforce
Granting Checkout Access | Salesforce
Granting Checkout Access
Available in: both 
Salesforce Classic
 and 
Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
User Permissions Needed
To grant 
Checkout
 access:
“Manage Billing”
To edit users:
“Manage Internal Users”
Users with the “Manage Billing” permission have access to 
Checkout
 when it is enabled for your organization. These users can also grant access to other users within your organization. Users with 
Checkout
 access can purchase 
Salesforce
 licenses, 
AppExchange
 app licenses, and other related products. Additionally, within 
Checkout
, users can view the organization's quotes, installed products, orders, invoices, payments, and contracts.
To give a user access to 
Checkout
:
From Setup, enter 
Users
 in the 
Quick Find
 box, then select 
Users
.
Click on the appropriate user's name to open the user detail page.
Click 
Edit
.
Select the 
Checkout Enabled
 checkbox. The 
user is notified by email when his or her 
Checkout
 account is activated and available for login.
See Also:
Managing Billing and Licenses
Purchase More Licenses for Your Users
Checkout User Guide
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=granting_checkout_access.htm&language=en_US
Release
200.14
