### Topic: Review a list of Email reports that are compatible with Data Extensions.
Review a list of Marketing Cloud Email reports that are compatible with Data Extensions. 
Resolution
Seven standard email reports use a filter based upon the list used in the Send. All other email reports include all data regardless of  the Send source. Refer to the chart below for a breakdown of all of the email reports and how the sendable Data Extensions are included.
Important: 
For these seven reports, the All Subscribers list should be selected. This returns all Sends over the time period, which includes Data Extension Send, however, the data will not be broken down or filtered by a Sendable Data Extension as it would be if you selected a list. The resulting report will likely be very large and require delivery of the report to the FTP. It's not currently possible to select a Publication List.
  
Email Report
Sendable Data Extension?
A-B Test Summary Report
Yes
Account Send Summary
Yes
Attribute By Tracking Event
Yes
Campaign Email Job Tracking Summary
Yes
Campaign Email Tracking Report
Yes
Campaign Email Tracking Summary
Yes
Conversion Tracking Statistics
Yes
Email Message Frequency
Includes Data Extension sends under All Subscribers
Email Performance By Attribute
Yes
Email Performance by Domain
Yes
Email Performance by List
Yes
Email Performance for All Domains
Yes
Email Performance Over Time
Yes
Forwarding Activity Details
Yes
Impression Tracking by Job
Yes
Impression Tracking For Triggered Send by Period
Yes
List Demographics
Includes Data Extension sends under All Subscribers
List Performance Over Time
Includes Data Extension sends under All Subscribers
List Size Over Time
Includes Data Extension sends under All Subscribers
Recent Email Sending Summary
Yes
Region Performance for Triggered Sends Over Time
Yes
Region Performance Over Time
Yes
Response Trend Analysis for an Email Send
Yes
Send Classification by Email Tracking Report
Yes
Spam Complaints Over Time
Yes
Subscriber Engagement
Includes Data Extension sends under All Subscribers
Subscriber Most Recent Activity
Yes
Subscribers Not Sent To
Includes Data Extension sends under All Subscribers
Triggered Sends Tracking
Yes
Unengaged Subscribers for a List
Includes Data Extension sends under All Subscribers
