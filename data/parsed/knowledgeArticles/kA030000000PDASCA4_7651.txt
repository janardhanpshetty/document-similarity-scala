### Topic: The only ability to report on list views is through the API using a tool such as Eclipse IDE or ANT.
This is done through the API and not our reporting engine.
Resolution
Please explain to the customer the following:
The only ability to report on list views is through the API using a tool such as Eclipse IDE or ANT. This is done by your in house programmers/developers.
Here is the link to the developer force that your developers can read to get the steps. 
http://boards.developerforce.com/t5/Apex-Code-Development/Access-to-List-View/m-p/194026 
or:  www.developer.force.com
I hope this helps.
