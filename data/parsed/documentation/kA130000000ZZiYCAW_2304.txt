### Topic: Portal Roles Limit | Salesforce
Portal Roles Limit | Salesforce
Portal Roles Limit
Limits for portal roles in your organization.
The maximum number of portal roles for an organization is 5000. This limit includes portal roles associated with all of the organization’s customer portals, partner portals, or communities.
 
To prevent unnecessary growth of this number, we recommend reviewing and reducing the number of roles for each of your portals and communities. Additionally, delete any unused portal roles.
 
If you still require more portal roles, please contact Salesforce Customer Support.
See Also:
Communities Limits
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_communities_portal_role_limit.htm&language=en_US
Release
202.14
