### Topic: How do I segment Insights results by other segment types such as sentiment, media type, language or keyword groups in a Topic Analysis widget?
How do I segment Insights results by sentiment, media type, language or keyword groups in a Topic Analysis widget?
Resolution
When segmenting within the Topic Analysis widget, results can be filtered by  all Insights, Insights by Provider, Frequently Used Insights or filters from the view by options such as language, sentiment or media type. 
 
In order to combine Insights with the other segmenting options, you must segment by Insights last. Once you segment by Insights, any subsequent segmentation must be by an Insight. 
 
For example, to see the results of
 Basic Demographics: Age
 for each keyword group in a Topic Profile:
 
1. Configure a Topic Analysis widget and click the resulting chart.
3. Click
 View by Keyword Groups
.
4. Click a keyword group and choose 
Insights by Provider | Basic Demographics | Age
.
