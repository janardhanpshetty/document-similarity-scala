### Topic: Tasks | Salesforce
Tasks | Salesforce
Tasks
Tasks help you to track and measure the activities associated with your patients. From the Health Cloud Console, you can ask a caregiver to drive a patient to an appointment, remind yourself to follow up on a missed appointment, or assign a patient a pre-admission survey to complete.
In Health Cloud, you can create a task from several places:
Save time and effort by creating tasks for individual patients, groups of patients, or even yourself from the patient list. Select patients from your patient list, create the task, and you’re done! It’s quick and easy to track things like wellness surveys or followup questionnaires that go to a group of your patients with common conditions. You can also assign yourself a task related to patient so that you can track important milestones.
You can also create tasks for a patient or a caregiver from the patient’s care plan. These tasks are specific measurable actions toward the goals that mitigate the associated problem. For example, create a task to attend weekly physical therapy sessions for an arthritis patient whose goal is to keep or increase range of motion.
When a care team member is part of the community, you can assign tasks to care team members using a link in their profile. These tasks are not directly tied to the problems and goals associated with the care plan.
All tasks have fields for basic information, like description, due date, task type, who the task is assigned to, and who is responsible for performing the task. Tasks that are created from within the care plan also include fields to let you associate the task with a specific problem and goal.
When you create a task for a care team member, the task is always assigned to that member. When you create a task from the patient list, you have two choices for the performer of the task: yourself or the patient.
You can track tasks belonging to you and to any care team members from the care plan task list. Use filters to show all the tasks related to the patient, tasks that are related to the goals and problems in the care plan, or unrelated tasks that aren’t specific to a goal or problem.
Patients see their tasks from within the community. Tasks you’ve assigned to yourself appear in your task list in the Salesforce Home tab, the Today page, and in the task list of the patient’s care plan.
Create Tasks Related to Your Patients and Their Care
Creating tasks for your patients or their caregivers is an easy way to track and manage activities related to a care plan.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tasks_overview.htm&language=en_US
Release
202.14
