### Topic: Chatter Chat is disabled if the Organization has the "User sharing" & "User Sharing - Allow Private Internal OWD " BT perms enabled and the User Sharing Org-wide Default is set to private.
Chatter Chat is disabled if the Organization has the BT permissions below enabled:
"User sharing"
&  
"User Sharing - Allow Private Internal OWD "
 and the User Sharing Org-wide Default is set to private.
Resolution
In this scenario, this behavior is WAD: currently the chatter chat does not obey the user sharing rules and hence we had turned off the chat if the above permissions are enabled.
For more 
information 
see "User Sharing Overview": https://org62.my.salesforce.com/kA130000000Prnq and "Understanding User Sharing": https://org62.my.salesforce.com/help/pdfs/en/salesforce_sharing_users_tipsheet.pdf
