### Topic: Use a Summary Function in a Custom Summary Formula | Salesforce
Use a Summary Function in a Custom Summary Formula | Salesforce
Use a Summary Function in a Custom Summary Formula
Summary functions let you use grouping values in custom summary formulas for summary, matrix, and joined reports. There are two summary functions: 
PARENTGROUPVAL
 and 
PREVGROUPVAL
.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except 
Database.com
User Permissions Needed
To create, edit, and delete reports:
“Create and Customize Reports”
AND
“Report Builder”
Double-click 
Add Formula
 in the Fields pane.
In the Custom Summary Formula dialog, under Functions, select 
Summary
.
Select 
PARENTGROUPVAL
 or 
PREVGROUPVAL
.
Select the grouping level and click 
Insert
.
Define the formula, including where to display the formula.
Click 
OK
.
Parent topic:
 
Work with Formulas in Reports
Previous topic:
 
Build a Custom Summary Formula
Next topic:
 
PARENTGROUPVAL and PREVGROUPVAL
See Also:
Work with Formulas in Reports
PARENTGROUPVAL and PREVGROUPVAL
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_summary_functions_using.htm&language=en_US
Release
202.14
