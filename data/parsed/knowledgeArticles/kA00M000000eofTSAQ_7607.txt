### Topic: Describes considerations for "interaction properties" (isGlobal, useGlobal, isFacet, autoFilter) in steps within Wave Analytics dashboards.
Wave Analytics dashboards contain steps that define the queries behind visible components (widgets). There are several properties that control how a step interacts with other dashboard components.
isGlobal indicates whether the filter that’s specified in the query is used as a global filter (
true
) or not (
false
). Default is 
false
. This property can only be applied to steps that are connected to a scope widget.
useGlobal indicates whether the step uses the dashboard’s global filter (
true
) or not (
false
).
isFacet enables bi-directional faceting between this step and other steps built from the same dataset. This property only works for compact form queries. To use with SAQL, ensure that autoFilter is also set to true.
autoFilter enables bi-directional faceting within the SAQL query.
Note: A step should never have both "isGlobal" and "isFacet" set as true. This will cause undesired behaviour.
Resolution
Depending on your use case, there are specific configurations of these properties to meet situational needs.
Global scope:
"isGlobal": true,
"useGlobal": false,
"isFacet": false,
"autoFilter": false,
 
Respects global, no other faceting:
"isGlobal": false,
"useGlobal": true,
"isFacet": false,
"autoFilter": false,
 
Respects global, faceted, no SAQL:
"isGlobal": false,
"useGlobal": true,
"isFacet": true,
"autoFilter": false,
 
Respects global, faceted, SAQL:
"isGlobal": false,
"useGlobal": true,
"isFacet": true,
"autoFilter": true,
 
Standalone (ignores global, no faceting):
"isGlobal": false,
"useGlobal": false,
"isFacet": false,
"autoFilter": false,
 
Ignores global, faceted, no SAQL:
"isGlobal": false,
"useGlobal": false,
"isFacet": true,
"autoFilter": false,
 
Ignores global, faceted, SAQL:
"isGlobal": false,
"useGlobal": false,
"isFacet": true,
"autoFilter": true,
