### Topic: Change Your Connect Offline Briefcase Account Selection | Salesforce
Change Your Connect Offline Briefcase Account Selection | Salesforce
Change Your Connect Offline Briefcase Account Selection
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
If an administrator does not assign a briefcase configuration to your profile, your briefcase contains the 
default briefcase contents
, which includes accounts that are associated with opportunities you own, accounts that you 
manually include
, and accounts related to your contacts. To change the selection of accounts in your Connect Offline briefcase:
From your personal settings, enter 
Connect Offline
 in the 
Quick Find
 box, then select 
Connect Offline
.
Click 
Briefcase Setup
.
Click 
Edit
 next to Briefcase Settings.
Choose one of the following options to specify the accounts you want to include in your briefcase:
Manually selected — Up to 
100
 accounts that you 
manually include
 by clicking the 
Include Offline
 button on the account detail page, and accounts related to your contacts.
Opportunity-based briefcase — Up to 5000 accounts that are associated with opportunities you own (
in addition to accounts that you 
manually include
 and accounts related to your contacts
). The following restrictions apply:
You must have at least read access to the account.
The opportunity must be open or have a close date in the past two months or up to 24 months in the future.
Opportunity-based briefcase is the default briefcase configuration.
Activity-based briefcase — Up to 5000 accounts related to the activities in your briefcase (in addition to accounts that you 
manually include
 and accounts related to your contacts).
All my accounts — Up to 5000 accounts you own or where you are on the account team (in addition to accounts that you 
manually include
 and accounts related to your contacts).
Click 
Save
.
Important
If an administrator assigns a briefcase configuration to your profile after you change the selection of accounts in your briefcase, the assigned briefcase configuration overrides your briefcase account selection the next time you synchronize Connect Offline.
See Also:
Log in to Connect Offline
Personalize Your Salesforce Experience
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=changing_the_briefcase_settings.htm&language=en_US
Release
202.14
