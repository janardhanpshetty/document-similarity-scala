### Topic: This article would help you with the steps to enable Compact Feed View.
How can I enable the compact feed view?
Resolution
To enable compact feeds, from the Feed View settings page for a feed-based case page layout, select Enable Compact Feed View in the Console.
Go to Setup/ Cases/ Page Layouts
Select a Feed Based Layout
Click on the "Feed View" link in the right hand corner (see screenshot below)
