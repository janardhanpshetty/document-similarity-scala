### Topic: Join us for a virtual best practices discussion with other Salesforce customers to collaboratively reimagine your customer, partner, and employee engagement using Salesforce's Community Cloud.
Using Communities to Improve Interaction and Collaboration
  
Communities can help build strong relationships with partners, customers, as well as employees. Drive more sales by connecting your employees with your distributors, resellers, and suppliers; deliver world-class service by giving your customers one place to get answers; and manage social listening, content, engagement, and workflow all in one place! 
 
Join us to collaboratively learn about:
 
Various use-cases for Community Cloud
How Salesforce uses Community Cloud
Best practices to improve adoption amongst Community Members
Target Audience
 
Community Managers, Salesforce Administrators and company stakeholders involved in managing relationships with customers, partners and employees
Learning Objectives
You will walk out of this Circles of Success best practice discussion with:
 
The ability to launch and maintain a successful user community
Best practices to keep your community active and engaged
Resources to help you going forward
Pre-Requisites
 
Access to a PC or Mac with ability to install 
GoToWebinar
Success Community Topics:
Any discussions/collaboration on the Salesforce Success Community relevant to this Roundtable will be tagged with the following Topics.
 
#CoS
#CirclesOfSuccess
#CommunityCloud
 
For more Community resources check out the 
Getting Started with Community Cloud Hub
 
Resolution
