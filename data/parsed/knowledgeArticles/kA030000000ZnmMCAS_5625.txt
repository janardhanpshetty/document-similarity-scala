### Topic: This is the process for having Salesforce Support Increase the Max Roll Up Summary Field limit for your Organization.
Available in: 
Contact Manager, Group, Professional, Enterprise, Performance, Unlimited, Developer,
 and 
Database.com 
Editions
Some organizations find that the default 
Max Roll-Up Summary Field limit
 per object is not sufficient to meet their needs. If you find that your organization's business process requires an increase to this limit, please take the following steps:
Resolution
Have a System Administrator log a Case with Salesforce Support requesting an increase to the 
Max Roll-Up Summary Field limit
 
Be sure to include the limit you are requesting this be increased to
Please also include a business reason to give us an idea of the background for this increase request.
Our Support Team will review the request and then action it accordingly.
Please note: as per Winter '16 release the default limit has been increased from 10 into 25.
Roll-up Summary Fields have performance implications hence an organization should not use more than 25 roll-up summary fields and should try to setup the reports to make those calculations.
