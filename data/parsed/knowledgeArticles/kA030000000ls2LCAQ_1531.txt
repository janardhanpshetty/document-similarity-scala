### Topic: Does Radian6 cover Foursquare?
Does Radian6 cover Foursquare?
Resolution
Radian6 does not cover Foursquare; however, a subset of Foursquare Tips content is saved as the Forums media type. Radian6 can cover Tweets that contain Foursquare information for users that have connected their Foursquare account to their Twitter account. 
