### Topic: This article describes a limitation related to importing contacts from mobile devices when State and Country picklist fields are enabled.
When importing contacts from an Android or iOS device using Salesforce1 into an organization that has State and Country picklist fields enabled, the State field isn’t populated. 
For more information, see "
Import Contacts from Mobile Device Contact Lists into Salesforce
" in the Salesforce Help.
 
Resolution
 
