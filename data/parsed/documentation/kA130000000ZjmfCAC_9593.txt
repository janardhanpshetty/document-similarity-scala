### Topic: Considerations for the Cloud Flow Designer | Salesforce
Considerations for the Cloud Flow Designer | Salesforce
Considerations for the Cloud Flow Designer
When you create a flow in the Cloud Flow Designer, familiarize yourself with its limitations and behaviors. For example, it supports a handful of locales and can’t open flows from managed packages.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
At run time, time zones for date/time values can differ from what you see in the Cloud Flow Designer. During run time, date/time values reflect the running user's time zone settings in Salesforce. In the Cloud Flow Designer, date/time values reflect the time zone set on your computer. The Cloud Flow Designer appends the GMT offset to your date/time value.
The Cloud Flow Designer doesn't support UTF-8 encoding for text in user input fields.
The Cloud Flow Designer contains embedded fonts for all locales it supports. The supported locales are:
English (US)
French (France)
German (Germany)
Spanish (Spain)
Japanese (Japan)
Chinese (Traditional)
Chinese (Simplified)
If you enter unsupported characters for a supported locale, they’re displayed using system fonts instead of the embedded fonts.
In unsupported locales, your system font settings are used to display all characters in the Cloud Flow Designer.
The Cloud Flow Designer can’t open flows that are installed from managed packages.
Don’t enter the string 
null
 as the value of a text field in the Cloud Flow Designer.
The Cloud Flow Designer has access to information that exists when you open it. If you modify data or metadata in your organization and need to refer to it in a flow, close and reopen the Cloud Flow Designer. For example, if you add a custom field or modify an Apex class with the Cloud Flow Designer open, close and reopen the Cloud Flow Designer.
The Cloud Flow Designer uses the permissions and locale assigned to the current user.
If you open a flow that was last opened in Winter ’12 or earlier, each Boolean decision is converted to a multi-outcome Decision element that:
Uses the same name as the old decision.
Takes the unique name of the old decision, appended with “_switch”.
Has an outcome labeled “True”. This outcome’s unique name matches that of the old decision, and its conditions are migrated from the True outcome of the old decision.
Has a default outcome labeled “False”.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=vpm_considerations_design_cfd.htm&language=en_US
Release
202.14
