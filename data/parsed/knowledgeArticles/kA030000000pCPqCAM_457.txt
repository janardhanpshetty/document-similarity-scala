### Topic: Validation Status
When portal users do a reset password from the portal link, emails are not signed by their DKIM
Resolution
This is a known issue with functionality, DKIM check the email headers of the email. and it should be similar to this. 
 
X-Priority: 1
X-SFDC-LK: ***************
X-SFDC-User: ***************
X-Sender: postmaster@salesforce.com
X-mail_abuse_inquiries: http://www.salesforce.com/company/abuse.jsp
X-SFDC-TLS-NoRelay: 1
X-SFDC-EmailCategory: Not Specified
X-SFDC-Binding: Default <--- "This should be user binding for the app to pick up org's DKIM settings." 
X-SFDC-Interface: internal
Please associate all Cases that report this issue with the following : Bug #: 
W-2585612
Known Issue link: https://success.salesforce.com/issues_view?id=a1p30000000javHAAQ
