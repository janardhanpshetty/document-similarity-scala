### Topic: Guidelines and Best Practices for Implementing My Domain | Salesforce
Guidelines and Best Practices for Implementing My Domain | Salesforce
Guidelines and Best Practices for Implementing My Domain
These tips help smooth the transition to a new domain name.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Performance
, 
Unlimited
, 
Enterprise
, 
Developer
, 
Professional
, and 
Group
 Editions.
Communicate the upcoming change to your users before deploying it.
Deploy your new domain when your org receives minimal traffic, like during a weekend, so you can troubleshoot while traffic is low.
Before deploying, first test the login policy customizations, custom UI features, Visualforce pages, and application URL changes in an sandbox environment.
If you’ve customized your Salesforce UI with features such as custom buttons or Visualforce pages, make sure that you test custom elements thoroughly before deploying your domain name. Look for hard-coded references and instance-based URLs in your customizations. Use your custom domain URLs instead.
Make sure that you update all application URLs before you deploy a domain name. 
For example, the 
Email Notification URL
 field in Chatter Answers continues to send notifications with the old URLs to internal users unless you update it.
If your domain is registered but has not yet been deployed, URLs contain your custom domain name when you log in from the My Domain login page. However, links that originate from merge fields that are embedded in emails sent asynchronously, such as workflow emails, still use the old URLs. 
After
 your domain is deployed, those links show the new My Domain URLs.
Help your users get started using your new domain name by providing links to pages they use frequently, such as your login page. Let your users know if you changed the login policy, and encourage them to update their bookmarks the first time they’re redirected.
Choose the Redirect Policy option 
Redirected with a warning to the same page within the domain
 to give users time to update their bookmarks with the new domain name. After a few days or weeks, change the policy to 
Not redirected
. This option requires users to use your domain name when viewing your pages. It provides the greatest level of security.
Only use 
Prevent login from https://login.salesforce.com
 if you’re concerned that users who aren’t aware of your custom domain try to use it. Otherwise, leave the option available to your users while they get used to the new domain name.
Bookmarks don’t work when the 
Redirect to the same page within the domain
 option is selected for partner portals. Manually change the existing bookmarks to point to the new domain URL by replacing the Salesforce instance name with your custom domain name. For example, replace 
https://
yourInstance
.salesforce.com/
 with 
https://
yourDomain
.my.salesforce.com/
 in the bookmark’s URL.
If you block application page requests that don’t use the new Salesforce domain name URLs, let your users know that they must either update old bookmarks or create new ones for the login page. They must also update any tabs or links within the application. Users are required to use the new URLs immediately if you change your login redirect policy to 
Not Redirected
.
If you are using My Domain, you can identify which users are logging in with the new login URL and when. From Setup, enter 
Login History
 in the 
Quick Find
 box, then select 
Login History
 and view the Username and Login URL columns.
On the 
login.salesforce.com
 page, users can click 
Log in to a custom domain
 to enter your custom domain name and log in. In this case, they must know the domain name. As a safeguard, give them a direct link to your custom domain’s login page as well.
If you have the following.
Do the following.
API integrations into your org
Check to see if the API client is directly referencing the server endpoint. The API client should use the 
LoginResult.serverURL
 value returned by the login request, instead of using a hard-coded server URL.
After your custom domain is deployed, Salesforce returns the server URL containing your domain. Redirect policy settings have no effect on API calls. That is, old calls to instance URLs continue to work. However, the best practice is to use the value returned by Salesforce.
Email templates
Replace references to the org’s instance URL with your custom domain.
Custom Visualforce pages or custom Force.com apps
Replace references to the org’s instance URL with your custom domain. See 
How to find hard-coded references with the Force.com IDE
.
Chatter
Tell your users to update any bookmarks in the left navigation of their Chatter groups.
Zones for Communities (Ideas/Answers/Chatter Answers)
Manually update the 
Email Notification URL
.
To update the URL, clear the existing URL so that the field is blank and save the page. Then the system populates the field with your new My Domain URL.
See Also:
My Domain URL Changes
Test and Deploy Your New Domain Name
My Domain
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=domain_name_guidelines.htm&language=en_US
Release
202.14
