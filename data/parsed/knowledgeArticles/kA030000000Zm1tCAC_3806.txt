### Topic: Console List View does not refresh when new Case records are created
In a Service Console App, the Console List View does not refresh when new Cases are created that match the List View Criteria. The App is configured like so:  
Choose How Lists Refresh:  Refresh Lists
Choose How Detail Pages Refresh:  Automatic
 
Resolution
This is working as designed. We have a Streaming API Filter Query that controls how the filter is
 searching for the records and qualifying them. Basically, in order for the List View to refresh, the Case Owner ID must match the viewing Users ID. 
If you are routing to a queue (via Email2Case), the viewer ID is probably not equal to the queue ID, so new email2case records don't trigger a refresh. 
The reason it doesn't work for manually creating cases is the same as why it won't work for E2C; because the viewer ID won't always match the owner ID in the topic parameters. If you had an org with 1000 users all creating cases and didn't filter on the owner, everyone's lists would be constantly refreshing and that renders the functionality useless.
If you want different behavior (refreshing when new cases come in for other owners) you could setup your own Push Topics implementation using the 
Streaming API
 
