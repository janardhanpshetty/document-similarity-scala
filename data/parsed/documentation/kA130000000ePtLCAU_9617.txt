### Topic: Define an SSO User Mapping | Salesforce
Define an SSO User Mapping | Salesforce
Define an SSO User Mapping
You can manually define a single-sign on (SSO) user mapping between a user in the Environment Hub and a user in a member org. Before you define a user mapping, enable SSO in the hub member org.
User Permissions Needed
To set up and configure the Environment Hub:
“Manage Environment Hub”
User mappings can be many-to-one but not one-to-many. In other words, you can associate multiple users in the Environment Hub to one user in a member org. For example, if you wanted members of your QA team to log in to a test org as the same user, you could define user mappings.
Log in to the Environment Hub, and then select a member org. If you don’t see any member orgs, check your list view.
Go to the Single Sign-On User Mappings related list, and then select 
New SSO User Mapping
.
Enter the username of the user that you want to map in the member org, and then look up a user in the Environment Hub.
Select 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=environment_hub_sso_mapping_create.htm&language=en_US
Release
202.14
