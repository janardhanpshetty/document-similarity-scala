### Topic: Unable to schedule Data Export
Customers may encounter an issue when attempting to schedule a Weekly Data Export.  Upon saving, the page refreshes but the scheduled export does not appear.  The can be fixed by Tier 3.  Please escalate requesting Tier 3 to run the scrutiny for ScrutinyCronSchedulerBackedUpJobs.
 
Tier 3 action - Run CronScheduler - ScrutinyCronSchedulerBackedUpJobs
Resolution
