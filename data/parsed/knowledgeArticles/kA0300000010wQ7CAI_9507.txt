### Topic: Automation Studio (AS) is the marketing automation tool within the Marketing Cloud that give users the power to set it and forget it. Using AS for your ongoing marketing campaigns will allow you to set the email sends, lists, and all other elements needed and let the programs run on their own. You sends can be a single send or a series of emails going to subscribers with time in between.
Automation Studio (AS) is the marketing automation tool within the Marketing Cloud that give users the power to set it and forget it. Using AS for your ongoing marketing campaigns will allow you to set the email sends, lists, and all other elements needed and let the programs run on their own. You sends can be a single send or a series of emails going to subscribers with time in between. 
What to think about when creating campaigns:
What campaigns do you hit ‘send’ for that could be automated?
Think about how you like to be communicated with - what is the cadence you respond too?
Are their individual emails you send that could be better in a drop campaign format? (Drip Campaign: multiple emails sent over a period of time)
Ways the Automated Sends or Drip Campaigns can be effective:
Build excitement leading up to a sale or event
Educate you subscriber about your brand
Re-engage subscribers that have been ‘absent’
Suggest additional products or features after a purchase
Educate on a product or feature
On-board customer to your company or a membership program
When thinking about the customer lifecycle - automations can be inserted into every stage.
Acquire/Onboard: Welcome Series
Instead of sending one welcome email, send three. The message can vary with each email but should adhere to an overall theme of familiarizing your subscriber with your brand and the ways you intend to interact with them moving forward.
Engage: Sale Lead-up or Teaser
If you have a big sale or promotion come up don’t just blast it out on one day. Build excitement leading up to it, tease some of the products and deals, keep them wanting more. Figure out the best timing to build excitement, but by send more than one email at different times of day, you are more likely to engage more of your list with the messaging.
Convert/Grow: Post Purchase Communication
Do you send a purchase confirmation email and then they get put back into the general marketing cycle? Why not use Automation to nurture that sale a bit more. Setting a drip campaign to follow a purchase allows you to take advantage of a very engaged customer - use this to suggest additional product, get feedback on the product and offer a discount for a future purchase made in X days.
Retain/Re-Engage: Re-engagement Campaign
If you have a subscriber that has not engaged with your emails in at least 6 months, set them on a path to re-engage. By asking them to ‘re-subscribe’, maybe giving them an offer, you have the potential to win back some absent clients, but also have the opportunity to clean your list and stop sending to unengaged subscribers which could be hurting your sender reputation. We recommend a three email approach to re-engagement and at the end of the three send, if no action is taken, you can move this subscriber to ‘Unsubscribed’.
There are many more use cases for creating Email Automations and Drip Campaigns. The easiest way to think about them is how would a conversation go between two people and what types of information do you want to tell your customers in manageable segments. 
For more resources check out the 
Achieve More with Salesforce: Gain Operational Efficiencies with Email Studio
Resolution
