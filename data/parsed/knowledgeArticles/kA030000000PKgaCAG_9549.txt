### Topic: This articles discusses the processes for nominating a customer for the Generic Streaming feature on their request.
From the release notes:  "You can now use the Streaming API to send custom event notifications that are not tied to Salesforce data changes.  For information on enabling generic streaming for your organization, contact salesforce.com, inc."
Pilot / Feature Name:    Generic Streaming
Product Manager:   Alex Toussaint
R&D Program URL:    None yet.
Is there a questionnaire the customer has to answer?:  None yet.
Can Backline enable this feature in a Developer or Sandbox organisation before a customer's pilot nomination is approved?:  No.
Skill Group:  Integration.
 
Resolution
Please take the following steps to nominate this customer:
- Ask them to complete the survey detailed in the R&D Program record.
- Create a Pilot Participant record ( a child of the R&D Program Record) and add the completed questionnaire.
- Inform the customer that they have been nominated, and they may or may not be contacted at some point by Product Management.
- Inform the customer that they should follow up with their Salesforce account team on the status of the nomination (SAM, CST, or AE if there is no SAM or CST).
- @Mention the SAM, CST (or AE if there is no SAM or CST) on the case, providing the link to the R&D Pilot Participant record.
- Close the case.
If there is no R&D Program record
, please ask on the appropriate skill group's Chatter group asking what actions need to be taken for this pilot.
If there is an R&D Program record but no survey
, skip the first step above and proceed with the remaining steps.
