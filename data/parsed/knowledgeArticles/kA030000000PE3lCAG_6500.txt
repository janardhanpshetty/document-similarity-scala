### Topic: Learn how to resolve common browser issues by clearing your cache and cookies in Safari.
Clearing your cache and cookies is a first troubleshooting step to resolving many issues, like a slow loading page. Follow the steps below. 
Note:
 For more information on Safari, please visit their help pages. 
Resolution
Clearing cache
 
1. In Safari, click the 
Safari
 menu | 
Empty Cache
. 
2. In the confirmation window, click 
Empty
.
 
Clearing cookies
 
1. Click the 
Safari
 menu |  
Preferences
. 
2. Click the 
Security
 icon. 
3. Click 
Show Cookies
. 
4. Click 
Remove All. 
 
5. Close the Preferences window to continue web browsing. 
