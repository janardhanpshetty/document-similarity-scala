### Topic: Just-in-Time Provisioning Errors | Salesforce
Just-in-Time Provisioning Errors | Salesforce
Just-in-Time Provisioning Errors
Following are the error codes and descriptions for Just-in-Time provisioning for SAML.
SAML errors are returned in the URL parameter, for example:
http://login.salesforce.com/identity/jit/saml-error.jsp?
ErrorCode=5&ErrorDescription=Unable+to+create+user&ErrorDetails=
INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST+TimeZoneSidKey
Note
Salesforce redirects the user to a custom error URL if one is specified in your SAML configuration.
Error Messages
Code
Description
Error Details
1
Missing Federation Identifier
MISSING_FEDERATION_ID
2
Mis-matched Federation Identifier
MISMATCH_FEDERATION_ID
3
Invalid organization ID
INVALID_ORG_ID
4
Unable to acquire lock
USER_CREATION_FAILED_ON_UROG
5
Unable to create user
USER_CREATION_API_ERROR
6
Unable to establish admin context
ADMIN_CONTEXT_NOT_ESTABLISHED
8
Unrecognized custom field
UNRECOGNIZED_CUSTOM_FIELD
9
Unrecognized standard field
UNRECOGNIZED_STANDARD_FIELD
11
License limit exceeded
LICENSE_LIMIT_EXCEEDED
12
Federation ID and username do not match
MISMATCH_FEDERATION_ID_AND_USERNAME_ATTRS
13
Unsupported provision API version
UNSUPPORTED_VERSION
14
Username change isn't allowed
USER_NAME_CHANGE_NOT_ALLOWED
15
Custom field type isn't supported
UNSUPPORTED_CUSTOM_FIELD_TYPE
16
Unable to map a unique profile ID for the given profile name
PROFILE_NAME_LOOKUP_ERROR
17
Unable to map a unique role ID for the given role name
ROLE_NAME_LOOKUP_ERROR
18
Invalid account
INVALID_ACCOUNT_ID
19
Missing account name
MISSING_ACCOUNT_NAME
20
Missing account number
MISSING_ACCOUNT_NUMBER
22
Unable to create account
ACCOUNT_CREATION_API_ERROR
23
Invalid contact
INVALID_CONTACT
24
Missing contact email
MISSING_CONTACT_EMAIL
25
Missing contact last name
MISSING_CONTACT_LAST_NAME
26
Unable to create contact
CONTACT_CREATION_API_ERROR
27
Multiple matching contacts found
MULTIPLE_CONTACTS_FOUND
28
Multiple matching accounts found
MULTIPLE_ACCOUNTS_FOUND
30
Invalid account owner
INVALID_ACCOUNT_OWNER
31
Invalid portal profile
INVALID_PORTAL_PROFILE
32
Account change is not allowed
ACCOUNT_CHANGE_NOT_ALLOWED
33
Unable to update account
ACCOUNT_UPDATE_FAILED
34
Unable to update contact
CONTACT_UPDATE_FAILED
35
Invalid standard account field value
INVALID_STANDARD_ACCOUNT_FIELD_VALUE
36
Contact change not allowed
CONTACT_CHANGE_NOT_ALLOWED
37
Invalid portal role
INVALID_PORTAL_ROLE
38
Unable to update portal role
CANNOT_UPDATE_PORTAL_ROLE
39
Invalid SAML JIT Handler class
INVALID_JIT_HANDLER
40
Invalid execution user
INVALID_EXECUTION_USER
41
Execution error
APEX_EXECUTION_ERROR
42
Updating a contact with Person Account isn’t supported
UNSUPPORTED_CONTACT_PERSONACCT_UPDATE
See Also:
About Just-in-Time Provisioning for SAML
Just-in-Time Provisioning for Portals
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sso_jit_errors.htm&language=en_US
Release
202.14
