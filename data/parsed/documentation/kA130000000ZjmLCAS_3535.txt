### Topic: Send a Chat Conferencing Request | Salesforce
Send a Chat Conferencing Request | Salesforce
Send a Chat Conferencing Request
As wise as support agents are, sometimes a single support agent doesn’t have all the information that’s required to solve a customer’s problem. Chat conferencing lets you invite one or more agents into your customer chats. That way, your agents can turn boring chats into veritable support parties for your customers—all without disrupting the flow of conversation! Send a chat conferencing request to ask another agent to join you in a customer chat.
Available in: Salesforce Classic
Live Agent is available in: 
Performance
 Editions and in 
Developer
 Edition orgs that were created after June 14, 2012
Live Agent is available in: 
Unlimited
 Edition with the Service Cloud
Live Agent is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions
User Permissions Needed
To create and edit configurations:
“Customize Application”
To enable chat conferencing:
“Enable Live Agent Chat Conference”
Note
You can conference in a single agent, or send a request to all agents and the first to accept will join the conference. You can conference multiple agents into a chat, but you need to send each request individually.
While you’re chatting with a customer, click 
.
Select the skill group of the agents that you want to transfer into the chat.
Select whether to send the conference request to all agents with that skill or to a specific agent.
Click 
Conference
 to send the conference request.
If the agent accepts the conference request, you see a notification in the chat log, and that agent can start chatting with you and the customer. If the agent declines the request, you see a notification above the chat log. The customer receives a notification when an agent joins or leaves a conference.
If you decide to exit the conference, click 
Leave
, and then click 
Leave
 again.
If the other agent leaves the conference, you'll see a notification in the chat log.
Any saved and attached records will open for other agents who join the conference. But only the originating or longest-attending agent will be able to attach other records. If the longest-attending agent attaches or removes records during the conference, other agents won't see these changes in their workspaces.
For more about transferring chats and workspaces, see 
Transfer Chats
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=live_agent_conference_in_chats.htm&language=en_US
Release
202.14
