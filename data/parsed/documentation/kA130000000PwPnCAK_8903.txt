### Topic: Remove Topics from Posts | Salesforce
Remove Topics from Posts | Salesforce
Remove Topics from Posts
Remove a topic from a post if it no longer applies.
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
User Permissions Needed
To remove topics from posts:
“Assign Topics”
You can remove topics from posts in the feed, whether they’re regular topics or hashtag topics. Removing topics doesn’t delete them altogether; it only removes them from the post. Deleting the text of hashtags doesn’t delete hashtag topics. 
Before you remove a topic from a post, consider whether you or someone else added it. Someone else may be tracking the update with topics you’re not aware of.
In the top corner of the post, click 
.
Click 
Edit Topics
.
Click 
 next to the topic you want removed from the post.
Click 
Done
 or press ENTER.
Removing a topic from a post removes the post from the topic feed on the topic detail page and from the feeds of any followers.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_topics_remove.htm&language=en_US
Release
202.14
