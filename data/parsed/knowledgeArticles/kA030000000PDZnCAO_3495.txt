### Topic: If you have an old salesforce org, or account, and you want to create a new org with the same usernames that were in the previous account, you will first have to change the original usernames. All salesforce usernames must be unique across all users.
How can I remove users from a previous or old salesforce account?
Resolution
If you have an old salesforce org, or account, and you want to create a new org with the same usernames that were in the previous account, you will first have to change the original usernames. All salesforce usernames must be unique across all users.
This will have to be done by the users or by an administrator within the organization.
Each user may change their username through the following process:
1. Click on:
Setup | Personal Setup | My Personal Information | Personal Information.
2. Click "Edit" to change your username
(a suggested change is to add a "1" at the end of the current user name).
An administrator can change the usernames by:
1. Click on:
Setup | Administration Setup | Manage Users | Users.
2. Click "Edit" next to the user's name to change the username.
Note: The username and email address may be different. You should leave the email address as a valid address. If you no longer have any use for the old organization, please log a case letting salesforce.com know about it.
In the event that the administrator is unable to access the old organization, they can reach out to Salesforce support to free up the usernames. This will apply for orgs that has expired or locked. 
