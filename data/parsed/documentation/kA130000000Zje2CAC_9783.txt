### Topic: Execute a SOQL Query or SOSL Search | Salesforce
Execute a SOQL Query or SOSL Search | Salesforce
Execute a SOQL Query or SOSL Search
Execute SOQL queries or SOSL searches in the Query Editor panel of the Developer Console.
Enter a SOQL query or SOSL search in the Query Editor panel.
If you want to query tooling entities instead of data entities, select 
Use Tooling API
.
Click 
Execute
. If the query generates errors, they are displayed at the bottom of the Query Editor panel. Your results display in the Query Results grid in the Developer Console workspace.
Warning
If you rerun a query, unsaved changes in the Query Results grid are lost.
To rerun a query, click 
Refresh Grid
 or click the query in the History panel and click 
Execute
.
For information on query and search syntax, see the 
Force.com SOQL and SOSL Reference
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=code_dev_console_tab_query_editor_soql_sosl.htm&language=en_US
Release
202.14
