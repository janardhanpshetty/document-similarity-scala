### Topic: Sometimes existing Reports or Dashboard folders may be unavailable in search due to some bad data in the Search table. Members of Tier 3 Analytics can run a scrutiny on the org that will resolve the issue.
Sometimes, Report or Dashboard folders are not searchable, even though they still exist in the organization.
Symptoms:
Reports or Dashboards in the folder are still available
The folder is still available via Data Loader, Workbench, or a direct link
The folder may show up if variations of search terms are searched (for example, searching "dashboard" instead of "dashboards ")
Resolution
This is caused by bad data in a search table. Members of Tier 3 Analytics can run a scrutiny on the org that will resolve the issue.
