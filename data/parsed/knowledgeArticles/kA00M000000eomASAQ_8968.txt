### Topic: An "Insufficient Privileges" error is received when accessing a Sandbox Detail Page, even by System Administrators. The issue can be caused by a Sandbox referencing a Sandbox Template that does not exist in the org.
An "
Insufficient Privileges
" error is received when accessing a Sandbox Detail Page, even by System Administrators.
 
Resolution
The issue can be caused by a Sandbox referencing a 
Sandbox Template
 that does not exist in the org.
To resolve the issue: 
Go to the 
Sandbox List
 page for the org on the blacktab.
Click on the "
API Bpos
" subtab.
Click on the "
Show Api Bpos
" button. 
Click the + (plus) sign beside the affected Sandbox name. 
The section "
Sandbox Info
" has a Template field which should show "00000000000". 
Click Edit and that field should then show as blank. 
Click to save and then the Sandbox Detail page should then be accessible.
