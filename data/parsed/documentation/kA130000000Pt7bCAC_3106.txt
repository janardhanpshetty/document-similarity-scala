### Topic: What happens to my namespace prefix when I install a package? | Salesforce
What happens to my namespace prefix when I install a package? | Salesforce
What happens to my namespace prefix when I install a package?
A namespace prefix is a globally unique identifier that you can request if you plan to create a managed package. All the components from a managed package that you install from another developer contain the developer's namespace prefix in your organization. However, unmanaged packages can also have a namespace prefix if they originated from an organization that contained a managed package. When you install an unmanaged package that contains a namespace prefix, Salesforce replaces the developer's namespace prefix with yours.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_distribution_installing_what_happens_to_my.htm&language=en_US
Release
202.14
