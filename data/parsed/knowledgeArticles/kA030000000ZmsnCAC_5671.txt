### Topic: This article explains why there are multiple emails received on a daily basis with subject "Chatter Answers Misconfiguration - Disabled or Inactive Zone".
We are getting emails over 200 on a daily basis with the subject as "Chatter Answers Misconfiguration - Disabled or Inactive Zone" . The zone 09axxxxxxxxxxxxxxx is either disabled or not available for Chatter Answers. 
Resolution
If your organization has multiple Force.com Sites, check if there are "Sites" that are referencing the Zone in their "Active Site Home Page".
You might be looking for something like the one below in your Visualforce page.
                            <chatteranswers:feeds communityId="09axxxxxxxxxxxx"/>
Check this article: 
Visualforce Pages for Chatter Answers
Chatter Answers only allow one zone to associate with one site. You will need to disassociate the other site using the or referencing the same zone as this is causing a redirect which in turn is sending out emails. 
Also note, you might continue to get the email despite making the change, this is only because the emails you receive were queued in the server. Once they are sent, you should not be seeing any further emails. 
