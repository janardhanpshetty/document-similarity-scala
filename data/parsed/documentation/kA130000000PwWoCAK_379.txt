### Topic: Salesforce Open CTI Supported Browsers | Salesforce
Salesforce Open CTI Supported Browsers | Salesforce
Salesforce Open CTI Supported Browsers
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
The minimum browser requirements for Open CTI are Microsoft® Internet Explorer® 8; Mozilla® Firefox® 3.6; Apple® Safari® 4; Google Chrome™ 11.0.
See Also:
Salesforce Open CTI Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=cloud_cti_api_browsers.htm&language=en_US
Release
202.9
