### Topic: Email sent to queue members not displaying other queue members email addresses when using email templates.
 If you set a template as a different email template (Standard or Custom Made) and the email is sent to the same members, only the recipient email will show.  This is confirmed to be working as designed within Salesforce.  
Setup > Customize > Cases > Support Settings
Example: Create a queue and add multiple members.  Check the "Send Email to Queue Members" checkbox for the queue.  Go to a Case and assign the case to the queue and click "Send Notification Email" checkbox.  
If Case Assigned Template = --None--, then all users receive email with other recipient's email addresses
If Case Assigned Template = Something other than --None--, all users receive email with their own email address but no other email addresses.
NOTE:
When sending an email alert to members of a queue using the default email template in Case Support Settings, all members of the queue will receive an email with the other member's email addresses included.
Resolution
