### Topic: Some standard objects have checkboxes that are specific to page layouts for that object. To configure how Salesforce displays the checkboxes, click Layout Properties when customizing the page layout.
In cases, you can populate the following checkbox:
- Email notification checkbox (case layout) /Notify contact checkbox (close case layout)
When editing a case,you can populate  the following checkbox:
Case assignment checkbox (case layout)
Those settings are in "Layout Properties" under Setup | App Setup | Customize | Cases | Page Layouts | Edit
Resolution
Some standard objects have checkboxes that are specific to page layouts for that object. To configure how Salesforce displays the checkboxes, click Layout Properties when customizing the page layout.
 
In your case, please follow the steps below:
1. Go to Setup | App Setup | Customize | Cases | Page Layouts and click on Edit located besides "Close Case Layout".
2. Click on "Layout Properties".
3. Activate "Notify contact checkbox". Use the Select by default checkbox associated with a checkbox if you want Salesforce to automatically select the option when a user accesses the edit page.
