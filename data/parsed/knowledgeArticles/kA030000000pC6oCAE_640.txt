### Topic: Security guidelines for sending password resets or usernames to clients.
Security guidelines for sending password resets or usernames to clients.
Resolution
Never send passwords and usernames in the same email. Usernames and Passwords are never to be given over the phone.
If a client does not know their user name, refer them to their account admin for assistance. If the client is unable to get their username from their admin or they are the admin, providing them with their username must be approved by the Account Executive or CSM.
Support may not create users or accounts for clients.
Support may not re-enable users or accounts that have been disabled. For standard users, the account Admin must do this. If the account Admin is disabled,
If the admin has accidentally disabled themselves get authorization from the AE or CSM to proceed with reinstating them.
For security,do not send usernames or passwords to addresses which do not match the address associated with the user. They should be able to contact an administrator at their company for this information. We also cannot copy additional email addresses with this sort of information.
***Note that ExactTarget employees should not edit email addresses for users (reply address on user or notification email address). If a client requests that this be changed, ask them to contact their administrator.***
Permissions on users should never be changed by ET employees, except in the specific example of an admin disabling themselves discussed above.
Agency clients without direct support should NOT be reset or unlocked by support but should be redirected to the partner for support. User Names for Agency clients without direct support should not be given out.
If you are unsure, please contact a supervisor for guidance.
