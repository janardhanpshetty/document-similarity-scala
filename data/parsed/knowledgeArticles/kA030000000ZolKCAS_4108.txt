### Topic: This article explains the possible cause for a standard profile not to show up in the profile list view
The standard system administrator profile is missing from the profile list view
Resolution
This usually happens if:
1) Custom profile was 
cloned
 with the same name as standard profile
2) 
Enhanced
 profile list view is not checked 
To fix this do either of the following
1) Rename the custom profile to any name other than standard profile
2) Turn on Enhanced profile list view
