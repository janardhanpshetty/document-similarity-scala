### Topic: This article was created to clarify what the OWNER LEVEL SHARING perm is in BT, and what the process is to have this permission enabled?
The "owner level sharing" perm in Blacktab was originally designed and intended for one particular customer.  As such, it was not designed with the intention that it would be enabled for all customers.
The "owner level sharing" permission allows customers who have it enabled to create Account sharing rules with a "Full" access level. In terms of C-R-U-D permissions, this means C-R-U, but not D, and Transfer, which ultimately allows users to change the owner of shared records. For the customer is was originally designed for, this allowed them to have a kind of queue based work style for their teams without having to configure Queues.
 
Resolution
As of now, there is no formal pilot for this feature.  The Sharing team in R&D are considering formalizing "Full" as an option on all access grants, but it won't hit the road map soon.  Until then, getting this feature enabled requires an escalation to Setup Tier3, and approval from the Sharing Product Manager.
If you have customer who you think might benefit from having this feature enabled, please escalate a case to Setup Tier3 with the following:
1. The ORG ID in BT where this feature is being requested
2. A detailed business case as to why this feature is being requested (i.e. what is the customer not able to accomplish with the current sharing features that leads them to believe that this feature will be a benefit to their sharing implementation)?
NOTE:
 This feature has not been fully tested, so without a valid business case (#2 above) this feature will not be enabled, and even with a valid business case, approval is at the discretion of the Sharing Product Manager.
