### Topic: Create Records in Chatter Groups | Salesforce
Create Records in Chatter Groups | Salesforce
Create Records in Chatter Groups
Use the group publisher to create records in Chatter groups.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
User Permissions Needed
To create a record in Chatter groups:
“Create” on the object
The actions available to you in the group publisher depend on your permissions, your role in the group, the type of group, and how your administrator has 
configured the group publisher
.
Depending on how your administrator configures the groups publisher layout, you can create account, contact, lead, opportunity, contract, campaign, case, and custom object records. Other objects aren’t supported.
When you create a record in a group, a record creation post displays several places, including in the group feed, on your profile, in the record feed, and in your company’s Chatter feed. Only users with the necessary permissions (via license, profile, permissions, and sharing rules) can view the record and the record creation feed post.
Tip
Filter the group feed to 
Show All Updates
 to view record creation posts.
All comments on the record creation post display in the group feed. This includes comments made on the post in the record feed, and those made by users who don’t belong to the group.
For example, John creates a record in a private group and the record creation post displays in the group feed and in the record feed. Sally isn’t a member of the same private group, but she has access to the record detail page and the record feed. Sally comments on the record creation post in the record feed, and it appears in the private group’s feed.
Record visibility in groups respects user permissions and sharing access rules in your organization. Group members with permissions can view the record and comment on it from any context (in a group, on the owner’s profile, on the record detail page, in the company’s Chatter feed).
Group members without permissions, such as Chatter Free users, can’t see the record or the record feed in the group or anywhere else.
You can’t create records in groups that allow customers.
If your administrator has enabled the ability to 
add records to groups
, as well:
Creating a new record within a group automatically creates a group-to-record relationship, same as when you add an existing record.
The group records list displays the records you create, in addition to the records you add.
You can 
remove records from Chatter groups
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_group_records_creating.htm&language=en_US
Release
202.14
