### Topic: Users are able to approve their own records even if they are not listed as the assigned approver.
Why are users able to approve records even though they are not listed as approvers?
Resolution
If a user has the Modify All on a particular object (Accounts, Leads, Contacts, etc) on their profile, they will be able to approve their own records even when they aren't listed as the assigned approver. This permission needs to be removed if you don't want your users to have the ability to do this.
