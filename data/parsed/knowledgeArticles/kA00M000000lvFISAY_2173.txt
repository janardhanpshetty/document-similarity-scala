### Topic: Beginning with Summer ‘16, the following changes will take place: 1) Users of existing orgs can continue to use IE11 to access Lightning Experience and the Salesforce1 mobile browser app on Windows 8.1 phones until December 16, 2017; and 2) Users of new orgs created after Summer ’16 will not be able to use IE11 to access Lightning Experience and the Salesforce1 mobile browser app. Instead, IE11 users will automatically be redirected to Salesforce Classic.
1. What is the change and when is it planned to happen?
Beginning with Summer ‘16*, the following changes will take place:
Users of existing orgs can continue to use IE11 to access Lightning Experience and the Salesforce1 mobile browser app on Windows 8.1 phones until December 16, 2017.
Users of new orgs created after Summer ’16 will not be able to use IE11 to access Lightning Experience and the Salesforce1 mobile browser app. Instead, IE11 users will automatically be redirected to Salesforce Classic.
* 
Currently targeted for June 2016; date subject to change
NOTE: 
There is no impact to Salesforce Classic. IE11 remains a supported browser for using Salesforce Classic. 
Because IE11 is the only approved browser for using the Salesforce1 mobile browser app on 
Windows 8.1 phones
, support for Salesforce on Windows 8.1 phones effectively ends when IE11 access is disabled. As a courtesy to your Windows phone users, attempts to access the S1 mobile browser app after IE11 is disabled will redirect to Salesforce Classic. But be aware that using Salesforce Classic in any mobile browser (including IE11 on Windows phones) is not officially supported by Salesforce. We recommend that your Windows 8.1 phone users upgrade to Windows 10 and the Microsoft Edge mobile browser, or another supported mobile device-browser combination, before IE11 is disabled for your org. The Salesforce1 mobile browser app will be supported in the Edge browser on Windows 10 phones starting with Summer ‘16.
 
2. What will happen after Summer ‘16?
Existing Orgs
With Summer ‘16, users on existing orgs can continue to use IE11 to access Lightning Experience, and the Salesforce1 mobile browser app on Windows 8.1 phones, up until December 16, 2017. 
Additionally, to help Salesforce admins manage the user transition process, a new Critical Update Console (CRUC) setting, “Disable Access to Lightning Experience from Microsoft IE11”, allows admins to proactively disable IE11 for accessing Lightning Experience and the Salesforce1 mobile browser app before December 16, 2017. 
NOTE:
 All existing orgs who have Lightning Experience enabled or disabled, will be set to allow access to Lightning Experience and the Salesforce mobile browser app using IE11. Salesforce admins can toggle the CRUC setting to disable IE11 access if desired.
 
User Notification
If IE11 access isn’t disabled by the admin via the CRUC setting, users will see a notification when accessing Salesforce with Lightning Experience enabled, which informs them that IE11 won’t be supported in the future. 
 
Salesforce1 Mobile Browser App Support on Windows 8.1 Phones
There are important considerations if you have users with Windows 8.1 phones and your org decides to use the CRUC setting to proactively disable IE11. Because IE11 is the only approved browser for using the Salesforce1 mobile browser app on Windows 8.1 phones, support for Salesforce on Windows 8.1 phones effectively ends when IE11 access is disabled. 
As a courtesy to your Windows phone users, attempts to access the S1 mobile browser app will redirect to Salesforce Classic. But be aware that using Salesforce Classic in any mobile browser (including IE11 on Windows phones) is not officially supported by Salesforce. We recommend that your Windows 8.1 phone users upgrade to Windows 10 and the Microsoft Edge mobile browser, or another supported mobile device-browser combination, before IE11 is disabled for your org. The Salesforce1 mobile browser app will be supported in the Edge browser on Windows 10 phones starting with Summer ‘16. See 
Requirements for the Salesforce1 Mobile App
 for more details.
 
New Orgs
Users on new orgs (that is, orgs created after Summer ’16 is released) won’t be able to access Lightning Experience or the Salesforce1 mobile browser app with IE11. Users in these orgs can only access Lightning Experience using supported browsers (see 
Supported Browsers for Lightning Experience
) and the Salesforce1 mobile browser app on supported device-browser combinations (see 
Requirements for the Salesforce1 Mobile App
).
 
IE11 users will be automatically redirected to the Salesforce Classic experience.
 
3. What alternatives are there for users and orgs currently using IE11?
Lightning Experience supports a number of modern browsers, including Safari 8, Microsoft Edge, and the latest stable versions of Chrome and Firefox. See 
Supported Browsers for Lightning Experience
 for additional details.
Starting with Summer ‘16, the Salesforce1 mobile browser app will be supported in Microsoft Edge on Windows 10 phones. Users with Windows supported 8.1 phones should upgrade. See 
Requirements for the Salesforce1 Mobile App
 for more details.
 
4. What action do customers need to take? What will happen if the action is not taken? 
Prior to the disablement of IE11 support, customers need to ensure that their users are using Lightning Experience-supported browsers and supported mobile device-browser combinations for the Salesforce1 mobile browser app. Customers with existing orgs have until December 16, 2017 to transition their users. Customers with new orgs created after the Summer ’16 release will not be able to use IE11 to access Lightning Experience and the Salesforce1 mobile browser app.
For existing orgs with Lightning Experience enabled, Salesforce admins can leverage the “Disable Access to Lightning Experience from Microsoft IE11” update within the Critical Update Console (within Setup) to manage the user transition process. This update allows admins to proactively disable IE11 access to Lightning Experience and the Salesforce1 mobile browser app in advance of the expiration date on December 16, 2017. See the 
Retirement of Support for Accessing Lightning Experience and the Salesforce1 Mobile Browser App Using Microsoft Internet Explorer version 11
 article for details on how to use the new CRUC setting.
If no action is taken, users will be automatically redirected to the Salesforce Classic experience when IE11 access ends.
 
5. Why are we retiring this product or feature?
With Summer ’16, the Lightning Platform adds a new advanced and flexible security architecture. These improvements depend on a number of security standards that are built into modern browsers. Unfortunately, IE11 does not support these standards.
 
6. How can Salesforce admins identify their users that are impacted? 
While admins can’t identify which users are accessing Lightning Experience, admins can view the login history from Setup to determine which browsers are used by their end users to log in to their Salesforce org. 
 
7. What is the escalation process for dissatisfied customers? 
For customers that have created a new org after Summer ‘16, a support case can be created to request an exception for the support of IE11 with Lightning Experience until December 16, 2017. There will be no extension of the disablement of IE11 support beyond this date.
Resolution
