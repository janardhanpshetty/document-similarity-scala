### Topic: This article explains how to uninstall the Nonprofit Starter Pack 3.0 and references a link to help with uninstalling version 2.0.
Overview
 
Prior to Nonprofit Starter Pack (NPSP) 3.0, you could install and uninstall the NPSP packages separately. Now with Nonprofit Starter Pack (NPSP) 3.0 all packages are required to be uninstalled. This article explains the exact that need to be taken before uninstalling the packages and then the order in which each package needs to be uninstalled. Failure to follow these steps in the correct order will prevent packages from being removed successfully.
If you need help uninstalling the Nonprofit Starter Pack (NPSP) versions 1.0 or 2.0 please click on the link below, as these versions are legacy versions and not support through Technical Support:
https://powerofus.force.com/kA080000000ChtJ?srPos=0&srKp=ka0&lang=en_US
 
Resolution
Uninstall NPSP 3.0
With the Nonprofit Starter Pack (NPSP) 3.0 there are 6 packages that make it up:
If you wish to remove the Nonprofit Starter Pack from your instance of Salesforce, you 
MUST
 uninstall all 6 packages that make it up following the step by step instructions below. Failure to follow these steps int he correct order will prevent packages from being removed successfully.
Note: If you uninstall just the "Nonprofit Starter Pack" package without removing the others your instance may be left in an unstable state, as there is no code and functionality in the Nonprofit Starter Pack package that the other packages rely on.
Uninstalling requires the following steps:
 
Backup Your Data
Remove Profile Access to Nonprofit Starter Pack App
Remove NPSP Page Layouts
Revert Button Overrides to Default
Uninstall NPSP Packages in Correct Order
For more information on the steps above please click on the link below:
https://powerofus.force.com/articles/Resource/NPSP-Uninstall-the-Nonprofit-Starter-Pack
 
