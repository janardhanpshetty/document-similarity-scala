### Topic: Share Files with People in Lightning Experience | Salesforce
Share Files with People in Lightning Experience | Salesforce
Share Files with People in Lightning Experience
Share files with people privately and set 
Viewer
 or 
Collaborator
 access for each person you share the file with.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
Share a file privately so only specific people in your company can see it. 
By default, anyone who can see a file can share it
.
To share a file with one or more users in your company:
Start by sharing the file from one of these locations:
Next to the file on Files home—Click 
Share
.
On the detail page of the file you want to share—Click 
Share
.
From the file preview player—Click 
Type the name of the person you want to share the file with and click the name to select it. Enter additional names if you wish to share with more than one person.
Choose the file permission you want them to have. By default, they have 
collaborator permission
 which lets them view, download, share, change permission, edit the file, and upload new versions. Select 
Viewer
 to give them permission to view, download, and share files.
Optionally, add information to the message that recipients receive.
Click 
Share
 and 
Close
. Recipients 
receive a message that you have shared a file with them, along with a link to the file. Lightning Experience users receive this message as an email notification. Salesforce Classic users get a private Chatter message on their My Messages page on the Chatter tab, and also get an email notification if they have email notifications for Chatter messages enabled.
On the Sharing Settings dialog box, click 
 next to a person's or group's name to stop sharing the file with them.
From the Share File window, click 
Who Can Access
 to see everyone the file has been shared with. Update file permissions or click X next to the name of a person or group to stop sharing the file with them. If you want to prevent others from changing who can access the file, select 
Prevent others from sharing and unsharing
.
Note
Documents from the Documents tab and attachments from the Notes and Attachments related list aren't listed on Files home and can't be shared as Salesforce Files.
Content files may be shared with more people than shown in 
Share with People
 if they are part of a content pack or delivery.
The maximum number of times a file can be shared is 100. This includes files shared with people, groups, and via links. If a file is privately shared and nears the 100 share maximum, consider making the file public by posting it to your feed.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_files_sharing_people_lex.htm&language=en_US
Release
202.14
