### Topic: Salesforce Data Access in Wave Analytics | Salesforce
Salesforce Data Access in Wave Analytics | Salesforce
Salesforce Data Access in Wave Analytics
Wave Analytics requires access to Salesforce data when extracting the data and also when the data is used as part of row-level security. Wave Analytics gains access to Salesforce data based on permissions of two internal Wave Analytics users: Integration User and Security User.
Wave Analytics uses the permissions of the Integration User to extract data from Salesforce objects and fields when a dataflow job runs. Because the Integration User has View All Data access, consider restricting access to particular objects and fields that contain sensitive data. If the dataflow is configured to extract data from an object or field on which the Integration User does not have permission, the dataflow job fails.
When you query a dataset that has row-level security based on the User object, Wave Analytics uses the permissions of the Security User to access the User object and its fields. The Security User must have at least read permission on each User object field included in a predicate. A predicate is a filter condition that defines row-level security for a dataset. By default, the Security User has read permission on all standard fields of the User object. If the predicate is based on a custom field, then grant the Security User read access on the field. If the Security User does not have read access on all User object fields included in a predicate expression, an error appears when you try to query the dataset using that predicate.
Control Access to Salesforce Objects and Fields
Wave Analytics requires access to Salesforce data when extracting the data and also when the data is used as part of row-level security. Configure the permissions of the Integration User on Salesforce objects and fields to control the dataflow’s access to Salesforce data. Configure the permissions of the Security User to enable row-level security based on custom fields of the User object.
See Also:
Control Access to Salesforce Objects and Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_salesforce_object_field_levels.htm&language=en_US
Release
202.14
