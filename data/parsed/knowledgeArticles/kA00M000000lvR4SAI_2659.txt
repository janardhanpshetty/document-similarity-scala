### Topic: Outline of who handles this type of package install issue. Dev handles this type of issue.
If a case is created with "The post install script failed. " as the error message, it is Dev. Meet SLA, brand it as follows, and assign with active assignment rules:
Case Reason
: 
Developer Support
General Application 
Area
: Apex/API Development
Functional Are
a:
 Apex Code Development
Usage does not work package install issues that pertain to issues with the post install script.
Sample error message that indicates the issue is Dev related:
Your request to install package &quot;Package name&quot; was unsuccessful. None of the data or setup information in your 
salesforce.com
 organization was affected. 
If your install continues to fail, contact Salesforce CRM Support through your normal channels and provide the following information. 
Organization: Test  (00DA0000000xxxx) 
User: SFDC User (005A000000xxxxx) 
Package: TestAgain (04t36000000xxxx) 
Problem: 
1. Unexpected Error 
The package installation failed. Please provide the following information to the publisher: 
Organization Name: Test Organization ID: 00DA0000000xxxx
Package: TestAgain
Version: 1.0 
Error Message: The post install script failed. 
Resolution
