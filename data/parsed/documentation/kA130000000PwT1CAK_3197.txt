### Topic: Troubleshooting Actions | Salesforce
Troubleshooting Actions | Salesforce
Troubleshooting Actions
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, 
Database.com
, and 
Developer
 Editions
I don’t see feeds on record detail pages for a certain object.
Feeds appear only for objects for which you’ve enabled feed tracking.
I see the feed on a record detail page, but I don’t see a publisher.
If there are no actions in the Quick Actions in the Salesforce Classic Publisher section on a page layout, the publisher in Salesforce Classic doesn't appear. Add at least one action to the page layout for the publisher to appear.
I can create actions, but I can’t add them to publishers.
Enable actions in the publisher
 to add nonstandard actions to publishers in Salesforce Classic.
I’m using Internet Explorer 10 and all the actions I’ve created appear in the publisher with the same icon, even though the actions are for different types of objects.
Internet Explorer version 10 doesn’t support the techniques Salesforce uses to show icons that correspond to the type of object an action is associated with. Consider using Chrome, Firefox, Safari, or an earlier version of Internet Explorer.
I’ve added an action to a page layout, but a user assigned to the profile that uses that page layout can’t see the action.
Be sure that the user has both Read and Edit permissions on the action’s relationship field. 
The relationship field is the field that’s automatically populated on the target object when a user creates a record using an action. For example, for an action on case that lets users create child cases, the default relationship field is 
Parent Case
. To be sure users can see the Create Child Case action, check that they have both Read and Edit permissions on the 
Parent Case
 field.
I don’t see a relationship field on my global create actions.
Relationship fields apply only to object-specific create actions, not to global actions.
I don’t see some of the actions in my Chatter groups.
Which actions you see depends on your role in the group, the type of group, and how your administrator has configured the publisher layout for groups. Chatter groups without customers display the global publisher layout by default, unless you override it with a customized group publisher layout. 
In Chatter groups that allow customers, the publisher displays standard actions only, such as Post, File, Link, and Poll.
See Also:
Actions Best Practices
Set Up Actions with Chatter Enabled
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=troubleshooting_publisher_actions.htm&language=en_US
Release
202.14
