### Topic: View a Quote’s Line Items Right in an Opportunity | Salesforce
View a Quote’s Line Items Right in an Opportunity | Salesforce
View a Quote’s Line Items Right in an Opportunity
Sync a quote with its related opportunity to show the quote’s updated line items directly in the opportunity. When you sync, any changes or additions to a quote’s line items are updated in the opportunity’s Products related list, and vice versa. Line item page layouts are customizable.
Available in: 
Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To customize page layouts:
“Customize Application”
To view page layouts (enhanced page layout editor only):
“View Setup”
Ask your administrator to customize the quote line item edit and detail pages you see when you select a line item on a quote.
See Also:
Find Object Management Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=quotes_line_rel_list.htm&language=en_US
Release
198.17
