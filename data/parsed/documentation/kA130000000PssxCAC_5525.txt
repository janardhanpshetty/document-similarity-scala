### Topic: Assigning Leads to Partner Users | Salesforce
Assigning Leads to Partner Users | Salesforce
Assigning Leads to Partner Users
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To assign leads to partners:
“Edit” on leads
Note
Starting in Summer ’13, the partner portal is no longer available for organizations that aren’t currently using it. Existing organizations continue to have full access. If you don’t have a partner portal, but want to easily share records and information with your partners, try Communities.
Existing organizations using partner portals may continue to use their partner portals or transition to Communities. Contact your Salesforce Account Executive for more information.
A vital piece of partner relationship management is lead assignment. Your partners can only work with leads that are assigned to them. There are two ways to handle lead assignment for partner users:
Assign each lead to an individual partner user
Assign leads to a lead queue and allow partner users to claim leads from the queue
In addition, you can create lead assignment rules that automatically assign leads to partner users or queues based on certain properties of those leads.
Note
Assignment rules cannot be triggered by actions in the portal.
Assigning a lead to a partner user or partner lead queue is just like assigning a lead to any other user or queue.
Leads assigned directly to a user or a queue that is not part of the lead pool are shown on the portal Leads tab. The partner user can see these by selecting an appropriate list view. Until a partner user has reviewed a lead, it displays in bold, and is in the My Unread Leads list view.
If a lead is assigned to a queue that is in the lead pool, the lead displays in the 
Available Leads list
 on the Home tab of the portal.
Tip
Assign partner leads to a single partner user (for example, a sales manager) in the partner account to enable that person to manage lead assignment for the partner users in that account.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=partner_portal_assign_leads_to_partners.htm&language=en_US
Release
202.14
