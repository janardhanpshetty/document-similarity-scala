### Topic: The article describes on how help Text works in VisualForce page using apex code.
How Help Text works in VisualForce page using apex code?
Resolution
Help Text works only when show header is set to true in the parent page.
Here is a snippet for sample code:
<apex:page showHeader="true" sidebar="true" tabStyle="Testing__c" standardController="Testing__c"> 
<apex:form > 
 
<apex:detail relatedList="false" title="false"/> 
 
</apex:form> 
</apex:page> 
