### Topic: Managing Validation Rules | Salesforce
Managing Validation Rules | Salesforce
Managing Validation Rules
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
Validation rules verify that the data a user enters in a record meets the standards you specify before the user can save the record. A validation rule can contain a formula or expression that evaluates the data in one or more fields and returns a value of “True” or “False”. Validation rules also include an error message to display to the user when the rule returns a value of “True” due to an invalid value.
From the validation rules page you can:
Define a validation rule
.
Click 
Edit
 next to a rule name to update the 
rule fields
.
Delete a validation rule.
Click a validation rule name to view more details or to 
clone the rule
.
Activate a validation rule
.
See Also:
Validation Rules
Examples of Validation Rules
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_managing_field_validation.htm&language=en_US
Release
202.14
