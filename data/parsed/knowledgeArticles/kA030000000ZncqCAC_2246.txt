### Topic: This article will guide agents through the login troubleshooting process, including how to route various types of cases.
All login cases should begin with the Feature Activations Team. Use the following guide to troubleshoot login issues and determine how to resolve or where to route each type of issue. 
Resolution
Before you begin:
 Make sure you are working with a System Administrator. We should not be resetting user passwords without first ensuring that the admin has already tried to resolve the issue. 
The first step is to gather the required information:
1. Username (note the BT link to the User ID)
2. Organization ID
3. Description of the issue 
The next steps will depend on the description of the issue: 
Tip: 
Go through the steps methodically to determine the root cause of the issue. Notice that resetting the password is not suggested until step 13. Please review all prior steps before resetting the password. 
1. Check 
Organization status
 to make sure it is available for use
2. Check the user's profile - 
If non-System Administrator, 
direct to their internal System Administrator
 unless System Administrator is requesting assistance for the end user
3. Is the user frozen? 
There is a log entry of someone freezing the user in the Setup Audit Trail (
Support does not unfreeze users on request
) - 
Send to System Administrator
Please have a look at the steps for 
resetting a password on a frozen user
No log of freezing in the Setup Audit Trail - 
Complete all other steps below before 
Sending to Security
4. Look at the username. Does it have:
A random 7 character string prefix? (applies to sandbox only) - 
Send to Usage
A space at the beginning or end of the username? - Escalate to FA SFDC Tier 2
5. User is a portal or community user in customer org - 
Send to Config
6. SFDC Partner Community user  - 
send to Alliances Technical Tier 1
 (NOTE: This only applies to logging into our Partner Community(not an org). Issues with partner users in customer orgs stay in support)
7. Username ends in "guest.salesforce.com" - 
Escalate to IT Apps Support
 
8. User can log in, but then sees a blank screen, insufficient privileges, etc it is often API Only User setting on the profile - 
Send to Config
9. Issue exists only in Salesforce1, browser login works fine - 
Send to Mobile
10. User is logging into a specific tool - route to the skill group that assists with that tool (some examples are listed below)
Data Loader - 
Send to Usage
Workbench - 
Send to Usage
Site.com - 
Send to Dev
Salesforce for Outlook - 
Send to EDI
11. No error on login.salesforce.com - browser troubleshooting (browser reloads, doesn't react, password needs reset often) 
​
Password reset every time
PC Configuration
Mac Configuration
Supported Browsers
12. Error on login "Your login Attempt has failed. The username or password may be incorrect, or your location or login time may be restricted. Please contact the administrator at your company for help."
Check user profile - If non-System Administrator - 
Direct to System Administrator
System Administrator 
with 
SSO enabled - 
Send to Dev
System Administrator 
without 
SSO enabled - check login history
13. Invalid Password (should have already verified they are a System Administrator)
​Have customer try password again (to ensure that it was not just a typo)
Check login history - make sure a login attempt is recorded. If not, skip to step #15. 
If there is an attempt recorded as incorrect password, 
reset password in BT
 only to the email address currently on file (See step #16 for other unsuccessful login reasons)
If they received the password reset email - try to login 
If they didn't receive the password reset email, confirm that Allow to Send Email is set to 1 or 2 (if this is set to 0 it will prevent password reset emails from sending)
If they still do not receive password reset email -
 
Send to EDI
14. User doesn't know the answer to the security question
Check User profile - If non-System Administrator - 
Direct to System Administrator
If System Administrator - 
Reset password in BT
15. No login history recorded
Are they entering the correct username?
Does the username provided match what's on file? - 
Reset password
 and they will get the correct username in the email. 
Do not give out username verbally or in writing, but you can confirm the username that they provide to you is correct.
If using Internet Explorer as browser, please make sure 
the configuration is performed correctly
Replication issue - 
Send to Usage
If still no login history
Sandbox - Send to Usage
Production - Make sure there are no login restrictions that would prevent this, then 
reproduce the issue via GTM
Since there could be an issue with the User record you can also request the System Admin to try deactivate and Reactivate the user or deactivate the user and create a new record making sure the user record is created manually and not via API
16. Review login history for reasons that login would fail
Failed: Computer activation required - How is user receiving verification?
SMS - no text received or incorrect mobile number - 
Send to Security
SMS - text received, but no verification code - 
Send to Security
Email - verify that the email address on the user record is correct
If correct, confirm that Allow to Send Email is set to 1 or 2 (if this is set to 0 it will prevent password reset emails from sending) 
If they still do not receive the activation email - 
Send to Security
If incorrect - 
Sandbox - deboof - 
Escalate to FA SFDC Tier 2
Production with more than one System Administrator - 
Direct to another System administrator
Production with no other System Administrator - 
System Administrator change
2-Factor Authentication required - 
Send to Security
Login Rate exceeded - 
Escalate to FA SFDC Tier 2
Password Lockout - 
unlock and reset the password
 (System Administrator only, otherwise 
direct to System Administrator
)
Restricted Domain -
 
provide custom URL verbally
Restricted IP Range - 
Send to Security
Restricted Hours - 
Escalate to FA SFDC Tier 2
 if System Administrator has been locked out completely
If you are unable to determine the root cause or resolve the login issue based on the steps provided above, escalate per standard process. 
