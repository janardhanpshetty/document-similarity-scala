### Topic: Service Organization Control reports for the Salesforce Serivces
Salesforce's information security control environment applicable to the Salesforce Services undergoes an independent evaluation in the form of SOC 1 (SSAE 16), SOC 2, or 
SOC 3
 reports. Salesforce's most recent SSAE16 SOC 1 Report, SOC 2 Report and SSAE16 SOC 1 Bridge (Gap) Letter are available upon request.
 
Resolution
The documents described in this article contain confidential information related to Salesforce and it's affiliates. By accessing or viewing any files described in this article you agree that the documents contain confidential information related to Salesforce and are subject to the help.salesforce.com
 
Confidentiality Notice and Confidentiality Terms
. Please review the help.salesforce.com 
Confidentiality Notice and Confidentiality Terms
 before requesting or accessing these files.
Customers can request a copy of Salesforce's SSAE16 SOC 1 Report, SOC 2 Report and SSAE16 SOC1 Bridge (Gap) Letter via their Salesforce Account Team point of contact.
