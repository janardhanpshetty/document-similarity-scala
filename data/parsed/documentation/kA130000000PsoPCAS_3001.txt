### Topic: Viewing Parent Accounts | Salesforce
Viewing Parent Accounts | Salesforce
Viewing Parent Accounts
Available in: Salesforce Classic
Available in: 
All
 Editions except 
Database.com
User Permissions Needed
To view accounts:
“Read” on accounts
To view parent accounts:
“Read” on accounts
The account hierarchy shows you the accounts that are associated through the 
Parent Account
 field, giving you a global view of a company and its subsidiaries. In the hierarchy, accounts are indented to show that they are subsidiaries of the parent account above them.
To view the account hierarchy, click 
View Hierarchy
 next to the account name on the account detail page. The Account Hierarchy page displays up to 500 child accounts. If you don’t have access to certain accounts that appear on the Account Hierarchy page, the columns for those accounts won’t display details.
To list an account as a subsidiary, edit the subsidiary account and type the name of an existing account in the 
Parent Account
 field. Alternatively, you can click the lookup icon to search for (or optionally, create) a parent account.
The parent account must be an existing account before it can be entered and saved in this field.
For companies with multiple office locations, you can also use the 
Account Site
 field to distinguish among the locations.
If your organization uses divisions, accounts that are associated via the 
Parent Account
 field do not need to be in the same division.
The 
Parent Account
 field and the 
View Hierarchy
 link are not supported for person accounts.
See Also:
Account Fields
Guidelines for Creating Accounts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=account_parent.htm&language=en_US
Release
202.14
