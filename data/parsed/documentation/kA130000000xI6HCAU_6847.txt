### Topic: Set Up a Sales Path in Lightning Experience: Walkthrough | Salesforce
Set Up a Sales Path in Lightning Experience: Walkthrough | Salesforce
Set Up a Sales Path in Lightning Experience: Walkthrough
Give your sales teams the guidance they need to close their deals with fewer delays—from anywhere. When you set up sales paths, you determine which fields are key for your sales reps to complete. You'll also provide tips, potential gotchas, and even words of encouragement to keep sales reps eager to close their deals faster.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To enable Sales Path:
“Customize Application”
To set up a sales path:
“View Setup” and “Modify All Data”
Walk through it: Set Up a Sales Path in Lightning Experience
 
See Also:
Get More Done Faster with Walkthroughs
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=lightning-sales-path.htm&language=en_US
Release
202.14
