### Topic: What’s the Contact Clean Info Object? | Salesforce
What’s the Contact Clean Info Object? | Salesforce
What’s the Contact Clean Info Object?
Contact Clean Info is a background object that stores the metadata used to determine a contact record’s clean status.
Available in: 
Salesforce Classic
Available with a 
Data.com Prospector
 license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available with a 
Data.com Clean
 license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Contact Clean Info provides a snapshot of the data in your Salesforce contact record and its matched 
Data.com
 record at the time the 
Salesforce
 record was cleaned.
Contact Clean Info includes a number of bit vector fields, whose component fields each correspond to individual object fields and provide related data or status information about those fields. For example, the bit vector field 
IsDifferent
 has an 
IsDifferentEmail
 field. If the 
IsDifferentEmail
 field’s value is 
False
, that means the 
Email
 field value is 
the same
 on the Salesforce contact record and its matched Data.com record.
These are the bit vector fields.
CleanedBy
 indicates who (a user) or what (a Clean job) cleaned the contact record.
IsDifferent
 indicates whether or not a field on the contact record has a value that differs from the corresponding field on the matched 
Data.com
 record.
IsFlaggedWrong
 indicates whether or not a field on the contact record has a value that is flagged as wrong to 
Data.com
.
IsReviewed
 indicates whether or not a field on the contact record is in a 
Reviewed
 state, which means that the value was reviewed but not accepted.
Developers can create triggers that read the Contact Clean Info fields to help automate the cleaning or related processing of contact records.
See Also:
Object Reference for Salesforce and Force.com
: 
ContactCleanInfo
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_understanding_contact_clean_object.htm&language=en_US
Release
200.20
