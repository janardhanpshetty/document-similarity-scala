### Topic: Salesforce enforces decimal point precision specified in field definitions only at the user interface (UI) level, not at the database or API level.
When you create a custom currency field and set the number of decimal places, Salesforce will round the input value, when you enter that value in the UI. 
But, if you insert or update a value using the API, Salesforce will not round the value. 
For example, for a currency field that has 2 decimal places
In the UI, if you enter $1.234, the value will be rounded to $1.23 
Using the API, if you enter $1.234, it will be saved to the database as $1.234
This behavior helps us maintain backwards compatibility for our systems, and is working as designed. 
Resolution
Some workarounds you can use to ensure that this behavior does not cause issues in your Org are
Round the currency value before using web callouts to APIs that expect 2 decimal places. 
Add display logic in the UI to round values to 2 decimal places. 
Write an "after insert/update" trigger to round the value to 2 decimal places. 
Create a formula field that takes the value of the currency field and rounds it to 2 decimal places. 
