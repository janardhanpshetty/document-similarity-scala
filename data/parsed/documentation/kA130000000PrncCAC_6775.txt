### Topic: Public Access Settings for Force.com Sites | Salesforce
Public Access Settings for Force.com Sites | Salesforce
Public Access Settings for Force.com Sites
Available in: Salesforce Classic
Available in: 
Developer
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To create and edit Force.com sites:
“Customize Application”
To edit public access settings for Force.com sites:
“Manage Users”
Public access settings control what public users can do on each Force.com site. To set the public access settings for your site:
From Setup, enter 
Sites
 in the 
Quick Find
 box, then select 
Sites
.
Click the name of the site you want to control.
Click 
Public Access Settings
 to open the Profile page for your site profile.
This page includes all the functionality for viewing and editing profile permissions and settings, but you can't clone or delete the profile.
In the site profile, you can:
Set the object permissions for your site. 
You can grant “Read” and “Create” permissions on all standard objects except products, price books, and ideas; and “Read,” “Create,” “Edit,” and “Delete” on all custom objects.
 All permissions that aren't set by default must be set manually.
Warning
We recommend setting the sharing to private for the objects on which you grant “Read” access for your site. This ensures that users accessing your site can view and edit only the data related to your site.
We also recommend securing the visibility of all list views. Set the visibility of your list views to 
Visible to certain groups of users
, and specify the groups to share to. List views whose visibility is set to 
Visible to all users
 may be visible to public users of your site. To share a list view with public users, create a new public group for those users and give them visibility. If the object's sharing is set to private, public users won't be able to see those records, regardless of list view visibility.
Control the visibility of custom apps. If you want to expose a custom app and its associated tabs to public users, make only that app visible and make it the default to avoid exposing other pages. If any of your site pages use standard Salesforce headers, other visible applications may be seen by public users.
Set the login hours during which users can access the site.
Restrict the IP address ranges from which you can access the site. Force.com sites ignore company-wide IP range restrictions in order to provide public access; however, you can restrict the IP range here.
Note
To set restrictions based on IP or login hours, HTTPS is required. You must use the secure URL associated with your Force.com domain to access your site.
To enforce HTTPS on all Force.com sites pages and allow all IP addresses to access your site, create the following IP ranges: 
0.0.0.0
 to 
255.255.255.255
, 
::
 to 
::fffe:ffff:ffff
, and 
::1:0:0:0
 to 
ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff
. However, as this may degrade the performance of your site, don't enforce HTTPS unless it is absolutely required. Changing from HTTP to HTTPS doesn't affect logged in users until the next time they log in.
The IP addresses in a range must be either IPv4 or IPv6. In ranges, IPv4 addresses exist in the IPv4-mapped IPv6 address space 
::ffff:0:0
 to 
::ffff:ffff:ffff
, where 
::ffff:0:0
 is 
0.0.0.0
 and 
::ffff:ffff:ffff
 is 
255.255.255.255.
 A range can’t include IP addresses both inside and outside of the IPv4-mapped IPv6 address space. Ranges like 
255.255.255.255
 to 
::1:0:0:0
 or 
::
 to 
::1:0:0:0
 aren’t allowed.
Enable Apex controllers and methods for your site. Controllers and methods that are already associated with your site's Visualforce pages are enabled by default.
Enable Visualforce pages for your site. Changes made here are reflected on the Site Visualforce Pages related list on the Site Details page, and vice versa.
See Also:
Configuring Force.com Sites
Force.com Sites-Related Apex Methods and Expressions
Create a List View in Salesforce Classic
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sites_public_access_settings.htm&language=en_US
Release
202.14
