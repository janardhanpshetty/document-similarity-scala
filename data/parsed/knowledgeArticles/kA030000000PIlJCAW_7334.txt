### Topic: This article describes how to make the callouts from trigger.
In certain scenarios, we need to make the callout from the trigger to call an external webservice however we are not able to do so as it gives the below mentioned error:
Callout from triggers are currently not supported.
 
Resolution
There is a work around to overcome this problem. You can invoke callouts from triggers by encapsulating the callouts in @future methods.
Annotations are defined with an initial @ symbol, followed by the appropriate keyword. To add an annotation to a method, specify it immediately before the method or class definition. For example:
global class MyClass {
     @future
     Public static void myMethod(String a)
     {
          //long-running Apex code
     }
}
You can get the more information regarding the same on the link:
http://www.salesforce.com/us/developer/docs/apexcode/index_Left.htm#StartTopic=Content/apex_classes_annotation.htm?SearchType=Stem
However, this scenario will work if your required operation can be performed in an asynchronous call as the @future method works asynchronously.
Also, there is a Governor Limit associated with the @future methods:
Total number of SOQL queries issued for future methods : 200
Total heap size for future methods : 12 MB
Total number of methods with the future annotation allowed per Apex invocation : 50
Salesforce also imposes a limit on the number of future method invocations: 200 method calls per full Salesforce user license or Force.com App Subscription user license, per 24 hours. This is an organization-wide limit. Chatter Only,Chatter customer users, Customer Portal User, and partner portal User licenses aren’t included in this limit calculation. For example, suppose your organization has three full Salesforce licenses, two Force.com App Subscription licenses, and 100 Customer Portal User licenses. Your entire organization is limited to only 1,000 method calls every 24 hours, calculated as 200 x (3+2), not 200 x (3+2+100).
For more information on limits, please refer the below link:
http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_gov_limits.htm
 
