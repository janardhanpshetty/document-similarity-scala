### Topic: Overwriting a tab with a visualforce page can lead to slow load times of that tab.
There are times when overwriting a tab with a visualforce (VF) page might be necessary, for example, when you'd like to take away the 'new' button from the tab page without taking away 'create' permissions for the tab's object. Even the most simple VF page can cause slow load times for this tab if the table is large enough. Take the following VF code for example:
 
<apex:page showHeader="true" tabstyle="Contact">

<apex:ListViews type="Contact" />

</apex:page>
This snippet would return all Contact results, since there is no controller with a filter or any logic. Even with only three lines of code, this page has potential to perform very slowly.
Resolution
A way to filter your results without adding a controller or extension could be through use of 'apex:enhancedList', which can take a parameter of 'listid' which would be a specific view to load upon page load.
The way to retrieve a listView ID would be: 
1. Access the tab we are overwriting
2. Select from the dropdown whichever default list view you'd like, or create a new one.
3. Edit the view, and on the edit page, look to the URL bar where an ID will be in the URL, and pull that ID.
How we'll use this ID in our visualforce page:
 
<apex:page showHeader="true" tabstyle="Contact">

<apex:enhancedList type="Contact" height="300" listid="00Bj0000002oFT0" />
     
</apex:page>
You'll see on the middle line that we specified a listID which is the ID pulled from the URL. 'height' is also a required attribute, so must be included when using apex:enhancedList.
The downside of using 
apex:enhancedList is that it doesn't allow for selecting a new list view when on the page; you only get the one you specified in the code.
If apex:enhancedList proves too restrictive, there is a script workaround you could try:
 
<apex:page showHeader="true" tabstyle="Contact">

<script src="/soap/ajax/15.0/connection.js"></script>
<script type="text/javascript" />
<script>
     window.onload = function() {

     sforce.connection.sessionId = '{!$Api.Session_ID}';

     var describeSObjectResult = sforce.connection.describeSObject("contact");
     var prefix = describeSObjectResult.keyPrefix;

parent.document.location.href = "/" + prefix + "?fcf=" + "00Bj0000002oFT0" ;
}
</script>

</apex:page>
The only piece of the script that should be edited to fit a specific org would be the ID at the end of the "parent.document.location.href". 
Using the script allows for change of the list view after page load.
These two options accommodate the avoidance of involving any associated apex classes, but for more complex filtering and logic, an apex class would be the way to go.
 
