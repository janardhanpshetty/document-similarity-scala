### Topic: Access Data in Another Salesforce Org with the Cross-Org Adapter for Salesforce Connect | Salesforce
Access Data in Another Salesforce Org with the Cross-Org Adapter for Salesforce Connect | Salesforce
Access Data in Another Salesforce Org with the Cross-Org Adapter for Salesforce Connect
Provide users with a seamless view of data in your other Salesforce orgs so that they have a complete view of the business. Setting up the cross-org adapter for Salesforce Connect is quick and easy with point-and-click tools.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Developer
 Edition
Available for an extra cost in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To create and edit external data sources:
“Customize Application”
To create and edit external objects:
“Customize Application”
To define or change object-level help:
“Customize Application”
To create and edit custom fields:
“Customize Application”
To edit permission sets and user profiles:
“Manage Profiles and Permission Sets”
To edit another user’s authentication settings for external systems:
“Manage Users”
Setting up Salesforce Connect with the cross-org adapter involves these high-level steps.
Define an external data source of type 
Salesforce Connect: Cross-Org
. 
Create an external data source for each provider org.
Create the external objects.
Perform this task only if you don’t sync to automatically create the external objects. In the subscriber org, create an external object for each object in the provider org that you want to access.
Create help content for the external objects.
Help your users distinguish between external objects and the other objects in the subscriber org, which can have similar names and types of data. On the subscriber org, create Visualforce pages to describe the external objects. When your users click 
Help for this Page
 on an external object, they read your custom help content.
Add custom fields and relationships to the external objects.
Create relationships between objects. If you didn’t sync to automatically create the external objects and their fields on the subscriber org, create a custom field for each of the provider org’s fields that you want to access.
Enable user access to external objects.
Grant object permissions through permission sets or profiles.
Enable user access to the fields on the external objects.
Grant field permissions through permission sets or profiles.
If the external data source uses per-user authentication:
Let users authenticate to the external system.
Grant users access to authentication settings for the external data source through permission sets or profiles.
Set up each user’s authentication settings.
You or your users can perform this task.
Tip
Train your users on how to set up their authentication settings for external systems. Make sure that they know which credentials to enter for the provider org. If you’re using OAuth 2.0, the OAuth flow displays the Salesforce login page twice: first to log in to the provider org to obtain an access token, and then to log back in to the subscriber org. Test the OAuth flow for potentially confusing prompts or redirects, and train your users as needed. OAuth flows vary, depending on your external system, authentication provider, and specified scopes.
See Also:
Cross-Org Adapter for Salesforce Connect
Considerations for Salesforce Connect—Cross-Org Adapter
Developer Guide: 
Visualforce Developer Guide
External Object Relationships
Subscriber and Provider Orgs in Salesforce Connect—Cross-Org Adapter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=xorg_setup.htm&language=en_US
Release
202.14
