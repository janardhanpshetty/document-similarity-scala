### Topic: Validation Status
You may find that the standard Opportunity report type may not be available to your end users. This is due to a limitation to the access of the User object.
Restricting access to the User table can impact the accessibility of report types and existing reports that use those report types. See the impacts at the URLs below:
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_sharing_users_concept.htm&language=en
https://help.salesforce.com/apex/HTViewHelpDoc?id=security_sharing_owd_external_user_srt.htm&language=en_US
Resolution
To remedy this issue, one of the 2 items below must be performed by an Administrator:
The profile(s) of the impacted user(s) should have the permission "View All Users" enabled.
Update the organization wide default sharing settings for the User object to Private to Public Read.
 
