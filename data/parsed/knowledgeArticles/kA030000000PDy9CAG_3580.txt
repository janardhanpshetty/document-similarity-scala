### Topic: Formula to find if time is AM or PM
Formula to find time in AM or PM
 
Resolution
Assumptions - Time is based off "CreatedDate" field which is a date/time field
1) IF( (FLOOR ( MOD(  CreatedDate  - ($System.OriginDateTime),1) *24))<=11, "AM", 
"PM")
 
Note: The result is in GMT.  This still will NOT account for Daylight Savings.
2) If you need to calculate the time in PST, you will need to add 8 hours to "$System.OriginDateTime" as follows:-
 
IF( (FLOOR ( MOD(  CreatedDate  - ($System.OriginDateTime+8/24),1) *24))<=11, "AM", 
"PM")
 
 
3) If the time to be calculated is in PST and daylight savings need to be considered, you will need to add 7 hours (since time goes behind by 1 hour during daylight savings w.r.t. PST) to "$System.OriginDateTime" as follows:-
 
 IF( (FLOOR ( MOD(  CreatedDate  - ($System.OriginDateTime+7/24),1) *24))<=11, "AM", 
"PM")
 
 
