### Topic: This article goes over using Process Builder, Auto Launched Flow and Custom object to achieve this requirement
How can Live Agent Blocking rule be unblocked after specific number of days?
Resolution
The solution involves using custom object, Process builder and flow in conjunction. Follow these steps to achieve the requirement
Create IP Unblock Object
 
Create an object in your org and let’s name it ‘IP Unblock’
Create a field in it called Block and default value unchecked
 
Creating the auto launched flow
 
Create a flow with the element "Record Update"
Give a name as "StartUnblocking"
In the update drop down choose "LiveChatBlockingRule"
In the field choose Developer name equals "Give the blocking rule developer name here". You can find it easily by going to LiveChatBlockingRules
In the update records section
Choose From IPAddress as 0.0.0.0
To IPAddress as 0.0.0.1
Its an auto launched flow so just save it and don't run it as it will immediately unblock
Set it as starting element
Remember to activate the flow
 
Creating Process in Process builder
 
Choose the object IP Unblock created in Step 1 and when a record is created
Click Advanced "Choose recursion" as YES
Criteria set condition are met
Choose the field Block equals TRUE
Choose Scheduled action "30 days from now" (just an example)
Add Action as Create a record
Give it Action name of your choice
Choose Record >> IP Unblock
Give the record name some value or if you have made it auto number no need
Block Boolean FALSE
In the false branching Add criteria
Block equals FALSE
Then in immediate action Choose Launch a Flow and select the flow "StartUnblocking" that you created in step 2
Activate the process
 
Setting the process in action
 
Now create a record in IP unblock object with Block field as Checked. This is the only manual step needed.
This new record will trigger process builder and then as scheduled action (30 days from now) another new record will be created with ‘Block’ Field Unchecked
This 2nd new record will launch your Unblock flow and thereby the IP ranges will be set from 0.0.0.0 to 0.0.0.1 in 30 days after you created the first record.
