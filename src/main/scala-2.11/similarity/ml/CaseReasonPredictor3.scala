package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.RandomForestClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
 * Created by jshetty on 9/15/16.
 * spark-submit --class similarity.ml.CaseReasonPredictor3 --master local[12] --driver-memory 8G /Users/jshetty/spark-applications/documentsimilarity/target/scala-2.11/documentsimilarity_2.11-1.0.jar
 * word2vec instead of hashingTF
 */
object CaseReasonPredictor3 {

  def main(args: Array[String]): Unit = {

    val getConcatenated = udf((first: String, second: String) => { first + " " + second })

    val spark = SparkSession.builder.master("local[12]").appName("Prediction of Case Reason").getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext
    sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val inputPath = "/Users/jshetty/casedata/data/reason_subject_description/forspark.csv"
    val inputP = spark.read.option("header","false").option("delimiter", "`").csv(inputPath).cache()
    val input = inputP.withColumnRenamed("_c0", "REASON").withColumnRenamed("_c1", "text")

    val stopWordsPath = "/Users/jshetty/spark-applications/documentsimilarity/input/stopwords/english_custom_stopwords.txt"
    val stopwds = sc.textFile(stopWordsPath).collect()

    val num_features = 270    // Word vector dimensionality
    val minCount = 3 //the minimum number of times a token must appear to be included in the word2vec model's vocabulary
    val nGrams = 3
    val numTrees = 35
    val maxDepth = 10

    // Indexer to convert String labels to Double
    val labelIndexer = new StringIndexer().setInputCol("REASON").setOutputCol("label").fit(input)
    val tokenizer = new RegexTokenizer().setInputCol("text").setOutputCol("words").setPattern("\\W")
    val textFiltered = new StopWordsRemover().setStopWords(stopwds).setInputCol("words").setOutputCol("wordsFiltered")
    val grams = new NGram().setInputCol("wordsFiltered").setOutputCol("wordsGrams").setN(nGrams)
    val identity = new IdentityTransformer().setInputCol("wordsGrams").setOutputCol("wordsGramsId")
    // Learn a mapping from words to Vectors.
    val word2Vec = new Word2Vec().setInputCol("wordsGramsId").setOutputCol("features").setVectorSize(num_features).setMinCount(minCount)

    // Apply RF
    val rf = new RandomForestClassifier().setLabelCol("label").setFeaturesCol("features").setNumTrees(numTrees).setMaxDepth(maxDepth)

    val labelConverter = new IndexToString().setInputCol("prediction").setOutputCol("predictedLabel").setLabels(labelIndexer.labels)
    val pipeline = new Pipeline().setStages(Array(labelIndexer, tokenizer, textFiltered, grams, identity, word2Vec, rf, labelConverter))

    val Array(trainingData, validationData, testData) = input.randomSplit(Array(0.5, 0.2, 0.3))

    // Train model. This also runs the indexers.
    val model = pipeline.fit(trainingData)

    // Make predictions
    val trainPredictions = model.transform(trainingData)
    val validationPredictions = model.transform(validationData)
    val testPredictions = model.transform(testData)

    // Evaluation metric: accuracy
    val evaluator = new MulticlassClassificationEvaluator().setLabelCol("label").setPredictionCol("prediction").setMetricName("accuracy")

    val accuracyTrain = evaluator.evaluate(trainPredictions)
    println("Accuracy on train data = " + accuracyTrain)
    val accuracyValidation = evaluator.evaluate(validationPredictions)
    println("Accuracy on validation data = " + accuracyValidation)
    val accuracyTest = evaluator.evaluate(testPredictions)
    println("Accuracy on test data = " + accuracyTest)

    // Evaluation metric: weighted f1 measure
    val evaluatorF1 = new MulticlassClassificationEvaluator().setLabelCol("label").setPredictionCol("prediction").setMetricName("f1")

    val accuracyTrainF1 = evaluatorF1.evaluate(trainPredictions)
    println("F1 weighted on train data = " + accuracyTrainF1)
    val accuracyValidationF1 = evaluatorF1.evaluate(validationPredictions)
    println("F1 weighted on validation data = " + accuracyValidationF1)
    val accuracyTestF1 = evaluatorF1.evaluate(testPredictions)
    println("F1 weighted on test data = " + accuracyTestF1)

    spark.stop()
  }
}
