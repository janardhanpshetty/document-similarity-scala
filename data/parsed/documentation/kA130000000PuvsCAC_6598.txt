### Topic: Tip Sheets | Salesforce
Tip Sheets | Salesforce
Tip Sheets
In addition to online help, Salesforce publishes printable documentation to help you learn about our features and successfully administer Salesforce.
Salesforce Communities
Getting Started With Salesforce Communities
Communities Managers Guide
Migrating From Portals to Communities
Salesforce CRM Content
Salesforce CRM Content Implementation Guide
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collaboration_admin_tipsheets.htm&language=en_US
Release
202.14
