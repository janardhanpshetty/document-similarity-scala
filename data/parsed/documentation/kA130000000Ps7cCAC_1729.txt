### Topic: Notes on Search Layouts | Salesforce
Notes on Search Layouts | Salesforce
Notes on Search Layouts
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
User Permissions Needed
To change search layouts:
“Customize Application”
Search layouts don’t apply to Salesforce CRM Content.
Search layouts don’t apply to campaign members, opportunity teams, or account teams.
The search layout doesn’t control which fields are searched for keyword matches. The list of fields searched is the same across Salesforce.
You can add up to 10 fields to each search layout.
You can’t remove unique identifying fields, such as 
Account Name
 or 
Case Number
, from the search layouts. These fields must be listed first in the order of fields in the search layout.
You can ‘t add long text fields such as 
Description
, 
Solution Details
, or custom long text area fields to search layouts.
All fields are available to be added to the search layout even if some fields are normally hidden for the user customizing the search layout.
For Professional, Enterprise, Unlimited, Performance, and Developer Edition organizations, 
search layouts don’t override field-level security. If a field is included in the search layout but hidden for some users via field-level security, those users do not see that field in their search results.
For Personal, Contact Manager, Group, and Professional Edition organizations, search layouts override page layout settings. If a field is included in the search layout but hidden in the page layout, that field will be visible in search results.
The search results layouts for leads, accounts, contacts, and opportunities also apply to the search results displayed when finding duplicate leads.
Formula fields are not available in search result layouts.
Don’t remove the 
Phone
 field from any lookup phone dialogs search layout. If you do, users can’t use the directory search results to enter a phone number into a SoftPhone dial pad.
To add a custom button to a list view or search layout, create the custom button for a standard or custom object, giving it the 
List Button
 
Display Type
. The custom button will be available in the list view and search result layouts for that object.
Tip
In account search results, you can visually differentiate business accounts from person accounts by adding the 
Is Person Account
 field, which displays as the person account icon (
).
You may specify the 
Is Person Account
 field as the first column in account search layouts. Otherwise, 
Account Name
 must be the first column.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_searchlayout_notes.htm&language=en_US
Release
202.14
