### Topic: Filter Your Files List | Salesforce
Filter Your Files List | Salesforce
Filter Your Files List
The filters on the 
Files
 page provide quick access to your files in Salesforce.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Enterprise
, 
Professional
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
The Files page lists all the files you have access to. Filters show you different groups of those files, letting you find what you’re looking for quickly.
Filter Name
Description
All Files
All files you own and have access to.
Recent
The most recent files you've viewed.
Owned by Me
Only the files you own.
Shared with Me
Only files that have been shared with you either by a private share or a public post to your profile.
Following
Only the files you're following in Chatter. This filter is available only in organizations that use Chatter.
Synced
All files that have been synced in your Salesforce Files Sync folder. This filter is available only to users who have the “Sync Files” permission.
Libraries
Files anyone has uploaded to libraries you're a member of, as well as all files you uploaded to your private library, grouped by library name.
See Also:
View Where a File is Shared
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_files_list_filter.htm&language=en_US
Release
202.14
