### Topic: Guide to automate tasks and specific rules for your ad campaigns.
Social.com provides several features designed to assist you with repeated daily tasks. You can use Social.com’s powerful data analysis engines to automate tasks you otherwise have to perform manually, multiple times per day.
A few examples of what you can automatically do with Social.com: 
1. Automatically redistribute budgets to top ad sets.
2. Raise budgets on weekends and then lower them during the week.
3. Get automatic notifications for any errors with your ads.
4. Raise bids for under-delivering ads
Resolution
Click 
here
 to access the full guide to Automation and Rules. 
