### Topic: This article will help you with instructions for Creating and Sending an SLA Report to an Account.
Monthly or Quarterly SLA Reports for Customers
Support Operations creates generic service level agreement reports for a small list (approximately 200) of customers; some monthly and some quarterly. There are similarities, but also some specifics requirements for each customer. For example Sprint Nextel has three metrics that are monitored, while most companies have one; and Cisco gets a monthly report that includes the Service Metric, in addition to other case statistics.  
The data received from support operations should not be sent directly to the customer, but should be reformatted based on what the customer is to receive per their SLA Agreement and sent only to the designated recipients at the company in pdf format.
In General the report that goes to the customer will be a one page pdf file with the Company name, the dates of the Service Availability, the Metric specified in the SLA and the Actual Availability from the report that Support Operations sends out.   The current Support Operations contact for this process is Heather Logan.
Any additional information is specific to the customer.  You should consult the SAS, AE or CSM for specific details regarding the format for each customer's requirements.  
NOTE:  
SLA reporting is only available by special contractual arrangement.  We do 
not
 advertise it as a widely available service.  If you have questions, please contact your team lead or manager.
 
Resolution
Here is a GENERIC EXAMPLE of what the Quarterly SLA Report for Ford Motor Credit Company would look like for the fourth quarter of 2006.
 
Line 1 is the Account name and the dates that this reporting is for. 
Line 2 is the service availability metric from the SLA record.  This is a related record on the account in salesforce, in this case it's 99.00%.  
https://na1.salesforce.com/a0230000004JqEm
Line 3 is the actual availability from the last couple of lines of the .txt file sent from Support Operations for your specific account. 
Attached to this solution is a copy of this SAMPLE quarterly SLA Report for Ford Motor Company 
EXAMPLE
 
Ford Motor Credit Company Service Availability for 10/1/2006 through 12/31/2006
Service Availability Metric:  Greater or Equal to 99.00%
Actual Availability:  99.89%
An account that has SLA reporting will have an SLA record that says something like 'in conjunction with Premier Support' in the 'Description of SLA Reporting' field.  To find the SLA record, go to an account, find the SLA related list, and open the record.  Here's an example SLA record:  
http://na1.salesforce.com/a023000000BLUtM
. 
ONLY accounts that have such records are to be provided with SLA reports on a regular basis.  If you have a one-off request for uptime performance data, see article #7236 instead:  
https://na1.salesforce.com/kA030000000PEDR?srPos=0&srKp=ka0
If an account is missing an SLA record, the AE should be involved to engage Legal to create the record.  Once the record has been created, send an email to Atul Nanda and Paul VanMaanen to get approval.  Once approved, please request access to the 
SLA Monthly & Quarterly Reporting Group
.  Once you have access, please post a request for the org to be included in monthly or quarterly processing going forward - please include a link to each account with a SLA record created.
