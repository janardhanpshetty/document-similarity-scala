### Topic: Determine Which Data to Include in the Dataset | Salesforce
Determine Which Data to Include in the Dataset | Salesforce
Determine Which Data to Include in the Dataset
First, determine what data you want to include in the dataset. For this example, you will create a Targets dataset that contains all sales targets.
You will obtain sales targets from the CSV file shown below.
AccountOwner
Region
Target
TargetDate
Tony Santos
Midwest
10000
1/1/2011
Lucy Timmer
Northeast
50000
1/1/2011
Lucy Timmer
Northeast
0
12/1/2013
Bill Rolley
Midwest
15000
1/1/2011
Keith Laz
Southwest
35000
1/1/2011
Lucy Timmer
Southeast
40000
1/1/2011
If you were to create the dataset without implementing row-level security, any user that had access to the dataset would be able to see the sales targets for all account owners. For example, as shown below, Keith would be able to view the sales targets for all account owners.
You need to apply row-level security to restrict access to records in this dataset.
Parent topic:
 
Row-Level Security Example based on Record Ownership
Next topic:
 
Determine Row-Level Security for Dataset
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_rowlevel_example_recordownership_determinedata.htm&language=en_US
Release
202.14
