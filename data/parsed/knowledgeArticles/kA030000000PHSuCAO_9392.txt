### Topic: This article discusses on how to use custom label to show dynamic values as parameters in templates.
Sometimes Customer may ask you to use a custom label to show dynamic values with respect to any criteria or any specific to user.
Resolution
We can use custom labels to use dynamic values by using "{0}" . This symbol represents where to put dynamic value in custom label
 
How to use {0} in  Labels
:
 
 
Label body : 
Hi {1}, <br><br> 
 
Please <a href="{0}">click here</a> to open google.com <br><br>   //this will automatically put value at {0} place at run time
Sincerely, <br><br> 
 
 
Google 
{0}
 
//End of label body 
 
From where to send dynamic value to Labels??
 
As you already know, you can use Custom Labels on Vf pages and templates.
 
Expression to use it on pages/templates:
 
<apex:outputText value="{!$Label.GoogleEmailTemplate}" escape="false">
    <apex:param value="{!relatedTo.Url__c}"/>
    <apex:param value="{!relatedto.name}"/>
</apex:outputText
 
Note : 
1.These <apex:param> tags will pass values stored in  their fields to label and label will show it at run time.
2. If we need to pass to parameters, for second parameter, you just need to use {1}, for the third parameter {2} and so on..
 
