### Topic: The close date 2 field is a column you can choose when you create an opportunity report. It does not show on the opportunity page layout.
What is the close date 2 field?
Resolution
The close date 2 field is a column you can choose when you create an opportunity report. It does not show on the opportunity page layout.
Currently, you cannot group by the same field twice in reports.
The CloseDate2 field exists as a workaround to this by presenting the same field twice. The CloseDate and CloseDate2 fields can then be used to group a report by the CloseDate in two different ways. For example, in a matrix report, grouping by the Year along the columns, and grouping by the Month along the rows.
