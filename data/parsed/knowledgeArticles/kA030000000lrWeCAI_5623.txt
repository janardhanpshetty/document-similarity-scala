### Topic: This KB illustrates the step-by-step instruction on how to export forecasts using APEX Dataloader.
How to export Quotas (Forecasting) using the Data Loader.
Resolution
Step #1
-Login to Dataloader and select 
Export.
Step#2
For Customizable Forecasting
-Check the checkbox for 'Show all Salesforce objects'
-Select 'Revenue Forecast (RevenueForecast)'
-Insert desired Export file name on the 'Choose a target for extraction' box
-Click Next
For Collaborative Forecasting/Forecasting 3
-Check the checkbox for 'Show all Salesforce objects'
-Select 'Forecasting Quota (
ForecastingQuota
)'
-Insert desired Export file name on the 'Choose a target for extraction' box
-Click Next
**NOTE: 
The Revenue Forecast Object (Collaborative Forecasting) is only available in Data Loader version 23 and above.
Step#3
-Click 'Select all fields' to select all forecast fields that you want to include on the export file. (Recommended)
-You can also select fields that you want to include on the export file and deselect the fields that you want to exclude.
Step#4
-Click 
FINISH
Step#5
-Click 
YES
 on the confirmation page
Step#6
-Click 
VIEW EXTRACTION 
button to view the CSV file
Step#7
-While viewing the CSV file on the default CSV viewer, you have the option to open the CSV file on your external file by clicking the 
"OPEN IN EXTERNAL PROGRAM" 
button.
Step#8
-You should get a CSV file similar to this. This include but not limited to the fields ID, PeriodID, StartDate, ProductFamily, OwnerID, ManagerID, Quota, Closed, etc.
Note that 
Report filters ( FeedItemCategory) cannot be exported using dataloader.
