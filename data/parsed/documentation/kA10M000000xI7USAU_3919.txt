### Topic: Schedule the Event Monitoring Wave Daily Dataflow | Salesforce
Schedule the Event Monitoring Wave Daily Dataflow | Salesforce
Schedule the Event Monitoring Wave Daily Dataflow
Schedule a daily dataflow at a time outside standard business hours before you start exploring with Event Monitoring Wave.
User Permissions Needed
To create and manage Wave apps:
“Manage Wave Analytics Templated Apps”
“Edit Wave Analytics Dataflows”
When you create Event Monitoring Wave, the creation process includes a dataflow that imports the latest Event Monitoring data to Wave. You can schedule the dataflow to be rerun every day to assure that your app uses up-to-date Salesforce data. Schedule the dataflow to take place sometime outside normal business hours so the dataflow doesn’t interrupt your use of the app.
Note
The Event Monitoring Wave dataflow runs only once when you create the app. Schedule it to run daily so the app uses the latest event monitoring data.
Go to 
Wave Analytics
 in the Force.com menu (top right of the Salesforce window) if you’re not already there. Open the Data Monitor by clicking the gear icon at the upper right of the screen. 
Select Dataflow view from the menu at the top left of the Data Monitor screen. 
Find the app you created; you may have to scroll down the page. Open the menu on the far right of the screen next to the app icon and name. Select 
Schedule
, and set a time for the dataflow. Select a time outside normal work hours so the dataflow doesn’t interrupt business activities. Click 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_app_event_monitor_schedule_dataflow.htm&language=en_US
Release
202.14
