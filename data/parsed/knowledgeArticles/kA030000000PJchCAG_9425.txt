### Topic: This article was written to help provide troubleshooting steps that can be used to help debug Opportunity Splits enablement failures, and to try and prevent escalations to R&D/CCE.
This article was written to help provide troubleshooting steps that can be used to help debug Opportunity Splits enablement failures, and to try and prevent escalations to R&D/CCE.
NOTE: 
 Most of the steps below require the use of both SPLUNK and/or Scrutiny fixers in Blacktab, and as such, are most applicable to Tier 3 Support only.
Summary
:
In few cases, Customers on Summer ‘13 sandbox or production instances are unable to turn on the new Opportunity Splits feature. Customers who try to enable the new feature in Summer ‘13 will ultimately receive a failure email like the one below: 
"You requested an upgrade to the Salesforce Opportunity Splits feature for the following organization: 
Company: <Org Name> 
Organization ID: <Org ID> 
Instance: <Instance Code> 
The upgrade process for Salesforce Opportunity Splits didn’t complete, so your organization rolled back to its previous state. 
For information about why the process didn’t complete, contact 
salesforce.com
 <
http://salesforce.com
> Customer Support"
 
Repro
:
1. Login to Summer ‘13 Org 
2. Attempt to enable new Opportunity Splits feature: 
Setup | App Setup | Customize | Opportunities | Opportunity Teams | Opportunity Splits | Enable Opportunity Splits 
3. Save setting, and confirm that a failure email is later received
Resolution
Troubleshooting Steps:
Get customer’s Org Id, Instance and run the following SPLUNK query:
 
index!=*x (index=<Instance>)  
<Org ID> ERROR_IN_PK_CHUNKING_ENQUEUE 
e.g. 
 
index!=*x (index=NA8)  
00D80000000LlGG
 ERROR_IN_PK_CHUNKING_ENQUEUE
 
Also...
 
Gack watcher would be another place to retrieve the  stack info.
https://delphi.dmz.salesforce.com/gacks/gackWatcher.jsp
Select the  target instance  where the org resides and and select “Earliest” field  2/4/12/...hours as appropriate and put /<<orgid>>/i in the search box. 
[KL] We should also look at splunk for message trend. Use the following query strings in Splunk
index!=*x (index=<instanceId>*)   <orgId> opjob 
index!=*x (index=<instanceId>*)   <orgId> logRecordTypeG=1
index!=*x (index=<instanceId>*)   <orgId> OpportunitySplitEnableItemHandler
index!=*x (index=<instanceId>*)   <orgId> opp_split_enable
 
 
IMPORTANT NOTE
:  
Please note that the customer might be hitting one or more of the below listed GACKs, hence its advised to check for all potential causes, fix as applicable and then request customers to re-try.
 
***It is recommended that somebody who has previously run scrutiny should run the below data scrutinies (or) supervise the person running it.***
 
  
GACK / Exception
Root Cause
Solution / Workaround
lib.gack.GackContext: sales.opportunity.
InvalidRollupException
: java.sql.SQLException: ORA-20423:
 
Note: This is typically seen in Sandbox Orgs due to partial data copy from Production. 
 
The root cause here is that the Opportunity Amount (which is a RSF, roll-up summary field) does not add up to the Sum of Opportunity Line Item Amounts.  Because one of the opportunity line items did not get copied to sandbox. This is a known issue with partial sandbox copy.
Fix the Rollup Issue by following the below steps:
1.    Login into the instance where the Org resides . On the BT page click the “SFA Core Misc. BT Pages” link.
2.    Click on the “Fix Opportunity Standard Rollup Values” scrutiny link.
3.    Enter the Org id for which the scrutiny needs to be run and your email id.
4.    
IMPORTANT
: Leave the commit checkbox 
UNCHECKED
 for now.
5.    Click “Fix Rollups”
 
Once the scrutiny is run you should get an email with the result.
 
***It is important to first run the scrutiny in non-commit mode.***
 
If no records are found you should see an email with...
HAS LINE ITEM FLAG SHOULD BE FALSE: FALSE
HAS LINE ITEM FLAG SHOULD BE TRUE: FALSE
OPPORTUNITY AMOUNT AND/OR QUANTITY INCORRECT: 0
 
If records are found you should see an email with...
HAS LINE ITEM FLAG SHOULD BE FALSE: <TRUE/FALSE>
HAS LINE ITEM FLAG SHOULD BE TRUE: <TRUE/FALSE>
OPPORTUNITY AMOUNT AND/OR QUANTITY INCORRECT: 
<N>
 
***If the result finds any rows then repeat the above steps, this time by clicking on the “Commit” checkbox.***
 
Ask Customers to re-try the feature enablement.
common.udd.object.PreSaveValidationException: 
Invalid currency: <Currency_Code>
 
e.g.
lib.gack.GackContext: common.udd.object.PreSaveValidationException: 
Invalid currency: TWD
 
lib.gack.GackContext: common.udd.object.PreSaveValidationException: 
Invalid currency: JPY
Note: This issue has been reported from Sandbox and Production Instances. 
 
The root cause here is that the <Currency Code> in question is an inactive currency for this Org. When running the upgrade job, we create Opportunity Splits data and the currency (CurrencyIsoCode) is derived from the parent opportunity. In this case, the parent opportunity has a currency code of <Currency Code>, which is currently inactive.
1.     Change the currency on the associated account and opportunity records to an active currency and retry (or)
2.     Request customer admin to make this currency code active and try re-enabling? (or)
3.     Alternatively, if there are just few old opportunities that leverage this currency – delete them and then re-try.
system.security.
NoDataFoundException
: ORA-20001:
Note: This is typically seen in Sandbox Org’s due to partial data copy from Production. 
 
The root cause here is that the Opportunity references a parent Account and the Account record is not copied.  This is a known issue with partial sandbox copy.
Fix the Data Issue by following the below steps:
1.     Login into the instance where the Org resides . On the BT page click the “Scrutiny” link.
2.     Enter the "Organization ID"
3.     Make sure the Instances -> "Run on instances" section has the correct instance checked.
4.     
IMPORTANT: Leave the fixer options (Run fixers and Commit fixers) 
UNCHECKED
 for now.
5.     In the "Task-Specific Options" section, enter the following in the "FK Fields": Opportunity.Account
6.     Leave the other 3 fields in this section blank.
7.     In the "Scrutiny Tasks" section search for "Keys" and expand it. Select "tools.scrutiny.generic.ScrutinyForeignKeys".
8.     Click “Run”
9.     Once the scrutiny is run you should get an email with the result.
 
If no records are found you should see an email with the following info:
●      Organization Id: 00Dxxxxxxxxxxxx
●      Options: FK: Opportunity.Account
●      Maximum quick rows: 100
●      Tasks: tools.scrutiny.generic.ScrutinyForeignKeys: Run-only
●      ---------- STOPPED COLLECTING OUTPUT -------------
●      tools.scrutiny.generic.ScrutinyForeignKeys
 
If any records are found you should get two emails:
●      One with the number of records found and one listing the actual record info for all records found.
 
***It is important to first run the scrutiny in non-commit mode.***
 
If the result finds any rows then repeat the above steps by clicking on the following in the Fixer Options section (check both the checkboxes now):
a.     Run fixers
b.     Commit fixers
 
Ask Customers to re-try the feature enablement.
ANOTHER_JOB_IN_PROGRESS_
JOB_NOT_ENQUEUED
`SplitsNotEnabled
Note: This issue has been reported from Sandbox and Production Instances. 
 
The root cause here is that a previous enablement job failed due to known issue and the queue did not get cleaned up fast enough.
1.     Wait for old job to cleanup before retrying feature enablement. (or)
2.     Use BT Message queue tool to delete the corresponding job
 
This could also happen when there is a message in the parallel_job_status table indicating that the job is waiting / processing while there is no real process running.
 
We do not have a scrutiny for this yet.
 
If a job is running for couple of days and we do not see any chunks getting processed in splunk. And if we do not see MQ activity then we should run a script to see if there is an orphan job and at this time the orphaned job needs to be cleaned via manual edit script.
 
*** Create a new investigation record for this scenario and let CCE handle, get appropriate approvals from customer and clean the orphaned job with a manual edit script***
common.sql.
OracleRowLockedException
: java.sql.SQLException: ORA-30006: resource busy; acquire with WAIT timeout expired
Note: This issue has been reported from Sandbox and Production Instances. 
 
The root cause here is due to transient race condition or other activities on Opportunities, there could be Row Locking issues
 
e.g. there was simultaneous Opportunity BATCH CRUD operation during enablement process that could have caused this.
We have seen that retrying the feature enablement has often succeeded. 
 
If possible ask customers to ensure there are no BATCH or APEX Jobs that does batch CRUD operations on opportunities are running while they try to enable the feature e.g . Mass Transfer, Mass Re-Assign Team Members etc. 
 
***This has also been called out in the ALERT section while trying to enable the Summer ‘13 Opportunity Splits Feature under Setup.***
Subject: Unexpected exception in queue processing type OPP_SPLITS_ENABLE with exception DmlOperationException
Thrown: common.udd.object.DmlOperationException
Subject: ERROR_IN_PK_CHUNK_PROCESSING
Thrown: lib.gack.GackContext: system.security.NoAccessException
Note: This issue has been reported on Sandbox
The root cause here is The load_opportunities plsql does a join b/w opportunity and opportunity_cfdata.
In customer's org there are records/opptys  missing custom field data and so were not returning any opptys causing the access check to fail.
Since enable job processes data in chunks the who chunk was marked as failing access checks.
Fix the Data Issue by following the below steps:
1.     Login into the instance where the Org resides . On the BT page click the “Scrutiny” link.
2.     Enter the "Organization ID"
3.     Make sure the Instances -> "Run on instances" section has the correct instance checked.
4.     
IMPORTANT: Leave the fixer options (Run fixers and Commit fixers) 
UNCHECKED
 for now.
5.     In the "Scrutiny Tasks" section search for "CustomEntity" and expand it. Select "
ScrutinyCustomFieldDataMissing.Opportunity 
".
6.     Click “Run”
7.     Once the scrutiny is run you should get an email with the result.
 
If no records are found you should see an email with the following info:
●      Organization Id: 00Dxxxxxxxxxxxx
●      Maximum quick rows: 100
●      Tasks: 
ScrutinyCustomFieldDataMissing.Opportunity 
: Run-only
●      ---------- STOPPED COLLECTING OUTPUT -------------
●     
ScrutinyCustomFieldDataMissing.Opportunity 
 
If any records are found you should get two emails:
●      One with the number of records found and one listing the actual record info for all records found.
 
***It is important to first run the scrutiny in non-commit mode.***
 
If the result finds any rows then repeat the above steps by clicking on the following in the Fixer Options section (check both the checkboxes now):
a.     Run fixers
b.     Commit fixers
 
Ask Customers to re-try the feature enablement.
Also you can check Splunk Logs for the Entry OpWithInvalidUserTypeException
 
This will give you a list of Opportunities that might be assigned to a User that does not have the  the right user type to be able to add OTMs (INV: W-2314539)
 
 
Note: If the customer org is not hitting any of the above root issue and we are not sure what is causing their enablement failure, please use the normal escalation process and create a new GUS investigation for R&D / CCE team to investigate further.
 
 
Frequently Asked Questions
 
Question
:
 The first attempt to enable the feature took 3+ hrs and the next attempt after fixing the data took just 10 mins.  Is that normal?
 
R&D Response
:
  This is perfectly fine.  The first attempt here upgraded most of their data and failed on few records, which were re-tried 5 times before the first attempt failed.  So all the second attempt had to do was process (upgrade) those few records, and hence it may process is quickly.  Nothing to be alarmed about this.
 
Opportunities "stuck" in processing: 
If you are able to fix the issue with one of the above scrutinies and the Opportunity Splits are still showing as in process, go to:
BT of the Org > 
 
SFA Core Misc. BT Pages
 > "
Reset Opportunity Splits Availability Status (for Enabling Splits for the first time)
"
