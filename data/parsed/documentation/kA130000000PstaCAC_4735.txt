### Topic: Confirm That Data.com Clean Is Enabled | Salesforce
Confirm That Data.com Clean Is Enabled | Salesforce
Confirm That Data.com Clean Is Enabled
Learn how to enable Data.com Clean so your organization can start keeping its records up to date.
Available in: Salesforce Classic
Available with a Data.com Prospector license in: 
Contact Manager
 (no Lead object), 
Group
, 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available with a Data.com Clean license in: 
Professional
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
User Permissions Needed
To enable or disable Data.com Clean:
“Customize Application”
Data.com Clean ensures that your CRM records are always up to date. You can use Clean with all your account, contact, and lead records—not just those records added from Data.com. When you purchase the Data.com product, Clean is automatically enabled, so you can set up automated Clean jobs and users can clean individual records manually or clean groups of records from a list view.
Enabling Clean makes clean features available to your organization, and it causes the 
Preferences
 and 
Jobs
 links to appear in Setup under the Clean section.
Note
Enabling Clean 
does not
 clean your records immediately, and you can click 
Disable
 to turn off the feature.
From Setup, enter 
Clean Settings
 in the 
Quick Find
 box, then select 
Clean Settings
.
Confirm that Data.com Clean is enabled.
Parent topic:
 
Set Up Data.com Clean
Next topic:
 
Configure Page Layouts for Data.com Prospector and Data.com Clean
See Also:
Data.com Clean
Set Up Data.com Clean
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=data_dot_com_clean_clean_enabling.htm&language=en_US
Release
202.14
