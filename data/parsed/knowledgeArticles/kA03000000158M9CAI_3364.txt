### Topic: The "EMAIL" field in the message summary for a Send Email template has a value of the Triggered Send ID selected.
In the summary section of a MobileConnect Send Email Message there is a field called "EMAIL" with an ID as the detail.  
Resolution
This field refers to the Triggered Send ID for the Triggered Send interaction found in the interactions tab of the email application. This is mostly used for reference on the back-end. An example of when you would use this is when you retrieve a TriggeredSendDefinition object via the Webservice API. 
