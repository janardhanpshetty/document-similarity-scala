### Topic: The Customize Application profile permission grants several other permissions on the profile. Administrators should be aware of the profile permissions that are enabled automatically when the "Customize Application" permission is enabled for a profile.
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, 
Developer
, and 
Database.com
 Editions
The Customize Application profile permission grants several other permissions on the profile.  Administrators should be aware of the profile permissions that are enabled automatically when the "Customize Application" permission is enabled for a profile.  
Resolution
The following permissions are enabled automatically when the Customize Application profile permission is enabled.
 
Customize Application (Permission):
 
Customize the organization using App Setup menu options.
 
Edit messages and custom links;
Modify standard picklist values;
Create, edit, and delete custom fields;
Create, edit, and delete page layouts (also requires the “Edit” permission for the object, for example, “Edit” on accounts);
Set field-level security;
Create, edit, and delete custom links;
Edit the Lead Settings;
Activate big deal alerts;
Create record types;
Set up Web-to-Case and email response rules;
Set up Web-to-Lead and email response rules;
Set up assignment and escalation rules;
Set up business hours;
Set up Email-to-Case or On-Demand Email-to-Case;
Edit Self-Service page layouts and portal color theme (also requires the “Manage Self-Service Portal” permission to set up and maintain Self-Service settings and delete your organization's Self-Closed Case Status value);
Set up and enable multilingual solutions;
Set up team selling;
Set up account teams;
Map custom lead fields;
Manage queues;
Create, edit, and delete workflow rules, tasks, field updates, outbound messages, and alerts;
Create, edit, and delete custom s-controls, custom objects, and custom tabs;
Rename tabs;
Manage custom apps and Service Cloud console apps;
Create and edit public calendars and resources;
Set up the console;
Enable, set up, and modify the Salesforce Customer Portal;
Set up and schedule analytic snapshots to run;
Create communities for ideas and answers;
Create Visualforce email templates
Related Article:
Cloning Profiles
