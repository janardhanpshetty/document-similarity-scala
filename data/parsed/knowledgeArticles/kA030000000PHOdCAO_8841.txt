### Topic: How to rename a dashboard within the Analysis Dashboard
The Analysis Dashboard provides the option to rename your dashboards to help organize your data. The steps to do so are outlined below.
Resolution
Rename your Dashboard
 
1) Log in to the 
Analysis Dashboard
.
2) Along the top, you will see the numbered dashboards that you currently have open. Select the dashboard for which you would like to change the name.
3) Click on the grey gear icon next to the numbered dashboards.
4) From the drop down menu select 
Rename Dashboard.
5) Type the new name in the window that populates and click 
Save
.
6) You will notice at the top right of your screen that the selected dashboard has been renamed.
