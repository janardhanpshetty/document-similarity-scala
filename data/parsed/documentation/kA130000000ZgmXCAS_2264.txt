### Topic: How to Reconnect a Social Account | Salesforce
How to Reconnect a Social Account | Salesforce
How to Reconnect a Social Account
Reconnect your social account for Social Customer Service.
Available in: Salesforce Classic
Social Customer Service is available in 
Enterprise
, 
Performance
, and 
Unlimited
 editions.
User Permissions Needed
To administer Social Customer Service:
“Manage Users”
AND
“Customize Application”
To create case feed items:
Feed Tracking for All Related Objects on the Case object
To send and receive social media posts or messages:
Case Feed enabled
AND
Access to a social account
Your social account can be disconnected from Social Customer Service if your social media network provider’s connect, or token, has expired. Many providers have expiration policies of 60 to 90 days.
From Setup, enter 
Social Media
 in the 
Quick Find
 box, then select 
Settings
.
On the 
Social Accounts
 tab, click 
Reauthorize
 in the 
Action
 column.
The social network opens and asks you to authenticate the account. Once your account is reauthenticated, you are returned to the Social Accounts tab.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=social_customer_service_reconnect.htm&language=en_US
Release
202.14
