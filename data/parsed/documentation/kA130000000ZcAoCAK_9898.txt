### Topic: Enable Similar Articles for Chatter Questions | Salesforce
Enable Similar Articles for Chatter Questions | Salesforce
Enable Similar Articles for Chatter Questions
When users ask questions in Chatter, similar questions appear as they type. If you’d like relevant Salesforce Knowledge articles to appear as well as questions, enable Similar Articles.
Available in: Salesforce Classic
Chatter Questions is available in: 
Personal
, 
Group
, 
Professional
, 
Developer
, 
Performance
, 
Enterprise
, and 
Unlimited
 Editions
User Permissions Needed
To enable Similar Articles for Chatter Questions:
“Customize Application”
Note
Chatter and Salesforce Knowledge must be enabled in your organization.
Enabling Similar Articles turns on the feature in:
Your internal Salesforce organization
Your communities built with Salesforce Tabs + Visualforce or the Napili template
From Setup, enter 
Knowledge Settings
 in the 
Quick Find
 box, then select 
Knowledge Settings
.
Under Chatter Questions Settings, select 
Display relevant articles as users ask questions in Chatter (also applies to communities with Chatter)
.
Note
Make sure that the Question action has been added to the desired page layouts. Otherwise, users can’t see it.
To learn more about how Similar Articles and Similar Questions work, see:
Display 
Similar Questions
 and Articles in Chatter
Display Similar Questions and Articles in the Napili Search
See Also:
Display Similar Questions and Articles in the Napili Search
Question-to-Case Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_chatter_questions_knowledge_deflection_enable.htm&language=en_US
Release
202.14
