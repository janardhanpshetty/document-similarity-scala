### Topic: Opportunity Reports | Salesforce
Opportunity Reports | Salesforce
Opportunity Reports
Opportunity reports provide information about your opportunities, including owners, accounts, stages, amounts, and more. The default settings show you the most commonly used information from each object, but you can customize a report to view other information, such as primary campaign source, forecast category, and synced quote.
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Tips for Working with Opportunity Reports
Opportunity reports can include all opportunity fields plus some extra columns for additional detail.
Standard Opportunity Reports
Standard opportunity reports help you report on your opportunity pipeline and history, opportunity sources, opportunity types, and more.
Opportunity Report Types
Report types provide a report template that you can customize to capture the unique data you’re after without creating a report from scratch. Many of the opportunities custom report types include information from associated objects, such as products, partners, and quotes.
Parent topic:
 
Standard Report Types
Previous topic:
 
Lead Reports
Next topic:
 
Product and Asset Reports
See Also:
The Report Run Page
Limit Report Results
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=reports_oppforesales.htm&language=en_US
Release
202.14
