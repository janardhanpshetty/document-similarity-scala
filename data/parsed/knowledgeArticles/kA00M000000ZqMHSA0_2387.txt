### Topic: A known limitation of Lightning Experience and Bounce management.
Emails sent from Contacts and Leads using Lightning Experience record bounce management as when using Classic Experience.  However when sending email from Lightning Experience users may see an error related to bounce flags on the Contact or Lead. This error would show as "Emails to one or more recipients have bounced before. Check email addresses to make sure they are valid."
Resolution
Bounce error workarounds
1. The user encountering the error would need to Switch back to Classic Experience by selecting their 
User Icon
 in the top right hand corner of the application.  
2. Next navigate to the Contact or Lead showing the error and click the 
Confirm Email address
 in the email field on the record to either modify email address or clear the bounce.  After this, the user can return back to Lightning Experience to send emails as normal.
NOTE
: Email sent using Lightning Experience and the feature "Send through External Email Services" do not record bounce management.
