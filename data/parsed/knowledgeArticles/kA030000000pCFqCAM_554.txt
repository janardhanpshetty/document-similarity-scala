### Topic: How to debug Salesforce1 Android Hybrid app using laptop
This article defines on how to debug Salesforce1 Android Hybrid app using a mobile device and a laptop to identify the issue(s). 
Resolution
NOTE:
 
Also, make sure to go through this article which has further steps to identify and narrow down the issue related to performance. Article # 
000213305
Remember, if the Developer Options are turned on few of the mobile companies won't entertain the warranty. So make sure to check with the customer if you are having them perform these steps. 
Turning On The Developer Options On The Android Device:
Go to Settings on the mobile
Select: About Device
Scroll down until you see 
"Build Number"
Keep tapping on the it for 7 times, until you get the the prompt stating "Congrats! Now you are a developer!"
To verify this, tap on the "Build Number" once again and you should see the below screen
Go back to "Settings" and check if you can see the option "Developer Options"
If yes, go to that and make sure the following are turned "ON"
You will get a prompt on device, select "OK"
Debugging The Issue:
 
If you already have a version of the Salesforce1 app installed on the device, please uninstall it now;
Install the debuggable Salesforce1 app by accessing from the mobile browser 
http://hockeyapp.net
 and clicking on the desired version (e.g. “Salesforce1 for Android 7.2.1”) which as “Android | debug” underneath.
Make sure that the device is connected to your laptop through a USB cable
Now launch the "Google Chrome" on laptop 
Go to "chrome://inspect/#devices"
Make sure "Discover USB Devices" is checked and the device is listed below it
On you device, you will the below notification.
Check "Always allow from this computer" and then tap on "OK"
You should be able to the app's running on the mobile in the next screen:
Now, launch Salesforce1 app on the mobile device and follow the steps in the Article # 
000213305
 
for further troubleshooting steps.
