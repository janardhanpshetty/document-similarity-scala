### Topic: Enable Chatter Answers in Your Community | Salesforce
Enable Chatter Answers in Your Community | Salesforce
Enable Chatter Answers in Your Community
Chatter Answers is a self-service support community where users can post questions and receive answers and comments from other users or your support agents.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To enable Chatter Answers in Salesforce Communities:
“Customize Application”
To set up Chatter Answers in Salesforce Communities, follow these high-level steps:
Enable Chatter Answers.
From Setup, enter 
Chatter Answers Settings
 in the 
Quick Find
 box, then select 
Chatter Answers Settings
.
Select 
Enable Chatter Answers
.
Ensure that your community members have access to the following objects within your organization:
Questions
Knowledge Articles
Data Categories
Create a zone for Chatter Answers.
Enable the zone for Chatter Answers.
Set the 
Visible In
 setting to the community that you want the zone shown in.
As a best practice, select a public group from your organization to designate as a Customer Support Agents Group.
Add the Q&A tab to your community.
From Setup, enter 
All Communities
 in the Quick Find box, then select 
All Communities
 and click the 
Manage
 link next to a community. To access this page, you need the Create and Set Up Communities” permission.
Click 
Administration
 | 
Tabs
.
Add the Q&A tab to the Selected Tabs list.
Click 
Save
.
Click 
Close
.
Make the Q&A tab visible on profiles that need access to it.
From Setup, enter 
Profiles
 in the 
Quick Find
 box, then select 
Profiles
.
Click 
Edit
 for the profile that you want to make the Q&A tab visible for.
Under Standard Tab Settings, set the 
Q&A
 tab to Default On.
Click 
Save
.
After you have enabled Chatter Answers in your community, consider the following extra deployment options:
A public-facing Force.com site with or without a portal.
A Visualforce tab, which provides branding, a customized landing page, and custom access to Chatter Answers within your community.
If a user self-registers for a community with Chatter Answers enabled, the Chatter Answers User permission is not automatically set for the user. Set permissions for Chatter Answers on the community user.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_enable_chatter_answers.htm&language=en_US
Release
202.14
