### Topic: Integrating your Adobe Genesis (also known as Omniture Analytics") data with your subscriber data from Marketing Cloud provides great opportunities to learn more about your subscribers.
Integrating Marketing Cloud with Adobe Genesis (also known as "Omniture Analytics") is the process of setting up Adobe Genesis and Marketing Cloud to expect data from each other and provide the expected data. 
There are two primary business purposes for integrating Marketing Cloud with Adobe Genesis, outlined below. 
Resolution
Two business purposes for the integration
Unified Reporting
Integrating Marketing Cloud with Adobe Genesis for 
unified reporting 
sets up Marketing Cloud to include Subscriber and Campaign identifiers in the URLs of email links so that Adobe Genesis can capture the data when a subscriber clicks a link.
 
Behavior-based re-marketing
Integrating Marketing Cloud with Adobe Genesis for 
behavior-based re-marketing 
sets up Adobe Genesis to create three lists of Subscribers based on their behavior on your website after clicking a link in an email. You can then use those lists of Subscribers to focus your continuing email marketing effort based on the Subscriber behavior. The three lists created by Adobe Genesis are:
 
1. 
Cart Abandonment - visitors who abandoned their shopping cart and the products that were in the cart at the time.
2. 
Product Views - visitors who viewed products and what products they viewed.
3. 
Product Purchase - visitors who purchased products and what products they purchased.
Learn more about the required setup by 
reviewing our documentation called "ExactTarget for Adobe Genesis."
 
Interested in including Adobe Genesis integrations with your Marketing Cloud account?
 Please reach out to your Account Executive to discuss the purchase and implementation of this functionality.
