### Topic: It's important that you comply with Google’s terms of service, which require that you have a commercial agreement with Google if you are using Google Maps API internally for your business (i.e. not a public-facing website).
At Salesforce, your success is our top priority, and we are committed to working with you to identify which features will help you best achieve your business goals. We want to notify you of an important change regarding the availability and support of the Google Maps API. You are receiving this email because you are a system administrator of a Salesforce organization that has installed the “Find Nearby - Accounts, Contacts, Leads” AppExchange package which uses the Google Maps API.
 
What is the change?
It's important that you comply with Google’s terms of service, which require that you have a commercial agreement with Google if you are using Google Maps API internally for your business (i.e. not a public-facing website). Unless you currently meet the following criteria, 
we recommend you reach out to Google to get a Google Maps API for Business license to keep using embedded Google Maps:
Your organization has a Google Maps API for Business license, OR
You’re using an AppExchange solution that has a commercial agreement with Google
 
Note:
 The “Find Nearby - Accounts, Contacts, Leads” AppExchange package does 
not
 have a commercial agreement with Google. You will need to obtain a Google Maps API for Business license to continue using this package.
 
Who does this affect?
There are three main categories of salesforce customers who will need to comply with Google’s terms of use:
You use the Google API to get a Google map view inside of your salesforce  instance
You have inserted a public key (as opposed to a 
Google Maps API for Business licensed key
) into a VisualForce page to render a maps view
You have installed any AppExchange solution that makes calls to Google (i.e. “Find Nearby”) that do not have a commercial agreement with Google
 
It will not impact you if:
You use links to Google Maps that open a new browser tab or window that takes you directly to maps.google.com
You use an AppExchange solution that has a commercial agreement with Google
 
When will the change occur?
As early as February 1, 2014
,
 if you do not have a 
Google Maps API for Business license
, your users will receive an error message on their webpage or device when accessing “Find Nearby” or any other page within your Salesforce application with embedded Google Maps. The message will direct you to this site: 
https://developers.google.com/maps/support/availability
, where you can find more information on how to obtain a Google Maps API for Business license.
 
What action should I take?
If you use Google Maps or Google Maps API in any of three categories outlined above, we recommend that you visit this page: 
https://developers.google.com/maps/support/availability
 to get a Google Maps for Business license. If you use an AppExchange solution that uses Google Maps API, we encourage you to contact your AppExchange solution provider to understand if they have a commercial agreement with Google.
Resolution
