### Topic: Manage Mobile Cards in the Enhanced Page Layout Editor | Salesforce
Manage Mobile Cards in the Enhanced Page Layout Editor | Salesforce
Manage Mobile Cards in the Enhanced Page Layout Editor
Add expanded lookups, components, and Visualforce pages to the Mobile Cards section of your page layout to have them show up as mobile cards in Salesforce1.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 editions except Database.com
User Permissions Needed
To customize page layouts:
“Customize Application”
To view page layouts:
“View Setup”
The items you place in the Mobile Cards section don’t show up on a record’s detail page in the full Salesforce site but only on the record’s related information page in Salesforce1.
You can add these types of elements to the Mobile Cards section of a page layout.
Components
Expanded lookups
Visualforce pages
Canvas apps
Some standard objects have mobile cards that are added for you by default. Customize the page layout to remove them or add additional cards.
Access the page layout editor
.
From the Expanded Lookups, Components, Visualforce Pages, or Canvas Apps categories, drag an element into the Mobile Cards section.
Save the page layout.
After saving the layout, the items you added show up immediately in Salesforce1. You may need to refresh to see the changes.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=layouts_customizing_related_content.htm&language=en_US
Release
202.14
