### Topic: Where Do Approval Request Posts Appear? | Salesforce
Where Do Approval Request Posts Appear? | Salesforce
Where Do Approval Request Posts Appear?
When your org has Approvals in Chatter enabled, approval request posts appear in various Chatter feeds. To see the approval request post, you must have access to the approval record.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Approval request posts show up in these feeds.
Chatter feed of the assigned approver
Submitter’s profile
Chatter feed of the submitter if the submitter is following the approval request record
Chatter feed of the approval request record
Chatter feed of anyone following the approval request record
Object-specific filter on the Chatter feed of anyone following the approval record
Company filter of every user with access to the approval record
See Also:
What Happens When You Opt Out of Chatter Approval Requests?
Considerations for Approvals in Chatter
Let Users Respond to Approval Requests from Chatter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=approvals_for_chatter_overview.htm&language=en_US
Release
202.14
