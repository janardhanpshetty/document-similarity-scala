### Topic: This article is intended to help define and guide the type of cases that are handled by the Work.com skill group.
What is a Work.com case?
Resolution
Feature
Example Case
Recognition
Thanks/Badges
 & 
Rewards
Can I restrict badge creators?
Feedback
Ad hoc feedback including 
Offer Feedback & Request Feedback
How do I create a report to view unsolicited feedback?
Coaching
Coaching relationships
How do I transfer a coaching relationship?
Goals & Metrics
Goals
 & 
Metrics
Why can't I see my team's goals?
Performance Cycles
Performance Cycles
 and 
Summaries
 (manager, self, peer & skip)
Is it possible to edit a performance cycle after it has been deployed?
What is not a Work.com Case
Marketing Cloud Cases
 - Do not select the following GAA and FA values, as we do not provide support for the Marketing Cloud:
Case Reason:
 Work.com
General Application Area:
 General User Question
Functional Area:
 Marketing Cloud
