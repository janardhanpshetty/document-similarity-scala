### Topic: Considerations for Syncing Events | Salesforce
Considerations for Syncing Events | Salesforce
Considerations for Syncing Events
Before you roll out Lightning Sync to your sales reps, familiarize yourself with these considerations for syncing events. That way, your rollout generates no surprises.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Lightning Sync can sync your sales reps’ events from their Microsoft® Exchange-based calendars to Salesforce. You decided whether reps sync events by setting up the feature from Lightning Sync configurations in Setup.
Keep in mind that there’s potential for Lightning Sync to overwrite event data unless you’re considerate about how you roll out the feature to your reps.
First, does one of these scenarios describe your rollout?
You’re migrating reps from Salesforce for Outlook
Your reps edit their events records in Salesforce
Your reps already have access to Lightning for Outlook (previously known as Salesforce App for Outlook)
For example, say that your rep maintains meeting notes in Salesforce event records. But the matching event in that rep’s Exchange-based calendar doesn’t include those notes. During the first sync, Lightning Sync overwrites the event in Salesforce with the one from the calendar.
If one of these scenarios describes your rollout scenario, you can avoid overwriting event data by following our 
Guidelines for Syncing Events
.
Guidelines for Syncing Events
Before you roll out Lightning Sync, address these guidelines to avoid overwriting event data during certain sync scenarios.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=exchange_sync_admin_considerations_events.htm&language=en_US
Release
202.14
