### Topic: Auto-Response From email address enforced for security purposes.
Administrators may notice post release Winter '15, Auto-Response 'From' email addresses must now be a valid authenticated email address.  
Previously, there were no requirements for this address, which meant that deceiving and potentially malicious emails could be sent out from a Salesforce Organization.
 
Resolution
The email address shown in the From line on auto-response messages now must be one of your verified organization-wide addresses, 
or the email address in your Salesforce user profile.  
