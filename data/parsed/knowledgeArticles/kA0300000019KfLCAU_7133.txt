### Topic: This is the process for increasing the maximum uploaded file size attachment limit.
Available in: Contact Manager, Group Edition, Professional Edition, Enterprise Edition, Performance (AKA - Unlimited) Edition, Developer Edition, Data.com
This feature controls the maximum size of the attachment uploaded using related list, Notes and Attachments. 
See also 
File Size Limits in Salesforce
 for the maximum file size limits in Salesforce.
Resolution
If you have reviewed the above documentation and would like to proceed with increasing the limit, please have the system administrator contact Salesforce with the following information;
1. What is Organization ID of the production or sandbox?  (Navigate to Setup > Company Profile > Company Information)
 
2. Are you the system administrator authorized on behalf of this organization to request this feature?
3. What is the requested limit?
Note: The default limit is 25MB. Support can increase the limit up to 65MB. Attachments larger than 36MB can be attached only via User Interface.
