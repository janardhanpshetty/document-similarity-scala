### Topic: Considerations for Customizing the Task List in Lightning Experience | Salesforce
Considerations for Customizing the Task List in Lightning Experience | Salesforce
Considerations for Customizing the Task List in Lightning Experience
Use compact layouts to customize the display and order of fields for items in the task list. However, certain fields remain in the task list even if you remove them from a layout, because they contain essential task information. For example, suppose that you remove the status and due date fields from a compact layout. The task checkbox and the due date still appear on tasks in the list. The remaining fields visible in the list reflect the fields you include in the compact layout.
Available in Lightning Experience in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=tasks_list_customization_considerations_lex.htm&language=en_US
Release
202.14
