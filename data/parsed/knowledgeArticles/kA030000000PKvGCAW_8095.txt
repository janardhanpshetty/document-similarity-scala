### Topic: Search indexers may fall behind due to a number of reasons. This article shows what Backline can check if this happens.
If search indexers fall behind, and Site Reliability contact Backline about it, what can Backline check?
Resolution
Is a clone in progress?
 
An index clone operation locks the index so indexing will fall behind during the clone. 
A clone can occur  either for an off-peak backup of a large index or from a sandbox refresh. 
Use BT > Search Status > Search Audit Trail to see if an index clone is in progress. 
If the clone is from the off-peak backup process, check on how long the last backup took to help set expectations. 
CCE can't terminate a clone operation, but if it's got a long way to go AND there is already a fairly current backup, then maybe DBOps can.
Is a lot of time being spent in partition splits?
 
Use BT > Search Status > Search Audit Trail to see if a lot of partition splits are being attempted and/or are in progress and/or are failing. 
Check Splunk (serverType=indexer) for gacks.
Is there a lot of bulk indexing activity? 
Bulk indexing (either from a customer's bulk upload or initiated internally from the BT > Search Manager page) is enqueued on a separate queue (the BulkBT indexing queue, priority=1000), but a lot of bulk work can slow down incremental and offload indexing when a batch of work from the BulkBT queue ends up locking the index. 
Check the BT > Search Status > Indexer Queue Diagnostic page. 
CCE or the Search Support Escalation rotation can temporarily prioritize certain entity types if the customer is really hot for certain entity types to get caught up.
Use Splunk to look at (logRecordTypeI=1)
. In addition to looking at fields like numberRecords, avgQueueTime and maxQueueTime, check the fetch time, store time and delete time for any anomalies.
