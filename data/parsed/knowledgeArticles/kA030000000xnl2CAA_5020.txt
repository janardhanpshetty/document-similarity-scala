### Topic: How to resolve the 'This summary can't be opened at this time. Please contact your administrator if access is necessary' error.
Some users may see a 'This summary can't be opened at this time. Please contact your administrator if access is necessary' error if they try to access a summary.  This would typically occur after clicking on the 'View Summary' link in an email notifying them that a summary has been shared with them.
Resolution
This error will occur if the user does not have privileges to view the summary or if it is still in the draft status.  If they do have the appropriate access and the summary was submitted it's possible that it was reopened.  If that's the case after it has been resubmitted the user should have access again.
