### Topic: If you get questions about Salesforce BCR, here are the talking points, and what can be discussed with customers.
Resolution
To Open
At Salesforce, trust is our #1 value and nothing is more important than the success of our customers and the privacy of our customers’ data.
Salesforce today announced that its privacy program has achieved the highest level of approval recognized by European data privacy authorities. Salesforce is the only top 10 software company to have achieved approval by European data privacy authorities for its binding corporate rules (BCR).
Summary
BCRs are company-specific, group-wide data protection policies approved by European data protection authorities to facilitate transfers of personal data from the Europe to other countries.
BCRs are widely considered the highest standard of data protection because they are based on strict principles established by EU data protection authorities and require intensive consultation with, and approval by, European data protection authorities.
Salesforce is the only top 10 software company in the world to achieve approval for its BCR.
Salesforce customers now have the assurance of both model clauses and BCRs as legal mechanisms for transferring personal data from Europe.
If you would like to incorporate the BCR into your Salesforce customer contract, you may access the data processing addendum including both the BCR and the model clauses at 
http://www2.sfdcstatic.com/assets/pdf/misc/data-processing-addendum.pdf
.
Additionally, we have a FAQ about our BCR available at 
http://www.salesforce.com/company/privacy/data-processing-addendum-faq.jsp
.
To Close
I’m happy to answer any questions you might have. I might not have all the answers at this time, but I’ll do my best to get you an answer as soon as possible.
FAQ
How does the Salesforce BCR benefit my company?
At Salesforce, our #1 value is our customers’ trusted success. The Salesforce BCR gives our customers additional assurance that the Salesforce privacy program meets the highest standards recognized by European authorities. The Salesforce BCR is an approved mechanism to validate transfers of data from Europe.
Do I have to sign the new data processing addendum incorporating the Salesforce BCR?
No, you do not. We are making this available to you if your organization would like to benefit from the protections offered by the Salesforce BCR.
What is the scope of the Salesforce BCR?
The Salesforce BCR applies to data submitted to Salesforce services branded as Sales Cloud, Service Cloud, Chatter, Communities, and Force.com. Customers using Salesforce services not within the scope of the Salesforce BCR, as well as customers who are in jurisdictions that do not yet recognize BCRs or who have not completed locally required formalities, may continue to use the European Commission’s standard contractual clauses, commonly known as “model clauses,” to legalize the international transfer of European personal data to Salesforce services. The model clauses continue to be incorporated into Salesforce’s 
data processing addendum
 for these situations. The model clauses also continue to apply for customers whose contracts already include the model clauses.
What about your other services? Why aren’t they covered by the Salesforce BCR?
At this time, Salesforce services branded as Sales Cloud, Service Cloud, Chatter, Communities and Force.com are covered by the Salesforce BCR. We intend to add other services over time. Customers using Salesforce services not within the scope of the Salesforce BCR, as well as customers who are in jurisdictions that do not yet recognize BCR or who have not completed locally required formalities, may continue to use the European Commission’s standard contractual clauses, commonly known as “model clauses,” to legalize the international transfer of European personal data to Salesforce services. The model clauses continue to be incorporated into Salesforce’s 
data processing addendum
 for these situations.
How does my company incorporate the Salesforce BCR into my Salesforce contract?
Salesforce’s 
data processing addendum
 has been updated to incorporate the Salesforce BCR. Customers may complete, sign and return the data processing addendum to 
dataprocessingaddendum@salesforce.com
.
What additional steps does my company need to take to benefit from the Salesforce BCR?
In addition to completing, signing and returning Salesforce’s data processing addendum, Salesforce’s customers may need to complete specific formalities with their data protection authorities under applicable local law. If you have questions about this, I’m happy to direct you to an expert.
Does the recent European Court of Justice decision regarding the EU-US Safe Harbor Framework impact the Salesforce BCR?
No. On October 16, 2015, the Article 29 Working Party confirmed that both model clauses and BCRs remain valid legal mechanisms for transferring personal data from the EU. This has also been reiterated by the European Commission in a November 4, 2015 communication to the European Parliament and the Council of the EU.
What is the difference between BCRs and the model clauses?
Both BCRs and model clauses are valid legal mechanisms for transferring personal data outside of the EEA. We have a FAQ document that addresses this issue in more detail. If you have further questions on this topic, please let us know, and we’ll connect you with an expert member of our team.
How is this relevant if I’m using Salesforce’s European data centers?
Even if you are utilizing Salesforce's European data centers, please note that Salesforce may transfer your organization's data to the US for temporary, non-storage purposes to provide you with customer support and to ensure the services are working properly.
