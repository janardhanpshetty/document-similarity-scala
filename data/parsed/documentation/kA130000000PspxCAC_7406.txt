### Topic: Set Dashboard Properties in Accessibility Mode | Salesforce
Set Dashboard Properties in Accessibility Mode | Salesforce
Set Dashboard Properties in Accessibility Mode
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create dashboards:
“Run Reports” AND “Manage Dashboards”
To create, edit, and delete dynamic dashboards:
“Run Reports” AND “Manage Dynamic Dashboards”
To enable choosing a different running user for the dashboard:
“View My Team's Dashboards”
 OR 
“View All Data”
Important
This topic applies only if you're 
not
 using the dashboard builder. 
Dashboard builder
 is a drag-and-drop interface for creating and modifying dashboards.
To set dashboard properties:
Edit a dashboard
 and click 
Dashboard Properties
.
Do the following:
Enter a title and description for the dashboard.
If you have the “Customize Application” permission, enter a unique name to be used by the API and managed packages.
Select the number of columns for this dashboard. Each dashboard can have two or three columns.
Important
Before removing a column, move its components to another column; otherwise, they may not be visible.
Select a folder to store the dashboard. The folder should be accessible by all of your intended viewers.
Choose the 
Dashboard Running User
 to set visibility settings for the dashboard:
Select 
Run as specified user
 and set the 
Running User
 field to show all dashboard users the same data, regardless of their personal security settings. If you don’t have “View All Data,” you can only choose yourself.
Select 
Run as logged-in user
 to show data to each user according to his or her access level.
Set the 
View Edit Page as
 field to preview the dashboard edit page from the point of view of the selected user.
If you have the “View My Team's Dashboards” or “View All Data” permission, select 
Let authorized users change running user
 to enable those with permission to change the running user on the dashboard edit page. Users with “View My Team's Dashboards” can view the dashboard as any user below them in the role hierarchy. Users with “View All Data” can edit the dashboard and view it as any user in their organization.
Under Component Settings, select the title color and size, text color, and background fade. If you don't want a gradient, choose the same color for both 
Starting Color
 and 
Ending Color
.
Click 
Save
.
Parent topic:
 
Edit Dashboards in Accessibility Mode
Next topic:
 
Adding and Editing Dashboard Components in Accessibility Mode
See Also:
Set Up Dynamic Dashboards
Choose a Dashboard Running User
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dashboards_setting_properties.htm&language=en_US
Release
202.14
