### Topic: Some customers may have noticed that their weekly exports occasionally take some time to complete, they remain in the status of pending and then finally move to processing before they complete and are available to download, in this article we try to understand very briefly the current architecture of our data export queues.
INTERNAL  - PLEASE DO NOT SHARE WITH CUSTOMERS
Some customers may have noticed that their weekly exports occasionally take some time to complete, they remain in the status of pending and then finally move to processing before they complete and are available to download, in this article we try to understand very briefly the current architecture of our data export queues.
 
Resolution
Heavy traffic can delay an export delivery. For example, assume that you schedule a weekly export to run until the end of the month, beginning April 1. The first export request enters the queue, but due to heavy traffic, the export isn’t delivered until April 8. On April 7, when your second export request is scheduled to be processed, the first request is still in the queue, so the second request isn’t processed until April 14.
[
Note
: T
here is no SLA for Data Exports
]
Data Exports follow FIFO just like Sandboxes but instead of queues, we're basically looking at CRON jobs.
Here are the list of CRON Jobs for the export queue on any given instance:
WeeklyExportTiny
WeeklyExportTiny2
WeeklyExportSmall
WeeklyExportSmall2
WeeklyExportMedium
WeeklyExportMedium2
WeeklyExportBig
WeeklyExportBig2
WeeklyExportGiant
WeeklyExportGiant2
You can find the above by going to 
Radio (PAL) > Instance> Scheduled Tasks> Search for "Export"
 and you'll see the above. [ 
You need to be an internal employee in order to access PAL/Radio
]
Each CRON job is responsible for processing "one org at a time" and as you can see 2 cron jobs are allocated based on the size, so orgs that are "Tiny" are queued in the "Tiny" Queue and from there 2 orgs from this queue are processed at the same time by the the following cron jobs:
WeeklyExportTiny
WeeklyExportTiny2
This is your typical sandbox queue setup, where small, medium, large queues process orgs based on the size of the queue.
Here are some approximates on the size/queue ratio:
tiny < 1536 (M)
1536< small < 3072
8192<big<30720
3072<bigger128000
3072<bigger<128000
128000<giant
[
Note: 
These are on approximates
]
For more information on Weekly Exports, please review the following documentation, 
Exporting Backup Data 
