### Topic: Changing the owner on the lead convert string is not triggering workflow rules on the opportunity object even when the criteria is met.
When getting ready to convert a lead changing the owner in the convert screen is not triggering the opportunity workflow rules even though the opportunity meets the criteria. This is currently working as designed based on how the order of operations is being triggered.
Resolution
This is working as designed. The check on the owner fields will evaluate the current user (user converting the Lead) and not the newly selected owner during the lead convert process. The Opportunity is first created, then the page renders, opportunity contact created, triggers and workflow rules evaluated, owner change committed. 
 
