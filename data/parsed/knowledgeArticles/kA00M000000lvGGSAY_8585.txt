### Topic: Validation Status
Whenever the user taps in an object's field capable of accepting input within Salesforce1, the iOS device display's an appropriate keyboard. Depending on the type of the field the iOS system might display one of several different keyboards.
On iOS Keyboard, we can see Backward (<) and Forward (>) arrows, which assist in navigation on the Page and we can jump to the next fields without being manually tapping on each field to provide an input. Though this provide an interactive User experience and ease, but there are limitations on Salesforce1 field types, which cannot be selected while using the arrow keys.
Limitations on Salesforce1 fields supported by iOS Keyboard arrows:
1. Read-Only fields cannot be selected.
2. Lookup, Checkbox, Date/Time and Picklist (Multi-Select) fields are skipped.
3. Picklist fields which are dependent will be skipped unless there is a valid value populated on Controlling fields.
Resolution
Tap on the fields from finger which are not selectable from arrow keys. If there are any Dependent Picklist, then we recommend selecting the value on Controlling Picklist field first and tap on 'Done' button available on the Keyboard, and then proceed to the Dependent Picklist field.
The reason why Dependent picklist field cannot be selected as it's displayed as 'Read-Only' and the arrow keys doesn't identify it as active during selection. If we try to select the value on the Controlling picklist field by moving the scroller on the keyboard to the desired value and tap on the Next (>) arrow, it jumps to the next active field instead of going to the Dependent picklist field because while tapping on the Next (>) arrow, there were still no valid value populated. Though, if we go back, we can find that the value from the scroller is defaulted on the Controlling picklist field and the Dependent picklist field is now active.
This behaviour is Working as Designed from Salesforce, where we do not control the iOS Keyboard actions which is designed by Apple.
 
