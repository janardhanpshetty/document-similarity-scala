### Topic: Article explaining why the subscribed reports differ from actual number of subscribed reports.
When you click the Reports tab and filter the view by 
"Items I'm Subscribed to"
, you will notice that the actual number of reports is different from the subscribed reports value on the bottom left of the view.
See example below:
The above image shows you have subscribed to 5 reports however the list only has 3 reports.
Resolution
This is because the report was transferred into a folder that you do not have access to.
In this case, you will need to reach out to either the 
Report Owner
 or your 
System Administrator
. 
