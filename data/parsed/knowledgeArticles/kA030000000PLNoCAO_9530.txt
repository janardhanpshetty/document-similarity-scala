### Topic: How is the "Restrict Email Domain" Perm in user set up different from the "Allowed email domains" in BT
Some Admins report that they are able to create users with email domains not listed in the Allow email domains section in user set up.(Manage users > Allowed email domains)
Resolution
This is caused when both the perms "Restrict Email Domain" and "Allow email domains"(In BT) are enabled. Note that the Allow email domains is linked to the "Disabled email change verification" perm.
Enabling the feature "Restrict Email Domain" lets the admins in the Org control the emails domains allowed during user creation. Once this perm is enabled in BT, the admin can add domains to allow from Manager users > Allowed email domains
Points to consider :
_______________
The Email field for existing users doesn’t have to comply with the whitelist. However, if you edit an existing user, you must update the Email field to match a whitelisted email domain.
 - >> How is this perm different from the Allowed domain list which is managed through BT by SFDC support?
 - The Allowed domain list(Managed through BT) is linked to the "Disable email change notification" perm and is only active when email change notification is disabled.
- >> What are the results of Having the Allowed domain list(In BT) and the "Restricted email domain" both enabled ?
 - In this Scenario, The domains allowed through BT in the Allowed domain list will override the domains which the customer has allowed through set up.
Please refer to activation database on Enabling the perms above..
