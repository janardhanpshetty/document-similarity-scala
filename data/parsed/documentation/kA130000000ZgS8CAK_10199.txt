### Topic: Live Agent Transcripts | Salesforce
Live Agent Transcripts | Salesforce
Live Agent Transcripts
A Live Agent transcript is a record of a chat between a customer and an agent. Salesforce automatically creates a transcript for each chat session.
Available in: Salesforce Classic
Live Agent is available in: 
Performance
 Editions and in 
Developer
 Edition orgs that were created after June 14, 2012
Live Agent is available in: 
Unlimited
 Edition with the Service Cloud
Live Agent is available for an additional cost in: 
Enterprise
 and 
Unlimited
 Editions
When a chat ends successfully—that is, when the chat is ended by a customer or an agent—the chat transcript is created as soon as the agent closes the chat window and any related tabs.
If a chat is disconnected or experiences another error, Salesforce still creates a chat transcript for it, though it takes up to 30 minutes to creates the transcript after the chat is disconnected.
You can associate a transcript with cases, accounts, contacts, and leads, or you can link it to other objects.
Note
If you have the correct permissions, you can create, view, edit, or delete chat transcripts, just like other record types in Salesforce. However, chat transcripts are meant to provide a paper trail for the chats between your agents and customers, so we don't recommend tampering with these records in most cases.
Live Agent Transcript Fields
Live Agent transcript fields help you track information about your agents’ chats with customers.
Live Agent Transcript Events
Live chat transcript events automatically track events that occur between your agents and customers during chats.
See Also:
Create Records
Delete Records
Edit Records
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=live_agent_chat_transcripts.htm&language=en_US
Release
202.14
