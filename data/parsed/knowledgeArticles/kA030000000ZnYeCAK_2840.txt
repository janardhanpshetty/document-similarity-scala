### Topic: Learn how to complete a Fiddler Trace for Microsoft Dynamics CRM Version 2011.
Fiddler is a tool that is often utilized by Marketing Cloud Support to capture and view the errors being produced in the MSCRM Integrations.  This trace needs to be performed on your end and the produced logs sent to Support for investigation.
Resolution
Good to know:
 It's a prerequisite to set-up Fiddler. 
 
Set up Fiddler
 
1. Download and install Fiddler from 
http://www.fiddler2.com/
.  
Important
: This step must be completed because the traffic from CRM to Marketing Cloud utilizes HTTPS.
2. Open up Fiddler, go into the 
Tools
 | 
Fiddler Options
 | 
HTTPS tab, 
and
 
make sure that the 
Capture and Decrypt
 options are checked.
3. After enabling those options, you'll be prompted to install a Certificate. You must install this certificate to perform a Fiddler trace or the trace will fail.
 
Capture the Error Produced within the MSCRM to Marketing Cloud Communications
 
1. Close all other browser windows, email clients and other programs that use the Internet to cut down on network traffic noise (Don't close the browser window MSCRM is using to produce the error!).
2. Go into MSCRM and recreate the issue, verifying Fiddler is capturing traffic in the Web Sessions window.
3. When the issue has occurred, go into Fiddler, and then click  
File |
 
Save |
 
All Sessions
.
4. 
Save
 the session file (*.saz file).
5. Send this .saz file to the Support analyst who requested it.  Please note, if the file is 8MB or larger, you will need to send a different way (ie, dropbox, FTP, etc..) as the attachments will get stripped prior to getting to the case owner.
