### Topic: Manage Salesforce Classic Mobile Views | Salesforce
Manage Salesforce Classic Mobile Views | Salesforce
Manage Salesforce Classic Mobile Views
Salesforce Classic Mobile setup available in: both Salesforce Classic and Lightning Experience
Mobile app available in: 
Performance
, 
Unlimited
, and 
Developer
 Editions, and for an extra cost in: 
Professional
 and 
Enterprise
 Editions
User Permissions Needed
To view Salesforce Classic Mobile devices and users:
“View Setup and Configuration”
To manage Salesforce Classic Mobile custom views:
“Manage Mobile Configurations”
To manage the custom views for a Salesforce Classic Mobile configuration, from Setup, enter 
Salesforce Classic Configurations
 in the 
Quick Find
 box, then select 
Salesforce Classic Configurations
. Then click the name of the mobile configuration and scroll down to the Mobile Views related list.
To see a list of all your custom views, choose All Objects in the 
Select an object
 drop-down list. You can also use the Select an object drop-down list to filter the views by object type.
To 
create a new mobile view
, select the object type from the Select an object drop-down list, and then click 
New Mobile View
.
To make changes to a custom mobile view, click 
Edit
 next to the view name.
To delete a mobile custom view, click 
Del
 next to the view name.
To view details about a mobile custom view, click its name.
See Also:
Salesforce Classic Mobile Overview for Administrators
Manage Salesforce Classic Mobile Configurations
Manage Salesforce Classic Mobile Devices
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=managing_mobile_views.htm&language=en_US
Release
202.14
