### Topic: Using Salesforce-Managed Values in Auth. Provider Setup | Salesforce
Using Salesforce-Managed Values in Auth. Provider Setup | Salesforce
Using Salesforce-Managed Values in Auth. Provider Setup
You can choose to let Salesforce automatically create key values when setting up a Facebook, Salesforce, LinkedIn, Twitter, or Google Auth. Provider. This allows you to skip the step of creating your own third-party application.
Available in: Lightning Experience and Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view the settings:
“View Setup and Configuration”
To edit the settings:
“Customize Application”
AND
“Manage Auth. Providers”
When you choose to use Salesforce-managed values in your Auth. Provider setup, Salesforce uses its own default application in the background from which it generates the values, eliminating the need for you to create your own application.
To use Salesforce-managed values, leave all of the following fields blank if they display in your Auth. Provider setup.
Consumer Key
Consumer Secret
Authorize Endpoint URL
Token Endpoint URL
User Info Endpoint URL
Default Scopes
If you specify a value for one of the preceding fields, then that indicates that you are using your own third-party application or connected app and you must specify values for the 
Consumer Key
 and 
Consumer Secret
.
Example
Suppose you want to set up single sign-on using a LinkedIn authentication provider to enable login to Salesforce with LinkedIn credentials. You can skip creating a LinkedIn application, since you choose to use Salesforce-created values in Auth. Provider setup. Next, you define the LinkedIn authentication provider in your organization and test the connection using the procedure in 
Configure a LinkedIn Authentication Provider
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sso_provider_global_auth.htm&language=en_US
Release
202.14
