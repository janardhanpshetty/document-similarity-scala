### Topic: Validation Status
As of Spring '16 Lightning Experience, Related Lists on the initial record will only show a few fields.  This is expected behavior within Lightning Experience at the moment.  
Resolution
To view all available fields for the Related Lists for a record, click View All and the missing fields will display.
