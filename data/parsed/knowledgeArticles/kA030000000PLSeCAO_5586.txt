### Topic: The scenario and problem described in this article was related to a password change for the administrator user specified during the Identity Connect setup. The Issue, Troubleshooting and Resolution sections are below. Even though the actual cause of the issue as stated above the process detailed below will go through additional checks related to the error leading up to what the actual issue is. The reason for this is because the error could be related to other issues as well as detailed in the Troubleshooting section.
The scenario and problem described in this article was related to a password change which occurred for administrator user used to bind to Active Directory which is specified during the Identity Connect setup.  
 
This article is broken up by the sections below which are 
Issue, Troubleshooting and Resolution
.
Even though the actual cause of the issue as stated above the process detailed below will go through additional checks related to the error leading up to what the actual issue is. The reason for this is because the error could be related to other issues as well as detailed in the Troubleshooting section.
 
 
Resolution
Problem upon trying to login to the Identity Connect administrator the following error is received:
 
(errorLogin/password combination is invalid) even though AD login is valid)
 
 
 
Troubleshooting:
 
Step 1
Look at the example screenshot above check and make sure the user you are trying to login as 
admin
, is a Active Directory user and that you are using the correct Active Directory 
User logon name
. You will have access the Active Directory tree which can be found in Windows under 
Control Panel>>System and Security>>Administrative Tools>>Active Directory Users and Computers
.  Find the user in the tree and right click on the user select 
Properties
 and select the 
Account
 tab. Here you will see the 
User logon name
 which is 
admin
 in this example. Verify the 
User login name
 is correct and verify the password being used is correct. See the example screenshot below.
 
Step 2
 
One it had been verified in the 
Step 1, t
hat the 
User login name
 and password are correct the next step is to make sure this user belongs to the proper group in Active Directory.
During the setup of Identity Connect after setting up the administrator user which acted is a bind DN for the Active Directory server you would have been prompted to specify the groups Active Directory users need to be member of in order to be able to login to the Identity Connect administrator.
 
To check what this group after setup of Identity Connect go to the directory where Identity Connect was installed and navigate to the conf directory for example:
 
/salesforceIdConnect/conf
 
Open the 
authentication.json
 file and navigate to the 
allowedGroups
 section.   Here you will find the group for which Active Directory users must be a member of in order to login to the Identity Connect administrator. Look at the example below, in this example Active Directory user must me a member of the 
Adm_users
 group in order to be able to login to the Identity Connect administrator.
 
"allowedGroups" : [
        "CN=
Adm_users
,OU=Groups,DC=testsync,DC=salesforce,DC=com",
           ],
 
The next thing to check is to make sure the user you are trying to login as a Identity Connect administrator is a member of the 
Adm_users
 group. As stated earlier in this article the example user we have been using is named 
admin
. You will have access the Active Directory tree which can be found in Windows under 
Control Panel>>System and Security>>Administrative Tools>>Active Directory Users and Computers
.  Find the user in the tree and right click on the user select 
Properties
 and select the 
Member Of
 tab. Here you will see the group the 
admin
 user belongs to. See the example screen shot below:
 
See the example screenshot below you will notice the admin user is a member of the Adm_users group.
 
 
 
At this point it has been  verified the 
admin
 user is a Active Directory user and the 
Logon user name
 in Active Directory 
admin
 is the value being used to login to the Identity Connect administrator . It has also been verified the that the 
admin
 user is a member group in Active Directory as specified in Identity Connect setup as the group which users must be a member of  in order to login to the Identity Connect administrator which is 
Adm_users
. So based on these set of checks everything should be fine related to the 
admin
 user so some other problem is occurring.
 
Step 3
 
The next check would be to examine the debug log files upon Identity Connect startup and after attempting to login to the Identity Connect administrator and receiving the 
errorLogin/password combination is invalid 
error.
 
Navigate to the following directory
 /salesforceIdConnect/logs
 and open the 
console.out
 and 
openidm0.log.0
 files. In this example, upon inspection of these log files the following two errors were seen:
 
1.
Failed pass-through authentication of james.sync on system/AD/account
 
2.NFO: OpenICF ConnectorInfo of ConnectorKey( bundleName=org.forgerock.openicf.connectors.ldap-connector bundleVersion=1.1.1.0-r6442 connectorName=org.identityconnectors.ldap.LdapConnector ) on host: #LOCAL was found.
Dec 10, 2013 1:06:59 PM org.identityconnectors.framework.impl.api.local.LocalConnectorFacadeImpl getPool
INFO: Method: getPool2 Creating new pool: ConnectorKey( bundleName=org.forgerock.openicf.connectors.ldap-connector bundleVersion=1.1.1.0-r6442 connectorName=org.identityconnectors.ldap.LdapConnector )
Dec 10, 2013 1:06:59 PM org.forgerock.openidm.provisioner.openicf.impl.OpenICFProvisionerService init
SEVERE: OpenICF 
connector test of SystemIdentifier{ uri='system/AD/'} failed!
org.identityconnectors.framework.common.exceptions.ConnectorSecurityException: javax.naming.AuthenticationException: [LDAP: error code 49 - 80090308: LdapErr: DSID-0C0903A9, comment: AcceptSecurityContext error, data 52e, v1db1
 
Looking over the errors this error gave a clue to what was going on:
 
connector test of SystemIdentifier{ uri='system/AD/'} failed!
 
This error would mean that the administrator user specified is a bind DN for the Active Directory server during setup had a problem. This is the Active Directory user the Identity Connect server users to commute with Active Directory.
 
The administrator user specified is a bind DN for the Active Directory server must have domain administration rights to all of the base contexts that will be managed by Identity connect, see the 
IC Users Guide under the section titled Configuring the Active Directory Connector
 for more info. Below is a screen hot of the setup screen where this user is specified. In the example screen shot the user specified is 
James Shreckengost
.
 
 
 
After the setup of Identity Connect you can check administrator user specified for the bind DN for the Active Directory server. Go to the directory where Identity Connect was installed and navigate to the conf directory for example:
 
/salesforceIdConnect/conf
 
Open the 
provisioner.openicf-ldap.json
 file search for the text 
principal
. Below is a example of this value notice it is the same user as in the screenshot shown above as administrator 
James Shreckengost
.
 
"principal" : "CN=
James Shreckengost
,OU=
Admin Accounts
,OU=
Resource Accounts
,DC=testsync,DC=Salesforce,DC=com",
 
The first thing to verify is the user specified 
James Shreckengost
 exists in Active Directory in the correct location. The value for 
principal
 in the above example is what is referred to in Active Directory as a 
distinguished name
. Based on the 
distinguished name
 given in the example above, in the Active Directory tree we should find a user named 
James Shreckengost
 within the Active Directory tree under 
Resource Accounts>>Admin Accounts
.
 
To verify the setting in Identity Connect is correct access the Active Directory tree which can be found in Windows under 
Control Panel>>System and Security>>Administrative Tools>>Active Directory Users and Computers
.   Navigate to the path and user as specified for the 
principal
 in the Identity Connect configuration as in the above example. In this example you will see in the Active Directory tree the user 
James Shreckengost
 does exist under 
Resource Accounts>>Admin Accounts
.
 
 
The example screen shot and values above show the user specified as the 
principal
 in the Identity Connect setup does exist in Active Directory and under the correct location in the Active Directory tree.
 
Resolution:
 
Up this point is has been verified the Administrator specified in the Identity Connect setup does exists in Active Directory and is located in the correct location in the Active Directory tree.
 
The only other issue there could be is that the Active Directory password for the user (James Shreckengost) changed. And in this scenario this is what caused the problem.  Identity Connect has to store the credentials for this user 
James Shreckengost
 and is the only user it does this for. The reason as mentioned before is because this is the user Identity Connect uses at startup to bind with Active directory. The resolution for this scenario is to manually add the new password which Identity Connect will encrypt upon startup for the user in this example 
James Shreckengost
.
 
Go to the directory where Identity Connect was installed and navigate to the conf directory for example:
 
/salesforceIdConnect/conf
 
Open the 
provisioner.openicf-ldap.json
 file search for the text 
credentials
 below is a example of this value.
 
 "credentials" : {
            "$crypto" : {
                "value" : {
                    "iv" : "8mh0zUnOK46DKQfUaX8sjw==",
                    "data" : "EsgBlkau0th1VTcaDpxUBQ==",
                    "cipher" : "AES/CBC/PKCS5Padding",
                    "key" : "openidm-sym-default"
                },
                "type" : "x-simple-encryption"
            }
        },
 
Shutdown the Identity Connect server and replace the section mentioned above in the 
provisioner.openicf-ldap.json
 with the following:
 
        "credentials" : "YOUR_NEW_PASSWORD",
Save the file and restart the Identity connect server and upon startup it will read the password you specified and encrypt it and use it when establishing a connection with Active Directory. 
