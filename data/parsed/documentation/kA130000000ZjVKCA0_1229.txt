### Topic: Enable Records in Chatter Groups | Salesforce
Enable Records in Chatter Groups | Salesforce
Enable Records in Chatter Groups
Records are allowed in groups by default. Customize the group publisher to include the Add Record action, so users can add records to groups.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
The Add Record action on the group publisher lets users add 
account, contact, lead, opportunity, contract, campaign, case, and custom object
 records to groups. The Add Record action isn’t available by default, so you must configure the group publisher to include this action.
Walk Through It: Allow Users to Add Records to Chatter Groups
We recommend that you also customize object layouts to include the Groups related list on record detail pages. Customize the layout of all the standard and custom objects in your organization that support group-record relationships. The Groups related list shows users the list of groups associated with the record.
Walk Through It: Add the Groups Related List to Records
If you don’t want to users to add records to groups, enter 
Chatter
 in the 
Quick Find
 box in Setup, then select 
Chatter Settings
, and deselect 
Allow records in groups in the Groups
 section. Disabling the feature hides the Group Records list on the group detail page and the 
Add Record
 action in the group publisher.
See Also:
Customize the Chatter Group Layout and Publisher
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=collab_group_records_enabling.htm&language=en_US
Release
202.14
