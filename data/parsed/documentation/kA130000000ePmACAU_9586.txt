### Topic: Compare Features Available in the Community Templates | Salesforce
Compare Features Available in the Community Templates | Salesforce
Compare Features Available in the Community Templates 
Thinking about using a template to build your community? There are major differences between the features available in each community template. Before you decide on which template to use, compare them.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Feature Comparison
Koa
Kokua
Napili
Salesforce Tabs + Visualforce
All Salesforce Objects
Accounts
Campaigns
Cases
Community Builder
Community Discussions
Contacts
Contacts to Multiple Accounts
Custom Objects
Customizations using Lightning Components
Customizations using Visualforce
Salesforce Knowledge
Required
Required
Recommended
Recommended
Knowledgeable People
Leads
Notes
Opportunities
Optimized for Mobile
Orders
Question-to-Case
Recommendations
Reputation
Topics
Trending Articles
See Also:
Implementation Guide: Using Templates to Build Communities
Implementation Guide: Getting Started with Salesforce Communities
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_template_feature_comparison_table.htm&language=en_US
Release
202.14
