### Topic: Events created via Cloud Scheduler don't move to Activity History after the date/time of the meeting has passed.
Why don't my events move to the Activity History related list after the meeting date/time has passed?
Resolution
This behavior is seen with events created via Cloud Scheduler under the following circumstances:
proposed dates/times are sent to the invitee, and
the invitee responds with times they are available, 
but
the meeting organizer fails to select and confirm one of the invitees available dates/times within the Open Activity record  
This results in the record remaining in the Open Activities related list and displaying "In the past" next to the "Select One" field.  This is considered expected behavior and the meeting organizer is presented with the following options when this occurs:
reschedule the meeting
send an update to the invitee
cancel the meeting 
