### Topic: When merging accounts of which one or both are enabled for customer / partner portal, a user receives an Insufficient Privileges error message. However the user has full access to all accounts in question.
When merging accounts of which one or both are enabled for customer / partner portal, a user receives an Insufficient Privileges error message. However the user has full access to all accounts in question.
Resolution
This can occur when the user that is merging the accounts does not have the Manage Users profile permission. Merging a portal activated account also has to update the related portal users. Thus the Manage Users profile permission is required for merging such accounts.
