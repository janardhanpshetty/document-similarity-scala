### Topic: This error is browser related and the article has link to supported browser and fixes around the same
Issue:
User sees the following error after clicking on a Work.com tab
Fix:
This error usually means the user is using an unsupported web browser.  For a full list of supported browsers please see: 
Supported Browsers
If the user is using a supported version of Internet Explorer and is still getting the error double-check that they have disabled 
Compatibility View
. Typically, clicking on the broken page icon on the web browsers location bar resolves the issue:
Resolution
