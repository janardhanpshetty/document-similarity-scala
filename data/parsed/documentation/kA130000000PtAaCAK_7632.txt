### Topic: About Field Sets | Salesforce
About Field Sets | Salesforce
About Field Sets
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
A field set is a grouping of fields. For example, you could have a field set that contains fields describing a user's first name, middle name, last name, and business title. When a field set is added to a Visualforce page, developers can loop over its fields and render them. If the page is added to a managed package, administrators can add, remove, or reorder fields in a field set to modify the fields presented on the Visualforce page without modifying any code. The same Visualforce page can present different sets of information, depending on which fields a subscriber prefers to keep. 
As an administrator, you can create or edit field sets for your organization, or 
edit any installed field set
. Field sets are available on all standard objects that support 
custom fields
, and any organization that supports creating Visualforce pages.
Fields added to a field set can be in one of two categories:
If a field is marked as 
Available for the Field Set
, it exists in the field set, but the developer hasn’t presented it on the packaged Visualforce page. Administrators can display the field after the field set is deployed by moving it from the 
Available
 column to the 
In the Field Set
 column.
If a field is marked as 
In the Field Set
, the developer has rendered the field on the packaged Visualforce page by default. Administrators can remove the field from the page after the field set is deployed by removing it from the 
In the Field Set
 column.
See Also:
Field Sets Required Bit
Create Custom Fields
Developer's Guide: Visualforce Developer's Guide
ISVforce Guide
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=fields_about_field_sets.htm&language=en_US
Release
202.14
