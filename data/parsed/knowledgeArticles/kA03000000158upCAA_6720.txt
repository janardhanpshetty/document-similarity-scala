### Topic: Explanation for being unable to install apps from the AppExchange into a sandbox where you are an administrator but not an administrator in the sandbox's related production environment and a workaround.
I need to install an app in sandbox where I am an administrator or have the required "Download AppExchange Packages" permission to 
Install a Package
 but I am not currently an administrator in the sandbox's related production org. I try to login to the AppExchange with my production login credentials, but receive the message:
Oops! Looks like you don't have permission to install apps.
To keep your Salesforce secure, only system administrators have permission to install apps. Please contact your Salesforce system administrator to install this app.
Or if you're feeling adventurous, sign up for a new free trial of Salesforce so you can install and try out the app on your own.
I have also tried to update the URL when prompted to login to read test.salesforce.com instead of login.salesforce.com, but I receive the error:
We've Encountered a Problem
We've encountered an intermittent problem. Please click back on your browser and try again. If you continue to encounter the problem, please log a case in your Salesforce org to help us solve the issue.
Is it possible to install an App in Sandbox if I am not an administrator in the sandbox's related production org?
 
Resolution
When trying to install an App from the AppExchange in Sandbox, you will be prompted to login. The login for this must be your login to production where your user record's assigned profile is either System Administrator or has the "Download AppExchange Packages" permission otherwise, you will receive the message stating that you do not have permission to install the app after clicking the "Get it Now" button.
If you try to use your sandbox login while the URL is set to login.salesforce.com it will state your username or password is incorrect and if you try to update the URL to test.salesforce.com, you will receive a message stating there was a problem with the authentication attempt.
The proper steps to install an app from the AppExchange are:
1. Search for and locate your desired app
2. Click "Get It Now" on the app's AppExchange listing page and then "Log in to the AppExchange"
3. From the login screen use the production org's login credentials for an administrator or user with the "Download AppExchange Packages" permission
4. You will be prompted to choose whether you'd like to Install in production or Install in sandbox. Select Install in sandbox | Accept the terms and conditions | Click the "Confirm and Install!" button
5. On the subsequent login screen enter your desired org's login credentials and then the corresponding Log In button
6. You will then be directed into the appropriate org to continue with the package installation steps
If you're production login credentials do not have the required permission and it is not possible for an administrator to grant you the necessary "Download AppExchange Packages" permission for your user record, you may use an alternative production org's login credentials where you do have the required permissions in order to retrieve the app or package's installation URL to then manually install the app into the desired sandbox.
 
1. If you do not have an alternative org's login credentials with the required permission you can 
Sign up for a free Developer Edition
 org in order to use that environment's login credentials
2. Ensure that you are logged out of the AppExchange in your browser
3. Repeat steps 1 - 4 in the proper steps to install an app above 
except in step 3. enter your alternative org's login credentials and in step 4. select Install in production
 instead of Install in sandbox.
4.  After clicking "Confirm and Install" you should be directed to an additional login page where you will see a URL in the browser's address bar similar to the following:
 
https://login.salesforce.com/?startURL=%2Fpackaging%2FinstallPackage.apexp%3Fp0%3D
04t30000000bqOuAAI
%26newUI%3D1%26src%3Du
A) Copy the app or package Id out of the login page's URL, it is the Id beginning with 04t as shown in bold and underlined in the URL above
B) Replace 
<PackageIdGoesHere>
 with the package's Id into the following install URL:
/_ui/core/mfpackage/install/InstallPackageLandingPage?p0=
<PackageIdGoesHere>
&src=U
For our example above, the result would be:
/_ui/core/mfpackage/install/InstallPackageLandingPage?p0=
04t30000000bqOuAAI
&src=U
6.  Copy the app's install URL you have just constructed and ensure that you log out of all active Salesforce sessions and then login to your desired sandbox where you would like to install the package.
Note:
 Make absolutely sure that you are only logged into the sandbox where you're attempting to install the package otherwise, you risk accidentally installing the package into the wrong org!
7. While logged into the desired sandbox, paste the copied portion of the package install URL after salesforce.com in your browser's address bar.
8. Follow the steps to install the package
If you have the correct permission in the target sandbox the installation should proceed as expected. However, if you do not have the required "Download AppExchange Packages" permission you can go through the installation steps but the install will fail and return the following message:
This app can't be installed.
There are problems that prevent this package from being installed.
Unable to Access the AppExchange 
You have attempted to create, upload, or download an AppExchange package without the necessary permissions. Please contact your administrator to resolve this issue.
 
