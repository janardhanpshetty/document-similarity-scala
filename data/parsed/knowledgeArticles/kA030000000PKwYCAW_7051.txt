### Topic: Some customers have observed that they are no longer able to send emails from their sandbox org after a refresh or at times after a release upgrade, when they go to the activity history related list on a particular record, they are unable to see certain standard buttons including the "Send Email" button.
Some customers have observed that they are no longer able to send emails from their sandbox org after a refresh or at times after a release upgrade, when they go to the activity history related list on a particular record, they are unable to see certain standard buttons including the "Send Email" button.
Resolution
This happens because the "Access to Send Email" setting under Deliverability is set to "System email only"
In order to fix this issue, all you need to do is change the Access level to "All Email" by navigating to the following click path
Setup>Administration Setup>Email administration>Deliverability>Changed "Access level" to All Email.
 
 
