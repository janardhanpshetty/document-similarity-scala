### Topic: What Happens When an Apex Exception Occurs? | Salesforce
What Happens When an Apex Exception Occurs? | Salesforce
What Happens When an Apex Exception Occurs?
When an exception occurs, code execution halts. Any DML operations that were processed before the exception are rolled back and aren’t committed to the database. Exceptions get logged in debug logs. For unhandled exceptions, that is, exceptions that the code doesn’t catch, Salesforce sends an email that includes the exception information. The end user sees an error message in the Salesforce user interface.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Performance
, 
Unlimited
, 
Developer
, 
Enterprise
, and 
Database.com
 Editions
User Permissions Needed
To access the Apex Exception Email Setup page
“View Setup”
To write Apex code
“Author Apex”
To use the Tooling API
“API Enabled”
Unhandled Exception Emails
When unhandled Apex exceptions occur, emails are sent that include the Apex stack trace and the customer’s org and user ID. No other customer data is returned with the report. 
Unhandled exception emails are sent by default to the developer specified in the 
LastModifiedBy
 field on the failing class or trigger. In addition, you can have emails sent to users of your Salesforce org and to arbitrary email addresses. To set up these email notifications, from Setup, enter 
Apex Exception Email
 in the 
Quick Find
 box, then select 
Apex Exception Email
. You can also configure Apex exception emails using the Tooling API object ApexEmailNotification.
Note
If duplicate exceptions occur in Apex code that runs synchronously, subsequent exception emails are suppressed and only the first email is sent. This email suppression prevents flooding of the developer’s inbox with emails about the same error. For asynchronous Apex, including batch Apex and methods annotated with 
@future
, emails for duplicate exceptions aren’t suppressed.
Unhandled Exceptions in the User Interface
If an end user runs into an exception that occurred in Apex code while using the standard user interface, an error message appears. The error message includes text similar to the notification shown here.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=code_apex_exceptions.htm&language=en_US
Release
202.14
