### Topic: Once adding a formula field that contains a reference to a custom settings and to a picklist, I receive the internal server error with stacktrace (1213319076)
When I add to my report a formula field that contains a reference to a custom settings and to a picklist, I receive the error:
An internal server error has occurred
An error has occurred while processing your request. The 
salesforce.com
 support team has been notified of the problem. If you believe you have additional information that may be of help in reproducing or correcting the error, please contact Salesforce Support. Please indicate the URL of the page you were requesting, any error id shown on this page as well as any other related information. We apologize for the inconvenience.
Thank you again for your patience and assistance. And thanks for using 
salesforce.com
!
Error ID: XXXXXXXXXXXXXXXX (1213319076)
The formula field contains the reference to a custom setting ($Setup.)
and the TEXT() logic referring a picklist.
Resolution
change the formula field removing the "TEXT"  and using ISPICKVAL() instead.
