### Topic: Displaying Data Using Data Elements | Salesforce
Displaying Data Using Data Elements | Salesforce
Displaying Data Using Data Elements
You can use a data element to display the data retrieved by a page data connection or a data repeater. The data element binds to a field in the object and acts as a placeholder that’s replaced with the field’s data when the page loads.
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To build, edit, and manage Site.com sites:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
When combined with a data repeater, data elements result in a “repeating template” that displays one or more records on the page. When used with a page data connection, data elements display data from a single record.
You can use data elements to display plain text, formatted text (for dates and numbers), or images. You can also add hyperlinks to data elements to allow site visitors to navigate to another page, such as a detailed description, or to refresh the data displayed on the page or the data repeater based on their selection. See 
Data Filtering Examples
.
When the page is open:
Drag a 
Data Element
 from the Page Elements pane onto the data repeater. Alternatively, if the page has a page data connection, drag the 
Data Element
 page element directly onto the page canvas.
Select the field to display. To 
customize how the field’s data is displayed
, click 
Customize
.
Note
The object’s fields are listed first, followed by the fields of all 
parent objects
, which use the format 
parent_object_name.field_name
.
Select the display type.
Option
Description
Text
Lets you display the field’s data as plain text.
Formatted text
Lets you choose from several text display formats if you’re working with dates, times, or currency.
Image
Lets you display the field’s data as an image if the field contains an image URL. The URL can be absolute or 
relative to the site
.
You can also select a field to use for the alternative text or enter custom text.
To create a hyperlink, select 
Add a hyperlink
. Otherwise, go to step 8.
Select the link type.
Option
Description
A URL
Lets you link to a Web page by:
Choosing a field that you want to reference, such as a field that stores the relative URLs of PDFs you uploaded to your site.
Choosing a field that you want to reference and clicking 
Customize
 to add an absolute URL or to 
create a custom link
, such as a URL query string.
An item in your site
Lets you link to a page, image, or file in the site by selecting the item type and then selecting the item. (If you can’t see the list of items, place your cursor in the 
URL
 field and press the DOWN key on your keyboard.)
You can also 
customize the URL
—for example, by creating a URL query string.
An email
Lets you link to an email message by entering the recipient's address, and the message subject and body.
You can use merge fields to access the object’s fields. For example, if an object has an 
Email
 field, enter the merge field, such as 
{!email}
, in the Email address text box.
When the link is clicked, it opens a new message window in the user’s email client and adds the appropriate email address to the To: field.
Optionally, enter a tooltip by selecting the required field or clicking 
Customize
 to add custom text.
The tooltip displays as a pop-up when the user hovers over the link.
If you’re linking to a URL or an item in your site, specify where the item should open.
Option
Description
Popup window
Loads the item into a popup window. When you select this option, you can set the title for the popup and control its appearance and size with the options that appear.
New window (_blank)
Loads the item into a new, unnamed browser window.
Same window (_self)
Loads the item into the same frame or window as the link. This is the default setting.
Topmost window (_top)
Loads the item into the topmost parent frameset or window of the frame that contains the link.
Parent window (_parent)
Loads the item into the parent frameset or window of the frame that contains the link.
Click 
Save
.
The data element is displayed on the page as a merge field. To test the output, 
preview the page
.
See Also:
Dynamically Retrieving Data with Data Repeaters
Retrieving Data with Page Data Connections
Displaying Data Using Custom Code
Displaying Data Using Content Blocks
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_data_repeater_element.htm&language=en_US
Release
202.14
