### Topic: Instructions included to uninstall Adobe Reader, Adobe Air and Chatter desktop
The following error i
s displayed when clicking on any preview of a document in Chatter Desktop.
 "There was an error opening this document. This file cannot be found."  
 
Resolution
Please follow the below steps in the sequential order
1. Uninstall Adobe Reader
2. Uninstall Adobe Air
3. Uninstall Chatter Desktop
4. Install Adobe Reader
5. Install Adobe Air
6. Install Chatter Desktop
 
*Please also confirm if temporarily disabling local anti-virus software will address the behavior.
NOTE
: If you do not have permission on your machine to do the above operations, you will need to work with your local IT Department or Machine Administrator. Salesforce Support does not have the permissions necessary to install or uninstall programs on your local machine.
 
