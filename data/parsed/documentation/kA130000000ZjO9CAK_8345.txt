### Topic: Question-to-Case Overview | Salesforce
Question-to-Case Overview | Salesforce
Question-to-Case Overview
Question-to-Case lets moderators create cases from questions in Chatter, which makes it easier to track and resolve your customers’ issues. Question-to-Case is available in the full Salesforce site and the Salesforce1 mobile browser app, as well as in communities where Chatter Questions is enabled.
Available in: Salesforce Classic
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
When a customer uses the Question action in Chatter to ask a question, similar questions and Knowledge articles appear below the Chatter publisher. If the similar questions and articles don’t address the issue, the customer posts the question.
If a question isn’t resolved, you can escalate the question to a case. Users with the “Moderate Chatter” or “Moderate Communities Feeds” user permission can create cases from questions directly in the feed, or you can set up processes—similar to workflow rules—in the Lightning Process Builder to automatically create cases from questions that meet specified criteria. Cases from questions are added to a queue so support agents can claim them.
When a customer’s question is turned into a case, the customer receives an email with the case number and a link to the case. The customer can view the case via a link on the question that’s visible only to them, while moderators see a note on the question indicating that a case was created.
Note
The person who asked the question must have access to cases so they can view their case.
Moderator Flag
 
Customer Flag
 
Note
On escalated questions in Salesforce (as opposed to communities), the notification is visible to all users, not just moderators.
When agents find a solution, they can respond to questions directly from the console, and the customer sees the agent’s response on the question or in the My Cases view. Agents choose whether the reply is visible to the community, or only to the customer who asked the question.
To get started, see 
Set Up Question-to-Case
.
See Also:
Create a Case from a Question in Chatter
Automatically Create Cases from Unresolved Questions in Chatter
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=questions_qtc_overview.htm&language=en_US
Release
202.14
