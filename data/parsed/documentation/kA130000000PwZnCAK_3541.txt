### Topic: Navigate to Your Communities | Salesforce
Navigate to Your Communities | Salesforce
Navigate to Your Communities
You can easily switch between working in your internal organization and collaborating with customers or partners in communities. The global header is not available by default, the administrator for your organization must enable it for community members.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Note
Organizations who enabled Communities before Winter ‘14 see the new global header by default when they turn on Communities.
The menu on the left side lets you switch between communities and your internal organization.
To access communities from within your organization, click 
 next to 
Organization Name
 in the drop-down and select the community you want to switch to. The drop-down shows 
Active
 communities that you’re a member of. If you have the “Create and Set Up Communities” permission, you also see 
Preview
 communities that you’re a member of.
To return to your internal organization, click 
 next to 
Community Name
 in the drop-down and select your organization name.
Internal users who aren’t members of any community only see the company name. External users see the drop-down menu only if they belong to more than one active community.
Note
If an external user who is only a member of one 
Active
 community is given the URL for a 
Preview
 community, they don’t see the drop-down menu in the 
Preview
 community.
You can work in more than one community at a time if you open each one in a different browser tab.
Switching between your internal organization and your communities doesn’t have to disrupt your workflow. We automatically return you to the page where you were last working, so you can pick up right where you left off.
Note
If your organization is setting up My Domain but hasn’t finished deployment of the new custom domain, switching from a community to your internal organization directs you to the Salesforce login page instead. This can happen when you’ve logged in using a My Domain URL that has been registered but not yet deployed. Once your domain is deployed, selecting your organization in the drop-down directs you to the internal organization as expected.
The 
Your Name
 menu on the right side contains links to edit contact information and log out. For internal users, it also contains a link to Help & Training and may also contain links to Setup and other tools depending on user permissions and enabled features. 
If your organization is using Lightning Experience, users with the appropriate permission can switch between Salesforce Classic and the new interface using the Switcher. Look for the 
Switch to Lightning Experience
 link in the 
Your Name
 menu. Communities aren’t supported in Lightning Experience, so to create, manage, or access communities from the global header, you’ll have to switch back to Salesforce Classic.
See Also:
Salesforce Communities Overview
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=networks_navigate.htm&language=en_US
Release
202.14
