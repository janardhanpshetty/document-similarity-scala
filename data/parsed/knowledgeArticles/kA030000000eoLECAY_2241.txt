### Topic: Account overview and summary of the Office Depot Platinum Support account.
Company Summary:  Office Depot (previously OfficeMax) is a big-box retailer of office products, with their headquarters in Boca Raton, Florida.
 
----------
 
Salesforce Account Page: https://na7.salesforce.com/00100000002W5Ho?srPos=0&srKp=500
Stack: S6
Database: ET676
Primary MID: 6201021
Suggested User to Impersonate: 
xyz_ETAdmin_6201021
 
----------
FTP Information
URL: ftp://ftp.s6.exacttarget.com 
Username: 6201021 
Password: bP.2E.9t7Z 
URL: ftp://ftp.s6.exacttarget.com 
Username: 6286555 
Password: s.7DRe.6j 
 
----------
 
Primary PSM: Chris Wirthwein
Secondary PSM: Michael Schmidt
Link to Platinum Support Accounts (off ET network): https://na7.salesforce.com/50100000000F0hj?srPos=0&srKp=501
Link to Platinum Support Accounts (on ET network): http://sps/sites/csc/Platinum/Lists/Platinum%20Support%20Accounts/Alpha%20by%20Client.aspx
 
----------
 
Additional ExactTarget Staff for Client
Relationship Manager: Brian Tomey
Program Lead: Brittany Link
Implementation Consultant: Cheryl Sargeant
Technical Architect: Tracy Novotny
Marketing Consultant: Jenny Stone
----------
 
Business Units:
Office Depot – 6286555 (Most business is transacted here)
OfficeMax – MaxPerks – 6201060 (Legacy OfficeMax BU, still active but being phased)
ODP Business Solutions – 6286556
Reliable – 6201070
Workplace – 6201069 (barely used, if at all)
Office Depot Contacts
Anjie Moin (
Anjie.moin@officedepot.com
) – VP.  The one in charge.
Amy Banda (
amy.banda@officedepot.com
) – next in command
Amanda Smith (
amanda.smith@officedepot.com
) 
Christina Devivo (christina.devivo@officedepot.com) 
Tom Griffin (
tom.griffin@officedepot.com
)
Kevin Montano (
kevin.montano@officedepot.com
)
Pilar Gonzalez (
pilar.gonzalez@officedepot.com
)
Gabby Souffront (
gabrielle.souffront@officedepot.com
)
Sev 1 notifications should go to all of the contacts listed above at Office Depot, as well as to 
officedepot@exacttarget.com
.  Maintenance and release notifications should also go to 
gdwoncall@officedepot.com
 and 
email.circulation@officedepot.com
.
----------
 
Monitoring Information & Links to Solutions: 
Marketing Cloud - Platinum - Office Depot Inc. - Feedback File Automation Handling
https://org62.my.salesforce.com/kA2300000019Kqd?srPos=0&srKp=ka2&lang=en_US
Marketing Cloud - Platinum - Office Depot, Inc. - Automation Error: File is Too Old
https://org62.my.salesforce.com/kA2300000019KqO?srPos=1&srKp=ka2&lang=en_US
Marketing Cloud - Platinum - Office Depot, Inc. - Nagios Alert for Program not started, running long, skipped or stopped
https://org62.my.salesforce.com/kA2300000019KqJ?srPos=2&srKp=ka2&lang=en_US
Marketing Cloud - Platinum - Office Depot Inc. - Automation Errors - Onboarding Suppression Automations
https://org62.my.salesforce.com/kA2300000019Kqi?srPos=4&srKp=ka2&lang=en_US
 
Resolution
