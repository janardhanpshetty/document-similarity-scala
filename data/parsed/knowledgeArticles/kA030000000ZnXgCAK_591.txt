### Topic: Akamai Technologies has been granted permission from Salesforce Technology executives to have their Salesforce production and main sandboxes traffic redirected to Akamai's edge servers.
Akamai Technologies has been granted permission from Salesforce Technology executives to have their Salesforce production and main sandboxes traffic redirected to Akamai's edge servers.  This requires Salesforce.com DNS additions of 'A' Records and CNAME's performed by Site Reliability - Network Operations.  
Network Operations must approve the changes before committing to Akamai on timing.
 Akamai Technologies' network operations teams do also have perform some actions and coordination in timing of these actions between Akamai and Salesforce is required.  Every app requires  Please the example flow.
Example Akamaization request flow
 
PLEASE NOTE: 
Akamai Action items are in Green
Salesforce Action items are in Blue
 
CERTIFICATE RELATED: 
AKAMAI will generate a SAN certificate for these URLs:
akamai--c.na11.visual.force.com
akamai--bmcservicedesk.na11.visual.force.com
  
DNS RELATED:
New DNS entries (‘A’ records) for ‘origin’ to be created by SF:
origin-akamai--c.na11.visual.force.com
origin-akamai--bmcservicedesk.na11.visual.force.com 
These have to be DNS ‘A’ records and should be set up similar to: 
akamai--c.na11.visual.force.com
akamai--bmcservicedesk.na11.visual.force.com 
 Once we have these ‘Origin’ DNS entries available Akamai will build the config to Akamaize the SF instance.
 
New DNS entries (‘CNAME’ records) for Certificate validation to be created by SF:
Once the certificate request is in place we will require SF to make DNS entries in this format. The exact entries will be provided once the cert request is in place. PLEASE NOTE THIS IS JUST A TEMPLATE.
    MD5hash.akamai--c.na11.visual.force.com         300   CNAME SHAhash.comodoca.
    MD5hash.akamai--bmcservicedesk.na11.visual.force.com         300   CNAME SHAhash.comodoca. 
 
Once the Akamai configuration is ready we will perform testing on the Akamai staging network. Akamai will spend about 2-3 days testing.
 
New DNS entries (‘CNAME’ records) to map to the Akamai network to be created by SF:
Once our testing is complete we will need one more set of DNS entries to be made.
akamai--c.na11.visual.force.com 200 IN CNAME akamai--.na11.visual.force.com.edgekey.net
akamai--bmcservicedesk.na11.visual.force.com 199 IN CNAME akamai--bmcservicedesk.na11.visual.force.com.edgekey.net 
 
Resolution
If your are in Tier 1/2, escalate this to Tier 3 immediately.  Tier 3 is to create a GUS user story for the Site Reliability - Network Operations team to execute these changes.
Please ensure that the timing and conference bridge information is captured from Akamai.  This will be required for SR-Net Ops to join.  Timing expectation is required for both Akamai and Salesforce respective Network Operations team as they will need to collaborate to complete the DNS changes.
If your are in Tier 3, log a GUS User Story and assign to the Site Reliability - Network Operations queue.
Please see this request as an example, W-2476800 (https://gus.my.salesforce.com/apex/adm_userstorydetail?id=a07B000000140iM&sfdc.override=1)
As Tier 3, there may be a need to log an exception request to escalate within SR - Net Ops have someone assigned.
SR - Net Ops has an approval process for changes and must be completed in order to move forward for A Record or CNAME changes for Akamai.
