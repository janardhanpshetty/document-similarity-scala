### Topic: This was a recent intentional change made by Salesforce to now comply with the P tag spacing.
With the Spring '15 Release there is an increase in spacing for Solution Records. Why is this occurring?
Resolution
The behavior for the <p> tag prior to Spring '15 was non-standard and has now been fixed to go with the actual standard, of having 1 space margins before and after <p> tags.
The recommendation is avoid using <p> tags within lists and other formatted text as it will result in this cosmetic change.
The correct tag to have separate lines without the additional spacing would be <br>.
Note: The HTML Solutions editor does 
not
 use <p> tags when adding new lines for this very reason
