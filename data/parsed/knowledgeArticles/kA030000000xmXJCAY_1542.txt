### Topic: Explains the logic with translating English to Arabic in a Facebook post
When I publish an Arabic post to Facebook containing English words through ConversationBuddy, the English words appear to shift or change in order.
Resolution
English is typically written left to right (LTR) and Arabic is typically written right to left (RTL).
A common software convention is to detect the language being used, and adapt text entry behavior to the language's written direction. Entering an English character leaves the field in LTR mode, while entering an Arabic character converts it to RTL mode. Combining LTR and RTL language is more complicated, as different parts of a sentence have different text directions.
The text field within ConversationBuddy only supports a single direction, so once you begin entering characters in a second language, it converts the direction of ALL characters to that language's direction.
If the second language is a different text direction, as in the case of English and Arabic, this will cause the text to get mirrored as the field rearranges the first language's characters to apply the direction switch.
Incidentally, Facebook performs a similar language detection in messages posted through its API. Given a mixed English/Arabic sentence, it will also apply a single direction to both languages, and the same text mirroring effect will be seen on the post when it appears on a page feed.
The effect of this is that, since the same incorrect text transformation is being applied to your mixed language text, twice, you can simply paste your text in the Message field, ignore the mirroring, and publish it knowing that it will appear on Facebook correctly.
