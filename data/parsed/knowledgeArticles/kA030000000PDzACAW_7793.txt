### Topic: This outlines the reason why the new button does not appear under partner portal settings. This new button allows orgs to create multiple partner portal orgs.
When the partner portal settings for an org the new button may not appear - this button allows you to create multiple partner portals.
Resolution
This is caused as the BT perm 'XForce Partner Portal Only' is enabled. This perm should only be enabled for orgs using PRM 2.0. However, most customer now use a more recent version, so this perm shouldn't be checked for most customers.
To get this perm disabled you should escalate the case to Tier 3.
