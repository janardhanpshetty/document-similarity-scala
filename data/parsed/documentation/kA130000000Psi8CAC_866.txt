### Topic: Approvals | Salesforce
Approvals | Salesforce
Approvals
It’s likely that you’re familiar with process automation in the form of workflow rules. Approvals take automation one step further, letting you specify a sequence of steps that are required to approve a record.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
An approval process automates how records are approved in Salesforce. An approval process specifies each step of approval, including who to request approval from and what to do at each point of the process.
Example
Your org has a three-tier process for approving expenses. This approval process automatically assigns each request to right person in your org, based on the amount requested.
If an expense record is submitted for approval, lock the record so that users cannot edit it and change the status to Submitted.
If the amount is $50 or less, approve the request. If the amount is greater than $50, send an approval request to the direct manager. If the amount is greater than $5,000 and the first approval request is approved, send an approval request to the vice president.
If all approval requests are approved, change the status to Approved and unlock the record. If any approval requests are rejected, change the status to Rejected and unlock the record.
Set Up an Approval Process
If Approvals is the right automation tool for your business process, follow these high-level steps to create one for your org.
Prepare Your Org for Approvals
Make sure that your users can submit their records for approval, and consider how you can make it easy for approvers to respond to approval requests.
Considerations for Approvals
Before you automate something with an approval process, be aware of the limitations of the feature.
Sample Approval Processes
Review samples of common approval processes to help you get started creating your own.
Approval History Reports
If you create a custom report type for approval process instances, users can view the historical details of completed and in-progress approval processes and their individual steps.
Manage Multiple Approval Requests
Transfer multiple approval requests from one user to another or remove multiple approval requests from the approval process.
Approval Requests for Users
Your admin can set up approval processes that let you and other users submit records for approval, which results in 
approval requests
.
Approval Process Terminology
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=what_are_approvals.htm&language=en_US
Release
202.14
