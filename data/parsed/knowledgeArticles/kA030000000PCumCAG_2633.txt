### Topic: This article addresses that issue and provides a workaround.
I would like to send a mass email and there is no functionality built into Salesforce to dynamically add an unsubscribe message. Is there an unsubscribe option which can be added to mass emails?
Resolution
There is no functionality built into Salesforce to dynamically add an unsubscribe message. Such a message would need to be added manually to the email template.
Here’s an example:
If you wish to unsubscribe to all future emails from Media Measurement Ltd <a href="mailto:unsubscribe@mediameasurement.com">click here</a>.
This piece of HTML can open an email for the mail recipient to send back to you or anyone else who can then opt them out of mass mails.
Note:
 Something more automated could be suggested on a future release of Salesforce via the  IdeaExchange.
- Unsubscribe link on mass emails:
http://success.salesforce.com/search?type=Idea&keywords=unsubscribe%20mass%20email
You can create and promote such an idea there. The IdeaExchange is reviewed by Salesforce Product Managers regularly so the more Salesforce users promote an idea, the better chance it could be implemented in a future release.
