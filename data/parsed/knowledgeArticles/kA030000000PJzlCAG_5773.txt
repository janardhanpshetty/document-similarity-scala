### Topic: I’m trying to add a social account but I don’t see all of the clients in my channel listed. Why?
Why don't I see all of the Clients in my Channel listed in the Client drop down menu when I'm adding social accounts?
Resolution
A Client will not be available when you try 
to add a 
social account
 if you do not have admin access to the Client. To select a Client in the Client drop down menu, you must have Client Admin or Channel Admin access for that Client. If you require admin access to a specific Client, please contact your Account Representative or  the Channel Admin for your account. 
