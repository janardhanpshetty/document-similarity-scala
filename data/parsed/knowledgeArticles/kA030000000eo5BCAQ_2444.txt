### Topic: User who has "Customize Application" User Permissions can not set other Salesforce users emails in Auto-Response Rules.
User who has "Customize Application" User Permissions can not set other Salesforce users emails in Auto-Response Rules.
Resolution
For security reasons we cannot use other users emails. Please set current login user's email address or verified organisation wide emails.
The workaround is to use a trigger to send responses instead of Auto-Response rules.
This change was published in the Winter '15 release notes:
Update Auto-Response Message Sender Addresses to Meet New Security Requirements
