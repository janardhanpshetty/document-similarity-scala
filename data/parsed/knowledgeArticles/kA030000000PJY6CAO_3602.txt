### Topic: When loading in Quotas via the Data Loader some month values are blank when looking at the Forecasting tab.
When loading in Quotas via the Data Loader some month values are blank when looking at the Forecasting tab.
The following error is also received in the error files of the Data Loader:
duplicate value found: <unknown> duplicates value on record with id: <unknown>
Resolution
Modify the date field in the excel csv file so that it displays in the correct format needed for our Data Loader to import: 
 
1. Select the Date cells in the csv file 
2 Right click and select "Format Cells". 
3. Select the tab "Number" and then Category "Custom". 
5. Type yyyy-MM-DDThh: mm: ss.ssZ. 
6. The date should appear in the format 2008-04-01T00: 00:00.00 Z 
7. Insert into the Forecasting Quota (ForecastingQuota) object 
The Quota values should then appear for you as normal.
