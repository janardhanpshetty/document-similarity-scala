### Topic: List of criteria used to determine which goals are included in Recent Work.
What Goals are Included In the Recent Work Page?
Resolution
All of your goals excluding those that meet the following criteria will appear in the Recent Work section:
Goals Created After Cycle End
Goals Completed Before Cycle Start
Goals Due Before Cycle Start
