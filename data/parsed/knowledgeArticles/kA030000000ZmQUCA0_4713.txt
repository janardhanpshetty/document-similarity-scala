### Topic: Account Name pulls blank when trying to pull the Primary Contact Account on the Opportunity record.
When generating a Mail Merge on the Opportunity record and trying to pull in the Primary Contact's Account Name, the resulting Mail Merge field ends up blank or "_____". 
Resolution
This is expected and due to the multiple cross object references. 
The workaround is to create a formula field on the Contact object that pulls the Account Name. 
The Formula will be a Text formula and will state Account.Name. 
You can then reference the formula field in the Mail Merge template so it will show the Account Name of the Primary Contact. 
