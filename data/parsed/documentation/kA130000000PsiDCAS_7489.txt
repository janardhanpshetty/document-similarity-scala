### Topic: Custom Help | Salesforce
Custom Help | Salesforce
Custom Help
Available in: Salesforce Classic
Available in: 
All
 Editions except 
Database.com
The 
Help & Training
 link at the top of every page opens the Salesforce Help & Training window which includes online help topics, solutions, and recommended training classes. Additionally, the 
Help for this Page
 link on any page opens a context-sensitive online help topic that describes that page.
Salesforce custom help functionality allows you to augment these standard help features with information on using fields and functionality unique to your organization or the Force.com AppExchange app you are developing.
Custom help allows you to:
Override the standard Salesforce context-sensitive online help topics for your custom objects using 
object-level help
Add 
field-level help
 that displays when users hover their mouse over a field
See Also:
Getting Started with Field-Level Help
Getting Started with Object-Level Help
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customhelp_about.htm&language=en_US
Release
202.14
