### Topic: Identify Your Salesforce Org’s Default Workflow User | Salesforce
Identify Your Salesforce Org’s Default Workflow User | Salesforce
Identify Your Salesforce Org’s Default Workflow User
Select a 
Default Workflow User
 that you want Salesforce to display with a workflow rule when the user that triggered the rule is not active.
Available in: Lightning Experience and Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To edit workflow and approval settings:
“Customize Application”
If your organization uses time-dependent actions in workflow rules, you must designate a default workflow user. When the user who triggered the rule isn’t active, Salesforce displays the username of the default workflow user in the 
Created By
 field for tasks, the 
Sending User
 field for email, and the 
Last Modified By
 field for field updates. Salesforce does not display this username for outbound messages. If a problem occurs with a pending action, the default workflow user receives an email notification.
When workflow email alerts approach or exceed certain limits, Salesforce sends a warning email to the default workflow user or—if the default workflow user isn't set—to an active system administrator.
From Setup, enter 
Process Automation Settings
 in the 
Quick Find
 box, then select 
Process Automation Settings
.
For 
Default Workflow User
 select a user.
Click 
Save
.
Parent topic:
 
Create a Workflow Rule
Previous topic:
 
Add Automated Actions to Your Workflow Rule
Next topic:
 
Activate Your Workflow Rule
See Also:
Daily Limit for Email Alerts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=workflow_defaultuser.htm&language=en_US
Release
202.14
