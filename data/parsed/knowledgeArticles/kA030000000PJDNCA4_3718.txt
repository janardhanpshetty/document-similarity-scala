### Topic: This article has information on hiding the Follow up section of the Log A Call Task. There is a link to an Idea that exists to remove that section as it is not standard functionality to remove the section.
Can I hide the "Followup Call" section of the "Log A Call" Page Layout?
Resolution
The "Log a Call" page layout contains what appears to be required fields in the bottom section for the optional "Create Followup Task" section.
To be clear-
this section is optional
-and the fields themselves wouldn't be required unless the user fills out any of the required fields in that section.
At this time there is no option to remove this section, but again, this is a Feature Request on our 
IdeaExchange
. 
