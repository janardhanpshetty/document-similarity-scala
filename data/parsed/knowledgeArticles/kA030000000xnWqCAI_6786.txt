### Topic: Submit up to 100 batch jobs simultaneously and actively manage the order of the queued jobs to control which batch jobs are processed first. This enhancement provides you more flexibility in managing your batch jobs.
Batch jobs can also be programmatically scheduled to run at specific times using the Apex scheduler, or scheduled using the Schedule Apex page in the Salesforce user interface. More information about this can be found at following links:
- 
http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_batch.htm
- 
http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_batch_interface.htm
Previously, you could submit only up to five batch jobs simultaneously.
The Apex flex queue enables you to submit up to 100 additional batch jobs for execution. Any jobs that are submitted for execution are in holding status and are placed in the Apex flex queue. Up to 100 batch jobs can be in the holding status. When system resources become available, the system picks up jobs from the top of the Apex flex queue and moves them to the batch job queue. The system can process up to five queued or active jobs simultaneously. The status of these moved jobs changes from Holding to Queued. Queued jobs get executed when the system is ready to process new jobs.
Administrators can modify the order of jobs that are held in the Apex flex queue to control when they get processed by the system. For example, administrators can move a batch job up to the first position in the holding queue so that it’s the first job that gets processed when the system fetches the next held job from the flex queue. Without administrator intervention, jobs are processed first-in first-out—in the order in which they’re submitted. To monitor and reorder held batch jobs in the Salesforce user interface, from Setup click Jobs | Apex Flex Queue.
More information about this feature can be found at 
http://releasenotes.docs.salesforce.com/en-us/spring15/release-notes/rn_apex_flex_queue_ga.htm
Resolution
