### Topic: View Dashboard Lists | Salesforce
View Dashboard Lists | Salesforce
View Dashboard Lists
The dashboard list contains all the dashboards you can view.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To view and refresh dashboards:
“Run Reports” AND access to dashboard folder
To create dashboards:
“Run Reports” AND “Manage Dashboards”
To edit and delete dashboards you created:
“Run Reports” AND “Manage Dashboards”
To edit and delete dashboards you didn’t create:
“Run Reports,” “Manage Dashboards,” AND “View All Data”
From the Dashboards tab, click 
Go to Dashboard List
. 
The enhanced Reports tab lists your recently viewed dashboards in that dashboard's folder.
On the dashboard list page, select a folder to view a list of dashboards stored in that folder, or search with filters.
Click a dashboard name to display the dashboard.
If you’re working in Salesforce Classic, Click 
 next to a dashboard name to edit or delete a dashboard.
Note
Dashboards in Group Edition organizations are view-only.
If Chatter is enabled, click 
 or 
 to follow or stop following a dashboard in your Chatter feed.
Parent topic:
 
Dashboards Help You Visualize Complex Information
Previous topic:
 
Use Dashboards on the iPad
See Also:
Dashboards Help You Visualize Complex Information
Get Started with Dashboards
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=dashboards_view.htm&language=en_US
Release
202.14
