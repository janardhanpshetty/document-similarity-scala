### Topic: This article explains how to evaluate the selectivity of a SOQL query and how to determine the fields that can be custom indexed.
A non-selective query may cause different programmatic elements (like an Apex trigger or batch Apex class) to fail. When querying large objects special design considerations must be taken into consideration.
Resolution
The complexity of the SOQL query has a massive influence on whether a field is suitable for indexing. We strongly recommended reviewing the following materials to fully understand how standard and custom indexes are different, and how they're employed.
Working with very large SOQL queries
Webinar: Inside the Force.com Query Optimizer
Database Query & Search Optimization Cheat Sheet
Force.com Query Optimizer FAQ
Maximizing the performance of Force.com SOQL, reports and list views
Force.com SOQL Best Practices: nulls and formula fields
Force.com SOQL Performance Tips: LastModifiedDate vs SystemModStamp
Keep in mind
 - While every effort has been made to ensure this article explained how the Salesforce SOQL query optimizer works at a high level, its behaviour can change without notice to accommodate additional performance enhancements. Developers are advised to use the 
Query Plan view
 (available since Summer '14) in the Developer Console, to be able to tune unselective queries.
The performance of a SOQL will depend on the presence of a selective filter. If a SOQL query contains at least 1 selective filter, the query is said to be selective. If the SOQL query doesn't contain a selective filter, the query is said to be un-selective and will require a full table scan.
From the perspective of the query optimizer, a filter can be simple or composite. A simple filter would be each of the field expressions (<field> <operator> <value>) in a condition expression that uses the "AND" operator. In contrast, the result of joining 2 or more field expressions via the "OR" operator is a composite filter. 
fieldExpression1 AND fieldExpression2 - shows 2 simple filters
fieldExpression1 OR fieldExpression2 - is a composite filter
fieldExpression1 AND (fieldExpression2 OR fieldExpression3) - shows 2 filters (one simple, and one composite).
Verify simple filter is selective
Determine if it has an index.
If the filter is on a standard field, it'll have an index if it is a primary key (Id, Name, OwnerId), a foreign key (CreatedById, LastModifiedById, lookup, master-detail relationship), and an audit field (CreatedDate,   SystemModstamp).
Custom fields will have an index if they have been marked as Unique or External Id
If the filter doesn't have an index, it won't be considered for optimization.
If the filter has an index, determine how many records it would return:
For a standard index, the threshold is 30 percent of the first million targeted records and 15 percent of all records after that first million. In addition, the selectivity threshold for a standard index maxes out at 1   million total targeted records, which you could reach only if you had more than 5.6 million total records.
For a custom index, the selectivity threshold is 10 percent of the first million targeted records and 5 percent all records after that first million.  In addition, the selectivity threshold for a custom index maxes out at 333,333 targeted records, which you could reach only if you had more than 5.6 million records.
If the filter exceeds the threshold,it won't be considered for optimization.
If the filter doesn't exceed the threshold, this filter IS selective, and the query optimizer will consider it for optimization.
Example:
1) Get the SOQL query and replace the list of fields with the count() operator:
SELECT count() FROM Appointment__c WHERE CreatedDate = this_year AND Status__c = 'Available' 
2) Check the number of active records in the table:
Select count() from Appointment__c
# of records: 239619
Threshold for standard indexes: 30% * 239619 = 71885
Threshold for custom indexes: 10% * 239619 = 23961
3) The only filter in the WHERE clause with an index is CreatedDate, so we will only consider the filter on this field.
Partial SOQL query: Select count() from Appointment__c where CreatedDate = this_year
# of records: 84567 > threshold for standard indexes
Is the used filter selective? NOT
4) Let's check though if the filter on Status__c is selective:
Partial SOQL query: Select count() from Appointment__c where Status__c = 'Available'
# of records: 14397 < threshold for custom indexes
5) If Status__c is custom indexed, "Status__c = 'Available'" would be a selective filter, and hence the SOQL query would be selective.
 
To Custom Index a field
 - Create
 
a new case and provide the affected SOQL query, and user Id that can be used for testing, as well as the values of any binding variables used.
What happens if more than one simple filter is selective? 
The query optimizer will choose the one with lower cost to drive the execution plan of the query.
How do you know if a composite filter is selective? 
The optimizer will determine if each of the simple filters is selective, and if in aggregate the number of returned records doesn't exceed the thresholds mentioned above. If these conditions are met, the filter will be selective.
