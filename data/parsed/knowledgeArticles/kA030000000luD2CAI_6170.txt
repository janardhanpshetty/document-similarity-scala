### Topic: How to check the SingleEmailMessage limit using Rest calls
From time to time organizations may hit their SingleEmailMessage limit. Errors such as SINGLE_EMAIL_LIMIT_EXCEEDED , "The daily limit for the org would be exceeded by this request" or other may be seen. This limit applies only to email messages composed and sent through the API or Apex. This is not trackable currently in "Setup".  However there is a convenient way to track the limits and usage for the organization using the Workbench tool.
You can access Workbench here: 
https://workbench.developerforce.com/login.php
1. Ensure you are logged into the organization where you want to verify your limits.
2. Navigate to https://workbench.developerforce.com/login.php
3. Accept any oauth prompts to complete authentication
4. Select "Jump to REST Explorer"
5. Select execute
6. From the options presented select:  /services/data/vXX.0/limits
7. Select the SingleEmail area to view the daily maximum and remaining calls.
Note: SingleEmailMessage calls are calculated for a 24 hour period from 12 midnight UTC (00:00) and the limit will reset to zero at this time each day. Emails to each recipient of an email are counted such as addresses in the "to" and "cc".  
There is a default value for each organization the limit is not related to license count. 
See 
https://www.salesforce.com/us/developer/docs/apexcode/Content/apex_gov_limits.htm
 for limits details.
Resolution
