### Topic: Summer '15 (196 and beyond) - Please keep these in mind when working with customers on issues related to Lightning App Builder.
Please keep these in mind when working with customers on issues related to Lightning App Builder.
1) Lightning App Builder is a GA fully supported feature by the Mobile Skills group. It is however based on Lightning pages which utilize standard and sometimes a customer's Custom Lightning Components. If there are questions around the specific custom components, those need to be handled by Developer Support (if the customer has paid Dev Support).
Please direct them to the documentation: https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/components_config_for_app_builder.htm
** NOTE: This includes custom third party components available via AppExchange **
2) Does LAB allow admins to create separate page layouts for the Salesforce1 mobile UI? 
No. The Lightning App Builder makes it easy to create CUSTOM APP PAGES for Salesforce1 - not reconfigure the standard or custom object page layouts. Admins are able to create App Home pages for custom mobile apps and add them to the Salesforce1 navigation menu. 
3) ONLY Global Actions can be used within the Lightning App Builder pages. Please ensure that the required users have access to these actions via assigned Record Types. Ensure that their profile/s are correctly configured. ** Global actions are accessed via the App Home Page. **
4) Can admins configure Lightning Tabs (pages) to be used in the full site? 
Currently, no. Lightning App Pages are only available in the Salesforce1 mobile UI. These Lightning Pages will be available for Salesforce Desktop (SFX) once that is available to customers (no ETA currently).
5) Are Lightning Apps available in a custom built apps or able to be distributed? 
No. At this point (200), a custom native app can already be built using Lightning components and the mobile SDK - this has to be done entirely programmatically at this point. 
Admins/Developers cannot currently use a Lightning Page app built with the App Builder in this fashion.
Resolution
