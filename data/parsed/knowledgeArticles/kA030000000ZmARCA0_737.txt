### Topic: This article contains information and links to Knowledge Articles and Known Issues new or top trending this week to help you solve cases in the Performance Group.
The following are the Top Trending and latest Known Issues and Articles for the Week:
  
General Functional Area
Article or Known Issue Name
Article or Known Issue #
Additional Comments
Live Agent
Live Agent auto-scroll issues in Chrome
000220435
 
Performance
INTERNAL - FOCUS Dashboard
000220823
 
Performance
INTERNAL - FOCUS and Graphite Dashboards
000220826
 
Performance
INTERNAL - The App Dash
000220825
 
Performance
Why closing Chrome doesn't really shut it down (and what to do about it)
000228062
 
Other
Winter '16 Release Notes
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Resolution
