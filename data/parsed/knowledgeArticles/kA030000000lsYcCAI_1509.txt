### Topic: Will the permission or registrations settings be affected for Managed Accounts if I transition to using Facebook Business Manager?
Will the permission or registrations settings be affected for Managed Accounts if I transition to using Facebook Business Manager?
Resolution
Using Facebook Business Manager does not affect Managed Accounts permissions or registration settings. You must continue to have a page management role (for example, admin) on your Facebook page to register it. Please note that Facebook Business Manager is not the same as Facebook business accounts. Facebook business manager is a way to manage pages that still requires you to have a page management role on the page. 
Related Topics
Facebook for Business
 
