### Topic: Collaboration Limits | Salesforce
Collaboration Limits | Salesforce
Collaboration Limits
Approvals Limits in Chatter
Approval limits for delegated approvers, approvals posts, and Sites or portal users.
Chatter Plus Limits
Feature limits for Chatter Plus (also known as Chatter Only).
Feed Tracking Limits
Limits for tracking objects, fields, and topics.
General Chatter Limits
Limits for Chatter features by edition, browser limits, and mention limits.
List View Limits on Feeds
Limits for list views on record feeds.
Search Limits for Files in Chatter
Search limits by file type and file size in Chatter.
User Sharing Limits in Chatter
Salesforce administrators can configure user sharing to show or hide an internal or external user from another user in an organization.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_collaboration_subparent.htm&language=en_US
Release
202.14
