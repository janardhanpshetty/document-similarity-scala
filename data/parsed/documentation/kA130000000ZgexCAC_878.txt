### Topic: Design the Dataflow to Load the Data | Salesforce
Design the Dataflow to Load the Data | Salesforce
Design the Dataflow to Load the Data
Now it’s time to figure out how the dataflow will extract the Salesforce data and load it into a dataset. You start by creating this high-level design for the dataflow.
Available in Salesforce Classic and Lightning Experience.
Available for an extra cost in 
Enterprise
, 
Performance
, and 
Unlimited
 Editions. Also available in 
Developer
 Edition
.
The dataflow will extract data from the Opportunity and OpportunityTeamMember objects, join the data, and then load it into the OppTeamMember dataset.
Now let’s implement that design in JSON, which is the format of the dataflow definition file. A dataflow definition file contains transformations that extract, transform, and load data into a dataset.
Based on the design, you create the JSON shown below.
{    
   "Extract_OpportunityTeamMember": {        
      "action": "sfdcDigest",        
      "parameters": {            
         "object": "OpportunityTeamMember",            
         "fields": [                
            { "name": "Name" },                
            { "name": "OpportunityId" },                
            { "name": "UserId" }            
         ]        
      }    
   },        
   "Extract_Opportunity": {        
      "action": "sfdcDigest",        
      "parameters": {            
         "object": "Opportunity",            
         "fields": [                
            { "name": "Id" },                
            { "name": "Name" },                
            { "name": "Amount" },                
            { "name": "StageName" },                
            { "name": "AccountId" },                
            { "name": "OwnerId" }            
         ]        
      }   
   },    
   "Augment_OpportunityTeamMember_Opportunity": {        
      "action": "augment",        
      "parameters": {            
         "left": "Extract_OpportunityTeamMember",            
         "left_key": [                
            "OpportunityId"            
         ],            
         "relationship": "TeamMember",            
         "right": "Extract_Opportunity",            
         "right_key": [                
            "Id"            
         ],            
         "right_select": [                
            "Name","Amount"            
         ]        
      }    
   },    
   "Register_Dataset": {        
      "action": "sfdcRegister",        
      "parameters": {        
         "alias": "OppTeamMember",        
         "name": "OppTeamMember",        
         "source": "Augment_OpportunityTeamMember_Opportunity",            
         "rowLevelSecurityFilter": ""        
      }    
   }
}
If you were to run this dataflow, Wave Analytics would generate a dataset with no row-level security. As a result, any user that has access to the dataset would be able to see the opportunity shared by the opportunity team.
For example, as shown below, Lucy would be able to view the opportunity that belongs to an opportunity team of which she is not a member.
You need to apply row-level security to restrict access to records in this dataset.
Parent topic:
 
Row-Level Security Example based on Opportunity Teams
Previous topic:
 
Determine Which Data to Include in the Dataset
Next topic:
 
Determine Row-Level Security for the Dataset
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_security_rowlevel_example_teammembership_designdataflow.htm&language=en_US
Release
202.14
