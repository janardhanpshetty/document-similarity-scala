### Topic: Upgrade to The New Version of Goals available with Summer '15 release
Upgrade to The New Version of Goals available with Summer'15 release.
 
Resolution
Help your sales teams precisely track goals and metrics related to their work with 
New Version of Goals
! The new Goals appear faster and are customizable like other objects. Your teams can also manage their goals on the go using Salesforce1. All organizations using goals will need to upgrade to 
New Version of Goals
 during the Summer ’15 release.
Access the new goals from the Goals tab or the Goals subtab on your Chatter profile. 
New Version of Goals
 look, feel, and act more like other Salesforce objects, and users can leverage additional
 Salesforce 
features. For example, administrators can create customizable fields, layouts, actions, and list views related to goals.
The goal detail page lists the metrics, activities, and other information related to a particular goal. Align goals with others by linking your goals with parent goals or subgoals. There’s more control over sharing settings than before. For example, you can choose between read-only and read/write access to better control how you collaborate.
Users can create standalone metrics or tie them to individual goals, and users can share metrics or create activities for individual metrics. Anyone with read/write access to metrics can now refresh metric values. Metrics also appear in relevant coaching spaces and performance summaries, and you can customize metrics to meet your organization’s needs.
 
 
Goals and metrics are also available on
 Salesforce1
, where users can review, edit, and create goals and metrics.
Note the following limitations:
No default list views are defined. You should create filters (such as Key Company Goals or My Goals) that are suitable for your organization.
The Percent metric type has been replaced with the Progress metric, which allows users to track numerical progress, including percentages.
