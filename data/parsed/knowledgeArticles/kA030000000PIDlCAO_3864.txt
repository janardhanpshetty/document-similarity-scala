### Topic: This article would help you in showing a date field's value in a specific time frame format in a report (day, week, month, quarter, year, etc.).
Steps to show date fields in a specific time frame in a report.
 
Resolution
Once you have selected a report type, i.e. Opportunity Report Type, click on the CREATE button.
Change the report format to either a Summary or Matrix Report.
Drag and Drop your date field, i.e. Close Date, in the grouping area of the report.
Click on the drop down arrow next to the date field and select, "Group Dates By".
The drop down will allow you to select the standard options to show how you would like to show your Date Field
Day
Calendar Week
Calendar Month
Calendar Quarter
Calendar Year
Fiscal Quarter
Fiscal Year
Calendar Month in Year
Calendar Day in Month
Note
: You may also have the ability to select your organization's 
custom 
fiscal year to show in the report as well. For example, as part of a custom fiscal year, you can create a 13-week quarter represented by three periods of 4, 4 and 5 weeks, rather than calendar months.
