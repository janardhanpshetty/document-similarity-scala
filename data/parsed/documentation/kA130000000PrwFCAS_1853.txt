### Topic: Configure Push Notifications for a Salesforce Console | Salesforce
Configure Push Notifications for a Salesforce Console | Salesforce
Configure Push Notifications for a Salesforce Console
Administrators can set up push notifications in a console so that users can see when a record they’re working on has been changed by others.
Salesforce console available in Salesforce Classic and App Launcher in Lightning Experience. Setup for Salesforce console available in Salesforce Classic.
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To configure push notifications:
“Customize Application”
Push notifications are visual indicators on lists and detail pages in a console that show when a record or field has changed during a user’s session. For example, if two support agents are working on the same case, and one agent changes the 
Priority
, a push notification appears to the other agent so he or she spots the change and doesn’t duplicate the effort.
Choose when push notifications appear and which objects and fields trigger them:
From Setup, enter 
Apps
 in the 
Quick Find
 box, then select 
Apps
.
Select a console app, and click 
Edit
.
In 
Choose How Lists Refresh
, select when push notifications appear.
Option
Description
None
Lists don’t refresh and push notifications don’t appear.
Refresh List
The entire list refreshes when there are any changes to it. Records are added or removed from the list based on the list’s criteria, but new records added to queues only refresh the record owner’s list.
Refresh List Rows
Rows in the list refresh when there are any changes to fields selected for push notifications.
In 
Choose How Detail Pages Refresh
, select when push notifications appear.
Option
Description
Do Not Refresh
Detail pages don’t refresh and push notifications don’t appear.
Automatically Refresh
The detail page automatically refreshes when a record is changed.
Flag
A message appears on the detail page when a record is changed.
Click 
Select objects and fields for notifications
, and select 
Edit
. The push notification settings you choose here apply to all your console apps.
Select the objects that you want to trigger push notifications. For example, if you want any changes to cases or case fields to trigger push notifications, move Cases from Available Items to Selected Items.
Under Fields, click 
Edit
 and choose the fields you want to trigger push notifications.
Click 
OK
, then select 
Save
.
Grant push notification users at least the “Read” permission on the Push Topics standard object. See 
User Permissions and Access
.
Important
When the 
Require HttpOnly attribute
 is turned on for session security, push notifications don’t appear.
Note
These objects and their fields are available for push notifications: accounts, contacts, cases, leads, opportunities, campaigns, tasks, and custom objects.
Push notifications aren't available in the console in Professional Edition.
If you set up push notifications for the lead object and use Data.com Clean rules, push notifications are triggered in the console. Keep in mind that extra notifications can affect your push notification limit.
See Also:
Salesforce Console Configurable Features
Salesforce Console
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=console2_push_notifications.htm&language=en_US
Release
202.14
