### Topic: Creating Branding Properties | Salesforce
Creating Branding Properties | Salesforce
Creating Branding Properties
With branding , you can quickly create style properties that can be reused by editors of your site.
Available in: Salesforce Classic
Available for purchase in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Available (with limitations) in: 
Developer
 Edition
User Permissions Needed
To build, edit, and manage Site.com sites:
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
To edit only content in Site.com sites:
Site.com Contributor User
AND
Contributor role assigned at the site level
When creating your branding properties, you can organize properties into multiple sections to make them easier to find in the Branding Editor. You can also organize your properties within sections.
Order properties within sections in a logical manner. For example, organize them alphabetically.
Order sections and properties by dragging and dropping them within the Branding Properties view.
From within your site, click 
Site Configuration
 | 
Branding Properties
. 
The Branding Properties editor appears.
Click 
.
Enter a name for the property in the Label field. 
The expression name is filled in automatically. The expression name is used in style sheets and code blocks.
Choose a type.
Set the default value.
To make the property required, click 
Required
.
Hover over any property and use the menu 
 to edit or delete it. You can double-click any section name to edit it.
See Also:
Site Branding Overview
Custom Property Types
Setting Up Branding Properties
Using the Branding Editor
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_branding_create.htm&language=en_US
Release
202.14
