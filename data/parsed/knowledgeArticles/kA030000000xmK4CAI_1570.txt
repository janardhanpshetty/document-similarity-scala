### Topic: True domains are displayed in Domains mentioned instead of Twitter's link shorten t.co.
In the past, when reviewing "Domains Mentioned" for the Radian6 free insight, t.co was the top mentioned domain. It is no longer listed. Why is there such a dramatic decrease in this domain being mentioned?
Resolution
http://t.co is 
Twitter's link shortener
. Tweets that post long links will shorten using this link shortener. In the past, Domains Mentioned captured these shortened links. As of the November '13 release, Domains Mentioned displays the true domain that is mentioned in Twitter content instead of  t.co. Posts processed prior to the November '13 release will continue to display t.co.
