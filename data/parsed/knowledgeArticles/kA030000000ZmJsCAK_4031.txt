### Topic: In Winter '15, you can enable compact feed so support agents working in the Salesforce console can see much more information about cases with less scrolling, making it easier to get the full history of a case and resolve customers’ issues more quickly.
Resolution: "
To enable compact feeds, from the Feed View settings page for a feed-based case page layout (Setup|Customize|Cases|Page Layouts), select Enable Compact Feed View in the Console.
User-added image
In order to see the case feeds compacts layouts, you must have the following the below requirements:
Be using Firefox, Chrome, or Internet Explorer 9 or higher
Enabled Publisher Actions
Select "Use Page Layout editor" in the Feed Setting for the Layout
Note: This is not available on legacy page layouts for case feed users."
@Kanchan Rohlania
 - FYI
Show More
Topics:
   
KBFeedback
Comment
 
  ·  
 
Like
 
Unlike
 
  ·  
 
Feeditem is bookmarked
 
Create New Task
Bookmark
Edit Topics
June 11, 2015 at 5:08 AM
  
Kanchan Rohlania
done
Show More
Like
Unlike
 
  ·  
 
June 11, 2015 at 10:58 AM 
Attach File
 
Click to comment
 
 
Followers
No followers.
« 
Go Back
Information
 
Skill Group
CRM Configuration
How to see case feeds compact layouts.
Resolution
To enable compact feeds, from the Feed View settings page for a feed-based case page layout (Setup|Customize|Cases|Page Layouts), select 
Enable Compact Feed View in the Console
.
In order to see the case feeds compacts layouts, you 
must
 have the following the below requirements:
Be using Firefox, Chrome, or Internet Explorer 9 or higher
Enabled Publisher Actions
Select "Use Page Layout editor" in the Feed Setting for the Layout
Note:
 This is not available on legacy page layouts for case feed users.
