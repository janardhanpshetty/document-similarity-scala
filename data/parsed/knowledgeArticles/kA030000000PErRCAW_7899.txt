### Topic: This article discussed how to handle admin request for data management through CSV file
How should I handle data management request received from customer with CSV file attached?
 
Resolution
Following are some of the best practices and talking points around this requirement:
Thank you for submitting your administrative request to update data in your 
salesforce.com
 organization. Your Premier + Admin Service Entitlement allows you to provide data in CSV file format and leverage Admin Service to have them updated(insert, update, upsert, delete and export)  to your 
salesforce.com
 organization. We would like to provide the following information to help you prepare a Salesforce-specific CSV format.
A comma-separated values (CSV) file stores 
tabular
 data (numbers and text) in plain-text form. CSV is a common, relatively simple file format that is widely supported by consumer, business, and scientific applications. Your CSV file’s ‘Quality’ is extremely important to ensure an effective and efficient Premier+ transaction.
Because Salesforce.com takes your data consistency and security very seriously, we will not manipulate your data in any circumstances. To leverage our data update services, you will have to provide data in correct format and accept full responsibility of your data accuracy.
Before we action your data update requests, you will need to understand that Salesforce Data Loader is the only Salesforce.com approved tool to process data update through API calls. The Data Loader requires the use of the Force.com API. If your Salesforce edition allows the use of the API (Enterprise, Unlimited and Developer Editions), you can download Data Loader from the Setup menu, under Administration Setup - Data Management. The Data Loader is an easy to use graphical-interface tool for extracting and inserting/updating data in your Salesforce instance. 
More information about Data Loader can be found at: 
http://wiki.developerforce.com/page/Apex_Data_Loader#Other_loading_options
.
Your CSV file will be used to feed Data loader, and it has to be compliant to Data Loader.
When you prepare the Salesforce-specific CSV format, please put the following highlights in consideration
Understand which object you want to upload data into. Normally, your Org has standard objects and custom objects. You will need to clearly let Salesforce support know which object you want the CSV file data upload to/insert in/delete from
 
Standard Objects and its fields can be accessed from Setup - >App Setup - >Customize->Objects (such as Leads, Accounts, Opportunities, etc) ->Fields
Custom Objects can be found at Setup - >App Setup - > Create ->Objects
Understand what Field Names/API Names are for those fields you want to update. Every field in each object has its Label and API level name. Data Loader will use API names for its functionality. So in your CSV file, try to use API names rather than field labels as columns headers.
For Standard object, you can find Field Name/API Name from Setup - >App Setup - >Customize->Objects (such as Leads, Accounts, Opportunities, etc) ->Fields
For Custom Object, you can find Field Name/API Name from Setup - >App Setup - > Create ->Objects then click the custom object label link.
Understand Field Type Limitations for Data Loader
 
Data Types Supported by Data Loader: 
http://help.salesforce.com/apex/HTViewHelpDoc?id=supported_data_types.htm
Data Loader only recognizes the ID rather than the names. Such as user ID, lookup record IDs. An easy way to find a user’s ID is copy it from User’s chatter profile URL
If your CSV file has Asian characters, please pay extra attention by checking this article: 
https://help.salesforce.com/apex/HTViewSolution?urlname=How-to-import-Asian-characters-from-a-CSV-file-into-Salesforce-1327108611074&language=en_US
Understand your System Setup/Implications to Data Uploads/Changes
By following the above suggestions, your CSV file should be ready for Admin team to upload. However, due to your unique system configurations such as validation rules, workflows, and Apex triggers, we cannot guarantee your CSV file will upload correctly or efficiently. 
You will need to be mindful of what else in your system might be affected with your Data Upload request and make the necessary system changes to allow for the Admin team to fulfill on your request
