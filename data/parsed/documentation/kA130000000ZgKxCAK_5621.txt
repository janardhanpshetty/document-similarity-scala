### Topic: Match Keys Used with Matching Rules | Salesforce
Match Keys Used with Matching Rules | Salesforce
Match Keys Used with Matching Rules
Match keys increase the effectiveness of matching rules. Review how match keys are used to create match key values for standard matching rules. By understanding match keys, you’ll get a better sense of how duplicate detection works.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
A 
match key
 is a formula that allows a matching rule to quickly return a list of possible duplicates.
Once a matching rule is activated, match keys are used to create match key values for all records. When a matching rule runs, it compares the match key values of the saved record and existing records. If the saved record has the same match key value as an existing record, it’s a potential duplicate and evaluated further. If the saved record has a unique match key value, it’s not considered a duplicate. On rare occasions, the use of match keys causes duplicates to be missed. It almost never happens, and we’re pretty sad when it does. Fortunately, the performance benefits of using match keys greatly outweighs the drawbacks.
How Match Keys and Match Key Values Are Created
The matching rule equation (that is, the arrangement of fields) is rewritten into a standardized format that translates OR statements into AND statements.
Values for fields in the matching rule are normalized.
A match key is created using the field combinations specified in the standardized field format. Matching rules can have multiple match keys. For standard matching rules or custom rules with standard field combinations, pre-defined match keys are used.
The match key is used to combine normalized field values for each record. And, voila, glorious match key values are born!
Note
We currently don’t create match keys for the 
Title
 and 
Address
 fields. Therefore, if those fields are included in your matching rule, they won’t generate match keys.
Match Key Notation
The common match key notation shows which fields and which characters in those fields are used in the match key.
The field used in the match key (1)
Number of words (or tokens) in the field value to include in match key (2). If no number is present, then all words are included.
Number of characters per word to include in the match key (3). If no number is present, then all characters are included.
Additional field used in the match key (4)
Note
Each custom matching rule can have a maximum of 10 match keys; you’re prevented from saving a matching rule that would require more.
Pre-Defined Match Keys for Standard Matching Rules
Standard matching rules use pre-defined match keys.
Match Key Notation
Objects Applied To
Match Key Value Examples
Company (2,6) City (_, 6)
Account
Account: Orange Sporting Company = orangesporti
City: San Francisco = sanfra
Key: orangesportisanfra
Company (2,6) ZIP (1,3)
Account
Account Name: salesforce.com = orangesports
ZIP: 94105-5188 = 941
Key: salesf941
Email
Contact
Lead
Email: john_doe@us.ibm.com = johndoe@ibm.com
Key: johndoe@ibm.com
First_Name (1,1) Last_Name Email
Contact
Lead
First Name: John = j
Last: Doe = doe = t (with double metaphone applied)
Email: john_doe@us.salesforce.com = johndoe@salesforce.com
Key: jt@salesforce.com
First_Name (1,1) Last_Name Company (2,5)
Contact
Lead
First Name: Marc = m
Last Name: Benioff = pnf (with double metaphone applied)
Company: salesforce.com = sales
Key: mpnfsales
First_Name (1,1) Last_Name Phone
Contact
Lead
First Name: Marc = m
Last Name: Benioff = pnf (with double metaphone applied)
Phone: 1-415-555-1234 = 415555
Key: mpnf415555
Website City (_,6)
Account
Website: https://www.salesforce.com = salesforce.com
City: San Francisco = sanfra
Key: salesforce.comsanfra
Website ZIP (1,3)
Account
Website: https://www.salesforce.com = salesforce.com
ZIP: 94105-5188 = 941
Key: salesforce.com941
Custom matching rules may also use these pre-defined match keys. For example, assume the matching rule equation for a custom contact matching rule is (
First Name
 AND 
Last Name
 AND 
Company
), and the Fuzzy matching method is selected for at least one of the fields. Then, the notation for its match key will be: First_Name (1,1) Last_Name Company (2,6).
See Also:
Matching Rule Reference
Matching Criteria for Matching Rules
Normalization Criteria for Matching Rule Match Keys
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=matching_rules_match_key_process.htm&language=en_US
Release
202.14
