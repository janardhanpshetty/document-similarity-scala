### Topic: Define a Custom Fiscal Year | Salesforce
Define a Custom Fiscal Year | Salesforce
Define a Custom Fiscal Year
Set up your company’s custom fiscal years to fit your company’s calendar. If you define a custom fiscal year and need to change it, edit the existing fiscal year definition.
Available in: Salesforce Classic
Available in: 
All
 Editions except for 
Database.com
.
User Permissions Needed
To view fiscal year:
“View Setup and Configuration”
To change your fiscal year:
“Customize Application”
Before defining a custom fiscal year, enable custom fiscal years. See 
Set the Fiscal Year
 for more information.
Before defining or editing any custom fiscal years, be aware of its impact on forecasting, reports, and other objects by reviewing 
Fiscal Years
.
Custom fiscal years cannot be deleted.
Define a New Custom Fiscal Year
From Setup, click 
Company Profile
 | 
Fiscal Year
.
Click 
New
. The Custom Fiscal Year template dialog opens.
Choose a template and click 
Continue
 to close the Custom Fiscal Year template dialog. For more information on the templates, see 
Choosing a Custom Fiscal Year Template
.
Set the fiscal year start date, the fiscal year name, and choose the week start day. You can also add a description for the fiscal year.
Note
If this is the first custom fiscal year you have defined, the 
Fiscal Year Start Date
 and the 
Week Start Date
 are set to today's date and day of week. If you have already defined a custom fiscal year, they will be set to the day after the last end date of your custom fiscal years.
To make changes other than the start date, year name, or week start day, see 
Customize the Fiscal Year Structure
.
Optionally, review the fiscal year definition by clicking on 
Preview
.
If it is correct, close the preview and click 
Save
 to save your fiscal year, or 
Save & New
 to save your fiscal year and define another fiscal year.
Warning
If your company uses forecasting, 
creating the first custom fiscal year deletes any quotas and adjustments in the corresponding and subsequent standard fiscal years.
Edit a Custom Fiscal Year
From Setup, click 
Company Profile
 | 
Fiscal Year
.
Click a defined fiscal year name to review the details. Close the fiscal year preview to continue.
Click 
Edit
 for the fiscal year you want to edit.
Change the 
Fiscal Year Start Date
, the 
Fiscal Year Name
, 
Description
, or 
Week Start Day
.
If changing the 
Fiscal Year Start Date
 causes this fiscal year to overlap with the previous fiscal year, or if it creates a gap between the fiscal years, the end date of the previous fiscal year is changed to the day before the start of this fiscal year.
If changing the end date causes this fiscal year to overlap the next fiscal year, or if it creates a gap between the fiscal years, the start date of the next fiscal year changes to the day after the end of this fiscal year.
Note
You cannot change the start or end date of a fiscal year that causes it to overlap with a fiscal year that is defined using a Gregorian year template.
Warning
If you change the start or end date of any quarter, period, or week, all forecast data (including quotas, forecast history, and forecast adjustments) that are within that date range, and all forecasts for date ranges automatically adjusted as a result of that change, will be lost. This includes end or start date changes resulting from inserting or deleting periods.
Click 
Preview
.
Review the fiscal year definition. If it is correct, close the preview and click 
Save
 to save your fiscal year. To make more detailed edits, see 
Customize the Fiscal Year Structure
.
Note
Unless you specify them, the fiscal year period labels for forecasting and reporting are set by the default label values for the fiscal year periods. To change them, see 
Customize the Fiscal Year Labels
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_define_cfy.htm&language=en_US
Release
202.14
