### Topic: View and Add Dependent Components to a Change Set | Salesforce
View and Add Dependent Components to a Change Set | Salesforce
View and Add Dependent Components to a Change Set
A dependency is a relationship where one or more components must exist for another component to exist. Add dependent components to a change set, unless the dependent components exist in every org where this change set is deployed.
To add dependent components to an outbound change set:
From Setup, enter 
Outbound Change Sets
 in the 
Quick Find
 box, then select 
Outbound Change Sets
.
In the Change Sets list, click the name of a change set.
Click 
View/Add Dependencies
.
On the Component Dependencies page, select the dependent components you wish to deploy and click 
Add to Change Set
.
Warning
If your change set contains more than 2500 dependencies, you can see only the first 2500 in the Component Dependencies page.
See Also:
Select Components for an Outbound Change Set
Upload an Outbound Change Set
Components Available in Change Sets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=changesets_outbound_dependencies.htm&language=en_US
Release
202.14
