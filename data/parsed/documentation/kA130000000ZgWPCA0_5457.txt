### Topic: Packaging Considerations for Salesforce Connect—All Adapters | Salesforce
Packaging Considerations for Salesforce Connect—All Adapters | Salesforce
Packaging Considerations for Salesforce Connect—All Adapters
Some special behaviors and limitations affect packaging of external data sources and their dependent external objects and custom fields.
Available in: both Salesforce Classic and Lightning Experience
Available in: 
Developer
 Edition
Available for an extra cost in: 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
After installing an external data source from a managed or unmanaged package, the subscriber must re-authenticate to the external system.
For password authentication, the subscriber must re-enter the password in the external data source definition.
For OAuth, the subscriber must update the callback URL in the client configuration for the authentication provider, then re-authenticate by selecting 
Start Authentication Flow on Save
 on the external data source.
Certificates aren’t packageable.
 
If you package an external data source that specifies a certificate, make sure that the subscriber org has a valid certificate with the same name.
In managed and unmanaged packages, external objects are included in the custom object component.
If you add an external data source to a package, no other components are automatically included in the package.
If you add an external object to a package, then list views, page layouts, custom fields, the custom object component that defines the external object, and the external data source are automatically included in the package. If the external object has lookup, external lookup, or indirect lookup relationship fields, the parent objects are also automatically included in the package.
If external data source access is assigned in a permission set or in profile settings that you add to a package, the enabled external data sources are automatically included in the package.
If an external object is assigned in a permission set or in profile settings that you add to a package, then custom fields, list views, page layouts, the custom object component that defines the external object, and the external data source are automatically included in the package.
See Also:
Considerations for Salesforce Connect—All Adapters
Salesforce Connect
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=platform_connect_considerations_packaging.htm&language=en_US
Release
202.14
