### Topic: Email cannot be changed for an inactive user
Can we change email for Inactive user without sending Email Change Verification ?
 
Resolution
If Email Change Verification is active, the email address on an inactive User record cannot be changed.
If a System Administrator attempts to change the email address, the system will allow the record to be saved, but the change will 
not
 take place as the system will not send a change verification email to the user. 
If Email Change Verification is disabled, the email address on an inactive user can be changed, provided the new email address is on one of the allowed domains. 
To Disable "Email Change Verification" please refer to the following Article : 
https://help.salesforce.com/apex/HTViewSolution?urlname=Disabling-Email-Change-Verification&language=en_US
 
