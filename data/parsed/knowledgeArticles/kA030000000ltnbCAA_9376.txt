### Topic: A quick guide to troubleshoot various performance issues on Salesforce1 app including one.app
How To Troubleshoot Various Performance Issues On Salesforce1 App Including one.app
Resolution
Lets start by few basic questions which are really important in order to resolve or get to the root cause of the issue:
Exact Issue
Steps To Reproduce
Is This Reproducible On Both Hybrid App And Also On One.app (mobile/desktop)
If On Hybrid App; Get The Following Information:
Platform: iOS or Android or Windows
Version Of The Salesforce1 App
OS Version Of The Device
Type Of Connectivity?
3G; 4G; LTE
WiFi
Does This Happen When Connected To Their VPN?
Now; Lets drill down a bit more by each question:
Exact Issue:
Call/Email the customer and make a note of the exact issue. Sometimes they tend to say that the app is working slow. We need to determine as to when is it behaving slow. In order to do that, obtain the "Steps To Reproduce".
Steps To Reproduce:
This is the most crucial part of the investigation. Make sure, that we capture the exact Steps To Reproduce.
A good example would be: 
Launch the Salesforce1 Hybrid app on the iPhone 6 plus device
Go to the Navigation Menu
Click on Entity named: Deal Alert
Click on New Button 
Notice the time taking to load the page
Details To Be Included:
New Button is over ridden with a VisualForce Page named: VF_Deal_Alert_New
Link to VF page: https://ap1.salesforce.com/xxxxxxxxxx
The VF Page is accessible by all the profiles
It contains the following APEX classes in it (Provide the links/names of the APEX Classes)
Or if there is no VF page over ridden, mention the Record Type assigned to the Profile 
If just on the Hybrid App; try the following steps:
Platform: iOS or Android or Windows
Version Of The Salesforce1 App - 
If it's not the new version; ask the customer to install the latest version and check. 
OS Version Of The Device - If the OS is not upto date, check with the customer if they can upgrade it. If not, proceed with further steps.
Type Of Connectivity:
3G/4G/LTE: (Even for the WiFi, you can perform the below steps)
Check the signal strength. Make sure that the device has at least 3-4 Signal Bars while accessing and working with Salesforce1 App
If possible, have the customer install the following apps on their devices in order to rule out network connectivity issue;
Note: Make sure to check with the customer if they can install the apps on their mobile devices. Few customers have restrictions where they cannot install other apps without checking with their IT team.
iOS:
 
iNet Tools
Once installed, navigate to the option named: Ping
Perform the Ping test to their instance:
Have the customer email the results to you
Now go back to the menu and select: TraceRoute
Have them enter their instance once again and perform the test
Have them email the results once again to you
Go through the logs and see where the packets are getting dropped and how many.
Android:
 
Ping & DNS
Select either Ping or TraceRoute Option from the Dropdown and perform the same steps mentioned for iOS. The UI is completely different, see the screenshots:
If this happens due to a VisualForce Page on a Hybrid app, you can follow the below steps on your laptop in order to make a note as to which component is taking a lot of time to load even on the browser on a machine or you can follow the steps outlined in this Internal KB article: 
INTERNAL - How to debug Salesforce1 Android Hybrid App
Make sure that you are performing the below steps on Google Chrome:
Obtain the Login Access
Go to User's Profile on BT
Login to the profile
Now change the URL to one.app; example: https://ap1.salesforce.com/one/one.app
Go to settings on the Google Chrome and select more tools
Now select Developer Tools
Navigate to the exact step where the issue is occurring
Make sure you select: Console; Mobile UI and delete the existing errors and logs
Once this is loaded, make a note of the errors and then select "Network" tab:
Have a look at the column named: Time Latency and see which component is taking more time
Sometimes it might due to a Javascript involved or Aura Component failing to load properly.
Investigate on the particular information further.
Does this happen on first login? or after the app is cached with Data? 
Are other applications running at the same time as Salesforce1?  Compare the performance with all other apps closed. 
Ensure a VPN or other Proxy server is not involved in the connection.
Note: This KB is still in the process of getting updated with more information. Stay tuned.
