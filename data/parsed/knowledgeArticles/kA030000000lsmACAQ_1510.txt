### Topic: Detailed steps on how to update the URL for existing Goals and Metrics via Workbench.
Internal Only:
 Steps on how to update the URL for existing metrics via Workbench:
1. Log in to Workbench - 
https://workbench.developerforce.com/login.php
2. Select your Production org and API version 32.0 then click the login with Salesforce button
3. Select queries | SOQL Query from the top menu
4. Enter the following query in the 'Enter or modify a SOQL query below: ' text-area:
 
SELECT Id, ActualValueExternalUrl FROM WorkGoal WHERE Name = 'k1' AND (Type = 'KeyResult' OR Type = 'Metric')
Replace the Name k1 with the name of your metric (surrounded by single quotes)
5. Hit the Query button
6. Click and drag to copy all the values in the generated table then paste it in Excel
7. You should have two columns, one for the Id and another for the ActualValueExternalUrl. ActualValueExternalUrl should contain the incorrect URL, so update it with the valid one and save the file as a CSV file.
8. Go back into Workbench and select data | Update from the top menu
9. Select WorkGoal for Object Type then Browse for the CSV file you just saved.
10. Hit Next
11. Map the fields to the matching columns in your CSV file - 
http://www.screencast.com/t/7TK9FTFjzWn
12. Hit the Map Fields button
13. Hit Confirm Update and the records should be updated.
 
Resolution
