package similarity.ml

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{NGram, RegexTokenizer}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, udf}

import scala.collection.immutable.HashSet


/**
 * Created by Janardhan Shetty on 9/9/16
 * spark-submit --class similarity.ml.DocumentSimilarityDF --master local[8] --driver-memory 8G /Users/jshetty/spark-applications/documentsimilarity/target/scala-2.11/documentsimilarity_2.11-1.0.jar arg0 arg1 agr2
 */

 object DocumentSimilarityDF {

  // Udf
  val jaccard = udf { (set1: Seq[Long], set2: Seq[Long]) =>
    val lSet1 = HashSet() ++ set1
    val lSet2 = HashSet() ++ set2
    val percent = (1.0F * lSet1.intersect(lSet2).size / lSet1.union(lSet2).size) * 100
    percent
  }

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder.master("local[12]").appName("Document Similarity").getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext
    import sqlContext.implicits._
    sqlContext.setConf("spark.sql.crossJoin.enabled", "true")

    val numGrams = 5
    val seed = 32
    val threshhold = 15.0

    // RDD
    val documentPath = "/Users/jshetty/spark-applications/documentsimilarity/data/parsed/documentation/*"
    val knowledgeArticlePath = "/Users/jshetty/spark-applications/documentsimilarity/data/parsed/knowledgeArticles/*"
    val resultPath = "/Users/jshetty/documentsimilarity/submission/result"
    val documentationInput = sc.wholeTextFiles(documentPath)
    val knowledgeArticlesInput = sc.wholeTextFiles(knowledgeArticlePath)

    // Trim fileNames, convert to DF and cache
    val documents = documentationInput.map(line => (line._1.split("/").last, line._2)).toDF("DocumentName", "DocumentContent").cache()
    val knowledgeArticles = knowledgeArticlesInput.map(line => (line._1.split("/").last, line._2)).toDF("ArticleName", "ArticleContent").cache()

    // Tokenize
    val documentTokenizer = new RegexTokenizer().setInputCol("DocumentContent").setOutputCol("documentWords").setPattern("\\W")
    val articleTokenizer = new RegexTokenizer().setInputCol("ArticleContent").setOutputCol("articleWords").setPattern("\\W")

    // Approach-1 : 5-gram + MurmurHashing + Jaccard Similarity
    val documentTokGram = new NGram().setInputCol("documentWords").setOutputCol("documentGrams").setN(numGrams)
    val articleTokGram = new NGram().setInputCol("articleWords").setOutputCol("articleGrams").setN(numGrams)

    // Try Hashing trick
    val documentHashing = new Hasher().setInputCol("documentGrams").setOutputCol("docGramsHashed").setSeed(seed)
    val articleHashing = new Hasher().setInputCol("articleGrams").setOutputCol("artGramsHashed").setSeed(seed)

    // Two-Pipelines
    val documentPipeline = new Pipeline().setStages(Array(documentTokenizer, documentTokGram, documentHashing))
    val articlePipeline = new Pipeline().setStages(Array(articleTokenizer, articleTokGram, articleHashing))

    val documentFit = documentPipeline.fit(documents).transform(documents)
    val articleFit = articlePipeline.fit(knowledgeArticles).transform(knowledgeArticles)

    val articleDoc = articleFit.join(documentFit)
    val jaccardDF = articleDoc.withColumn("score",jaccard(col("artGramsHashed"), col("docGramsHashed")))

    val result = jaccardDF.select("DocumentName", "ArticleName", "score").filter(s"score > $threshhold").sort($"score".desc).coalesce(1)
    result.write.csv(resultPath)
    sc.stop()

  }
}
