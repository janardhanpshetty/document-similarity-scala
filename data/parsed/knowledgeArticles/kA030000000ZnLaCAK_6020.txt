### Topic: Outlook user has issue clicking on email (mailto:) links within Salesforce and elsewhere in Internet Explorer browser
When clicking on a field containing email address, the default mail handler MS Outlook normally opens as expected to compose an email. However in the IE  browser, it attempts to load the page "mailto:emailladdress" where email address is the email I selected. IE Displays "Internet Explorer Cannot display page." 
Resolution
Ensure that the user wants to use Microsoft Outlook as their default email client 
 
1- To change this setting, on your keyboard press and hold down the Windows Key and type "Set Default Programs" - Click on the "Set Default Programs "action. 
From there, click on Microsoft Office Outlook. then "Choose defaults for this program" and then check the box next to "MAILTO" 
2) Make sure that Internet Explorer is in protected mode.  Steps for this vary by version of Internet Explorer, but it is found in Internet Options, Security Tab, Click on the applicable zone (i.e. Internet, Trusted Sites) and check the box to "Enable Protected Mode"
