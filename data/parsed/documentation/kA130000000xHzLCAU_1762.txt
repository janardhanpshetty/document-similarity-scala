### Topic: Add the Files Related List to Page Layouts | Salesforce
Add the Files Related List to Page Layouts | Salesforce
Add the Files Related List to Page Layouts
Add the Files related list to your page layouts for accounts, opportunities, leads, cases, and other objects so users can attach files to records while leveraging the rich features and flexibility of Salesforce Files. Use files instead of attachments to make the files available outside of the context of the individual record. The file owner or admin still decides sharing settings for the file.
Available in: Salesforce Classic and Lightning Experience
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, 
Contact Manager
, and 
Developer
 Editions
You can customize page layouts to include the Files related list. This allows users to add files to a record and see a list of files associated with the record such as a lead or a case.
From Setup, enter 
Object Manager
 in the 
Quick Find
 box, then select the name of the object you want to add the Files related list to (such as Opportunity).
Click 
Page Layouts
.
Click the page layout you want to customize.
In the list of available items in the left pane, click 
Related Lists
.
Drag 
Files
 to the Related Lists section.
Clic 
Save
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=admin_files_related_list_setup.htm&language=en_US
Release
202.14
