### Topic: Hiding the "Clone and Customize" button in content detail page
This is not a problem if you will assign a member user as a library administrator provided that the profile of that user is either delegated admin or system admin because they have the system and app permissions shown below. 
 
·         
Manage Salesforce CRM Content
·         
Create Libraries
·         
Manage Content Permissions
·         
Manage Content Properties
·         
Manage Content Types
They will be able to access Salesforce CRM Content in the setup page, and can disable “Enable content pack creation” setting after creating a content pack so that viewers will not be able to create content pack or have access to "Clone and Customize" button to customize content.
However, if you assign a standard user as library administrator, that user does not have access to "Salesforce CRM Content" in setup. It means that they are left with either the ability to create or customize content or not at all.
Resolution
 
The only way to assign these system and app permissions to a particular standard or regular user added as content library administrator is to use the Permission Set feature. Here are the steps:
 
·         
Click on your name
·         
Click on Setup
·         
Click on Manage Users
·         
Click on Permission Sets
·         
Click on New
·         
Type the desired label (e.g. Create Content Pack Permission). API name will be auto-populated
·         
Select “Salesforce” for the user license
·         
Click on Save
·         
Click on Edit button on top
·         
Enable the following app and system permissions:
 
·         
Manage Salesforce CRM Content
·         
Create Libraries
·         
Manage Content Permissions
·         
Manage Content Properties
·         
Manage Content Types
·         
Click on Save
     
 
·         
Find the user record that you want this permission set associated. Here are the steps:
 
·         
Click on your name
·         
Click on Setup
·         
Click on Manage Users
·         
Click on Users
·         
Click the name of the user
·         
Scroll down to the “Permission Set Assignments” section and click on “Edit Assignments” button
·         
Move the permission set that you created from “Available Permission Sets” window to “Enabled Permission Sets” window
·         
Click on Save
