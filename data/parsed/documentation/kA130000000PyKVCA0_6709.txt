### Topic: Reduction Orders | Salesforce
Reduction Orders | Salesforce
Reduction Orders
Use reduction orders to track requests to reduce, return, deactivate, or disable products or services for a given customer.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
A 
reduction order
 is an agreement between a company and a customer to process product returns, de-provision services, or reduce services that have been provisioned. For example, if a customer has purchased 30 items through an order and later requests to cancel that order, you can create a reduction order to track the request.
You can create multiple reduction orders for a single order; however, you can’t create a single reduction order for multiple orders. For example, if you need to reduce order products that were purchased through three activated orders, you need to create three reduction orders—one for each original order—even if all those orders were for the same account.
See Also:
Reduce Orders
Activation Limitations for Orders and Reduction Orders
Editing and Deletion Limitations for Orders and Reduction Orders
Order Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=orderreduction_overview.htm&language=en_US
Release
202.14
