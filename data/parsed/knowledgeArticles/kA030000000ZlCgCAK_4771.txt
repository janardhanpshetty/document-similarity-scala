### Topic: Displaying Images for Social Network sites for Leads in the Salesforce1 app
Prior to Spring 14, these images displayed for Accounts and Contacts only.
Resolution
If a Lead is linked to a Social Network profile, the profile image selected for the Lead displays when a User views the Lead in Salesforce1.
Profile images from LinkedIn appear ONLY when the user is logged into LinkedIn in the full Salesforce site; images from Facebook or Twitter might appear even if the user isn't currently logged in to those networks.
