### Topic: How can I make the country field a picklist on our web-to-lead form and update the standard text field in salesforce?
How can I make the country field a picklist on our web-to-lead form and update the standard text field in salesforce?
Resolution
To change the Country field from a text to a picklist, substitute the following for the Country field in the web-to-lead html form (the below example is only for the United States and Canada country codes):
 
<label for="country">Country</label><select  id="country" name="country">
 
<option value="">--None--</option>
 
<option value="US">US</option>
 
<option value="CA">CA</option>
 
</select><br>
 
<br>
 
Also Note:
 
If you have recently switched from using the text State and Country fields to State and Country as pick-list fields, you can generate a new Web to Lead form from within setup, which will contain HTML pick-list fields
