### Topic: Create Case Team Roles | Salesforce
Create Case Team Roles | Salesforce
Create Case Team Roles
Before you set up case teams or predefine case teams, create roles to determine team members level of access to cases.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To set up case teams:
“Customize Application”
AND
“Manage Users”
To add team members:
“Edit” on cases
You can create an unlimited number of case team roles, but we recommend no more than 20 so as not to overwhelm team members.
From Setup, enter 
Case Team Roles
 in the 
Quick Find
 box, then select 
Case Team Roles
.
Click 
New
, and enter the role’s name.
From Case Access, choose the role’s level of access to cases.
Read/Write
Members can view and edit cases and add related records, notes, and attachments to them.
Read Only
Members can view cases and add related records to them.
Private
Members can’t access cases.
If you want members in the role visible to customer portal users viewing cases, choose 
Visible in Customer Portal
. Even if Visible in Customer Portal isn’t chosen, customer portal users added to case teams can view themselves on Case Team related lists.
Click 
Save
.
Note
You can’t delete roles, but you can click 
Replace
 next to a role you want to replace across all cases. If your org has one role, you can’t replace it.
Tip
Roles don’t change a case owner’s access to cases, which is Read/Write by default.
Predefine Case Teams
After you define case team roles, you can predefine case teams so that support agents can quickly add people who they frequently work with to cases.
Set Up Email Alerts for Case Teams
Create email alerts for case teams so that each time a case is created or updated, team members are notified.
See Also:
Case Teams
Set Up Case Teams
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=caseteam_roles.htm&language=en_US
Release
202.14
