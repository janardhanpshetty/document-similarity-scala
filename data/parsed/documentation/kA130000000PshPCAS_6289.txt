### Topic: Enable and Configure Similar Opportunities | Salesforce
Enable and Configure Similar Opportunities | Salesforce
Enable and Configure Similar Opportunities
Allow users to find Closed/Won opportunities that match the attributes of an opportunity they're currently working on, so they can quickly access information that might help them close their open deals.
Available in: Salesforce Classic
Available in: 
All
 Editions for organizations activated before Summer ’09
Available in: 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions for organizations activated after Summer ’09
User Permissions Needed
To enable and configure similar opportunities:
“Customize Application”
Note
Searches are constructed as 
OR
 statements. Therefore, Closed/Won opportunities need to match only one criteria to be considered similar. For best search results, include multiple search terms.
When deciding which opportunity fields to display on the Similar Opportunities related list, be aware that users will see all fields that are displayed, regardless of sharing rules and user permissions.
From Setup, enter 
Similar Opportunities
 in the 
Quick Find
 box, then select 
Similar Opportunities
.
Click 
Edit
.
Select 
Enable Similar Opportunities
.
Select the fields or related lists that you want Similar Opportunities searches to match against, and then click 
Add
.
Select the fields to display in the Similar Opportunities related list.
Click 
Save
.
Make sure the Similar Opportunities related list is added to the opportunity page layout.
See Also:
Guidelines for Finding Similar Opportunities
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_oppsimilaropp.htm&language=en_US
Release
202.14
