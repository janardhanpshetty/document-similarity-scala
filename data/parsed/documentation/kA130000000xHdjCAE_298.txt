### Topic: Considerations for Shared Activities (Multiple Contacts on an Activity) | Salesforce
Considerations for Shared Activities (Multiple Contacts on an Activity) | Salesforce
Considerations for Shared Activities (Multiple Contacts on an Activity)
Use Shared Activities to let users relate multiple contacts to each event or task and to view information about open activities and activity history on contact detail pages. If you enable Shared Activities, or if it’s enabled by default in your Salesforce org, you can’t disable it. Whether you enable Shared Activities or leave it disabled, review these considerations.
Available in Lightning Experience in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Available in Salesforce Classic in: 
All
 Editions Except 
Database.com
Enabling Shared Activities
Limit on Number of Contacts
Your sales reps can relate up to 50 contacts to each event or task.
Time Needed to Enable Shared Activities
Enabling Shared Activities takes up to 48 hours, depending on the volume of activity in your Salesforce org. During this process, sales reps can continue working with events and tasks.
 The Activity Settings page in Setup displays messages about the status and what to do if the process doesn’t finish.
Display of Related Contacts Alongside Other Activity Details
To show related contacts on event and task detail pages, ensure that the Name related list is included on event and task page layouts. Be aware that the items in the Name related list aren’t fields, so they don’t adhere to field-level security settings.
Visualforce Display of Events
If a Visualforce page uses the standard controller to display Shared Activities events, use API version 26.0 or later.
Leaving Shared Activities Disabled
Limitations in Custom Reports Based on Activity Relationships
If you don’t enable Shared Activities, custom reports based on activity relationships work as follows.
They show only invitees to an event but not the organizer.
They don’t show events to which no one has been invited.
No Support for Activity Custom Lookups
If you don’t enable Shared Activities, custom report types that use activity custom lookup field relationships aren’t supported.
Duplicate Records Created for Email
If you don’t enable Shared Activities and a sales rep sends an email to multiple contacts, Salesforce creates a closed task for each contact. As a result, Salesforce creates duplicate records instead of just one task corresponding to the email.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=activities_shared_considerations.htm&language=en_US
Release
202.14
