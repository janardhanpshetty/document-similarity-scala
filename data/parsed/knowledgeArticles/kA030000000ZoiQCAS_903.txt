### Topic: Query activity fails and throws an error message which contains "String or binary data would be truncated." What does this mean and how do I fix it?
Query activity fails and throws an error message which contains "String or binary data would be truncated." What does this mean and how do I fix it?
Resolution
This is caused by the length of the data being returned exceeding the maximum size allowed for a field or fields in the target data extension. 
To fix this, find the field in question and increase the size of the field. 
Best practice would be to make a copy of the source data extension to be used as the target data extension and retain the maximum values. This ensures that the data returned from the source data extension does not exceed the max value of the target data extension.
One way to determine which field is throwing this error is to perform the following query on every field that has a maximum length set: 
(this example query is assuming we are pulling from the _Bounce data view for MID 10671779) 
select max(len(SubscriberKey)) as SubscriberKey, 
max(len(Domain)) as Domain, 
max(len(BounceCategory)) as BounceCategory, 
max(len(BounceSubcategory)) as BounceSubcategory, 
max(len(BounceType)) as BounceType, 
max(len(SMTPBounceReason)) as SMTPBounceReason, 
max(len(SMTPMessage)) as SMTPMessage, 
max(len(TriggererSendDefinitionObjectID)) as TriggererSendDefinitionObjectID, 
max(len(TriggeredSendCustomerKey)) as TriggeredSendCustomerKey 
from c10671779._Bounce with (nolock) 
This will return the largest record in each of the different fields. These numbers can then be compared with the maximum length set for each of these fields to determine the field where the error is occurring.
