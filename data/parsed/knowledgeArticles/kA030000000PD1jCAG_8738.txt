### Topic: This article would tell you how you can have the values displayed in a single currency within excel, after the data is exported from a report.
How to display a single currency after exporting from a report to excel?
Resolution
Add the "Amount (Converted)" field to the report.  After export to excel, you will see the converted amounts.
