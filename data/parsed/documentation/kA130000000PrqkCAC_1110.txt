### Topic: Create and Modify Content Packs in Salesforce CRM Content | Salesforce
Create and Modify Content Packs in Salesforce CRM Content | Salesforce
Create and Modify Content Packs in Salesforce CRM Content
Available in: Salesforce Classic
Available in: 
Contact Manager
, 
Group
, 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create or modify content packs in Salesforce CRM Content:
Manage Libraries
 checked in your library permission definition
OR
Add Content
 checked in your library permission definition
A 
content pack
 is a collection of related documents or files that are stored as a group in Salesforce CRM Content. For example, you 
may want to create a content pack with a product list, price quote, and contract to send to a particular customer
. Any file in Salesforce CRM Content can be added to a content pack, from traditional business documents such as Microsoft® PowerPoint presentations and Adobe® PDF files, to audio files, video files, and Google docs. 
Using email or instant messaging, you can distribute the content pack to colleagues in your organization or leads and contacts outside your organization. The recipient of a 
content delivery
 can click a single URL to open a preview player with which he or she can preview and download the content. You can then view tracking information to see how often the content pack was viewed and which documents were downloaded
. .
Note
Content packs support all files types; however, the preview player launched by the content-delivery URL displays only PowerPoint, Word, Excel, and PDF files. The preview player does not display copy-protected PDFs. Also, working with content packs requires Adobe Flash® Player, version 9.0.11.5 or higher. If you do not have Flash installed, Salesforce provides a link to Adobe's website where you can download Flash for free.
To work with content packs:
Depending on whether you want to create, customize, or modify a content pack, use one of the following options:
Note
The following options are available only if 
Enable content pack creation
 is selected on the Salesforce CRM Content page in Setup . If content pack creation is disabled after packs have been created, Salesforce doesn’t delete the packs, but they can’t be customized or modified.
To create a new content pack, click the 
Libraries
 tab and then choose 
Create New
 | 
Content Pack
.
To create a new content pack by copying an existing pack and adding, removing, or reordering files, open the content details page for the pack and click 
Clone & Customize
.
To update a content pack and publish a new version, open the content details page for the pack and click 
Edit
 | 
Edit Content Pack
.
Click 
Search files
 to display all the content in your libraries. To refine your results, select a specific library to search or enter a search term in the text box.
In addition to files and documents, search results also list content packs.
Drag the desired content from the search results into the assembly section in the lower half of the window. The following options help you assemble your content pack:
In the search results, click a document to preview it in the lower half of the window. Choose 
Add to Content Pack
 or 
Hide preview
 as needed.
In the search results, hover over a document and click the folder icon (
) to view the content packs that use the document.
In the search results, hover over a content pack and click the folder icon (
) to view all the documents in the pack.
In the assembly section, hover over a document and click the garbage can icon (
) to remove that document from the pack you are assembling.
Click 
Clear
 at any time to revert your changes; click 
Cancel
 to return to the Libraries tab.
Note
The maximum number of files that can be included in a content pack is 50.
When you are done assembling or modifying your content pack, click 
Save
 and assign or change the content pack's name.
In the Save or Publish Content dialog:
Select a library. If you do not want the content pack to be visible to other users in your organization, for example if your work is still in progress, choose 
Save in my personal library
.
Optionally, add or modify the content pack's description.
Optionally, choose a language. The 
Language
 drop-down list is displayed if multi-language support is enabled. If you do not choose a language, Salesforce associates your content with your personal language setting. If users restrict their content searches to a particular language, only content associated with that language is displayed in the search result set.
If you are modifying the content pack, complete the 
Reason for Change
 field.
To publish the content on behalf of another author, choose that author from the drop-down list.
Tag your content. Your tagging permission depends on the tagging rule assigned to the library:
If the library does not have a tagging rule or if your administrator assigned the open tagging rule, you can enter tags in the 
Tags
 field. As you type a tag, Salesforce CRM Content autosuggests tags based on your My Recent Tags list and the Popular Tags section on the Libraries tab. The My Recent Tags list on the Contribute window shows the 20 tags you have used most recently. Click a tag to add it to the 
Tags
 field automatically.
If your administrator assigned the guided tagging rule, you can choose from the list of suggested tags or enter a new tag. Click a suggested tag to add it to the 
Tags
 field automatically.
If your administrator assigned the restricted tagging rule, you must choose from the list of suggested tags. When you select a tag it turns green.
You can't change or delete tag names. You can remove tags from a document, but that doesn't delete the tag.
Tags are case insensitive. You can't have two tags with the same name even if they use different upper and lowercase letters. The case of the original tag is always used.
If multiple record types are available, choose one from the drop-down list. The record type determines which custom fields appear for you to categorize and define your content.
Click 
Publish
. You can then view the content details page, return to the Libraries tab, or publish another file.
See Also:
Viewing and Editing Content Deliveries
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=content_docpack_create.htm&language=en_US
Release
202.14
