### Topic: An internal article to track open and new Data Import Wizard Known Issues.
Note: It is the responsibility of each case owner to carefully review each Known Issue (KI) listed to confirm whether or not it may be applicable and explain a reported issue in a customer case.
It is Support's responsibility and not the customers to determine whether they may be affected by a KI.
If you are unsure whether a customer may be affected by a particular KI or have questions please reach out to your local subject matter expert or escalate to T3 for absolute confirmation 
before
 sending it to the customer and/or associating your case to the related Bug.
Once you've confirmed that the customer is affected with absolute certainty, populate the KI's Reference value into the case's GUS Bug# field to associate the case to the internal Bug record to track customer impact.
If you locate a Unified Data Import Wizard KI that is not listed here please perform a KBFeedback requesting to have it added.
 
Resolution
Summer 16' - Updating Campaign Member status using Data Import Wizard throws error saying status is not mapped
https://success.salesforce.com/issues_view?id=a1p3A00000182kn
Summer '16: Data Import Wizard incorrectly displaying relationship fields for matching criteria when importing custom object records
https://success.salesforce.com/issues_view?id=a1p3A00000181pWQAQ
Data Import Wizard does not recognize multiple external IDs on entity lookups
https://success.salesforce.com/issues_view?id=a1p3A000000E1SnQAK
Data Import Wizard on Lead / Custom Objects throws error "Something has gone wrong. Error in $A.getCallback() [[object Object]]"
https://success.salesforce.com/issues_view?id=a1p3A000000jkryQAA
Clicking on Launch Wizard! button via Data Import Wizard in Summer '16 throws generic error
https://success.salesforce.com/issues_view?id=a1p3A000000jkoBQAQ
Data Import Wizard mapping more than 65 fields may result in mapped fields not being populated
https://success.salesforce.com/issues_view?id=a1p3A0000017xYIQAY​
Data Import Wizard not updating Account when mapping Contacts using Name
https://success.salesforce.com/issues_view?id=a1p300000008ZLBAA2
Your import failed: Import for object Account did not succeed. Error received - null when matching contact lookup field's related records by name
https://success.salesforce.com/issues_view?id=a1p30000000eWgLAAU
"Account Currency is not mapped and is required in a multi-currency organization." when uploading new campaign members
https://success.salesforce.com/issues_view?id=a1p30000000jhz5AAA
Lead import as Campaign Members causes error "Your import failed:Import for object Lead did not succeed. Error received - null" 
https://success.salesforce.com/issues_view?id=a1p30000000eVDW
Data Import Wizard Recent Imports are displayed in GMT rather than user specific locale
https://success.salesforce.com/issues_view?id=a1p30000000jhZ7AAI
No option to select Person Account record types in Data Import Wizard
https://success.salesforce.com/issues_view?id=a1p30000000jhZCAAY
Person Accounts not available in Data Import Wizard
https://success.salesforce.com/issues_view?id=a1p30000000jhZHAAY
Spring 16' - Existing Contacts are not added to the campaigns as campaign members using New Data Import Wizard
https://success.salesforce.com/issues_view?id=a1p30000000eUmLAAU
Data Import Wizard cannot be opened in the Service Cloud Console
https://success.salesforce.com/issues_view?id=a1p30000000jhaKAAQ
Data Import Wizard: Inconsistent update of Lead/Campaign Member status values
https://success.salesforce.com/issues_view?id=a1p30000000jds6AAA
Importing Campaign Member via Data Import Wizard is always taking DEFAULT Campaign Member “status”
https://success.salesforce.com/issues_view?id=a1p300000008d2GAAQ
Spring 16'- When trying to click on wizard to import Campaign Members a system error is displayed
https://success.salesforce.com/issues_view?id=a1p30000000eUs9
Few orgs on Spring 16' are still showing options for legacy campaign import wizards
https://success.salesforce.com/issues_view?id=a1p30000000eUsE
Person Accounts not available in Data import wizard if Business Accounts are disabled for a non-admin profile
https://success.salesforce.com/issues_view?id=a1p30000000eWZU​
Winter 16 - New Data Import wizard showing inconsistent behavior in Contact Manager edition orgs on upsert operation
https://success.salesforce.com/issues_view?id=a1p30000000eOrRAAU
Duplicate Salesforce ID is displayed in Data Import Wizard
https://success.salesforce.com/issues_view?id=a1p30000000eMySAAU
Data Import Wizard: not recognizing account division pick list values
https://success.salesforce.com/issues_view?id=a1p30000000jglCAAQ
Contact imported via Data Import Wizard isn't associated to intended account 
https://success.salesforce.com/issues_view?id=a1p3A000000jkrjQAA
Campaign Member Status field unavailable on Data Import Wizard mapping page when importing Person Accounts
https://success.salesforce.com/issues_view?id=a1p3A000000jksNQAQ
Importing Campaign Members using Data Import Wizard logs the user out of the active session.
https://success.salesforce.com/issues_view?id=a1p3A000001843I​
Data Import Wizard - Custom Contact Fields are hidden from mapping page if there is no active Account record type
https://success.salesforce.com/issues_view?id=a1p3A000001856E
Data Import Wizard returns Duplicate Value error when adding Contacts or Campaign Members and matching by email
https://success.salesforce.com/issues_view?id=a1p3A000001856JQAQ
Data Import Wizard - Links to recommended Data Fields in Data Import Wizard is Broken returns URL No Longer Exists.
https://success.salesforce.com/issues_view?id=a1p3A000001856O
