### Topic: Delete Personal Tags for Deactivated Users | Salesforce
Delete Personal Tags for Deactivated Users | Salesforce
Delete Personal Tags for Deactivated Users
Your org can have up to 5,000,000 personal and public tags applied to records across all users. If your org is approaching this limit, delete personal tags for deactivated users.
Available in: Salesforce Classic
Personal Tag Cleanup available in: 
All
 Editions
User Permissions Needed
To delete personal tags for deactivated users:
“Customize Application”
From Setup, enter 
Personal Tag Cleanup
 in the 
Quick Find
 box, then select 
Personal Tag Cleanup
.
Select one or more deactivated users and click 
Delete
.
You can’t restore personal tags after you delete them.
See Also:
Topics and Tags Settings
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=customize_tag_cleanup.htm&language=en_US
Release
202.14
