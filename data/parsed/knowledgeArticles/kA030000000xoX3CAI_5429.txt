### Topic: Possible reasons why the reward tabs may be missing.
Why can't I see the Reward Funds or Reward Fund Types tabs?
Resolution
There are a couple of reasons why you may not be seeing the Reward Funds or Reward Fund Types tabs:
1. Tangible Rewards has not been enabled.  To enable tangible rewards have your admin do the following:
Login in to Salesforce and Navigate to 
Setup | Customize | Work.com | Settings
Check the box to enable 
Restrict Custom Badge Creators. By default, all users can create custom badges. Enable this setting to restrict badge creation to a subset of users. 
​
2. You do not have a Work.com license assigned to you.  Your admin can take the follow actions to assign you a Work.com feature license:
Login in to Salesforce and Navigate to 
Setup | Manage Users | Users
Search for your name in the list and click on it
Click the 
Edit
 button and enable the box next to the 
Work.com User
 field
 
