### Topic: I tried to reply but my email approval response wasn’t delivered. What should I do? | Salesforce
I tried to reply but my email approval response wasn’t delivered. What should I do? | Salesforce
I tried to reply but my email approval response wasn’t delivered. What should I do?
Approvers must have the “API Enabled” system permission to approve or reject approval requests via email.
 Contact your administrator.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=faq_email_approvals_my_reply_wasnt_delivered.htm&language=en_US
Release
200.20
