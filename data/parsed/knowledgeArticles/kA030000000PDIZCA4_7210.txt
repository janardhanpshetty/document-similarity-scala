### Topic: With the introduction of the Cross Filtering functionality in Enterprise, Unlimited and Developer Edition organizations now have access to run a report to identify these records. For our Group and Professional Edition customers there is no single report that shows accounts without contacts. However, here is a work around.
How can I create a report showing accounts without contacts?
Resolution
With the introduction of the Cross Filtering functionality in  
Enterprise, Unlimited and Developer Editions
. These organizations now have access to run a report to identify Accounts Without Contact records. The Cross Filters will permit you to include a sub-scope within an Accounts report to narrow the results to only those Accounts without Contacts.
1. On your Accounts report, click 
Customize
 to enter the Report Builder.
2. Click on the "
Add
" Multi-Button and select "
Cross Filters
."
3. In the Cross Filter section now displayed,
 select Accounts without Contacts
.
Once in place, it will narrow the scope of the report to only those Accounts lacking Contacts.
_________________________________________
For our 
Professional Edition 
customers there is no single report that shows accounts without contacts.  
However, there is a workaround:
1. Create a report new report type using Accounts as primary object and Contacts as secondary, choosing "
"A" records may or may not have related "B" records.
" logic. For further information about how to create Custom report types, please see the article 
Set Up a Custom Report Type
2. Create a new report using the new Custom report type that you just created. This can be done by going to the reports tab, clicking new report.. and choosing the label you put for your custom report type.
3. Select to show "all accounts", created date "all time"
4. add as report columns the account name, contact ID and account ID fields
5. Hover over the contact ID column until a little arrow pointing down appears, click the arrow. Select Sort ascending.
6. select show and make sure details is flagged.
7. run the report. the report will show you all accounts existing on the system (up to 2000 records. in order to see all accounts please download the report. For performance purposes, please consider splitting the reports into multiple report reducing the time frame) showing the ones without contacts on the top. If you export the report, you will see the contact ID empty on the records you are looking for.
_________________________________________
For 
Group edition customers
 you might need to:
1. Create a new report using the Accounts report type.
2. Select to show "all accounts", created date "all time"
3. add as report columns the account name and account ID fields, run the report and export it in excel
4. Create a new report using the "Contacts & Accounts" report type
5. Select to show "all accounts", created date "all time"
6. add as report columns the account name and account ID fields, run the report and export it in excel
7. Use the VLOOKUP function to match the ID that has a contact with the ones that do not have a contact attached. (
Please note: Salesforce support can not assist you with a VLOOKUP Function in Excel. Please refer to Microsoft Help for that functionality.
)
Please note: 
there should be no contacts without account as per the relationship that link the 2 objects. The only exception would be Person accounts and in order to create a report on person accounts you can simply add the filter "is Person account equals true".
 
