### Topic: This article provides an overview of Event Tracking
Creating and tracking events and registration is a common request. Whether it's a webinar or event, tracking registration and attendance is crucial for understanding campaign ROI. Without having to purchase additional products customers can utilize standard Salesforce functionality to develop an application up in no time.
Resolution
Process Overview
Key external pieces and Salesforce functionality: 
Build a Campaign associated with the Event/Webinar
Build an external Web registration form (sites page), containing that Campaign ID, so that all registrants will be captured as campaign members
If the Web Registration form is coded with the Campaign ID, it catches all registrants and adds them as "campaign members" to the Campaign Record, whether or not they are existing contacts.  
However, if you are only inviting existing leads and contacts to your event, you can code the link in the initial email invitation you send, (linking to the Reg Form), to pass the contact record ID to the Form (as a hidden field). That will then match the registrant (campaign member) to an existing contact record.
For using a Webinar, leverage an external service such as GoToWebinar (a division of GoToMeeting), ON24, Adobe Presenter (which is what Salesforce currently uses).
Record the Webinar, and build a separate campaign ID and registration form to track that.  
The Campaign record in Salesforce will allow for Reporting and Dashboard creation, associated with the Webinar/Event itself.  
Alternatively, if a customer does not have developer resources they can purchase a pre-built application from one of our partners off of the AppExchange.
 
ActevaRSVP - Event Registration & RSVP Tracking for Events & Training - 
http://sites.force.com/appexchange/listingDetail?listingId=a0N300000016YA7EAM
