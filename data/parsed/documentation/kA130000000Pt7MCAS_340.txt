### Topic: Assign Record Types and Page Layouts in the Enhanced Profile User Interface | Salesforce
Assign Record Types and Page Layouts in the Enhanced Profile User Interface | Salesforce
Assign Record Types and Page Layouts in the Enhanced Profile User Interface
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
Record types available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To edit record type and page layout access settings:
“Manage Profiles and Permission Sets”
In the enhanced profile user interface, Record Types and Page Layout Assignments settings determine the record type and page layout assignment mappings that are used when users view records. They also determine which record types are available when users create or edit records.
To specify record types and page layout assignments:
From Setup, enter 
Profiles
 in the 
Quick Find
 box, then select 
Profiles
.
Select a profile.
In the 
Find Settings...
 box, enter the name of the object you want and select it from the list.
Click 
Edit
.
In the Record Types and Page Layout Assignments section, make changes to the settings as needed.
Setting
Description
Record Types
Lists all existing record types for the object.
--Master--
 is a system-generated record type that's used when a record has no custom record type associated with it. When 
--Master--
 is assigned, users can't set a record type to a record, such as during record creation. All other record types are custom record types.
Page Layout Assignment
The page layout to use for each record type. The page layout determines the buttons, fields, related lists, and other elements that users with this profile see when creating records with the associated record type. Since all users can access all record types, every record type must have a page layout assignment, even if the record type isn't specified as an assigned record type in the profile.
Assigned Record Types
Record types that are checked in this column are available when users with this profile create records for the object. If 
--Master--
 is selected, you can't select any custom record types; and if any custom record types are selected, you can't select 
--Master--
.
Default Record Type
The default record type to use when users with this profile create records for the object.
The Record Types and Page Layout Assignments settings have some variations for the following objects or tabs.
Object or Tab
Variation
Accounts
If your organization uses person accounts, the accounts object additionally includes 
Business Account Default Record Type
 and 
Person Account Default Record Type
 settings, which specify the default record type to use when the profile's users create business or person account records from converted leads.
Cases
The cases object additionally includes 
Case Close
 settings, which show the page layout assignments to use for each record type on closed cases. That is, the same record type may have different page layouts for open and closed cases. With this additional setting, when users close a case, the case may have a different page layout that exposes how it was closed.
Home
You can't specify custom record types for the home tab. You can only select a page layout assignment for the --Master-- record type.
Click 
Save
.
See Also:
How is record type access specified?
Assign Custom Record Types in Permission Sets
Work in the Enhanced Profile User Interface Page
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=users_profiles_record_types.htm&language=en_US
Release
202.14
