### Topic: You can contact our Customer Service Department by completing our Billing Inquiry Form, by email, and by phone. You can access this form from your emailed Invoice or this address:
Our Customer Service Representatives are available during regular business hours.  
 
Please have an Account Number or your Contract and Invoice number ready to facilitate your request. This information is all available in the top right hand side of your invoice.
Resolution
Customers in the United States, Canada, and Latin America can email 
billing@salesforce.com
 or call +1-415-901-8457 (US) or 1-800-no-software (1-800-667-6389) 
Customers in Europe, the Middle East, or Africa can email 
billing@emea.salesforce.com
 or call +44 1753 422153
Customers in Asia Pacific and Australia can email 
billing@apac.salesforce.com
 or call +65 6302 5700 (Asia Pacific) or +612 9394 7377 (Australia)
Customers in Japan can email 
billing@jp.salesforce.com
 or call +03-4222-1510
