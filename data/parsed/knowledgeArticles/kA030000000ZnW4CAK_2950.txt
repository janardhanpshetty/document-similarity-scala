### Topic: Microsoft Dynamics CRM 2011 presents the error "HttpContent(ContentType = "text/html; charset=utf-8", GetLength() = 6048, ... streamed data ...) was already read; you must call LoadIntoBuffer() to allow multiple read or write call"
Microsoft Dynamics CRM 2011 send presents an error after importing subscribers. The error is:
HttpContent(ContentType = "text/html; charset=utf-8", GetLength() = 6048, ... streamed data ...) was already read; you must call LoadIntoBuffer() to allow multiple read or write call
Resolution
This type of error indicates a validation error in the email. Review  your email to see identify whether there are any personalization strings in the email that are not included in the chosen field mapping set. If there are, you can resolve the error by modifying the field mapping set to include the personalization value or remove the personalization string from the email.
 
Related Topics
Field and Email Mappings
Email - Personalization Strings
Email - What is personalization?
