### Topic: Solution History | Salesforce
Solution History | Salesforce
Solution History
Available in: Salesforce Classic
Available in: 
Professional
, 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
The Solution History related list of a solution detail page tracks changes to the solution. Any time a user modifies any of the standard or custom fields whose history is set to be tracked on the solution, a new entry is added to the Solution History related list. All entries include the date, time, nature of the change, and who made the change. Modifications to the related lists on the solution are not tracked in the solution history.
See Also:
Solutions Overview
Solution Fields
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=solution_history.htm&language=en_US
Release
202.14
