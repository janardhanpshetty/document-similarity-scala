### Topic: Complete Setting up the Wave Analytics Platform | Salesforce
Complete Setting up the Wave Analytics Platform | Salesforce
Complete Setting up the Wave Analytics Platform
Enhance the Wave platform user experience and fine-tune Wave access to Salesforce data with optional setup procedures.
Enable Preview Thumbnails for All Lenses and Dashboards
Make it easier for users to find their lenses and dashboards on the home page and on app pages. Replace the generic icons with preview thumbnails of the contents of all lenses and dashboards, including those assets that have row-level security restrictions.
Enable Wave Analytics for Communities
Securely share Wave Analytics apps with community partner and customer users.
Enable Downloading from Wave Analytics
Enable users to download the results from lens explorations and dashboard widgets as images (
.png
), Microsoft® Excel® (
.xls
), or comma-separated values (
.csv
) files.
Grant Users Access to the Flex Dashboard Designer
To grant users access to the flex dashboard designer, enable the feature and assign the “Create and Edit Wave Analytics Dashboards” user permission to the users. If you assign the permission but don’t enable the feature, the users can’t access the new designer, but they can access the old one.
Enable Annotations on Dashboard Widgets
Annotate dashboard widgets with comments posted in the dashboard and in Chatter.
Enable the Wave REST API
You can access Wave Analytics datasets and lenses programmatically using the Wave REST API. Selecting this option overrides the "APIEnabled" permission for individual users and gives all Wave users (including Community users) access to the Wave API.
Connected App for Wave Analytics for iOS
Install the Analytics for iOS Connected App to allow your mobile clients to easily connect to your org. The Connected App gives you control over who’s logging in and how your mobile clients share images and links.
Wave Analytics Security Implementation Guide
Wave Analytics has different levels of security that your organization can implement to ensure that the right user has access to the right data.
Salesforce Data Access in Wave Analytics
Wave Analytics requires access to Salesforce data when extracting the data and also when the data is used as part of row-level security. Wave Analytics gains access to Salesforce data based on permissions of two internal Wave Analytics users: Integration User and Security User.
Parent topic:
 
Wave Platform Setup
Previous topic:
 
Assign Wave Analytics Permission Sets to Users
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=bi_help_setup_optional.htm&language=en_US
Release
202.14
