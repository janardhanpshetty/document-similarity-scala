### Topic: How to address SOS issues
We have several customers complain about SOS issues. During a sev1 or a sev0 with SOS we need to ensure we diagnose quickly and provide quality data while escalating to our R&D team. The below template and playbook is a bare minimum that we absolutely need while creating an investigation
 
Resolution
When creating an investigation for the SOS team this is the minimum set of requirements:
*******SOS investigation Template***********
 
Issue Summary:
 
Business Impact:
Configuration details:
orgId
User/Agent Id:
Steps to reproduce the issue:
Investigation:
1. What was found in the SOS dashboard
 
2. Check maintenance schedule for Live Agent by going to this link: 
https://trust.salesforce.com/trust/calendar
 
3.  Session Ids
Session lifecycle from Splunk
 
Leverage Splunk for viewing the SCRT logs (server side component of the SOS product)
 
Lookup by sosSessionId, it is the one starting with ‘0NX’
Sumologic
Go to 
sfdc.co/sumlogic
 (SSO login)
Fetch correlation Ids from sosSessionId: _sourceCategory=sosapi_tel_prod “id=SESSION” <sosSessionId>
Fetch all logs from 2 correlation Ids (one for client and other for Agent):  _sourceCategory=sosapi_tel_prod “correlation_id=<correlation-ID-1>” OR “correlation_id=<correlation-ID-2>”
More details on reading logs here: 
http://sfdc.co/troubleshooting-sos
 
Splunk
Go to 
https://splunk-web.crz.salesforce.com/
 (Login required)
Run the following query to get all Liveagent stack logs ordered by time: (Please replace sfdcSosSessionId value in 5 places)
 
index="distapps" sourcetype=CASE(liveagentserverV1:*)  logRecordType=lasos [search index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lasos sfdcSosSessionId=0NXD00000004ClM action=CLIENT_CREATE_CORE_SOS_SESSION_RETURNED | dedup sessionId | table sessionId] | table _time, action, _raw | append [ search
index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lasos sfdcSosSessionId=0NXD00000004ClM [search index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lasos sfdcSosSessionId=0NXD00000004ClM action=AGENT_ACCEPT_REQUEST_ENTRY | dedup sessionId | table sessionId] | table _time, action, _raw] | append [ search
index="distapps" sourcetype=CASE(liveagentserverV1:*) (logRecordType=G OR logRecordType=gslog OR logRecordType=gglog) ([search index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lasos sfdcSosSessionId=0NXD00000004ClM action=AGENT_ACCEPT_REQUEST_ENTRY | dedup sessionId | table sessionId] OR [search index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lasos sfdcSosSessionId=0NXD00000004ClM action=CLIENT_CREATE_CORE_SOS_SESSION_RETURNED | dedup sessionId | table sessionId]) | table _time, action, _raw] | sort by _time
 
 
Session lifecycle from sumologic
Sample query:
_sourceCategory=sosapi_tel_prod "id=SESSION" 0NX123456789
Sample session id 0NX123456789
 
4. What were the result of the endpoints
Check maintenance schedule for Live Agent by going to this link: 
https://trust.salesforce.com/trust/calendar
https://sos-status-monitor.herokuapp.com/test-sos
                             *NOTE: This only checks the health of NA14*
If the message on the screen is "SOS session created Successfully!" ignore the false alarm. If it is anything else, jump to 4.
Pinging Health Check Endpoint                                                           *NOTE: Can check health of all SuperPods*
Use PostMan or any http api client
GET {{liveAgentServer}}/content/_internal/api/sosMonitor
The {{liveAgentServer}} host name is found by taking the LA VIP host name from the LA GUS and PROD Infrastructure document and prepending “d.” To the name.
For Intuit’s example, liveAgentServer = 
https://d.la1w1.salesforceliveagent.com
If createOpenTokSession succeeds you should get the following: openTokSessionId={id}
Otherwise the returned error message(s) should give clues on what went wrong
 
5. Check to ensure customer is not hitting sos limits
 
Splunk Queries For Various Org Limit Checks
Approx SCRT Endpoint hourly limit check
index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lauri earliest=-1h SosRequest organizationId=00DE0000000a8TL | stats count
 
Approx number of GoInstant API on Org in Core (Inferred from hourly check above)
index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lauri earliest=-24h SosRequest organizationId=00DE0000000a8TL | stats count
 
If rate limit exceeded for any Org in last 24 hrs
index="distapps" sourcetype=CASE(liveagentserverV1:*) (logRecordType=G OR logRecordType=gslog OR logRecordType=gglog) earliest=-24h "Api request rate limit exceeded*"
 
Approx number of Omni routings for any Org in last 1 hr
index="distapps" sourcetype=CASE(liveagentserverV1:*) logRecordType=lapsr action="ADDED" earliest=-1h organizationId=00DE0000000a8TL | stats count
********* end of template ******
For additional debugging assistance can be found here:
Additional Playbooks:
https://docs.google.com/document/d/1-Dp7rMhwvZ9d6Heg6xgYgn3jXj-Ewpd6LJ3zJ_gmvSs/edit
# 
Sumo Logic dashboards: 
Androic dashboard: 
https://service.sumologic.com/ui/dashboard.html?f=140186283&t=d
 
SOS dashboard: 
https://service.sumologic.com/ui/dashboard.html?f=54168731&t=r
Once Investigation is created, please post in  “Intuit Peak2” 
chatter
 group with following info: timeline, investigation #, who is Incident manager quarterbacking this incident (this should be someone from MCS, and MCS decides this before reaching out to R&D), what are the swimlane actions when is next update Please refrain adding large group of  people to gchats. For Sev1 and 2 incidents, updates should be provided hourly   Once Incident resolved, please clearly call that out in chatter group. Only R&D managers or ICs should directly contact ICs.   There have been cases where ICs actively working the problem receive many @ mentions from many angles.   Incident quarterback can obtain information from assigned R&D manager.
