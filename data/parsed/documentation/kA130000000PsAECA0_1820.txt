### Topic: Force.com Sites Overview | Salesforce
Force.com Sites Overview | Salesforce
Force.com Sites Overview
Available in: Salesforce Classic
Available in: 
Developer
, 
Enterprise
, 
Performance
, and 
Unlimited
 Editions
Salesforce organizations contain valuable information about partners, solutions, products, users, ideas, and other business data. Some of this information would be useful to people outside your organization, but only users with the right access and permissions can view and use it. In the past, to make this data available to the general public, you had to set up a Web server, create custom Web pages (JSP, PHP, or other), and perform API integration between your site and your organization. Additionally, if you wanted to collect information using a Web form, you had to program your pages to perform data validation.
With Force.com sites, you no longer have to do any of those things. 
Force.com Sites enables you to create public websites and applications that are directly integrated with your Salesforce organization—without requiring users to log in with a username and password.
 
You can publicly expose any information stored in your organization through a branded URL of your choice. You can also make the site's pages match the look and feel of your company's brand.
 Because sites are hosted on Force.com servers, there are no data integration issues. And because sites are built on native Visualforce pages, data validation on collected information is performed automatically. You can also enable users to register for or log in to an associated portal seamlessly from your public site.
Note
Force.com Sites is subject to these additional 
Terms of Use
.
For information on Site.com, which is a Web content management system (CMS) that makes it easy to build dynamic, data-driven Web pages and edit content in real time, see 
Site.com Overview
.
The following examples illustrate a few ways that you can use sites:
Create an ideas site—Use sites to host a public community forum for sharing and voting on ideas about your company, services, or products. Ideas websites can be made public using sites.
Publish a support FAQ—Provide helpful information on a public website where customers can search for solutions to their issues.
Create a store locator tool—Add a public tool to your portal that helps customers find stores in their area.
Publish an employee directory—Add an employee directory to your company's intranet by creating a site restricted by IP range.
Create a recruiting website—Post job openings to a public site and allow visitors to submit applications and resumes online.
Publish a catalog of products—List all of your company's products on a public website, with model numbers, current prices, and product images pulled dynamically from your organization.
Because Force.com sites are served directly from the Salesforce organization, a site's availability is directly related to the organization's availability. During your organization's maintenance window for major releases, your sites will be unavailable; users who try to access a site will see a Force.com-branded maintenance page or your custom 
Service Not Available Page
. It's a good idea to inform your site users of the release maintenance windows and related sites unavailability in advance. You can view specific maintenance windows, listed by instance, at 
trust.salesforce.com/trust/status/#maint
.
The Force.com Domain
For each of your sites, you determine the URL of the site by establishing the site's domain name. You can choose one of the following domain options:
Use your Force.com domain name, which is your unique 
subdomain prefix
 plus 
force.com
. For example, if you choose 
mycompany
 as your subdomain prefix, your domain name would be 
http://mycompany.force.com
. The name is case sensitive.
Note
Your Force.com domain name is used for all the sites that you create. For example, your company could create one public site for partners, another for developers, and a third for support. If your company's domain is 
http://mycompany.force.com
, those three sites might have the following URLs:
http://mycompany.force.com/partners
http://mycompany.force.com/developers
http://mycompany.force.com/support
Create a branded, custom Web address, such as 
http://www.mycompanyideas.com
, by registering through a domain name registrar. Create CNAME records to redirect your branded domain and subdomains to your Force.com domain without exposing the 
force.com
 name in the URL. It can take up to 48 hours for your Force.com domain to become available on the Internet.
 
Custom Web addresses aren't supported for sandbox or Developer Edition organizations.
Note
The format of the secure URLs for your Force.com sites depends on the organization type or Edition. Your unique subdomain prefix is listed first, followed by Edition or environment type, then instance name and the force.com suffix. In the following examples, the subdomain prefix is “mycompany,” the sandbox name is “mysandbox,” the instance name is “na1,” and the sandbox instance name is “cs1”:
Organization Type
Secure URL
Developer Edition
https://mycompany-developer-edition.na1.force.com
Sandbox
https://mysandbox-mycompany.cs1.force.com
Production
https://mycompany.secure.force.com
The subdomain prefix for Developer Edition is limited to 22 characters. The secure URL is displayed on the Login Settings page. The URL is case sensitive.
See Also:
Setting Up Force.com Sites
Managing Force.com Sites
Force.com Sites Limits and Billing
Administrator setup guide: Force.com Sites Implementation Guide
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=sites_overview.htm&language=en_US
Release
202.14
