### Topic: Users receive an error message when they click on a dashboard component if they profile does not give access to the object(s) referenced in the report.
Sometimes you will notice that users are able to view dashboards, but upon clicking on a specific dashboard component, the following error message is thrown:
Unable to Access Page
 
The value of the parameter contains a character that is not allowed or the value exceeds the maximum allowed length. 
Resolution
This normally happens when the Profile of the user in question does not have access to the object(s) the report is based on.
Examples:
- The report is based off Cases and the user's profile does not have Read access to cases.
- The Dashboard uses a Field that is not accessible to the user via field level security. For reference, you can read through this link:
Field-Level Security Overview
In either scenario, you need to edit the user's profile and give them at least Read access to the object(s) in the report/dashboard. 
