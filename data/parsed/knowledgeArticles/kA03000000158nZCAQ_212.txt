### Topic: To be used when Contacts appear to be stuck in certain Activities within a Journey Builder interaction.
A customer will reach out stating that contacts are not moving through an interaction or that they are not entering the next step on an interaction.
Resolution
Troubleshooting steps
 
The first thing you want to do is see if the Activity that the Contact is "stuck" in is a Wait Activity.  
You can then search "InteractionStudio.ActivityLog" for a given DefinitionID to see if the Contact should or should not be in the Wait Activity and is still based on the StartDate value (The StartDate value is when they started the Wait). Based on how long the Wait should be, you can determine if they should still be in there. 
Another helpful table for this is the "InteractionStudio.ActivityWaitQueue."  This will show you whether or not the Wait has been processed or not.
If you've confirmed that the Contact should not be in the Wait Activity or they are stuck in an activity besides a Wait step, do the following: 
 
1. Search "InteractionStudio.ActivityLog" for a given ContactKey that is stuck.  
2. Find the StartDate and projected end date value for the Activity the ContactKey is stuck in based on the values in the serialized object field (this is for Wait Activities, for other activity types use the EndDate value).  
3. Calculate the projected end date value for the activity and then connect to AppLog on the appropriate stack.
4. Search the error table for the MID where the ErrorDate is approximately the time that the Subscriber should have exited the Activity they are stuck in.
This error should give you insight into why the contact is not progressing through the interaction.
