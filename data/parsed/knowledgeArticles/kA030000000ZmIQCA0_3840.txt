### Topic: To assign a user a Work.com User feature license through DataLoader set their UserPermissionsWorkDotComUserFeature field to true.
To assign a user a Work.com User feature license through DataLoader set their  
UserPermissionsWorkDotComUserFeature 
field to
 true
Resolution
1. Open 
DataLoader
 (or 
LexiLoader
 if you're on a Mac)
2. Click the 
Export
 button
3. Login with your Salesforce Admin credentials if prompted.
 
Note:
 If your organization restricts IP addresses, logins from untrusted IPs are blocked until they’re activated. Salesforce automatically sends you an activation email that you can use to log in. The email contains a security token that you must add to the end of your password. For example, if your password is mypassword, and your security token is XXXXXXXXXX, you must enter mypasswordXXXXXXXXXX to log in.
4. Hit the 
Next
 button after you've logged in successfully
5. Select the 
User
 object
6. Choose the 
Id, FirstName, Lastname
 and 
UserPermissionsWorkDotComUserFeature
 fields then click 
Finish
7. Open the CSV file you just exported
8. Set the 
UserPermissionsWorkDotComUserFeature 
column to
 true
 for every user you want to assign a Work.com User Feature license to and save the file
Note:
 Ensure the users have a valid salesforce license. Work.com User Feature licenses can be added to the following salesforce license types:
Salesforce CRM (a.k.a. Full CRM)
Salesforce Platform 
Salesforce Platform One
Force.com One App
Partner App Subscription
Force.com App Subscription
Chatter Plus
9. Go back into DataLoader and hit the 
Update
 button
10. Select the 
User
 object, choose the file from step 8 and hit 
Next
11. Click 
Create or Edit a Map
12. Click 
Auto-Match Fields to Column
 then click 
OK
13. Click 
Next
14. Choose a directory to store success and error files and click 
Finish
You should be all set at this point.  If an error file was generated open it up and correct any errors you find.  You can confirm the updates were made by going to a user's detail page and verify the Work.com User Checkbox is enabled.
