### Topic: Cause of security errors when clicking links in an email sent by the Marketing Cloud
When clicking links from a Marketing Cloud Send, clients might receive a security error, indicating "Your connection is not private" or something similar. See attached screenshot for an example.
Resolution
If the clients have enabled HSTS (HTTP Strict Transport Security) on their domain, it automatically turns all links to https.
If no SSL is in place, this will cause a security error.
If they do not have SSL, this is what can be done:
Client needs to disable HSTS, or if they cannot do that, they will need to purchase 2 SSL instances through their Marketing Cloud Account Executive:
1 for images
1 for pages, click, view
Once they purchase these, we can proceed with securing their links and images which will resolve the error.
Quick fix option if they cannot remove HSTS:
We can revert their tag set back to ET defaults until they purchase the SSL instances (which means all links and images will reference the exacttarget urls). Once the SSLs are ready, we can apply to their account and change the tags back to their domain and they will then be secured.
Just a reminder, SSL setup can take 2-4 weeks to complete so if the client is in a bind and needs to send, the Marketing Cloud default tag set is the only option we have to offer.
