### Topic: This article points to the link which explains what "Manager User" permission is all about
The "Manage User" permission is a powerful permissions that provides access numerous parts of of the system setup to allow one to manage a user.
Resolution
See the following blog entry for details on what "Manage User" allows one to do:
http://sites.force.com/blogs/ideaView?c=09a30000000D9xo&id=08730000000JLAh&returnUrl=/apex/ideaList%3Fc%3D09a30000000D9xo%26sort%3Drecent
