### Topic: Learn why a Measure that's set to pull soft bounces reports values that are different than the soft bounce number in Tracking.
Resolution
Curious why Measure and Tracking counts for Sends appear different? It's because 
Measures
 for a Send can report soft bounces, block bounces, and technical bounces separately. 
Tracking
 for a Send combines the data for all bounce types into a single value, so that number will always be larger.
