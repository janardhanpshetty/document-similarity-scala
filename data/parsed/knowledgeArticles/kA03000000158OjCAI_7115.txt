### Topic: Data Import Wizard “Accounts & Contacts” creates duplicate contact record if matching field (Names or email address) contains extra trailing blank space
Data Import Wizard “Accounts & Contacts” creates duplicate contact record if matching field contains extra trailing blank space.  Suppose, Contact record John Smith, Account Acme already exists in Salesforce.  In CSV file, suppose First Name contains an extra trailing blank space.  For example, First Name = "John ", Last Name = "Smith".  
1) Go to Data Import Wizard
3) Click on “Launch Wizard”
4) Click on “Accounts and Contacts”
5) Click on “Add new and update existing records”, ensure the following:
Match Contact by: Name
Match Account by: Account Name and Account Site
6) Click on CSV.  Select CSV file FirstNameWithSpace.csv
7) Click on Next
8) Click on Next
9) Click on “Start Import”
10) Go to Contact List View and you'll notice new contact record was created.
Every time you import FirstNameWithSpace.csv, duplicates are created for John Smith because CSV contains one extra space at end of First Name.  
* The same duplicates can happen if matching on email address and if emails in CSV file contain trailing extra space.
Resolution
The workaround is to simply remove extra space from matching field.  In this example, in CSV file, remove trailing extra space from First or Last Name:
Old:
 First Name = "John ", Last Name = "Smith".
New:
 First Name = "John", Last Name = "Smith".
If matching is done on email address, need to remove tailing spaces from email address in CSV file.  R&D plans to fix this behavior in future release so Data Import can automatically filter out extra white space.
