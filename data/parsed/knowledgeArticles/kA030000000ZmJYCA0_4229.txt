### Topic: Built-in filters cannot be added to custom reports.
Some Standard and canned reports have dedicated View filters and field filters, typically a "My Records" vs "All Records" view and a date filter. 
On the editing screen, these filters look like this:
On the report results screen, they look like this:
 
Resolution
There is currently no way to add this kind of dedicated filter to a custom report.
