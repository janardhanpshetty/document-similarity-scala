### Topic: This article contains a list of most common errors received while working with Mail Merge and their resolution/work around.
Please find below a list of common errors on Mail Merge/Extended Mail Merge. Along with the error, there are links to knowledge article to help reach resolution
Resolution
1. Error: Mail Merge File Error sforceCommandFile.frc
While performing a Mail Merge, you may get the following message instead of generating the word document:
 
Do you want to save this file?
Name: sforceCommandFile.frc
Type: Unknown File Type
From: na1.salesforce.com
 
This message generally appears when the ActiveX control, that is responsible for Mail Merge gets, unregistered. Follow the instructions given in the article link below to get rid of this error and successfully generate the Word Document: 
Mail Merge File Error sforceCommandFile.frc
2. Error: Exception: CMMHost::GenMailMerge:Exception
You will come across this error message the browser used for running Mail Merge is not optimized to use Mail Merge. This error can be resolved by following the instructions given in the following article: 
Getting Error using mail merge: Exception: CMMHost::GenMailMerge:Exception
3. Error: Unsupported Browser For Mail Merge
This error message appears when users are using Office 2010 with Windows 7 for Standard Mail Merge feature. The reason is standard mail merge is not supported with Winds 7 and Office 2010. To resolve this issue, you need to get Extended Mail Merge enabled for your organization. For more information, please refer to this article:
"Unsupported Browser For Mail Merge" error received while performing a Mail Merge using Office 2010 with Windows 7
4. Error: Failed to move template to results folder rename limit was exceeded (100)
OR
Failed to download the template file https...
OR
Please ensure that Microsoft Word is installed on your machine. Word could not fire the event
These error messages would appear if the browser is not optimized for using Mail Merge. To resolve this issue, please follow the steps given in this article:
What is the mail merge error 'Failed to download...' or 'Failed to move...?'
5. Error: Mail Merge SFCOM error: Cannot create http object?
This message may appear when a required file to use Mail Merge is not installed properly at your network or browser security setting. To resolve this error, run the setup file attached in the article link given below:
What to do if I get Mail Merge SFCOM error: cannot create http object?
6. Error: Exception: CMMHost::GenMailMerge:Exception Object variable or With block variable not set
OR
Exception:CMMHost::GenMailMerge:Exception Word cannot start the converter mswrd632.wpc.
These error messages are found due to Microsoft Security patch release. Modifying the registry, as suggested in the below mentioned article, would help you get rid of such errors. 
Why am I receiving a mail merge error after the recent Windows XP security update?
7. Error: "Could Not Match ServerURL in registry values"
This is a registry error which can be fixed by setting all add-ins as trusted programs. The steps are given in the below mentioned article which would help you getting this error resolved. 
Mail Merge 'Trusted Add Ins' & 'Macro Security Settings'
8. Error: "Exception: CMMHost::GenMailMerge:Exception Overflow"
This error message appears when the macro security setting for Microsoft Word is set to "High". Setting the macro security to "Medium" will allow you to use Mail Merge within Salesforce. Follow the steps given in this article to get rid of this error:
Mail Merge 'Trusted Add Ins' & 'Macro Security Settings'
9. Error: Mail Merge Exception: Automation server can't create object
This error message also appears when ActiveX Controls are missing from your desktop. Following the instructions given in this article may resolve the issue: 
Mail Merge Exception: Automation server can't create object
10. Error: Your request failed because this mail merge template was too large to process?
This error message would appear if you are using a Mail Merge Template that is larger than 1 MB. This is an expected behavior. Templates used for Extended Mail Merge are limited to 1MB, whereas Mail Merge templates are limited to 2MB.
