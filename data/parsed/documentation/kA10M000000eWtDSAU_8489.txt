### Topic: Sandbox and Deployment Resources | Salesforce
Sandbox and Deployment Resources | Salesforce
Sandbox and Deployment Resources
In addition to online help, Salesforce creates video demos, guides, and tip sheets to help you learn about our features and successfully administer Salesforce.
Release Management Videos
For End Users
For Admins
Release Management: Overview
Release Management: Developing and Testing on Sandbox
Release Management: Deploying Changes Using Change Sets
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=deploy_sandboxes_resources.htm&language=en_US
Release
202.14
