### Topic: With the Spring ‘16 release, the Data.com Social Key feature will be retired from the Data.com Clean application.
What exactly will happen in the Spring ‘16 release?
 
Once your org is upgraded to the Spring '16 release, Data.com Clean will no longer populate the Social Persona object with Twitter, Facebook, or LinkedIn handles.  Associated Data.com metrics for Social Key will no longer be accessible. API access to these social handles (via the “Social Persona” API) will no longer be available.
 
What is the recommended replacement or workaround?
 
Salesforce users may continue to manually associate a Twitter or Facebook handle from the Social Persona tab on Contacts and Leads.
 
Why are you retiring this product?
As of the Winter ‘16 release, LinkedIn will no longer be making its functionality available for use with Social Accounts, Contacts and Leads.
 
With this change, Social Key will no longer be able to deliver 
LinkedIn handles. Since this comprised the majority of data delivered by the Social Key, the decision was made to retire the feature.
 
Salesforce will instead focus its development efforts on the next generation of Data.com social features. 
 
Will previously populated by Data.com Clean social handles remain in place?
 
Yes, there is no impact to existing Social Persona records. 
 
Where can I find additional information? 
For additional questions, you can open a case with Customer Support via the Help & Training portal.
For an overview of our policy on retiring functionality, please click 
here
.
Resolution
