### Topic: When you upload a file to a library, it takes about 6 minutes before it shows up in the library.
When you upload a file to a Library, the files need to go through an indexing process, which can take up to 6 minutes. After the file has been indexed, you can see it within the Library.
Due to this behavior, you might not be able to find recently uploaded files in a Library.
A workaround to this issue is the following: you can access the uploaded files from Recent Items.
Resolution
