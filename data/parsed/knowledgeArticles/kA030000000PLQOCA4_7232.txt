### Topic: This article discusses the different options that can be used to mass update lists of records.
Requirement:  to be able to mass update records from a list view, including changing multiple field values.
Resolution
There are two potential methods:
"Mass Update And Mass Edit From List View" AppExchange App
 (note: this is not supported by Salesforce)
"Mass Update Anything" code sample from our Developer site
 (this is also not supported by Salesforce)
