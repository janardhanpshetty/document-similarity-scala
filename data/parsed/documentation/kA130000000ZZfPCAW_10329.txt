### Topic: Sample Metrics Formulas | Salesforce
Sample Metrics Formulas | Salesforce
Sample Metrics Formulas
Available in: both Salesforce Classic and Lightning Experience
Available in: 
All
 Editions
For details about using the functions included in these samples, see 
Formula Operators and Functions
.
Temperature Conversion
This formula converts Celsius degrees to Fahrenheit.
1.8 * degrees_celsius__c + 32
Unit of Measure Conversion
This formula converts kilometers to miles.
Miles__c/.621371192
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=useful_advanced_formulas_metrics.htm&language=en_US
Release
202.14
