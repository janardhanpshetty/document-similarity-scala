### Topic: Marketing Cloud Users who has a username of "api.*" is not visible in the application.
You created a User with a username that starts with "api." and receive a notification that the User was successfully created, but you can no longer find the newly created user under "My Users" in the Marketing Cloud application.
 
Resolution
Any User whose username begins with "api." doesn't appear in the Marketing Cloud application.  These Users can be administered in the Web Services API after they are created. The intent is to allow a User to be created for use with API based processes that can't be deactivated in the application.
