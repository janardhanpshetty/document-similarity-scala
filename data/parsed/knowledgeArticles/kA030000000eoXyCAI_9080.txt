### Topic: This article describes what details customers need to provide to salesforce support if they want to remap success community profiles
Sometimes, customers that are active members of the Success Community (includes IdeaExchange, Answers, etc.) have a need to change Salesforce user accounts (either within the same ORG ID, or moving to a completely new ORG ID) for a variety of reasons (leaving company, moving to a new org in same company, etc).  Often times when this is done you may want to retain your content (all of the Ideas, Votes, Comments, etc that you have submitted in the Success Community). This process is referred to as a remap, and the profiles are mapped 1:1. We are not able to perform profile merges nor remap multiple users to one user.
Resolution
Customers may need to contact salesforce support by providing documented permission of their understanding of below pointers before requesting a profile remap from one user to the another in the success community.
1) 
Support team can assist with profile remaps at this time since merging of profiles is not feasible, Old profile will be mapped to the new username and only current profile will be accessible post remap.
 
​2) You do not have any Dreamforce registration tied to the profiles that they are trying to remap since we cannot transfer Dreamforce details in profile remaps, you should wait and contact us when the Dreamforce is over. If you need to transfer Dreamforce registration please refer to this 
article
.
Dreamforce session information is not included in Success Community Profile Remaps. It is not possible to transfer past DF session details from one user to another. Please confirm that you understand that past DF session details will not be transferred when performing a profile remap. Support may be able to generate a list of past DF sessions associated to a particular user record if requested.
3) Please provide the URLs for their 2 profiles on the Success Community. Here is an 
example
 of a Salesforce Success Community URL and you can follow the steps 
here
 to get the id for your success community profile.
 Username A has profile A associated to it (old profile)
 Username B is the new one with profile B (new profile)​
 
4) Remap is 1:1 which means that Username B will be used to access A's information, all the emails for Profile A will be received on email address tied to Username B going forward. You are only losing profile B and any emails for profile B.
 
5) The remap process does not transfer the Trailhead history. We cannot remap or merge Developer community profiles so Trailhead history will remain unchanged as a result of the success community remap. If you need help with Trailhead please click 
here
.
6) Please promote 
this idea
 for profile remaps and note that the idea is currently under consideration by product management team with no ETA on delivery.
7) We cannot currently disable profiles in the success community, please vote for 
this
 idea.
8) If you have already synched your certification and Trailhead Data with your current profile please try to remove the sync first from the old profile before requesting a remap as that could not be carried over in the remap. Please read 
this article
 on how this could be done
9) If you have linked a Partner Community Profile user (username@partnerforce.com) to the Salesforce user record that you're having remapped, this link will not be updated when the Success Community remap is performed. Once the Success Community remap has been completed you will be required to authenticate the new profile/username to the Partner Community login and re-establish the previous partner portal permissions as they are not carried over. It is recommended to grant an alternative Partner Community user Admin privileges. To do so log in to the partner community (partners.salesforce.com | Log In) select the More picker at the top of the /partnerHome page and chose Manage Users to grant/confirm that a user from the list of users at /userManagement has privileges to re-establish your appropriate permissions post remap. Please confirm your understanding of this implication.
If you are unable to re-establish the link between your user login and your partner community user login (username@partnerforce.com) after the remap, it may be necessary for Salesforce Support to create an internal case on your behalf to engage appropriate Partner Support resources.
