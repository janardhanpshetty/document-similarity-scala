### Topic: Use this Internal knowledge article to resolve an issue if a client is having trouble linking their Facebook ad account to Active Audiences
How to diagnose the issue?
When a client attempts to link a Facebook ad account to Active Audiences from the administration tab and they are unable to do so. This is because a pop-up that either allows the client to add their login details or select an ad account to link does not appear. 
 
Resolution
How to resolve the issue?
The issue is being caused by cookies on the browser. If in google chrome the client can resolve the issue by opening a new incognito window. Note all open incognito windows most first be closed. 
Opening an incognito window in chrome
If the client is using Firefox they can resolve the issue by clearing their cookies. You can do this by clicking on 
History
> 
Clear Recent History> c
heck the cookies box and select 
everything 
as the time range to clear.
If the client is unable to resolve the issue using the above methodology escalate the issue to activeaudiences-support@salesforce.com. 
This issue is currently being tracked by Jira issue: https://jira.exacttarget.com:8443/browse/SAUDIENCE-1656
 
