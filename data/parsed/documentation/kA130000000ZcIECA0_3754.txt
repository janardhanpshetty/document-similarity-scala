### Topic: Configure and Customize Pages in Your Community | Salesforce
Configure and Customize Pages in Your Community | Salesforce
Configure and Customize Pages in Your Community
Pages are the building blocks of your community. They control how your users see and interact with the community.
Available in: Salesforce Classic
Available in: 
Enterprise
, 
Performance
, 
Unlimited
, and 
Developer
 Editions
User Permissions Needed
To create, customize, or publish a community:
“Create and Manage Communities”
OR
Site.com Publisher User
 field enabled on the user detail page
AND
Site administrator or designer role assigned at the site level
To enable Communities:
“Customize Application”
To create article types and article actions:
“Manage Salesforce Knowledge”
To create data categories:
“Manage Data Categories”
Each page contains a collection of components that are organized around a task or function—whether it’s opening a case or searching for an article. The pages that comprise each of the preconfigured community templates are ready for you to use with very little configuration or customizing needed. Additionally, you can create new pages or drag and drop other components on to existing pages as needed. If you create custom Lightning components, they appear in the list of available components on the left side of the page.
When you open a page for editing, you can set properties for how information appears, move components to different positions, or delete any components you don’t need.
Note
You can edit components within a page and delete inactive pages, but you can’t rename pages.
For example, the Home page in the Napili template contains components that let customers see trending articles and topics, explore articles organized by category, read discussions from the community, search or information, ask a question, and contact support. If you’re creating a community to deflect cases only and don’t want to let customers open a case within the community, you can remove the component for contacting support.
To edit a page, go to Community Builder
From the Page drop-down, select the page to edit.
Click 
 to open the Page Editor.
Select the component you want to edit.
In the Property Editor, edit any of the properties of the component.
To add a component to the page, simply drag it from the Lightning Components menu and drop it where you want it on the page.
To delete a component from the page, hover over the component and click 
.
After you’re done configuring the page, click 
Publish
.
See Also:
Which Community Template Should I Use?
Implementation guide: Community Templates for Self-Service
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=siteforce_commtemp_views.htm&language=en_US
Release
202.14
