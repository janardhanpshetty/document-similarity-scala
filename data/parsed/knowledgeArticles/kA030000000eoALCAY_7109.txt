### Topic: Visiting New York and wanting to check in on your most valuable customers and highest-quality leads? If your Salesforce records have accurate geocode information, it’s a cinch to find your nearby accounts, contacts, and leads.
Visiting New York and wanting to check in on your most valuable customers and highest-quality leads? If your Salesforce records have accurate geocode information, it’s a cinch to find your nearby accounts, contacts, and leads.
  
User Permissions Needed
To use the Salesforce API:
"API Enabled"
Resolution
1. Using the Salesforce API, just query the records within a specified radius of a latitude and a longitude.
This example shows the Salesforce Object Query Language (SOQL) query to get the first 20 contacts within a 50-mile radius of San Francisco:
SELECT Name, Email, MailingAddress, MailingCity, MailingState, MailingCountry, MailingLatitude, MailingLongitude
FROM Contact 
WHERE distance(MailingAddress, geolocation(37.775, -122.418), 'mi') < 50 
ORDER BY distance(MailingAddress, geolocation(37.775, -122.418), 'mi')
LIMIT 20;
 
