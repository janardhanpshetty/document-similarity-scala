### Topic: The breakdown of Engagement-Over-Time-Twitter-Dashboard
Case 
12712676
Breakdown of 
Analyze - Engagement Over Time (Twitter Dashboard) Clicks
 
Resolution
When a client uses Social Studio to publish a tweet that contains a link in it, they can opt in to use 
bit.ly
 to shorten that link and track it.
We pull those counts from 
bit.ly
 and store them." 
So "clicks" is the number of times the link the published with SS was clicked upon. (tracked via bitly)
