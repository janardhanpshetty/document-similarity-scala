### Topic: Account Settings | Salesforce
Account Settings | Salesforce
Account Settings
Enhance your accounts with hierarchy information, insights, logos, autofill, and more. To find all these settings, from Setup, go to the Accounts Settings page.
User Permissions Needed
To change account settings
“Customize Application”
Setting
Description
Show View Hierarchy Link
Displays a link to the account hierarchy on account details. Click the link to view the relationships between the account and its parent account.
Account hierarchy is available in Salesforce Classic.
Enable Account Insights
Provides users with a customized list of timely, account-relevant news from US sources.
Account Insights is available on account and opportunity records in Lightning Experience and Salesforce1.
Be sure to add the Insights component to your opportunity page layouts.
Enable Account Autofill
Displays US-based companies in the Account Name field as users enter information. Users can select a suggested company from the list, making it easier to create new business accounts.
Account Autofill is available in Lightning Experience.
Enable Account Logos
Displays company logos, when available, on US-based accounts. New logos may replace ones from social profiles. Logos also appear with account suggestions.
To remove a logo from an account, contact Salesforce Customer Support.
Account Logos is available in Lightning Experience and Salesforce1.
This release contains a beta version of Account Logos that is production quality but has known limitations.
Contacts to Multiple Accounts
Allows users to relate a contact to multiple accounts. After enabled, be sure to 
complete the additional setup steps
.
Contacts to Multiple Accounts is available in Lightning Experience, Salesforce Classic, and Salesforce1
See Also:
Account Hierarchy
Account Insights
Contacts to Multiple Accounts
Accounts
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=accounts_settings.htm&language=en_US
Release
202.14
