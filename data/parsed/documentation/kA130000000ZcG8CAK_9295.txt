### Topic: Activities in Salesforce1: Limits and Differences from the Full Salesforce Site | Salesforce
Activities in Salesforce1: Limits and Differences from the Full Salesforce Site | Salesforce
Activities in Salesforce1: Limits and Differences from the Full Salesforce Site
Events
The Subject field doesn’t include a picklist of previously defined subjects.
Archived events aren’t available.
You can’t use Shared Activities to relate multiple objects to an event.
You can’t add events to Microsoft® Outlook®.
You can’t add invitees to events or remove them from events.
You can’t accept or decline an event you’ve been invited to.
The Recurrence and Reminder sections aren’t displayed on event detail or edit pages.
Events respect your 
Salesforce
 time zone settings, not the time zone setting on your mobile device.
When you view 
Salesforce
 events from the 
Events
 item in the 
Salesforce1
 navigation menu, the date bar always begins on Sunday and ends on Saturday. The begin and end dates are the same regardless of your device and 
Salesforce
 locale settings.
If viewing the current day’s event list at 11:59pm, the list doesn’t automatically refresh to the next day at Midnight.
Invitee related lists display slightly different content from the full 
Salesforce
 site. In the full site, the Invitee list includes the event owner and invitees. In 
Salesforce1
, the Invitee related list includes invitees only. The following queries let you reproduce the full site functionality in 
Salesforce1
.
If you use Shared Activities in your organization, to allow the event organizer to see all the invitees, use this query:
SELECT RelationId FROM EventRelation WHERE isInvitee = true AND eventId='[Event_Id]'
where 
Event_Id
 is the child event’s ID.
To allow the event organizer to see all the invitees if your organization doesn’t use Shared Activities, use this query:
SELECT RelationId FROM EventRelation WHERE eventId='[Event_Id]'
These queries get the main event’s relations and display them for the given child event. You can add a 
WHERE
 clause to further filter the results.
Tasks
The Subject field doesn’t include a picklist of previously defined subjects.
Only the 
My Tasks
, 
Completed Within Last 7 Days
, 
Delegated
, and 
Today
 lists are available for the Tasks item in 
Salesforce1
. No other task lists, including 
Overdue
, 
This Month
, and 
All Open
, are available in the mobile app.
Archived tasks aren’t available.
You can’t use Shared Activities to relate multiple contacts to a task.
Group (multiuser) tasks aren’t available.
The Recurrence and Reminder sections aren’t displayed on task detail or edit pages.
When you close a task by tapping the 
 icon, the task is shown crossed out until you refresh the list.
In task lists, the order of the fields in the priority picklist determines the order in which tasks are sorted.
The more tasks that you have, and the more relationships that your tasks have to other records, the longer it can take to view tasks or use other features in the 
Salesforce1
 app.
When more than 
1,000
 overdue tasks exist, task lists in Salesforce1 don’t display any overdue tasks at all.
 Use the full site to view your overdue tasks and close them, postpone them, or delete their due dates.
Task layouts contain a few unique elements that make tasks easier to work with. These elements don’t appear in a compact layout because you can’t change them, but users always see them:
The 
 and 
 icons represent the status of the 
IsClosed
 field to users with the Edit Task permission.
The 
 icon represents a task marked high priority (including custom high priority).
All tasks show the subject.
If the due date exists and a user has permission to view it, all tasks show the due date.
Tasks include the primary contact and the related account or other record, when they exist.
The fields in each list can vary depending on the settings in your 
Salesforce
 org.
You control the layout of task records and tasks in the task list using compact layouts. You control related lists, as always, using the page layout editor. Adding the subject or due date field to either layout doesn’t change the appearance of tasks—those fields never appear twice.
Below the built-in task elements, 
Salesforce1
 displays up to three other fields.
The default compact layout for tasks includes two fields: the name of a lead or contact, and an opportunity, account, or other record the task is related to.
In an Activities related list, a task’s fields depend on what record you’re viewing and how you’ve defined the layout for that object.
For more information, see 
Compact Layouts
.
Help Site URL
https://help.salesforce.com/apex/HTViewHelpDoc?id=limits_mobile_sf1_activities.htm&language=en_US
Release
200.20
